@RestResource(urlMapping='/TalentBaseController/*')
global with sharing class TalentBaseController {
    public static User usr;
    public static final String soqlGetUserInfo = 'SELECT Id, profile.Id, profile.Name, LocaleSidKey, Contact.Account.Merge_State__c,LanguageLocaleKey, AccountId, Address,duplicateUsername__c, FirstName, CommunityNickname, LastName, Suffix, FullPhotoUrl, Title, Peoplesoft_Id__c, OPCO__c, MediumPhotoUrl, Office_Code__c, ' +
        'Contact.Account.Assign_End_Notice_Close_Date__c, Contact.TC_Talent_Preference_Flag__c, Contact.Account.Talent_Recruiter__c, Contact.Account.Talent_PTO_hours__c, Contact.TC_MailingCity__c,Contact.TC_MailingState__c, Contact.Account.Talent_Account_Manager__c, Contact.Account.Talent_Community_Manager__c, Contact.Account.Talent_CSA__c, Contact.Account.Talent_Biller__c, Contact.Id, Contact.MailingCity, Contact.MailingState, ' +
        'Contact.Account.Talent_End_Date__c, Contact.Account.Talent_Start_Date__c, Contact.Account.Talent_Preference__c, Talent_Preference__c, Contact.End_Date_Change_Requested__c, Contact.Recruiter_Change_Requested__c, ' +
        'Contact.Account.Talent_Recruiter__r.IsActive, Contact.Account.Candidate_status__c, Contact.Account.Talent_Account_Manager__r.IsActive, Contact.Account.Talent_Community_Manager__r.IsActive, Contact.Account.Talent_CSA__r.IsActive, Contact.Account.Talent_Biller__r.IsActive, ' +
        'Contact.Account.TC_Survey_Taken__c, Contact.Title, Contact.Account.Talent_Delivery_Manager__c,Contact.Account.Talent_Delivery_Manager__r.IsActive,Contact.Account.Talent_Community_Rep__c, Contact.Account.Talent_Community_Rep__r.IsActive , Contact.Account.Talent_Program_Manager__c,Contact.Account.Talent_Program_Manager__r.IsActive, Contact.Vendor_Name__c, Contact.externalSourceId__c, Contact.TC_Mailing_Address__c, Contact.Account.Talent_Community_Status__c  ' +
        'FROM User ' +
        'WHERE Id = :runningUserId';

    public TalentBaseController() {
        usr = [select id, Name, Opco__c, Title, FullPhotoUrl, duplicateUsername__c,Peoplesoft_Id__c from User where id =: UserInfo.getUserId()];

    }

    @AuraEnabled
    public static TalentBaseModel GetAllTalentById(string recordId){
        TalentBaseModel tbm = new TalentBaseModel();
        string runningUserId = recordId;
        User runningUser = Database.query(soqlGetUserInfo);
        tbm.OperatingCompany = runningUser.Opco__c;
        return tbm;
    }

    @AuraEnabled
    public static TalentBaseModel GetAllTalentDataById(string recordId){
        TalentBaseModel tbm = new TalentBaseModel();

        try {
            string runningUserId = recordId;

            User runningUser = Database.query(soqlGetUserInfo);

            tbm = FillTalentBaseModel(runningUser);

            //Custom Settings
            try {
                tbm.UsefulLinks = FillUsefulLinks(TC_UsefulLinks__c.getInstance(tbm.UserId), tbm.userLang, false, runningUser.Id);
                tbm.OpCoMappings = TC_Opco_Mappings__c.getInstance(tbm.UserId);
                tbm.FAQCategories = TC_KnowledgeController.GetKnowledgeFirstLevelCategoriesByProfile(tbm.ProfileId);
            }
            catch (Exception ex){
                System.debug('GetAllTalentData CustomSettings Error: ' + ex.getMessage());
            }
        }
        catch (Exception ex){
            System.debug(ex);
        }

        return tbm;
    }

    @HttpGet
    @AuraEnabled
    global static TalentBaseModel GetAllTalentData() {
        System.debug(Site.getBaseURL());
        boolean isMobileApp = (RestContext.request != null);
        TalentBaseModel tbm = new TalentBaseModel();

        try {
            string runningUserId = UserInfo.getUserId();
            User runningUser = Database.query(soqlGetUserInfo);

            tbm = FillTalentBaseModel(runningUser);

            //Custom Settings
            try {
                tbm.UsefulLinks = FillUsefulLinks(TC_UsefulLinks__c.getInstance(tbm.UserId),tbm.userLang, isMobileApp, runningUser.Id);
                tbm.OpCoMappings = TC_Opco_Mappings__c.getInstance(tbm.UserId);
                tbm.KeyContacts = TC_KeyContactsController.GetKeyContacts(tbm.UserId, tbm.OpCoMappings.Community_Base_Url__c);
                tbm.FAQCategories = TC_KnowledgeController.GetKnowledgeFirstLevelCategoriesByProfile(tbm.ProfileId);
            }
            catch (Exception ex){
                System.debug('GetAllTalentData CustomSettings Error: ' + ex.getMessage());
            }
        }
        catch (Exception ex){
            System.debug(ex);
        }

        return tbm;
    }

    @AuraEnabled
    public static void saveRecord(String title){
        system.debug('@@@@@ Returning Record');
        user u= new user();
        u.id=UserInfo.getUserId();
        u.Title=title;
        update u;
    }

    @AuraEnabled
    public static User getUser() {
        User u = [select id, Name, Title, FullPhotoUrl,duplicateUsername__c,LocaleSidKey, Peoplesoft_Id__c from User where id =: UserInfo.getUserId()];
        return u;
    }

    public static String[] GetSurveyConfig(Id profileId) {
        String[] config = new String[]{'', ''};

            System.debug('Survey: ProfileId: ' + profileId);
        System.debug('Survey: ');

        TC_Survey_Config__mdt[] survey = [SELECT Id, Survey_Condition_Login_Limit__c, Survey_Link__c FROM TC_Survey_Config__mdt WHERE Community_Profile__c in (SELECT Id
                                                                                                                                                               FROM Community_Profiles__mdt
                                                                                                                                                               WHERE Profile_Id__c = :profileId)];

        if (survey.size() > 0) {
            config[0] = String.valueOf(survey[0].Survey_Condition_Login_Limit__c);
            config[1] = survey[0].Survey_Link__c;
        }
        return config;
    }

    @AuraEnabled
    public Static void insertTalentReferral(String Firstname, String Lastname, string Title, string phone, String Note, String Skills, String Email, Id AccountId) {
        System.debug('>>>>>>>>');
        Talent_Referral__c tr = new Talent_Referral__c();
        tr.First_Name__c = Firstname;
        tr.Last_Name__c = Lastname;
        tr.Title__c = Title;
        tr.Phone__c = Phone;
        tr.Note__c = Note;
        tr.Skills__c = Skills;
        tr.Email__c = Email;
        tr.Talent__c = AccountId;
        tr.Shared_Job__c = false;


        if (tr != null) {
            System.debug('>>>>>>>>');
            insert tr;
        }
    }

    @AuraEnabled
    public static void createNewTalentReferral(String Email, String FirstName, String LastName, Boolean sharedJob, string reqId, string talentId) {
        try{
            List<Talent_Referral__c> tr = [Select ID, Name, Email__c from Talent_Referral__c where Email__c = :Email];
            reqId = TC_TalentAllDataAccessController.getReqId(reqId);
            if(tr.size() > 0) {
                tr[0].First_Name__c = FirstName;
                tr[0].Last_Name__c = LastName;
                update tr[0];

                Talent_Job_Referral__c tjr = new Talent_Job_Referral__c();

                //if (reqId.startsWith('a1T'))
                //    tjr.Req__c = reqId;
                //else if (reqId.startsWith('006')) { 
                    List<Reqs__c> reqs = [SELECT Id, EnterpriseReqId__c FROM Reqs__c WHERE EnterpriseReqId__c = :reqId LIMIT 1];
                    if (reqs.size() > 0) {
                        if (!String.isBlank(reqs[0].Id)) {
                            tjr.Req__c = reqs[0].Id;
                        }
                    }
                //}

                tjr.Talent_Referral__c = tr[0].id;
                tjr.Referral_Email__c = Email;
                insert tjr;
            }

            if(tr.size() == 0) {
                Talent_Referral__c tr1 = new Talent_Referral__c();
                tr1.Email__c = Email;
                tr1.First_Name__c = FirstName;
                tr1.Last_Name__c = LastName;
                tr1.Talent__c = talentId;
                tr1.Shared_Job__c = sharedJob;
                insert tr1;

                Talent_Job_Referral__c tjr1 = new Talent_Job_Referral__c();

                if (reqId.startsWith('a1T'))
                    tjr1.Req__c = reqId;
                else if (reqId.startsWith('006')) {
                    List<Reqs__c> reqs = [SELECT Id, EnterpriseReqId__c FROM Reqs__c WHERE EnterpriseReqId__c = :reqId LIMIT 1];
                    if (reqs.size() > 0) {
                        if (!String.isBlank(reqs[0].Id)) {
                            tjr1.Req__c = reqs[0].Id;
                        }
                    }
                }

                tjr1.Talent_Referral__c = tr1.id;
                tjr1.Referral_Email__c = Email;
                insert tjr1;
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            throw e;
        }
    }

    @AuraEnabled
    public static Account getAccount() {
        Account a = [select Talent_Recruiter__c, Talent_Account_Manager__c, Talent_Community_Manager__c, Talent_PTO_hours__c from Account WHERE Peoplesoft_Id__c = :usr.Peoplesoft_Id__c];
        return a;
    }

    private static TalentBaseModel FillTalentBaseModel(User u) {
        TalentBaseModel tbm = new TalentBaseModel();

        try {
            tbm.UserId = u.Id;
            tbm.ContactId = u.Contact.Id;
            tbm.AccountId = u.AccountId;
            tbm.userLang = u.LanguageLocaleKey;

            tbm.ProfileId = u.profile.Id;
            tbm.ProfileName = u.profile.Name;

            if (u.Contact.Account.Talent_Recruiter__r.IsActive)
                tbm.RecruiterId = u.Contact.Account.Talent_Recruiter__c;
            if (u.Contact.Account.Talent_Account_Manager__r.IsActive)
                tbm.AccountManagerId = u.Contact.Account.Talent_Account_Manager__c;
            if (u.Contact.Account.Talent_Community_Manager__r.IsActive)
                tbm.CommunityManagerId = u.Contact.Account.Talent_Community_Manager__c;
            if (u.Contact.Account.Talent_Delivery_Manager__r.IsActive)
                tbm.TalentDeliveryManagerId = u.Contact.Account.Talent_Delivery_Manager__c;
            if (u.Contact.Account.Talent_Program_Manager__r.IsActive)
                tbm.TalentProgramManagerId = u.Contact.Account.Talent_Program_Manager__c;
            if (u.Contact.Account.Talent_Community_Rep__r.IsActive)
                tbm.CommunityRepId = u.Contact.Account.Talent_Community_Rep__c;
            if (u.Contact.Account.Talent_CSA__r.IsActive)
                tbm.CSAId = u.Contact.Account.Talent_CSA__c;
            if (u.Contact.Account.Talent_Biller__r.IsActive)
                tbm.BillerId = u.Contact.Account.Talent_Biller__c;
            if (u.Contact.Account.Talent_PTO_hours__c != null)
               tbm.PTOhours = u.Contact.Account.Talent_PTO_hours__c;

            /*tbm.RecruiterId = u.Contact.Account.Talent_Recruiter__c;
tbm.AccountManagerId = u.Contact.Account.Talent_Account_Manager__c;
tbm.CommunityManagerId = u.Contact.Account.Talent_Community_Manager__c;
tbm.CSAId = u.Contact.Account.Talent_CSA__c;*/

            if (u.CommunityNickname != '')
                tbm.FullName = u.CommunityNickname + ' ' + u.LastName;
            else
                tbm.FullName = u.FirstName + ' ' + u.LastName;
            tbm.ProfileUrl = Site.getBaseUrl() + '/s/profile/' + String.valueOf(u.Id).substring(0, 15);
            tbm.SettingsUrl =  Site.getBaseUrl() + '/s/settings/' + String.valueOf(u.Id).substring(0, 15) + '?tabset-cceed=2';
            //tbm.ProfileUrl = Site.getBaseUrl() + '/' + String.valueOf(u.Id).substring(0, 15);
            tbm.ContactUrl = Site.getBaseUrl() + '/' + u.Contact.Id;
            tbm.JobsearchUrl = Site.getBaseUrl() + '/s/jobsearch';
            tbm.automatchUrl = Site.getBaseUrl() + '/s/automatch';
            tbm.FirstName = u.FirstName;
            tbm.Usernamedup = u.duplicateUsername__c;
            tbm.LastName = u.LastName;
            tbm.Suffix = u.Suffix;
            tbm.CandidateStatus = u.Contact.Account.Candidate_Status__c;
			tbm.talentCommunityStatus = u.Contact.Account.Talent_Community_Status__c;
            if(String.isBlank(u.Contact.Account.Merge_State__c)){
                tbm.TC_Mailing_Address = u.Contact.TC_Mailing_Address__c;
                tbm.MailingCity = u.Contact.MailingCity;
                tbm.MailingState = u.Contact.MailingState;
            }else if(!String.isBlank(u.Contact.Account.Merge_State__c) && String.valueOf(u.Contact.Account.Merge_State__c)=='Post'){
                tbm.TC_Mailing_Address = u.Contact.TC_Mailing_Address__c;
                tbm.MailingCity = u.Contact.TC_MailingCity__c;
                tbm.MailingState = u.Contact.TC_MailingState__c;
            }
            tbm.Title = u.Title;
            tbm.StartDate = GetStartDate(tbm);
            tbm.AssignEndNoticeCloseDate = u.Contact.Account.Assign_End_Notice_Close_Date__c;
            tbm.EndDateChangeRequested = u.Contact.End_Date_Change_Requested__c;
            tbm.RecruiterRequested = u.Contact.Recruiter_Change_Requested__c;
            if(u.Contact.Account.Talent_End_Date__c != null)
             tbm.EndDate =  u.Contact.Account.Talent_End_Date__c;
            tbm.AssignmentProgressPercent = GetAssignmentProgress(tbm);

            tbm.DaysRemainingOnAssignmentStart = GetAssignmentStartDays(tbm);
            tbm.StatDateInFuture = isAssignmentinFuture(tbm);

            tbm.PeoplesoftId = u.Peoplesoft_Id__c;
            tbm.FullPhotoUrl = u.FullPhotoUrl;
            tbm.MediumPhotoUrl = u.MediumPhotoUrl;
            if(!u.profile.Name.contains('GS')){
                tbm.OperatingCompany = formatOpCo(u.OPCO__c, u.profile.Name);
            }else{
                tbm.OperatingCompany = 'TGS';
            }
            tbm.communityName = formatcomName(u.profile.Name);
            tbm.PreferencesJson = u.Talent_Preference__c;
            tbm.SurveyStatus = u.Contact.Account.TC_Survey_Taken__c;

            String[] surveyConfig = GetSurveyConfig(u.profile.Id);

            if (!String.isEmpty(surveyConfig[0]) && !String.isEmpty(surveyConfig[1])) {
                tbm.SurveyConditionLimit = Integer.valueOf(surveyConfig[0]);
                tbm.SurveyLink = surveyConfig[1];
            }

            tbm.SavedJobs = GetSavedJobs(u.AccountId);
            tbm.SiteBaseUrl = Site.getBaseUrl();
            tbm.VendorName = u.Contact.Vendor_Name__c; // Added by Venkat
            tbm.hasPersonalCases = hasPersonalCases();
            tbm.CBProfileId = u.Contact.externalSourceId__c;
            tbm.OfficeCode = u.Office_Code__c;

        }
        catch (Exception ex) {
            System.debug('FillTalentBaseModel Error: ' + ex.getMessage());
        }
        return tbm;
    }

    @AuraEnabled
    public static List<TC_UsefulLinks> FillUsefulLinks(TC_UsefulLinks__c usefulLinksCustomSetting, String userLang, boolean isMobileApp, String userId) {
        List<TC_UsefulLinks> linkList = new List<TC_UsefulLinks>();

        integer i = 1;
        do {
            string labelField;
            system.debug('=====================userLang'+userLang);

            if(userLang == 'fr_CA'){
                labelField = 'Useful_Links_Label_' + i + '_fr__c';
            }else{
                labelField = 'Useful_Links_Label_' + i + '__c';
            }
            string urlField = 'Useful_Links_Url_' + i + '__c';
            string iconField = 'Useful_Links_Icon_' + i + '__c';
            string iconCategoryField = 'Useful_Links_Icon_Category_' + i + '__c';
            string categoryField = 'Useful_Links_Category_' + i + '__c';
            string sldsField = 'Useful_Links_SLDS_' + i + '__c';
            string recentFormerDaysField = 'Useful_Links_Recent_Former_Days_' + i + '__c';

            string labelValue = (string)usefulLinksCustomSetting.get(labelField);
            string urlValue = (string)usefulLinksCustomSetting.get(urlField);
            string iconValue = (string)usefulLinksCustomSetting.get(iconField);
            string iconCategoryValue = (string)usefulLinksCustomSetting.get(iconCategoryField);
            string categoryValue = (string)usefulLinksCustomSetting.get(categoryField);
            Boolean sldsValue = (Boolean)usefulLinksCustomSetting.get(sldsField);
            integer recentFormerDaysValue = integer.valueOf(usefulLinksCustomSetting.get(recentFormerDaysField));

            TC_UsefulLinks t = new TC_UsefulLinks();
            t.Category = categoryValue;
            t.Icon = iconValue;
            t.IconCategory = iconCategoryValue;
            t.Label = labelValue;
            t.SLDS = sldsValue;
            t.RecentFormerDays = recentFormerDaysValue;

            //Pass the login URL through the API
            try {
                if (isMobileApp) {
                    if (urlValue.contains('tc_abenity_login_redirect')){
                        String urlRedirect = TC_AbenitySsoController.GetAbenitySSOUrl(userId);
                        if (!String.isEmpty(urlRedirect))
                            urlValue = urlRedirect;
                        else
                            urlValue = 'https://allegisbenefits.employeediscounts.co/';
                    }
                }

                t.URL = urlValue;
            }
            catch (Exception ex) {
                System.debug(ex.getMessage());
            }

            linkList.add(t);

            i++;
        }
        while(i < 12);

        if (!isMobileApp) {
            List<Topic> topics = [SELECT Id, Name, TalkingAbout, Description FROM Topic WHERE ManagedTopicType LIKE '%Featured%'];
            for (Topic topic : topics) {
                TC_UsefulLinks t = new TC_UsefulLinks();
                t.Category = 'resources';
                t.Icon = '';
                t.IconCategory = '';
                t.Label = topic.Name;
                t.SLDS = false;
                t.RecentFormerDays = 9999;
                t.URL = Site.getBaseUrl() + '/s/topic/' + topic.Id;
                linkList.add(t);
            }
        }

        return linkList;
    }

    @AuraEnabled
    public static void SaveTalent(String TalentBaseModelJson) {
        SaveTalent(TalentBaseModelJson, true, false, false);
    }

    @AuraEnabled
    public static void SaveTalent(String TalentBaseModelJson, Boolean updateUser, Boolean updateContact, Boolean updateAccount) {
        System.debug('SaveTalent:Enter');
        TalentBaseModel tbm = (TalentBaseModel)System.JSON.deserialize(TalentBaseModelJson, TalentBaseModel.class);
        System.debug('SaveTalent:json: ' + TalentBaseModelJson);

        try {
            String runningUserId = tbm.UserId;

            User runningUser = Database.query(soqlGetUserInfo);

            //Save User Object
            if (updateUser) {
                System.debug('SaveTalent:Updating User');

                if (runningUser.Talent_Preference__c != tbm.PreferencesJson)
                    runningUser.Talent_Preference__c = tbm.PreferencesJson;

                if (runningUser.Title != tbm.Title)
                    runningUser.Title = tbm.Title;

                update runningUser;
            }

            //Save Contact Object
            if (updateContact) {
                System.debug('SaveTalent:Updating Contact');

                if (runningUser.Contact.End_Date_Change_Requested__c != tbm.EndDateChangeRequested) {
                    System.debug(runningUser.Contact.End_Date_Change_Requested__c + '--' + tbm.EndDateChangeRequested);
                    runningUser.Contact.End_Date_Change_Requested__c = tbm.EndDateChangeRequested;
                }

                if (runningUser.Contact.Recruiter_Change_Requested__c != tbm.RecruiterRequested) {
                    System.debug(runningUser.Contact.Recruiter_Change_Requested__c + '--' + tbm.RecruiterRequested);
                    runningUser.Contact.Recruiter_Change_Requested__c = tbm.RecruiterRequested;
                }

                if (runningUser.Contact.Title != tbm.Title) {
                    System.debug(runningUser.Contact.Title + '--' + tbm.Title);
                    runningUser.Contact.Title = tbm.Title;
                }

                update runningUser.Contact;
            }

            //Save Account Object
            if (updateAccount) {
                System.debug('SaveTalent:Updating Account');

                if (runningUser.Contact.Account.Talent_Preference__c != tbm.PreferencesJson){
                    runningUser.Contact.Account.Talent_Preference__c = tbm.PreferencesJson;
                    if(runningUser.Contact.TC_Talent_Preference_Flag__c == false){
                        runningUser.Contact.TC_Talent_Preference_Flag__c = true;
                    }else{
                        runningUser.Contact.TC_Talent_Preference_Flag__c = false;
                    }
                }
                if (runningUser.Contact.Account.Assign_End_Notice_Close_Date__c != tbm.AssignEndNoticeCloseDate )
                    runningUser.Contact.Account.Assign_End_Notice_Close_Date__c = tbm.AssignEndNoticeCloseDate;

                if (runningUser.Contact.Account.TC_Survey_Taken__c != tbm.SurveyStatus )
                    runningUser.Contact.Account.TC_Survey_Taken__c = tbm.SurveyStatus;

                if (runningUser.Contact.Account.TC_Survey_Taken__c != tbm.SurveyStatus )
                    runningUser.Contact.Account.TC_Survey_Taken__c = tbm.SurveyStatus;

                update runningUser.Contact.Account;
                update runningUser.Contact;
            }
        }
        catch (Exception ex) {
            System.debug('An unexpected error has occurred: ' + ex.getMessage());
        }
    }

    private static string formatOpCo(string opco, string profileName) {
        
		String newCoEnabler = system.Label.JobSeeker_CreateAcc;

        if (opco == 'TEK' || opco == 'TEKsystems, Inc.'){
            return 'TEKsystems';
        }else if ((opco == 'ONS' || opco == 'Aerotek') && !profileName.contains('Aston') && !profileName.contains('EASi')){
            return 'Aerotek';
        }else if (profileName.contains('Aston')){
                return 'Aston Carter';
		}else if (profileName.contains('EASi') && newCoEnabler != 'True'){
                return 'EASi';
        }else if (profileName.contains('EASi') && newCoEnabler == 'True'){
            return 'NewCo';
        }else{
            return '';
		}
    }

    private static string formatcomName(string opco) {
        if (opco.contains('Tek') || opco.contains('TEK'))
            return 'the TEKsystems Community';
        else if (opco.contains('Aston'))
            return 'Aston Carter Communities';
        else if (opco.contains('Aero'))
            return 'Aerotek Communities';
        else
            return '';
    }

    private static Date GetStartDate(TalentBaseModel tbm){
        try {
            Talent_Work_History__c[] twh = [SELECT Id, Start_Date__c FROM Talent_Work_History__c WHERE Talent__c = :tbm.AccountId ORDER BY Start_Date__c DESC LIMIT 1];

            if (twh.size() > 0)
                if (twh[0].Start_Date__c != null)
                return twh[0].Start_Date__c;

            return null;
        }
        catch (Exception ex) {
            System.debug('GetStartDate Error: ' + ex.getMessage());
            return null;
        }
    }

    private static Integer GetAssignmentProgress(TalentBaseModel tbm){
        try {
            if (tbm.EndDate > Date.today()) {

                Integer daysLeft = Date.today().daysBetween(tbm.EndDate);
                Integer daysBetween = tbm.StartDate.daysBetween(tbm.EndDate);

                Integer i1 = daysBetween - daysLeft;
                Double i2 = Double.valueOf(i1) / Double.valueOf(daysBetween);
                Double i3 = Double.valueOf(i2) * 100;

                Integer progress = Integer.valueOf(i3.round());
                if (progress > 0)
                    return progress;
                else
                    return 0;
            }
            else {
                return 100;
            }
        }
        catch (Exception ex) {
            System.debug(ex.getMessage());
            return 0;
        }
    }

    @AuraEnabled
    public static string GetSkillsFromApi(string title, string terms, string max) {
        User usr = [select Id, Name, Title, OPCO__c from User where Id =: UserInfo.getUserId()];
    
        return TC_GetSkillsRestController.GetSuggested(title, terms, max, usr.OPCO__c);
    }

    @AuraEnabled
    public static String Skills(String httpRequestType,String requestBody) {
        system.debug('==============' + httpRequestType);
        String queryString;
        String location;
        //String requestBody ='{"query":{"term": {"titlevector": "qa tester"}},"aggs":{"skill_counts":{"terms":{"script":{"inline": "doc['skillsvector'].values"},"size": 100}}}}';

        ApigeeOAuthSettings__c serviceSettings = null;
        if (httpRequestType == 'POST' ) {
            serviceSettings = ApigeeOAuthSettings__c.getValues('SkillSearch');
        }
        return match(serviceSettings, queryString, httpRequestType, requestBody, location);
    }

    private static String match(ApigeeOAuthSettings__c connSettings, String queryString, String httpRequestType, String requestBody, String location) {
        Http connection = new Http();
        HttpRequest req = new HttpRequest();
        System.debug('serviceSettings 2'+connSettings.Service_URL__c);
        System.debug('queryString >>>>>>>> ' + connSettings.Service_URL__c + queryString);
        //queryString = addTrackingIds(queryString);
        System.debug('requestBody >>>>>>>> ' + requestBody);
        req.setEndpoint(connSettings.Service_URL__c);
        req.setMethod(connSettings.Service_Http_Method__c);
        String timeOutvalue = Label.ATS_SEARCH_TIMEOUT;
        req.setTimeout(Integer.valueOf(timeOutvalue));
        req.setHeader('Authorization', 'Bearer ' + connSettings.OAuth_Token__c);
        if (httpRequestType == 'POST' ) {
            req.setBody(requestBody);
            req.setHeader('Content-Type','application/json') ;
        }
        String responseBody;
        try {
            HttpResponse response = connection.send(req);
            responseBody = response.getBody();
            System.debug('responseBody >>>>>>>> ' + responseBody);
        } catch (CalloutException ex) {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Match Service failed! ' + ex.getMessage());
            // responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', ex.getMessage());
        } catch (Exception ex) {
            // Callout to apigee failed
            System.debug(LoggingLevel.ERROR, 'Apigee call to Match Service failed! ' + ex.getMessage());
            throw ex;
        }

        if (String.isBlank(responseBody)) {
            throw new SearchController.InvocableApiException('Apigee match service call returned empty response body!');
        }
        return responseBody;
    }
    private static String addTrackingIds(String q) {
        if (!q.endsWith('&')) {
            q += '&';
        }
        return q + 'userId=' + UserInfo.getUserId() + '&sessionId=' + UserInfo.getSessionId();
    }

    private static String GetAssignmentStartDays(TalentBaseModel tbm){
        try {
            if (tbm.StartDate > Date.today()) {

                Integer daysLeft = Date.today().daysBetween(tbm.StartDate);
                if (daysLeft > 0)
                    return String.valueOf(daysLeft);
                else
                    return 'NoFutureAssignment';
            }else{
                return 'NoFutureAssignment';
            }

        }
        catch (Exception ex) {
            System.debug(ex.getMessage());
            return 'NoFutureAssignment';
        }
    }

    private static Boolean isAssignmentinFuture(TalentBaseModel tbm){
        try {
            if (tbm.DaysRemainingOnAssignmentStart == 'NoFutureAssignment') {
                return false;
            }else if(!String.isBlank(tbm.DaysRemainingOnAssignmentStart)){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception ex) {
            System.debug(ex.getMessage());
            return false;
        }
    }

    @AuraEnabled
    public static User GetUserInfo(string TalentAccountId, string UserField) {
        try {
            string soql = 'SELECT Id, IsActive, FirstName, LastName, Title, Phone, FullPhotoUrl, MediumPhotoUrl, Email FROM User WHERE Id in (SELECT ' + UserField + ' FROM Account WHERE Id = \'' + TalentAccountId + '\')';

            if (UserField.startsWith('005')) {
                soql = 'SELECT Id, IsActive, FirstName, LastName, Title, Phone, FullPhotoUrl, MediumPhotoUrl, Email FROM User WHERE Id = \'' + UserField + '\'';
            }

            System.debug('GetUserInfo SOQL: ' + soql);
            User u = Database.query(soql);

            if (u != null) {
                System.debug('GetUserInfo Found User');
                return u;
            }
            else {
                System.debug('GetUserInfo List Empty');
                return null;
            }
        }
        catch (Exception ex) {
            System.debug('GetUserInfo Exception: ' + ex.getMessage());
            return null;
        }
    }

    public static List<Talent_Saved_Job__c> GetSavedJobs(string TalentAccountId) {
        return [SELECT Id, ReqIDs__c, Job_Search_Source__c, Type_Of_Intrest__c, Saved_Flag__c, Distance_Criteria__c, Location_Criteria__c, Search_Criteria__c
                FROM Talent_Saved_Job__c
                WHERE Saved_Flag__c = true
				//Type_Of_Intrest__c = 'Saved'
                //AND Is_Active__c = true
                AND Talent__c = :TalentAccountId];
    }

    @AuraEnabled
    public static SObject[] GetDocuments(string AccountId) {
        SObject[] docs = [SELECT CreatedDate, Id, CreatedBy.Name ,Document_Name__c, Azure_URL__c, Document_Type__c, Default_Document__c, (Select Id from Attachments) FROM Talent_Document__c WHERE mark_for_deletion__c = false and Document_Type__c =:'Communities' and committed_document__c = true and Talent__c = :AccountId ORDER BY LastModifiedDate DESC];

        if (docs.size() > 0)
            return docs;
        else
            return null;
    }

    @AuraEnabled
    public static integer GetLoginHistoryCount(string UserId) {
        integer loginCount = database.countQuery('SELECT count() FROM LoginHistory WHERE UserId = :UserId');
        return loginCount;
    }

    @AuraEnabled
    public static String GetLinkedInStep1() {
        TC_LinkedInControllerLightning linkedin = new TC_LinkedInControllerLightning();
        return linkedin.connect();
    }

    @AuraEnabled
    public static String GetLinkedInStep2(string code) {
        TC_LinkedInControllerLightning linkedin = new TC_LinkedInControllerLightning();
        return linkedin.connect_step_2(code);
    }

    private static boolean hasPersonalCases(){
        try{
            system.debug('came into hasPersonalCases');
            Case c = [SELECT Id, CaseNumber From Case where CreatedById = :UserInfo.getUserId() LIMIT 1];
            return true;
        }catch(Exception e){
            system.debug('Exception : '+e.getMessage());
            return false;
        }

    }

}