@isTest
public class TalentBaseController_Test  {
	
	static testMethod void tbcTest() {
		
		//User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		//System.runAs (thisUser) {
		//Test.startTest();
			Set<String> customerUserTypes = new Set<String> {'CspLitePortal'};
			Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];

			User thisUser = [SELECT Id,Peoplesoft_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
			long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
            Integer random = Math.Round(Math.Random() * timeDiff );

			Account talent = CreateTalentTestData.createTalent();
			//talent.Peoplesoft_ID__c = '36249190';
			talent.Peoplesoft_ID__c = (string.valueOf(random * 143)).substring(0,7);
			talent.Talent_Preference__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
			talent.Assign_End_Notice_Close_Date__c = System.today();
			update talent;

			Contact con =[select id from contact where accountid =: talent.id Limit 1];
			con.End_Date_Change_Requested__c = 'change requested';
			con.Recruiter_Change_Requested__c = 'yes';
			update con;

			
			UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];			
			User user1 ;
			//User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
			System.runAs (thisUser) {
			user1= new User(
			Username = 'testusr'+random+'@allegisgroup.com.dev',
			ContactId = con.Id,
			ProfileId = p.Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = ' User'+random,
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US',
			OPCO__c = 'AP',
			Region__c= 'Aerotek MidAtlantic',
			Region_Code__c = 'test',
            Office_Code__c = '10001',
            //Peoplesoft_Id__c = string.valueOf(random * 143),
			Peoplesoft_Id__c = talent.Peoplesoft_ID__c,
            User_Application__c = 'CRM',
			Talent_Preference__c = 'test preferences'			
			);
			insert user1;
			
			}
			Test.startTest();
			System.runAs(user1) {			

				Talent_Work_History__c talentWorkHistory = CreateTalentTestData.createWorkHistory(talent);
				System.debug('talentWorkHistory----'+talentWorkHistory);

				System.debug('Account talent_end_date----'+talent.Talent_End_Date__c);
				TalentBaseController tbc = new TalentBaseController();
				
				TalentBaseController.GetAllTalentData();
				TalentBaseController.getUser();
				TalentBaseController.getAccount();
				TalentBaseController.GetUserInfo(UserInfo.getUserId(), '');
				TalentBaseController.GetDocuments(talent.id) ;
				
				String talentJason = '{"UserId" :"'+ user1.id+'", "ContactId":"'+ con.ID+'", "AccountId":"'+talent.id+'", "FirstName":"test", "LastName":"testlast" }';

				TalentBaseController.SaveTalent(talentJason, true, true, true);
				TalentBaseController.SaveTalent(talentJason);
			}

			Test.stopTest();
			//}
		
	} 
	static testMethod void tbcTest2() {
		
			Set<String> customerUserTypes = new Set<String> {'CspLitePortal'};
			Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
			User thisUser = [SELECT Id,Peoplesoft_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
			long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
            Integer random = Math.Round(Math.Random() * timeDiff );
			Account talent = CreateTalentTestData.createTalent();
			talent.Peoplesoft_ID__c = (string.valueOf(random * 143)).substring(0,7);
			talent.Talent_Preference__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
			talent.Assign_End_Notice_Close_Date__c = System.today();
			update talent;

			Contact con =[select id from contact where accountid =: talent.id Limit 1];
			con.End_Date_Change_Requested__c = 'change requested';
			con.Recruiter_Change_Requested__c = 'yes';
			update con;
			
			UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];			
			User user1 ;

			System.runAs (thisUser) {
			user1= new User(
			Username = 'testusr'+random+'@allegisgroup.com.dev',
			ContactId = con.Id,
			ProfileId = p.Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = ' User'+random,
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US',
			OPCO__c = 'AP',
			Region__c= 'Aerotek MidAtlantic',
			Region_Code__c = 'test',
            Office_Code__c = '10001',
            Peoplesoft_Id__c = talent.Peoplesoft_ID__c,
            User_Application__c = 'CRM',
			Talent_Preference__c = 'test preferences'
			);
			insert user1;			
			}			
			Test.startTest();
			System.runAs(user1) {
				TalentBaseModel tbm1 =(TalentBaseModel)TalentBaseController.GetAllTalentById(user1.id);
				TalentBaseModel tbm2 =(TalentBaseModel)TalentBaseController.GetAllTalentDataById(user1.id);
				TalentBaseController.saveRecord('TestTitle');
				TalentBaseController.insertTalentReferral('testFirstName', 'testLastName', 'testTitle', '9797979797', 'TestNote', 'Java', 'test@mail.com', talent.Id);
				Integer i= TalentBaseController.GetLoginHistoryCount(user1.id);
				String str= TalentBaseController.GetLinkedInStep1();
				String str1 = TalentBaseController.GetLinkedInStep2('');				
			}
			Test.stopTest();	
	} 
	static testMethod void tbcTest3() {
		long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );

		Account talent = CreateTalentTestData.createTalent();
		talent.Peoplesoft_ID__c = (string.valueOf(random * 143)).substring(0,7);
		talent.Talent_Preference__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
		talent.Assign_End_Notice_Close_Date__c = System.today();
		talent.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
		update talent;
						
		Reqs__c newreq = new Reqs__c (Account__c =  talent.Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10,Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 ); 
        insert newreq;			
			
		Test.startTest();
		String requestBody ='{"query":{"term": {"titlevector": "qa tester"}},"aggs":{"skill_counts":{"terms":{"script":{"inline": "doc[\'skillsvector\'].values"},"size": 100}}}}';
		TalentBaseController.createNewTalentReferral('test@mail.com', 'TestFirstName', 'TestLastName', true, newreq.Id, talent.Id);
		TalentBaseController.createNewTalentReferral('test@mail.com', 'TestFirstName', 'TestLastName', true, newreq.Id, talent.Id);
		//String str =  TalentBaseController.Skills('GET',requestBody);	

		Test.stopTest();	
	} 
}