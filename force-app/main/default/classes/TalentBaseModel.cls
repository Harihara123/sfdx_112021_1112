global class TalentBaseModel {
    @AuraEnabled
    public String SiteBaseUrl {get;set;}

    @AuraEnabled
    public String ProfileUrl {get;set;}

    @AuraEnabled
    public String SettingsUrl {get;set;}

    @AuraEnabled
    public String ContactUrl {get;set;}

    @AuraEnabled
    public String JobsearchUrl {get;set;}

    @AuraEnabled
    public String automatchUrl {get;set;}

    @AuraEnabled
    public String UserId {get;set;}

    @AuraEnabled
    public String ContactId {get;set;}

    @AuraEnabled
    public String AccountId {get;set;}

    @AuraEnabled
    public String ProfileId {get;set;}

    @AuraEnabled
    public String ProfileName {get;set;}

    @AuraEnabled
    public String BillerId {get;set;}

    @AuraEnabled
    public String RecruiterId {get;set;}

    @AuraEnabled
    public String TalentProgramManagerId {get;set;}
	
	@AuraEnabled
    public String TalentDeliveryManagerId {get;set;}
	
	@AuraEnabled
    public String CommunityRepId {get;set;}
    
    @AuraEnabled
    public String CandidateStatus {get;set;}

	@AuraEnabled
    public String talentCommunityStatus {get;set;}
	
	@AuraEnabled
    public String PTOhours {get;set;}

    @AuraEnabled
    public String AccountManagerId {get;set;}

    @AuraEnabled
    public String CommunityManagerId {get;set;}

    @AuraEnabled
    public String CSAId {get;set;}

    @AuraEnabled
    public String FullName {get;set;}

    @AuraEnabled
    public String FirstName {get;set;}

    @AuraEnabled
    public String LastName {get;set;}

    @AuraEnabled
    public String Suffix {get;set;}

    @AuraEnabled
    public String MailingCity {get;set;}

    @AuraEnabled
    public String TC_Mailing_Address {get;set;}

    @AuraEnabled
    public String MailingState {get;set;}

    @AuraEnabled
    public String OperatingCompany {get;set;}

    @AuraEnabled
    public String Title {get;set;}

    @AuraEnabled
    public Date StartDate {get;set;}

    @AuraEnabled
    public String EndDateChangeRequested {get;set;}

    @AuraEnabled
    public Date AssignEndNoticeCloseDate {get;set;}

    @AuraEnabled
    public String RecruiterRequested {get;set;}

    @AuraEnabled
    public Date EndDate {
        get;
        set{
            EndDate = value;

            Date startDate = value;
            Date dueDate = Date.today();
            Integer numberDaysDue = dueDate.daysBetween(startDate);
            if (numberDaysDue > 0){
                //IsTalentCurrent = true;
                DaysRemainingOnAssignment = numberDaysDue;
                DaysSinceAssignment = 0;
            }
            else {
                //IsTalentCurrent = false;
                numberDaysDue = 0;
                DaysRemainingOnAssignment = 0;
                DaysSinceAssignment = startDate.daysBetween(dueDate);
            }
        }
    }

    @AuraEnabled
    public Boolean StatDateInFuture {get;set;}

    @AuraEnabled
    public String DaysRemainingOnAssignmentStart {get;set;}

    /*@AuraEnabled
    public String EndDateFormatted {
        get{
            Date thisEndDate = EndDate;
            DateTime newEndDate = DateTime.newInstance(thisEndDate.year(), thisEndDate.month(), thisEndDate.day());
            String newendDateFormatted = newEndDate.format('MMMM d, yyyy');
            return newendDateFormatted;
        }
        set;
    }*/

    //@AuraEnabled
    //public Boolean IsTalentCurrent {get;set;}

    @AuraEnabled
    public Integer AssignmentProgressPercent{get;set;}

    @AuraEnabled
    public Integer DaysRemainingOnAssignment{get;set;}

    @AuraEnabled
    public Integer DaysSinceAssignment{get;set;}

    @AuraEnabled
    public String PeoplesoftId {get;set;}

    @AuraEnabled
    public String FullPhotoUrl {get;set;}

    @AuraEnabled
    public String MediumPhotoUrl {get;set;}

    @AuraEnabled
    public String PreferencesJson {get;set;}

    @AuraEnabled
    public User Recruiter {get;set;}

    @AuraEnabled
    public User AccountManager {get;set;}

    @AuraEnabled
    public String Usernamedup {get;set;}

    @AuraEnabled
    public User CommunityManager {get;set;}

    @AuraEnabled
    public User CSA {get;set;}

    //@AuraEnabled
    //public Map<string, Talent_Document__c> Documents {get;set;}

    @AuraEnabled
    public SObject[] Documents {get;set;}

    @AuraEnabled
    public TC_Opco_Mappings__c OpCoMappings {get;set;}

    @AuraEnabled
    public List<TC_UsefulLinks> UsefulLinks {get;set;}

    @AuraEnabled
    public String FAQCategories {get;set;}

    @AuraEnabled
    public String communityName {get;set;}

    @AuraEnabled
    public String SurveyStatus {get;set;}

    @AuraEnabled
    public Integer SurveyConditionLimit {get;set;}

    @AuraEnabled
    public String SurveyLink {get;set;}

    @AuraEnabled
    public String userLang{get;set;}

    @AuraEnabled
    public List<Talent_Saved_Job__c> SavedJobs {get;set;}

    @AuraEnabled
    public List<TC_KeyContactModel> KeyContacts {get;set;}

	@AuraEnabled
    public String VendorName{get;set;} // Added by Venkat

	@AuraEnabled
    public boolean hasPersonalCases{get;set;} // Added by Venkat

	@AuraEnabled
    public String CBProfileId {get;set;}

	@AuraEnabled
    public String OfficeCode {get;set;}
}