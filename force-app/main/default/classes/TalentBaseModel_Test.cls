@isTest 
public class TalentBaseModel_Test {
    public static testmethod void TestTalentBaseModel(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'abc', Email='abc@allegisgroup.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abc@allegisgroup.com');
		insert u;
        System.runAs(u){
        TalentBaseModel talent = new TalentBaseModel();
        talent.SiteBaseUrl='';
        talent.ProfileUrl='';
        talent.ContactUrl='';
        talent.JobsearchUrl='';
        talent.automatchUrl='';
        talent.UserId='';
        talent.ContactId='';
        talent.AccountId='';
        talent.ProfileName='';
        talent.BillerId='';
        talent.RecruiterId='';
        talent.AccountManagerId='';
        talent.CommunityManagerId='';
        talent.CSAId='';
        talent.FullName='';
        talent.FirstName='';
        talent.LastName='';
        talent.Suffix='';
        talent.MailingCity='';
        talent.MailingState='';
        talent.OperatingCompany='';
        talent.Title='';
        talent.StartDate=system.today();
        talent.EndDateChangeRequested='';
        talent.AssignEndNoticeCloseDate=system.today()-1;
        talent.RecruiterRequested='';
        talent.EndDate=system.today()+5;
        talent.StatDateInFuture=true;
        talent.DaysRemainingOnAssignmentStart='';
        talent.AssignmentProgressPercent=60;
        talent.DaysRemainingOnAssignment=15;
         talent.DaysSinceAssignment=60;
         talent.PeoplesoftId='';
         talent.FullPhotoUrl='';
         talent.MediumPhotoUrl='';
         talent.PreferencesJson='';
         talent.Recruiter=u;
         talent.AccountManager=u;
         talent.Usernamedup='';
         talent.CommunityManager=u;
         talent.CSA=u;
         talent.communityName='';
         talent.SurveyStatus='';
         talent.SurveyConditionLimit=50;
         talent.SurveyLink='';
         talent.userLang='';
         talent.VendorName='';
         talent.hasPersonalCases=true;
        }
    }

}