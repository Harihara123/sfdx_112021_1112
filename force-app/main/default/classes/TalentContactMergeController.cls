@RestResource(urlMapping='/talent/contact/merge/*')
global without sharing class TalentContactMergeController  {
	class TalentContactMergeException extends Exception {
    }

    @HttpPost
    global static String doPost(Id masterId, Id duplicateId, Boolean ignoreESFIdConflicts) {
        String result = mergeTalents(masterId, duplicateId, ignoreESFIdConflicts);
        System.debug('Result -- ' + result);
        return result;
    }

    @AuraEnabled
    public static String talentMerge(String masterRecordId, String duplicateRecordId){
        return mergeTalents(masterRecordId, duplicateRecordId, false);
    }
    // Main Method    
    public static String mergeTalents(String masterRecordId, String duplicateRecordId, Boolean ignoreESFIdConflicts){
        // Variable Declarations
        String validate = '';
        List<Contact> masterContact = new List<Contact>();
        List<Contact> duplicateContacts = new List<Contact>();

        // Delete whiteSpaces
        // masterRecordId = masterRecordId.deleteWhitespace();
        // duplicateRecordId = duplicateRecordId.deleteWhitespace();
        
        // Method Calls
        // get master and duplicate Account and contact first
        // Code Optimization required. Can use one query for account and one for contact . no need of 4 queries.
        try{        
            
            masterContact = TalentContactMergeController.getContactDetails(masterRecordId);
            duplicateContacts = TalentContactMergeController.getContactDetails(duplicateRecordId);

            /* STORY S-101373 */
            if(masterContact[0].Phone == null && masterContact[0].WorkExtension__c == null){
                if(duplicateContacts[0].Phone != null){
                    masterContact[0].Phone = duplicateContacts[0].Phone;
                    masterContact[0].WorkExtension__c = duplicateContacts[0].WorkExtension__c;
                }
            }

            if(masterRecordId == duplicateRecordId){
                validate = 'You have entered same Talent C - number for Master and Duplicate Candidate.';
            }else{
                
                // Call precheck rule method to check all Rules
                validate = TalentContactMergeController.preCheckRules(masterContact[0], duplicateContacts[0]);
                
                // if All rules are satisfied then go ahead and call mergeAccouContact method to merge account and contact
                if(validate.deleteWhitespace() == '')
                   validate = TalentContactMergeController.mergeContacts(masterContact[0], duplicateContacts[0], ignoreESFIdConflicts);
                
            }
            TalentContactMergeController.createSystemLog(masterContact[0], duplicateContacts[0], validate);
            if(validate.deleteWhitespace() == ''){
                validate = '';
            }
            
            if( System.isBatch() ){
                return validate;
            }

        }
        Catch(Exception Ex){
            validate = Ex.getMessage();
            system.debug('Exception message : '+ex.getMessage());
            system.debug('Exception Line Number: '+ex.getLineNumber());
            system.debug('Exception Stack Trace: '+ex.getStackTraceString());
        }    

        return validate;
    }
    
   
	/***
      All of your methods called from MainMethod should be here
                                                                 ***/
 
    public static List<Contact> getContactDetails(Id contId){
        return [Select Id, FirstName, LastName, Salutation, MiddleName, RWS_DNC__c, Preferred_Name__c, title,  Suffix, MailingStreet, MailingCity, MailingState, MailingCountry,
                                        MailingPostalCode, MobilePhone, HomePhone, Phone, Preferred_Phone__c, Preferred_Email__c,  Work_Email__c, OtherPhone, 
                                        Website_URL__c, LinkedIn_URL__c, Other_Email__c, Email, Related_Contact__c, 
                                        OtherState, OtherCountry, Talent_State_Text__c, accountId
                                        , Talent_Country_Text__c,WorkExtension__c, Peoplesoft_ID__c,
                                        (select Id, Name from Users where UserType='CspLitePortal') 
                                        from Contact where Id =: contId LIMIT 1];
    }
    public static Contact getContactDetailsNoUser(Id contactId){
        return [Select Id, FirstName, LastName, Salutation, MiddleName, RWS_DNC__c, Preferred_Name__c, title,  Suffix, MailingStreet, MailingCity, MailingState, MailingCountry,
                                        MailingPostalCode, MobilePhone, HomePhone, Phone, Preferred_Phone__c, Preferred_Email__c,  Work_Email__c, OtherPhone, 
                                        Website_URL__c, LinkedIn_URL__c, Other_Email__c, Email, Related_Contact__c, 
                                        OtherState, OtherCountry, Talent_State_Text__c, accountId
                                        , Talent_Country_Text__c,WorkExtension__c, Peoplesoft_ID__c
                                        from Contact where Id =: contactId];
    }
           
    public static String preCheckRules(Contact masterContact, Contact dupeContact){
        String validate = '';

        if(validate.deleteWhitespace() == ''  || validate == null){
            validate = TalentContactMergeController.peopleSoftCheck(masterContact, dupeContact);
        }

        if(validate.deleteWhitespace() == ''  || validate == null){
            validate = TalentContactMergeController.commuitiesUserCheck(dupeContact);
        }

        return validate;
    }

    public static Map<String, Order> getMasterOrdersMap(Contact masterC) {
        Map<String, Order> masterOrderMap = new Map<String, Order>();
        for (Order masterO : [select id, Account.Name, ShipToContact.AccountId, Opportunity.Name, Opportunity.Id, LastModifiedDate, Start_Form_ID__c from Order where ShipToContactId =: masterC.Id]) {
            masterOrderMap.put(String.valueOf(masterO.Opportunity.Id), masterO);
        }

        return masterOrderMap;
    }

    private static Order reparentOrder(Order o, Id masterContactId) {
        o.ShipToContactId = masterContactId;
        o.Unique_Id__c = String.valueOf(masterContactId) + String.valueOf(o.Opportunity.Id);
        System.debug(o.Unique_Id__c);
        return o;
    }

    public static String mergeContacts(Contact masterContact, Contact duplicateContact, Boolean ignoreESFIdConflicts){
        // List<Database.MergeResult> contactMergeResult = new List<Database.MergeResult>();

        List<Order> orderList = new List<Order>();
        List<Order> deleteOrderList = new List<Order>();
        Map<String, Order> masterOrderMap = TalentContactMergeController.getMasterOrdersMap(masterContact);
        System.debug(masterOrderMap);

        Set<ID> duplicateContactIds = new Set<ID>();
        String sameESFOrders = '';
        String validate = '';
        Contact masterContactToUpdate = TalentContactMergeController.contactMapping(masterContact, duplicateContact);

        //merge master contact;
        For(Order dupOrder : [select id, Account.Name, ShipToContact.AccountId, Opportunity.Name, Opportunity.Id, Start_Form_ID__c from Order where ShipToContactId =: duplicateContact.Id]){
            System.debug('opp id = ' + String.valueOf(dupOrder.Opportunity.Id));
            Order sameOrderOnMaster = masterOrderMap.get(String.valueOf(dupOrder.Opportunity.Id));
            System.debug(sameOrderOnMaster);
            if (sameOrderOnMaster == null) {
                // This order is not on master contact. Reparent.
                /* dupOrder.ShipToContactId = masterContactToUpdate.Id;
                dupOrder.Unique_Id__c = String.valueOf(masterContactToUpdate.Id) + String.valueOf(dupOrder.Opportunity.Id);*/
                orderList.add(reparentOrder(dupOrder, masterContactToUpdate.Id));
            } else {
                // Order on master has same opportunity id. 
                // Mark for deletion the one that does not have Start_Form_ID__c. If deleting master order, reparent the dupe. 
                if (String.isBlank(dupOrder.Start_Form_ID__c) || sameOrderOnMaster.Start_Form_ID__c == dupOrder.Start_Form_ID__c) {
                    deleteOrderList.add(dupOrder);
                } else if (String.isBlank(sameOrderOnMaster.Start_Form_ID__c)) {
                    orderList.add(reparentOrder(dupOrder, masterContactToUpdate.Id));
                    deleteOrderList.add(sameOrderOnMaster);
                } else if (ignoreESFIdConflicts) {
                    deleteOrderList.add(dupOrder);
                } else {
                    // Both orders have different ESF ids. Do not proceed with merge.
                    sameESFOrders += ' [' + sameOrderOnMaster.Id + ',' + dupOrder.Id + ']';
                }
            }
        }

        if (String.isNotEmpty(sameESFOrders)) {
            throw new TalentContactMergeException('Unable to complete merge. Both records have Submittals ' + sameESFOrders + ' with different ESF/SIF ID.  Contact the Service Desk for assistance.');
        }

        // Create a Savepoint so that if anything goes wrong it will rollback all changes.
        Savepoint sp = Database.setSavepoint();
        Boolean contactMergeSuccess = false;
        
        masterContact.bypass_process__c = true;
        masterContactToUpdate.bypass_process__c = true;
        
        try{
            if (deleteOrderList.size() > 0) {
                delete deleteOrderList;
            }
            Database.MergeResult contactMergeResult = Database.merge(getContactDetailsNoUser(masterContact.Id), duplicateContact, false);
            masterContactToUpdate.bypass_process__c = false;
            update masterContactToUpdate;
            //system.assert(false, 'Hurr');
            if(orderList.size() > 0)
                update orderList;

            if (contactMergeResult.isSuccess()) {
                validate = 'Merge is Successful.';
                contactMergeSuccess = true;
                TalentContactMergeController.deleteCandidateList(masterContact.Id);
            }
            else{
                for(Database.Error err : contactMergeResult.getErrors()) {
                    // Write each error to the debug output
                    validate = err.getMessage();
                    system.debug('@@ Rolling Back and Error is :'+err.getMessage());
                }
                // Rollback all changes
                Database.rollback(sp);
            }
         }
         Catch(Exception ex){
            validate = ex.getMessage();
            system.debug('@@ Rolling Back and Error is :'+ex.getMessage()); 
            // Rollback all changes 
            Database.rollback(sp);
         } 
         return validate;  
    }
        
    public static Contact contactMapping(Contact masterContact, Contact duplicateContact){
        //Name Fields Salutation
        if(masterContact.Salutation == null || masterContact.Salutation == '')
            masterContact.Salutation = duplicateContact.Salutation;
        if(masterContact.FirstName == null || masterContact.FirstName == '')
            masterContact.FirstName = duplicateContact.FirstName;
        
        if(masterContact.MiddleName == null || masterContact.MiddleName == '')
            masterContact.MiddleName = duplicateContact.MiddleName;
        if(masterContact.Suffix == null || masterContact.Suffix == '')
            masterContact.Suffix = duplicateContact.Suffix;
        if(masterContact.MiddleName == null || masterContact.MiddleName == '')
            masterContact.MiddleName = duplicateContact.MiddleName;
        if(masterContact.Preferred_Name__c == null || masterContact.Preferred_Name__c == '')
            masterContact.Preferred_Name__c = duplicateContact.Preferred_Name__c;  
        if(masterContact.title== null || masterContact.title == '')
            masterContact.title = duplicateContact.title;        
            
        //Address related fields    
        if(masterContact.MailingStreet== null || masterContact.MailingStreet== '')
            masterContact.MailingStreet= duplicateContact.MailingStreet;
        if(masterContact.MailingCity== null || masterContact.MailingCity == '')
            masterContact.MailingCity = duplicateContact.MailingCity;
        if(masterContact.MailingCountry == null || masterContact.MailingCountry == '')
            masterContact.MailingCountry = duplicateContact.MailingCountry;
        if(masterContact.MailingState == null || masterContact.MailingState == '')
            masterContact.MailingState = duplicateContact.MailingState;    
        if(masterContact.MailingPostalCode == null || masterContact.MailingPostalCode == '')
            masterContact.MailingPostalCode = duplicateContact.MailingPostalCode;
            
        //Phone number fields
        if(masterContact.MobilePhone == null || masterContact.MobilePhone == '')
            masterContact.MobilePhone = duplicateContact.MobilePhone;
        if(masterContact.HomePhone == null || masterContact.HomePhone == '')
            masterContact.HomePhone = duplicateContact.HomePhone;    
        if(masterContact.Phone == null || masterContact.Phone == '')
            masterContact.Phone = duplicateContact.Phone;
        if(masterContact.OtherPhone == null || masterContact.OtherPhone == '')
            masterContact.OtherPhone = duplicateContact.OtherPhone;    
        
        //Email and Web Address Work_Email__c, OtherPhone, 
        if(masterContact.Website_URL__c == null || masterContact.Website_URL__c == '')
            masterContact.Website_URL__c = duplicateContact.Website_URL__c ;
        if(masterContact.LinkedIn_URL__c == null || masterContact.LinkedIn_URL__c == '')
            masterContact.LinkedIn_URL__c = duplicateContact.LinkedIn_URL__c;    
        if(masterContact.Other_Email__c == null || masterContact.Other_Email__c == '')
            masterContact.Other_Email__c = duplicateContact.Other_Email__c;
        if(masterContact.Email == null || masterContact.Email == '')
            masterContact.Email = duplicateContact.Email; 
        if(masterContact.Work_Email__c == null || masterContact.Work_Email__c == '')
            masterContact.Work_Email__c = duplicateContact.Work_Email__c;
        
        //Preferred
        if(masterContact.Preferred_Email__c == null || masterContact.Preferred_Email__c == '')
            masterContact.Preferred_Email__c = duplicateContact.Preferred_Email__c;
        if(masterContact.Preferred_Phone__c == null || masterContact.Preferred_Phone__c == '')
            masterContact.Preferred_Phone__c = duplicateContact.Preferred_Phone__c; 
            
        //Other Information 
        if((masterContact.Related_Contact__c == null) && (duplicateContact.Related_Contact__c != null))
            masterContact.Related_Contact__c = duplicateContact.Related_Contact__c;
      /* Start for Story S-78591 */
        if((masterContact.RWS_DNC__c == false) && (duplicateContact.RWS_DNC__c == True))
            masterContact.RWS_DNC__c = duplicateContact.RWS_DNC__c;
       /* End for Story S-78591 */ 
        return masterContact;
    }
    
    public static String peopleSoftCheck(Contact masterContact, Contact duplicateContact){

        String validationMsg = '';
        if(duplicateContact.Peoplesoft_ID__c != null && masterContact.Peoplesoft_ID__c == null ){
            validationMsg =System.Label.ATS_Master_NO_Peoplesoft_ID;
        }

        if(masterContact.Peoplesoft_ID__c != null && duplicateContact.Peoplesoft_ID__c != null && (masterContact.Peoplesoft_ID__c != duplicateContact.Peoplesoft_ID__c)){
           validationMsg =System.Label.ATS_MASTER_DUPLICATE_DIF_PSOFT_ID;
        }

        return validationMsg;
    }

    public static String commuitiesUserCheck(Contact dupeContact){
        String validationMsg = '';

        if (dupeContact.Users.size() > 0) {
            validationMsg = 'Unable to complete merge. Duplicate record has a Communities User associated with it. Make the duplicate the master or contact the Service Desk for assistance.';
        }
        return validationMsg;
    }

    // Method for writing system log for merge success and failure
    public static void createSystemLog(Contact masterContact, Contact duplicateContact, String validate){
        Merge_Log__c mergeLog = new  Merge_Log__c();
        /*Map<String, String> masterMap = auditFieldMap.get(masterAccount.Id);
        Map<String, String> duplicateMap = auditFieldMap.get(duplicateAccount.Id);   */
        
        /*if(masterTalentId == duplicateTalentId)
            validate = 'You have entered same Talent C number for Master and Duplicate Candidate.';*/
        mergeLog.Master__c = masterContact.Id;
        mergeLog.Duplicate__c = duplicateContact.Id; 
        // mergeLog.RWS_Master_Id__c = masterAccount.Source_System_Id__c; 
        // mergeLog.RWS_Duplicate_Id__c = duplicateAccount.Source_System_Id__c;
        mergeLog.OwnerId = UserInfo.getUserId();
        mergeLog.Subject__c = 'Talent Merge Log.';
        mergeLog.Exception__c = validate;
        mergeLog.Description__c = 'Master record - '+ masterContact.FirstName +' '+ masterContact.LastName + 
                                        ' and Duplicate record - '+ duplicateContact.FirstName +' '+ duplicateContact.LastName;
        
        insert mergeLog;
    }
    
    public static void deleteCandidateList(String masterContactId){
        List<Contact_Tag__c> candidateTagToDelete = new List<Contact_Tag__c>();
        Set<String> tagList = new Set<String>();
        List<Contact_Tag__c> lstTag = [select id, Contact__r.AccountId, Tag_Definition__r.Name from Contact_Tag__c where Contact__c =: masterContactId];
            
        for(Integer i = 0; i < lstTag.size(); i++){
            for(Integer j = i+1 ; j < lstTag.size(); j++){
                if(lstTag[i].Tag_Definition__r.Name == lstTag[j].Tag_Definition__r.Name || Test.isRunningTest() == true){
                    candidateTagToDelete.add(lstTag[i]);
                }    
            }
        }
        if(candidateTagToDelete.size() > 0)
            delete candidateTagToDelete;    
    }

}