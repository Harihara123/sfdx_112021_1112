@isTest
public class TalentContactMergeController_Test  {
	public static List<Account> talAccounts = new List<Account>();

	public static testMethod void doPostTest(){
    
        Test.startTest();
			TalentContactMergeController_Test.mergeTalentTest();
			
        	List<Contact> lstContact = [Select Id,AccountId, Talent_Id__c from Contact];
			Tag_Definition__c tagDef = new Tag_Definition__c(Tag_Type__c = 'Team', Tag_Name__c = 'Team');
			insert tagDef;
        	Contact_Tag__c tag = new Contact_Tag__c(Contact__c = lstContact[0].Id, Tag_Definition__c = tagDef.Id);
			Contact_Tag__c tag2 = new Contact_Tag__c(Contact__c = lstContact[0].Id, Tag_Definition__c = tagDef.Id);
			TalentContactMergeController_Test.createSubmittals(lstContact[0].AccountId);
			//TalentContactMergeController_Test.createSubmittals(lstContact[1].AccountId);
			//TalentContactMergeController_Test.createOrder(lstContact[0].AccountId, lstContact[0].Id);
			//TalentContactMergeController_Test.createOrder(lstContact[1].AccountId, lstContact[1].Id);
			
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{ "masterId":"'+lstContact[0].Id+'", "duplicateId":"'+lstContact[1].Id+'","ignoreESFIdConflicts":false }';
            
            req.requestURI = '/services/apexrest/talent/merge/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            TalentContactMergeController.doPost(lstContact[0].Id, lstContact[1].Id,false);
			TalentContactMergeController.deleteCandidateList(lstContact[0].Id);
    	
        Test.stopTest();    
        
        
    }
	public static testMethod void sameRecordEnteredTest(){
    Test.startTest();
			TalentContactMergeController_Test.mergeTalentTest();
			//TalentContactMergeController_Test.submittalPrecheckTest();
        	List<Contact> lstContact = [Select Id, Talent_Id__c from Contact];
			Tag_Definition__c tagDef = new Tag_Definition__c(Tag_Type__c = 'Team', Tag_Name__c = 'Team');
			insert tagDef;
        	Contact_Tag__c tag = new Contact_Tag__c(Contact__c = lstContact[0].Id, Tag_Definition__c = tagDef.Id);
			Contact_Tag__c tag2 = new Contact_Tag__c(Contact__c = lstContact[0].Id, Tag_Definition__c = tagDef.Id);
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{ "masterId":"'+lstContact[0].Id+'", "duplicateId":"'+lstContact[0].Id+'","ignoreESFIdConflicts":false }';
            
            req.requestURI = '/services/apexrest/talent/merge/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            TalentContactMergeController.doPost(lstContact[0].Id, lstContact[0].Id,false);
			TalentContactMergeController.deleteCandidateList(lstContact[0].Id);
    	
        Test.stopTest();    
        
        
    }
    
    public static testMethod void mergeSuccessTest(){
		Test.startTest();
    	TalentContactMergeController_Test.mergeTalentTest();
    	Test.stopTest();    
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
    	List<Account> lstAccount = [Select Id,Talent_Id__c from Account where RecordTypeId =: types[0].Id];
       
    	String validation = TalentContactMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
    	//system.assertEquals(validation, 'Merge is Successful.');
    }
   
    public static testMethod void mlaPrecheckTest(){
        
        Test.startTest();
            TalentContactMergeController_Test.mergeTalentTest();
        Test.stopTest();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Account acc = new Account(Name='Test', RecordTypeId = RecordTypeIdAccount);
        insert acc;
        Account account = lstAccount[0];
        account.Talent_Ownership__c = 'MLA';
        account.Current_Employer__c = acc.Id;
        update account;
        TalentContactMergeController_Test.createTargetAccount(lstAccount[0].Id);
        TalentContactMergeController_Test.createTargetAccount(lstAccount[1].Id);
        Contact contact = [Select Id from Contact where AccountId =: lstAccount[1].Id];
        lstAccount[0].Do_Not_Contact__c=True;
        update lstAccount[0];
        
        lstAccount[1].Do_Not_Contact__c=True;
        update lstAccount[1];
        String validation = TalentContactMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        }    
    
    public static testMethod void invalidIDTest(){
        
        String validation=''; 
            validation = TalentContactMergeController.talentMerge('C-xxxxxxxxx', 'C-xxxxxxxxx');
        
    }
    

    public static testMethod void nullAccountContactAddressTest(){
        
        String validation;
        Test.startTest();
            TalentContactMergeController_Test.mergeTalentTest();
            List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
            TalentContactMergeController_Test.createTargetAccount(lstAccount[0].Id);
            TalentContactMergeController_Test.createTargetAccount(lstAccount[1].Id);
            Contact contact = [Select Id, MailingStreet, MailingCity from Contact where AccountId =: lstAccount[0].Id];
            contact.MailingCity = '';
            contact.MailingStreet = '';
            contact.FirstName = '';
            update contact;  
        Test.stopTest();                    
       validation = TalentContactMergeController.talentMerge('hdksjhdjksha', 'nhkdhadhakls');
      validation = TalentContactMergeController.talentMerge('hdksjhdjksha', 'hdksjhdjksha');
    }
  
    
    
//Test Data Methods
    public static void mergeTalentTest(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        String u = 'chs'+String.valueOf(Datetime.now().formatGMT('HHmmssSSS'))+'@allegisgroup.com';
        User testUser = new User(firstname = 'Future', lastname = 'User',
            alias = 'future', defaultgroupnotificationfrequency = 'N',
            digestfrequency = 'N', email =  u,
            emailencodingkey = 'UTF-8', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey = 'America/Los_Angeles',
            username = u,
            userpermissionsmarketinguser = false,
            userpermissionsofflineuser = false);
     System.runAs(testUser) {
        
        Account masterAccount = TalentContactMergeController_Test.createTalentAccount('master');
        Contact masterContact = TalentContactMergeController_Test.createTalentContact2(masterAccount.Id);
        Account duplicateAccount = TalentContactMergeController_Test.createTalentAccount('duplicate');
        Contact duplicateContact = TalentContactMergeController_Test.createTalentContact(duplicateAccount.Id);
		
      }
    }
    
    public static void mergeTalentTest2(){
      
        Account masterAccount = TalentContactMergeController_Test.createTalentAccount2('master');
        Contact masterContact = TalentContactMergeController_Test.createTalentContact2(masterAccount.Id);
        Account duplicateAccount = TalentContactMergeController_Test.createTalentAccount2('duplicate1');
        Contact duplicateContact = TalentContactMergeController_Test.createTalentContact(duplicateAccount.Id);
		TalentContactMergeController_Test.createOrder(masterAccount.Id, masterContact.Id);
		TalentContactMergeController_Test.createOrder(duplicateAccount.Id, duplicateContact.Id);
       
    }
    
    public static Account createTalentAccount(String clientAccountID){
        
        
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=true, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
	public static Account createTalentAccount2(String clientAccountID){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test1 Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Account createClientAccount(){
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        
        return newClientAcc;
    }

    public static Contact createTalentContact(String accountID){
        
        List<Account> lstAccount = [Select Id,recordTypeId, Talent_Id__c from Account where id=:accountID];
        Contact newC = null;
        if(lstAccount.size()>0){
            Account acc = lstAccount[0];
             List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
             Boolean isTalent = false;
             if(types[0].id == acc.recordTypeId){
                 isTalent = true;
             }
            if(isTalent){
                List<RecordType> contRectypes = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
                newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,recordTypeId = contRectypes[0].id
                  ,Email='tast@talent.com',
				  Phone='9090909090',
				  RWS_DNC__c=True);
            }
            else{
                newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',
				  Phone='9090909090',
				  RWS_DNC__c=True);
            }
	        Database.insert(newC);            
        }        
        return newC;
        /*
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        return newC;
        */
    }
    public static Contact createTalentContact2(String accountID){
        List<Account> lstAccount = [Select Id,recordTypeId, Talent_Id__c from Account where id=:accountID];
        Contact newC = null;
        if(lstAccount.size()>0){
            Account acc = lstAccount[0];
             List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
             Boolean isTalent = false;
             if(types[0].id == acc.recordTypeId){
                 isTalent = true;
             }
            if(isTalent){
                List<RecordType> contRectypes = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
                newC = new Contact(Firstname='', LastName='Talent2'
                  ,AccountId=accountID, Website_URL__c=''
                  ,MailingStreet = ''
                  ,MailingCity=''
                  ,recordTypeId = contRectypes[0].id
                  ,Email='test@gmail.com');
            }
            else{
                newC = new Contact(Firstname='', LastName='Talent2'
                  ,AccountId=accountID, Website_URL__c=''
                  ,MailingStreet = ''
                  ,MailingCity=''
                  ,Email='');
            }
	        Database.insert(newC);            
        }        
        return newC;
    }

    public static void createSubmittals(String accountId){
        List<Opportunity> opps = new List<Opportunity>();
        
        for (Integer k=0; k<2 ;k++) {
                opps.add(new Opportunity(Name= 'Test' + ' Opportunity ' + k,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=accountId));
        }
        
        // Insert all opportunities for all accounts.
        insert opps;
    }
    
    public static void createOrder(String accountId, String contactId){
        Opportunity op = [SELECT Id FROM Opportunity limit 1];
        Order o = new Order();
            o.ShipToContactId = contactId;
            o.AccountId = accountId;
			o.OpportunityId = op.Id;
            o.EffectiveDate = date.today();
            o.Status = 'Linked';
        insert o;
    }
    
    
    public static void createTargetAccount(String AccountId){
        
        String userId = '';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
        User futureUser = new User(firstname = 'Future', lastname = 'User',
            alias = 'future', defaultgroupnotificationfrequency = 'N',
            digestfrequency = 'N', email = AccountId + '@apogeesolutions.com',
            emailencodingkey = 'UTF-8', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey = 'America/Los_Angeles',
            username = AccountId + '@apogeesolutions.com',
            userpermissionsmarketinguser = false,
            userpermissionsofflineuser = false);
            insert(futureUser);
            userId = futureuser.Id;
        }
        
        Target_Account__c ac = new Target_Account__c();

        ac.Account__c = AccountId;
        ac.User__c = userId;
        ac.Expiration_Date__c = date.today().addDays(4);
        
        insert ac;
    }
}