@RestResource(urlMapping = '/Talent/TalentInfo/V1/*')
/*
Sample Request for the wrapper class TalentCreationService:

{
"sourceTracking":{
"source":"SourceBreaker",
"vendor":"CVLibrary",
"transactionId":"AB1234Z",
"externalCandidateId":"SBx213"
},
"recruiter":{
"firstName":"Praveen",
"lastName":"Chinnappa",
"email":"pchinnappa@testmail.com"
},
"talent":{
"profile":{
"firstName":"TestFirstName",
"lastName":"TestLastName",
"jobTitle":"JavaDeveloper",
"desiredSalary":"$100000",
"desiredRate":"$50",
"desiredFrequency":"Daily",
"workEligibility":"I have the right to work for any employer",
"geographicPreference":"Within France",
"highestEducation":"Bachelor's Degree",
"securityClearance":"Confidential Or R"
},
"contact":{
"phone":{
"mobile":"+43123421 54",
"work":"112 4324 6543",
"home":"",
"unspecified":[
"1223212233",
"152431233"
]
},
"email":{
"primary":"",
"work":"",
"other":"",
"unspecified":[
"testa@testmail.com",
"teat123@teatmail.com"
]
}
},
"address":{
"streetAddress":"",
"city":"",
"stateProvince":"",
"country":"GB",
"postalCode":"CB123"
},
"resume":{
"fileType":"pdf",
"base64":"aswsdf"
},
"attachment":{
"fileType":"doc",
"base64":"awesdf"
}
}
}

Sample Response:

{
"message": "Talent creation/update successful",
"talentAccountId": "001xxxxx",
"talentContactId": "003xxxx",
“talentUrl”:” https://allegisgroup.lightning.force.com/lightning/r/Contact/0031E00002TprIrQAJ/view”
}



*/
global without sharing class  TalentCreationService {
    public static final String talent_upserted ='Talent Upserted';                
	public static final String talent_exception_blank ='Talent Upsert Failure - Resume Blank';
	public static final String talent_exception ='Talent Upsert Failure - Exception';
	public static final String exceptionStr ='Exception';
	public static final String resumeUpload ='Resume Uploaded';
	@httpPost
    global static void createTalent(){
		String contentTypeStr ='';
        TalentCreationRespWrapper respWrapper = new TalentCreationRespWrapper();
        List<External_Talent_Import_Log__c> tLogList = new List<External_Talent_Import_Log__c>();
		External_Talent_Import_Log__c tlog = new External_Talent_Import_Log__c();
		TalentWrapper wrapObject = new TalentWrapper();
        Restrequest request = RestContext.request;
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        
        try{
            Blob body = request.requestBody;
            String requestJSONBody = body.toString();
			
            wrapObject  = TalentWrapper.parse(requestJSONBody);
			if(wrapObject.talent.resume.fileType != '' &&  wrapObject.talent.resume.fileType != Null){
				contentTypeStr = getMimeType(wrapObject.talent.resume.fileType);
			}
            if(wrapObject.talent.resume.base64 != Null && wrapObject.talent.resume.base64 !='' && contentTypeStr !=''){
                Map<String,String> talentUpsertResult = new Map<String,String>();
                talentUpsertResult = Candidate.doPost(prepareCandidateWrapperObject(wrapObject));                
                
                if(talentUpsertResult.get('Status') == 'Success'){
                  	tlog.Status__c = talent_upserted;
                   	tlog.Talent_Upsert_AccountId__c = talentUpsertResult.get('candidateAccId');
                    tlog.Talent_Upsert_ContactId__c = talentUpsertResult.get('candidateId');
                    respWrapper.message = 'Talent creation/update successful';
                    respWrapper.talentAccountId = talentUpsertResult.get('candidateAccId');
                    respWrapper.talentContactId = talentUpsertResult.get('candidateId');
                    String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
                    respWrapper.talentUrl = baseUrl+'/lightning/r/Contact/'+respWrapper.talentContactId+'/view';

                    // if(wrapObject.talent.address.city != null || wrapObject.talent.address.country != null){
                    //     String contactId = talentUpsertResult.get('candidateId');
                    //     System.enqueueJob(new AddressDoctorLatLong(contactId));
                    // }
                     String conid=talentUpsertResult.get('candidateId');
                     if(wrapObject.sourceTracking.source=='PhenomCRM')
                   {
                       System.enqueueJob(new TalentLeadActivityQueue(wrapObject,conid)); 
                   }

                }else if(talentUpsertResult.get('Status') == 'Fail'){
                    tlog.Status__c =talent_exception;
                    tlog.Exception_Log__c +='Talent Upsert Exception::'+System.now()+':\n';
                    tlog.Exception_Log__c +=talentUpsertResult.get('description');
                    
                    respWrapper.message = 'Talent creation/update failed -'+talentUpsertResult.get('description');

                }   
            }else{
                // when resume is blank
                tlog.Status__c =talent_exception_blank;
                respWrapper.message = 'Talent creation/update failed - Missing CV / Invalid file type';
            }
            if(tlog.Status__c ==talent_upserted){
				deleteExistingTalentDocument(tlog.Talent_Upsert_AccountId__c);
				string fileName=wrapObject.talent.profile.firstName+'_'+wrapObject.talent.profile.lastName+'.'+wrapObject.talent.resume.fileType;
				Attachment att = new Attachment();
				att.ParentId = CreateTalentDocument(tlog.Talent_Upsert_AccountId__c,fileName);
				att.Name =fileName;
				att.Body= EncodingUtil.base64Decode(wrapObject.talent.resume.base64);
				att.ContentType= contentTypeStr;
				insert att;
				tlog.Status__c =resumeUpload;		
			}    
            response.statusCode = 200;
			
        }
        catch(Exception e){
            response.statusCode = 400;
            tlog.Status__c =exceptionStr;
            tlog.Exception_Log__c ='Exception::'+System.now()+':\n';
            tlog.Exception_Log__c +=e.getStackTraceString() +'\n';
			
            respWrapper.message = 'Talent creation/update failed -'+e.getMessage();
            ConnectedLog.LogException('SourceBreaker Talent Creation Service','TalentCreationService','createTalent', e);
        }
        finally{
            response.responseBody = Blob.valueOf(JSON.serialize(respWrapper));

			wrapObject.talent.resume.base64 = '';
			tlog.JSON_Payload__c=JSON.serialize(wrapObject);// storing the json without base64
			
			tLogList.add(tlog);
			SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, tLogList, false);
			Insert securityDecision.getRecords();
            //insert tlog;
        }
        
         
        
    }

	 //This method checks for latest talent document
    //w.r.t duplicate contacts and delete
    public static void deleteExistingTalentDocument(String contAccntId){        
        list<Talent_Document__c> talentDocument=[select id, Mark_For_Deletion__c from Talent_Document__c where Document_Type__c='Resume - Job Board' and Talent__c=:contAccntId with SECURITY_ENFORCED  ORDER BY CreatedDate DESC ];
        //need to discuss if more than one record of this type found , all will be deleted or only the latest one  
        if(talentDocument.size()>0){
                 delete talentDocument;
          }        
    }

	public static String CreateTalentDocument(String AccntId, String fileName){
        Talent_Document__c talentDocumentObject = new Talent_Document__c();
        talentDocumentObject.Talent__c = AccntId; 
        talentDocumentObject.Document_Name__c = fileName;
        talentDocumentObject.Committed_Document__c = true;
        talentDocumentObject.Document_Type__c ='Resume - Job Board';
        
        insert talentDocumentObject;    
        return talentDocumentObject.id;
    }

	public static string getMimeType(String filetype){
		String contentTypeStr ='';
		List<Global_LOV__c> resumeTypeList =[SELECT Id, LOV_Name__c,Name,Text_Value__c,Text_Value_2__c FROM Global_LOV__c WHERE LOV_Name__c ='ATSGenericFiletypesAllowed' AND Text_Value__c=:filetype  with SECURITY_ENFORCED limit 1];
		if(resumeTypeList.size()>0){
			contentTypeStr = (String.isNotBlank(resumeTypeList[0].Text_Value_2__c)  ? resumeTypeList[0].Text_Value_2__c : '');
		}
		return contentTypeStr;
	}
    public static  ATSCandidate prepareCandidateWrapperObject(TalentWrapper wrapObject){
        
        ATSCandidate.ATSNameDetails nameObject = new ATSCandidate.ATSNameDetails();
        nameObject.firstName = wrapObject.talent.profile.firstName;
        nameObject.lastName = wrapObject.talent.profile.lastName;                
        //Set email
        ATSCandidate.ATSEmailDetails emailObject = new ATSCandidate.ATSEmailDetails();
        emailObject.email = wrapObject.talent.contact.email.primary;
        emailObject.workEmail = wrapObject.talent.contact.email.work;
        emailObject.otheremail = wrapObject.talent.contact.email.other;                
        //Set Phone       
        ATSCandidate.ATSPhoneDetails phoneObject = new ATSCandidate.ATSPhoneDetails();                
        phoneObject.phone = wrapObject.talent.contact.phone.work;        
        phoneObject.mobile =  wrapObject.talent.contact.phone.mobile;
        phoneObject.otherPhone =  wrapObject.talent.contact.phone.home;
        //Set Address         
        ATSCandidate.ATSAddressDetails addressObject = new ATSCandidate.ATSAddressDetails();
        addressObject.city = wrapObject.talent.address.city;
        addressObject.country = wrapObject.talent.address.country;
        addressObject.state = wrapObject.talent.address.stateProvince;
        addressObject.street = wrapObject.talent.address.streetAddress;
        addressObject.postalCode = wrapObject.talent.address.postalCode;
        
        if(addressObject.city =='' && addressObject.country =='' && addressObject.state =='' && addressObject.street =='' && addressObject.postalcode ==''){
            addressObject = null;
        }
        
        ATSCandidate candWrapper = new  ATSCandidate();
        
        candWrapper.nameDetails = nameObject;
        candWrapper.emailDetails = emailObject;
        candWrapper.phoneDetails = phoneObject;
        candWrapper.mailingAddress = addressObject;
        candWrapper.vendorId = wrapObject.sourceTracking.vendor;
        candWrapper.jbSourceId = wrapObject.sourceTracking.vendor;
        candWrapper.sourceId = wrapObject.SourceTracking.externalCandidateId;
        candWrapper.transactionsource = wrapObject.SourceTracking.source;
        candWrapper.jobTitle = wrapObject.talent.profile.jobTitle;
        candWrapper.desiredSalary = wrapObject.talent.profile.desiredSalary;
        candWrapper.desiredRate = wrapObject.talent.profile.desiredRate;
        candWrapper.geoPrefComments = wrapObject.talent.profile.geographicPreference;
        candWrapper.highestEducation = wrapObject.talent.profile.highestEducation;
        candWrapper.workEligibility = wrapObject.talent.profile.workEligibility;
        candWrapper.securityClearance = wrapObject.talent.profile.securityClearance;
        candWrapper.talentUpdate = 'YES';
        candWrapper.sfId='';
		candWrapper.recruiterEmail = wrapObject.recruiter.email;
		candWrapper.recruiterFirstName = wrapObject.recruiter.firstName;
		candWrapper.recruiterLastName = wrapObject.recruiter.lastName;
        if(wrapObject.TalentLeadActivity!=null){
          candWrapper.leadUpdatedDate=wrapObject.TalentLeadActivity.leadUpdatedDate; 
		}
        return candWrapper;
        
    }
    private class TalentCreationRespWrapper{
        public string message;
        public string talentAccountId;
        public string talentContactId;
        public string talentUrl;
    }
    
}