@isTest
public class TalentCreationService_Test {
    @TestSetup
	public static void testDataSetup() { 
    	Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.006');
        insert glob;
        
        Global_LOV__c glob1 = new Global_LOV__c(LOV_Name__c ='ATSGenericFiletypesAllowed', Text_Value__c ='pdf', Text_Value_2__c='application/pdf',Source_System_Id__c='ATSGenericFiletypesAllowed.006');
        insert glob1;
    }
    @isTest static void testCreateTalent() {
         RestRequest req = new RestRequest(); 
         RestResponse res = new RestResponse();
		 String requestJSONBody = '{'+
									'"sourceTracking":{'+
									'"source":"SourceBreaker",'+
									'"vendor":"CVLibrary",'+
									'"transactionId":"AB1234Z",'+
									'"externalCandidateId":"SBx213"'+
									'},'+
									'"recruiter":{'+
									'"firstName":"Praveen",'+
									'"lastName":"Chinnappa",'+
									'"email":"pchinnappa@testmail.com"'+
									'},'+
									'"talent":{'+
									'"profile":{'+
									'"firstName":"TestFirstName",'+
									'"lastName":"TestLastName",'+
									'"jobTitle":"JavaDeveloper",'+
									'"desiredSalary":"$100000",'+
									'"desiredRate":"$50",'+
									'"desiredFrequency":"Daily",'+
									'"workEligibility":"I have the right to work for any employer",'+
									'"geographicPreference":"Within France",'+
									'"highestEducation":"",'+
									'"securityClearance":"Confidential Or R"'+
									'},'+
									'"contact":{'+
									'"phone":{'+
									'"mobile":"+43123421 54",'+
									'"work":"112 4324 6543",'+
									'"home":"",'+
									'"unspecified":["1223212233","152431233"]'+
									'},'+
									'"email":{'+
									'"primary":"",'+
									'"work":"",'+
									'"other":"",'+
									'"unspecified":["testa@testmail.com","teat123@teatmail.com"]}'+
									'},'+
									'"address":{'+
									'"streetAddress":"",'+
									'"city":"",'+
									'"stateProvince":"",'+
									'"country":"GB",'+
									'"postalCode":"CB123"'+
									'},'+
									'"resume":{'+
									'"fileType":"pdf",'+
									'"base64":"aswsdf"'+
									'},'+
									'"attachment":{'+
									'"fileType":"doc",'+
									'"base64":"awesdf"'+
									'}}}';
            
            req.requestURI = '/services/apexrest/Talent/TalentInfo/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            TalentCreationService.createTalent();
        	system.assertEquals(res.statusCode, 200, 'Passed');
        	Test.stopTest();
    }
    
    @isTest static void testCreateTalent_MissingFileType() {
         RestRequest req = new RestRequest(); 
         RestResponse res = new RestResponse();
		 String requestJSONBody = '{'+
									'"sourceTracking":{'+
									'"source":"SourceBreaker",'+
									'"vendor":"CVLibrary",'+
									'"transactionId":"AB1234Z",'+
									'"externalCandidateId":"SBx213"'+
									'},'+
									'"recruiter":{'+
									'"firstName":"Praveen",'+
									'"lastName":"Chinnappa",'+
									'"email":"pchinnappa@testmail.com"'+
									'},'+
									'"talent":{'+
									'"profile":{'+
									'"firstName":"TestFirstName",'+
									'"lastName":"TestLastName",'+
									'"jobTitle":"JavaDeveloper",'+
									'"desiredSalary":"$100000",'+
									'"desiredRate":"$50",'+
									'"desiredFrequency":"Daily",'+
									'"workEligibility":"I have the right to work for any employer",'+
									'"geographicPreference":"Within France",'+
									'"highestEducation":"",'+
									'"securityClearance":"Confidential Or R"'+
									'},'+
									'"contact":{'+
									'"phone":{'+
									'"mobile":"+43123421 54",'+
									'"work":"112 4324 6543",'+
									'"home":"",'+
									'"unspecified":["1223212233","152431233"]'+
									'},'+
									'"email":{'+
									'"primary":"",'+
									'"work":"",'+
									'"other":"",'+
									'"unspecified":["testa@testmail.com","teat123@teatmail.com"]}'+
									'},'+
									'"address":{'+
									'"streetAddress":"",'+
									'"city":"",'+
									'"stateProvince":"",'+
									'"country":"GB",'+
									'"postalCode":"CB123"'+
									'},'+
									'"resume":{'+
									'"fileType":"docx",'+
									'"base64":"aswsdf"'+
									'},'+
									'"attachment":{'+
									'"fileType":"doc",'+
									'"base64":"awesdf"'+
									'}}}';
            
            req.requestURI = '/services/apexrest/Talent/TalentInfo/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            TalentCreationService.createTalent();
        	system.assertEquals(res.statusCode, 200, 'Passed');
        	Test.stopTest();
    }
    
    @isTest static void testCreateTalent_TalentUpsertFailure() {
         RestRequest req = new RestRequest(); 
         RestResponse res = new RestResponse();
		 String requestJSONBody = '{'+
									'"sourceTracking":{'+
									'"source":"SourceBreaker",'+
									'"vendor":"CVLibrary",'+
									'"transactionId":"AB1234Z",'+
									'"externalCandidateId":"SBx213"'+
									'},'+
									'"recruiter":{'+
									'"firstName":"Praveen",'+
									'"lastName":"Chinnappa",'+
									'"email":"pchinnappa@testmail.com"'+
									'},'+
									'"talent":{'+
									'"profile":{'+
									'"firstName":"TestFirstName",'+
									'"lastName":"TestLastName",'+
									'"jobTitle":"JavaDeveloper",'+
									'"desiredSalary":"$100000",'+
									'"desiredRate":"$50",'+
									'"desiredFrequency":"Daily",'+
									'"workEligibility":"I have the right to work for any employer",'+
									'"geographicPreference":"Within France",'+
									'"highestEducation":"",'+
									'"securityClearance":"Confidential x"'+
									'},'+
									'"contact":{'+
									'"phone":{'+
									'"mobile":"+43123421 54",'+
									'"work":"112 4324 6543",'+
									'"home":"",'+
									'"unspecified":["1223212233","152431233"]'+
									'},'+
									'"email":{'+
									'"primary":"",'+
									'"work":"",'+
									'"other":"",'+
									'"unspecified":["testa@testmail.com","teat123@teatmail.com"]}'+
									'},'+
									'"address":{'+
									'"streetAddress":"",'+
									'"city":"",'+
									'"stateProvince":"",'+
									'"country":"GB",'+
									'"postalCode":"CB123"'+
									'},'+
									'"resume":{'+
									'"fileType":"pdf",'+
									'"base64":"aswsdf"'+
									'},'+
									'"attachment":{'+
									'"fileType":"doc",'+
									'"base64":"awesdf"'+
									'}}}';
            
            req.requestURI = '/services/apexrest/Talent/TalentInfo/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            TalentCreationService.createTalent();
        	system.assertEquals(res.statusCode, 200, 'Passed');
        	Test.stopTest();
    }
     @isTest static void testCreateTalent_Exception() {
         RestRequest req = new RestRequest(); 
         RestResponse res = new RestResponse();
		 String requestJSONBody = '{'+
									'"sourceTracking":{'+
									'"source":"SourceBreaker"'+
									'"vendor":"CVLibrary",'+
									'"transactionId":"AB1234Z",'+
									'"externalCandidateId":"SBx213"'+
									'},'+
									'"recruiter":{'+
									'"firstName":"Praveen",'+
									'"lastName":"Chinnappa",'+
									'"email":"pchinnappa@testmail.com"'+
									'},'+
									'"talent":{'+
									'"profile":{'+
									'"firstName":"TestFirstName",'+
									'"lastName":"TestLastName",'+
									'"jobTitle":"JavaDeveloper",'+
									'},'+
									'"contact":{'+
									'"phone":{'+
									'"mobile":"+43123421 54",'+
									'"work":"112 4324 6543",'+
									'"home":"",'+
									'"unspecified":["1223212233","152431233"]'+
									'},'+
									'"resume":{'+
									'"fileType":"pdf",'+
									'"base64":"aswsdf"'+
									'},'+
									'"attachment":{'+
									'"fileType":"doc",'+
									'"base64":"awesdf"'+
									'}}}';
            
            req.requestURI = '/services/apexrest/Talent/TalentInfo/V1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
        	Test.startTest();
            TalentCreationService.createTalent();
        	system.assertEquals(res.statusCode, 400, 'Failed');
        	Test.stopTest();
    }
}