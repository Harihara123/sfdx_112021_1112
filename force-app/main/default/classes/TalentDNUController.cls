public class TalentDNUController {
    
    //Create a Method to get piclist values using global describe call
    @AuraEnabled
    public static Map<String, List<String>> getAllPicklistValues(String objectStr, string fld) {
        Map<String, List<String>> objectPicklistValuesMap = new Map<String, List<String>>();
        List<String> lstfields = fld.split(',');
        Integer i = 0;
        for(String fldName : lstfields){
                objectPicklistValuesMap.put('Contact.'+ fldName, TalentDNUController.picklistValues(objectStr, fldName));
                
        }
        return objectPicklistValuesMap;
    }
    public static List<String> picklistValues(String objectStr, string fld){    
        List<String> allOpts = new list < String > ();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(objectStr);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values =
        fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
    
    
    @AuraEnabled
    public static void saveDNUDetails(Contact conObj){
       Contact con =[select id, DNU_Last_Modified_By__c, DNU_Last_Modified_Date__c, DNU_Reason__c, Do_Not_Use__c from Contact where id=: conObj.id];
        if(conObj.Do_Not_Use__c  && !con.Do_Not_Use__c ){
            conObj.DNU_Last_Modified_By__c = userInfo.getUserId();
            conObj.DNU_Last_Modified_Date__c = system.now();
        }
        update conObj;
        
    }
    
}