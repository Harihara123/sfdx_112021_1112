@isTest
public class TalentDNUController_Test {

    	 public static Account createTalentAccount(){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Contact createTalentContact(String accountID){
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        return newC;
    }
    
    public static testMethod void talentDNUPicklistTest(){
        Account masterAccount = TalentDNUController_Test.createTalentAccount();
        Contact masterContact = TalentDNUController_Test.createTalentContact(masterAccount.Id);
        
        Map<String, List<String>> picklistValueMap = TalentDNUController.getAllPicklistValues('Contact', 'LeadSource');
    }
}