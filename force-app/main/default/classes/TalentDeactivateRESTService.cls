@RestResource(urlMapping='/Person/TalentDeactivate/V1')
global without sharing Class TalentDeactivateRESTService
{ 
     @HttpPost
    global static Map<string,string> doPost(String ID, Boolean activateTalent)
    {
        RestResponse response = RestContext.response;
        if(response != null){
        	response.addHeader('Content-Type', 'application/json');
        }    
        String  message;
        String success = '';
        Map<String,String> result = new Map<String,String>();
        try{
        Contact c = [select Id,Name,AccountId,Talent_Ownership__c,Record_Type_Name__c from contact where  id =: ID or AccountId =: ID] ;
        List<Account> acc = new List<Account>();
       
	   
	   Account a = new Account();
        if(activateTalent != null && activateTalent){
                a = [select id,Talent_Ownership__c,Record_Type_Name__c from Account where id =: c.AccountId ];
                
				If(a.Talent_Ownership__c != 'INACTIVE'){
					result.put('Success','false');
					message ='The talent with ID '+ID+' is already active';
				}else{
				
					String latestValue = '';
                        String beforeInactive = '';
                        Boolean isInactiveAvailable = false;
                        for(AccountHistory ah : [Select field, NewValue, OldValue from AccountHistory where AccountId =:c.AccountId  AND Field='Talent_Ownership__c'  Order By CreatedDate DESC]){
                        if(latestValue == ''){	
                            latestValue = String.ValueOf(ah.NewValue);
							a.Talent_Ownership__c = latestValue;
						}
                        if(ah.NewValue == 'INACTIVE'){
                            a.Talent_Ownership__c = String.ValueOf(ah.OldValue);
                            if(String.isBlank(a.Talent_Ownership__c)){
                              	a.Talent_Ownership__c  = 'RWS';
                            }
                          break;  
                        }
                            
                        }
                        message ='The talent with ID '+ID+' has been activated';
					update a;
					result.put('Success','true');
				} 
				result.put('Message',message);
        
        }else{
            if(c.Record_Type_Name__c == 'Talent'){
                 a = [select id,Talent_Ownership__c,Record_Type_Name__c from Account where id =: c.AccountId and Record_Type_Name__c = 'Talent'];
                
                a.Talent_Ownership__c = 'INACTIVE';
                message ='The talent with ID '+ID+' has been inactivated';
                
                update a;
                 result.put('Success','true');
                 result.put('Message',message);
                
            }else{
                 message = 'The talent with ID '+ID+' could not be inactivated';
                 result.put('Success','false');
                 result.put('Message',message);
                 result.put('RecordType:', c.Record_Type_Name__c);
            }
            
            }
            connectedlog.LogInformation('Information', 'TalentDeactivateRESTService', 'doPost', null, null, null, message, result); 
        }Catch(Exception ex )
        {
		  result.put('Success','false');
          if(activateTalent != null && activateTalent){
            result.put('Message','The talent with ID '+ID+' could not be activated'); 
			}else{
            result.put('Message','The talent with ID '+ID+' could not be inactivated'); 
			
          }
         ConnectedLog.LogException('Person/TalentDeactivate','TalentDeactivateRESTService','doPost',ex);
         }
            
          return result;
         }

}