@RestResource(urlMapping='/Person/dupeMatch/V1')
global without sharing class TalentDedupeSearch {
    Public static Map<String,String> logMatch = new Map<String,String>();
    Public static Boolean phoneFlag = false;
    Public static Boolean emailFlag = false;
    Public Static String inputSourceId ;
    public class JSONWrapper {
        
        public String name;
        public List<String> email;
        public List<String> phone;
        public String linkedInUrl;
        public String externalSourceId;
        public List<String> recordTypes;
        public String returnCount;
        public Boolean IsAtsTalent;
        public List<String> City;
        public List<String> State;
        public List<String> Country;
        public List<String> Zip;
       
        
        public List<JSONWrapper> parse(String json) {
            return (List<JSONWrapper>) System.JSON.deserialize(json, List<JSONWrapper>.class);
        }
    }
    
    
    @HttpGet
    global static void getDedupTalents() {
      
        try{
            
            Map<String, String> params = RestContext.request.params;
            String getStrParam = RestContext.request.params.get('Criteria');
            
            JSONWrapper obj = new JSONWrapper();
            List<JSONWrapper> objData = obj.parse(getStrParam);
           
            List<List<SObject>> res = TalentDedupeSearch.postDedupTalents( objData[0].name, objData[0].email, objData[0].phone, objData[0].linkedInUrl, objData[0].externalSourceId, objData[0].recordTypes, objData[0].returnCount, objData[0].IsAtsTalent );
            
            RestContext.response.addHeader('Content-Type', 'application/json');
            
            if( res.isEmpty() || res[0].isEmpty() ){
                RestContext.response.responseBody = Blob.valueOf('[{"Message" : "No duplicates found"}]'); 
               logMatchRules('No_Match','',objData[0].externalSourceId,'No_Match');
            }else{
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
            }   
            
        }catch (Exception excep) {
            System.debug(excep);
            ConnectedLog.LogException('TalentDedupeSearch', 'getDedupTalents', excep);
        }     
        
    }
    
    @HttpPost
    global static List<List<SObject>> postDedupTalents(String name, list<String> email, list<String> phone, String linkedInUrl, String externalSourceId, list<String> recordTypes, String returnCount, Boolean IsAtsTalent ) {
        List<List<SObject>> results = new List<List<SObject>>();
        try{
        	inputSourceId = externalSourceId;
            list<id> recordTypeIds = new list<id>();
            //System.debug('recordTypes--'+recordTypes);        
        if( recordTypes != null && !recordTypes.isEmpty() ){
            for( String recTypeName : recordTypes){
                if( Schema.SObjectType.contact.getRecordTypeInfosByName().get( recTypeName ) != null ){
                    recordTypeIds.add( Schema.SObjectType.contact.getRecordTypeInfosByName().get( recTypeName ).getRecordTypeId() );    
                }
            }
          //  System.debug('recordTypeIds--'+recordTypeIds);
        }
        
        /* Return SOQL result from SOQL if externalSourceId is passed in params and externalSourceId has a match in SOQL */
        if(!String.isBlank( externalSourceId ) ){
            List<List<SObject>> lstContact = new List<List<SObject>>();
            //logMatch.put('Input:',externalSourceId);
            // for 13.0 release
            List<TalentVendor_Xref__c> talentVendorXref = [select id,VendorTalentId__c,vendorId__c, contactId__c from TalentVendor_Xref__c where VendorTalentId__c =: externalSourceId limit 1];
            if(talentVendorXref.size()>0){
                list<contact> contact = [select Name, Id, email, Other_Email__c, Work_Email__c, HomePhone, phone, MobilePhone, OtherPhone, Accountid, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, Title, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Preferred_Name__c, Talent_Id__c, Account.Name  from Contact where id =: talentVendorXref[0].contactId__c limit 1];
            	lstContact.add(contact); 
                if( contact != null &&  !contact.isEmpty()) {
                    
                    logMatchRules('Match_VendorCandidateID',contact[0].id,inputSourceId,'Match_VendorCandidateID');
                    return lstContact;
            	}
            }
            
            
            
        }
        
        if( IsAtsTalent == null ){
                IsAtsTalent = true;
            }
            
            
            
            String phoneParam;
            String emailParam;
            
        //Start Neel S-107676 - Adding wildcard for name        
       //D-12728-PP - Talent Dedupe defect fixes and testing.
        /* String searchStr='';
        if(String.isNotBlank(name)) {
            for(String sString : name.split(' ')) {
                if(String.isNotBlank(searchStr)) {
                    searchStr = searchStr + ' ';
                }
                searchStr = searchStr + sString+ '*';
            }
            name = searchStr;
        }*/
        //End Neel S-107676 
        
        //Start Siva STORY S-110297 - Excluding fake phone numbers
		List<String> noFakePhs = new List<String>();
		noFakePhs = TalentDedupeSearch.excludeFakePhone(phone);
		//End Siva STORY S-110297

        //Start for  STORY S-152681
		List<String> noFakeEmail = new List<String>();
		noFakeEmail = TalentDedupeSearch.excludeFakeEmail(email);
		//End for STORY S-152681

        if(noFakePhs.size() == 1 ){
            phoneParam = noFakePhs[0];
        }else if(noFakePhs.size() > 1 ){
            phoneParam = convertListIntoCondition(noFakePhs);
        }
        
        if( noFakeEmail.size() == 1){
            emailParam = noFakeEmail[0];
        }else if(noFakeEmail.size() > 1 ){
            emailParam = convertListIntoCondition(noFakeEmail);
        }


        
        //    List<List<SObject>> nameSearch      = new List<List<SObject>>();
        List<List<SObject>> phoneSearch     = new List<List<SObject>>();
        List<List<SObject>> emailSearch     = new List<List<SObject>>();
        
        List<List<SObject>> concatenatedList  = new List<List<SObject>>();
        
        if( recordTypeIds.size() > 0 ){
            /*
if(! String.isEmpty(name )){
nameSearch  = [FIND :name IN NAME  FIELDS RETURNING Contact(  Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid, Current_Employer__c, Customer_Status__c, Do_Not_Contact__c,Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];  
}
*/ 
            if(! String.isEmpty(phoneParam )){
                phoneSearch = [FIND :phoneParam IN PHONE FIELDS RETURNING Contact(FirstName,LastName, Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, AccountId, Current_Employer__c, Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, LastModifiedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title, MailingCity,TC_Title__c, MailingState, Talent_Country_Text__c,Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];
            }
            
            if(! String.isEmpty(emailParam )){
                emailSearch = [FIND :emailParam IN EMAIL FIELDS RETURNING Contact(FirstName,LastName, Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, AccountId, Current_Employer__c,Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, LastModifiedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState,Talent_Country_Text__c, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];    
            }
            
            
        }else{
            /*
if(! String.isEmpty(name )){
nameSearch  = [FIND :name IN NAME  FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, email, Talent_Id__c, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid,Current_Employer__c, Customer_Status__c, Do_Not_Contact__c,Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name  ) ];
}
*/
            if(! String.isEmpty(phoneParam )){
                phoneSearch = [FIND :phoneParam IN PHONE FIELDS RETURNING Contact(FirstName,LastName, Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, AccountId,Current_Employer__c,Customer_Status__c, Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate,LastModifiedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Talent_Country_Text__c,Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name   ) ];
            }
            if(! String.isEmpty(emailParam )){
                emailSearch = [FIND :emailParam IN EMAIL FIELDS RETURNING Contact(FirstName,LastName, Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, AccountId,Current_Employer__c, Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, LastModifiedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState,Talent_Country_Text__c,Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name  ) ];    
            }
        }
        
        /*
if( nameSearch.size() == 0 ){
return new List<List<SObject>>();
}
*/
        if( phoneSearch.size() == 0 && emailSearch.size() == 0 ){
            /**Dedupe Does not find match on VendorID, Phone or Email -- No_Match**/
            return new List<List<SObject>>();
        }
        
        
        if(emailSearch.size()>0){   
            for(integer i=0; i<emailSearch[0].size(); i++){
                
                if( phoneSearch.size() > 0 ) {
                for(integer j=0; j<phoneSearch[0].size(); j++){
                    if(emailSearch[0][i].id == phoneSearch[0][j].id){
                        phoneSearch[0].remove(j);
                        // removing common records from either one of the list as we are looking for records either phone or email matches.
                        // and we will be concatinating both the list. so duplicates will be removed.
                    }
                    }   
                }
            }
        }
       // system.debug('Phone Search-->'+phoneSearch[0]);
       //system.debug('Email Search-->'+emailSearch[0]);
       
        // Combining both phone search and email search results in one single list for futher logic
        List<Sobject> temp = new List<Sobject>();
        if(phoneSearch.size()>0){
        	temp.addAll(phoneSearch[0]); 
            phoneflag = true;
            }
        if(emailSearch.size()>0){
            emailflag = true;
        	temp.addAll(emailSearch[0]);  
           }
        concatenatedList.add(temp);
			
			
					if( concatenatedList[0].size() > 0 ){
                       System.debug('Size'+concatenatedList[0].size());
						if(Userinfo.getUserType() =='cspLitePortal'){
							// logic for communities user - dont check for name match just apply dedupe rules and return record.
							results.add(TalentDedupeSearch.applyDedupeRules(concatenatedList[0],returnCount));
						}else{
							List<SObject> lstNameMatchedContacts = new List<SObject>();
                           	// iterate all records to find firstname/lastname/name is matched
							for(integer i=0; i<concatenatedList[0].size(); i++){
                                
							   // system.debug('RecordtypeName-->'+String.valueOf(concatenatedList[0][i].get('Record_Type_Name__c')));
									if(String.valueOf(concatenatedList[0][i].get('Record_Type_Name__c')) =='Talent'  && IsAtsTalent){
                                        //D-12728-PP - Talent Dedupe defect fixes.
                                        if(name.equalsIgnoreCase(String.valueOf(concatenatedList[0][i].get('Name'))))
                                        {
                                            
                                            lstNameMatchedContacts = new List<SObject>();
                                            lstNameMatchedContacts.add(concatenatedList[0][i]);
                                            break;
                                         
                                        }
                                       else if(name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('LastName'))) && name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('FirstName'))) ){
											lstNameMatchedContacts.add( concatenatedList[0][i] );
                                           	}
                                        else if(name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('Name')))){
                                            lstNameMatchedContacts.add( concatenatedList[0][i] );
										}    
									}else if(IsAtsTalent == false){
										if(name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('LastName'))) && name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('FirstName'))) ){
											lstNameMatchedContacts.add( concatenatedList[0][i] );
										}else if(name.containsIgnoreCase(String.valueOf(concatenatedList[0][i].get('Name')))){
											lstNameMatchedContacts.add( concatenatedList[0][i] );
										}
									}
                       
                    
							}
							if(lstNameMatchedContacts.size()>0){
                               
                                results.add(TalentDedupeSearch.applyDedupeRules(lstNameMatchedContacts,returnCount));
                                
							}
                            else{
                             if(phoneFlag == true && emailFlag == true){
                             logMatchRules('Match_Email_Phone_NoMatch_Name','',inputSourceId,'No_Match');    
                             }else if(phoneFlag == true){
                             logMatchRules('Match_Phone_NoMatch_Name','',inputSourceId,'No_Match');    
                             }else if (emailFlag == true){
                             logMatchRules('Match__Email_NoMatch_Name','','','No_Match');     
                             }
                            
                            }
                            }
                        
                    } 
            return results;
        }Catch(Exception e){
            System.debug(e);
            ConnectedLog.LogException('TalentDedupeSearch', 'postDedupTalents', e);
            return null;
        }
    }

	public static List<String> excludeFakePhone(List<String> phone){
		List<String> noFakePhs = new List<String>();
		pattern sameNum = pattern.compile('(\\d)\\1+');
		pattern seq0 = pattern.compile('01234.*');
		pattern seq1 = pattern.compile('12345.*');
		pattern seq2 = pattern.compile('.*12345');
		pattern seq3 = pattern.compile('09876.*');
		pattern pairRepeats = pattern.compile('.*(\\d)(\\d)(\\1\\2){3}.*');
		pattern threeRepeats = pattern.compile('.*(\\d)(\\d)(\\d)(\\1\\2\\3){2}.*');
		pattern fourRepeats = pattern.compile('.*(\\d)(\\d)(\\d)(\\d)(\\1\\2\\3\\4){1}.*');
		pattern fiveRepeats = pattern.compile('.*(\\d)(\\d)(\\d)(\\d)(\\d)(\\1\\2\\3\\4\\5){1}.*');

		for(String s : phone) {
			String ph = s.replaceAll('[^0-9]', '');
			if(!(sameNum.matcher(ph).matches() || seq0.matcher(ph).matches() || seq1.matcher(ph).matches() || seq2.matcher(ph).matches() || seq3.matcher(ph).matches() || pairRepeats.matcher(ph).matches() || threeRepeats.matcher(ph).matches() || fourRepeats.matcher(ph).matches() || fiveRepeats.matcher(ph).matches())) {
				noFakePhs.add(s);
			}
		}
		return noFakePhs;
	}

    public static List<String> excludeFakeEmail(List<String> email){
		List<String> noFakeEmail = new List<String>();
		List<String> fakeEmailList = new List<String>();
		List<Exclude_for_Dedupe_Check__mdt> excludeEmailList =[select Email__c from Exclude_for_Dedupe_Check__mdt];
		for(Exclude_for_Dedupe_Check__mdt mdtObj : excludeEmailList){
			fakeEmailList.add(String.valueOf(mdtObj.Email__c));
		}

		for(String em : email){
			if(!fakeEmailList.contains(em.toLowerCase())){
				noFakeEmail.add(em);
			}
		}
		return noFakeEmail;
	}

    public static List<SObject> applyDedupeRules(List<SObject> matchedContactList, String returnCount){
		List<SObject> finalRecordList = new List<SObject>();
        //  if there is only single record return the results
					if((matchedContactList.size() == 1) || (matchedContactList.size() > 0 && returnCount.touppercase() == 'MANY') ){
						finalRecordList.addAll(matchedContactList);
                      
                        if(phoneFlag == true && emailFlag == true){
                           logMatchRules('Match_Email_Phone_Name',matchedContactList[0].id,inputSourceId,'Match_Email_Phone_Name');
                        }else if(phoneFlag == true){
                           logMatchRules('Match__Phone_Name',matchedContactList[0].id,inputSourceId,'Match__Phone_Name');
                        }else {
                           logMatchRules('Match__Email_Name',matchedContactList[0].id,inputSourceId,'Match__Email_Name');
                        }
                        
					}
                
					// if multiple records look for most recently created record 
					else if(matchedContactList.size() > 0 && returnCount.touppercase() == 'ONE'){
						datetime recentCreatedDate   = null;
						datetime recentCreatedDateForPs = null;
						boolean hasPeoplesoft   = false;  
                       
						for(integer i=0; i<matchedContactList.size(); i++){
                             if(matchedContactList[i].get('Peoplesoft_ID__c') != null ){
								hasPeoplesoft = true;
                            	if( recentCreatedDate == null ){
									finalRecordList.clear();
									recentCreatedDate = datetime.valueOf(matchedContactList[i].get('CreatedDate') );
									finalRecordList.add( matchedContactList[i] );
                                     logMatchRules('Match_Email_Phone_Name_PSID',matchedContactList[0].id,inputSourceId,'Match_Email_Phone_Name_PSID');
								}else{
									if( datetime.valueOf( matchedContactList[i].get('CreatedDate') ) > recentCreatedDate ){
										finalRecordList.clear();
										System.debug('Match_Email_Phone_Name_CreatedDate');
                                        recentCreatedDate = datetime.valueOf( matchedContactList[i].get('CreatedDate') );
										finalRecordList.add( matchedContactList[i] );
                                        logMatchRules('Match_Email_Phone_Name_CreatedDate',matchedContactList[0].id,inputSourceId,'Match_Email_Phone_Name_CreatedDate');
                                        }
								}
                            
                            
							}else if( !hasPeoplesoft ){
                            System.debug('NO PSID');
								if( recentCreatedDateForPs == null ){
									finalRecordList.clear();
									recentCreatedDateForPs = datetime.valueOf( matchedContactList[i].get('CreatedDate') );
									finalRecordList.add( matchedContactList[i] );
								}else{
									if( datetime.valueOf(matchedContactList[i].get('CreatedDate') ) > recentCreatedDateForPs ){
										finalRecordList.clear();
										recentCreatedDateForPs = datetime.valueOf( matchedContactList[i].get('CreatedDate') );
										finalRecordList.add( matchedContactList[i] );
                                        logMatchRules('Match_Email_Phone_Name_CreatedDate',matchedContactList[0].id,inputSourceId,'Match_Email_Phone_Name_CreatedDate');
									}
								}
                            
							}
                        
						}	
					
					}
					
					return finalRecordList;
	}

    /* This method rconverts the list of strings into OR condition */
    public static string convertListIntoCondition(List<String> lstString ){
        String[] tmp1 = New String[]{};
            
            for(String str : lstString){
                tmp1.add(str);
            }
        
        string concat = string.join(tmp1,' OR ');
        
        return concat;
        
    }
  Public Static Void logMatchRules(String DeDuperesult,String DuplicateTalent, String Input, String logMessage)
  {
    Map<String,String> logMap =new Map<String,String>();
    logMap.put('DeDuperesult',DeDuperesult);
    logMap.put('DuplicateTalent',DuplicateTalent);   
    logMap.put('Input',Input); 
      System.debug('logMap::'+logMap);
    connectedlog.LogInformation('Information', 'TalentDedupeSearch', 'postDedupTalents', null, null, null, logMessage, logMap);
     
  }
  
    
}