@isTest
public class TalentDedupeSearchV2_Test {  
    @isTest 
    static void DedupeSearchGetTestMethod() {
        RestRequest req = new RestRequest();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'GET';
        req.addParameter('Criteria', '[{"name" : "TestData","email" : ["TestData@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "externalSourceId" : "00123","returnCount" : "ONE","recordTypes" : ["Talent"], "IsAtsTalent" : true,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]');
        RestContext.request = req;
        String getStrParam = RestContext.request.params.get('Criteria');
        Test.startTest();
        TalentDedupeSearch_v2.getDedupTalents();
        Test.stopTest();        
    }
	@isTest 
    static void DedupeSearchPostTestMethod() {
        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","email" : ["TestData@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "externalSourceId" : "00123","returnCount" : "ONE","recordTypes" : ["Talent"], "IsAtsTalent" : true,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]';      
		req.requestBody =Blob.valueof(JsonMsg);
		RestContext.request = req;
		RestContext.response= res;        
        String getStrParam  = RestContext.request.requestBody.toString();
        Test.startTest();
		TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();       
    }
	@isTest 
    static void DedupeSearchGetTestMethod2() {
        Account newAcc = BaseController_Test.createTalentAccount('');
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.email = 'TestData@mail.com';
        con.phone='100000001';
		con.externalSourceId__c = '00123';
		update con;
		
		RestRequest req = new RestRequest();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'GET';
        req.addParameter('Criteria', '[{"name" : "TestData","email" : ["TestData@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "externalSourceId" : "00123","returnCount" : "ONE","recordTypes" : ["Talent"], "IsAtsTalent" : true,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]');
        RestContext.request = req;
        Test.startTest();
        TalentDedupeSearch_v2.getDedupTalents();
        Test.stopTest();        
    }
	@isTest 
    static void DedupeSearchPostTestMethod2() {        
        Account newAcc = BaseController_Test.createTalentAccount('');
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.email = 'TestData@mail.com';
        con.phone='100000001';
		con.externalSourceId__c = '00123';
		update con;

        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","email" : ["TestData@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "externalSourceId" : "00123","returnCount" : "ONE","recordTypes" : ["Talent"], "IsAtsTalent" : true,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]';      
		req.requestBody =Blob.valueof(JsonMsg);
		RestContext.request = req;
		RestContext.response= res;        
        Test.startTest();
		TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();        
    }
	@isTest  
    static void DedupeSearchPostTestMethod3() {        
        Account newAcc = BaseController_Test.createTalentAccount('');
		List<Contact> conList = new List<Contact>();
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.email = 'TestData@mail.com';
        con.phone='100000001';		
		conList.add(con);
		//con.linkedInUrl = 'test';
		Contact con1 = BaseController_Test.createTalentContact(newAcc.Id);
		con1.Firstname = 'Test';
		con1.lastname = 'Data';
        con1.email = 'TestData9@test.com';
        con1.phone='100000003';				
		conList.add(con1);
		Contact con2 = BaseController_Test.createClientContact(newAcc.Id);
		con2.lastname = 'TestData';
        con2.email = 'TestData1@mail.com';
        con2.phone='100000002';		
		conList.add(con2);
        
		update conList;

        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","email" : ["TestData@mail.com","TestData1@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "returnCount" : "ONE","IsAtsTalent" : true,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]'; 
		req.requestBody =Blob.valueof(JsonMsg);	
		RestContext.request = req;		
		RestContext.response= res;        
        Test.startTest();
		//TalentDedupeSearch_v2.getDedupTalents();
        TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();
		}

	@isTest   (seeAllData=true)
    static void DedupeSearchPostTestMethod4() {        
        Account newAcc = BaseController_Test.createTalentAccount('');
		List<Contact> conList = new List<Contact>();
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.email = 'TestData@mail.com';
        con.phone='100000001';
		con.Peoplesoft_ID__c = '10000' ;	
		conList.add(con);
        Contact con1 = BaseController_Test.createTalentContact(newAcc.Id);
		con1.Firstname = 'Test';
		con1.lastname = 'Data';
        con1.email = 'TestData9@test.com';
        con1.phone='100000003';
		con1.Peoplesoft_ID__c = '10009' ;
		conList.add(con1);
		
		Contact con2 = BaseController_Test.createClientContact(newAcc.Id);
		con2.lastname = 'TestData';
        con2.email = 'TestData1@test.com';
        con2.phone='100000001';
		con2.Peoplesoft_ID__c = '10008' ;		
		conList.add(con2);

		Contact con3 = BaseController_Test.createClientContact(newAcc.Id);
		con3.lastname = 'TestData';
        con3.email = 'TestData@test.com';
        con3.phone='100000001';	
		//con3.Peoplesoft_ID__c = '10007' ;	
		conList.add(con3);

		Contact con4 = BaseController_Test.createClientContact(newAcc.Id);
		con4.lastname = 'Test1';
        con4.email = 'TestData@mail.com';
        con4.phone='100000001';
		//con4.Peoplesoft_ID__c = '10006' ;	
		conList.add(con4);

		Contact con5 = BaseController_Test.createClientContact(newAcc.Id);
		con5.lastname = 'Test1';
        con5.email = 'TestData@mail5.com';
        con5.phone='100000001';	
		con5.Peoplesoft_ID__c = '10005' ;	
		conList.add(con5);
        
		update conList;

		List<Task> tasks = new List<Task>();
		tasks.add(new Task(
		ActivityDate = Date.today().addDays(7),
		Subject='Sample Task',
		WhoId = con.Id,
		OwnerId = UserInfo.getUserId(),
		Status='In Progress'));
		tasks.add(new Task(
		ActivityDate = Date.today().addDays(7),
		Subject='Sample Task',
		WhoId = con4.Id,
		OwnerId = UserInfo.getUserId(),
		Status='In Progress'));

		insert tasks;

        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","email" : ["TestData@mail.com","TestData1@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "returnCount" : "ONE","recordTypes" : ["Talent"],"IsAtsTalent" : false,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]'; 
		req.requestBody =Blob.valueof(JsonMsg);	

		RestContext.request = req;		
		RestContext.response= res;        
        
		ID[] fixedSearchResults = new ID[6]; 
		fixedSearchResults[0] = con.id;
		fixedSearchResults[1] = con1.id;
		fixedSearchResults[2] = con2.id;
		fixedSearchResults[3] = con3.id;
		fixedSearchResults[4] = con4.id;
		fixedSearchResults[5] = con5.id; 
		Test.setFixedSearchResults(fixedSearchResults);

		Test.startTest();
		TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();
		}
	@isTest  
    static void DedupeSearchPostTestMethod5() {
        Account newAcc = BaseController_Test.createTalentAccount('');
		List<Contact> conList = new List<Contact>();
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.email = 'TestData@mail1.com';
        con.phone='100000021';
		//con.Peoplesoft_ID__c = '10000' ;	
		conList.add(con);

		//con.linkedInUrl = 'test';
		Contact con1 = BaseController_Test.createTalentContact(newAcc.Id);
		con1.Firstname = 'Test1';
		con1.lastname = 'Data';
        con1.email = 'TestData@mail.com';
        con1.phone='100000011';
		//con1.Peoplesoft_ID__c = '10009' ;
		//con4.LastActivityDate = Date.today();				
		conList.add(con1);
		
		Contact con2 = BaseController_Test.createClientContact(newAcc.Id);
		con2.lastname = 'Test1Data';
        con2.email = 'TestData1@test.com';
        con2.phone='100000001';
		//con2.Peoplesoft_ID__c = '10008' ;		
		conList.add(con2);

		Contact con3 = BaseController_Test.createClientContact(newAcc.Id);
		con3.lastname = 'TestData';
        con3.email = 'TestDat@mail.com';
        con3.phone='100000001';	
		con3.Peoplesoft_ID__c = '10007' ;	
		conList.add(con3);

		Contact con4 = BaseController_Test.createClientContact(newAcc.Id);
		con4.lastname = 'TestData';
        con4.email = 'TestData@mail.com';
        con4.phone='100000001';
		con4.Peoplesoft_ID__c = '10006' ;	
		//con4.LastActivityDate = Date.today();	
		conList.add(con4);

		Contact con5 = BaseController_Test.createClientContact(newAcc.Id);
		con5.lastname = 'Test1';
        con5.email = 'TestData@mail5.com';
        con5.phone='100000001';	
		//con5.Peoplesoft_ID__c = '10005' ;	
		conList.add(con5);        
		update conList;	

        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","email" : ["TestData@mail.com","TestData1@mail.com"],"phone" : ["100000001"],"linkedInUrl" : "test", "returnCount" : "ONE","recordTypes" : ["Talent"],"IsAtsTalent" : false,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]'; 
		req.requestBody =Blob.valueof(JsonMsg);
		
		RestContext.request = req;		
		RestContext.response= res;      
        
		ID[] fixedSearchResults = new ID[6]; 
		fixedSearchResults[0] = con.id;
		fixedSearchResults[1] = con1.id;
		fixedSearchResults[2] = con2.id;
		fixedSearchResults[3] = con3.id;
		fixedSearchResults[4] = con4.id;
		fixedSearchResults[5] = con5.id; 
		Test.setFixedSearchResults(fixedSearchResults);

		Test.startTest();
		TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();
		}
	@isTest  (seeAllData=true)
    static void DedupeSearchPostTestMethod6() {
        
        Account newAcc = BaseController_Test.createTalentAccount('');
		List<Contact> conList = new List<Contact>();
		Contact con = BaseController_Test.createTalentContact(newAcc.Id);
		con.lastname = 'TestData';
        con.phone='100000001';
		conList.add(con);

		//con.linkedInUrl = 'test';
		Contact con1 = BaseController_Test.createTalentContact(newAcc.Id);
		con1.lastname = 'TestData';
        //con1.email = 'TestData@mail.com';
        con1.phone='100000001';
		//con1.Peoplesoft_ID__c = '10009' ;
		//con4.LastActivityDate = Date.today();				
		conList.add(con1);
		
		Contact con2 = BaseController_Test.createClientContact(newAcc.Id);
		con2.lastname = 'TestData';
        //con2.email = 'TestData1@test.com';
        con2.phone='100000001';
		con2.Peoplesoft_ID__c = '10008' ;		
		conList.add(con2);      
        
		update conList;

		List<Task> tasks = new List<Task>();
		tasks.add(new Task(
		ActivityDate = Date.today(),
		Subject='Sample Task',
		WhoId = con.Id,
		WhatId = newAcc.Id,
		OwnerId = UserInfo.getUserId(),
		Status='In Progress'));
		tasks.add(new Task(
		ActivityDate = Date.today().addDays(7),
		Subject='Sample Task',
		WhoId = con1.Id,
		WhatId = newAcc.Id,
		OwnerId = UserInfo.getUserId(),
		Status='Completed'));
		 
		insert tasks;

		tasks[0].ActivityDate = Date.today().addDays(2);
		tasks[1].ActivityDate = Date.today().addDays(2);
		update tasks;

        RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
        req.requestUri ='/services/apexrest/Person/dupeMatch/V2';
        req.httpMethod = 'POST';
        String JsonMsg = '[{"name" : "TestData","phone" : ["100000001"],"linkedInUrl" : "test", "returnCount" : "ONE","recordTypes" : ["Talent"],"IsAtsTalent" : false,"City" : ["testCity"],"State" : ["testState"],"Country" : ["testCountry"],"ignoreIs_ATS_Talent" : false}]'; 
		req.requestBody =Blob.valueof(JsonMsg);		

		RestContext.request = req;		
		RestContext.response= res;
        
        ID[] fixedSearchResults = new ID[3]; 
		fixedSearchResults[0] = con.id;
		fixedSearchResults[1] = con1.id;
		fixedSearchResults[2] = con2.id;
		Test.setFixedSearchResults(fixedSearchResults);

		Test.startTest();
		//TalentDedupeSearch_v2.getDedupTalents();
        TalentDedupeSearch_v2.postDedupTalents();
        Test.stopTest();
		}
}