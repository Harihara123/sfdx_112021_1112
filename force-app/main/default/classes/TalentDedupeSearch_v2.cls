@RestResource(urlMapping='/Person/dupeMatch/V2')
global without sharing class TalentDedupeSearch_v2 {

    public class JSONWrapper {

        public String name;
        public List<String> email;
        public List<String> phone;
        public String linkedInUrl;
        public String externalSourceId;
        public List<String> recordTypes;
        public String returnCount;
        public Boolean IsAtsTalent;
        public List<String> City;
        public List<String> State;
        public List<String> Country;
        public List<String> Zip;
        public Boolean ignoreIs_ATS_Talent;
        
        public List<JSONWrapper> parse(String json) {
            return (List<JSONWrapper>) System.JSON.deserialize(json, List<JSONWrapper>.class);
        }
    }


    @HttpGet
    global static void getDedupTalents() {

            
        try{

            Map<String, String> params = RestContext.request.params;
            String getStrParam = RestContext.request.params.get('Criteria');
            
            JSONWrapper obj = new JSONWrapper();
            List<JSONWrapper> objData = obj.parse(getStrParam);
            List<List<SObject>> res = TalentDedupeSearch_v2.retriveDuplicateTalents( objData[0].name, objData[0].email, objData[0].phone, objData[0].linkedInUrl, objData[0].externalSourceId, objData[0].recordTypes, objData[0].returnCount, objData[0].IsAtsTalent, objData[0].ignoreIs_ATS_Talent );

            RestContext.response.addHeader('Content-Type', 'application/json');

            if( res.isEmpty() || res[0].isEmpty() ){
                RestContext.response.responseBody = Blob.valueOf('[{"Message" : "No duplicates found"}]'); 
            }else{
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res[0]));
            }   

        }catch (Exception excep) {
            
            Log__c exceptionLog = new Log__c();
            exceptionLog = Core_Log.logException(excep);
            String jsonDescription =  '{ "message" :' + excep.getMessage() + ', "params" : ' + RestContext.request.params + '}';
            exceptionLog.Description__c = jsonDescription;           
            DateTime TodayDateTime = datetime.parse(system.now().format());         
            exceptionLog.Log_Date__c = TodayDateTime;
            exceptionLog.Class__c = 'TalentDedupeSearch_v2';
            exceptionLog.Related_Object__c = 'Contact';
            database.insert(exceptionLog);
        }     
        
    }

    @HttpPost
    global static void postDedupTalents() {

        RestResponse response = RestContext.response;
        
        try{

            String getStrParam  = RestContext.request.requestBody.toString();
           
            List<JSONWrapper> objData = (List<JSONWrapper>)JSON.deserialize(getStrParam, List<JSONWrapper>.class);
            List<List<SObject>> res = TalentDedupeSearch_v2.retriveDuplicateTalents( objData[0].name, objData[0].email, objData[0].phone, objData[0].linkedInUrl, objData[0].externalSourceId, objData[0].recordTypes, objData[0].returnCount, objData[0].IsAtsTalent, objData[0].ignoreIs_ATS_Talent );

            RestContext.response.addHeader('Content-Type', 'application/json');

            if( res.isEmpty() || res[0].isEmpty() ){
                RestContext.response.responseBody = Blob.valueOf('[{"Message" : "No duplicates found"}]'); 
            }else{
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res[0]));
            }   

        }catch (Exception excep) {

            Log__c exceptionLog = new Log__c();
            exceptionLog = Core_Log.logException(excep);
            String jsonDescription =  '{ "message" :' + excep.getMessage() + ', "params" : ' + RestContext.request.params + '}';
            exceptionLog.Description__c = jsonDescription;           
            DateTime TodayDateTime = datetime.parse(system.now().format());         
            exceptionLog.Log_Date__c = TodayDateTime;
            exceptionLog.Class__c = 'TalentDedupeSearch_v2';
            exceptionLog.Related_Object__c = 'Contact';
            database.insert(exceptionLog);
        }     


    }
    

    global static List<List<SObject>> retriveDuplicateTalents( String name, list<String> email, list<String> phone, String linkedInUrl, String externalSourceId, list<String> recordTypes, String returnCount, Boolean IsAtsTalent, Boolean ignoreIs_ATS_Talent ){
		
        list<id> recordTypeIds = new list<id>();
        
        if( recordTypes != null && !recordTypes.isEmpty() ){
            for( String recTypeName : recordTypes){
                if( Schema.SObjectType.contact.getRecordTypeInfosByName().get( recTypeName ) != null ){
                    recordTypeIds.add( Schema.SObjectType.contact.getRecordTypeInfosByName().get( recTypeName ).getRecordTypeId() );    
                }
            }
        }
        
        /* Return SOQL result from SOQL if externalSourceId is passed in params and externalSourceId has a match in SOQL */
        if(!String.isBlank( externalSourceId ) ){
            List<List<SObject>> lstContact = new List<List<SObject>>();
            list<contact> contact = [select Name, Id, email, Other_Email__c, Work_Email__c, HomePhone, phone, MobilePhone, OtherPhone, Accountid, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, Title, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Preferred_Name__c, Talent_Id__c, Account.Name  from Contact where externalSourceId__c =: externalSourceId limit 1];
            lstContact.add(contact);
           
            if( contact != null &&  !contact.isEmpty()) {
                return lstContact;
            }
        }
    	
        if( IsAtsTalent == null ){
            IsAtsTalent = true;
        }
		
        List<List<SObject>> results = new List<List<SObject>>();
        
        String phoneParam;
        String emailParam;

        //Start Neel S-107676 - Adding wildcard for name        
        String searchStr='';
        if(String.isNotBlank(name)) {
            for(String sString : name.split(' ')) {
                if(String.isNotBlank(searchStr)) {
                    searchStr = searchStr + ' ';
                }
                searchStr = searchStr + sString+ '*';
            }
            name = searchStr;
        }
        //End Neel S-107676 

        if(phone.size() == 1 ){
            phoneParam = phone[0];
        }else if(phone.size() > 1 ){
            phoneParam = convertListIntoCondition(phone);
        }
        
        if( email.size() == 1){
            emailParam = email[0];
        }else if(email.size() > 1 ){
            emailParam = convertListIntoCondition(email);
        }
        
        List<List<SObject>> nameSearch      = new List<List<SObject>>();
        List<List<SObject>> phoneSearch     = new List<List<SObject>>();
        List<List<SObject>> emailSearch     = new List<List<SObject>>();

        List<List<SObject>> returnResults   = new List<List<SObject>>();
     
        if( recordTypeIds.size() > 0 ){
             
            	if(! String.isEmpty(name )){
                    nameSearch  = [FIND :name IN NAME  FIELDS RETURNING Contact(  Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid, Current_Employer__c, Customer_Status__c, Do_Not_Contact__c,Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];  
                }
                
                if(! String.isEmpty(phoneParam )){
                    phoneSearch = [FIND :phoneParam IN PHONE FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid, Current_Employer__c, Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title, MailingCity,TC_Title__c, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];
                }
                
                if(! String.isEmpty(emailParam )){
                    emailSearch = [FIND :emailParam IN EMAIL FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid, Current_Employer__c,Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name WHERE RecordTypeId IN :recordTypeIds ) ];    
                }

        }else{
            
                if(! String.isEmpty(name )){
                    nameSearch  = [FIND :name IN NAME  FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, email, Talent_Id__c, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid,Current_Employer__c, Customer_Status__c, Do_Not_Contact__c,Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name  ) ];
                }
                if(! String.isEmpty(phoneParam )){
                    phoneSearch = [FIND :phoneParam IN PHONE FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid,Current_Employer__c,Customer_Status__c, Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name   ) ];
                }
                if(! String.isEmpty(emailParam )){
                    emailSearch = [FIND :emailParam IN EMAIL FIELDS RETURNING Contact( Name,TC_Name__c, Preferred_Name__c, Id, Talent_Id__c, email, Other_Email__c, Work_Email__c,TC_Email__c, HomePhone, phone, MobilePhone, OtherPhone,TC_Phone__c, Accountid,Current_Employer__c, Customer_Status__c,Do_Not_Contact__c, Record_Type_Name__c, Talent_Ownership__c, Peoplesoft_ID__c, Source_System_Id__c, LastActivityDate, CreatedDate, Createdby.Name, Mailing_Address_Formatted__c, Related_Account_Name__c, RecordTypeId, Account.Last_Activity_Date__c, Account.Candidate_Status__c, Account.Talent_Current_Employer_Formula__c, title,TC_Title__c, MailingCity, MailingState, Account.Talent_Profile_Last_Modified_Date__c, Related_Contact__c, Related_Contact__r.Name, Account.Name  ) ];    
                }
        }


        if( nameSearch.size() == 0 ){
            return new List<List<SObject>>();
        }

        if( phoneSearch.size() == 0 && emailSearch.size() == 0 ){
            return new List<List<SObject>>();
        }


        List<SObject> listSObj  = new List<SObject>();
        Contact contactObj      = new Contact();

        for(integer i=0; i<nameSearch[0].size(); i++){

            boolean addItemToReturn = false;
            boolean emailMatched    = false;

            if( emailSearch.size() > 0 ) {
                for(integer j=0; j<emailSearch[0].size(); j++){
                    if(nameSearch[0][i].id == emailSearch[0][j].id){
                        contactObj =  (Contact) nameSearch[0][i];

                        if( ( IsAtsTalent && contactObj.Record_Type_Name__c == 'Talent') || 
                            ( IsAtsTalent == false) ||
                            ( ignoreIs_ATS_Talent  == true && 
                             	(contactObj.Record_Type_Name__c == 'Talent' ||
                                 contactObj.Record_Type_Name__c == 'Reference' ||
                                 contactObj.Record_Type_Name__c == 'Client') )) {

                                addItemToReturn = true;
                                emailMatched    = true;
                        }

                        break;
                    }
       
                }   
            }

            /* If phone matched, No need to look for the email */ 
            if( phoneSearch.size() > 0 && emailMatched == false ){
        
                for(integer k=0; k<phoneSearch[0].size(); k++){
                    if(nameSearch[0][i].id == phoneSearch[0][k].id){
                        contactObj =  (Contact) nameSearch[0][i];

                        if( ( IsAtsTalent && contactObj.Record_Type_Name__c == 'Talent' ) ||
                            ( IsAtsTalent == false)  ||
                            ( ignoreIs_ATS_Talent  == true && 
                             	(contactObj.Record_Type_Name__c == 'Talent' ||
                                 contactObj.Record_Type_Name__c == 'Reference' ||
                                 contactObj.Record_Type_Name__c == 'Client') ))  {
                                addItemToReturn = true;
                        }
                        break;
                    }
                }

            }
            
            if(addItemToReturn){
                listSObj.add( nameSearch[0][i] );
            }

        }
        
        returnResults.add(listSObj);

        /* If only one record is availiblle - return it  */
        if(returnResults[0].size() == 1){
            return returnResults;
        }
        
        if( returnCount.touppercase() == 'ONE' && returnResults[0].size() > 0 ){
            List<SObject> lstMatchedContacts = new List<SObject>();
            datetime recentActivityForPS = null;
            datetime recentActivity      = null;
            datetime recentCreatedDate   = null;
            datetime recentCreatedDateForPs = null;
            //SObject contact = new SObject();
            
            boolean hasLastActivity = false;
            boolean hasPeoplesoft   = false;
            boolean haslastActForPS = false;
            
            for(integer i=0; i<returnResults[0].size(); i++){
                
                if(returnResults[0][i].get('Peoplesoft_ID__c') != null ){
                    hasPeoplesoft = true;
                    
                    /* Get Recent Activity Contact */
                    if( returnResults[0][i].get('LastActivityDate') != null ){
                        hasLastActivity = true;
                        if( recentActivity == null ){
                            lstMatchedContacts.clear();
                            recentActivity = date.valueOf( returnResults[0][i].get('LastActivityDate') );
                            lstMatchedContacts.add( returnResults[0][i] );
                        }else{
                            if( date.valueOf( returnResults[0][i].get('LastActivityDate') ) > recentActivity ){
                                lstMatchedContacts.clear();
                                recentActivity = date.valueOf( returnResults[0][i].get('LastActivityDate') );
                                lstMatchedContacts.add( returnResults[0][i] );
                            }
                        }
                    }
                    
                    /* If None of the record has Activity - Will go to get the newest record*/
                    if( !hasLastActivity ){
                        
                        if( recentCreatedDate == null ){
                            lstMatchedContacts.clear();
                            recentCreatedDate = datetime.valueOf( returnResults[0][i].get('CreatedDate') );
                            lstMatchedContacts.add( returnResults[0][i] );
                        }else{
                            if( datetime.valueOf( returnResults[0][i].get('CreatedDate') ) > recentCreatedDate ){
                                lstMatchedContacts.clear();
                                recentCreatedDate = datetime.valueOf( returnResults[0][i].get('CreatedDate') );
                                lstMatchedContacts.add( returnResults[0][i] );
                            }
                        }
                    }
                    
                }else if( !hasPeoplesoft ){
                    
                    /* Get Recent Activity Contact */
                    if( returnResults[0][i].get('LastActivityDate') != null ){
                        
                        haslastActForPS = true;
                        if( recentActivityForPS == null ){
                            
                            lstMatchedContacts.clear();
                            recentActivityForPS = date.valueOf( returnResults[0][i].get('LastActivityDate') );
                            lstMatchedContacts.add( returnResults[0][i] );
                        }else{
                            
                            if( date.valueOf( returnResults[0][i].get('LastActivityDate') ) > recentActivityForPS ){
                                
                                lstMatchedContacts.clear();
                                recentActivityForPS = date.valueOf( returnResults[0][i].get('LastActivityDate') );
                                lstMatchedContacts.add( returnResults[0][i] );
                            }
                        }
                    }
                    
                    /* If None of the record has Activity - Will go to get the newest record*/
                    if( !haslastActForPS ){
                        
                        if( recentCreatedDateForPs == null ){
                            lstMatchedContacts.clear();
                            recentCreatedDateForPs = datetime.valueOf( returnResults[0][i].get('CreatedDate') );
                            lstMatchedContacts.add( returnResults[0][i] );
                        }else{
                            if( datetime.valueOf( returnResults[0][i].get('CreatedDate') ) > recentCreatedDateForPs ){
                                lstMatchedContacts.clear();
                                recentCreatedDateForPs = datetime.valueOf( returnResults[0][i].get('CreatedDate') );
                                lstMatchedContacts.add( returnResults[0][i] );
                            }
                        }
                    }
                    
                }
                
            }
            
            If( !lstMatchedContacts.isEmpty() ){
                results.add(lstMatchedContacts);
                return results;
            }
            
        }else if(returnCount.touppercase() == 'MANY' && returnResults.size() > 0 ){
            return returnResults;
        }
       
        return new List<List<SObject>>();

    }


    /* This method rconverts the list of strings into OR condition */
    public static string convertListIntoCondition(List<String> lstString ){
        String[] tmp1 = New String[]{};
            
        for(String str : lstString){
            tmp1.add(str);
        }
    
        string concat = string.join(tmp1,' OR ');
        
        return concat;
        
    }
  
    
}