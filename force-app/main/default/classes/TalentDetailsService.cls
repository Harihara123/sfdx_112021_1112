@RestResource(urlMapping='/talent/getTalentDetails/*')
// URL: /services/apexrest/talent/getTalentDetails?contactId=0039E00000KMdsmQAD&includeResume=false&submittalId=80126000001qwIxAAI

global with sharing class TalentDetailsService  {

    @HttpGet
    global static void getTalentDetails() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String contactId = RestContext.request.params.get('contactId');
        String inclResume = RestContext.request.params.get('includeResume');
        String submittalId = RestContext.request.params.get('submittalId');
        String sourceSystemId = RestContext.request.params.get('sourceSystemId');
        Boolean resumeFlag = false;

        String response;
        if (!String.isBlank(inclResume) && inclResume == 'true') {
            resumeFlag = true;
        }
        System.Debug('sourceSystemId :' + sourceSystemId);
        System.Debug('contactId :' + contactId);
        System.Debug('includeResume :' + resumeFlag);

        try {
            if (!String.isBlank(contactId)) {
                JSONGenerator jsonGen = JSON.createGenerator(true);
                jsonGen.writeStartObject();
                jsonGen = getContactDetails (contactId, resumeFlag, jsonGen);
                if(!String.isBlank(submittalId)){
                    jsonGen = getOrderDetails (submittalId, jsonGen);
                }
                jsonGen.writeEndObject();

                res.statusCode = 200;
                res.responseBody = Blob.valueOf(jsonGen.getAsString());
                system.debug('=====Jsn======='+jsonGen);
            } else {
                response = 'Please pass contactId paramter. Parameter List (case-sensitive): contactid - Salesforce Contact Id, includeResume - return resume also';
                res.statusCode = 400;
                res.responseBody = Blob.valueOf(response);
            }

        } catch (Exception e) {
            response = 'Exception for the given input : ' + RestContext.request.params;
            response += e.getTypeName() + ': ' + e.getLineNumber() + ' - ' + e.getMessage() + '\r\n' + e.getStackTraceString();
            response += 'Please pass contactId paramter. Parameter List (case-sensitive): contactid - Salesforce Contact Id, includeResume - return resume also';
            res.statusCode = 400;
            res.responseBody = Blob.valueOf(response);
        }
    }

    private static JSONGenerator getContactDetails(String contactId, Boolean includeResume, JSONGenerator jsonGen) {
		List<Contact> contactList = new List<Contact>();
		if (contactId.startsWith('001')) {
			contactList = [select Id, name, FirstName, LastName, email, 
				Preferred_Phone_Value__c, Preferred_Email_Value__c, HomePhone, MobilePhone, title, accountId, MailingCity, MailingCountry, 
				MailingPostalCode, MailingState, MailingStreet, About_Me__c, LinkedIn_URL__c, Facebook_URL__c, 
				Twitter_URL__c,Preferred_Name__c,Website_URL__c, Account.Source_System_Id__c from contact where AccountId= :contactId LIMIT 1];
		} else {
			contactList = [select Id, name, FirstName, LastName, email, 
				Preferred_Phone_Value__c, Preferred_Email_Value__c, HomePhone, MobilePhone, title, accountId, MailingCity, MailingCountry, 
				MailingPostalCode, MailingState, MailingStreet, About_Me__c, LinkedIn_URL__c, Facebook_URL__c, 
				Twitter_URL__c,Preferred_Name__c,Website_URL__c, Account.Source_System_Id__c from contact where id= :contactId];
		}
		System.debug(contactList);
        if (contactList.size() == 1) {
            jsonGen.writeStringField('contactId', contactList[0].Id);
            jsonGen.writeStringField('firstName' , String.isBlank(contactList[0].FirstName)? '' : contactList[0].FirstName);
            jsonGen.writeStringField('lastName' , String.isBlank(contactList[0].LastName)? '' : contactList[0].LastName);
            jsonGen.writeStringField('primaryPhone' , String.isBlank(contactList[0].Preferred_Phone_Value__c)? '' : contactList[0].Preferred_Phone_Value__c);
            jsonGen.writeStringField('homePhone' , String.isBlank(contactList[0].HomePhone)? '' : contactList[0].HomePhone);
            jsonGen.writeStringField('mobilePhone' , String.isBlank(contactList[0].MobilePhone)? '' : contactList[0].MobilePhone);
            jsonGen.writeStringField('email' , String.isBlank(contactList[0].Preferred_Email_Value__c)? '' : contactList[0].Preferred_Email_Value__c);
            jsonGen.writeStringField('mailingCountry', String.isBlank(contactList[0].MailingCountry)? '' : contactList[0].MailingCountry);
            jsonGen.writeStringField('mailingState' , String.isBlank(contactList[0].MailingState)? '' : contactList[0].MailingState);
            jsonGen.writeStringField('mailingCity' , String.isBlank(contactList[0].MailingCity)? '' : contactList[0].MailingCity);
            jsonGen.writeStringField('mailingPostalCode' , String.isBlank(contactList[0].MailingPostalCode)? '' : contactList[0].MailingPostalCode);
            jsonGen.writeStringField('accountId' , contactList[0].AccountId);
			jsonGen.writeStringField('ContactSourceSystemId' , String.isBlank(contactList[0].Account.Source_System_Id__c)? '' : contactList[0].Account.Source_System_Id__c);
            jsonGen.writeStringField('linkedinUrl' , String.isBlank(contactList[0].LinkedIn_URL__c)? '' : contactList[0].LinkedIn_URL__c);
            if (includeResume) {
                jsonGen.writeStringField('resumeHtml', getResumeHtml(contactList[0].AccountId));
            }

        } else if (contactList.size() == 0) {
           System.debug('No Contact record found for talent = ' + contactId);
        } else {
            System.debug('More than one matching record found for talent = ' + contactId);
        }
        return jsonGen;

    }
    
    private static JSONGenerator getOrderDetails(String submittalId, JSONGenerator jsonGen) {

        List<Order>orderLst = [SELECT Id, Status, Opportunity.Req_origination_System_Id__c,OpportunityId, Source_System_Id__c, Opportunity.Req_Origination_Partner_System_Id__c FROM Order where Id =: submittalId];
        if (orderLst.size() == 1) {
            jsonGen.writeStringField('orderId', orderLst[0].Id);
			jsonGen.writeStringField('status', orderLst[0].Status);
			jsonGen.writeStringField('OrderSourceSystemId', String.isBlank(orderLst[0].Source_System_Id__c)? '' : orderLst[0].Source_System_Id__c);
            jsonGen.writeStringField('reqPartnerSystemId', String.isBlank(orderLst[0].Opportunity.Req_Origination_Partner_System_Id__c )? '' : orderLst[0].Opportunity.Req_Origination_Partner_System_Id__c );
            jsonGen.writeStringField('reqSourceSystemId', String.isBlank(orderLst[0].Opportunity.Req_origination_System_Id__c)? '' : orderLst[0].Opportunity.Req_origination_System_Id__c);
        } else if (orderLst.size() == 0) {
           System.debug('No Order record found for talent = ' + submittalId);
        } else {
            System.debug('More than one matching record found for talent = ' + submittalId);
        }
        return jsonGen;

    }
    
    private static String getResumeHtml(String talentId) {
        String resumeStr = '';
        Talent_Document__c resume = [
            SELECT  Id, Document_Type__c, Default_Document__c, LastModifiedDate, 
                (SELECT Id FROM Attachments) 
            FROM Talent_Document__c 
            WHERE  Document_Type__c = 'Resume' 
                AND HTML_Version_Available__c = true 
                AND Mark_For_Deletion__c = false 
                AND Committed_Document__c = true 
                AND Talent__c = :talentId
            ORDER BY Default_Document__c ASC, LastModifiedDate DESC
            LIMIT 1];
        
        if (resume != null) {
            Attachment att = [SELECT Id, Body FROM Attachment WHERE Id = :resume.Attachments[0].Id LIMIT 1];
            if (att != null) {
                resumeStr = att.Body.toString();
            }
        }
        return resumeStr;
    }
}