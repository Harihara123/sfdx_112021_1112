@IsTest
public class TalentDetailsService_Test {
    @TestSetup
    static void testSetup(){
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        Account newAcc = BaseController_Test.createTalentAccount('');
        Contact newCon = BaseController_Test.createTalentContact(newAcc.Id);
        insert new Order(ShipToContactId=newCon.Id , AccountId=newAcc.Id,EffectiveDate = date.today(),Status = 'Linked');
        Talent_Document__c talentDocument = new Talent_Document__c();
        talentDocument.Talent__c = newAcc.Id;
        talentDocument.Document_Name__c = 'test.txt';
        talentDocument.Document_Type__c = 'Resume'; 
        talentDocument.Default_Document__c = false;
        talentDocument.Internal_Document__c = true;
        talentDocument.Committed_Document__c = true;
        Database.insert(talentDocument);
    }
    @isTest static void test_getTalentDetails(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        Contact con =[Select Id From Contact LIMIT 1];
        Order submittal =[Select Id From Order LIMIT 1];
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('contactId', con.Id);
        req.addParameter('includeResume', 'false');
        req.addParameter('submittalId', submittal.Id);
        //res.statusCode=200;
        RestContext.request= req;
        RestContext.response= res;
        Test.startTest();
        TalentDetailsService.getTalentDetails();
        System.assert(res.statusCode == 200);
        Test.stopTest();
    }
    @isTest static void test_getTalentDetails2(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        Contact con =[Select Id From Contact LIMIT 1];
        Order submittal =[Select Id From Order LIMIT 1];
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('contactId', con.Id);
        req.addParameter('includeResume', 'true');
        req.addParameter('submittalId', submittal.Id);
        //res.statusCode=200;
        RestContext.request= req;
        RestContext.response= res;
        Test.startTest();
        TalentDetailsService.getTalentDetails();
        //System.assert(res.statusCode == 200);
        Test.stopTest();
    }
     @isTest static void test_getTalentDetails3(){
    DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('contactId', null);
        //res.statusCode=200;
        RestContext.request= req;
        RestContext.response= res;
        Test.startTest();
        TalentDetailsService.getTalentDetails();
        //System.assert(res.statusCode == 200);
        Test.stopTest();
    }

}