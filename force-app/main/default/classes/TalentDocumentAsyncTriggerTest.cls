@isTest
public class TalentDocumentAsyncTriggerTest {
    @isTest
    public static void test() {
        
        insert new CDC_Data_Export_Flow__c(Name = 'DataExport', Change_Event_Topic_Name__c = 'topicx',  Data_Export_All_Fields__c = false, Sobject_Topic_Name__c = 'ingest10x');
        Test.startTest();
        Test.enableChangeDataCapture();
        Account acc = TestData.newAccount(1, 'Talent');
        insert acc;
        Contact con = TestData.newContact(acc.Id, 1, 'Talent');
        insert con;
        
        Talent_Document__c TD = new Talent_Document__c(Talent__c = acc.Id, Mark_For_Deletion__c = false, Document_Type__c = 'Social Profile');
        insert TD;
        Test.getEventBus().deliver();
        List<Task> taskList = new List<Task>();
        taskList = [SELECT Id,Subject FROM Task limit 1]; 
        Test.stopTest();
        System.assertEquals(1, taskList.size(), 'The change event trigger did not create the expected task.');        
    }
}