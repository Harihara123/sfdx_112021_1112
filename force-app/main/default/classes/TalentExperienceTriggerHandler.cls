/*******************************************************************
Name        : TalentExperienceTriggerHandler
Created By  : Ajay Panachickal
Date        : 6/8/2016
Story/Task  : ATS-1288
Purpose     : Class that contains all of the functionality called by the
              TalentExperienceTrigger. All contexts should be in this class.
Modified BY : Ajay Panachickal
Date        : 05/17/2017
Story/Task  : ATS-3909

********************************************************************/
public without sharing class TalentExperienceTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, Talent_Experience__c> oldMap = new Map<Id, Talent_Experience__c>();
    Map<Id, Talent_Experience__c> newMap = new Map<Id, Talent_Experience__c>();

    private static boolean hasBeenProccessed = false;

    
    //-------------------------------------------------------------------------
    // Boolean Check Method to Avoid Recursion on After Update Event - Akshay ATS - 2819
    //-------------------------------------------------------------------------
    @TestVisible
    private static boolean run = true;
        public static boolean runOnce(){
            if(run){
                run=false;
                return true;
            }else{
                return run;
            }
        }

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Talent_Experience__c> newRecs) {
		trg_Update_personaIndicator(beforeInsert, newRecs);
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Talent_Experience__c> newRecs) {
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration ||
            ((UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration) && UserInfo.getName() == 'EMEA Integration')) {
            trg_Update_Talent_LastModified(afterInsert, oldMap, newRecs);
        }

        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentExperienceTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + newRecs.size() + ' Talent_Experience__c records: ' + CDCDataExportUtility.joinObjIds(newRecs));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'Talent_Experience__c');
            hasBeenProccessed = true; 
        }
        
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Talent_Experience__c>oldMap, Map<Id, Talent_Experience__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Talent_Experience__c>oldMap, Map<Id, Talent_Experience__c>newMap) {
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration ||
            ((UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration) && UserInfo.getName() == 'EMEA Integration')) {
            List<Talent_Experience__c> newRecs = newMap.values();
            trg_Update_Talent_LastModified(afterUpdate, oldMap, newRecs);
        }
         
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentExperienceTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newMap.values().size() + ' Talent_Experience__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'Talent_Experience__c');
            hasBeenProccessed = true; 
       }

      
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Talent_Experience__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Talent_Experience__c>oldMap) {
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            List<Talent_Experience__c> newRecs = new List<Talent_Experience__c>();
            trg_Update_Talent_LastModified(afterDelete, oldMap, newRecs);
        }

        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentExperienceTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Talent_Experience__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Talent_Experience__c');
            hasBeenProccessed = true; 
       }
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, Talent_Experience__c>oldMap) {
    }

   /***************************************************************************************************************************************
    * Description - Method used after insert / update / delete to update the TalentLastModified By And Date 
    *               when Talent_Experience__c is inserted / updated / deleted.
    * 
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Ajay Panachickal            6/8/2016               Created
    * Akshay Konijeti            12/20/2016              Modified for ATS-2725
    * Akshay Konijeti            01/16/2017              Modified for ATS-3062
    * Akshay Konijeti            02/01/2017              Modified for ATS-3176
    * Ajay Panachickal           05/17/2017              ATS-3909

    *****************************************************************************************************************************************/
    public void trg_Update_Talent_LastModified (String Context, Map<Id, Talent_Experience__c>oldMap, List<Talent_Experience__c>TriggerNew) {
        if(TalentExperienceTriggerHandler.runOnce()){
            Set<Id> setAccountids = new Set<Id>();
            Set<Id> TErecsCA = new Set<Id>();  //added for ATS-3062 by akshay getting the Ids of TriggerNew records with CurrentAssignment True
            Set<Id> TalentrecsCA = new Set<Id>(); //added by akshay for ATS-3176   
            List<Account> accountsToUpdate = new List<Account>();
            List<Talent_Experience__c> TEToUpdateCA = new List<Talent_Experience__c>();
            
            if (!Context.equals(afterDelete)) {
                for (Talent_Experience__c talentExp : TriggerNew) {
                    setAccountids.add(talentExp.Talent__c);
                }
            } else {
                List<Talent_Experience__c> deletedTalentExps = oldMap.values();
                for (Talent_Experience__c talentExp : deletedTalentExps) {
                    setAccountids.add(talentExp.Talent__c);
                }
            }
            
            // Get 'Talent' accounts for the related events.
            String queryAccounts = 'SELECT Id, Talent_Profile_Last_Modified_Date__c, Talent_Profile_Last_Modified_By__c,Current_Employer__c,Talent_Current_Employer_Formula__c,Talent_Current_Employer_Text__c, Talent_Ownership__c from Account where RecordType.Name = \'Talent\' and Id IN :setAccountids'; //  fields Current_Employer__c,Talent_Current_Employer_Formula__c,Talent_Current_Text__c added by akshay ATS-2725
            Map<Id, Account> accountMap = new Map<Id, Account>((List<Account>)Database.query(queryAccounts));
            
            /* code added by akshay for ATS-2725 start 12/20/2016 
             * This code is bulkified, if there are more than one Talent Experience records in an upload with current employer checked 
             * and with the same Parent Talent Account, the last entry will be the value on the Talent Account Current employer field.
             */

            //handling for after insert start
            if(Context.equals(afterInsert)){
                for (Talent_Experience__c talentExp : TriggerNew) {
                    
                    if(talentExp.Current_Assignment__c ){ 
                        Account act = accountMap.get(talentExp.Talent__c); 
                        TErecsCA.add(talentExp.Id);//added for ATS-3062 akshay
                        TalentrecsCA.add(talentExp.Talent__c);//added by akshay for ATS-3176
                        if(talentExp.Client__c != NULL)
                            act.Current_Employer__c = talentExp.Client__c;
                        else
                            act.Current_Employer__c = NULL;
                        
                        if(String.isNotBlank( talentExp.Organization_Name__c))
                            act.Talent_Current_Employer_Text__c = talentExp.Organization_Name__c;

						if (act.Talent_Ownership__c == 'MLA' 
							|| act.Talent_Ownership__c == 'RPO' 
							|| act.Talent_Ownership__c == 'AP')
						{	
							if(talentExp.Allegis_Placement__c && talentExp.Current_Assignment__c) { //added by akshay for S - 55186
								act.Candidate_Status__c = 'Placed';
							} else {
								act.Candidate_Status__c = 'Candidate'; 
							}	
						}

                    }
                }
            }
            //handling for after insert end
            
            
            //handling for after update start
            if(Context.equals(afterUpdate)){
                for (Talent_Experience__c talentExp : TriggerNew) {
                    Talent_Experience__c talentOld = OldMap.get(talentExp.id);
                    Account act = accountMap.get(talentExp.Talent__c);
                    if((talentExp.Current_Assignment__c <> talentOld.Current_Assignment__c) || (talentExp.Client__c <> talentOld.Client__c) || (talentExp.Organization_Name__c <> talentOld.Organization_Name__c && (talentExp.Current_Assignment__c)) ){//added Client__c condition by akshay for prod defect 12/06
                        
                        TErecsCA.add(talentExp.Id);//added for ATS-3062 akshay
                        TalentrecsCA.add(talentExp.Talent__c);//added by akshay for ATS-3176
                        
                        if(talentExp.Client__c != NULL && talentExp.Current_Assignment__c)
                            act.Current_Employer__c = talentExp.Client__c;
                        else
                            act.Current_Employer__c = NULL;
                        
                        if(String.isNotBlank( talentExp.Organization_Name__c) && talentExp.Current_Assignment__c)
                            act.Talent_Current_Employer_Text__c = talentExp.Organization_Name__c;
                        else
                            act.Talent_Current_Employer_Text__c = NULL; 
 
                    }

					if (act.Talent_Ownership__c == 'MLA' 
						|| act.Talent_Ownership__c == 'RPO' 
						|| act.Talent_Ownership__c == 'AP')
					{	
						if(talentExp.Allegis_Placement__c && talentExp.Current_Assignment__c) { //added by akshay for S - 55186
							act.Candidate_Status__c = 'Placed';
						} else {
							act.Candidate_Status__c = 'Candidate'; 
						}	
					}
                } 
            }
            //handling after update end

            //handling for after delete start
            if (Context.equals(afterDelete)) {
                for (Talent_Experience__c talentExp : OldMap.values()) {
                    // Remove Account current employer values ONLY if deleting the current employment record.
                    if (talentExp.Current_Assignment__c) {
                        Account act = accountMap.get(talentExp.Talent__c);
                        act.Current_Employer__c = NULL;
                        act.Talent_Current_Employer_Text__c = NULL;

                        if(talentExp.Allegis_Placement__c)//added by akshay for S - 55186
                          act.Candidate_Status__c = 'Candidate';
                    }
                }
            }
            //handling after delete end     
            
            /* code added by akshay for ATS-2725 end 12/20/2016 */
            for (Account act : accountMap.values()) {
                act.Talent_Profile_Last_Modified_Date__c = System.now();
                act.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();
                // System.debug(logginglevel.warn, 'Accounts to update Akshay:'+act);
                accountsToUpdate.add(act);
            }
            
            if (accountsToUpdate != Null && accountsToUpdate.size() > 0) {
                update accountsToUpdate;
            }

            /* code added by akshay for ATS-3062 start 01/16/2017 */
            if (!Context.equals(afterDelete)) {
                if(!TalentrecsCA.isEmpty()){//added by akshay for ATS-3176
                    TeToUpdateCA = [Select Id,Talent__c from Talent_Experience__c Where Talent__c IN: TalentrecsCA AND Id Not IN : TErecsCA AND Current_Assignment__c = true  ];
                    System.debug(Logginglevel.warn, 'Akshay Remove Current Assignment recs:'+TeToUpdateCA);
                    
                    if(!TeToUpdateCA.isEmpty()){
                        for(Talent_experience__c TE : TeToUpdateCA){
                            TE.Current_Assignment__c = false;
                        }
                        
                        Database.update(TeToUpdateCA, false);
                    }
                }
            }
            /* code added by akshay for ATS-3062 end 01/16/2017 */
        }
    }
	
	public void trg_Update_personaIndicator (String Context, List<Talent_Experience__c>TriggerNew) {
        for (Talent_Experience__c talExp: TriggerNew) {
			if(Context.equals(beforeInsert)){
				if(UserInfo.getUserType() == 'CSPLitePortal' ){
					 talExp.personaIndicator__c = 'SelfService';
				}else{
					 talExp.personaIndicator__c = 'Recruiter';
				}
			}		
		}
	}

}