public class TalentFirstListController {
    
    @AuraEnabled
    public static List<TalentFirstOpp> getListViewData(String contactId) {
      
        List<TalentFirstOpp> tfoList = new List<TalentFirstOpp>();

        For(Opportunity opp : [SELECT Id, Name, OwnerId, Owner.Name, Req_Job_Title__c, Organization_Office__c, Organization_Office__r.Name, 
                                    Division__c, StageName, CloseDate, CreatedDate, Req_Work_Remote__c 
                                FROM Opportunity WHERE AccountId IN 
                                    (SELECT AccountId FROM Contact WHERE Id =: contactId) AND RecordType.Name = 'Talent First Opportunity']
        ) {
            TalentFirstOpp tfo = new TalentFirstOpp();
            tfo.id = opp.Id;
            tfo.oppLink = opp.Id;  // will be updated on front end to be link
            tfo.oppName = opp.Name;
            tfo.ownerLink = opp.OwnerId;
            tfo.ownerName = opp.Owner.Name;
            tfo.jobTitle = opp.Req_Job_Title__c;
            tfo.officeLink = opp.Organization_Office__c;
            tfo.officeName = opp.Organization_Office__r.Name;
            tfo.division = opp.Division__c;
            tfo.stage = opp.StageName;
            tfo.closeDate = opp.CloseDate;
            tfo.createdDate = opp.CreatedDate;
            tfo.remoteWork = opp.Req_Work_Remote__c;

            tfoList.add(tfo);
        }

        return tfoList;       
    }

    public Class TalentFirstOpp {
        @AuraEnabled public String id;
        @AuraEnabled public String oppLink;
        @AuraEnabled public String oppName;
        @AuraEnabled public String ownerLink;
        @AuraEnabled public String ownerName;
        @AuraEnabled public String jobTitle;
        @AuraEnabled public String officeLink;
        @AuraEnabled public String officeName;
        @AuraEnabled public String division;
        @AuraEnabled public String stage;
        @AuraEnabled public Date closeDate;
        @AuraEnabled public DateTime createdDate;
        @AuraEnabled public String remoteWork;
    }  
}