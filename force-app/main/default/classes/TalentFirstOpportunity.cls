public class TalentFirstOpportunity {
	@AuraEnabled
    public static List<Id> PerformSFServerCall(String methodName, Map<String, Object> parameters){
        try{
            List<Id> OppIdList=null;
        	if(methodName == 'CreateTalentFirstOpportunity'){
				list<string> drzBoardId = new list<string>();
				Id contId=(Id)parameters.get('ContactId');
            	Map<Id, String> ContactMap=new Map<Id, String>();
            	ContactMap.put((Id)parameters.get('ContactId'),'ContactId');
				if(parameters.containskey('DRZTFOBoardId')){
                    string x = string.valueOf(parameters.get('DRZTFOBoardId')).removeStart('(').removeEnd(')');
                    drzBoardId = x.split(',');
                }
				OppIdList=CreateTalentFirstOpportunity(ContactMap,drzBoardId);
			}
        	return OppIdList;
        }catch(Exception excp){
            ConnectedLog.LogException('TalentFirstOpportunity','PerformSFServerCall',excp);
            return null;
        }        
	}	
    @AuraEnabled
    public static List<Id> CreateTalentFirstOpportunity(Map<Id, String> ContactMap,list<string> drzBoardId){
		try{
            List<Id> TalentFirstOppIdList = new List<Id>();
			if(ContactMap != null && ContactMap.size()>0){                
                List<Opportunity> TalentFirstOpportunityList = new List<Opportunity>();            
                Id TalentFirstRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Talent First Opportunity').getRecordTypeId();
                USER usr = [select division__c ,office__c,OPCO__c,CompanyName from USER where ID=:UserInfo.getUserId() WITH SECURITY_ENFORCED];
                List<User_Organization__c> uo = [Select id,Office_Name__c,name from User_Organization__c where name =: usr.office__c WITH SECURITY_ENFORCED];
                List<Contact> ContactList=[Select id, AccountId, Title, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Account.Skills__c, Account.Skill_Comments__c, Account.Goals_and_Interests__c, Account.Desired_Rate__c, Account.Desired_Rate_Max__c, Account.Desired_Salary__c, Account.Desired_Salary_Max__c, Account.Desired_Currency__c, Account.Desired_Rate_Frequency__c from Contact where id in :ContactMap.Keyset() WITH SECURITY_ENFORCED];
                for(Contact cont: ContactList){
                    Opportunity TalentFirstOpportunity= new Opportunity();
                    TalentFirstOpportunity.RecordTypeId= TalentFirstRecordTypeId;
                    TalentFirstOpportunity.StageName= 'Open';
                    //TalentFirstOpportunity.Req_Opportunity_Owner__c= UserInfo.getUserId();
                    TalentFirstOpportunity.OwnerId= UserInfo.getUserId();
                    TalentFirstOpportunity.AccountId= cont.AccountId;
                    TalentFirstOpportunity.Req_Job_Title__c= cont.Title;
                    TalentFirstOpportunity.Req_Worksite_Street__c= cont.MailingStreet;
                    TalentFirstOpportunity.Req_Worksite_City__c= cont.MailingCity;
                    TalentFirstOpportunity.Req_Worksite_State__c= cont.MailingState;
                    TalentFirstOpportunity.Req_Worksite_Country__c= cont.MailingCountry;
                    TalentFirstOpportunity.Req_Worksite_Postal_Code__c= cont.MailingPostalCode;               
                    TalentFirstOpportunity.EnterpriseReqSkills__c= cont.Account.Skills__c;
                    TalentFirstOpportunity.Req_Qualification__c= cont.Account.Skill_Comments__c;
                    TalentFirstOpportunity.Description= cont.Account.Goals_and_Interests__c;
                    TalentFirstOpportunity.Req_Pay_Rate_Min__c= cont.Account.Desired_Rate__c;
                    TalentFirstOpportunity.Req_Pay_Rate_Max__c= cont.Account.Desired_Rate_Max__c;
                    TalentFirstOpportunity.Req_Salary_Min__c= cont.Account.Desired_Salary__c;
                    TalentFirstOpportunity.Req_Salary_Max__c= cont.Account.Desired_Salary_Max__c;
                    TalentFirstOpportunity.Currency__c= cont.Account.Desired_Currency__c;
                    TalentFirstOpportunity.Req_Rate_Frequency__c= cont.Account.Desired_Rate_Frequency__c;
                    TalentFirstOpportunity.OPCO__c = usr.CompanyName;
                    TalentFirstOpportunity.CloseDate=Date.today()+30;
                    TalentFirstOpportunity.Name='Talent First Opportunity - ' +DateTime.now();
                    TalentFirstOpportunity.Division__c = usr.division__c;  
                    if(!uo.isEmpty()){
                        TalentFirstOpportunity.Organization_Office__c = uo[0].id; 
                    }
                    TalentFirstOpportunityList.add(TalentFirstOpportunity);				
                }
                if(TalentFirstOpportunityList.size() > 0){
                    Database.SaveResult[] TalentFirstOpporResults=Database.Insert(TalentFirstOpportunityList,false);
                    for(Database.SaveResult OppResults: TalentFirstOpporResults){
                        if(!OppResults.isSuccess()){
                            for (Database.Error e : OppResults.getErrors()){ 
                                TalentFirstOppIdList=null;
                            }
                        }else{
                            TalentFirstOppIdList.add(OppResults.getId());
							if(drzBoardId !=null && !drzBoardId.isEmpty()){
                                Callable extension = (Callable) Type.forName('DRZ_ConnectedBridge').newInstance();
                                Object metaDataCall = extension.call('getDRZMetaData', new Map<String, Object>());
                                system.debug('-metaDataCall-'+metaDataCall);                                
                                //Callable extension = (Callable) Type.forName('DRZ_ConnectedBridge').newInstance();
                                if(metaDataCall !=null){
                                    SFDRZ_Metadata__mdt O = (SFDRZ_Metadata__mdt)metaDataCall;
                                    if(O.IsActive__c == True)
                                    Object result = extension.call('createTalentFirstOpportunityOnDRZBoards', new Map<String, Object> {'Opportunity' => OppResults.getId(),'BoardIds' => drzBoardId});
                                }
                            }
                        }
                    }                    
                }else{
                        TalentFirstOppIdList=null;
                }                
        	}
        	return TalentFirstOppIdList;
    	}Catch(Exception excp){
            ConnectedLog.LogException('TalentFirstOpportunity','CreateTalentFirstOpportunity',excp);
    		return null;
    	}
    }   
    @AuraEnabled
    public static List<OpportunityContactRole> getContactRoles(String recordId) {        
        List<OpportunityContactRole> contactRoles = new List<OpportunityContactRole>();
        try {
            contactRoles = [SELECT Id, ContactId, Contact.Name, Contact.AccountId, Contact.Account.Name, 
                            Contact.Account.Account_Street__c, Contact.Account.Account_City__c, 
                            Contact.Account.Account_Country__c, Contact.Account.Account_Zip__c, 
                            Contact.Account.Account_State__c
                            From OpportunityContactRole 
                            WHERE OpportunityId=:recordId WITH SECURITY_ENFORCED];
            return contactRoles;
        } catch(Exception ex) {
            throw new AuraHandledException('Something went wrong: ' + ex.getMessage());
        }
    }
    
    @AuraEnabled(cacheable=true)
    public static Contact matchReq(String recordId) {
        try {
        	Opportunity opp = [Select Id, AccountId From Opportunity Where Id=: recordId  WITH SECURITY_ENFORCED LIMIT 1];
        	return [Select Id, AccountId, Name From Contact Where AccountId=:opp.AccountId  WITH SECURITY_ENFORCED LIMIT 1];
        }
        catch(Exception ex) {
            throw new AuraHandledException('Something went wrong: ' + ex.getMessage());
        }
    }
}