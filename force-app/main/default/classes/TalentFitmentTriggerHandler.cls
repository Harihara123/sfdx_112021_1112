public class TalentFitmentTriggerHandler  {

	String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, TalentFitment__c> oldMap = new Map<Id, TalentFitment__c>();
    Map<Id, TalentFitment__c> newMap = new Map<Id, TalentFitment__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<TalentFitment__c> newRecs) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<TalentFitment__c> newRecs) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
		if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentFitmentTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + newRecs.size() + ' TalentFitment__c records: ' + CDCDataExportUtility.joinObjIds(newRecs));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'TalentFitment__c');
        }
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, TalentFitment__c>oldMap, Map<Id, TalentFitment__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, TalentFitment__c>oldMap, Map<Id, TalentFitment__c>newMap) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentFitmentTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newMap.values().size() + ' TalentFitment__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'TalentFitment__c');
        }
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, TalentFitment__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, TalentFitment__c>oldMap) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentFitmentTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' TalentFitment__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'TalentFitment__c');
        }
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, TalentFitment__c>oldMap) {
    }
}