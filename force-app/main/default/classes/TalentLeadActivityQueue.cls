public without sharing class TalentLeadActivityQueue implements Queueable {

	public TalentWrapper paramObject;
	String contactId = '';

	public TalentLeadActivityQueue(TalentWrapper paramObject, String contactId) {
		this.paramObject = paramObject;
		this.contactId = contactId;
	}
	public void execute(QueueableContext QC) {
		try {
			List<String> postingIds = new List<String> ();
			for (TalentWrapper.postingsVisited posting : paramObject.TalentLeadActivity.postingsVisited) {
				if (String.isNotBlank(posting.postingId)) {
					postingIds.add(posting.postingId);
				}
			}
			List<Job_Posting__c> joblostinglist = new List<Job_Posting__c> ();
			joblostinglist = [select id, name from Job_Posting__c where Name = :postingIds WITH SECURITY_ENFORCED];

			Map<String, String> postingNameMap = new Map<String, String> ();

			for (Job_Posting__c jp : joblostinglist) {
				postingNameMap.put(jp.name, jp.id);
			}
			List<Talent_Lead_Activity__c> tldList = new list<Talent_Lead_Activity__c> ();
			for (TalentWrapper.postingsVisited pv : paramObject.TalentLeadActivity.postingsVisited) {
				Talent_Lead_Activity__c tld = new Talent_Lead_Activity__c();
				tld.Contact__c = contactId;
				tld.Activity_Date__c = (DateTime)JSON.deserialize('"' + pv.activityDate + '"', DateTime.class);
				tld.Job_Posting__c = postingNameMap.get(pv.postingId);
				tld.Lead_Created_Date__c = (DateTime)JSON.deserialize('"' + paramObject.TalentLeadActivity.leadCreatedDate + '"', DateTime.class);
				tld.Lead_Updated_Date__c = (DateTime)JSON.deserialize('"' +paramObject.TalentLeadActivity.leadUpdatedDate + '"', DateTime.class);
				if (String.isNotBlank(pv.postingId)){
					tld.UniqueID__c = contactId;
				}
				else {
					tld.UniqueID__c = contactId + postingNameMap.get(pv.postingId);
				}

				tldList.add(tld);
			}

			Database.UpsertResult[] saveResultList = Database.upsert(tldList, Talent_Lead_Activity__c.UniqueID__c, false);

		}
        catch(DMLException dexp) {
			ConnectedLog.LogException('Candidate/TalentLeadActivity','TalentLeadActivityQueue','execute', dexp);
			throw new AuraHandledException(Label.Error);
		}
		catch(Exception e) {
			ConnectedLog.LogException('Candidate/TalentLeadActivity','TalentLeadActivityQueue','execute', e);
			throw new AuraHandledException(Label.Error);
		}

	}

}