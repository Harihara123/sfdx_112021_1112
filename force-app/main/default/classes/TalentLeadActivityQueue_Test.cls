@isTest
public class TalentLeadActivityQueue_Test {
    
    @TestSetup
    static void TestDataSetup() {
        JobPostingTestDataFactory.northAmericaTestDataSetup();
    }
    
    @isTest public static void testFetchJobPostingRecord() {
        
        Contact con=[select id,name from Contact LIMIT 1];
        Job_Posting__c posting=[select id,name from Job_Posting__c LIMIT 1];
        String postingname= posting.name;
        String requestJSONBody = '{'+
            '"sourceTracking":{'+
            '"source":"PhenomCRM",'+
            '"vendor":"8",'+
            '"transactionId":"AB1234Z",'+
            '"externalCandidateId":"SBx213"'+
            '},'+
            '"talentLeadActivity":{'+
            '"postingsVisited":['+
            '{'+
            '"postingId":"'+postingname+'",'+
            '"activityDate":"2020-07-07T14:58:15.000Z"'+
            ' }'+
            '],'+
            '"leadCreatedDate":"2020-07-07T14:58:15.000Z",'+
            '"leadUpdatedDate":"2020-07-07T14:58:15.000Z"'+
            '},'+
            '"recruiter":{'+
            '"firstName":"Praveen",'+
            '"lastName":"Chinnappa",'+
            '"email":"pchinnappa@testmail.com"'+
            '},'+
            '"talent":{'+
            '"profile":{'+
            '"firstName":"TestFirstName",'+
            '"lastName":"TestLastName",'+
            '"jobTitle":"JavaDeveloper",'+
            '"desiredSalary":"$100000",'+
            '"desiredRate":"$50",'+
            '"desiredFrequency":"Daily",'+
            '"workEligibility":"I have the right to work for any employer",'+
            '"geographicPreference":"Within France",'+
            '"highestEducation":"",'+
            '"securityClearance":"Confidential Or R"'+
            '},'+
            '"contact":{'+
            '"phone":{'+
            '"mobile":"+43123421 54",'+
            '"work":"112 4324 6543",'+
            '"home":"",'+
            '"unspecified":["1223212233","152431233"]'+
            '},'+
            '"email":{'+
            '"primary":"",'+
            '"work":"",'+
            '"other":"",'+
            '"unspecified":["testa@testmail.com","teat123@teatmail.com"]}'+
            '},'+
            '"address":{'+
            '"streetAddress":"",'+
            '"city":"",'+
            '"stateProvince":"",'+
            '"country":"GB",'+
            '"postalCode":"CB123"'+
            '},'+
            '"resume":{'+
            '"fileType":"pdf",'+
            '"base64":"aswsdf"'+
            '},'+
            '"attachment":{'+
            '"fileType":"doc",'+
            '"base64":"awesdf"'+
            '}}}';
        TalentWrapper wrapObject = new TalentWrapper();
        wrapObject=TalentWrapper.parse(requestJSONBody);
        TalentLeadActivityQueue talentlead=new TalentLeadActivityQueue(wrapObject,String.valueof(con.id));
        Test.startTest();
        System.enqueueJob(talentlead);
        Test.stopTest();
        Talent_Lead_Activity__c lead=[Select id,name,Lead_Updated_Date__c from Talent_Lead_Activity__c];
        System.assertEquals('2020-07-07 10:58:15', String.valueof(lead.Lead_Updated_Date__c));
        
    }
      @isTest public static void testFetchJobPostingRecordwithnoJP() {
        
        Contact con=[select id,name from Contact LIMIT 1];
        Job_Posting__c posting=[select id,name from Job_Posting__c LIMIT 1];
        String postingname= posting.name;
        String requestJSONBody = '{'+
            '"sourceTracking":{'+
            '"source":"PhenomCRM",'+
            '"vendor":"8",'+
            '"transactionId":"AB1234Z",'+
            '"externalCandidateId":"SBx213"'+
            '},'+
            '"talentLeadActivity":{'+
            '"postingsVisited":['+
            '{'+
            '"postingId":"",'+
            '"activityDate":"2020-07-07T14:58:15.000Z"'+
            ' }'+
            '],'+
            '"leadCreatedDate":"2020-07-07T14:58:15.000Z",'+
            '"leadUpdatedDate":"2020-07-07T14:58:15.000Z"'+
            '},'+
            '"recruiter":{'+
            '"firstName":"Praveen",'+
            '"lastName":"Chinnappa",'+
            '"email":"pchinnappa@testmail.com"'+
            '},'+
            '"talent":{'+
            '"profile":{'+
            '"firstName":"TestFirstName",'+
            '"lastName":"TestLastName",'+
            '"jobTitle":"JavaDeveloper",'+
            '"desiredSalary":"$100000",'+
            '"desiredRate":"$50",'+
            '"desiredFrequency":"Daily",'+
            '"workEligibility":"I have the right to work for any employer",'+
            '"geographicPreference":"Within France",'+
            '"highestEducation":"",'+
            '"securityClearance":"Confidential Or R"'+
            '},'+
            '"contact":{'+
            '"phone":{'+
            '"mobile":"+43123421 54",'+
            '"work":"112 4324 6543",'+
            '"home":"",'+
            '"unspecified":["1223212233","152431233"]'+
            '},'+
            '"email":{'+
            '"primary":"",'+
            '"work":"",'+
            '"other":"",'+
            '"unspecified":["testa@testmail.com","teat123@teatmail.com"]}'+
            '},'+
            '"address":{'+
            '"streetAddress":"",'+
            '"city":"",'+
            '"stateProvince":"",'+
            '"country":"GB",'+
            '"postalCode":"CB123"'+
            '},'+
            '"resume":{'+
            '"fileType":"pdf",'+
            '"base64":"aswsdf"'+
            '},'+
            '"attachment":{'+
            '"fileType":"doc",'+
            '"base64":"awesdf"'+
            '}}}';
        TalentWrapper wrapObject = new TalentWrapper();
        wrapObject=TalentWrapper.parse(requestJSONBody);
        TalentLeadActivityQueue talentlead=new TalentLeadActivityQueue(wrapObject,String.valueof(con.id));
        Test.startTest();
        System.enqueueJob(talentlead);     
        Test.stopTest();
        Talent_Lead_Activity__c lead=[Select id,name,Lead_Updated_Date__c from Talent_Lead_Activity__c];
        System.assertEquals('2020-07-07 10:58:15', String.valueof(lead.Lead_Updated_Date__c));
        
    }
      
}