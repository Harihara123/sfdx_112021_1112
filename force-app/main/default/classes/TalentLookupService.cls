@RestResource(urlMapping='/talent/lookupTalentDetails/*')
global class TalentLookupService {
    @HttpPost
    global static void getTalentDetails() {
        // Commment
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        ContactDetailsParser parser,parsedData,responseData;
        String response; 
        
        try {
            String jsonInput  = req.requestBody.toString();
            parser = new ContactDetailsParser();
            parsedData = parser.parse(jsonInput);
            
            List<String> notFoundIds = new List<String>();
            //List<String> psIds = new List<String>();
            Set<String> rwsIds = new Set<String>();
            Set<String> psIds = new Set<String>();
			Set<String> mdmIds = new Set<String>();
            Set<String> invalidIds = new Set<String>();
			Map<String, Boolean> idFound = new Map<String, Boolean>();

			responseData = new ContactDetailsParser();
            responseData.ContactDetails = new List<ContactDetailsParser.ContactDetail>();

			for (ContactDetailsParser.ContactDetail contactDetail : parsedData.ContactDetails) {
                if (!String.isBlank(contactDetail.externalIdType) && !String.isBlank(contactDetail.externalId)) {
                    if (contactDetail.externalIdType.equalsIgnoreCase('RWS')) {
						rwsIds.add('R.' + (contactDetail.externalId).trim());
						idFound.put('R.' + (contactDetail.externalId).trim(), false);
                    } else if (contactDetail.externalIdType.equalsIgnoreCase('PS')) {
                        psIds.add((contactDetail.externalId).trim());
						idFound.put((contactDetail.externalId).trim(), false);
                    } else if (contactDetail.externalIdType.equalsIgnoreCase('MDMID')){
						mdmIds.add((contactDetail.externalId).trim());
						idFound.put((contactDetail.externalId).trim(), false);
					} else {
                        invalidIds.add((contactDetail.externalId).trim());
						idFound.put((contactDetail.externalId).trim(), false);
                    }
                } else if(String.isBlank(contactDetail.externalIdType) && !String.isBlank(contactDetail.externalId)) {
					invalidIds.add((contactDetail.externalId).trim());
				} else if(!String.isBlank(contactDetail.externalIdType) && String.isBlank(contactDetail.externalId)){
					responseData.ContactDetails.add(TalentLookupService.addMessage('Please provide valid externalId'));
				}
            }

            if((rwsIds.size() > 0 || psIds.size() > 0) || mdmIds.size() > 0){
                
                /*responseData = new ContactDetailsParser();
                responseData.ContactDetails = new List<ContactDetailsParser.ContactDetail>();*/
				List<Contact> contactList = [select Id, name, FirstName, LastName, email, Preferred_Phone__c, Preferred_Email__c,Work_Email__c,
												Preferred_Phone_Value__c, Preferred_Email_Value__c, HomePhone, MobilePhone, title, MailingCity, MailingCountry, 
												MailingPostalCode, MailingState, MailingStreet,Phone,  Other_Email__c, OtherPhone,
												Preferred_Name__c,Website_URL__c, Source_System_Id__c, Peoplesoft_Id__C, MDM_ID__c
												from contact where (Source_System_Id__c in :rwsIds or Peoplesoft_Id__c in :psIds or MDM_ID__c in : mdmIds)];

                ContactDetailsParser.ContactDetail contactDetailResp;
                ContactDetailsParser.Email email;
                ContactDetailsParser.Phone phone;
                ContactDetailsParser.Address address;
               
				for(Contact c : contactList) {
					String s = c.Peoplesoft_ID__c;
					String str = c.Source_System_Id__c;
					String mdm = c.MDM_ID__c;

					if(rwsIds.contains(str)){
						idFound.remove(str);
					}else if(psIds.contains(s)){
						idFound.remove(s);
					}else if(mdmIds.contains(mdm)){
						idFound.remove(mdm);
					}

					contactDetailResp = new ContactDetailsParser.ContactDetail();
                    
					contactDetailResp.FirstName = c.FirstName;
					contactDetailResp.LastName = c.LastName;
					contactDetailResp.Title = c.title;
					contactDetailResp.EmployeeId = c.Peoplesoft_ID__c;
					contactDetailResp.RWSId = c.Source_System_Id__c;
					contactDetailResp.MDMId = c.MDM_ID__c;
					contactDetailResp.PreferredName = c.Preferred_Name__c;
					contactDetailResp.SalesforceContactID = c.Id;
                    
					contactDetailResp.Emails = new List<ContactDetailsParser.Email>();
					if (!String.isBlank(c.Email)) {
						email = new ContactDetailsParser.Email();
						email.Id = c.Email;
						email.Type = 'Home';
						if(c.Preferred_Email_Value__c == c.Email){
							email.Preferred = true;
						}
						contactDetailResp.Emails.add(email);
					}
                   
					if (!String.isBlank(c.Other_Email__c)) {
						email = new ContactDetailsParser.Email();
						email.Id = c.Other_Email__c;
						email.Type = 'Other';
						if(c.Preferred_Email_Value__c == c.Other_Email__c){
							email.Preferred = true;
						}
						contactDetailResp.Emails.add(email);
					}
					
					if (!String.isBlank(c.Work_Email__c)) {
						email = new ContactDetailsParser.Email();
						email.Id = c.Work_Email__c;
						email.Type = 'Work';
						if(c.Preferred_Email_Value__c == c.Work_Email__c){
							email.Preferred = true;
						}
						contactDetailResp.Emails.add(email);
					}
					contactDetailResp.Phones = new List<ContactDetailsParser.Phone>();
					if (!String.isBlank(c.Phone)) {
						phone = new ContactDetailsParser.Phone();
						phone.Phone_Number = c.Phone;
						phone.Type = 'Work';
						if (c.Preferred_Phone_Value__c == c.Phone){
							phone.Preferred = true;
						}
						contactDetailResp.Phones.add(phone);
					}
					if (!String.isBlank(c.HomePhone)) {
						phone = new ContactDetailsParser.Phone();
						phone.Phone_Number = c.HomePhone;
						phone.Type = 'Home';
						if (c.Preferred_Phone_Value__c == c.HomePhone){
							phone.Preferred = true;
						}
						contactDetailResp.Phones.add(phone);
					}
					if (!String.isBlank(c.MobilePhone)) {
						phone = new ContactDetailsParser.Phone();
						phone.Phone_Number = c.MobilePhone;
						phone.Type = 'Mobile';
						if (c.Preferred_Phone_Value__c == c.MobilePhone){
							phone.Preferred = true;
						}
						contactDetailResp.Phones.add(phone);
					}
                   
					//STORY S-126499
					if (!String.isBlank(c.OtherPhone)) {
						phone = new ContactDetailsParser.Phone();
						phone.Phone_Number = c.OtherPhone;
						phone.Type = 'OtherPhone';
						if (c.Preferred_Phone_Value__c == c.OtherPhone){
							phone.Preferred = true;
						}
						contactDetailResp.Phones.add(phone);
					}
                   
					contactDetailResp.Addresses = new List<ContactDetailsParser.Address>();
                    
					if (!String.isBlank(c.MailingCity) || !String.isBlank(c.MailingCountry) || 
						!String.isBlank(c.MailingPostalCode) || !String.isBlank(c.MailingState) ||
						!String.isBlank(c.MailingStreet)) {
							address = new ContactDetailsParser.Address();
							address.Street = c.MailingStreet;
							address.City = c.MailingCity;
							address.State = c.MailingState;
							address.Country = c.MailingCountry;
							address.PostalCode = c.MailingPostalCode;
							address.Type = 'Home';
							contactDetailResp.Addresses.add(address);
						}
						contactDetailResp.message = 'Success';
					responseData.ContactDetails.add(contactDetailResp);
                    
				}

				if (idFound.size() > 0){
				    
					for(String s : idFound.keySet()){
						notFoundIds.add(s);
					}
				}
				
                  // If no talent found for rws and peoplesoft id
                if(contactList.size() == 0){
                    if(invalidIds.size() > 0){
					
						for(String i : invalidIds){
							//responseData.ContactDetails.add(TalentLookupService.addMessage('No Matching talent found for given rws id ' +rwsIds+ ' and peoplesoft id'+psIds+ ' and invalid External Id type is added for given Ids ' + i));
							responseData.ContactDetails.add(TalentLookupService.addMessage('Invalid External Id type is added for given Ids ' + i));
							
						}

						for (String s : idFound.keySet()){
						    if(!invalidIds.contains(s)){
								responseData.ContactDetails.add(TalentLookupService.addMessage('No Matching talent found for given id ' + s));
							}
						}
							res.responseBody = Blob.valueOf(JSON.serialize(responseData));

                    } else {
                         responseData.ContactDetails.add(TalentLookupService.addMessage('No Matching talent found for given id ' +notFoundIds));    
						//res.responseBody = Blob.valueOf('Invalid External Id type is added for given Ids ' +invalidIds);
						//res.responseBody = Blob.valueOf('No Matching talent found for given rws ' +rwsIds+ ' and peoplesoft id'+psIds);
						res.responseBody = Blob.valueOf(JSON.serialize(responseData));
					}

                } else  {
				    for (String s : notFoundIds){
						responseData.ContactDetails.add(TalentLookupService.addMessage('No Matching talent found for given id ' +s));    		
					}
					for (String str : invalidIds){
						if(!notFoundIds.contains(str)){
							responseData.ContactDetails.add(TalentLookupService.addMessage('No Matching talent found for given id ' +str));
						}
					}
					res.responseBody = Blob.valueOf(JSON.serialize(responseData));
				
				}
				
                res.statusCode = 200; 
				res.addHeader('Content-Type','application/json');  
                
            } else {
			    
					responseData.ContactDetails.add(TalentLookupService.addMessage('Invalid External Id type is added for given Ids '+ invalidIds));
				    //response = 'Invalid External Id type is added for given Ids ' +invalidIds;
				
					res.responseBody = Blob.valueOf(JSON.serialize(responseData));
					res.statusCode = 200;
					
					//res.responseBody = Blob.valueOf(JSON.serialize(responseData));
				    res.addHeader('Content-Type','application/json');
            }
        } catch (Exception e) {
            response = 'Exception for the given input : ' + RestContext.request.params;
            response += e.getTypeName() + ': ' + e.getLineNumber() + ' - ' + e.getMessage() + '\r\n' + e.getStackTraceString();
            response += 'Please pass contactId paramter. Parameter List (case-sensitive): contactid - Salesforce Contact Id, includeResume - return resume also';
            res.statusCode = 400;
            res.responseBody = Blob.valueOf(response);
			res.addHeader('Content-Type','application/json');
        }
        //retun responseData;
        
    }
    
     private static ContactDetailsParser.ContactDetail addMessage(String message){
        ContactDetailsParser.ContactDetail contactDetailResp = new ContactDetailsParser.ContactDetail();
        contactDetailResp.message = message;
        return contactDetailResp;
    }
    
   
}