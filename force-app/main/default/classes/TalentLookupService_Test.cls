@isTest
public class TalentLookupService_Test  {
    @TestSetup
    static void testSetup(){
   		Account talent = CreateTalentTestData.createTalent();
			//talent.Peoplesoft_ID__c = '36249190';
			talent.Peoplesoft_ID__c = (string.valueOf(23456 * 143)).substring(0,7);
			talent.Talent_Preference__c = '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
			talent.Assign_End_Notice_Close_Date__c = System.today();
			update talent;

			Contact con =[select id from contact where accountid =: talent.id Limit 1];
			con.End_Date_Change_Requested__c = 'change requested';
			con.Recruiter_Change_Requested__c = 'yes';
			con.externalSourceId__c = '12345';
			con.MDM_ID__c ='12345';
			con.Source_System_Id__c='12345';
        	con.Work_Email__c='test@test.com';
			update con;
        System.assert(con.End_Date_Change_Requested__c=='change requested');
        System.assert(con.Recruiter_Change_Requested__c=='yes');
        System.assert(con.externalSourceId__c=='12345');
        

    }
    private static string generateResopnse1(){
        Contact c = [Select Id from Contact limit 1];
        String json=		'{'+
                            '  "ContactDetails": ['+
                            '    {'+
                            '      "EmployeeId": "12345",'+
                            '      "RWSId": "R.12345",'+
                            '      "MDMID": "12345",'+
                            '      "SalesforceContactID": "12345",'+
                            '      "externalId": "12345",'+
                            '      "externalIdType": "RWS",'+
                            '      "FirstName": "12345",'+
                            '      "LastName": "12345",'+
                            '      "PreferredName": "12345",'+
                            '      "Email": ['+
                            '        {'+
                            '          "Id": "test@test.com",'+
                            '          "Type": "Work",'+
                            '          "Preferred": "true"'+
                            '        }'+
                            '      ],'+
                            '      "Address": ['+
                            '        {'+
                            '          "Street": "HomeStreet",'+
                            '          "City": "HomeCity",'+
                            '          "State": "Philadelphia",'+
                            '          "Country": "USA",'+
                            '          "PostalCode": "12345",'+
                            '          "Type": "Home"'+
                            '        }'+
                            '      ],'+
                            '      "Phone": ['+
                            '        {'+
                            '          "Phone_Number": "9090909090",'+
                            '          "Preferred": "false",'+
                            '          "Type":"Home"'+
                            '        }'+
                            '      ],'+
                            '      "Title": "Doctor",'+
                            '      "message": "Sample test"'+
                            '    }'+
                            '  ]'+
                            '}';
        System.assert(json=='{'+
                            '  "ContactDetails": ['+
                            '    {'+
                            '      "EmployeeId": "12345",'+
                            '      "RWSId": "R.12345",'+
                            '      "MDMID": "12345",'+
                            '      "SalesforceContactID": "12345",'+
                            '      "externalId": "12345",'+
                            '      "externalIdType": "RWS",'+
                            '      "FirstName": "12345",'+
                            '      "LastName": "12345",'+
                            '      "PreferredName": "12345",'+
                            '      "Email": ['+
                            '        {'+
                            '          "Id": "test@test.com",'+
                            '          "Type": "Work",'+
                            '          "Preferred": "true"'+
                            '        }'+
                            '      ],'+
                            '      "Address": ['+
                            '        {'+
                            '          "Street": "HomeStreet",'+
                            '          "City": "HomeCity",'+
                            '          "State": "Philadelphia",'+
                            '          "Country": "USA",'+
                            '          "PostalCode": "12345",'+
                            '          "Type": "Home"'+
                            '        }'+
                            '      ],'+
                            '      "Phone": ['+
                            '        {'+
                            '          "Phone_Number": "9090909090",'+
                            '          "Preferred": "false",'+
                            '          "Type":"Home"'+
                            '        }'+
                            '      ],'+
                            '      "Title": "Doctor",'+
                            '      "message": "Sample test"'+
                            '    }'+
                            '  ]'+
                            '}');
        return json;
    }
	private static string generateResopnse2(){
        Contact c = [Select Id from Contact limit 1];
        String json=		'{'+
                            '  "ContactDetails": ['+
                            '    {'+
                            '      "EmployeeId": "12345",'+
                            '      "RWSId": "R.12345",'+
                            '      "MDMID": "12345",'+
                            '      "SalesforceContactID": "12345",'+
                            '      "externalId": "12345",'+
                            '      "externalIdType": "12345",'+
                            '      "FirstName": "12345",'+
                            '      "LastName": "12345",'+
                            '      "PreferredName": "12345",'+
                            '      "Email": ['+
                            '        {'+
                            '          "Id": "test@test.com",'+
                            '          "Type": "Work",'+
                            '          "Preferred": "false"'+
                            '        }'+
                            '      ],'+
                            '      "Address": ['+
                            '        {'+
                            '          "Street": "HomeStreet",'+
                            '          "City": "HomeCity",'+
                            '          "State": "Philadelphia",'+
                            '          "Country": "USA",'+
                            '          "PostalCode": "12345",'+
                            '          "Type": "Home"'+
                            '        }'+
                            '      ],'+
                            '      "Phone": ['+
                            '        {'+
                            '          "Phone_Number": "9090909090",'+
                            '          "Preferred": "true",'+
                            '          "Type":"Home"'+
                            '        }'+
                            '      ],'+
                            '      "Title": "Doctor",'+
                            '      "message": "Sample test"'+
                            '    }'+
                            '  ]'+
                            '}';
        System.assert(json=='{'+
                            '  "ContactDetails": ['+
                            '    {'+
                            '      "EmployeeId": "12345",'+
                            '      "RWSId": "R.12345",'+
                            '      "MDMID": "12345",'+
                            '      "SalesforceContactID": "12345",'+
                            '      "externalId": "12345",'+
                            '      "externalIdType": "12345",'+
                            '      "FirstName": "12345",'+
                            '      "LastName": "12345",'+
                            '      "PreferredName": "12345",'+
                            '      "Email": ['+
                            '        {'+
                            '          "Id": "test@test.com",'+
                            '          "Type": "Work",'+
                            '          "Preferred": "false"'+
                            '        }'+
                            '      ],'+
                            '      "Address": ['+
                            '        {'+
                            '          "Street": "HomeStreet",'+
                            '          "City": "HomeCity",'+
                            '          "State": "Philadelphia",'+
                            '          "Country": "USA",'+
                            '          "PostalCode": "12345",'+
                            '          "Type": "Home"'+
                            '        }'+
                            '      ],'+
                            '      "Phone": ['+
                            '        {'+
                            '          "Phone_Number": "9090909090",'+
                            '          "Preferred": "true",'+
                            '          "Type":"Home"'+
                            '        }'+
                            '      ],'+
                            '      "Title": "Doctor",'+
                            '      "message": "Sample test"'+
                            '    }'+
                            '  ]'+
                            '}');
        return json;
    }
	private static string generateResopnse3(){
        Contact c = [Select Id from Contact limit 1];
        String json=		'{'+
                            '  "ContactDetails": ['+
                            '    {'+
                            '      "EmployeeId": "12345",'+
                            '      "RWSId": "R.12345",'+
                            '      "MDMID": "12345",'+
                            '      "SalesforceContactID": "12345",'+
                            '      "externalId": "12345",'+
                            '      "externalIdType": "MDMID",'+
                            '      "FirstName": "12345",'+
                            '      "LastName": "12345",'+
                            '      "PreferredName": "12345",'+
                            '      "Email": ['+
                            '        {'+
                            '          "Id": "test@test.com",'+
                            '          "Type": "Work",'+
                            '          "Preferred": "false"'+
                            '        }'+
                            '      ],'+
                            '      "Address": ['+
                            '        {'+
                            '          "Street": "HomeStreet",'+
                            '          "City": "HomeCity",'+
                            '          "State": "Philadelphia",'+
                            '          "Country": "USA",'+
                            '          "PostalCode": "12345",'+
                            '          "Type": "Home"'+
                            '        }'+
                            '      ],'+
                            '      "Phone": ['+
                            '        {'+
                            '          "Phone_Number": "9090909090",'+
                            '          "Preferred": "true",'+
                            '          "Type":"Home"'+
                            '        }'+
                            '      ],'+
                            '      "Title": "Doctor",'+
                            '      "message": "Sample test"'+
                            '    }'+
                            '  ]'+
                            '}';
        return json;
    }
   private static string generateResopnseFalse(){
        String json =  '{   \"person\" : [  {\"EmployeeID\": \"05645028\",'+
            '        \"FirstName\":\"James\",'+
            '        \"LastName\": \"Lanier\",'+
            '        \"MiddleName\":\"\",'+
            '        \"NamePrefix\": \"\",'+
            '        \"EmailAddress\": [ {\"Email\": \"test11werewrewtrtrtretewrerserewrwerewtrtrtertretretretwretswtewtewtrwretrtrtretretre@test.com\"}],'+
            '        \"Phone\": [ {\"Type\": \"CELL\",'+
            '        \"Phone\": \"301/555-1212\"'+
            '        }, {'+
            '        \"Type\": \"HOME\",'+
            '        \"Phone\":\"846/243-6326\"'+
            '        },{'+
            '        \"Type\": \"OFFICE\",'+
            '        \"Phone\": \"410/555-1212\"}],'+
            '        \"Address\": [{'+
            '        \"Type\":\"\",'+
            '        \"Address\":\"5643 Open Sky\",'+
            '        \"City\": \"Columbia\",'+
            '        \"State\": \"MD\",'+
            '        \"Postal\": \"21044\",'+
            '        \"Country\": \"USA\" }]},'+
            '        {\"EmployeeID\": \"05526633\",'+
            '        \"FirstName\":\"Heather\",'+
            '        \"LastName\": \"Jones\",'+
            '        \"MiddleName\":\"Annalisa\",'+
            '        \"NamePrefix\": \"\",'+
            '        \"EmailAddress\": [ {\"Email\": \"test12sfrtretretrtsgrgtergtretgfdsegregtfdrgergtregtregtrgsfddgrgrtrtrsdfdrgtrdgfgtfdgr@test.com\"}],'+
            '        \"Phone\": [ {\"Type\": \"CELL\",'+
            '        \"Phone\": \"410/730-6156\"'+
            '        }, {'+
            '        \"Type\": \"MOBILE\",'+
            '        \"Phone\":\"864/243-6326\"'+
            '        },{'+
            '        \"Type\": \"HOME\",'+
            '        \"Phone\": \"240/555/1212\"}],'+
            '        \"Address\": [{'+
            '        \"Type\":\"\",'+
            '        \"Address\":\"964 Stoll St. #2\",'+
            '        \"City\": \"Baltimore\",'+
            '        \"State\": \"MD\",'+
            '        \"Postal\": \"21225\",'+
            '        \"Country\": \"USA\" }]}'+
            ']}';
       System.assert(json=='{   \"person\" : [  {\"EmployeeID\": \"05645028\",'+
            '        \"FirstName\":\"James\",'+
            '        \"LastName\": \"Lanier\",'+
            '        \"MiddleName\":\"\",'+
            '        \"NamePrefix\": \"\",'+
            '        \"EmailAddress\": [ {\"Email\": \"test11werewrewtrtrtretewrerserewrwerewtrtrtertretretretwretswtewtewtrwretrtrtretretre@test.com\"}],'+
            '        \"Phone\": [ {\"Type\": \"CELL\",'+
            '        \"Phone\": \"301/555-1212\"'+
            '        }, {'+
            '        \"Type\": \"HOME\",'+
            '        \"Phone\":\"846/243-6326\"'+
            '        },{'+
            '        \"Type\": \"OFFICE\",'+
            '        \"Phone\": \"410/555-1212\"}],'+
            '        \"Address\": [{'+
            '        \"Type\":\"\",'+
            '        \"Address\":\"5643 Open Sky\",'+
            '        \"City\": \"Columbia\",'+
            '        \"State\": \"MD\",'+
            '        \"Postal\": \"21044\",'+
            '        \"Country\": \"USA\" }]},'+
            '        {\"EmployeeID\": \"05526633\",'+
            '        \"FirstName\":\"Heather\",'+
            '        \"LastName\": \"Jones\",'+
            '        \"MiddleName\":\"Annalisa\",'+
            '        \"NamePrefix\": \"\",'+
            '        \"EmailAddress\": [ {\"Email\": \"test12sfrtretretrtsgrgtergtretgfdsegregtfdrgergtregtregtrgsfddgrgrtrtrsdfdrgtrdgfgtfdgr@test.com\"}],'+
            '        \"Phone\": [ {\"Type\": \"CELL\",'+
            '        \"Phone\": \"410/730-6156\"'+
            '        }, {'+
            '        \"Type\": \"MOBILE\",'+
            '        \"Phone\":\"864/243-6326\"'+
            '        },{'+
            '        \"Type\": \"HOME\",'+
            '        \"Phone\": \"240/555/1212\"}],'+
            '        \"Address\": [{'+
            '        \"Type\":\"\",'+
            '        \"Address\":\"964 Stoll St. #2\",'+
            '        \"City\": \"Baltimore\",'+
            '        \"State\": \"MD\",'+
            '        \"Postal\": \"21225\",'+
            '        \"Country\": \"USA\" }]}'+
            ']}');
        return json;
    }
    public static testmethod void testMethod1() {      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        string requestJSONBody = generateResopnse1();            
        req.requestURI = 'talent/lookupTalentDetails/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJSONBody);
        RestContext.request = req;
        RestContext.response= res;
        System.assert(req.requestURI=='talent/lookupTalentDetails/*');
        System.assert(req.httpMethod=='POST');
        ContactDetailsParser cdp = new ContactDetailsParser(); 
        cdp.parse(requestJSONBody);
        TalentLookupService.getTalentDetails();        
    }
    public static testmethod void testMethod2() {      
        String json ='{"ContactDetail":[{"EmployeeId":"12345","RWSId":"12345","MDMID":"12345","SalesforceContactID":"12345","externalId":"12345","externalIdType":"PS","FirstName":"12345","LastName":"12345","PreferredName":"12345","Email":[{"Id":"test@test.com","Type":"12345","Preferred":"false"}],"Addresses":[{"Address":"usa","Street":"usa","City":"usa","State":"usa","Country":"usa","PostalCode":"usa","Type":"usa"}],"Phones":[{"phone":"9090909090","Preferred":"false"}],"Title":"12345","message":"12345"}]}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        string requestJSONBody = generateResopnse2();            
        req.requestURI = 'talent/lookupTalentDetails/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJSONBody);
        RestContext.request = req;
        RestContext.response= res;
         System.assert(req.requestURI=='talent/lookupTalentDetails/*');
        System.assert(req.httpMethod=='POST');
        ContactDetailsParser cdp = new ContactDetailsParser();        
        String newstring= generateResopnse2();
        cdp.parse(newstring);
        TalentLookupService.getTalentDetails();        
    }
    public static testmethod void testMethod3() {      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        string requestJSONBody = generateResopnse3();            
        req.requestURI = 'talent/lookupTalentDetails/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJSONBody);
        RestContext.request = req;
        RestContext.response= res;
         System.assert(req.requestURI=='talent/lookupTalentDetails/*');
        System.assert(req.httpMethod=='POST');
        String json ='{"ContactDetail":[{"EmployeeId":"12345","RWSId":"12345","MDMID":"12345","SalesforceContactID":"12345","externalId":"12345","externalIdType":"MDMID","FirstName":"12345","LastName":"12345","PreferredName":"12345","Email":[{"Id":"test@test.com","Type":"12345","Preferred":"false"}],"Addresses":[{"Address":"usa","Street":"usa","City":"usa","State":"usa","Country":"usa","PostalCode":"usa","Type":"usa"}],"Phones":[{"phone":"9090909090","Preferred":"false"}],"Title":"12345","message":"12345"}]}';
        ContactDetailsParser cdp = new ContactDetailsParser();
        //cdp.ContactDetail cd = new cdp.ContactDetail();
        String newstring= generateResopnse1();
        cdp.parse(newstring);
        TalentLookupService.getTalentDetails();        
    }
    public static testmethod void testMethod4() {      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        string requestJSONBody = generateResopnseFalse();            
        req.requestURI = 'talent/lookupTalentDetails/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJSONBody);
        RestContext.request = req;
        RestContext.response= res; 
         System.assert(req.requestURI=='talent/lookupTalentDetails/*');
        System.assert(req.httpMethod=='POST');
        ContactDetailsParser cdp = new ContactDetailsParser();
        String newstring= generateResopnse1();
        cdp.parse(newstring);
        TalentLookupService.getTalentDetails();        
    }
}