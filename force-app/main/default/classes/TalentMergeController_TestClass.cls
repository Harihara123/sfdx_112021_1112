@isTest
public class TalentMergeController_TestClass{ 
    public static List<Account> talAccounts = new List<Account>();
    
    @testSetup static void loadTestData() {
		//talAccounts.add(CreateTalentTestData.createTalent());
        //talAccounts.add(CreateTalentTestData.createTalent());
	}
    public static testMethod void doPostTest(){
    
        Test.startTest();
			TalentMergeController_TestClass.mergeTalentTest();
        	List<Contact> lstContact = [Select Id, Talent_Id__c from Contact];
        	
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
			String requestJSONBody = '{ "masterId":"'+lstContact[0].Id+'", "duplicateId":"'+lstContact[1].Id+'","ignoreESFIdConflicts":false }';
            
            req.requestURI = '/services/apexrest/talent/merge/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            TalentMergeController.doPost(lstContact[0].Id, lstContact[1].Id,false);
    	
        Test.stopTest();    
        
        
    }
	public static testMethod void sameRecordEnteredTest(){
    Test.startTest();
    	TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();    
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[0].Talent_Id__c);
        //system.assertEquals(validation, 'You have entered same Talent C - number for Master and Duplicate Candidate.');
    }
    
    public static testMethod void mergeSuccessTest(){
		Test.startTest();
    	TalentMergeController_TestClass.mergeTalentTest();
    	Test.stopTest();    
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
    	List<Account> lstAccount = [Select Id,Talent_Id__c from Account where RecordTypeId =: types[0].Id];
       
    	String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
    	//system.assertEquals(validation, 'Merge is Successful.');
    }

   public static testMethod void mergePhoneNumbersTest(){
		Test.startTest();
    	TalentMergeController_TestClass.mergeTalentPhoneTest();
    	Test.stopTest();    
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
    	List<Account> lstAccount = [Select Id,Talent_Id__c from Account where RecordTypeId =: types[0].Id];       

        Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c));       
    	system.assertEquals((String)response.get('Success'), 'true');
    }
    /*
    public static testMethod void testOrderStatusUpdate(){
        Test.startTest();
        TalentMergeController_TestClass.mergeTalentTest2();        
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Contact contact1 = [Select Id from Contact where AccountId =: lstAccount[0].Id];
        Contact contact2 = [Select Id from Contact where AccountId =: lstAccount[1].Id];
        //createSubmittals
       // Order order1 = TalentMergeController_TestClass.createOrder(lstAccount[0].Id, contact1.Id);        
       // Order order12 = TalentMergeController_TestClass.createOrder(lstAccount[0].Id, contact1.Id);
       // Order order13 = TalentMergeController_TestClass.createOrder(lstAccount[0].Id, contact1.Id);
        
       // Order order2 = TalentMergeController_TestClass.createOrder(lstAccount[1].Id, contact2.Id);
        //order2.Start_Form_ID__c = 'st134';
        Map<String,Order> applicationOrderOldNewMap = new Map<String,Order>();
        //{String.valueOf(order1.Id)=>order1,
          //  																String.valueOf(order12.Id)=>order12};
		
		String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        System.debug('testOrderStatusUpdate validation ' + validation);
        Test.stopTest();
    }*/
    
    public static testMethod void testSubmittalAndATSjobPrecheckRules(){
        //submittalAndATSjobPrecheckRules
        Test.startTest();
    	TalentMergeController_TestClass.mergeTalentTest();
    	Test.stopTest();    
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
    	List<Account> lstAccount = [Select Id,Talent_Id__c from Account where RecordTypeId =: types[0].Id];
       
    	String validation = TalentMergeController.submittalAndATSjobPrecheckRules(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        System.debug('validation >> '+validation);
    }
    public static testMethod void submittalPrecheckTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
    	Test.startTest();
    	TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Contact contact = [Select Id from Contact where AccountId =: lstAccount[1].Id];
        TalentMergeController_TestClass.createOrder(lstAccount[1].Id, contact.Id);
		// TalentMergeController.preCheckRules(lstAccount[0].Id, lstAccount[1].Id);
        // TalentMergeController.MLAPrecheckRules(lstAccount[0].Id, lstAccount[1].Id);
        String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        //system.assertEquals(validation, 'Merge is Successful.');
    }
    
    public static testMethod void submittalPrecheckTest2(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Test.startTest();
            TalentMergeController_TestClass.mergeTalentTest2();
        Test.stopTest();    
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Contact contact = [Select Id from Contact where AccountId =: lstAccount[1].Id];
        TalentMergeController_TestClass.createOrder(lstAccount[1].Id, contact.Id);
       // TalentMergeController.preCheckRules(lstAccount[0].Id, lstAccount[1].Id);
        // TalentMergeController.MLAPrecheckRules(lstAccount[0].Id, lstAccount[1].Id);
        String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        //system.assertEquals(validation, 'Merge is Successful.');
    
    }
    public static testMethod void mlaPrecheckTest(){
        
        Test.startTest();
            TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Account acc = new Account(Name='Test', RecordTypeId = RecordTypeIdAccount);
        insert acc;
        Account account = lstAccount[0];
        account.Talent_Ownership__c = 'MLA';
        account.Current_Employer__c = acc.Id;
        update account;
        TalentMergeController_TestClass.createTargetAccount(lstAccount[0].Id);
        TalentMergeController_TestClass.createTargetAccount(lstAccount[1].Id);
        Contact contact = [Select Id from Contact where AccountId =: lstAccount[1].Id];
        lstAccount[0].Do_Not_Contact__c=True;
        update lstAccount[0];
        
        lstAccount[1].Do_Not_Contact__c=True;
        update lstAccount[1];
        String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        }

    public static testMethod void exceptionTest(){
	
        Test.startTest();  
        TalentMergeController_TestClass.mergeTalentTest2();
        
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Contact con = [Select Id from Contact where AccountId =: lstAccount[0].Id];
        TalentMergeController_TestClass.createTargetAccount(lstAccount[0].Id);
        TalentMergeController_TestClass.createTargetAccount(lstAccount[1].Id);
        
        Tag_Definition__c tagDef = new Tag_Definition__c(Tag_Type__c = 'Team', Tag_Name__c = 'Team');
        insert tagDef;
        Contact_Tag__c tag = new Contact_Tag__c(Contact__c = con.Id, Tag_Definition__c = tagDef.Id);
        Contact_Tag__c tag2 = new Contact_Tag__c(Contact__c = con.Id, Tag_Definition__c = tagDef.Id);
        insert tag;
        insert tag2;
        
        lstAccount[0].Do_Not_Contact__c=True;
        update lstAccount[0];
        
        lstAccount[1].Do_Not_Contact__c=True;
        update lstAccount[1];
     Test.stopTest();   
        String validation = TalentMergeController.talentMerge(lstAccount[0].Talent_Id__c, lstAccount[1].Talent_Id__c);
        
    }
    
    public static testMethod void invalidIDTest(){
        
        String validation=''; 
            validation = TalentMergeController.talentMerge('C-xxxxxxxxx', 'C-xxxxxxxxx');
        
    }
   
    public static testMethod void getMasterRecordTest(){
               Test.startTest();        
            TalentMergeController_TestClass.mergeTalentTest();
            TalentMergeController_TestClass.mergeTalentTest();
            TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        Contact con = [Select Id from Contact where AccountId =: lstAccount[0].Id];
        ID var = TalentMergeController.getMasterRecordId(lstAccount[0].Talent_Id__c);
        ID var2 =TalentMergeController.getMasterRecordId('xyz');
        
        
    }
    public static testMethod void getTalentRecordsTest(){
       
        Test.startTest();        
            TalentMergeController_TestClass.mergeTalentTest();
            TalentMergeController_TestClass.mergeTalentTest();
            TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        contact[] con = TalentMergeController.getTalentRecords(lstAccount[0].Talent_Id__c,lstAccount[1].Talent_Id__c);
        
        }
   

    public static testMethod void nullAccountContactAddressTest(){
        
        String validation;
        Test.startTest();
            TalentMergeController_TestClass.mergeTalentTest();
            List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
            TalentMergeController_TestClass.createTargetAccount(lstAccount[0].Id);
            TalentMergeController_TestClass.createTargetAccount(lstAccount[1].Id);
            Contact contact = [Select Id, MailingStreet, MailingCity from Contact where AccountId =: lstAccount[0].Id];
            contact.MailingCity = '';
            contact.MailingStreet = '';
            contact.FirstName = '';
            update contact;  
        Test.stopTest();                    
       validation = TalentMergeController.talentMerge('hdksjhdjksha', 'nhkdhadhakls');
      validation = TalentMergeController.talentMerge('hdksjhdjksha', 'hdksjhdjksha');
    }
    public static testMethod void resumeTest(){
        
        Test.startTest();
            TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        List<Talent_Document__c> lstdoc = new List<Talent_Document__c>();
        for(Integer i = 0; i < 4; i++){
            Talent_Document__c doc = new Talent_Document__c();        
            doc.Document_Name__c = 'resume';
            doc.Document_Type__c = 'Resume';
            doc.Talent__c = lstAccount[0].Id;
            lstdoc.add(doc);
            
            Talent_Document__c doc2 = new Talent_Document__c();
            doc2.Document_Name__c = 'resume2';
            doc2.Document_Type__c = 'Resume - Dubbed';
            doc2.Talent__c = lstAccount[0].Id;
            lstdoc.add(doc2);
            
            Talent_Document__c doc3 = new Talent_Document__c();
            doc3.Document_Name__c = 'resume3';
            doc3.Document_Type__c = 'Other';
            doc3.Talent__c = lstAccount[0].Id;
            lstdoc.add(doc3);
        }
        insert lstdoc;
        TalentMergeController.resumeRecordHandler(lstAccount[0].id, lstAccount[1].id);
        
    }
    
    public static testMethod void jobBoardsResumeTest(){
        
        Test.startTest();
        TalentMergeController_TestClass.mergeTalentTest();
        Test.stopTest();
        List<Account> lstAccount = [Select Id, Talent_Id__c from Account];
        List<Talent_Document__c> lstdoc = new List<Talent_Document__c>();
         Talent_Document__c doc = new Talent_Document__c();        
        doc.Document_Name__c = 'job Boards';
        doc.Document_Type__c = 'job Boards';
        doc.Talent__c = lstAccount[1].Id;
        lstdoc.add(doc);
        doc = new Talent_Document__c();        
        doc.Document_Name__c = 'job Boards2';
        doc.Document_Type__c = 'job Boards';
        doc.Talent__c = lstAccount[1].Id;
        insert lstdoc;
        String validation = TalentMergeController.talentMerge(lstAccount[0].id, lstAccount[1].id);
        System.assert(validation!=null);
    }
    
//Test Data Methods
    public static void mergeTalentTest(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        String u = 'chs'+String.valueOf(Datetime.now().formatGMT('HHmmssSSS'))+'@allegisgroup.com';
        User testUser = new User(firstname = 'Future', lastname = 'User',
            alias = 'future', defaultgroupnotificationfrequency = 'N',
            digestfrequency = 'N', email =  u,
            emailencodingkey = 'UTF-8', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey = 'America/Los_Angeles',
            username = u,
            userpermissionsmarketinguser = false,
            userpermissionsofflineuser = false);
     System.runAs(testUser) {
        
        Account masterAccount = TalentMergeController_TestClass.createTalentAccount('master');
        Contact masterContact = TalentMergeController_TestClass.createTalentContact2(masterAccount.Id);
        Account duplicateAccount = TalentMergeController_TestClass.createTalentAccount('duplicate');
        Contact duplicateContact = TalentMergeController_TestClass.createTalentContact(duplicateAccount.Id);
      }
    }
    
    public static void mergeTalentTest2(){
      
        Account masterAccount = TalentMergeController_TestClass.createTalentAccount2('master');
        Contact masterContact = TalentMergeController_TestClass.createTalentContact2(masterAccount.Id);
        Account duplicateAccount = TalentMergeController_TestClass.createTalentAccount2('duplicate1');
        Contact duplicateContact = TalentMergeController_TestClass.createTalentContact(duplicateAccount.Id);
       
    }
    public static void mergeTalentPhoneTest(){
      	
        Account masterAccount = TalentMergeController_TestClass.createTalentAccount2('master');
        Contact masterContact = TalentMergeController_TestClass.createTalentContact2(masterAccount.Id);
        masterContact.phone = null;
        masterContact.WorkExtension__c  = null;
        
        Account duplicateAccount = TalentMergeController_TestClass.createTalentAccount2('duplicate1');
        Contact duplicateContact = TalentMergeController_TestClass.createTalentContact(duplicateAccount.Id);
        duplicateContact.Phone = '1234567890';
        upsert (new List<Contact>{masterContact,duplicateContact});
    }
    public static Account createTalentAccount(String clientAccountID){
        
        
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test Talent',Do_Not_Contact__c=true, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
public static Account createTalentAccount2(String clientAccountID){
        List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
        TalentPreferenceInternal tp = TalentPreferenceInternal.newPrefsJson();
        String jsonTP = JSON.serialize(tp);
        
        Account newAcc = new Account(Name='Test1 Talent',Do_Not_Contact__c=false, RecordTypeId = types[0].Id, Talent_Preference_Internal__c=jsonTP);
        
        
        Database.insert(newAcc);
        return newAcc;
    }
    public static Account createClientAccount(){
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        
        return newClientAcc;
    }

    public static Contact createTalentContact(String accountID){
        
        List<Account> lstAccount = [Select Id,recordTypeId, Talent_Id__c from Account where id=:accountID];
        Contact newC = null;
        if(lstAccount.size()>0){
            Account acc = lstAccount[0];
             List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
             Boolean isTalent = false;
             if(types[0].id == acc.recordTypeId){
                 isTalent = true;
             }
            if(isTalent){
                List<RecordType> contRectypes = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
                newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,recordTypeId = contRectypes[0].id
                  ,Email='tast@talent.com',RWS_DNC__c=True);
            }
            else{
                newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
            }
	        Database.insert(newC);            
        }        
        return newC;
        /*
        Contact newC = new Contact(Firstname='Test', LastName='Talent'
                  ,AccountId=accountID, Website_URL__c='test.com'
                  ,MailingStreet = 'Address St'
                  ,MailingCity='City'
                  ,Email='tast@talent.com',RWS_DNC__c=True);
        Database.insert(newC);
        return newC;
        */
    }
    public static Contact createTalentContact2(String accountID){
        List<Account> lstAccount = [Select Id,recordTypeId, Talent_Id__c from Account where id=:accountID];
        Contact newC = null;
        if(lstAccount.size()>0){
            Account acc = lstAccount[0];
             List<RecordType> types = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Account'];
             Boolean isTalent = false;
             if(types[0].id == acc.recordTypeId){
                 isTalent = true;
             }
            if(isTalent){
                List<RecordType> contRectypes = [SELECT Id from RecordType where Name = 'Talent' And SobjectType = 'Contact'];
                newC = new Contact(Firstname='', LastName='Talent2'
                  ,AccountId=accountID, Website_URL__c=''
                  ,MailingStreet = ''
                  ,MailingCity=''
                  ,recordTypeId = contRectypes[0].id
                  ,Email='test@gmail.com');
            }
            else{
                newC = new Contact(Firstname='', LastName='Talent2'
                  ,AccountId=accountID, Website_URL__c=''
                  ,MailingStreet = ''
                  ,MailingCity=''
                  ,Email='');
            }
	        Database.insert(newC);            
        }        
        return newC;
    }

    public static void createSubmittals(String accountId){
        List<Opportunity> opps = new List<Opportunity>();
        
        for (Integer k=0; k<2 ;k++) {
                opps.add(new Opportunity(Name= 'Test' + ' Opportunity ' + k,
                                       StageName='Prospecting',
                                       CloseDate=System.today().addMonths(1),
                                       AccountId=accountId));
        }
        
        // Insert all opportunities for all accounts.
        insert opps;
    }
    
    public static Order createOrder(String accountId, String contactId){
        
        Order o = new Order();
            o.ShipToContactId = contactId;
            o.AccountId = accountId;
            o.EffectiveDate = date.today();
            o.Status = 'Linked';
        insert o;
        return o;
    }
    
    
    public static void createTargetAccount(String AccountId){
        
        String userId = '';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (thisUser) {
        User futureUser = new User(firstname = 'Future', lastname = 'User',
            alias = 'future', defaultgroupnotificationfrequency = 'N',
            digestfrequency = 'N', email = AccountId + '@apogeesolutions.com',
            emailencodingkey = 'UTF-8', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey = 'America/Los_Angeles',
            username = AccountId + '@apogeesolutions.com',
            userpermissionsmarketinguser = false,
            userpermissionsofflineuser = false);
            insert(futureUser);
            userId = futureuser.Id;
        }
        
        Target_Account__c ac = new Target_Account__c();

        ac.Account__c = AccountId;
        ac.User__c = userId;
        ac.Expiration_Date__c = date.today().addDays(4);
        
        insert ac;
    }
}