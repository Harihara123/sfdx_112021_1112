public class TalentMergeHandler  {

   private final static String PostStr = 'Post'; 

  public static String handleMerge(Id masterAccountId, Id duplicateAccountId,String masterContactId,Id duplicateContactId,Id mergeMappingId) {

            Account masterAccount = new Account(Id = masterAccountId);
            Contact masterContact = new Contact(Id = masterContactId);
                            
            TriggerStopper.isMergeEvent = true;//setting this static variable so it bypasses most trigger activities.
                            
            Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Id = mergeMappingId);//[Select id,name from Talent_Merge_Mapping__c Where Contact_Survivor_Id__c = :masterContactId  limit 1];// instantiating with external ID instead of salesforce ID - removes the need to query tmm record

            /*// To filter out duplicate orders for a contact
            Map<String, Order> mapDupContOrder = new Map<String, Order>();
                    //adding the order merge logic 
            For(Order dupOrder : [select id, Account.Name, ShipToContact.AccountId, Opportunity.Name, Opportunity.Id from Order where ShipToContact.AccountId =: duplicateAccountId And OpportunityId != null  FOR UPDATE ]){
             //   String dupeString = String.valueOf(masterContactId) + String.valueOf(dupOrder.Opportunity.Id);
               String dupeString = masterContactId + '' + dupOrder.Opportunity.Id;
System.debug('dupeString: '+ dupeString);
                if(!mapDupContOrder.containsKey(dupeString)){
                    dupOrder.ShipToContactId = masterContactId;
                    dupOrder.Unique_Id__c = dupeString;
                    orderList.add(dupOrder);
                    mapDupContOrder.put(dupeString,dupOrder);
                }                       
            }*/
			List<Order> orderList = new List<Order>();
			List<Order> deleteOrderList = new List<Order>();
			Map<String, Order> masterOrderMap = TalentMergeHandler.getMasterOrdersMap(masterContact);
			String sameESFOrders = '';
			String validate = '';
			//merge master contact;
			For(Order dupOrder : [select id, Account.Name, ShipToContact.AccountId, Opportunity.Name, Opportunity.Id, Start_Form_ID__c from Order where ShipToContactId =: duplicateContactId]){
				Order sameOrderOnMaster = masterOrderMap.get(String.valueOf(dupOrder.Opportunity.Id));
				if (sameOrderOnMaster == null) {
					// This order is not on master contact. Reparent.
					/* dupOrder.ShipToContactId = masterContactToUpdate.Id;
					dupOrder.Unique_Id__c = String.valueOf(masterContactToUpdate.Id) + String.valueOf(dupOrder.Opportunity.Id);*/
					orderList.add(reparentOrder(dupOrder, masterContact.Id));
				} else {
					// Order on master has same opportunity id. 
					// Mark for deletion the one that does not have Start_Form_ID__c. If deleting master order, reparent the dupe. 
					if (String.isBlank(dupOrder.Start_Form_ID__c) || sameOrderOnMaster.Start_Form_ID__c == dupOrder.Start_Form_ID__c) {
						deleteOrderList.add(dupOrder);
					} else if (String.isBlank(sameOrderOnMaster.Start_Form_ID__c)) {
						orderList.add(reparentOrder(dupOrder, masterContact.Id));
						deleteOrderList.add(sameOrderOnMaster);
					} else {
						// Both orders have different ESF ids. Do not proceed with merge.
						sameESFOrders += ' [' + sameOrderOnMaster.Id + ',' + dupOrder.Id + ']';
					}
				}
			}

			if (String.isNotEmpty(sameESFOrders)) {
				tmm.Result__c='FAIL-EXCEPTION';
                tmm.Result_Reason__c = 'Unable to complete merge. Both records have Submittals ' + sameESFOrders + ' with different ESF/SIF ID.  Contact the Service Desk for assistance.';
                tmm.Fetched__c = true;
                update tmm;
				return String.valueOf(masterContactId)+'-'+tmm.Result__c;
			}
            
            savePoint sp1  = Database.setSavepoint();
            

            try {
                 System.debug('Printing on console');
                 System.debug(masterAccount);
                 System.debug(duplicateAccountId);
				 
				 //S-142098 Delete duplicate work history for duplicate record.
				 TalentMergeHandler.deleteDuplicateWorkHistory(duplicateAccountId,masterAccountId);
				 if (deleteOrderList.size() > 0) {
					delete deleteOrderList;
				 }
				 if(orderList.size() > 0)
					update orderList;

                 Database.MergeResult db1 = Database.merge(masterAccount, duplicateAccountId);
                 if(db1.isSuccess()){
                        System.debug('Printing on console contacts');
                 System.debug(masterContact);
                 System.debug(duplicateContactId);
                        Database.MergeResult db2 = Database.merge(masterContact, duplicateContactId);
                        if(db2.isSuccess()){
                                if(orderList.size() > 0) {
                                   update orderList;
                                }
                                 //write to talent merge object 'Success Status'
                                 tmm.Result__c = 'SUCCESS';
                                 tmm.Account_Merge_Status__c = PostStr;
                                 tmm.Contact_Merge_Status__c = PostStr; 
                                 tmm.Result_Reason__c = null; 
                                 masterAccount.Merge_State__c = PostStr;
                                 masterContact.Merge_State__c = PostStr;
                                 Update masterAccount;
                                 update masterContact; 
                        } else {
                                // write db2 failure status to talent merge object
                                tmm.Result__c = 'FAIL-CONTACT';
                                tmm.Result_Reason__c = String.valueOf(db2.errors[0]);
                                Database.rollback(sp1);
                        }
                 } else {
                         // write db1 failure status to talent merge object
                         tmm.Result__c = 'FAIL-ACCOUNT';
                         tmm.Result_Reason__c = String.valueOf(db1.errors[0]);
                          Database.rollback(sp1);
                 } 
                 tmm.Fetched__c = true; 
                 update tmm;
            } catch(Exception e) {
                    Database.rollback(sp1);
                    tmm.Result__c='FAIL-EXCEPTION';
                    tmm.Result_Reason__c = e.getMessage();
                    tmm.Fetched__c = true;
                    update tmm;
             }  

           return String.valueOf(masterContactId)+'-'+tmm.Result__c;
	}

	private static Map<String, Order> getMasterOrdersMap(Contact masterC) {
        Map<String, Order> masterOrderMap = new Map<String, Order>();
        for (Order masterO : [select id, Account.Name, ShipToContact.AccountId, Opportunity.Name, Opportunity.Id, LastModifiedDate, Start_Form_ID__c from Order where ShipToContactId =: masterC.Id]) {
            masterOrderMap.put(String.valueOf(masterO.Opportunity.Id), masterO);
        }

        return masterOrderMap;
    }

    private static Order reparentOrder(Order o, Id masterContactId) {
        o.ShipToContactId = masterContactId;
        o.Unique_Id__c = String.valueOf(masterContactId) + String.valueOf(o.Opportunity.Id);
        return o;
    }

	public static void deleteDuplicateWorkHistory(String duplicateAccountId, String masterAccountId){
       
		List<Talent_Work_History__c> workHistoryForDuplicate  = new List<Talent_Work_History__c>([select id, sourceId__c FROM Talent_Work_History__c WHERE talent__c =: duplicateAccountId]);
		List<Talent_Work_History__c> workHistoryForMaster  = new List<Talent_Work_History__c>([select id, sourceId__c FROM Talent_Work_History__c WHERE talent__c =: masterAccountId]);
		List<Talent_Work_History__c> workHistoryToDelete = new List<Talent_Work_History__c>();

		if (workHistoryForMaster.size() > 0 && workHistoryForDuplicate.size() > 0) {
			for (Talent_Work_History__c master : workHistoryForMaster) {
				for(Talent_Work_History__c dupe : workHistoryForDuplicate) {
					if (master.sourceId__c == dupe.sourceId__c){
						workHistoryToDelete.add(dupe);
					}
				}
			}
			delete workHistoryToDelete;
		}
		
    }

}