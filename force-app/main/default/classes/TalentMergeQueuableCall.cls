public class TalentMergeQueuableCall  implements Queueable { 

   TalentMerge_RecordList_JSON recordListJSON;

   public TalentMergeQueuableCall(TalentMerge_RecordList_JSON recordListJSON) {
      this.recordListJSON = recordListJSON;
   } 

   public void execute(QueueableContext context) {
       try {
              for ( TalentMerge_RecordList_JSON.RecordIds  recordIds : recordListJSON.recordList) {
                            
                    TalentMergeHandler.handleMerge(recordIds.masterAccountId, recordIds.duplicateAccountId, recordIds.masterContactId, recordIds.duplicateContactId, recordIds.mergeMappingId);
               }
       } catch (Exception e) {
            System.debug(logginglevel.ERROR, ' Failed to parse merged Ids !  '  + e.getMessage());
       }
    }

  
}