@RestResource(urlMapping='/TalentMergeRESTHelper/*')
global with sharing class TalentMergeRESTHanldler  {


   public class RequestListJSON {
        TalentMerge_RecordList_JSON[] requestList; 
   }


    @HttpPost
    global static void doPost() {

        Long startTime = DateTime.now().getTime();    

        String body = RestContext.request.requestBody.toString();
        //system.debug('body:  ' + body);

        body = JSON.serializePretty(JSON.deserializeUntyped(body));
        RequestListJSON tmJSON = (RequestListJSON)JSON.deserialize(body, RequestListJSON.class);

        for (TalentMerge_RecordList_JSON recordList : tmJSON.requestList) {
             System.enqueueJob(new TalentMergeQueuableCall(recordList));
        }  

        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeNumberField('NumberOfQueuedJobs',  getQueuedAsyncApexJob());
        gen.writeNumberField('TransactionTimeMilSec',  (DateTime.now().getTime() -  startTime));
        gen.writeEndObject();
        RestContext.response.responseBody = Blob.valueOf(gen.getAsString()); 
   } 


   public static Integer getQueuedAsyncApexJob() {
       return  Database.countQuery('select count()  FROM AsyncApexJob where ApexClass.Name = \'TalentMergeQueuableCall\' and (Status=\'Queued\' OR Status=\'Processing\') ');
  }


}