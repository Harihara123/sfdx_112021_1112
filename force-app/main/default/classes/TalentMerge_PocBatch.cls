global class TalentMerge_PocBatch implements Database.Batchable<SObject> {

    global final String Query;
	
	global TalentMerge_PocBatch(String q) {
      Query = q;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
     *  String query = 'SELECT Account_Survivor_Id__c, Account_Victim_Id__c, Contact_Survivor_Id__c, Contact_Victim_Id__c, Id, Name FROM Talent_Merge_Mapping__c where Batch_Id__c = ?  and Fetched__c = null limit 50000';
     *  Id batchInstanceId = Database.executeBatch(new TalentMerge_PocBatch(query), 200);
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(Query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Talent_Merge_Mapping__c> scope) {

     System.enqueueJob(new TalentMerge_PocQueue(scope));   
	
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}


    public class TalentMerge_PocQueue implements Queueable {

        private List<Talent_Merge_Mapping__c> talentMerge {get;set;}
                 
        public TalentMerge_PocQueue(List<Talent_Merge_Mapping__c> talentMerge){

           this.talentMerge = talentMerge; 
        }
        
        public void execute(QueueableContext context) {
                if(!Test.isRunningTest()) {
                     for (Talent_Merge_Mapping__c obj  : talentMerge) {
                           TalentMergeHandler.handleMerge(obj.Account_Survivor_Id__c,  obj.Account_Victim_Id__c,  obj.Contact_Survivor_Id__c, obj.Contact_Victim_Id__c, obj.Id); 
                   }  
              }
        }  
    } 
}