public class TalentMerge_RecordList_JSON  {

      public RecordIds[] recordList;  

      public class RecordIds  {

         public String masterAccountId;
         public String duplicateAccountId;
         public String masterContactId;
         public String duplicateContactId;
         public String mergeMappingId;
    }
}