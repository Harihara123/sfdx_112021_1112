// batchable class to handle removing the talent Ids from Search index for talent merge
/*
*  user the following syntac to run the code to start the batch 
*  String query = 'SELECT Id, ESearch_Dup_Id__c, ESearch_Fetched__c FROM TalentMerge_RemoveDup__c where ESearch_Fetched__c = null limit 10000';
*  Id batchInstanceId = Database.executeBatch(new TalentMerge_Remove_ESearch_PocBatch(query), 100);
*
*/

global class TalentMerge_Remove_ESearch_PocBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    global final String Query;

    global TalentMerge_Remove_ESearch_PocBatch(String Query) {

        this.Query = Query;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC,  List<Sobject> objectList) {

                if(!Test.isRunningTest()) {
                     for (SObject obj  : objectList) {
                            String responseBody;
                            String ServiceUrl;
                            Http connection = new Http();
                            HttpRequest req = new HttpRequest();
                            ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Remove Talent');
                            req.setMethod(serviceSettings.Service_Http_Method__c);
                            String timeOutvalue = Label.ATS_SEARCH_TIMEOUT;
                            req.setTimeout(Integer.valueOf(timeOutvalue));
                            req.setHeader('Authorization', 'Bearer ' + serviceSettings.OAuth_Token__c);  

                            ServiceUrl = serviceSettings.Service_URL__c.replace('{ID}', (String)obj.get('ESearch_Dup_Id__c') );
                            req.setEndpoint( ServiceUrl );
                                                                    
                            try {
                                   HttpResponse response = connection.send(req);
                                   System.debug('RESPONSE++++=========> ' + response);
                                   responseBody = response.getBody();
                                   System.debug('RESPONSEBODY++++=====> ' + responseBody);
                                   if (response.getStatusCode() == 200) {
                                       obj.put('ESearch_Fetched__c', true);
                                   } else {
                                          System.debug(LoggingLevel.ERROR, ' Failed to update record ! Id : ' + (String)obj.get('ESearch_Dup_Id__c'));
                                         obj.put('ESearch_Fetched__c', false);
                                   }
                            } catch (Exception ex) {
                                    System.debug(LoggingLevel.ERROR, 'Apigee call to Req Search Service failed! ' + ex.getMessage());
                                   // throw ex;
                                    obj.put('ESearch_Fetched__c', false);
                           }  
                   }  
                   update objectList;
              }  
    }
    
    global void finish(Database.BatchableContext BC) { }


}