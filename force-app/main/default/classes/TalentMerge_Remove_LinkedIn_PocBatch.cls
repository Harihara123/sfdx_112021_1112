global class TalentMerge_Remove_LinkedIn_PocBatch implements Database.Batchable<SObject> , Database.AllowsCallouts {

    global final String Query;

   private static WebServiceRequestInterface WebServiceRequestDI = new WebServiceRequest();


	
	global TalentMerge_Remove_LinkedIn_PocBatch(String Query) {
		this.Query = Query;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(Query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<TalentMerge_RemoveDupLInkedIn__c> scope) {

        if(!Test.isRunningTest()) {
                 Boolean isSandbox = false; // For Talent Merge We only will hit prod, in load we will test 1 record pointing to prod !           
                 LinkedInEnvironment envVariable = LinkedInUtility.getEnvironmentName(isSandbox); 
                 LinkedInIntegration__mdt configItems = LinkedInUtility.getLinkedInConfiguration(envVariable);
                 String integrationContext = configItems.IntegrationContext__c;
                 String authToken = LinkedInUtility.createAuthorizationHeader(isSandbox);
                 deleteCandidateTalentToLinkedIn(scope, isSandbox, integrationContext, authToken);   
           }  
    }  
    
	

	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}

     


    private static void deleteCandidateTalentToLinkedIn(List<TalentMerge_RemoveDupLInkedIn__c> scope, Boolean isSandbox, String integrationContext, String authToken) { 

                String syncCandidateEndPoint = getQueryString(scope,  integrationContext);

                try {
                        HttpRequest newRequest = new HttpRequest();
                        newRequest.setHeader('Authorization', authToken);   
                        WebServiceResponse response = WebServiceRequestDI.deleteJson(syncCandidateEndPoint, newRequest);
                        System.debug(response.statusCode);
                        if (response.StatusCode == 200) {
                              setFetchedValue(scope, true);
                        } else {
                              System.debug(LoggingLevel.ERROR, ' Failed to update record ! Id : ');
                              setFetchedValue(scope, false);
                       }
                 } catch (Exception ex) {
                       System.debug(LoggingLevel.ERROR, 'Apigee call to Req Search Service failed! ' + ex.getMessage());
                       setFetchedValue(scope, false);
                 } 
                update scope; 

}


    private static string  getQueryString(List<TalentMerge_RemoveDupLInkedIn__c> scope, String integrationContext) {

        String qStr = '';
        Integer index = 0; 
        for (; index <  scope.size(); ++index) {
            String prefix = 'ids['  + index  + ']';
            qStr =  qStr  + prefix + '.atsCandidateId=' + scope[index].Linkedin_Dup_Id__c + '&' + prefix + '.dataProvider=ATS&' + prefix + '.integrationContext=' + integrationContext;
            qStr =  qStr  +  (index <  scope.size() ? '&' : '');                
        }
        return  'https://api.linkedin.com/v2/atsCandidates?' + qStr;
    }

    private static void setFetchedValue(List<TalentMerge_RemoveDupLInkedIn__c> scope, boolean status) {

          for (TalentMerge_RemoveDupLInkedIn__c tm : scope) {
                tm.LinkedIn_Fecthed__c = status;
          }
          update scope; 
    }

}