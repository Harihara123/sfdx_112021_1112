public class TalentMerge_ReqParam  {

 public TalentMerge_RecordList[] recordList;

 public class TalentMerge_RecordList {
       public TalentMerge_RecordIds recordIds;

    }

 public class TalentMerge_RecordIds {
        public String AccountMasterId;
        public String AccountDupId;
        public String ContactMasterId;
        public String ContactDupId;
        public String MergeMapId;
    } 

}