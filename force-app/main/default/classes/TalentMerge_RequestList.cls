public class TalentMerge_RequestList  {

     public TalentMerge_RecordList_JSON[] recordList;  

     public class TalentMerge_RecordList_JSON  {

         public String masterAccountId;
         public String duplicateAccountId;
         public String masterContactId;
         public String duplicateContactId;
         public String mergeMappingId;
    }

}