@RestResource(urlMapping='/TalentMerge/Update/FetchInputData/*')
global with sharing class TalentMerge_Update_FetchDataRestHandler  {


  private static String selectStr = 'select Account_Survivor_Id__c, Account_Victim_Id__c, Contact_Survivor_Id__c, Contact_Victim_Id__c, Id from Talent_Merge_Mapping__c where ' 
                                        + TalentMerge__c.getInstance('Param').Acc_Cont_Update_Query__c;

    @HttpPost
    global static void doPost(Integer numberOfRecords, Integer BatchId) {

        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField('recordList', Database.query(selectStr)); 
        gen.writeEndObject();
        RestContext.response.responseBody = Blob.valueOf(gen.getAsString()); 
   } 


}