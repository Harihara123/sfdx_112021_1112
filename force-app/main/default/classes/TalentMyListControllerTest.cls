@isTest
public class TalentMyListControllerTest  {
 @isTest
    public static void currentUserLists_givenUserWithLists() {
        Account acct = new Account(Name='SFDC Account');
    insert acct;
// Once the account is inserted, the sObject will be
    // populated with an ID.
    // Get this ID.
    ID acctID = acct.ID;
    // Add a contact to this account.
    Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='8989898989',
        AccountId=acctID);
		insert con;
		ID cntId=con.ID;
		 Tag_Definition__c testList = new Tag_Definition__c();
		testList.Tag_Name__c = 'Test List';
		insert testList;

		ID tagDefId=testList.ID;

		Contact_Tag__c cntTagList = new Contact_Tag__c();
		cntTagList.Contact__c=cntId;
		cntTagList.Tag_Definition__c=tagDefId;
		insert cntTagList;
		System.assertEquals(cntTagList.Tag_Definition__c,tagDefId);

        Test.startTest();
            List<SObject> resultLists = new List<SObject>();
            resultLists = TalentMyListsController.getTalentInMyLists(cntId);
			System.debug('Size of the final result:'+resultLists.size());
           // System.assertEquals(1, resultLists.size(), 'The inserted list should have been returned.');
        Test.stopTest();
    }

	@isTest
    public static void noContactListData_returnsContactID() {
        Account acct = new Account(Name='SFDC Account3');
    insert acct; 
	ID acctID = acct.ID;
    // Add a contact to this account.
    Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='8989898989',
        AccountId=acctID);
		insert con;
		ID cntId=con.ID;
		
		 Test.startTest();
            List<SObject> resultLists1 = new List<SObject>();
            resultLists1 = TalentMyListsController.getTalentInMyLists(cntId);
			System.debug('Size of the final result:'+resultLists1.size());
            System.assertEquals(1, resultLists1.size());
        	resultLists1 = TalentMyListsController.getTalentInMyLists(null);
        Test.stopTest();
	}

	@isTest
		public static void Delete_ContactTagRcrd() {
		   Account acct = new Account(Name='SFDC Account2');
		insert acct;
	// Once the account is inserted, the sObject will be
		// populated with an ID.
		// Get this ID.
		ID acctID = acct.ID;
		// Add a contact to this account.
		Contact con = new Contact(
			FirstName='Joe',
			LastName='Smith',
			Phone='8989898989',
			AccountId=acctID);
			insert con;
			ID cntId=con.ID;
			 Tag_Definition__c testList = new Tag_Definition__c();
			testList.Tag_Name__c = 'Test List1';
			insert testList;

			ID tagDefId=testList.ID;
			System.assertEquals(testList.Tag_Name__c ,'Test List1');
			Contact_Tag__c cntTagList = new Contact_Tag__c();
			cntTagList.Contact__c=cntId;
			cntTagList.Tag_Definition__c=tagDefId;
			insert cntTagList;
			
			List<String> lstRecordId = new List<String>();
			lstRecordId.add(cntTagList.Id);
			
			Test.startTest();
				TalentMyListsController.deleteContactTagRecords(lstRecordId);				
			Test.stopTest();
	}
	@isTest
		public static void SetNDeleteContactTagNote() {
		   Account acct = new Account(Name='SFDC Account2');
		insert acct;
	// Once the account is inserted, the sObject will be
		// populated with an ID.
		// Get this ID.
		ID acctID = acct.ID;
		// Add a contact to this account.
		Contact con = new Contact(
			FirstName='Joe',
			LastName='Smith',
			Phone='8989898989',
			AccountId=acctID);
			insert con;
			ID cntId=con.ID;
			 Tag_Definition__c testList = new Tag_Definition__c();
			testList.Tag_Name__c = 'Test List1';
			insert testList;

			ID tagDefId=testList.ID;

			Contact_Tag__c cntTagList = new Contact_Tag__c();
			cntTagList.Contact__c=cntId;
			cntTagList.Tag_Definition__c=tagDefId;
			insert cntTagList;

			List<String> lstRecordId = new List<String>();
			lstRecordId.add(cntTagList.Id);
			System.assertEquals(testList.Tag_Name__c ,'Test List1');
			Test.startTest();
				TalentMyListsController.setNotesInContactTag('SampleNotes', cntTagList.Id);
				TalentMyListsController.deleteContactTagNotes(cntTagList.Id);
			Test.stopTest();
	}

}