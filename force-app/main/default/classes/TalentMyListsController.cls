public class TalentMyListsController  {
    
         @AuraEnabled
    public static List<SObject> getTalentInMyLists (Id contactId) {
		List<Contact_Tag__c> lstTalentInMyList = new List<Contact_Tag__c>();
		List<Contact> Contact = new List<Contact>();
		if(contactId != null) {
			try{
	
				String soqlQuery = 'SELECT Notes__c,Tag_Definition__r.LastModifiedDate,Tag_Definition__r.Tag_Name__c,  Contact__r.Name,Contact__r.Record_Type_Name__c  from Contact_Tag__c WHERE Contact__c = ' + 
									'\'' + contactId + '\''+'and Tag_Definition__r.OwnerId='+'\''+UserInfo.getUserID()+'\'';
                
                system.debug(soqlQuery+':This is the query');
				lstTalentInMyList =  Database.query(soqlQuery);
				System.debug('Size of query'+ lstTalentInMyList.size());
				if(lstTalentInMyList.size()==0)
				{
				soqlQuery = 'SELECT Full_Name__c,Record_Type_Name__c FROM Contact WHERE Id ='+ 
									'\'' + contactId + '\'';
				Contact =  Database.query(soqlQuery);
				return Contact;
				}
				else{
					return lstTalentInMyList;
				}
				
			}
			catch(QueryException qe){
				System.debug('The following exception has occured:'+ qe.getMessage());
				return lstTalentInMyList;
				}
		}
		else{
			System.debug('Contact Id is not present for Query Talent List details on Landing page');
			return lstTalentInMyList;	

		}
			}
    
@AuraEnabled
 public static void deleteContactTagRecords(List<String> lstRecordId) {
     system.debug('Number of Records:'+lstRecordId);
  // for store Error Messages  
  List <String> oErrorMsg = new List <String>();
  // Query Records for delete where id in lstRecordId [which is pass from client side controller] 
 if(lstRecordId != null) {
			
	
				List<Contact_Tag__c> lstDeleteRec =[select Id from Contact_Tag__c where Id IN:lstRecordId];
     
     system.debug('List of records that are return:'+lstDeleteRec);
				Database.DeleteResult[] DR_Dels  =  Database.delete(lstDeleteRec,false);
				
     
  
  // delte contact list with Database.DeleteResult[] database class.
  // It deletes some queried contacts using <samp class="codeph apex_code">Database.<span class="statement">delete</span></samp> 
  // with a false second parameter to allow partial processing of records on failure.
  // Next, it iterates through the results to determine whether the operation was successful or not
  // for each record. and check if delete contact successful so print msg on debug, 
  // else add error message to oErrorMsg List and return the list  
 
  // Iterate through each returned result
  for (Database.DeleteResult dr: DR_Dels) {
   if (dr.isSuccess()) {
      system.debug('successful delete contact');
     // Operation was successful
   } else {
    // Operation failed, so get all errors   
    oErrorMsg.add('');
    for (Database.Error err: dr.getErrors()) {
     // add Error message to oErrorMsg list and return the list
     oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
    }
   }
  }
  //return oErrorMsg;
  system.debug(oErrorMsg);
 }
}
    /*CHANGED*/
     @AuraEnabled
   public static string setNotesInContactTag(String noteText, String tagId){
		System.debug('noteText=='+noteText+'tagId=='+tagId);
        List<Contact_Tag__c> contactList = [select id, Notes__c from Contact_Tag__c where Id =:tagId  WITH SECURITY_ENFORCED limit 1];
        if (contactList.size() > 0) {
            contactList[0].Notes__c = noteText;
            update contactList[0];
			return 'Success';
        }else{
			return 'Failed';
		}
        
   }

     @AuraEnabled
   public static string deleteContactTagNotes(String deleteRecordsIds){
		System.debug('deleteRecordsIds=='+deleteRecordsIds);
        List<Contact_Tag__c> contactList = [select id, Notes__c from Contact_Tag__c where Id =:deleteRecordsIds WITH SECURITY_ENFORCED  limit 1];
        if (contactList.size() > 0) {
            contactList[0].Notes__c = '';
            update contactList[0];
			return 'Success';
        }else{
			return 'Failed';
		}
        
   }
    /*CHANGED*/
}