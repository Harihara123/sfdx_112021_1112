/**
 * Author: Ajay Panachickal
 * Date: 28-Jun-2016
 * 
 * Class representing the structure of Account.Talent_Preference_Internal__c.
 * Used when parsing the JSON preferences in Apex.
 * 
 */
public class TalentPreferenceInternal {
	public class Employability {
		public Boolean reliableTransportation;
		public String[] eligibleIn;
		public Boolean drugTest;
		public Boolean backgroundCheck;
		public Boolean securityClearance;
	}
	

	public class DesiredLocation {
		public String city;
		public String state;
		public String country;
	}

	public class CommuteLength {
		public String distance;
		public String unit;
	}

	public class GeoPreferences {
		public List<DesiredLocation> desiredLocation;//changed by akshay 10/25/17 for S - 41845
		public CommuteLength commuteLength;
		public Boolean nationalOpportunities;
	}
	
	public List<String> languages;
    public Employability employabilityInformation;
    public GeoPreferences geographicPrefs;
  
    public static TalentPreferenceInternal newPrefsJson() {
        TalentPreferenceInternal prefs = new TalentPreferenceInternal();
      
        prefs.languages = new List<String>();
       
        Employability emp = new Employability();
        emp.backgroundCheck = null;
        emp.drugTest = null;
        emp.reliableTransportation = null;
        emp.securityClearance = null;
        emp.eligibleIn = new List<String>();
        prefs.employabilityInformation = emp;
        
     
        
        GeoPreferences gprefs = new GeoPreferences();
        gprefs.nationalOpportunities = null;
        gprefs.commuteLength = new CommuteLength();
        //added by akshay 10/25/17 for S - 41845 start
        DesiredLocation Dl = new DesiredLocation();
        Dl.city='';
        Dl.state='';
        Dl.country='';
        gprefs.desiredLocation = new List<DesiredLocation>();//changed by akshay 10/25/17 for S - 41845
        gprefs.desiredLocation.add(Dl);
        //added by akshay 10/25/17 for S - 41845 end
        prefs.geographicPrefs = gprefs;
        

        return prefs;
    }


}