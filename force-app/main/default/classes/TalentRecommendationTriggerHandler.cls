/*******************************************************************
Name        : TalentRecommendationTriggerHandler
Created By  : Ajay Panachickal
Date        : 6/8/2016
Story/Task  : ATS-1288
Purpose     : Class that contains all of the functionality called by the
              TalentRecommendationTrigger. All contexts should be in this class.
********************************************************************/
public with sharing class TalentRecommendationTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, Talent_Recommendation__c> oldMap = new Map<Id, Talent_Recommendation__c>();
    Map<Id, Talent_Recommendation__c> newMap = new Map<Id, Talent_Recommendation__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Talent_Recommendation__c> newRecs) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Talent_Recommendation__c> newRecs) {
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            trg_Update_Talent_LastModified(afterInsert, oldMap, newRecs);
        }
        callDataExport(newRecs);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Talent_Recommendation__c>oldMap, Map<Id, Talent_Recommendation__c>newMap) {
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Talent_Recommendation__c>oldMap, Map<Id, Talent_Recommendation__c>newMap) {
        List<Talent_Recommendation__c> newRecs = newMap.values();
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            trg_Update_Talent_LastModified(afterUpdate, oldMap, newRecs);
        }
        callDataExport(newRecs);
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Talent_Recommendation__c>oldMap) {
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Talent_Recommendation__c>oldMap) {
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            List<Talent_Recommendation__c> newRecs = new List<Talent_Recommendation__c>();
            trg_Update_Talent_LastModified(afterDelete, oldMap, newRecs);
        }
        callDataExport(oldMap.values());
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, Talent_Recommendation__c>oldMap) {
    }

        /***************************************************************************************************************************************
    * Description - Method used after insert / update / delete to update the TalentLastModified By And Date 
    *               when Talent_Recommendation__c is inserted / updated / deleted.
    * 
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Ajay Panachickal            6/8/2016               Created

    *****************************************************************************************************************************************/
    public void trg_Update_Talent_LastModified (String Context, Map<Id, Talent_Recommendation__c>oldMap, List<Talent_Recommendation__c>TriggerNew) {
        Set<Id> setAccountids = new Set<Id>();
        List<Account> accountsToUpdate = new List<Account>();
        List<Task> refTasks = new List<Task>();
        //S-32578 Added By Karthik
        Set<Id> accountIDs = new Set<Id>();  //To loop Reference check accounts
        Map<Id,Id> accConIds = new Map<Id,Id>(); //Map to capture Account, Contact Ids
        List<Talent_Recommendation__c> TriggerNewUpdate = [Select id, Comments__c, Talent_Contact__r.AccountId, Reference_Check_Completed__c from Talent_Recommendation__c where Id IN: TriggerNew];
        if (!Context.equals(afterDelete)) {
            
            for (Talent_Recommendation__c talentRecco : TriggerNewUpdate) {
                setAccountids.add(talentRecco.Talent_Contact__r.AccountId);
                //S-32578 Added By Karthik
                system.debug('###### Talent_Contact ########'+talentRecco.Talent_Contact__r.AccountId);
                if(oldMap.get(talentRecco.Id) != null){
                    if(talentRecco.Reference_Check_Completed__c && 
                            oldMap.get(talentRecco.Id).Reference_Check_Completed__c != talentRecco.Reference_Check_Completed__c
                            ){
                        accountIDs.add(talentRecco.Talent_Contact__r.AccountId);
                    }
                 }
                 else{
                     if(talentRecco.Reference_Check_Completed__c){
                        accountIDs.add(talentRecco.Talent_Contact__r.AccountId);
                     }
                 }   
            }
        } 
        else {
            List<Talent_Recommendation__c> deletedTalentExps = [Select id, Comments__c, Talent_Contact__r.AccountId, Reference_Check_Completed__c from Talent_Recommendation__c where Id IN: oldMap.values()];
            for (Talent_Recommendation__c talentRecco : deletedTalentExps) {
                setAccountids.add(talentRecco.Talent_Contact__r.AccountId);
            }
        }
        
        //S-32578 Added By Karthik
        //Create Reference Check task on click of 'Save And Complete' on Talent Recommendation
        for(Account acc : [Select ID, (Select Id,AccountID from Contacts limit 1) from Account where ID in: accountIDs]){
            for(Contact c : acc.contacts){
                accConIds.put(c.AccountID, c.Id);
            }
            //system.debug('::accConIds::'+accConIds);
        }
        
        for (Talent_Recommendation__c talentRecco : TriggerNewUpdate) {
            if(accConIds.containsKey(talentRecco.Talent_Contact__r.AccountId)){
                Task t = new Task();
                t.OwnerID = Userinfo.getUserId();
                t.Subject = 'Reference Check Completed';
                t.Type = 'Reference Check';
                t.Priority = 'Normal';
                t.WhatId = talentRecco.Talent_Contact__r.AccountId;
                t.WhoId = accConIds.get(talentRecco.Talent_Contact__r.AccountId);
                t.ActivityDate = Date.Today();
                t.Status = 'Completed';
                t.Description = talentRecco.Comments__c;
                refTasks.add(t);
            }
        }
        if (refTasks != Null && refTasks.size() > 0) {
                System.debug('!!!!!!!!!!' +refTasks);
                insert refTasks;
        }
        //S-32578 Added By Karthik - End
        
        // Get 'Talent' accounts for the related events.
        String queryAccounts = 'SELECT Id, Talent_Profile_Last_Modified_Date__c, Talent_Profile_Last_Modified_By__c from Account where RecordType.Name = \'Talent\' and Id IN :setAccountids';
        Map<Id, Account> accountMap = new Map<Id, Account>((List<Account>)Database.query(queryAccounts));
        
        for (Account act : accountMap.values()) {
            act.Talent_Profile_Last_Modified_Date__c = System.now();
            act.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();

            accountsToUpdate.add(act);
        }
        
        if (accountsToUpdate != Null && accountsToUpdate.size() > 0) {
            update accountsToUpdate;
        }
    }
    
    // method to sync data with GCP.
    private static void callDataExport(List<Talent_Recommendation__c> newRecords) {
           
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentRecommendationTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' Talent_Recommendation__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Talent_Recommendation__c');
        }

    }

}