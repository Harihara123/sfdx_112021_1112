/***************************
 * Farid Roghani
 * Logs and update Talent Retry Hrxml
 * ************************/

public class TalentRetryLog  {

    public static final String HRXML = 'HRXML';
    public static final String STATUS_OPEN = 'Open';
    public static final String STATUS_SUCCESS = 'Success';
    public static final String STATUS_FAILED = 'Failed';

    public static final String APPLICATION_EXCEPTION = 'APPLICATION_EXCEPTION';



  // log an action retry . example hrxml failures 
  public static void logTalentRetryAction(String actionName,  Map<Id,String> errorMap) {


      List<Talent_Retry__c> talentRetryList = new List<Talent_Retry__c>();
        
      if (errorMap.size() > 0) {

          for (Id recordId : errorMap.keyset()) {

                   Talent_Retry__c tretry = new Talent_Retry__c(Action_Name__c = actionName, 
                                                                Action_Record_Id__c = recordId,
                                                                Status__c   = STATUS_OPEN,
                                                                Description__c = errorMap.get(recordId));

                   talentRetryList.add(tretry);
          }
      }
        
      insert talentRetryList;

  }
    
  public static void logRetryRun(String status, List<ID> talentRetryIds) {

        if (talentRetryIds.size() > 0) {
              Talent_Retry__c[] talentRetry = [SELECT Id FROM Talent_Retry__c WHERE Id in: talentRetryIds]; 
              for (Talent_Retry__c tr : talentRetry) {
                       tr.Status__c = status;    
              }
              update talentRetry;
      }
  }


}