public with sharing class TalentWorkHistoryTriggerHandler  {

   private static boolean hasBeenProccessed = false;


   public void OnAfterInsert(List<Talent_Work_History__c> newRecords) {
       callDataExport(newRecords);

    }


     
    public void OnAfterUpdate (List<Talent_Work_History__c> newRecords) {        
       callDataExport(newRecords);

    }


     public void OnAfterDelete (Map<Id, Talent_Work_History__c> oldMap) {
       callDataExport(oldMap.values());

    }


    private static void callDataExport(List<Talent_Work_History__c> newRecords) {
           
      Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
           if (config.Enable_Trace_Logging__c) {
               ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TalentWorkHistoryTriggerHandler', 'callDataExport', 
                  'Triggered ' + newRecords.size() + ' Talent_Work_History__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
           }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'Talent_Work_History__c');
            hasBeenProccessed = true; 
        }

    }


}