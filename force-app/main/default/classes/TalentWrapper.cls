public class TalentWrapper {
   public class Email {
		public String primary;
		public String work;
		public String other;
		public List<String> unspecified;
	}

	public SourceTracking sourceTracking;
	public TalentLeadActivity talentLeadActivity;
	public Recruiter recruiter;
	public Talent talent;

	public class Address {
		public String streetAddress;
		public String city;
		public String stateProvince;
		public String country;
		public String postalCode;
	}

	public class Phone {
		public String mobile;
		public String work;
		public String home;
		public List<String> unspecified;
	}

	public class SourceTracking {
		public String source;
		public String vendor;
		public String transactionId;
		public String externalCandidateId;
	}

	public class TalentLeadActivity {
		public List<PostingsVisited> postingsVisited;
		public String leadCreatedDate;
		public String leadUpdatedDate;
	}

	public class PostingsVisited {
		public String postingId;
		public String activityDate;
	}

	public class Talent {
		public Profile profile;
		public Contact contact;
		public Address address;
		public Resume resume;
		public Resume attachment;
	}

	public class Recruiter {
		public String firstName;
		public String lastName;
		public String email;
	}

	public class Profile {
		public String firstName;
		public String lastName;
		public String jobTitle;
		public String desiredSalary;
		public String desiredRate;
		public String desiredFrequency;
		public String workEligibility;
		public String geographicPreference;
		public String highestEducation;
		public String securityClearance;
	}

	public class Resume {
		public String fileType;
		public String base64;
	}

	public class Contact {
		public Phone phone;
		public Email email;
	}

	
	public static TalentWrapper parse(String json) {
		return (TalentWrapper) System.JSON.deserialize(json, TalentWrapper.class);
	}

}