public class TargetAccount_OfficeController{
    public List<officeInformation> offices {get;set;}
    //Wrapper
    public class officeInformation{
        public boolean selected{get;set;}
        public User_Organization__c office{get;set;}
        // constructor
        public officeInformation(boolean selected,User_Organization__c office)
        {
            this.selected = selected;
            this.office = office;
        }
    }
    string accountInContext;
    set<String> officeObject = Schema.SObjectType.User_Organization__c.fields.getMap().keyset();
    
    string organization_OfficeRecordType =  Schema.sObjectType.User_Organization__c.getRecordTypeInfosByName().get(Label.Organization_Office_RecordTypeName).getRecordTypeId();
    // constructor
    public TargetAccount_OfficeController()
    {
            offices = new List<officeInformation>();
            accountInContext = ApexPages.currentPage().getParameters().get('accountId');
            boolean selected = false;
            //build the query
            string query = 'select ';
            integer len = query.length();
            //loop over the office fields
            for(string fld : officeObject)
            {
              if(len == query.Length())
                  query += fld;
              else
                  query = query + ',' + fld;
            }
            //append the object name
            query = query + ' from User_Organization__c where Active__c = true and RecordTypeId = \'' + organization_OfficeRecordType +'\' order by OpCo_Code__c,Office_Code__c,Office_Name__c Limit 1000'; 
            // get the offices that are active
            for(User_Organization__c org : database.query(query))
            {
                  officeInformation office = new officeInformation(selected,org);
                  offices.add(office);
            }
    }
    // method to create target accounts
    public pageReference createTargetAccounts()
    {
        List<Target_Account__c> targetAccounts = new List<Target_Account__c>();
        //loop over the offices and pick the ones that are selected
        for(officeInformation office : offices)
        {
            if(office.selected)
            {
                Target_Account__c account = new Target_Account__c(Account__c = accountInContext,Office__c = office.office.Id,OwnerId = userInfo.getUserId(),Name__c = office.office.Office_Name__c);
                targetAccounts.add(account);
            }
        }
        // insert records
        if(targetAccounts.size() > 0)
            database.insert(targetAccounts,false);
        return redirect();    
    }
    public pageReference redirect()
    {
       return new PageReference('/'+accountInContext);
    }    
}