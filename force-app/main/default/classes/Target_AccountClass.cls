/*************************************************************************************************
Apex Class Name :  Target_AccountClass
Version         : 1.0 
Created Date    : 06 MAR 2012
Function        : This class is invoked from the TargetAccount_AddTeamMember Trigger and adds the team members 
                    as soon as a target account record is created.It also deletes the account team members if 
                    there is a delete operation on the Target accounts . 
                  It checks the IS_Target_Account checkbox on the related account.
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dhruv Gupta                03/06/2012              Original Version
* Pavan                      03/27/2012              Updated Indentation
**************************************************************************************************/

public class Target_AccountClass {
         
         /*
            * Method name     : CreateAccountTeamMembers
            * Description     : Creates Account Team members
            * Return Type     : void
            * Parameter       : List<Target_Account__c> lstTargetAccount
         */
         public static void CreateAccountTeamMembers(List<Target_account__c> lstTargetAccount)
         {
                List<AccountTeamMember> lstNewAccMembers      = new List<AccountTeamMember>();   //List to create new account team members 
                List<AccountShare> newShare                   = new List<AccountShare>();        //List to create account share records
                List<Log__c> logRecords                       = new List<Log__c>();              // Collection to maintain log record information
                for(Target_Account__c tarAcct : lstTargetAccount)
                {                  
                    // Create account team member records 
                    AccountTeamMember newAccTeamMember = new AccountTeamMember(); 
                    newAccTeamMember.AccountId         = tarAcct.Account__c; 
                    newAccTeamMember.UserId            = tarAcct.User__c; 
                  //  newAccTeamMember.TeamMemberRole    = Label.Target_Accounts_Team_Role;   
                    newAccTeamMember.TeamMemberRole    = tarAcct.Role__c;                             
                    lstNewAccMembers.add(newAccTeamMember);  
                }
               
                
                Integer counter = 0;  
                if(lstNewAccMembers.size() > 0)
                {
                      for(Database.SaveResult sr : Database.insert(lstNewAccMembers,false))
                      { 
                          if(!sr.isSuccess())
                          { 
                                  // Add logic to insert a log record, invoke method Core_Log.logException(ex) to create the log record
                                  logRecords.add(Core_Log.logException(sr.getErrors()[0]));
                          }
                          else
                          {
                               //    Create share records for each account team members
                               //    Access on account = edit
                               //    Access on opportunity = private
                               newShare.add(new AccountShare(UserOrGroupId = lstNewAccMembers[counter].UserId, AccountId = lstNewAccMembers[counter].Accountid, AccountAccessLevel = Label.Target_Accounts_Account_Access_Level,OpportunityAccessLevel = Label.Target_Accounts_OpportunityAccess));
                          } 
                          counter++; 
                      }
                      // insert the account share records
                      if(newShare.size() > 0)
                      {            
                           // reset the counter
                           counter = 0; 
                           for(Database.SaveResult sr : Database.insert(newShare,false))
                           { 
                                  if(!sr.isSuccess())
                                  { 
                                        //Insert the log record accordingly 
                                        logRecords.add(Core_Log.logException(sr.getErrors()[0]));                   
                                  } 
                                    counter++; 
                           } 
                      }
              }
              // insert log records accordingly
              if(logRecords.size() > 0)
                database.insert(logRecords,false);
      }
      
  
      /*
     * Method name     : DeleteAccountTeamMembers
     * Description     : Delete Account Team members
     * Return Type     : void
     * Parameter       : List<Target_Account__c> lstTargetAccount
     */
    public static void DeleteAccountTeamMembers(List<Target_Account__c> lstTargetAccount)
    {
            System.debug('!!!!!!!in deleteclass'+lstTargetAccount);
            //    Delete the account team members records when the target account records are deleted
            List<AccountTeamMember> lstAccMembersToDel   = new List<AccountTeamMember>();  //list of account team members to delete
            Set<String>lstAccountIds                     = new Set<String>();                   //    set of Account id's
            Set<String>lstUserIds                        = new Set<String>();                   //    set of user id's
            List<Task> tasksToUpdate                     = new List<Task>();            
            List<Log__c> logRecords                      = new List<Log__c>();              // Collection to maintain log record information
            for(Target_Account__c ta : lstTargetAccount)
            {                                   
                  lstAccountIds.add(ta.account__c);
                  lstUserIds.add(ta.User__c);                                                                                
            }  
            // retrieve the tasks related to the accounts and update them accordingly
           /* for(Task task : [select Related_To_Targeted_Account__c from Task where whatId in :lstAccountIds and ownerId in :lstUserIds])
            {
                task.Related_To_Targeted_Account__c = false;
                tasksToUpdate.add(task);
            }*/
            if(tasksToUpdate.size() > 0)
            {
                for(database.saveResult result : database.update(tasksToUpdate,false))
                {
                    if(!result.isSuccess())
                    {
                        //Insert the log record accordingly 
                        logRecords.add(Core_Log.logException(result.getErrors()[0]));  
                    }
                }
            }
            //    retrieve list of account team members based on the AccountIds  and UserIds
            lstAccMembersToDel  = [SELECT Id from AccountTeamMember WHERE AccountID IN :lstAccountIds and UserId IN :lstUserIds LIMIT 50000];
                              
            //    delete the account team members
            if(lstAccMembersToDel.size() > 0)
            {
                  integer counter = 0;
                  for(Database.deleteResult result : Database.delete(lstAccMembersToDel,false))
                  {
                         if(!result.isSuccess())
                         {
                           // insert the log record accordingly
                           logRecords.add(Core_Log.logException(result.getErrors()[0]));
                         }
                       System.debug('!!!!!!!in deleteclass success'+lstAccMembersToDel);  
                  } 
            }
            // insert the log records accordingly
            if(logRecords.size() > 0)
                database.insert(logRecords,false);
    }
    

}