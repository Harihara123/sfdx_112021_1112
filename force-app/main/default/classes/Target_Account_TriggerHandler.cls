/***************************************************************************
Name        : Target_Account_TriggerHandler
Created By  : JFreese (Appirio)
Date        : 18 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : Class that contains all of the functionality called by the
              Target_Account_Trigger. All contexts should be in this class.
*****************************************************************************/

public class Target_Account_TriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, Target_Account__c> oldMap = new Map<Id, Target_Account__c>();

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Target_Account__c> newRecs) {
     //   TargetAccount_AddTeamMember(beforeInsert,oldMap, newRecs); // Commented as this is not required anymore -- Nidish
     UserOwnerSync(newRecs);
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Target_Account__c> newRecs) {
        TargetAccount_AddTeamMember(afterInsert,oldMap, newRecs);
        Call_updateStrategicAccount(afterInsert, oldMap, newRecs);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Target_Account__c>oldMap, Map<Id, Target_Account__c>newMap) {
     List<Target_Account__c> newRecs = newMap.values();
     if(!TriggerStopper.TargetAccountUserOwnerUpdate){
     UserOwnerSync(newRecs);
     }
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Target_Account__c>oldMap, Map<Id, Target_Account__c>newMap) {
        List<Target_Account__c> newRecs = newMap.values();        
        if(!TriggerStopper.TargetAccountUpdate){
        TargetAccount_AddTeamMember(afterUpdate,oldMap, newRecs); 
        }
        if(!TriggerStopper.StrategicAccount){ 
        Call_updateStrategicAccount(afterUpdate, oldMap, newRecs);
        }
    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Target_Account__c>oldMap) {
    TargetAccountDelete(beforeDelete, oldMap, oldMap.values());  
    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Target_Account__c>oldMap) {
        List<Target_Account__c> Records = oldMap.values();
        List<Target_Account__c> newRecs = new List<Target_Account__c>();
        TargetAccount_AddTeamMember(afterDelete,oldMap, Records);        
        Call_updateStrategicAccount(afterDelete, oldMap, newRecs);
    }
    
    
      //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
   public void OnAfterUndelete (Map<Id, Target_Account__c>newMap) {
        List<Target_Account__c> newRecs = newMap.values();
        TargetAccount_AddTeamMember(afterInsert,oldMap, newRecs);
        Call_updateStrategicAccount(afterUndelete, oldMap, newRecs);
    }

    /************************************************************************************************************
    Original Header: trigger TargetAccount_AddTeamMember on Target_Account__c(before insert,after insert,after delete)
    Name        :  TargetAccount_AddTeamMember
    Version     : 1.0
    Description :  This trigger is invoked whenever a target account record is created or deleted from the Targetted
                   By related list of Accounts.List of accounts is sengt as a parameter to Target_AccountClass.
    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                   Description
    *-----------------------------------------------------------------------------
    * Dhruv Gupta               03/01/2012              Original Version
    * Pavan                     03/27/2012              Updated the trigger events
    * Pavan                     05/24/2012             Implemented logic to auto follow targeted accounts
    ***************************************************************************************************************/
    //Move declarations outside method so they can be accessed by the three subsequent methods
    Map<string,set<string>> subscriptionInfo = new Map<string,set<string>>();
    List<Target_Account__c> targetAccountsFiltered = new List<Target_Account__c>();
    List<Target_Account__c> DeleteAccountTeamRecords = new List<Target_Account__c>();
    List<Target_Account__c> DeleteSubscriptionRecords = new List<Target_Account__c>();
    public void  TargetAccount_AddTeamMember(String Context, Map<Id, Target_Account__c>oldMap, List<Target_Account__c> Records){      
     /*   if(Context.equals(beforeInsert))
        {
            filterTargetAccounts(Records);
        //    processTargetedUser();
        }
        else
       { */
            if(Context.equals(afterInsert)){
                filterTargetAccounts(Records);
                if(targetAccountsFiltered.size() > 0)
                {
                    processSubscriptionInfo(targetAccountsFiltered);
                    // create subscriptions accordingly
                    autoFollowHelper.addSubscriptions(subscriptionInfo);
                  //  Target_AccountClass.CreateAccountTeamMembers(targetAccountsFiltered);    // call the Target_AccountClass method // commented and moved to below condition -Nidish
                }
                if(Records.size() > 0){
                Target_AccountClass.CreateAccountTeamMembers(Records);    // call the Target_AccountClass method
                }    
            }else if(Context.equals(afterUpdate)){
                for(Target_Account__c TA : Records){                   
                    if((oldMap.get(TA.Id).Account__c != TA.Account__c ) || (oldMap.get(TA.Id).User__c != TA.User__c)){
                    System.debug('!!!!!!!Userchanged');
                          DeleteAccountTeamRecords.add(oldMap.get(TA.Id));
                    }else if((oldMap.get(TA.Id).Target__c == TRUE)&& (TA.Target__c == FALSE)){
                    System.debug('!!!!!!!TargetChanged');
                            DeleteSubscriptionRecords.add(oldMap.get(TA.Id));
                          }
                    if(TA.target__c){
                            targetAccountsFiltered.add(TA);
                    }                                                         
                }
                if(DeleteAccountTeamRecords.size() > 0){
                    Target_AccountClass.DeleteAccountTeamMembers(DeleteAccountTeamRecords);    // call the Target_AccountClass method
                    processSubscriptionInfo(DeleteAccountTeamRecords);
                    // remove subscriptions
                    autoFollowHelper.removeSubscriptions(subscriptionInfo);
                    subscriptionInfo.clear();                     
                }
                if(DeleteSubscriptionRecords.size() > 0){
                    processSubscriptionInfo(DeleteSubscriptionRecords);
                    // remove subscriptions
                    autoFollowHelper.removeSubscriptions(subscriptionInfo);
                    subscriptionInfo.clear();                                       
                }
                if(targetAccountsFiltered.size() > 0) {
                    processSubscriptionInfo(targetAccountsFiltered);
                    // create subscriptions accordingly
                    autoFollowHelper.addSubscriptions(subscriptionInfo);                
                }
                if(Records.size() > 0){
                Target_AccountClass.CreateAccountTeamMembers(Records);    // call the Target_AccountClass method
                } 
            TriggerStopper.TargetAccountUpdate= true;       
            }else{
                filterTargetAccounts(Records);
                if(targetAccountsFiltered.size() > 0){
                    processSubscriptionInfo(targetAccountsFiltered);
                    // remove subscriptions
                    autoFollowHelper.removeSubscriptions(subscriptionInfo);
                 //   Target_AccountClass.DeleteAccountTeamMembers(targetAccountsFiltered);  // call the Target_AccountClass method // commented and moved to below condition -Nidish                     
                }
                if(Records.size() > 0){
                    Target_AccountClass.DeleteAccountTeamMembers(Records);    // call the Target_AccountClass method
                }
            }
     //   }
    }

    /*******************************************************************
    Method to populate the subscription info map // Updated line 136 and 140. Replaced ownerid with user__c - Nidish.
    ********************************************************************/
    private void processSubscriptionInfo(List<Target_Account__c> accounts){    
    System.debug('!!!!!chatterunfollow!!!!!!!!!!'+accounts);
        for(Target_Account__c acc : accounts)
        {
            if(subscriptionInfo.get(acc.Account__c) != Null)
                subscriptionInfo.get(acc.Account__c).add(acc.User__c);
            else
            {
               set<string> temp = new set<string>();
               temp.add(acc.User__c);
               subscriptionInfo.put(acc.Account__c,temp);
            }
        }
    }
    /*************************************************************************
    Method to process targeted by user name.   // Commenting this method and leveraged the same functionality by formula fields. - Nidish
    **************************************************************************/

  /*  private void processTargetedUser()
    {
        set<string> userIds = new set<string>();
        List<Target_Account__c> filteredAccounts = new List<Target_Account__c>();

        Map<string,User> userMap = new Map<string,User>();

        for(Target_Account__c acc : targetAccountsFiltered)
        {
            userIds.add(acc.ownerId);
            filteredAccounts.add(acc);
        }
        // get the user names
        if(userIds.size() > 0)
        {
            for(User usr : [select Id, Name,Division__c, OPCO__c, Office__c from User where id in :userIds])
               userMap.put(usr.Id,usr);
        }
        //Assign Division and OwnerName accordingly
        for(Target_Account__c acc : filteredAccounts)
        {
            acc.Name__c = userMap.get(acc.ownerId).Name;
            acc.User__c = userMap.get(acc.ownerId).Id;
            acc.User_Division__c = userMap.get(acc.ownerId).Division__c;
            acc.Users_Opco__c = userMap.get(acc.ownerId).OPCO__c;
            acc.Users_Office__c = userMap.get(acc.ownerId).Office__c;
        }
    } */
    /***********************************************************************
    Method to filter out Target accounts that are created for Office // updated the filter to use only Target Records - Nidish.
    *************************************************************************/
    private void filterTargetAccounts(List<Target_Account__c> accounts)
    {
        for(Target_Account__c acc : accounts)
        {
           // if(acc.Office__c == Null)           
            if(acc.target__c){
                targetAccountsFiltered.add(acc);
            }
        }
    }
    
    /***********************************************************************
    Method to pass values updateStrategicAccount -- Nidish
    ***********************************************************************/
    public void Call_updateStrategicAccount (String Context, Map<Id, Target_Account__c>oldMap, List<Target_Account__c>TriggerNew) {
       set<string> associatedAccounts = new set<string>();
       set<string> associatedAccountsTarget = new set<string>();
        //loop over the events
        if(!Context.equals(afterDelete))
        {
            for(Target_Account__c TA : TriggerNew)
            {
                boolean addToList = true;
                boolean addToTargetList = true;
                if(Context.equals(afterUpdate)){
                    if(oldMap.get(TA.Id).Account__c != TA.Account__c){
                          associatedAccounts.add(oldMap.get(TA.Id).Account__c);
                          associatedAccountsTarget.add(oldMap.get(TA.Id).Account__c);
                    }
                    else{
                        if(oldMap.get(TA.Id).MDM__c != TA.MDM__c)
                            addToList = true;
                        else
                            addToList = false;
                      
                      if(Label.Target_Account_DataLoad == 'TRUE'){
                        if((oldMap.get(TA.Id).Target__c != TA.Target__c)|| (oldMap.get(TA.Id).User__c != TA.User__c)) 
                           addToTargetList = true;
                        else
                            addToTargetList = false;
                       }     
                    }
                }else if(Context.equals(afterInsert)){
                    if(TA.Target__c)
                       addToTargetList = true;
                    else
                       addToTargetList = false;

                    if(TA.MDM__c)
                            addToList = true;
                        else
                            addToList = false;
                 }

                //addto collection
                if(addToList)
                   associatedAccounts.add(TA.Account__c);
                if(addToTargetList)
                   associatedAccountsTarget.add(TA.Account__c);   
            }
        }else{
            List<Target_Account__c> TriggerOld = oldMap.values();
            for(Target_Account__c TA : TriggerOld){
               associatedAccounts.add(TA.Account__c);
               associatedAccountsTarget.add(TA.Account__c); 
            }                
        }
         if(associatedAccounts.size() > 0)
           updateStrategicAccount(associatedAccounts);

         if(associatedAccountsTarget.size()>0)
            StampAccount(associatedAccountsTarget); 

       TriggerStopper.StrategicAccount= true;     
    }
    /***********************************************************************
    Method to update strategic account field on account -- Nidish
    *************************************************************************/
    public static void updateStrategicAccount(set<string> AccountAssociations)
    {
        // collection to store the exceptions
        List<Log__c> errorLogs = new List<Log__c>();
        List<Account> Accounts = new List<Account>();
        // query on Accounts and theTarget_Accounts__c for AccountAssociations 
     if(AccountAssociations.size()> 0){
        for(Account Acc :  [select id, StrategicAccount__c,(select id, MDM__c from Target_Accounts__r where MDM__c = TRUE) from Account where id in :AccountAssociations limit :Limits.getLimitQueryRows()]) 
        { 
               if(Acc.Target_Accounts__r.size()>0 && Acc.StrategicAccount__c == False){
               Acc.StrategicAccount__c = TRUE;
               }else if(Acc.Target_Accounts__r.size() == 0 && Acc.StrategicAccount__c == True){
               Acc.StrategicAccount__c = FALSE;
               }
               Accounts.add(Acc);
               System.debug('!!!!!!!!Accounts strategic account to be updated'+Accounts); 
        }
     }    
      
        if(Accounts.size() > 0){ 
          //  database.update(Accounts,false);
            for(database.SaveResult result : database.update(Accounts,false)){
                        // insert the log record accordingly
                        if(!result.isSuccess()){
                             errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                        }
                    }
        }
        
        if(errorLogs.size() > 0)
         database.insert(errorLogs,false);         
    }
    
     /***********************************************************************
    Method to Stamp account with Alias names of target account -- Nidish
    *************************************************************************/
    public static void StampAccount(set<string>AssociatedAccountsTarget)
    {
        // collection to store the exceptions
        list<Log__c> errorLogs = new list<Log__c>();
        list<Account> AccountsToStamp = new list<Account>(); 
        list<String> SplittedString = new list<String>();      
        String Stamp = '';
        // query on Accounts and theTarget_Accounts__c    
     if(AssociatedAccountsTarget.size()>0){
        for(Account Acc :  [select id, Target_Account_Stamp_0__c, Target_Account_Stamp_1__c, Target_Account_Stamp_2__c, Target_Account_Stamp_3__c, Target_Account_Stamp_4__c,(select id, User__r.Alias from Target_Accounts__r where Target__c = TRUE) from Account where id in :AssociatedAccountsTarget limit :Limits.getLimitQueryRows()]) 
        { 
               for(Target_Account__c TAR : Acc.Target_Accounts__r){
                Stamp += ':'+TAR.User__r.Alias;                
               }
               Stamp = Stamp+':';
               System.debug('!!!!!Stamp!!!!!!'+Stamp);
               SplittedString = splitEqually(Stamp,250);
               Acc.Target_Account_Stamp_0__c = Acc.Target_Account_Stamp_1__c = Acc.Target_Account_Stamp_2__c = Acc.Target_Account_Stamp_3__c =Acc.Target_Account_Stamp_4__c = Acc.Target_Account_Stamp_5__c = NULL;
               if(SplittedString.size()>0){
                    if(SplittedString.size() >= 6)
                    Acc.Target_Account_Stamp_5__c = SplittedString[5]; 
                    if(SplittedString.size() >= 5)
                    Acc.Target_Account_Stamp_4__c = SplittedString[4];
                    if(SplittedString.size() >= 4)
                    Acc.Target_Account_Stamp_3__c = SplittedString[3];
                    if(SplittedString.size() >= 3)
                    Acc.Target_Account_Stamp_2__c = SplittedString[2];
                    if(SplittedString.size() >= 2)
                    Acc.Target_Account_Stamp_1__c = SplittedString[1];
                    if(SplittedString.size() >= 1)
                    Acc.Target_Account_Stamp_0__c = SplittedString[0]; 
               } 
               AccountsToStamp.add(Acc);
               Stamp = '';
               System.debug('!!!!!!!!Accounts to be stamped with Alias name'+AccountsToStamp); 
        } 
      }  
        
        if(AccountsToStamp.size() > 0){ 
          //  database.update(Accounts,false);
            for(database.SaveResult result : database.update(AccountsToStamp,false)){
                        // insert the log record accordingly
                        if(!result.isSuccess()){
                             errorLogs.add(Core_Log.logException(result.getErrors()[0]));
                        }
                    }
        }

        if(errorLogs.size() > 0)
         database.insert(errorLogs,false);
    // TriggerStopper.StrategicAccount= true;     
    }
    
    //--------------------------------------------------------------------
    // Validation method to check for MDM and Target flags before delete -Nidish
    //----------------------------------------------------------------------
       public static void TargetAccountDelete(String Context, Map<Id, Target_Account__c>oldMap, List<Target_Account__c> triggerOldRec){
          
           Set<String> ProfileIds = new Set<String>();
           for(Target_Account_Delete_Profiles__c Pr : Target_Account_Delete_Profiles__c.getall().values()){
           ProfileIds.add(Pr.ProfileId__c);
           }           
           for(Target_Account__c PA:triggerOldRec){
              if(PA.MDM__c && UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration && UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Admin && UserInfo.getProfileId().substring(0,15) != Label.Profile_IS_Admin){
                oldMap.get(PA.Id).addError('<span style="color:red"><b>'+Label.MDM_Target_Accounts_Delete_error+'</b></span>',false);
                }else if(PA.Target__c && !ProfileIds.contains(UserInfo.getProfileId().substring(0,15)) && PA.User__c <> UserInfo.getUserId()){
                oldMap.get(PA.Id).addError('<span style="color:red"><b>'+Label.Target_Account_Delete_Error+'</b></span>',false);
                }   
           }
           
       }
    //----------------------------------------------------------------------------
    //Sync user and owner
    //----------------------------------------------------------------------------
    private void UserOwnerSync(List<Target_Account__c> TriggerNew){ 
        for(Target_Account__c TA: TriggerNew){
         if(TA.User__c <> NULL){
            if(TA.OwnerId <> TA.User__c)
                TA.OwnerId = TA.User__c;
            }else{
                TA.addError('<span style="color:red"><b>User cannot be blank</b></span>',false);   
            }
         }   
            
        TriggerStopper.TargetAccountUserOwnerUpdate = true;
    } 

   //----------------------------------------------------------------------------
    //method to Splict string into multiple strings by length - Nidish
    //----------------------------------------------------------------------------
    public static List<String> splitEqually(String text, integer Maxsize) {
    List<String> ret = new List<String>();
    System.debug('@@@@@ret before@@@@@' +ret);
    for (integer start = 0; start < text.length(); start += Maxsize) {
        ret.add(text.substring(start, Math.min(text.length(), start + Maxsize)));
    }
    System.debug('@@@@@ret after@@@@@' +ret);    
    return ret;
}     

}