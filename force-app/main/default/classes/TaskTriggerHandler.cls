/***************************************************************************
Name        : TaskTriggerHandler
Created By  : Vivek Ojha (Appirio)
Date        : 24 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Class that contains all of the functionality called by the
              TaskTrigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class TaskTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, Task> oldMap = new Map<Id, Task>();
    Map<Id, Task> newMap = new Map<Id, Task>();
    static ID accRecordTypeID;
    static ID conRecordTypeID;
     static set<string> ContRecSet =new set<string>{'Client','CSA','Recruiter','Talent'};//Added recordtype Talent for ATS-3539
     public Map<id, Contact> Contmap = new Map<id,Contact>();
     
      // static Set<ID> clientcontactRecTypeIDs;
      
         static Set<ID> clientcontactRecTypeIDs;

    private static boolean hasBeenProccessed = false;


    static {
        accRecordTypeID = Utility.getRecordTypeId('Account', 'Talent');
        conRecordTypeID = Utility.getRecordTypeId('Contact', 'Talent');
        
        //Getting ids of contact record type 'Client','CSA','Recruiter','Talent'
        Map<String,Id> contactRecordTypeMap = Utility.getRecordTypeMap('Contact');
         clientcontactRecTypeIDs = new Set<ID>();
        for(String rec :ContRecSet){
            if(contactRecordTypeMap.get(rec) != null) {
                clientcontactRecTypeIDs.add(contactRecordTypeMap.get(rec));
            }
            }
    }
    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<Task> newRecs) {
        // Skip when the current user profile is 'System Integration'
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            Task_isAssociatedToTargetAccount(beforeInsert, oldMap, newRecs);
            Task_checkAndupdateWhatIdForTalent(beforeInsert, newRecs);
        }
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(List<Task> newRecs, Map<Id, Task> newMap) {
        // Skip when the current user profile is 'System Integration'
        if ((UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) || (UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration && UserInfo.getLastName()==system.label.SOA_Data_M_UserLname)) {            
            if (!TriggerStopper.TaskUpdateTalentLastModified) {
                TriggerStopper.TaskUpdateTalentLastModified = True;
                trg_Update_Talent_LastModified(afterInsert, oldMap, newRecs);
                ActivityHelper.updateLastServiceDateOnContact(newRecs, ContMap);
                 UpdateContact(contmap);
                
            }
        }

        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TaskTriggerHandler', 'OnAfterInsert', 
                    'Triggered ' + newRecs.size() + ' Task records: ' + CDCDataExportUtility.joinObjIds(newRecs));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecs, 'Task');
            hasBeenProccessed = true; 
        }
        
        ContactRoleUtility.createContactRolesForActivities(newMap);
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, Task>oldMap, Map<Id, Task>newMap) {
        // Skip when the current user profile is 'System Integration'
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            List<Task> newRecs = newMap.values();
            Task_isAssociatedToTargetAccount(beforeUpdate, oldMap, newRecs);
        }
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, Task>oldMap, Map<Id, Task>newMap) {
        // Skip when the current user profile is 'System Integration'
        if ((UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) || (UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration && UserInfo.getLastName()==system.label.SOA_Data_M_UserLname)) {            
            if (!TriggerStopper.TaskUpdateTalentLastModified) {
                TriggerStopper.TaskUpdateTalentLastModified = True;
                List<Task> newRecs = newMap.values();
                trg_Update_Talent_LastModified(afterUpdate, oldMap, newRecs);
                ActivityHelper.updateLastServiceDateOnContact(newRecs, ContMap);
                 UpdateContact(contmap);
            }
        }

        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TaskTriggerHandler', 'OnAfterUpdate', 
                    'Triggered ' + newMap.values().size() + ' Task records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'Task');
            hasBeenProccessed = true; 
        }
        


    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, Task>oldMap) {

    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, Task>oldMap) {
        // Skip when the current user profile is 'System Integration'
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
            if (!TriggerStopper.TaskUpdateTalentLastModified) {
                TriggerStopper.TaskUpdateTalentLastModified = True;
                List<Task> oldRecs = new List<Task>();
                trg_Update_Talent_LastModified(afterDelete, oldMap, oldRecs);
                oldRecs = oldMap.values();
                ActivityHelper.updateLastServiceDateOnContact(oldRecs);
            }
        }

        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'TaskTriggerHandler', 'OnAfterDelete', 
                    'Triggered ' + oldMap.values().size() + ' Task records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
            }
            PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'Task');
            hasBeenProccessed = true; 
        }
    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, Task>oldMap) {

    }

    /***************************************************************************************************************************************
    * Description - Method used to check for WhatId on a 'Talent' contact's task. Update if not present.
    *           If task contact (WhoId) is a Talent and Related to (WhatId is null), set WhatId to contact.AccountId
    * 
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Ajay Panachickal            12/28/2016               Created
    *****************************************************************************************************************************************/
    public void Task_checkAndupdateWhatIdForTalent (String Context, List<Task>TriggerNew) {
        Set<Id> setContactids = new Set<Id>();

        if (Context.equals(beforeInsert)) {
            for (Task task : TriggerNew) {
                // Find contacts to update basd on the condition.
                if (task.WhoId != Null && task.WhatId == Null) {
                    setContactIds.add(task.WhoId);
                }
            }                

            String queryContacts = 'Select Id, RecordType.DeveloperName, AccountId from Contact where Id IN :setContactids and RecordType.DeveloperName = \'Talent\'';
            Map<Id, Contact> cMap = new Map<Id, Contact>((List<Contact>)Database.query(queryContacts));
            System.debug(cMap);
            
            for (Task task : TriggerNew) {
                // Lookup Contact in the Map and set What Id on the task.
                if (cMap != Null && cMap.get(task.WhoId) != Null && task.WhoId != Null && task.WhatId == Null) {
                    task.WhatId = cMap.get(task.WhoId).AccountId;
                }
            }
        } 
    }

    /***************************************************************************************************************************************
    * Description - Method used after insert / update / delete to update the TalentLastModified By And Date 
    *               when Task is inserted / updated / deleted.
    * 
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Ajay Panachickal            6/8/2016               Created
    * Ajay Panachickal            9/30/2016              Updated to base Account query on the event.WhoId to accommodate 'Related To' opportunity events.
    *****************************************************************************************************************************************/
    public void trg_Update_Talent_LastModified (String Context, Map<Id, Task>oldMap, List<Task>TriggerNew) {
        Set<Id> setContactids = new Set<Id>();
        Set<string> setContactidsstrn = new Set<string>();
        List<Account> accountsToUpdate = new List<Account>();
        
        if (!Context.equals(afterDelete)) {
            for (Task task : TriggerNew) {
                // Filter out tasks not associated to an account.
                if (task.WhoId != Null && task.Type != 'Merge Completed') {
                    setContactids.add(task.WhoId);
                    setContactidsstrn.add(task.WhoId);
                }
            }
        } else {
            List<Task> deletedTasks = oldMap.values();
            for (Task task : deletedTasks) {
                // Filter out tasks not associated to an account.
                if (task.WhoId != Null) {
                    setContactids.add(task.WhoId);
                    setContactidsstrn.add(task.WhoId);
                }
            }
        }
        //Added by hareesh to populate the custom last activity dates for contacts
        if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
        string contQuery = 'Select id,Title,Last_Event_OpCo__c,Last_Event__c,Total_Meals__c,Total_Meetings__c,AccountId,Account.Master_Global_Account_Id__c,Last_Service_Date__c from Contact where recordtypeid in: clientcontactRecTypeIDs  and id IN :setContactids';
        Map<Id, Contact> ClientcontMap = new Map<Id,Contact>((List<Contact>)Database.query(contQuery)) ;
        
       
         if(setContactidsstrn.size() > 0) {
         
                contmap=(ActivityHelper.updateLastActivity(setContactidsstrn,ClientcontMap));
                
            }
            }
        
        // Get 'Talent' accounts for the related events.
        String queryAccounts = 'SELECT Id, Talent_Profile_Last_Modified_Date__c, Talent_Profile_Last_Modified_By__c from Account where RecordTypeID = \''+accRecordTypeID+'\' and Id IN (SELECT AccountId from Contact where Id IN :setContactids AND RecordTypeID = \''+conRecordTypeID+'\')';
        Map<Id, Account> accountMap = new Map<Id, Account>((List<Account>)Database.query(queryAccounts));
        
        for (Account act : accountMap.values()) {
            if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration) {
                act.Talent_Profile_Last_Modified_Date__c = System.now();
                act.Talent_Profile_Last_Modified_By__c = UserInfo.getUserId();

            accountsToUpdate.add(act);
            } else if (UserInfo.getLastName()==system.label.SOA_Data_M_UserLname) {
                if((act.Talent_Profile_Last_Modified_Date__c != NULL) && act.Talent_Profile_Last_Modified_Date__c.Date() <> System.now().date()){
                    act.Talent_Profile_Last_Modified_Date__c = System.now(); 
                    accountsToUpdate.add(act);   
                }
            } 
        }

        if (accountsToUpdate != Null && accountsToUpdate.size() > 0) {
            
            
            try{
                update accountsToUpdate;
                System.Debug('Insert successful on Contact');
            }
            catch (Exception e)
            {
                for (Task opp: TriggerNew) {
                   opp.addError('There was a problem creating the task');
                }
            }
        }
    }

    /******************************************************************************************************************************
    * Original Header : trigger Task_isAssociatedToTargetAccount on Task (before insert,before update)
    * Name        - Task_isAssociatedToTargetAccount
    * Description - Trigger that checks if the task is created against a targeted accounted or not
    * Modification Log :
    * ---------------------------------------------------------------------------
    * Developer                   Date                   Description
    * ---------------------------------------------------------------------------
    * Pavan                       03/28/2012             Created
    * Ganesh                      04/19/2012             Target Account checkbox on Activities to a picklist with
                                                         label "Target Account Activity" and values of "Target" and "Non-Target"
    ********************************************************************************************************************************/

    public void Task_isAssociatedToTargetAccount(String context,Map<Id, Task> oldMap, List<Task> newRecs) {

        Map<string,List<Task>> ownerMap = new Map<string,List<Task>>();
        string TGS_TASK_SUBJECT = 'Salesforce Reminder: Please complete the Opportunity OA Form';
        string TO_DO = 'To Do';
        Map<string,List<Task>> accountIds = new Map<string,List<Task>>();
        List<Task> filteredTasks = new List<Task>();
        Map<id,List<SObject>> associatedTasks =  new Map<id,List<SObject>>();
        Map<string,List<Task>> associatedProfiles = new Map<string,List<Task>>();
        // associate the task to primary account
        for(Task task : newRecs)
        {
               if(task.whoId != Null && task.whatId == Null)
               {
                  associatedTasks = ActivityHelper.processAssociatedContactMap(associatedTasks,task);
               }
               boolean processTasks = true;
               if(context.equals(beforeUpdate) && oldMap.get(task.Id).ownerId == task.OwnerId)
                   processTasks = false;
                //populate owners office and profile Name on create
               if(processTasks)
               {
                       if(associatedProfiles.get(Task.OwnerId)!= Null) {
                           associatedProfiles.get(Task.OwnerId).add(Task);
                       }
                       else
                       {
                           List<Task> tasks = new List<Task>();
                           tasks.add(task);
                           associatedProfiles.put(task.OwnerId,tasks);
                       }
               }
        }
        //associate the contacts primary account to tasks
        if(associatedTasks.size() > 0) {
            ActivityHelper.associateToPrimaryAccount(associatedTasks);
        }
        //assign owners office name nd profile name accordingly
        if(associatedProfiles.size() > 0) {
           ActivityHelper.assignOwnerOffice(associatedProfiles);
        }

        //clear the associatedTask map
        associatedTasks.clear();

        for(Task task : newRecs)
        {
               boolean isUpdate = true;
               boolean assignType = true;
               boolean assignTitle = true;

               // check for TGS tasks on create
               if(context.equals(beforeInsert))
               {
                   // update the default type for task
                   if(task.subject != Null && task.subject.contains(TGS_TASK_SUBJECT)) {
                       task.Type = TO_DO;
                   }
               }
               else
               {
                      if(!(task.OwnerId != oldMap.get(task.Id).OwnerId || task.whatId != oldMap.get(task.Id).whatId)) {
                          isUpdate = false;
                      }
                      if(task.Type == oldMap.get(task.Id).Type) {
                          assignType = false;
                      }
                      if(task.whoId == oldMap.get(task.Id).whoId) {
                          assignTitle = false;
                      }
               }

               // add the task to filtered list accordingly
               if(isUpdate) {
                   filteredTasks.add(task);
                }
               //populate the Activity Type
               if(assignType) {
                   task.Activity_Type__c = task.Type;
                }
               // process to assign contact title
               if(assignTitle) {
                   associatedTasks = ActivityHelper.processAssociatedContactMap(associatedTasks,task);
                }

        }
        //populate the title by querying related contact
        if(associatedTasks.size() > 0) {
            ActivityHelper.assignTitleToActivities(associatedTasks);
        }

        //loop over the tasks and filter out those that are related to accounts
        for(Task task : filteredTasks)
        {
               if(task.whatId != Null && string.valueOf(task.whatId).subString(0,3) == Label.AccountIdPrefix)
               {
                      // populate ownerMap
                      if(ownerMap.get(task.ownerId) != Null)
                      {
                          ownerMap.get(task.ownerId).add(task);
                      }
                      else
                      {
                            processMap(true,task.ownerId,task,ownerMap,accountIds);
                      }
                      // populate accounts Map
                      if(accountIds.get(task.whatId) != Null) {
                          accountIds.get(task.whatId).add(task);
                      }
                      else
                      {
                          processMap(false,task.whatId,task,ownerMap,accountIds);
                      }
               }
        }
        if(accountIds.size() > 0)
        {
            ActivityHelper.associateToTargetAccount(accountIds,ownerMap);
        }

     /******************************************************************************************
     ER-209
     ********************************************************************************************/
         if(context.equals(beforeUpdate))
         {
                for(Task task : newRecs)
                {
                       if((OldMap.get(task.Id).Description == null || OldMap.get(task.Id).Description == '')&& (task.Description != null && task.Description !='') && task.Status == 'Completed' && task.Completed_Flag__c == False )
                       {
                                task.Completed__c = System.Now();
                                task.Completed_Flag__c = True;
                       }
                       if(OldMap.get(task.Id).Status != 'Completed' && task.Status == 'Completed' && (task.Description !='' && task.Description != null )&& task.Completed_Flag__c== False)
                       {
                                task.Completed__c = System.Now();
                                task.Completed_Flag__c = True;
                       }
                       if(OldMap.get(task.Id).Completed_Flag__c == False && task.Completed_Flag__c == True && date.valueOf(task.ActivityDate).daysBetween(date.today())>0)
                       {
                                task.Days_to_Complete__c = date.valueOf(task.ActivityDate).daysBetween(date.today());
                                System.debug('&&&&&&task.ActivityDate'+task.ActivityDate);
                                System.debug('&&&&&&date.valueOf(task.ActivityDate)'+date.valueOf(task.ActivityDate));
                       }
                       if(OldMap.get(task.Id).Completed_Flag__c == False && task.Completed_Flag__c == True && date.valueOf(task.ActivityDate).daysBetween(date.today())<0)
                       {
                                task.Days_to_Complete__c = 0;
                       }

                }
         }
         if(context.equals(beforeInsert))
         {
                for(Task task : newRecs)
                {
                       // to uncheck complete flag, complete date and comments when repeated task is created.
                       if(task.RecurrenceRegeneratedType<> NULL && task.Completed_Flag__c)
                       {
                       task.Description =NULL;
                       task.Completed__c=NULL;
                       task.Completed_Flag__c=FALSE;
                       }
                       if(task.Status == 'Completed' && (task.Description !='' && task.Description != null )&& task.Completed_Flag__c== False)
                       {
                                task.Completed__c = System.Now();
                                task.Completed_Flag__c = True;
                                if(date.valueOf(task.ActivityDate).daysBetween(date.today())>0)
                                {
                                      task.Days_to_Complete__c = date.valueOf(task.ActivityDate).daysBetween(date.today());
                                }
                                else
                                {
                                      task.Days_to_Complete__c = 0;
                                }
                       }
                }
         }



    }
     /**********************************************************
     Method to populate the Map
     **********************************************************/
     void processMap(boolean isOwner,string key, Task task,Map<string,List<Task>> ownerMap,Map<string,List<Task>> accountIds)
     {
         // create a list of Tasks
         List<Task> Tasks = new List<Task>();
         Tasks.add(task);
         if(isOwner) {
             ownerMap.put(key,Tasks);
         }
         else {
             accountIds.put(key,Tasks);
        }

     }

//Method to update the contacts only once during the trigger execution-Part of handling Long running Transaction
    //---------------------------------------------------
    public void UpdateContact(map<id,contact> contmap)  {
        List<Log__c> errorLogs = new List<Log__c>(); 
        
        if(contmap.size() > 0)         
        {          
            for (database.saveResult result : database.update(contmap.values(),false))         
            {         
                // insert the log record accordingly         
                if(!result.isSuccess() )         
                {         
                    errorLogs.add(Core_Log.logException(result.getErrors() [0]));         
                }         
            }         
        }                   
        if (errorLogs.size() >0) {        
            database.insert(errorLogs,false);
        }
    }
    


}