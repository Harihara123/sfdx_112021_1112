@isTest
public class TaskTriggerHandler_Test {
    @testSetup
    static void createTestData(){
        Account testAccount = CreateTalentTestData.createTalent();
        testAccount.skills__c = '{"skills":["ABC","Test","Skill1","Skiil3","ABC Skill4","Java"]}';
        update testAccount;
        CreateTalentTestData.createWorkHistory(testAccount);
    }
    static testMethod void validateDeleteTask() {
        Account acc=[Select Id From Account LIMIT 1];
        Task tsk =  new Task(whatId = acc.id, 
                            Subject = 'Test G2',
                            Type = 'G2',
                            Status = 'Completed' ,
                            ActivityDate =  system.today() );
        insert tsk;
        Delete tsk;
    }
}