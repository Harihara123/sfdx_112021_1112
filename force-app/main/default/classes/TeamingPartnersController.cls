public with sharing class TeamingPartnersController {
    
    String OppReqId;
    List<Teaming_Partner__c> lstTeamPartners = new List<Teaming_Partner__c>();
    Opportunity opp;
    string redirectURL = '/apex/Req_Creation?&RecordType='+Label.Reqs_Recordtype;
    Map<string,Teaming_Partner__c> TeamingPartnersMap = new Map<string,Teaming_Partner__c>();
    
    //Constructor
    public TeamingPartnersController() 
    {
           OppReqId = ApexPages.currentPage().getParameters().get('Id');
                
           opp = [select Name,AccountId,Account.Name,(select Name,Account__c,Account__r.Name,Agreement_Signed_Date__c,Subcontract_Number__c,Opportunity__c from Teaming_Partners__r) from Opportunity where id = :OppReqId];
           //append the return Url
           redirectURL = redirectURL+'&retURL=%2F'+opp.Id;
           
           if(opp != Null)
           {
                for(Teaming_Partner__c tPartners : opp.Teaming_Partners__r)
                { 
                   lstTeamPartners.add(tPartners);
                   TeamingPartnersMap.put(tPartners.Id,tPartners);
                }
           }
    }        
    
    public List<Teaming_Partner__c> GetlstTeamPartners() {
        return lstTeamPartners;
    }
    
    // Redirect the user to create req recordtype selection page.
    public PageReference newReq() 
    { 
        pageReference OppDetail;
        string accid = Apexpages.currentpage().getparameters().get('TeammingID');            
        if(lstTeamPartners.size() > 0)
        {
            if(accid != Null) 
                OppDetail = new pageReference(redirectURL+'&govtAccount='+TeamingPartnersMap.get(accid).Account__c);                    
        }
        else 
            OppDetail = new pageReference(redirectURL);                    
       
        if(oppDetail != Null)
            OppDetail.setRedirect(true);
        return OppDetail;                               
    }
    
    public PageReference Cancel() {
          pageReference OppDetail = new pageReference('/'+Opp.Id);   
          OppDetail.setRedirect(true);
          return OppDetail;
    }           
}