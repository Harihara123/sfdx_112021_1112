/*************************************************************************************************
Apex Class Name :  TestMethod_TeamingPartnersController
Version         : 1.0 
Created Date    : 
Function        : TestMethod_TeamingPartnersController
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* Dthuo                                                             Original Version
***************************************************************************************************/ 
@isTest
Private Class TestAccountPhoneFormat {
  public static testMethod void testInsertNewAccount()
    {  
      long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT' + random,
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );         
         test.startTest();
         Insert acct;
         test.stopTest();

//This will test if the Account was inserted
         acct=[SELECT Name,Phone FROM Account WHERE id = :acct.Id];
         System.debug('Account_after' +acct.name +acct.Phone);              
 
    }
}