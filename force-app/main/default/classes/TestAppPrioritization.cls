@isTest
public class TestAppPrioritization {
	
	@testSetup
	static void testSetup() {
		DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        	 insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='True');
        insert DRZSettings1;
		User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
           Global_LOV__c glov = new Global_LOV__c(
               LOV_Name__c = 'ATSResumeFiletypesAllowed',
               Source_System_Id__c = 'ATSResumeFiletypesAllowed.00023',
               Text_Value__c = 'docx'
           );
           insert glov;
			Account talAcc = CreateTalentTestData.createTalent();
			Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
			

			Job_Posting__c jobPosting = null;
			List<Opportunity> lstOpportunity = new List<Opportunity>(); 
			TestData TdAcc = new TestData(1);
			testdata tdcont = new Testdata();
			string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
			String accName = '';
			String sourceSystmId= '121112';
			Opportunity NewOpportunity = null;
			for(Account Acc : TdAcc.createAccounts()) {  
				System.debug('Acc:'+Acc);
				accName = 'New Opportunity'+Acc.name;
				NewOpportunity  = new Opportunity( Name =  accName, LDS_Account_Name__c = Acc.id, Accountid = Acc.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                    Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                    Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                    Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly', Req_Division__c='easi');  

                    lstOpportunity.add(NewOpportunity);
				break;
			}
			insert lstOpportunity;

			 OpportunityTeamMember otm = new OpportunityTeamMember (OpportunityId = lstOpportunity[0].id,UserId = userinfo.getuserid(),TeamMemberRole = 'Recruiter');
				insert otm;
			// Ceate Job_Posting__c
			jobPosting = new Job_Posting__c(Currency_Code__c='111', 
			Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=NewOpportunity.Id, Posting_Detail_XML__c='Sample Description',
			Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.'+sourceSystmId,State__c='MD');
			
			insert jobPosting;

			Job_Posting__c jobPostingOfccp = new Job_Posting__c(Currency_Code__c='111', 
			Delete_Retry_Flag__c=false, Job_Title__c = 'OFCCPTitle',Opportunity__c=NewOpportunity.Id, Posting_Detail_XML__c='Sample Description',
			OFCCP_Flag__c = true,Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.12525354',State__c='MD');

			insert jobPostingOfccp;
			//Job_Posting__c jp = ApplicationManagementTestData.createJobPostingObject('');
			List<Account> accList = TdAcc.createAccounts();
			Order order = new Order();
			order.AccountId = accList[0].Id;
			order.OpportunityId = jobPosting.Opportunity__r.Id;
			order.ShipToContactId = talCont.Id;
			order.RecordTypeId = Schema.getGlobalDescribe().get('Order').getDescribe().getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
			order.Unique_Id__c = String.valueOf(talCont.Id)+String.valueOf(jobPosting.Opportunity__r.Id);
			order.Status = 'Applicant';
        
			order.EffectiveDate =  Date.today();
			order.Has_Application__c = true;
		

			insert order;

			Event event = new Event();
		 
			event.WhatId = order.Id;
			event.WhoId = talCont.Id;
			event.Application_Id__c = 'CS_Application_233';
			event.Inviter_Full_Name__c = 'Test Application';
			//event.JB_Source_ID__c = submittal.jbSourceId;
			event.Vendor_Application_ID__c = 'CS_Application_233';
			event.Vendor_Segment_ID__c = 'ved1111';
			event.Job_Posting__c = jobPosting.Id;
			event.DurationInMinutes = 1;
			event.StartDateTime = DateTime.now();
			event.Subject = order.Status;
			event.Type = order.Status;
			event.Activity_Type__c  = order.Status;
			event.Source__c = 'Test Name';
			//event.Feed_Source_ID__c = submittal.vendorId;
			event.ECID__c = '';
			event.ICID__c = '';			
			//event.OwnerId = '005240000038GjfAAE';            
			event.Job_Posting_Owner_Email__c = jobPosting.Owner.email;//STORY S-113884 Added Job posting Owner's Email to event Email by Siva 1/23/2019
        
			insert event;

			Event event2 = new Event();
		 
			event2.WhatId = order.Id;
			event2.WhoId = talCont.Id;
			event2.Application_Id__c = 'CS_Application_233';
			event2.Inviter_Full_Name__c = 'Test Application';
			//event2.JB_Source_ID__c = submittal.jbSourceId;
			event2.Vendor_Application_ID__c = 'CS_Application_233';
			event2.Vendor_Segment_ID__c = 'ved1111';
			event2.Job_Posting__c = jobPosting.Id;
			event2.DurationInMinutes = 1;
			event2.StartDateTime = DateTime.now();
			event2.Subject = order.Status;
			event2.Type = order.Status;
			event2.Activity_Type__c  = order.Status;
			event2.Source__c = 'PhenomPeople';
			//event2.Feed_Source_ID__c = submittal.vendorId;
			event2.ECID__c = '';
			event2.ICID__c = '';			
			//event2.OwnerId = '005240000038GjfAAE';            
			event2.Job_Posting_Owner_Email__c = jobPosting.Owner.email;//STORY S-113884 Added Job posting Owner's Email to event2 Email by Siva 1/23/2019
        
			insert event2;

			TalentFitment__c tft = new TalentFitment__c();
			tft.ApplicantId__c = event.Id;
			tft.Status__c = 'New';
			tft.Recruiter__c = event.Job_Posting__r.OwnerId;
			tft.Applicant_Name__c = event.Who.Name;
			tft.Applicant_Source__c = 'ZIP';
			insert tft;

			TalentFitment__c tft2 = new TalentFitment__c();
			tft2.ApplicantId__c = event2.Id;
			tft2.Status__c = 'New';
			tft2.Recruiter__c = event2.Job_Posting__r.OwnerId;
			tft2.Applicant_Name__c = event2.Who.Name;
			tft2.Applicant_Source__c = 'PhenomPeople';
			insert tft2;

			App_Prioritization_Settings__c settings = new App_Prioritization_Settings__c();
			settings.Name='AppPriority';
			settings.RecordsCreatedAfterTime__c = -6;
			settings.RecordsCreatedBeforeTime__c = 5;
			settings.RecordsRetrievedOnJobToTalentAPI__c = 5;
			settings.RecordsRetrievedOnTalentToJobAPI__c = 5;
			settings.RecordsToBeProcessesPerQueue__c = 1;
			insert settings;		

			
			List<Id> eventIds = new List<Id>();
			eventIds.add(event.Id);
			Map<Id,TalentFitment__c> talentFitmentMap = new Map<Id,TalentFitment__c>();
			talentFitmentMap.put(tft.Id,tft);

			

			ApigeeOAuthSettings__c apigeeOauth = new ApigeeOAuthSettings__c();
  
			apigeeOauth.Name = 'Match to Req Service GET';
			apigeeOauth.Client_Id__c = '1QYcoEvo4ueqvG42PyHP7TE9UMTxjeuKJMTQY';
			apigeeOauth.Client_Secret__c = '2dGZFmR2mRIr4Sh321';
			apigeeOauth.Service_Http_Method__c = 'GET'; 
			apigeeOauth.Service_URL__c = 'https://sampleSearchRequest.com/search?';     
			apigeeOauth.Token_URL__c = 'https://sampletokenRequesturl.com/token?';
			apigeeOauth.OAuth_Token__c = 'Q3bO24L1B2GUFQiKHLOqAB12JbcSywK'; 

			insert apigeeOauth;
			//System.enqueueJob(new MatchAPICalloutsQueueble(eventIds,talentFitmentMap));
			
			
			
         }  
	}

	@istest
	static void testMatchQueueJob() {
		Map<Id,TalentFitment__c> talentFitmentMap = new Map<Id,TalentFitment__c>();
		List<Id> eventIds = new List<Id>();
		List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 10];
		List<Event> evtList = [SELECT Id,Job_Posting__r.Opportunity__c FROM Event LIMIT 10];
		
		for(TalentFitment__c tft: [SELECT ApplicantId__c,Id,Status__c FROM TalentFitment__c LIMIT 10]) {
			talentFitmentMap.put(tft.Id,tft);
			eventIds.add(tft.ApplicantId__c);
		}
		System.debug('eventIds:'+eventIds);
		System.debug('talentFitmentMap:'+talentFitmentMap);
		MatchAPICalloutsQueueble queueble = new MatchAPICalloutsQueueble(eventIds,talentFitmentMap);
		queueble.fetchEventIdsFromJob(oppList[0].Id,evtList);
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
		
		Test.startTest();
		
		
		//MatchAPICalloutsQueueble callout = new MatchAPICalloutsQueueble(eventIds,talentFitmentMap);
		System.enqueueJob(new MatchAPICalloutsQueueble(eventIds,talentFitmentMap));
		//callout.talentToJobAPI(talentIds,eventFitmentMap,contactMap);

		Test.stopTest();
		List<AsyncApexJob> jobInfoList = [SELECT Id FROM AsyncApexJob LIMIT 1];
		System.assertEquals(1,jobInfoList.size());

	}

	@istest
	static void testMatchQueueJob2() {		
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
		Test.startTest();
		
		
		/*DateTime now = System.now();
		Datetime local = now.addSeconds(10);
		String str_dt = String.valueOf(local.format('yyyy-MMM-dd HH:mm:ss'));
		String str_schedule = String.valueOf(local.format('yyyy-MMM-dd HH:mm:ss SSS'));
		String[] str_now = str_dt.split(' ');
		String[] str_date = str_now[0].split('-');
		String[] str_time = str_now[1].split(':');
		String cron_exp = str_time[2] + ' ' + str_time[1] + ' ' + str_time[0] + ' ' + str_date[2] + ' ' + str_date[1] + ' ? ' + str_date[0];
		ScheduleRequestMatchAPI scheduleRequests = new ScheduleRequestMatchAPI();
		System.Schedule('Schedule '+ Userinfo.getUserId() + str_schedule, cron_exp, scheduleRequests);*/
		System.schedule('Schedule '+ Userinfo.getUserId() + '15 mins', '0 1 * * * ?', new ScheduleRequestMatchAPI());

		Test.stopTest();
		List<CronTrigger> jobList =  [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger LIMIT 1];
		System.assertEquals(1, jobList.size());
	}
}