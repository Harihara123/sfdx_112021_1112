global class TestBatch  implements Database.Batchable<sObject> {
    String Query;
    global TestBatch(){
        Query ='SELECT Id, OwnerId,  Name FROM Global_LOV__c limit 100000';
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Global_LOV__c> scope) {
        try {
            delete scope;
            
        
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
            System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
            System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
            System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
        String subject = 'Alert Notification Batch Status';
        
        mail.setSubject(subject + a.Status);
        mail.setPlainTextBody
            ('The batch Apex job processed ' + a.TotalJobItems +
             ' batches with '+ a.NumberOfErrors + ' failures.');
      //  Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }


}