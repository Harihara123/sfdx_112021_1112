@isTest
public class TestBatch_CopyLegacyReqToEnterpriseReq
{
    static testmethod void method1()
    {
        ProcessLegacyReqs__c plr = new ProcessLegacyReqs__c();
        plr.Name = 'BatchLegacyReqLastRunTime';
        plr.LastExecutedBatchTime__c = System.Now().addDays(-10);
        insert plr;
        
        Reqs__c req = new Reqs__c(RecordTypeId='012U0000000DWpIIAW',Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Batch_CopyLegacyReqToEnterpriseReq obj = new Batch_CopyLegacyReqToEnterpriseReq();
        Database.executeBatch(obj,200);
    }
    
}