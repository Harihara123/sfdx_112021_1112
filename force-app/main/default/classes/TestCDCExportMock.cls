@isTest
Global class TestCDCExportMock implements HttpCalloutMock {
global HTTPResponse respond(HTTPRequest req) {
        String responseString ='{"message":"Success"}';
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(200);
        return res;
}
}