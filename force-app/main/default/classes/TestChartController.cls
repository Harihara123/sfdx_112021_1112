@isTest(seeAlldata= false)
public class TestChartController {


    private static testMethod void testgetFinishPage() {
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
         
        
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();

 test.startTest();
        
        Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'Interest', closedate = system.today()+1, Impacted_Regions__c = 'Aerotek Central');
            insert newopportunity; 
   PageReference pageRef = Page.Req_Submittal_Chart;
  
   Test.setCurrentPage(pageRef);
   pageRef.getParameters().put('id',newopportunity.id);
   
 
    string recdtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
   ApexPages.StandardController sc = new ApexPages.standardController(newopportunity);
  Chartcontroller ct = new ChartController(sc);

       Opportunity newopportunity1 = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'Interest', closedate = system.today()+1, Impacted_Regions__c = 'Aerotek Central', Req_Created_from_Proactive__c = 'Created as Won',recordtypeid=recdtypeid);
            insert newopportunity1; 
             pageRef.getParameters().put('id',newopportunity.id);
             ApexPages.StandardController sc1 = new ApexPages.standardController(newopportunity1);
  Chartcontroller ct1 = new ChartController(sc1);
            
   //System.assertNotEquals(null,controller.getFinishPage());
   
   test.stopTest();
}

}