@isTest(seeAlldata= false)
public class TestCompensationController {


    private static testMethod void testgetFinishPage() {
        
         Routing_Controller_Settings__c settings = new Routing_Controller_Settings__c();
        List<Routing_Controller_Settings__c> recList = new List<Routing_Controller_Settings__c>();
        settings = new Routing_Controller_Settings__c();
        settings.Name = 'RegionsTobeIncluded';
        recList.add(settings);
        settings = new Routing_Controller_Settings__c();
        settings.Name = 'LocationTobeIncluded';
        recList.add(settings);
        
        settings = new Routing_Controller_Settings__c();
        settings.Name = 'DivisionTobeIncluded';
        recList.add(settings);
        settings = new Routing_Controller_Settings__c();
        settings.Name = 'Opportunityid';
        recList.add(settings);
        settings = new Routing_Controller_Settings__c();
        settings.Name = 'CompensationURL';
        recList.add(settings);        
        insert recList;
        
        
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();

 test.startTest();
        
        Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'Interest', closedate = system.today()+1);
            insert newopportunity; 
   PageReference pageRef = Page.Compensationform;
  
   Test.setCurrentPage(pageRef);
   pageRef.getParameters().put('id',newopportunity.id);
   ApexPages.StandardController sc = new ApexPages.standardController(newopportunity);
  CompensationController  controller = new CompensationController(sc);
       
   System.assertNotEquals(null,controller.getFinishPage());
   
   test.stopTest();
}

}