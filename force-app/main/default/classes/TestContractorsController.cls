@isTest(SeeAllData=true)
Global class TestContractorsController implements WebServiceMock {
           // Implement this interface method
    global void doInvoke(
                           Object stub,
                           Object request,
                           Map<String, Object> response,
                           String endpoint,
                           String soapAction,
                           String requestName,
                           String responseNS,
                           String responseName,
                           String responseType) 
       { 
          List<WS_ContractorService.GetContractorDataResponseType> newStarts = new List<WS_ContractorService.GetContractorDataResponseType>();
          WS_ContractorService.GetContractorDataResponseType start = new WS_ContractorService.GetContractorDataResponseType();
          start.AccountName = 'Mock response'; 
          newStarts.add(start);
          WS_ContractorService.GetContractorDataResponseRoot_element respElement = new WS_ContractorService.GetContractorDataResponseRoot_element();  
          respElement.GetContractorDataResponse = newStarts;
          response.put('response_x', respElement);          
      }
    static testmethod void contactTestRun()
    {
        
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Contractors_Contact_Data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       ContractorsController AccPage = new ContractorsController(controller);
       accPage.RequestStatus = 'Former';
       accPage.startsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.fetchFormerRecordsCont();
       accPage.processResponse();
       accPage.errorMessages();
       accPage.selectedFilterOption = 'Corporate';
       accPage.sortColIndex = 1 ;             
    }
    
    static testmethod void contactTestRun1()
    {
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Contractors_Contact_Data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       ContractorsController AccPage = new ContractorsController(controller);
       accPage.RequestStatus = 'Current';
       accPage.startsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.fetchFormerRecordsCont();
       accPage.processResponse();
       accPage.getDivisionList();
       accPage.fetchCurrentRecords();
       accPage.selectedFilterOption = 'All';
       accPage.fetchFilteredRecords();
       accPage.getFirstPage();
       accPage.getPagenumber();
       accPage.getTotalpages();
       accPage.firstBtnClick();
       accPage.previousBtnClick();  
       accPage.nextBtnClick();
       accPage.lastBtnClick();
       accPage.getPreviousButtonEnabled();
       accPage.getNextButtonEnabled();
       accPage.sortAction();
       accPage.sortColIndex = 2 ;
       accPage.errorMessages();
             
    }
    
    static testmethod void accountTestRun()
    {
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
       PageReference pageRef = Page.Contractors_Account_Data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       ContractorsController AccPage = new ContractorsController(controller);
       accPage.RequestStatus = 'Current';
       accPage.startsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.fetchFormerRecordsCont();
       accPage.processResponse();
       accPage.getDivisionList();
       accPage.fetchFilteredRecords();
       accPage.getFirstPage();
       accPage.getPagenumber();
       accPage.getTotalpages();
       accPage.firstBtnClick();
       accPage.previousBtnClick();  
       accPage.nextBtnClick();
       accPage.lastBtnClick();
       accPage.getPreviousButtonEnabled();
       accPage.getNextButtonEnabled();
       accPage.sortAction();
       accPage.errorMessages();
       accPage.sortColIndex = 6;
             
    }
    
    static testmethod void accountTestRun1()
    {
        // Test data
        
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
       PageReference pageRef = Page.Contractors_Account_Data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails);
       ApexPages.currentPage().getParameters().put('Full','1');  
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       ContractorsController AccPage = new ContractorsController(controller);
       accPage.RequestStatus = 'Former';
       accPage.startsInformation();
       accPage.startRequest();
       accPage.processResponse();       
       accPage.fetchFormerRecordsCont();
       accPage.processResponse();
       accPage.fetchFormerRecords();
       accPage.errorMessages();
       accPage.selectedFilterOption = 'Corporate';
       accPage.exceptionDescription ='test';
       accPage.sortColIndex = 4;
       accPage.sortColIndex = 6;
       accPage.sortColIndex = 7;
       accPage.sortColIndex = 8;
       accPage.sortColIndex = 9;
       accPage.sortColIndex = 10;
       accPage.sortColIndex = 11;
       accPage.sortColIndex = 12;
       accPage.sortColIndex = 13;
             
    }
    
    static testMethod void testWebService(){
      /*  WS_ContractorServiceType.ContractorDataEndpoint1 WSContractors1 = new WS_ContractorServiceType.ContractorDataEndpoint1();
        WSContractors1.timeout_x = 11999;
        WSContractors1.getContractors('1234',new list<string>{'1234'},'02/03/2015','03/02/2015','test'); */
        
         Integration_Endpoints__c custom_setting_obj1 = new Integration_Endpoints__c();
         Integration_Certificate__c custom_setting_obj2=new Integration_Certificate__c();
                  
         custom_setting_obj1.name='TIBCO.TestContractor';
         custom_setting_obj1.Endpoint__c= 'https://sf2allegisdcmSit.allegistest.com:20046/ESFAccount';
         insert custom_setting_obj1;              
                  Test.setMock(WebServiceMock.class, new TestContractorsController());
                  test.startTest();
                       List<String> ContactID= new list<string>() ;
                       List<WS_ContractorService.GetContractorDataResponseType> newStarts = new List<WS_ContractorService.GetContractorDataResponseType>();
                       WS_ContractorServiceType.ContractorDataEndpoint1 WSContractors = new WS_ContractorServiceType.ContractorDataEndpoint1();
                       newStarts = WSContractors.getContractors('12-12', ContactID, '1/1/2014' , '1/1/2015', 'Current');                                       
                       
                       AsyncWS_ContractorService.GetContractorDataResponseRoot_elementFuture ws_request = new AsyncWS_ContractorService.GetContractorDataResponseRoot_elementFuture();
                       AsyncWS_ContractorServiceType.AsyncContractorDataEndpoint1 asyncWScontractors = new AsyncWS_ContractorServiceType.AsyncContractorDataEndpoint1();
                       list<WS_ContractorService.GetContractorDataResponseType> ws_response = new list<WS_ContractorService.GetContractorDataResponseType>();
                       // System.assertEquals(null, ws_request.getValue());
                  test.stopTest();
        
    }  
  /*  public static testmethod void testContWebService() { 
Test.setMock(WebServiceMock.class, new TestContractorsController()); 
Test.startTest(); 

ContinuationSOAPController demoWSDLClass = new ContinuationSOAPController(); 
// Invoke the continuation by calling the action method 
Continuation conti = demoWSDLClass.startRequest(); 

// Verify that the continuation has the proper requests 
Map<String, HttpRequest> requests = conti.getRequests(); 
system.assert(requests.size() == 1); 

// Perform mock callout 
// (i.e. skip the callout and call the callback method) 
HttpResponse response = new HttpResponse(); 
response.setBody('Mock response body'); 
// Set the fake response for the continuation 
String requestLabel = requests.keyset().iterator().next(); 
Test.setContinuationResponse(requestLabel, response); 
// Invoke callback method 
Object result = Test.invokeContinuationMethod(demoWSDLClass, conti); 
// result is the return value of the callback 
System.assertEquals(null, result); 
// Verify that the controller's result variable 
// is set to the mock response. 
System.assertEquals('Mock response body', demoWSDLClass.result); 

Test.stopTest(); 
} */
    
}