/******************************************************************************************************************

    Apex Class Name:  TestData 
    Version     : 1.0.1 
    Created Date: 02 MAR 2012
    Function    : Class providing test data for test classes
    Modification Log :
    -----------------------------------------------------------------------------
    Developer                   Date                    Description
    ----------------------------------------------------------------------------                 
    Dhruv Gupta          02 MAR 2012              Original Version
    Ganesh                15/06/2012              Included Random value for create users                                              
********************************************************************************************************************/
@isTest
public with sharing class TestData {
   
  //  public static final Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Recruiter').getRecordTypeId();
  
  
    public Integer listSize {get;set;}
    
    public List<User> users {get; set;}
    public List<Account> accounts {get;set;}
    public List<Opportunity> opportunities {get;set;}
    public List<Contact> Contacts{get;set;}
    public Reqs__c Reqs{get;set;}
    public List<Recruiter_Team__c> recruiterTeams {get;set;}
    public static Map<String,User>  mapOtherTestUser = new  Map<String,User>();
    public contact contactwa{get;set;}
    public Product2 testProduct {get; set;}
    
    public TestData() {
        this.listSize = 20;
    }
    
    public TestData(Integer listSize) {
        this.listSize = listSize;
    }
    
    /*
     * Method name  : newAccount
     * Description  : Calls overloaded newAccount method
     */ 
        
    public static Account newAccount(Integer i) {
        // Hareesh-updated test account creation to consider only CRM contacts
        

        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        //update test account creations only for Client accounts
        return new Account(
            Name = 'TESTACCT' + random,
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());      
    }
    
    //Neel added this method to create account for all record type
    public static Account newAccount(Integer i, String recType) {
       
        return new Account(
            Name = 'TESTACCT' + i,
            Phone= '2345'+i,ShippingCity = 'Testshipcity'+i,ShippingCountry = 'Testshipcountry'+i,
            ShippingPostalCode = 'TestshipCode'+i,ShippingState = 'Testshipstate'+i,
            ShippingStreet = 'Testshipstreet'+i,BillingCity ='TestBillstreet'+i,
            BillingCountry ='TestBillcountry'+i,BillingPostalCode ='TestBillCode'+i,
            BillingState ='TestBillState'+i,BillingStreet ='TestBillStreet'+i,AccountSource=i+'Data.com',
            Talent_Current_Employer_Text__c='Test Employer'+i, Talent_Profile_Last_Modified_Date__c=System.today(),
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get(recType).getRecordTypeId());      
    }
    
    public static Order newOrder1(Id accountId, Id contactId,Id oppId, Id userId, String recType) {
        	return new Order(
                AccountId = accountId,
                Job_Title__c = 'Job Title',
                EffectiveDate = system.today(),
                
                Status = 'Interviewing',
                Point_of_Contact_AM__c = userId,
                Skills__c = '{"skills":["Salesforce.com";"Marketing Cloud";"TEST1234"]}',
                Hiring_Manager__c = contactId,
                Type = 'VMS',
                Talent_Qualifications__c = 'Some qualifications',
                Description = 'Description',
                ShipToContactId=contactId,
                OpportunityId=oppId,
                Submittal_Applicant_Source__c='REFERRAL',
                Pay_Rate__c = 1000,
                Rate_Frequency__c = 'Daily',
                Salary__c = 1000000,
                Proactive_Submittal_Currency__c ='USD - U.S Dollar',
                recordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get(recType).getRecordTypeId()
            );
    }
    public static Order newOrder(Id accountId, Id contactId, Id userId, String recType) {
        	return new Order(
                AccountId = accountId,
                Job_Title__c = 'Job Title',
                EffectiveDate = system.today(),
                Status = 'Interviewing',
                Point_of_Contact_AM__c = userId,
                Skills__c = '{"skills":["Salesforce.com";"Marketing Cloud";"TEST1234"]}',
                Hiring_Manager__c = contactId,
                Type = 'VMS',
                Talent_Qualifications__c = 'Some qualifications',
                Description = 'Description',
                ShipToContactId=contactId,
               Submittal_Applicant_Source__c='REFERRAL',
                Pay_Rate__c = 1000,
                Rate_Frequency__c = 'Daily',
                Salary__c = 1000000,
                Proactive_Submittal_Currency__c ='USD - U.S Dollar',
                recordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get(recType).getRecordTypeId()
            );
    }
    public static Order newOrder2(Id accountId, Id contactId, Id userId, String recType) {
        	return new Order(
                AccountId = accountId,
                Job_Title__c = 'Job Title',
                EffectiveDate = system.today(),
                Status = 'Submitted',
                Point_of_Contact_AM__c = userId,
                Skills__c = '{"skills":["Salesforce.com";"Marketing Cloud";"TEST1234"]}',
                Hiring_Manager__c = contactId,
                Type = 'VMS',
                Talent_Qualifications__c = 'Some qualifications',
                Description = 'Description',
                ShipToContactId=contactId,
               Submittal_Applicant_Source__c='REFERRAL',
                Pay_Rate__c = 1000,
                Rate_Frequency__c = 'Daily',
                Salary__c = 1000000,
                Proactive_Submittal_Currency__c ='USD - U.S Dollar',
                recordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get(recType).getRecordTypeId()
            );
    }
    private void checkAccounts() {
        if (this.accounts == null || this.accounts.isEmpty()) {
            createAccounts();
        }
    }
    
    private void checkContacts() {
        if (this.Contacts== null || this.Contacts.isEmpty()) {
            createContacts('Recruiter');
        }
    }
        
    public static Contact newContact(Id accountId, Integer i, String recType) {
        system.debug('===>>inside newContact');
        return new Contact(
            LastName = 'TESTCONTACT' + i,
            AccountId = accountId,
            FirstName = 'testcon' + i,
           
            Email = 'testemail@test.com',
            MailingStreet = '123' + i + ' Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',
            
            recordTypeId =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recType).getRecordTypeId()
        );
    }         
    
     public static Opportunity newOpportunity(Id accId, Integer i) {
    //Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
    //List<Schema.PicklistEntry> P = F.getPicklistValues();
        return new Opportunity(
            Name = 'TESTOPP' + i,
            CloseDate = system.today(),
            StageName = 'test',
            accountId= accId,
            Req_Job_Description__c = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890124',
            Req_Job_Title__c = 'Sr Java Developer   ',
            Location__c = 'Hanover, MD',
            CreatedDate=system.today()
        );      
    }
    
    public List<Contact> createContacts(String recType) {
            system.debug('===>>inside createContacts');
        checkAccounts();
        
        this.Contacts= new List<Contact>{};
        
        for(Integer i = 0; i < this.listSize; i++) {          
            this.Contacts.add(TestData.newContact(this.accounts[i].Id, i,recType));            
        }
        insert this.Contacts; 
        return contacts;
      }
      
   Public contact createContactWithoutAccount(id accountid)
   {
    this.contactwa = new Contact();
    contactwa.firstname = 'sampledata';
    contactwa.lastname = 'testcontact';
    contactwa.accountid = accountid;
    contactwa.email = 'test@test.com';
    insert this.contactwa;
    return this.contactwa;
   }
    public List<Opportunity> createOpportunities() {
        checkAccounts();
        this.opportunities = new List<Opportunity>{};
        for(Integer i = 0; i<this.listSize; i++) {
            
            this.opportunities.add(TestData.newOpportunity(this.accounts[i].Id,i));
        }
        insert this.opportunities;
        return opportunities;
        }            
    
    /*
     * Method name  : createAccounts
     * Description  : Populates class accounts attribute by list of Account objects and inserts them to DB.
     */
    public List<Account> createAccounts() {
          this.accounts = new List<Account>{};
          for(Integer i = 0; i < this.listSize; i++) {
              this.accounts.add(TestData.newAccount(i));              
          }
          insert this.accounts;
          return accounts;
    } 
    
    /* Method Name: CreateProducts(Skills)
    *Description: Populates the product test data for Req Test Methods
    */
    
    public Product2 createTestProduct(){        
        Testproduct = new Product2();
        testProduct.Name='StandardProduct';
        testProduct.Category__c='TestCategory';
        testProduct.Category_Id__c ='123';
        testProduct.Division_Name__c='testDivision';
        testProduct.Job_code__c ='TestJob';
        testProduct.Jobcode_Id__c='123';
        testProduct.OpCo__c ='AeroTek';
        testProduct.OpCo_Id__c='123';
        testProduct.Segment__c ='testSegemnt';
        testProduct.Segment_Id__c ='123';
        testProduct.Skill__c ='testSkill';
        testProduct.Skill_Id__c ='123';
        testProduct.IsActive = true;        
        return testProduct;
        
    }
    
    public static Unified_Merge__c createMergeObject(Id con1ID, Id con2ID){
        Unified_Merge__c rdyObject = new Unified_Merge__c();
        rdyObject.Input_ID_1__c = con1ID;
        rdyObject.Input_ID_2__c = con2ID;
        rdyObject.Process_State__c = 'Ready';
        
        return rdyObject;
    }
    
}