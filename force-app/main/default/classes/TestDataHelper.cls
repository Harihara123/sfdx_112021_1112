/******************************************************************************************************************************
* Name        - TestDataHelper
* Description - Class to maintain the utility test methods 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pavan                       03/21/2012             Created
********************************************************************************************************************************/
@isTest(seeAlldata=false)
public class TestDataHelper
{
    
     // method to return a reference of contact
     public static contact createContact()
     {
           return new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
     } 
     //  method to return refernce of account
     public static Account createAccount()
     {
         return new Account(Name = 'Test Account',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
     }
     // method to return a refernce of target account
     public static Target_Account__c createTargetAccount()
     {
           //create account
           Account acc = createAccount();
           insert acc;
           return new target_Account__c(Account__c = acc.Id, user__c=userinfo.getuserid());
     }
     //Mehod to create list of accounts
     public static List<Account> getAccount()
     {
         List<Account> acc = new List<Account>();
         for(integer i = 0;i < 500;i++)
         {
            Account account = new Account(Name = 'Test Account - '+i,recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            acc.add(account);
         }
         return acc;
     }
     // method to create list of strategic initiatives
     public static List<Strategic_Initiative__c> getInitiativeRecords()
     {
       List<Strategic_Initiative__c> initiatives = new List<Strategic_Initiative__c>();
       for(integer i=0;i < 500;i++)
       {
         Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative-'+i,Start_Date__c = system.Today() - 30);
         initiatives.add(si);
       }
       return initiatives;
     }
     // method to insert SI Report record
  /*   public static SI_Report_SObject_QueryInformation__c getSIReportQuery()
     {
       SI_Report_SObject_QueryInformation__c obj = new SI_Report_SObject_QueryInformation__c();
       obj.Object__c = 'Account';
       obj.Query__c = 'Select Name,shippingCity,shippingCountry,BillingCity,BillingCountry,(Select OwnerId from Tasks),(Select OwnerId from Events),(Select Name from Reqs__r) from Account';
       return obj;
     } */
     // Method to create Events
     public static List<Event> getEvents()
     {
        List<Event> events = new List<Event>();
        for(integer i=0;i<200;i++)
        {
          Event event = new Event(subject='Meeting - '+i,DurationInMinutes = i,ActivityDateTime = system.now()+i);
          events.add(event);
        }
        return events;
     }
     //Method to create Tasks
     public static List<Task> getTasks()
     {
        List<Task> tasks = new List<Task>();
        for(integer i=0;i<200;i++)
        {
          Task task = new Task(subject='Meeting - '+i);
          tasks.add(task);
        }
        return tasks;
     }
     
     
     //Create a task assosiated to the account
     public static List<Task> getTasks2()
     {
        List<Task> tasks_acc = new List<Task>();
        Account acc = createAccount();
        for(integer i=0;i<200;i++)
        {
          Task task = new Task(subject='Meeting - '+i,whatid=acc.id);
          tasks_acc.add(task);
        }
        return tasks_acc;
     }
     //Method to create a user
     public static user createUser(string profile)
     {
         user newUser;
         try{
             Profile userProfile = [select Name from Profile where Name = :profile];
             
             long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
             Integer random = Math.Round(Math.Random() * timeDiff );
             newUser = new User(alias = 'test', 
                             email='testuser@allegisgroup.com',
                             emailencodingkey='UTF-8', 
                             lastname= profile + ' User', 
                             languagelocalekey='en_US',
                             localesidkey='en_US', 
                             profileid = userProfile.Id,
                             timezonesidkey='America/Los_Angeles', 
                             OPCO__c = 'test',
                               Region__c= 'Aerotek MidAtlantic',
                             Region_Code__c = 'test',
                             Office_Code__c = '10001',
                             Peoplesoft_Id__c = string.valueOf(random * 143),
                             User_Application__c = 'CRM',
							 CompanyName = 'Aerotek',
                             username='testusr'+random+'@allegisgroup.com.dev');             
           } Catch(Exception e)
           {  
               system.assertEquals('List has no rows for assignment to SObject',e.getMessage());
           }
           return newUser;
     }

	 //Method to create a user
     public static List<user> createUsers(Integer num, string profile)
     {
		List<User> usrList = new List<User>();
		for(Integer i=0; i<num; i++) {
			user newUser;
			 try{
				 Profile userProfile = [select Name from Profile where Name = :profile];
             
				 long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
				 Integer random = Math.Round(Math.Random() * timeDiff );
				 newUser = new User(alias = 'test', 
								 email='testuser'+i+'@allegisgroup.com',
								 emailencodingkey='UTF-8', 
								 lastname= 'User'+i, 
								 languagelocalekey='en_US',
								 localesidkey='en_US', 
								 profileid = userProfile.Id,
								 timezonesidkey='America/Los_Angeles', 
								 OPCO__c = 'TEK',
								 Region__c= 'Aerotek MidAtlantic',
								 Region_Code__c = 'test',
								 Office_Code__c = '10001',
								 Peoplesoft_Id__c = string.valueOf(random * 143),
								 User_Application__c = 'CRM',
								 username='testusr'+random+'@allegisgroup.com.dev');             
			   } Catch(Exception e)
			   {  
				   system.assertEquals('List has no rows for assignment to SObject',e.getMessage());
			   }
			   usrList.add(newUser);
		}
        insert usrList; 

		return usrList;
     }

     //Method to create OA Calculator custom setting data
     public static void createOACaculatorData()
     {
        List<OA_Calculator__c> oaCalcualtor = new List<OA_Calculator__c>();
        // insert records in to custom setting
        OA_Calculator__c calc1 = new OA_Calculator__c();
        calc1.Name = 'Test Field 1';
        calc1.Consider_for_BV__c = false;
        calc1.Consider_for_Standard_Probability__c = true;
        calc1.Field_API_Name__c = 'Access_To_Funds__c';
        calc1.Assessment_Values__c = 'Funding Approved/Budget Known';
        calc1.Score__c = 5;
        calc1.Value_based_on_Oppty_Type__c = 'Section A';
        calc1.RFX__c = TRUE;
        calc1.Non_RFX__c = TRUE;
        calc1.Service_Weights__c = 1;
        calc1.TEK_Weights__c = 1;
        calc1.ONS_Weights__c = 1;
        oaCalcualtor.add(calc1);
        // insert records in to custom setting
        OA_Calculator__c calc2 = new OA_Calculator__c();
        calc2.Name = 'Test Field 2';
        calc2.Consider_for_BV__c = true;
        calc2.Consider_for_Standard_Probability__c = true;
        calc2.Field_API_Name__c = 'Aligned_to_defined_GS_Objective__c';
        calc2.Assessment_Values__c = 'Yes';
        calc2.RFX__c = TRUE;
        calc2.Non_RFX__c = TRUE;
        calc2.Score__c = 3;
        calc2.Service_Weights__c = 1;
        calc2.TEK_Weights__c = 1;
        calc2.ONS_Weights__c = 1;
        oaCalcualtor.add(calc2);
        // insert records in to custom setting
        OA_Calculator__c calc3 = new OA_Calculator__c();
        calc3.Name = 'Test Field 3';
        calc3.Consider_for_BV__c = true;
        calc3.Consider_for_Standard_Probability__c = true;
        calc3.Field_API_Name__c = 'Aligned_to_defined_GS_Objective__c';
        calc3.Assessment_Values__c = 'No';
        calc3.Score__c = 1;
        calc3.RFX__c = TRUE;
        calc3.Non_RFX__c = TRUE;
        calc3.Service_Weights__c = 1;
        calc3.TEK_Weights__c = 1;
        calc3.ONS_Weights__c = 1;
        oaCalcualtor.add(calc3);
        // insert records in to custom setting
        OA_Calculator__c calc4 = new OA_Calculator__c();
        calc4.Name = 'Test Field 4';
        calc4.Consider_for_BV__c = true;
        calc4.Consider_for_Standard_Probability__c = true;
        calc4.Field_API_Name__c = 'Compelling_Event__c';
        calc4.Assessment_Values__c = 'Solution is required, pain is verified and felt';
        calc4.Score__c = 7;
        calc4.Value_based_on_Oppty_Type__c = 'Section A';
        calc4.RFX__c = TRUE;
        calc4.Non_RFX__c = TRUE;
        calc4.Service_Weights__c = 1;
        calc4.TEK_Weights__c = 1;
        calc4.ONS_Weights__c = 1;
        oaCalcualtor.add(calc4);
        // insert records in to custom setting
        OA_Calculator__c calc5 = new OA_Calculator__c();
        calc5.Name = 'Test Field 5';
        calc5.Consider_for_BV__c = true;
        calc5.Consider_for_Standard_Probability__c = true;
        calc5.Field_API_Name__c = 'Champion_Exec_as_Key_Decision_Maker__c';
        calc5.Assessment_Values__c = 'Solution is required, pain is verified and felt';
        calc5.Score__c = 7;
        calc5.RFX__c = TRUE;
        calc5.Non_RFX__c = TRUE;
        calc5.Service_Weights__c = 1;
        calc5.TEK_Weights__c = 1;
        calc5.ONS_Weights__c = 1;
        calc5.Value_based_on_Oppty_Type__c = 'Section B';
        oaCalcualtor.add(calc5);
        // insert records in to custom setting
        OA_Calculator__c calc6 = new OA_Calculator__c();
        calc6.Name = 'Test Field 6';
        calc6.Consider_for_BV__c = false;
        calc6.Consider_for_Standard_Probability__c = true;
        calc6.Field_API_Name__c = 'Formal_Decision_Criteria__c';
        calc6.Assessment_Values__c = 'Defined';
        calc6.Score__c = 3;
        calc6.RFX__c = TRUE;
        calc6.Non_RFX__c = TRUE;
        calc6.Value_based_on_Oppty_Type__c = 'Section A';
        calc6.Service_Weights__c = 1;
        calc6.TEK_Weights__c = 1;
        calc6.ONS_Weights__c = 1;
        oaCalcualtor.add(calc6);
        // insert records
        insert oaCalcualtor;
        
        List<OA_Calculator_Weights_mapping__c> oaCalcualtorWeights = new List<OA_Calculator_Weights_mapping__c>();
        // insert records in to custom setting
        OA_Calculator_Weights_mapping__c calcw1 = new OA_Calculator_Weights_mapping__c();
        calcw1.Name = 'Test Field 1';
        calcw1.Opco__c = 'Aerotek, Inc.';
        calcw1.Business_Unit__c = 'EASI';
        calcw1.Weights_to_be_used__c= 'Service_Weights__c';
        calcw1.POA_Multiplier__c= 100;
        calcw1.BV_Multiplier__c= 100;
        calcw1.Override_Standard_Probability__c= FALSE;
        calcw1.Use_Stage_Criteria__c = False;        
        oaCalcualtorWeights.add(calcw1);
        
        OA_Calculator_Weights_mapping__c calcw2 = new OA_Calculator_Weights_mapping__c();
        calcw2.Name = 'Test Field 2';
        calcw2.Opco__c = 'TEKsystems, Inc.';
        calcw2.Business_Unit__c = 'TEK Global Services';
        calcw2.Weights_to_be_used__c= 'ONS_Weights__c';
        calcw2.POA_Multiplier__c= 80;
        calcw2.BV_Multiplier__c= 100;
        calcw2.Override_Standard_Probability__c= TRUE;
        calcw2.Use_Stage_Criteria__c = TRUE;        
        oaCalcualtorWeights.add(calcw2);
        
        insert oaCalcualtorWeights;
     }
     // Method to insert profile extension data
     public static void createProfileExtension()
     {
         Profile_Extension__c ext = new Profile_Extension__c(Allow_Delete_Reqs__c = false,Name = 'Aerotek AM');
         insert ext;
     }
     
}