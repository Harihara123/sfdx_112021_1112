@isTest(SeeAllData=true)
Global class TestHeaderController implements WebServiceMock {
           // Implement this interface method
    global void doInvoke(
                           Object stub,
                           Object request,
                           Map<String, Object> response,
                           String endpoint,
                           String soapAction,
                           String requestName,
                           String responseNS,
                           String responseName,
                           String responseType) 
       {       
             
          List<WS_Header_Service.getSFDCHeaderResponseType> Header = new List<WS_Header_Service.getSFDCHeaderResponseType>(); 
          WS_Header_Service.getSFDCHeaderResponseType WsHeader = new WS_Header_Service.getSFDCHeaderResponseType();  
          WsHeader.WentDirectClient = 'Mock response';
          Header.add(WsHeader);  
          WS_Header_Service.getSFDCHeaderResponseRoot_element respElement = new WS_Header_Service.getSFDCHeaderResponseRoot_element();  
          respElement.getSFDCHeaderResponse= Header;
          response.put('response_x', respElement);  
      }

       static testmethod void contactTestRun(){
        
        // Test data
     /*   Reqs__c req = new Reqs__c(name='test',Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req; */
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Header_Contact_Data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       HeaderController AccPage = new HeaderController(controller);       
       accPage.HeaderInformation();             
    }
    
    static testmethod void contactTestRun1()
    {
        // Test data
      /*  Reqs__c req = new Reqs__c(name='test',Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req; */
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1-234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Header_Contact_Data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       HeaderController AccPage = new HeaderController(controller);       
       accPage.HeaderInformation();
       accpage.startrequest();       
    }
    
    static testmethod void accountTestRun(){
        // Test data
      /*  Reqs__c req = new Reqs__c(name='test',Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req; */
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                         
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
       PageReference pageRef = Page.Header_Account_Data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       HeaderController AccPage = new HeaderController(controller);
       accPage.HeaderInformation(); 
       accpage.startrequest();            
    }
    
    static testmethod void accountTestRun1()
    {
        // Test data        
      /*  Reqs__c req = new Reqs__c(name='test',Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req; */
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
       PageReference pageRef = Page.Header_Account_Data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       HeaderController AccPage = new HeaderController(controller);
       accPage.HeaderInformation();
       accpage.startrequest();
       accPage.exceptionDescription ='test';
       accPage.exceptionMessage ='test';
                    
    }
    
    static testMethod void testWebService(){
         Integration_Endpoints__c custom_setting_obj1 = new Integration_Endpoints__c();
         Integration_Certificate__c custom_setting_obj2=new Integration_Certificate__c();
                  
         custom_setting_obj1.name='TIBCO.TestHeader';
         custom_setting_obj1.Endpoint__c= 'https://sf2allegisdcmSit.allegistest.com:20048/ESFAccount';
         insert custom_setting_obj1;              
                  Test.setMock(WebServiceMock.class, new TestHeaderController());
                  test.startTest();
                       List<String> ContactID= new list<string>() ;
                       List<WS_Header_Service.getSFDCHeaderResponseType> Header = new List<WS_Header_Service.getSFDCHeaderResponseType>();
                       WS_Header_ServiceType.GetSFDCHeaderEndpoint1 WSHeader = new WS_Header_ServiceType.GetSFDCHeaderEndpoint1 ();
                       Header = WSHeader.getHeaders('1234','123','123','1231',ContactID);
                      
                  test.stopTest();
    } 
    
}