@IsTest
public class TestLinkedInApigeeFacade  {

    @IsTest
    public static void testAsyncResponseNoBody(){
        RestRequest mRequest = new RestRequest();
        RestResponse mResponse = new RestResponse();
        mRequest.requestURI = 'https://allegisgroup--aprildev.lightning.force.com/services/apexrest/LinkedIn';
        mRequest.httpMethod = 'POST';
        RestContext.request =  mRequest;
        RestContext.response = mResponse;
        Integer statusCode = LinkedInApigeeFacade.doPost();
        System.debug(statusCode);
        System.assertEquals(500, statusCode);
    }

    @IsTest
    public static void testAsyncResonseWithBody(){
        RestRequest mRequest = new RestRequest();
        RestResponse mResponse = new RestResponse();
        mRequest.requestURI = 'https://allegisgroup--aprildev.lightning.force.com/services/apexrest/LinkedIn';
        mRequest.httpMethod = 'POST';
        String mBodyString = '{ "id":"59a92119-3b72-4d2f-8e12-137a13180df6-1","type":"EXPORT_CANDIDATE_PROFILE","expiresAt":1481402799192 }';
        mRequest.requestBody = Blob.valueOf(mBodyString);
        RestContext.request =  mRequest;
        RestContext.response = mResponse;
        Integer statusCode = LinkedInApigeeFacade.doPost();
        System.debug(statusCode);
        System.assertEquals(200, statusCode); 
    }

    @IsTest
    public static void testAsyncResonseWithBadBody(){
        RestRequest mRequest = new RestRequest();
        RestResponse mResponse = new RestResponse();
        mRequest.requestURI = 'https://allegisgroup--aprildev.lightning.force.com/services/apexrest/LinkedIn';
        mRequest.httpMethod = 'POST';
        //missing " in front of expiresAt
        String mBodyString = '{ "id":"59a92119-3b72-4d2f-8e12-137a13180df6-1type":"EXPORT_CANDIDATE_PROFILE","expiresAt:1481402799192 }';
        mRequest.requestBody = Blob.valueOf(mBodyString);
        RestContext.request =  mRequest;
        RestContext.response = mResponse;
        Integer statusCode = LinkedInApigeeFacade.doPost();
        System.debug(statusCode);
        System.assertEquals(400, statusCode); 
    }

}