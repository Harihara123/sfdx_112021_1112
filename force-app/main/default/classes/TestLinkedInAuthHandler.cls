@isTest
private class TestLinkedInAuthHandler {
	
	@isTest static void testConfigurationRetrieval() {
		Boolean tConfigured = FALSE; 
		LinkedInAuthHandlerForAts tHandler; 
		tHandler= new LinkedInAuthHandlerForAts(LinkedInEnvironment.DEV);
		//test successful call 
        tConfigured = tHandler.getConfigurationItems();
		if(tConfigured){
			tConfigured = TRUE;
			System.assertEquals('8HuXHvDie4QcqYXW', tHandler.mClientSecret);
			System.assertEquals('6930ezifu57y', tHandler.mClientId);
			System.assertEquals('https://www.linkedin.com/oauth/v2/accessToken', tHandler.mAuthorizationURL);
			System.debug('mClientSecret= ' + tHandler.mClientSecret);
			//System.assertEquals('https://api.linkedin.com/v2/atsCandidates', tHandler.mCallUrl);
		}else{
			System.assertEquals(tConfigured, FALSE);
		} 
		tConfigured = FALSE;
		tHandler= new LinkedInAuthHandlerForAts(LinkedInEnvironment.TEST);
        tConfigured = tHandler.getConfigurationItems();
		if(tConfigured){
			tConfigured = TRUE;
			System.assertEquals('8HuXHvDie4QcqYXW', tHandler.mClientSecret);
			System.assertEquals('6930ezifu57y', tHandler.mClientId);
			System.assertEquals('https://www.linkedin.com/oauth/v2/accessToken', tHandler.mAuthorizationURL);
			System.debug('mClientSecret= ' + tHandler.mClientSecret);
			//System.assertEquals('https://api.linkedin.com/v2/atsCandidates', tHandler.mCallUrl);
		}else{
			System.assertEquals(tConfigured, FALSE);
		}
		tConfigured = FALSE; 
		tHandler= new LinkedInAuthHandlerForAts(LinkedInEnvironment.PROD);
        tConfigured = tHandler.getConfigurationItems();
		if(tConfigured){
			tConfigured = TRUE;
			System.assertEquals('AOOP3b3cA97rZsVh', tHandler.mClientSecret);
			System.assertEquals('jkluydoo20z9', tHandler.mClientId);
			System.assertEquals('https://www.linkedin.com/oauth/v2/accessToken', tHandler.mAuthorizationURL);
			System.debug('mClientSecret= ' + tHandler.mClientSecret);
			//System.assertEquals('https://api.linkedin.com/v2/atsCandidates', tHandler.mCallUrl);
		}else{
			System.assertEquals(tConfigured, FALSE);
		}  
	} 
	@isTest static void goodCallEndToEnd(){ 
		LinkedInAuthHandlerForAts tHandler = new LinkedInAuthHandlerForAts(LinkedInEnvironment.DEV);
		LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
    	mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
    	mReturnJson.expires_in = '1799';
    	LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
		Test.setMock(HttpCalloutMock.class, tMock);
		LinkedInAuthHandlerForAts.LinkedInDAO mAuthDao = new LinkedInAuthHandlerForAts.LinkedInDAO();
		try{
			mAuthDao = tHandler.getLinkedInAuthorization();
		}catch(LinkedInAuthException lae){
			System.assertNotEquals(null, lae);
		}
		System.debug(mAuthDao); 
	}
	@isTest static void badCallEndToEnd(){ 
		LinkedInAuthHandlerForAts tHandler = new LinkedInAuthHandlerForAts(LinkedInEnvironment.DEV);
		LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
    	mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
    	mReturnJson.expires_in = '1799';
    	LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 500);
		Test.setMock(HttpCalloutMock.class, tMock);
		LinkedInAuthHandlerForAts.LinkedInDAO mAuthDao = new LinkedInAuthHandlerForAts.LinkedInDAO();
		try{
			mAuthDao = tHandler.getLinkedInAuthorization();
		}catch(LinkedInAuthException lae){
			System.assertNotEquals(null, lae);
		}
		System.debug(mAuthDao); 
	}	
}