@isTest(SeeAllData=false)
public class TestMassUpdate {
    
    private static testMethod void TestMySave()
  {
      
      List <Account> lstAccount = new List<Account>();
      Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
      
      lstAccount.add(acct);
        
        insert lstAccount;
      
      
      Test.setCurrentPage(Page.Addtoaccountteam);
  ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstAccount);
  stdSetController.setSelected(lstAccount);
  ActController ext = new ActController(stdSetController);
       ext.MassAccounts();
       List <Target_Account__c> lstTarget = new List<Target_Account__c>();
      lstTarget=[select id,user__c,Account__c from Target_Account__c where Account__c =: acct.id ];
      system.assertEquals(lstTarget[0].Account__c, acct.id);
      ext.Targetaccounts();
      lstTarget=[select id,user__c,Account__c,Target__c from Target_Account__c where Account__c =: acct.id ];
      system.assertEquals(lstTarget[0].Target__c, True);
          system.assertEquals(lstTarget[0].Account__c, acct.id);
      ext.Deleteaccounts();
      lstTarget=[select id,user__c,Account__c,Target__c from Target_Account__c where Account__c =: acct.id ];
      system.assertEquals(lstTarget.size(), 0);
      ext.cancel();
      
     
  }

}