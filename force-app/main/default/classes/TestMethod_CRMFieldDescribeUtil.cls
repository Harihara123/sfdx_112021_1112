/*************************************************************************************************
Apex Class Name :  TestMethod_CRMFieldDescribeUtil
Version         : 1.0 
Created Date    : 12/19/2017 
Function        : Test Method for triggers
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* ***                                                               Original Version
* Sandeep                  12/19/2017                Initial Version of CRM_FieldDescribeUtil
**************************************************************************************************/ 
@isTest(seeAlldata=false)
public class TestMethod_CRMFieldDescribeUtil {
    
    static testMethod void Test_GetDependentOptionsImpl(){
        Schema.sObjectField opcoControlField= Opportunity.OpCo__c.getDescribe().getSObjectField();
		Schema.sObjectField depedentField=Opportunity.Req_Division__c.getDescribe().getSObjectField();
		Map<String, List<String>> resultmap=CRM_FieldDescribeUtil.getDependentOptionsImpl(depedentField,opcoControlField);
        System.assert(resultmap.size()>0);
        System.assert(resultmap.get('AG EMEA').size()>0,'Test failed');
    }

}