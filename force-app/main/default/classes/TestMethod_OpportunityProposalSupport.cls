/*************************************************************************************************
Apex Class Name :  TestMethod_TeamingPartnersController
Version         : 1.0 
Created Date    : 
Function        : TestMethod_TeamingPartnersController
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* ***                                                               Original Version
* Kdolch                     09/28/2013              Added Impacted Regions on line29
**************************************************************************************************/ 
@isTest(seeAlldata=true)
private class TestMethod_OpportunityProposalSupport {

    //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    
    static testMethod void Test_OpportunityProposalSupport() {   
         
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
                
        Test.startTest();    
        if(user != Null) {
            Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'pursure', closedate = system.today()+1);
            insert newopportunity; 
            
            System.assertNotEquals (newopportunity,null);                       
            OpportunityProposalSupport.SendProposals(newopportunity.id);
            
            newopportunity.Proposal_Support_Count__c = 0;
            update newopportunity;
            OpportunityProposalSupport.SendProposals(newopportunity.id);    
            try {                   
                OpportunityProposalSupport.SendProposals(null);
            } catch (QueryException err) {
                system.assert(err.getmessage().contains('List has no rows for assignment to SObject'));
            }
        }
        Test.stopTest(); 
    }
}