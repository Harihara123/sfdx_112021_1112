/**********************************************************************
Apex class Name     : TestMethod_ReqMerge
Version             : 1.0
Created Date        : 04.06.2015
Function            : Test Method for Req Merge

Modification Log :
*----------------------------------------------------------------------
*    Developer            Date            Description
*----------------------------------------------------------------------
*    *****
*    Aditya V        04.06.2015        Original Version
***********************************************************************/

@isTest
private class TestMethod_ReqMerge {
    
    static User user = TestDataHelper.createUser('System Administrator');
    
    public static testMethod void Test_ReqMerge() {

        insert user;
        
        system.runAs(user)
       {  
       Account NewAccounts = new Account(
                      Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = 'TEST-1111',
                      Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
                      ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
                      ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
                      BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
                      BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
                  );
         insert NewAccounts;
                  
          User_Organization__c newOffice = new User_Organization__c();
          newOffice.Office_Name__c = 'TEST';
          newOffice.Active__c = true;
          insert newOffice ;   
                  
        Reqs__c pr = new Reqs__c(VMS_Requirement_Status__c='On Hold',VMS_Requirement_Closed_Reason__c='On Hold',Max_Submissions_Per_Supplier__c=10,VMS_End_Client__c='WLPT',
                 Req_Origination_System__c='Salesforce',Merged_Req_ID__c='2001-4',Merged_Req_System__c='Beeline',VMS__c=True,Proactive_Req__c=True);
                  
        Reqs__c rc = new Reqs__c(
                  Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
                  Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
                  Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,
                  Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample', Organization_Office__c = newOffice.id, VMS_Requirement_Status__c='On Hold',VMS_Requirement_Closed_Reason__c='On Hold',Max_Submissions_Per_Supplier__c=10,VMS_End_Client__c='WLPT',
                  Proactive_Req_Link__c=pr.id, Req_Origination_System__c='Beeline',Merged_Req_ID__c='a1Tq00000005tQf',Merged_Req_System__c='Salesforce',VMS__c=True,Proactive_Req__c=True);
                  
           test.startTest();
           upsert rc;
           test.stopTest();
        
        ApexPages.StandardController ReqMergeAV = new ApexPages.StandardController(rc);
        ReqMerge rm = new ReqMerge(ReqMergeAV);
        rm.MergeReqs();
        system.assert(rm != null);
    }
    
}
}