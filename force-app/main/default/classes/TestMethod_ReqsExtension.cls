/*************************************************************************************************
Apex Class Name :  TestMethod_ReqsExtension
Version         : 1.0 
Created Date    : 
Function        : Test Method for Reqs Extension
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* ***                                                               Original Version
* Rajkumar                  03/06/2013              Added Test methods to include the validation for Stagging Reqs
* Dthuo                     09/12/2013              Added Impacted Regions on line39
**************************************************************************************************/ 
@isTest(seeAlldata= false)
private class TestMethod_ReqsExtension {

    //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');    
    static testMethod void Test_ReqsExtension() {   
         
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id);
        Account_National_Owner_Fields__c cusSett = new Account_National_Owner_Fields__c();
        cusSett.Name = 'Aerotek_SSO_Owner__c';
        insert cusSett ;
        Product2 testProduct=TdAcc.createTestProduct();
        insert testProduct;
        Test.startTest();    
        if(user != Null) {
        
        
            Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity' , Accountid = lstNewAccounts[0].id,
                        Formal_Decision_Criteria__c = 'Defined', 
                        Response_Type__c = 'RFI' , stagename = 'Interest', 
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1);  
            insert NewOpportunity;
                                
            Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
            insert newreq;                                 
            
            newreq.Primary_Contact__c = lstNewContact.id;
            newreq.Account__c = lstNewAccounts[0].Id;
            newreq.National_Account_Manager__c = userinfo.getuserid();
            newReq.Filled__c = 10;
            newReq.Bill_Rate_Max__c = 50; 
            newReq.Bill_Rate_Min__c = 25;
            newReq.Standard_Burden__c = 2;
            newReq.Duration__c = 5;
            newReq.Duration_Unit__c = 'days';
            newReq.Job_category__c = 'admin';
            newReq.Req_priority__c = 'green';
            newReq.Pay_Rate_Max__c = 10;
            newReq.Pay_Rate_Min__c = 5;
            newReq.Job_description__c = 'test';
            newReq.Organizational_role__c = 'Centre Manager';
            newReq.Product__c= testProduct.Id;
            newReq.stage__c =  'Qualified';
      
            update newreq;
            

            
            // Insert Recruiter Team
            Recruiter_Team__c RTeam = new Recruiter_Team__c(Name = 'SampleData', public__c = True);
            insert Rteam;
            try {
                Team_Members__c RTeamMemberExp = new Team_Members__c(Contact__c = lstNewContact.id,Recruiter_Team_Default__c = Rteam.id);
                insert RTeamMemberExp;
            } catch(exception err) {
                system.assert(err.getmessage().contains('FIELD_FILTER_VALIDATION_EXCEPTION'));
            }
            
            lstNewContact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(label.Contact_Recruiter_Recordtype_Name).getRecordTypeId();
            lstNewContact.Contact_Active__c = true;
            update lstNewContact;
             // Insert Recruiter Team Memeber's contact with 'Recruiter' Contact.
            Team_Members__c RTeamMember = new Team_Members__c(Contact__c = lstNewContact.id,Recruiter_Team_Default__c = Rteam.id);
            insert RTeamMember;            
            
            // Call ReqsExtension             
            ApexPages.currentPage().getParameters().put('govtAccount',string.valueof(lstNewAccounts[0]));
            ApexPages.currentPage().getParameters().put('retUrl',string.valueof(lstNewContact.id));
            ReqsExtension  Reqext = new ReqsExtension (new ApexPages.StandardController(newreq));
            Reqext.saveReq();
            Reqext.getDefaultRecruiterTeams();
            Reqext.getOprCompSelOpp();
            Reqext.getDivSelOpp();
            Reqext.ChangeOprCom();
            Reqext.ChangeSegment();
            Reqext.getSegmentSelOpp();
            Reqext.ChangeDiv();
            Reqext.getJobCodeSelOpp();
            Reqext.ChangeJobCode();
            Reqext.getCatSelOpp();
            Reqext.ChangeCat();
            Reqext.changeSkill(); 
            Reqext.displayErrorMessages(); 
            ReqExt.clearErrorMessages();
            ReqExt.getReqObj();
            
            newReq.stage__c =  'Qualified';
            newreq.Account__c = lstNewAccounts[0].id;
            newReq.Product__c =testProduct.Id;
            update newreq;
                        
            // Call ReqsExtension             
            ApexPages.currentPage().getParameters().put('retUrl',string.valueof(lstNewAccounts[0].id));
            ReqsExtension  Reqextacc = new ReqsExtension (new ApexPages.StandardController(newreq));
            
            // Call ReqsExtension             
            ApexPages.currentPage().getParameters().put('retUrl',string.valueof(NewOpportunity.id));
            ReqsExtension  Reqextopp = new ReqsExtension (new ApexPages.StandardController(newreq));                                                        
                
            // Call OfficeInformationHelper
            OfficeInformationHelper office = new OfficeInformationHelper();           
        }
        Test.stopTest();
    }
    static testmethod void test_StagingReqExtension(){
            TestData TdAcc = new TestData(1);
            Product2 testProduct=TdAcc.createTestProduct();
            insert testProduct;     
            //Testing Staging Rec..             
            Reqs__c newStagingTestreq = new Reqs__c (Product__c=testProduct.Id, stage__c = 'Staging', Placement_Type__c = 'Contract',
            Positions__c = 10);             
            insert newStagingTestreq; 
            
            test.startTest();
            system.runAs(testAdminUser){                
                 // Call ReqsExtension             
                ApexPages.currentPage().getParameters().put('Staging','1');
                ApexPages.currentPage().getParameters().put('Id',newStagingTestreq.Id);
                ReqsExtension  Reqext = new ReqsExtension (new ApexPages.StandardController(newStagingTestreq));
                Reqext.saveReq();
                
            }
        
    }
     static testmethod void test_StagingCloneReqExtension(){
        
            TestData TdAcc = new TestData(1);
            Product2 testProduct=TdAcc.createTestProduct();
            insert testProduct;     
            //Testing Staging Rec..             
            Reqs__c newStagingTestreq = new Reqs__c (Product__c=testProduct.Id, stage__c = 'Staging', Placement_Type__c = 'Contract',
            Positions__c = 10);             
            insert newStagingTestreq; 
            
            test.startTest();
            system.runAs(testAdminUser){                
                 // Call ReqsExtension             
                ApexPages.currentPage().getParameters().put('Clone','1');
                ApexPages.currentPage().getParameters().put('Id',newStagingTestreq.Id);
                ReqsExtension  Reqext = new ReqsExtension (new ApexPages.StandardController(newStagingTestreq));
                Reqext.saveReq();
                
            }
        
    }
}