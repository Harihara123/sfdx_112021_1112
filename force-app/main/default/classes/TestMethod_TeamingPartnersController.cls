/*************************************************************************************************
Apex Class Name :  TestMethod_TeamingPartnersController
Version         : 1.0 
Created Date    : 
Function        : TestMethod_TeamingPartnersController
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------                 
* ***                                                               Original Version
* Dthuo                     09/12/2013              Added Impacted Regions on line29
**************************************************************************************************/ 
@isTest(seeAlldata=true)
private class TestMethod_TeamingPartnersController {

    //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    
    static testMethod void Test_TeamingPartnersController() {   
         
        TestData TdAcc = new TestData(3);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
                
        Test.startTest();    
        if(user != Null) {
        
            Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[1].id,Impacted_Regions__c='Option1', 
            Proposal_Support_Count__c = null, stagename = 'pursure', closedate = system.today()+1);
            insert newopportunity;             
            
            Teaming_Partner__c Teamingpartner = new Teaming_Partner__c(Account__c = lstNewAccounts[0].id,
                Agreement_Signed_Date__c = system.today(), Subcontract_Number__c = '100',
                    Opportunity__c = newopportunity.id);
            insert Teamingpartner;                       
                             
            PageReference pg = Page.TeamingPartners; Test.setCurrentPage(pg); 
            ApexPages.currentPage().getParameters().put('id', newopportunity.id); 
            System.assertNotEquals (newopportunity,null);
                      
            TeamingPartnersController Teaming = new TeamingPartnersController();
            Teaming.GetlstTeamPartners();
            ApexPages.currentPage().getParameters().put('TeammingID', Teamingpartner.id); 
            Teaming.newReq();        
            Teaming.Cancel();
                
            Teaming_Partner__c Teamingpartner1 = new Teaming_Partner__c(Account__c = lstNewAccounts[2].id,
                Agreement_Signed_Date__c = system.today(), Subcontract_Number__c = '100',
                    Opportunity__c = newopportunity.id);
            try {
                insert Teamingpartner1;             
            } catch(DMLException err) {
                system.assert(err.getmessage().contains('DUPLICATE_VALUE'));
            } 
        }
        Test.stopTest();
    }
}