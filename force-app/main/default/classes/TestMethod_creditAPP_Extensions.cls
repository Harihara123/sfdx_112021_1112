@isTest(seeAlldata=true)
private class TestMethod_creditAPP_Extensions {

    //variable declaration
   // static User user = TestDataHelper.createUser('Aerotek AM');  
    
    static testMethod void Test_creditAPP_Extensions() {            
    
        User user = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND isActive=true LIMIT 1]; 
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
                       
        Test.startTest();    
        if(user != Null) {
            system.runAs(user)
            { 
            //Test converage for the 'NewCreditApp' visualforce page
            PageReference pageRef = Page.NewCreditApp;
            Test.setCurrentPageReference(pageRef);        
            ApexPages.currentPage().getParameters().put('retUrl',lstNewAccounts[0].id);
            creditAPP_Extensions CreditExt = new creditAPP_Extensions(new ApexPages.StandardController(New Credit_App__c()));
            CreditExt.redirect();
          }
        }
        Test.StopTest();
    }
}