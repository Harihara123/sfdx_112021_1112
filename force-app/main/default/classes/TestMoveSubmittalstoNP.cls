@isTest
public class TestMoveSubmittalstoNP {

    
   static testmethod void processRecordsTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
       Test.startTest();
       
       TestData td=new TestData();
       td.listSize=5;
       List<Account> acclist=td.createAccounts();
       List<Contact> conList=td.createContacts('Talent');
       
       List<Opportunity> oppList = td.createOpportunities();
       Opportunity opp=oppList.get(0);
       opp.Req_Total_Positions__c=1;
       update opp;
       Schema.DescribeSObjectResult d = Schema.SObjectType.Order;

       Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
	   Schema.RecordTypeInfo rtByName =  rtMapByName.get('OpportunitySubmission');
       
       List<Order> odList=new List<Order>();
       Order NewOrder1 = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,
                                    Status ='Started',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());
    
       Order NewOrder2 = new Order(Name = 'New Order2', EffectiveDate = system.today()+1,
                                    Status ='Submitted',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());   
       odList.add(NewOrder1);
   //    odList.add(NewOrder2);
       insert odList;
       
       Set<Id> recordIds=new Set<Id>();
       recordIds.add(opp.Id);
       
       MoveSubmittalstoNP.processRecords(recordIds);
          
       //NewOrder1.status='Started';
       //update NewOrder1;
          
    }
    @isTest
    static void processRecordsfromPostionObjTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG_EMEA');
        insert DRZSettings;
        TestData td=new TestData();
        td.listSize=5;
        List<Account> acclist=td.createAccounts();
        List<Contact> conList=td.createContacts('Talent');
       
       List<Opportunity> oppList = td.createOpportunities();
       Opportunity opp=oppList.get(0);
       opp.Req_Total_Positions__c=1;
       opp.IsAutoClose__c = true;
       update opp;
       Schema.DescribeSObjectResult d = Schema.SObjectType.Order;

       Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
	   Schema.RecordTypeInfo rtByName =  rtMapByName.get('OpportunitySubmission');
       
       List<Order> odList=new List<Order>();
       /*Order NewOrder1 = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,
                                    Status ='Started',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());*/
    
       Order NewOrder2 = new Order(Name = 'New Order2', EffectiveDate = system.today()+1,
                                    Status ='Submitted',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());   
       //odList.add(NewOrder1);
       odList.add(NewOrder2);
       insert odList;
       
       Set<Id> recordIds=new Set<Id>();
       recordIds.add(opp.Id);

       test.starttest();
       MoveSubmittalstoNP.processRecordsfromPostionObj(opp.Id);
       test.stoptest();
       Order ord = [select Status from Order where Id=:odList[0].Id LIMIT 1];
       string orderStatus=ord.Status.left(14);
       system.assertEquals(orderStatus,'Not Proceeding');
    }
    @isTest
    static void submittalsToNPForAutoClosedReqTest(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.;AG_EMEA');
        insert DRZSettings;
        TestData td=new TestData();
        td.listSize=5;
        List<Account> acclist=td.createAccounts();
        List<Contact> conList=td.createContacts('Talent');
       
       List<Opportunity> oppList = td.createOpportunities();
       Opportunity opp=oppList.get(0);
       opp.Req_Total_Positions__c=1;
       opp.IsAutoClose__c = true;
       update opp;
       Schema.DescribeSObjectResult d = Schema.SObjectType.Order;

       Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
	   Schema.RecordTypeInfo rtByName =  rtMapByName.get('OpportunitySubmission');
       
       List<Order> odList=new List<Order>();
       /*Order NewOrder1 = new Order(Name = 'New Order1', EffectiveDate = system.today()+1,
                                    Status ='Started',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());*/
    
       Order NewOrder2 = new Order(Name = 'New Order2', EffectiveDate = system.today()+1,
                                    Status ='Submitted',AccountId = opp.AccountId, OpportunityId = opp.Id,shiptocontactid=conList[0].id,
                                    recordtypeid = rtByName.getRecordTypeId());   
       //odList.add(NewOrder1);
       odList.add(NewOrder2);
       insert odList;
       
       Set<Id> recordIds=new Set<Id>();
       recordIds.add(opp.Id);

       test.starttest();
       MoveSubmittalstoNP.submittalsToNPForAutoClosedReq(recordIds);
       test.stoptest();
       Order ord = [select Status from Order where Id=:odList[0].Id LIMIT 1];
       string orderStatus=ord.Status.left(14);
       system.assertEquals(orderStatus,'Not Proceeding');
    }
}