@isTest
private class TestNewVPAccountController 
{
  static testMethod void testSave() 
    {
        Account a = new Account();
        a.name='Account Name';
        a.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        insert a;
 
        PageReference ref = new PageReference('/apex/AccountPage?aId=' + a.Id);
        test.setCurrentPage(ref);
 
        Test.startTest();
          ExtNewAccount controller = new ExtNewAccount(new ApexPages.StandardController(a));
          controller.NewHistoric();
        Test.stopTest();
    }
  static testMethod void testSave2() 
    {
       Account a = new Account();
       a.name='Account Name';
       a.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
       insert a;
 
       PageReference ref = new PageReference('/apex/AccountPage?aId=' + a.Id);
       test.setCurrentPage(ref);
 
       Test.startTest();
         ExtNewAccount controller = new ExtNewAccount(new ApexPages.StandardController(a));
         controller.NewImplementation();
       Test.stopTest();
 
    }
 }