@isTest(SeeAllData=true)
Global class TestOpen_ReqsController implements WebServiceMock {
           // Implement this interface method
    global void doInvoke(
                           Object stub,
                           Object request,
                           Map<String, Object> response,
                           String endpoint,
                           String soapAction,
                           String requestName,
                           String responseNS,
                           String responseName,
                           String responseType) 
       {       
          List<WS_Open_Req_service.GetOpenRequisitionsResponseType > Open_ReqsNew = new List<WS_Open_Req_service.GetOpenRequisitionsResponseType >(); 
          WS_Open_Req_service.GetOpenRequisitionsResponseType OReq = new WS_Open_Req_service.GetOpenRequisitionsResponseType();
          OReq.AccountManagerName = 'Mock response'; 
          Open_ReqsNew.add(OReq);
          WS_Open_Req_service.GetOpenRequisitionsResponseRoot_element respElement = new WS_Open_Req_service.GetOpenRequisitionsResponseRoot_element();  
          respElement.GetOpenRequisitionsResponse = Open_ReqsNew;
          response.put('response_x', respElement); 
      }
      
    static testmethod void contactTestRun(){        
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Req_Contact_data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       Open_ReqsController AccPage = new Open_ReqsController(controller);    
       accPage.Open_ReqsInformation();
       accPage.startRequest();
       accPage.processResponse();            
    }
    
    static testmethod void contactTestRun1(){
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
        Contact con = new Contact(
            LastName = 'TESTCONTACT1',
            AccountId = acct.id,
            FirstName = 'testcon',
            Email = 'testemail@test.com',
            MailingStreet = '123 Main St',
            MailingCity = 'Hanover',
            MailingState = 'MD',
            MailingPostalCode = '55001',
            MailingCountry = 'USA',Siebel_ID__c='1234',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
            
       insert con;
       System.assertNotEquals(null,con.id);
       
       //Calling page and class
       PageReference pageRef = Page.Req_Contact_data360;
       Test.setCurrentPage(pageRef);
       string conDetails= con.id;
       ApexPages.currentPage().getParameters().put('Contact',conDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(con);
       Open_ReqsController AccPage = new Open_ReqsController(controller);
       accPage.Open_ReqsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.getFirstPage();
       accPage.getPagenumber();
       accPage.getTotalpages();
       accPage.firstBtnClick();
       accPage.previousBtnClick();  
       accPage.nextBtnClick();
       accPage.lastBtnClick();
       accPage.getPreviousButtonEnabled();
       accPage.getNextButtonEnabled();
       accPage.sortAction();
       accPage.fetchMyRecords();
       accPage.fetchAllRecords();
       accPage.resetVariables();
            
    }
    
    static testmethod void accountTestRun(){
        // Test data
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
       PageReference pageRef = Page.Req_Account_data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails); 
        
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       Open_ReqsController AccPage = new Open_ReqsController(controller);       
       accPage.Open_ReqsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.getFirstPage();
       accPage.getPagenumber();
       accPage.getTotalpages();
       accPage.firstBtnClick();
       accPage.previousBtnClick();  
       accPage.nextBtnClick();
       accPage.lastBtnClick();
       accPage.getPreviousButtonEnabled();
       accPage.getNextButtonEnabled();
       accPage.sortAction();
       accPage.fetchMyRecords();
       accPage.fetchAllRecords();
       accPage.resetVariables();
      // accPage.errorMessages();
             
    }
    
    static testmethod void accountTestRun1()
    {
        // Test data
        
        Reqs__c req = new Reqs__c(Siebel_ID__c='1234',Filled__c=8,Loss__c=4,Wash__c=2,Positions__c=20);
        insert req;
        Account acct = new Account(
            Name = 'TESTACCTFORMAT1',
            Phone= '+1.111.222.3333',Fax= '+1.111.222.3333',ShippingCity = 'Testshipcity',ShippingCountry = 'USA',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='USA',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );  
        
        insert acct;
        System.assertNotEquals(null,acct.id);
        
       
       //Calling page and class
        PageReference pageRef = Page.Req_Account_data360;
       Test.setCurrentPage(pageRef);
       string accDetails= acct.id;
       ApexPages.currentPage().getParameters().put('Account',accDetails); 
       ApexPages.currentPage().getParameters().put('Full','1'); 
      
       ApexPages.StandardController controller = new ApexPages.StandardController(acct);
       Open_ReqsController AccPage = new Open_ReqsController(controller); 
       accPage.Open_ReqsInformation();
       accPage.startRequest();
       accPage.processResponse();
       accPage.exceptionDescription ='test';
             
    }
    
    static testMethod void testWebService(){
     Integration_Endpoints__c custom_setting_obj1 = new Integration_Endpoints__c();
         Integration_Certificate__c custom_setting_obj2=new Integration_Certificate__c();
                  
         custom_setting_obj1.name='TIBCO.TestOpenreq';
         custom_setting_obj1.Endpoint__c= 'https://sf2allegisdcmSit.allegistest.com:20046/ESFAccount';
         insert custom_setting_obj1;              
                  Test.setMock(WebServiceMock.class, new TestOpen_ReqsController());
                  test.startTest();
                       List<String> ContactID= new list<string>() ;
                       List<WS_Open_Req_service.GetOpenRequisitionsResponseType > Open_ReqsNew = new List<WS_Open_Req_service.GetOpenRequisitionsResponseType >(); 
                       WS_Open_Req_serviceType.RequisitionEndpoint1 WSOpen_Reqs = new WS_Open_Req_serviceType.RequisitionEndpoint1();
                          Open_ReqsNew = WSOpen_Reqs.getOpenRequisitions_Account('12-34'); // For Account
                          Open_ReqsNew = WSOpen_Reqs.getOpenRequisitions_Contact(ContactID); // For Contact    
                          
                       AsyncWS_Open_Req_service.GetOpenRequisitionsResponseRoot_elementFuture ws_request = new AsyncWS_Open_Req_service.GetOpenRequisitionsResponseRoot_elementFuture();
                       AsyncWS_Open_Req_serviceType.AsyncRequisitionEndpoint1 asyncWS_openReq = new AsyncWS_Open_Req_serviceType.AsyncRequisitionEndpoint1();
                       list<WS_Open_Req_service.GetOpenRequisitionsResponseType> ws_response = new list<WS_Open_Req_service.GetOpenRequisitionsResponseType>();                   
                      
                  test.stopTest();
    }
    
}