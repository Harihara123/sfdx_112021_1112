@isTest(seeAlldata=true)
public class TestSupportRequestTriggerHandler {
    static User user = TestDataHelper.createUser('system administrator');
    
    
    static testMethod void TestSupportRequestTriggerHandler() {  
        System.runAs(user) {
            
            test.starttest();
            
            List<support_request__c> sptreqlst=new list<support_Request__c>();
            List<opportunity> Opplst=new list<opportunity>();
            string recordtypeids =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId();
            TestData TdAcc = new TestData(1);
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            
            Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                         Formal_Decision_Criteria__c = 'Defined', 
                                                         Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                         Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Clinical Solutions',
                                                         OPCO__c ='Aerotek, Inc',                    
                                                         Impacted_Regions__c='Industrial Central',recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),
                                                         Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                         Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1);  
            
            opplst.add(NewOpportunity);
            
            
            
            
            Opportunity NewOpportunity1 = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                          Formal_Decision_Criteria__c = 'Defined', 
                                                          Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                          Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Vertical Sales',Amount=5500000,
                                                          opco__c='TEKsystems, Inc.',                    
                                                          Impacted_Regions__c='TEK Canada',Product_Team__c='VS Healthcare',recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),
                                                          Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                          Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1); 
            opplst.add(NewOpportunity1);
            
            Opportunity NewOpportunity2 = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                          Formal_Decision_Criteria__c = 'Defined', 
                                                          Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                          Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Vertical Sales',Amount=5500000,
                                                          recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),                   
                                                          Impacted_Regions__c='TEK Canada',Product_Team__c='VS Healthcare',opco__c='TEKsystems, Inc.',
                                                          Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                          Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1); 
            opplst.add(NewOpportunity2);
            
            
            Opportunity NewOpportunity3 = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                          Formal_Decision_Criteria__c = 'Defined', 
                                                          Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                          Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Test',Amount=55000,
                                                          
                                                          Impacted_Regions__c='TEK Canada',Product_Team__c='Test',opco__c='TEKsystems, Inc.',recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),
                                                          Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                          Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1); 
            opplst.add(NewOpportunity3);
            
            Insert Opplst;
            
            Support_Request__c sptReq = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity.id,Description__c='test',recordtypeid= recordtypeids,ownerid=UserInfo.getUserId());
            sptreqlst.add(sptreq);
            system.debug('spt '+sptReq.owner.name);
            
            Support_Request__c sptReq1 = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity1.id,Description__c='test',recordtypeid=Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId(),ownerid=UserInfo.getUserId() );
            sptreqlst.add(sptreq1);
            
            Support_Request__c sptReq2 = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity1.id,Description__c='test',recordtypeid=Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId(),ownerid=UserInfo.getUserId() );
            sptreqlst.add(sptreq2);
            Support_Request__c sptReq3 = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity2.id,Description__c='test',recordtypeid=Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId(),ownerid=UserInfo.getUserId() );
            sptreqlst.add(sptreq3);
            
            Support_Request__c sptReq4 = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity3.id,Description__c='test3',recordtypeid=Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId(),ownerid=UserInfo.getUserId() );
            sptreqlst.add(sptreq4);
            
            insert sptreqlst;
            
            sptreq3.Description__c = 'testing update';
            string ADC_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Alternate Delivery Center').getRecordTypeId();
            sptreq3.recordtypeid = ADC_recordtypeid;
            sptreq3.Status__c='Approved';        
            update sptreq3;  
            system.debug('sptrq'+ sptReq1);
            test.stopTest();
            List<Support_Request__c> sprtqry=[select id, owner.name from Support_Request__c where id =:sptReq1.id ];
            
            system.debug('sprtqry'+ sprtqry[0].owner.name);
            list<Group> queue = [SELECT Id,Name,Type FROM Group WHERE Type = 'queue' AND Name = 'Aerotek Healthcare'];
            
            system.assertEquals(sprtqry[0].Ownerid, queue[0].id);
            
            
            
            
        } 
        
    }
    
    static testMethod void TestSupportRequestTriggerHandler2() {  
        
        test.starttest();
        User user1 = TestDataHelper.createUser('system administrator');
        user1.Division__c ='EASi Engineering';
        user1.Office__c ='ONS-SDO - Tallahassee, IN-40844';
        //user1.Business_Unit_Region__c ='E&S Central';
        
        System.runAs(user1) {
            
            List<support_request__c> sptreqlst=new list<support_Request__c>();
            List<opportunity> Opplst=new list<opportunity>();
            string recordtypeids =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId();
            TestData TdAcc = new TestData(1);
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            
            Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                         Formal_Decision_Criteria__c = 'Defined', 
                                                         Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                         Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='EASi Engineering',
                                                         OPCO__c ='Aerotek, Inc',                    
                                                         Impacted_Regions__c='E&S Central',recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),
                                                         Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                         Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1);  
            
            opplst.add(NewOpportunity);
            
            Insert Opplst;
            
            Support_Request__c sptReq = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity.id,Description__c='test',recordtypeid= recordtypeids,ownerid=UserInfo.getUserId());
            sptreqlst.add(sptreq);
            system.debug('spt '+sptReq.owner.name);
            
            insert sptreqlst;
            
            List<Support_Request__c> sprtqry =[select id, owner.name from Support_Request__c where id =:sptReq.id ];
            
            system.debug('sprtqry'+ sprtqry[0].owner.name);
            list<Group> queue = [SELECT Id,Name,Type FROM Group WHERE Type = 'queue' AND Name = 'Orphaned Queue'];
            
            system.assertEquals(sprtqry[0].Ownerid, queue[0].id);
        }	
        test.stopTest();
    }
    
    static testMethod void TestSupportRequestTriggerHandler3() {  
        
        test.starttest();
        System.runAs(user) {
            
            List<support_request__c> sptreqlst=new list<support_Request__c>();
            List<opportunity> Opplst=new list<opportunity>();
            string recordtypeids =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId();
            TestData TdAcc = new TestData(1);
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            
            Opportunity NewOpportunity2 = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                          Formal_Decision_Criteria__c = 'Defined', 
                                                          Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                          Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Vertical Sales',Amount=5500000,
                                                          recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),                   
                                                          Impacted_Regions__c='TEK Canada',Product_Team__c='VS Healthcare',opco__c='TEKsystems, Inc.',
                                                          Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                          Sales_Resource_Requirements__c = 'Less than standard effort/light investment',
                                                          Delivery_Office__c ='00570 - SRC Torrance CA',                                                      
                                                          CloseDate = system.today()+1); 
            opplst.add(NewOpportunity2);
            
            Insert Opplst;
            
            Support_Request__c sptReq3 = new  Support_Request__c(Status__c='Submitted',Opportunity__c=NewOpportunity2.id,
                                                                 Description__c='test',
                                                                 recordtypeid=Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Proposal').getRecordTypeId(),
                                                                 ownerid=UserInfo.getUserId(),
                                                                 Alternate_Delivery_Shared_Positions__c = 10, 
                                                                 Alternate_Delivery_Office__c = '00570 - SRC Torrance CA' );
            sptreqlst.add(sptreq3);
            system.debug('spt '+sptreq3.owner.name);
            
            string ADC_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Alternate Delivery Center').getRecordTypeId();
            
            Support_Request__c sptReq4 = new  Support_Request__c(Status__c='Submitted',
                                                                 Opportunity__c=NewOpportunity2.id,Description__c='test',
                                                                 recordtypeid=ADC_recordtypeid,
                                                                 ownerid=UserInfo.getUserId(),
                                                                 Alternate_Delivery_Shared_Positions__c = 10, 
                                                                 Alternate_Delivery_Office__c = '00570 - SRC Torrance CA' );
            sptreqlst.add(sptreq4);
            insert sptreqlst;
            
            sptreq3.Description__c = 'testing update';
            sptreq3.recordtypeid = ADC_recordtypeid;
            sptreq3.Status__c='Approved';  
            update sptreq3; 
            
        }	
        test.stopTest();
    }    
    
    static testMethod void TestSupportRequestTriggerHandler4() 
    {  
        System.runAs(user) 
        {
			List<support_request__c> sptreqlst=new list<support_Request__c>();
            List<opportunity> Opplst=new list<opportunity>();
            string recordtypeids =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Escalation').getRecordTypeId();
            TestData TdAcc = new TestData(1);
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            
            Opportunity NewOpportunity2 = new Opportunity( Name = 'New Opportunity'+lstNewAccounts[0].name , Accountid = lstNewAccounts[0].id,
                                                          Formal_Decision_Criteria__c = 'Defined', 
                                                          Response_Type__c = 'RFI' , stagename = 'Interest', 
                                                          Customers_Application_or_Project_Scope__c = 'Precisely Defined',BusinessUnit__c='Vertical Sales',Amount=5500000,
                                                          recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),ownerid=UserInfo.getUserId(),                   
                                                          Impacted_Regions__c='TEK Canada',Product_Team__c='VS Healthcare',opco__c='TEKsystems, Inc.',
                                                          Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                                                          Sales_Resource_Requirements__c = 'Less than standard effort/light investment',
                                                          Delivery_Office__c ='00570 - SRC Torrance CA',                                                      
                                                          CloseDate = system.today()+1); 
            opplst.add(NewOpportunity2);
            
            insert Opplst;
            
            Test.starttest();
            
            string ADC_recordtypeid =Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Alternate Delivery Center').getRecordTypeId();
            
            Support_Request__c supportRequestRecord = new  Support_Request__c(Status__c='Approved',
                                                                              Opportunity__c=NewOpportunity2.id,Description__c='test',
                                                                              recordtypeid=ADC_recordtypeid,
                                                                              ownerid=UserInfo.getUserId(),
                                                                              Alternate_Delivery_Shared_Positions__c = 10, 
                                                                              Alternate_Delivery_Office__c = '00570 - SRC Torrance CA' );
            insert supportRequestRecord;
            
            supportRequestRecord.Alternate_Delivery_Positions_approved__c = 10;
            update supportRequestRecord;
            
            delete supportRequestRecord;
            
            Test.stopTest();
            
        }	
    }    
}