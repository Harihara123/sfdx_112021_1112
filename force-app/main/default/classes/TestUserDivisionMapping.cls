@isTest
public class TestUserDivisionMapping  {
	
	@IsTest
    static void updateDivisionMapping() {
		List<String> profiles = new List<String>{'Aerotek AM','Single Desk 1','System Administrator'};
		List<User> usrList = new List<User>();
		List<Profile> userProfile = [select Name from Profile where Name IN :profiles];
		for(Integer i=0; i<3; i++) {
			user newUser;
			long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
			Integer random = Math.Round(Math.Random() * timeDiff );
			newUser = new User(alias = 'test', 
							email='testuser'+i+'@allegisgroup.com',
							emailencodingkey='UTF-8', 
							lastname= 'User'+i, 
							languagelocalekey='en_US',
							localesidkey='en_US', 
							profileid = userProfile[i].Id,
							timezonesidkey='America/Los_Angeles', 
							OPCO__c = 'ONS',
							Region__c= 'Aerotek MidAtlantic',
							Region_Code__c = 'test',
							Office_Code__c = '10001',
							Peoplesoft_Id__c = string.valueOf(random * 143),
							User_Application__c = 'CRM',
							Division__c = 'Aerotek Aviation LLC',
							Office__c = 'ONS-Dayton, OH-00853',
							username='testusr'+random+'@allegisgroup.com.dev');    
			usrList.add(newUser);
		}

		insert usrList;
	}

	@IsTest
    static void updateDivisionMappingBatch() {
		List<String> profiles = new List<String>{'Aerotek AM','Single Desk 1','System Administrator'};
		List<User> usrList = new List<User>();
		List<Profile> userProfile = [select Name from Profile where Name IN :profiles];
		for(Integer i=0; i<3; i++) {
			user newUser;		
             
			long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
			Integer random = Math.Round(Math.Random() * timeDiff );
			newUser = new User(alias = 'test', 
							email='testuser'+i+'@allegisgroup.com',
							emailencodingkey='UTF-8', 
							lastname= 'User'+i, 
							languagelocalekey='en_US',
							localesidkey='en_US', 
							profileid = userProfile[i].Id,
							timezonesidkey='America/Los_Angeles', 
							OPCO__c = 'ONS',
							Region__c= 'Aerotek MidAtlantic',
							Region_Code__c = 'test',
							Office_Code__c = '10001',
							Peoplesoft_Id__c = string.valueOf(random * 143),
							User_Application__c = 'CRM',
							Division__c = 'Aerotek Aviation LLC',
							Office__c = 'ONS-Dayton, OH-00853',
							username='testusr'+random+'@allegisgroup.com.dev');    
			usrList.add(newUser);
		}

		insert usrList;

		Test.startTest();

		BatchUpdateDivisionMappingFields obj = new BatchUpdateDivisionMappingFields();
		DataBase.executeBatch(obj); 

		Test.stopTest();
	}
}