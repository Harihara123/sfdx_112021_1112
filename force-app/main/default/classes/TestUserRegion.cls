/*
* This test class will cover the user region association upon creation.
* Createdby : Dthuo
* Created : 02-18-2014
*/

@isTest (SeeAllData=false)
Private class TestUserRegion {
    
    static User SysUser = TestDataHelper.createUser('System Administrator');
    
public static testmethod void createUser()
{
       if(SysUser !=Null){
                 
                    
         string status; 
         List<User> User = new List<user>();
         Profile userProfile = [select Name from Profile where Name ='Aerotek AM'];
           
          system.runAs(SysUser)
          {
         
         User_Organization__c Org = new User_Organization__c(OpCo_Code__c ='10001',Office_Code__c='20002',OpCo_Name__c='OpCo', Region_Name__c='Region',Region_Code__c='30003');
         insert org; 
                    
          }              
             long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
             Integer random = Math.Round(Math.Random() * timeDiff );
             
             User userd =  new User(alias = 'test', 
                             email='testuser@allegisgroup.com',
                             emailencodingkey='UTF-8', 
                             lastname= 'User', 
                             isActive=true,
                             languagelocalekey='en_US',
                             localesidkey='en_US', 
                             profileid = userProfile.Id,
                             timezonesidkey='America/Los_Angeles', 
                             //Peoplesoft_Id__c = string.valueOf(random * 143),
                             UserPermissionsJigsawProspectingUser= false,  
                             UserPermissionsMobileUser = false,                                                       
                             username='testusr'+random+'@allegisgroup.com.dev'); 
               
         
            Test.startTest(); 
        
             insert userd;
                   
         User auser=[Select id,isActive,UserPermissionsJigsawProspectingUser,UserPermissionsMobileUser from User where Id=:userd.Id];
          For (User a:[SELECT region__c, OpCo__c, email from User where Id=:auser.Id]){
            
            
            // Validate that the user region is updated
            system.debug('user1 ' + a.region__c );
            system.debug('user2 '  + a.OpCo__c);
            system.debug('user3 '  + a.email);
               }
           
          
           If ( userd.isActive == true){
            userd.UserPermissionsJigsawProspectingUser= true;
            userd.UserPermissionsMobileUser = true;
           
            system.debug('userActive: ' + userd.UserPermissionsMobileUser);          
           }
           update userd;
           For (User b:[SELECT UserPermissionsMobileUser from User WHERE Id=:userd.Id]){
            system.debug('userActive1: ' + userd.UserPermissionsMobileUser);
           }
           
           system.runAs(SysUser)
          {
           if(UserInfo.getProfileId().left(15) == system.label.Profile_System_Integration && auser.IsActive==true) auser.UserPermissionsMobileUser = true;
           
           system.debug('profile ' + auser.UserPermissionsMobileUser);
           update auser;
           
           user zuser =[SELECT isActive from User where Id =:auser.Id];
           zuser.isactive= false;
           system.debug('userActivez: ' + zuser.isActive);
           update zuser;
           
           User buser=[Select id,isActive,UserPermissionsJigsawProspectingUser,UserPermissionsMobileUser from User where Id=:auser.Id];
           If ( buser.isActive == false){
            buser.UserPermissionsJigsawProspectingUser= false;
            buser.UserPermissionsMobileUser = false;
            buser.isActive = true;    
            system.debug('userActive1: ' + buser.UserPermissionsMobileUser);     
            system.assert(buser.UserPermissionsMobileUser = true);  
           }
           update buser;
           
          
            system.debug('userlog: ' + buser.UserPermissionsMobileUser); 
          }
       }  
            Test.stopTest(); 
 }
//}
}