/*******************************************************************
* Name  : TestUtility
* Author: Vivek Ojha(Appirio India) 
* Date  : March 8, 2015
* Details : Test class for Utility
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* Nidish                      6/18/2015                     Added line 25 to insert account
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class TestUtility {

    static testMethod void testCreateHistory() {
     
        List<History_Record_Setting__c> recList1 = new List<History_Record_Setting__c>();
        History_Record_Setting__c settings = new History_Record_Setting__c();
        settings.Name = 'FirstRecord';
        settings.Object_Name__c = 'Account';
        settings.Field_Name__c = 'Name';
        recList1.add(settings);
        settings = new History_Record_Setting__c();
        settings.Name = 'SecondRecord';
        settings.Object_Name__c = 'Account';
        settings.Field_Name__c = 'Aerotek_SSO_Owner__c';
        recList1.add(settings);
        insert recList1;
        TestData TdAcc = new TestData(1);
        Account acc = TestData.newAccount(1);
        insert acc;
        Account acc1 = TestData.newAccount(1);
        insert acc1;
        map<Id, Account> newMap = new map<Id, Account>();
        newMap.put(acc.Id, acc);
        
        Contact cont = TestData.newContact(acc.Id,1, 'Client');
        insert cont;
        map<Id, Contact> newMapCon = new map<Id, Contact>();
        newMapCon.put(cont.Id, cont);
        
        
        Account oldAcc = acc.Clone();
        Contact oldCon = cont.Clone();
        
        map<Id, Account> oldMap = new map<Id, Account>();
        oldAcc.Name = 'New Name';
        oldAcc.Aerotek_SSO_Owner__c = cont.Id ;
        oldMap.put(acc.Id, oldAcc);
        
        map<Id, Contact> oldMapCon = new map<Id, Contact>();
        oldCon.Best_Way_to_Approach__c= 'New Name';
        oldCon.AccountId = acc1.Id;
        oldMapCon.put(cont.Id, oldCon);
        
        Utility.createHistoryRecord(oldMap,newMap,  'Account');
        
        delete settings ;
        List<History_Record_Setting__c> recList = new List<History_Record_Setting__c>();
        settings = new History_Record_Setting__c();
        settings.Name = 'ThirdRecord';
        settings.Object_Name__c = 'Contact';
        settings.Field_Name__c = 'Best_Way_to_Approach__c';
        recList.add(settings);
        settings = new History_Record_Setting__c();
        settings.Name = 'FourthRecord';
        settings.Object_Name__c = 'Contact';
        settings.Field_Name__c = 'AccountId';
        recList.add(settings);
        insert recList;
        
        Utility.createHistoryRecord(oldMapCon,newMapCon,  'Contact');
        
        Account_Intel_History__c history = [select Field_API_Name__c from Account_Intel_History__c where Account__c = :acc.Id LIMIT 1];
        
        System.assertNotEquals(history, null, 'History Record should be created for account');
        System.assertEquals(history.Field_API_Name__c, 'Name', 'History record should be created for account name');
    }

    static testMethod void testValidLinkedInURL() {        
        Boolean res = Utility.validateLinkedInURL('https://www.linkedin.com/');
        System.assert(res);
    }
    static testMethod void testInValidLinkedInURL() {        
        Boolean res = Utility.validateLinkedInURL('ht2tps://www.linkedin1.com/in/phanisammeta');
        System.assert(!res);
    }
}