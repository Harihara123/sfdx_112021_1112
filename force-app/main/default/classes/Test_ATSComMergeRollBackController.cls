@istest
public class Test_ATSComMergeRollBackController  {
    @isTest
    public static void testATSComMerge(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account acc1 = CreateTalentTestData.createTalent();
		Contact con1 = CreateTalentTestData.createTalentContact(acc1);
        con1.TC_LastName__c='Test';
        Update con1;
        Account acc2 = CreateTalentTestData.createTalentAccount();
        Contact con2 = CreateTalentTestData.createTalentContact(acc2);
        Talent_Work_History__c twh = new Talent_Work_History__c(uniqueId__c='uniq', SourceId__c = 'R.Test', Start_Date__c = System.today(), End_Date__c = System.today() + 5, Talent_Recruiter__c = 'peoplesoftId', Talent_Account_Manager__c = 'peoplesoftId2', Job_Title__c = 'Job Title', FInish_Code__c = '', Talent__c = acc1.Id);
        insert twh;
        Opportunity opp = new Opportunity(Name = 'TESTOPP', StageName = 'Offer Accepted', AccountId = acc1.id, Req_Product__c = 'Permanent', CloseDate = System.today(), OpCo__c = 'Major, Lindsey & Africa, LLC');
        insert opp;
        Test.startTest();
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
        Order o = new Order(Name = 'TestOrder',AccountId = acc1.id,ShipToContactId = con1.Id,OpportunityId = opp.Id,Status = 'Linked',EffectiveDate = System.today(),RecordTypeId = recTypeId);
        insert o;
        CreateTalentTestData.createTalentEvent(con1,acc1);
        CreateTalentTestData.createTalentTask(con1);
        Tag_Definition__c td = new Tag_Definition__c(Tag_Name__c='Test');
        Insert td;
        Contact_Tag__c ct = new Contact_Tag__c(Contact__c = con1.Id,Tag_Definition__c=td.Id);
        Insert ct;
        Assessment__c assessmentObj = new Assessment__c(Assessment_Name__c = 'Test', Talent__c = con1.Id);
        //Insert assessmentObj;
        Talent_Recommendation__c tRec = new Talent_Recommendation__c(Talent_Contact__c = con1.Id);
        Insert tRec;
        AccountTeamMember atm = new AccountTeamMember(AccountId = acc1.Id, UserId = UserInfo.getUserId());
        Insert atm;
        TalentMerge__c tm = new TalentMerge__c(Name = 'Param', Acc_Cont_Update_Query__c = 'Batch_Id__c=:BatchId AND Account_Merge_Status__c = \'TO_BE_MERGED\' AND Fetched__c = false Limit:numberOfRecords', InputDataQuery__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' Limit:numberOfRecords', InputDataQuery_Backup__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' AND Fetched__c = false Limit:numberOfRecords', NumDays__c = 6);
        Insert tm;
        Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Account_Victim_Id__c = acc2.Id, Account_Survivor_Id__c = acc1.Id, Contact_Victim_Id__c = con2.Id, Contact_Survivor_Id__c = con1.Id, Batch_Id__c=16);
		Insert tmm;
        Database.merge(acc1, acc2.Id);
        Database.merge(con1, con2.Id);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/talent/atscommergerollback/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        String result = ATSComMergeRollBackController.doPost(acc1.Id, acc2.Id, con1.Id, con2.Id, tmm.Id);
        Test.stopTest();
    }
    /*@isTest
    public static void testTalentMergehandler(){
        Account acc1 = CreateTalentTestData.createTalent();
		Contact con1 = CreateTalentTestData.createTalentContact(acc1);
        Account acc2 = CreateTalentTestData.createTalentAccount();
        Contact con2 = CreateTalentTestData.createTalentContact(acc2);
        Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Account_Victim_Id__c = acc2.Id, Account_Survivor_Id__c = acc1.Id, Contact_Victim_Id__c = con2.Id, Contact_Survivor_Id__c = con1.Id, Batch_Id__c=16);
		Insert tmm;
        Opportunity opp = new Opportunity(Name = 'TESTOPP', StageName = 'Offer Accepted', AccountId = acc1.id, Req_Product__c = 'Permanent', CloseDate = System.today(), OpCo__c = 'Major, Lindsey & Africa, LLC');
        insert opp;
        Test.startTest();
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
        Order o = new Order(Name = 'TestOrder',AccountId = acc1.id,ShipToContactId = con1.Id,OpportunityId = opp.Id,Status = 'Linked',EffectiveDate = System.today(),RecordTypeId = recTypeId);
        insert o;
        TalentMergeHandler.handleMerge(acc1.Id, acc2.Id, con1.Id, con2.Id, tmm.Id);
        Test.stopTest();
    }*/
    @isTest
    public static void testTalentMergeUpdateFetchDataResthandler(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/TalentMerge/Update/FetchInputData/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        TalentMerge__c tm = new TalentMerge__c(Name = 'Param', Acc_Cont_Update_Query__c = 'Batch_Id__c=:BatchId AND Account_Merge_Status__c = \'TO_BE_MERGED\' AND Fetched__c = false Limit:numberOfRecords', InputDataQuery__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' Limit:numberOfRecords', InputDataQuery_Backup__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' AND Fetched__c = false Limit:numberOfRecords', NumDays__c = 6);
        Insert tm;
        TalentMerge_Update_FetchDataRestHandler.doPost(1000, 16);
    }
    @isTest
    public static void testTalentMergeFetchDataResthandler(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/TalentMergeFetchData/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        TalentMerge__c tm = new TalentMerge__c(Name = 'Param', Acc_Cont_Update_Query__c = 'Batch_Id__c=:BatchId AND Account_Merge_Status__c = \'TO_BE_MERGED\' AND Fetched__c = false Limit:numberOfRecords', InputDataQuery__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' Limit:numberOfRecords', InputDataQuery_Backup__c = 'Batch_Id__c=:BatchId and Account_Merge_Status__c = \'ATS_COPIED\' AND Contact_Merge_Status__c = \'ATS_COPIED\' AND Fetched__c = false Limit:numberOfRecords', NumDays__c = 6);
        Insert tm;
        TalentMergeFetchDataRestHandler.doPost(1000, 16);
    }
    @isTest
    public static void testAtsComTalentMergeController(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account acc1 = CreateTalentTestData.createTalent();
		Contact con1 = CreateTalentTestData.createTalentContact(acc1);
        Account acc2 = CreateTalentTestData.createTalentAccount();
        Contact con2 = CreateTalentTestData.createTalentContact(acc2);
        Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Account_Victim_Id__c = acc2.Id, Account_Survivor_Id__c = acc1.Id, Contact_Victim_Id__c = con2.Id, Contact_Survivor_Id__c = con1.Id, Batch_Id__c=16);
		Insert tmm;
        Opportunity opp = new Opportunity(Name = 'TESTOPP', StageName = 'Offer Accepted', AccountId = acc1.id, Req_Product__c = 'Permanent', CloseDate = System.today(), OpCo__c = 'Major, Lindsey & Africa, LLC');
        insert opp;
        Test.startTest();
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
        Order o = new Order(Name = 'TestOrder',AccountId = acc1.id,ShipToContactId = con1.Id,OpportunityId = opp.Id,Status = 'Linked',EffectiveDate = System.today(),RecordTypeId = recTypeId);
        insert o;
        o.AccountId = acc2.id;
        o.ShipToContactId = con2.Id;
        update o;
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/talent/atscommerge/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        AtsComTalentMergeController.doPost(acc1.Id, acc2.Id, con1.Id, con2.Id, tmm.Id);
        Test.stopTest();
    }
}