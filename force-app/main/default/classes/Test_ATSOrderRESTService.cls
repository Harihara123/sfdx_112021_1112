@istest
public class Test_ATSOrderRESTService  {

    static testMethod void  insertSubmittal(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            String sourceSystmId = '54555334';
           	
            Account talAcc = CreateTalentTestData.createTalent();
            Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
			Job_Posting__c jobposting = createJobPostingObject(sourceSystmId);
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();

            ATSSubmittal submittal = new ATSSubmittal();
           	submittal.ecid = '';
            submittal.icid = '';
           submittal.inviterFullName = 'test name';
           submittal.vendorSegmentId = 'ved1111';
           submittal.vendorApplicationId ='CS_Application_233';
           
            submittal.vendorId = 'vendor101';
            submittal.feedSourceId = 'feed11';
            submittal.jobPostingId = sourceSystmId;
            submittal.talentId = talCont.Id;
            submittal.externalSourceId = 'Talent_Submittal_111';
            submittal.isApplicantSubmittal = true;
            submittal.appliedDate = '06/05/2020';
           
            submittal.desiredSalary = '1222';
            submittal.desiredRate = '122';
            submittal.transactionId = '12345677';
            submittal.integrationId = '12345678';
            submittal.talentDocumentId = '';
            submittal.acceptedTnc = 'true';
           	submittal.transactionsource = 'PP';
           	submittal.inviterEmail = 'test@demo.com';
           	submittal.ecvid = '12233';
           	

            req.requestURI = '/services/apexrest/Person/Application/V1/';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            ATSOrderRESTService.doPost(submittal);
            submittal.appliedDate = '2020-04-04T05:45:47Z';
           	ATSOrderRESTService.doPost(submittal);
            Test.stopTest();
     }
    }


    public static Job_Posting__c createJobPostingObject(String sourceSystmId){
        Job_Posting__c jobPosting = null;
        try{
             DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        	 insert DRZSettings;
            DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        	insert DRZSettings1; 
             List<Opportunity> lstOpportunity = new List<Opportunity>(); 
             TestData TdAcc = new TestData(1);
             testdata tdcont = new Testdata();
             string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
             String accName = '';
         //  Opportunity NewOpportunity = null;
             for(Account Acc : TdAcc.createAccounts()) {                     
          
           accName = 'New Opportunity'+Acc.name;
                Opportunity NewOpportunity  = new Opportunity( Name =  accName, LDS_Account_Name__c = Acc.id, Accountid = Acc.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                    Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                    Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                    Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly', Req_Division__c='easi');  

                    lstOpportunity.add(NewOpportunity);
                    break;
            }
            insert lstOpportunity;
            // Ceate Job_Posting__c
                   jobPosting = new Job_Posting__c(Currency_Code__c='111',OFCCP_Flag__c=true, 
            Delete_Retry_Flag__c=false, Job_Title__c = 'testJobTitle',Opportunity__c=lstOpportunity[0].Id, Posting_Detail_XML__c='Sample Description',
            Private_Flag__c=false,RecordTypeId=Utility.getRecordTypeId('Job_Posting__c','Posting'),Source_System_id__c='R.'+sourceSystmId,State__c='MD');

            insert jobPosting;

            return jobPosting; 
        }catch(Exception e){
            System.debug('Could not insert JOBPOSTINGOBJECT '+e.getMessage());
        }
        return jobPosting;
    }
    // Test method for the update an existing order 
    static testMethod void  upsertSubmittal(){
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            String sourceSystmId = '541555334';
            Account talAcc = CreateTalentTestData.createTalent();
            Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
            Job_Posting__c jobPosting = createJobPostingObject(sourceSystmId);
            System.debug('jobPosting '+jobPosting);

            ATSSubmittal submittal = new ATSSubmittal();
            //submittal.accountId = '0011w000003ZaJNAA0';
            submittal.jobPostingId = sourceSystmId;
            submittal.talentId = talCont.Id;
            submittal.externalSourceId = 'Talent_Submittal_111';
            submittal.isApplicantSubmittal = true;
            submittal.appliedDate = '09/04/2018';
           submittal.ecid = '';
            submittal.icid = '';
           submittal.inviterFullName = 'test name';
           submittal.vendorSegmentId = 'ved1111';
           submittal.vendorApplicationId ='CS_Application_233';
           submittal.desiredSalary = '1222';
           submittal.desiredRate = '122';
		   submittal.inviterEmail = 'abc@yopmail.com';
           submittal.ecvid = 'a243546';
           
            req.requestURI = '/services/apexrest/Person/Application/V1/';
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response= res;
            Map<String, String> results = ATSOrderRESTService.doPost(submittal);
            String stats = results.get('Status');
            String recordId = results.get('recordId');

            System.assert(results != null && stats != null);
            Test.stopTest();
       }
    }

	@IsTest
	public static void getJobPosting_Test() {
		User user = TestDataHelper.createUser('System Integration');
		System.runAs(user) {
             Test.startTest();
				String sourceSystmId = '54555334';  
				String boardPostingId = '372789';        	
				Account talAcc = CreateTalentTestData.createTalent();
				Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
				Job_Posting__c jobposting = createJobPostingObject(sourceSystmId);
				
				Job_Posting_Channel_Details__c jpcd = new Job_Posting_Channel_Details__c();
				jpcd.Board_Posting_Id__c = boardPostingId;
				jpcd.Job_Posting_Id__c = jobposting.Id;
				insert jpcd;
           
				ATSSubmittal submittal = new ATSSubmittal();
				submittal.vendorId = '6';
				submittal.feedSourceId = 'feed11';
				submittal.jobPostingId = boardPostingId;
				submittal.talentId = talCont.Id;		
				
				Job_Posting__c jp = ATSOrderRESTService.getJobPosting(submittal);		
				System.assertNotEquals(jp.Opportunity__c, null);
			Test.stopTest();
		}
	}

}