@isTest
private class Test_ATS_OrderEventAggregation_RestApi  {

    private static String domainURL = '/services/apexrest/ordereventaggregatedcount';
    private static String methodType = 'GET'; 
    
    private static RestRequest req = new RestRequest(); 
    private static RestResponse res = new RestResponse();

    static {
        req.httpMethod = methodType; 
        req.requestURI = domainURL;  
    }


    static testMethod void testGetOrderEventAggregation() {
        req.addParameter('jobid', '');

        RestContext.request = req;
        RestContext.response = res;
        
        try {
            Map<String, Map<String, String>> aggreatedMap =  ATS_OrderEventAggregation_RestApi.getOrderEventAggregation();
            System.AssertEquals(5, 4);  
        } catch (Exception e) { 
            System.AssertEquals(5, 5);
        }
    }

    static testMethod void testGetOrderEvent() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account talent =  createAccount();
        ATS_Job__c job = createJob();
        Order order = getJobOrderId(talent, job ); 
        order = createSubmitAssociated(order);
        String jobOrderId = createOrderEventNotProceeding(order, talent);
        
        //System.Debug(logginglevel.warn, '>>>> The job Id Is :'  +  job.Id);

        req.addParameter('jobid', job.Id);
       
        RestContext.request = req;
        RestContext.response = res;
        
        try {
            Map<String, Map<String, String>> aggreatedMap =  ATS_OrderEventAggregation_RestApi.getOrderEventAggregation();
            System.Debug(logginglevel.warn, '>>>> The result Map is:'  +  aggreatedMap);
            System.AssertEquals(aggreatedMap.size(), 3); 
            Map<String, String> orderMap = aggreatedMap.get('Not Proceeding');
            String count = orderMap.get('count');
            System.AssertEquals(1, Integer.valueof(count)); 
        } catch (Exception e) { 
		System.debug('*****************e****'+e);
            //System.AssertEquals(5, 4);
        }
    }

    static Account createAccount() {
        Account account = CreateTalentTestData.createTalentAccount('RPO'); 
        Contact contact = CreateTalentTestData.createTalentContact(account); 

        return account; 
    }

    static ATS_Job__c createJob() {
        ATS_Job__c job = CreateTalentTestData.createATSJob();
        return job;
    }

    static Order getJobOrderId(Account account, ATS_Job__c  job) {
        Order order = CreateTalentTestData.associateTalentToJob(account , job);
        return order; 
    }

    static Order createSubmitAssociated(Order order) {
        order  = CreateTalentTestData.submitAssociated(order);
        return order; 
    }
  
    static String createOrderEventNotProceeding(Order order,  Account talent) {
        order = CreateTalentTestData.createOrderEventNotProceeding(Order, talent);
        return order.Id;
    }
}