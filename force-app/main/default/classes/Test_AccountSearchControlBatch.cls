/*******************************************************************
* Name  : Test_AccountSearchControlBatch
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 23, 2015
* Details : Test class for AccountSearchControlBatch
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_AccountSearchControlBatch {
   //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');
       
    static testMethod void test_AccountSearchControl() {
        // The query used by the batch job.
         string Clientrecordtypeid; 
      Clientrecordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        String query = 'SELECT Id,Account_Search__c,AccountId ,Account.name FROM Contact where recordtypeid=\'012U0000000DWoy\' LIMIT 1' ;
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();  
        Account accnt = TestData.newAccount(1);
        accnt.Name = 'testAccount';
        insert accnt; 
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id); 
        lstNewContact.Account_Search__c= accnt.Id;
        lstNewContact.Account = lstNewAccounts[0] ;
        lstNewContact.Account.name='test123';
        update lstNewContact;
        System.debug('lstNewContact.Account_Search__c=='+lstNewContact.Account_Search__c+'==lstNewAccounts[0].id=='+lstNewAccounts[0].id);
        System.debug('lstNewContact.Account_Search__c=='+lstNewContact.Account_Search__c+'==lstNewContact.Account.name=='+ lstNewContact.Account.name);       
        Test.startTest();
        AccountSearchControlBatch c = new AccountSearchControlBatch(query);
        Database.executeBatch(c, 1);
        Test.stopTest();
    }
   
}