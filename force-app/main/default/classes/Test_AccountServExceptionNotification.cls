@isTest

private class Test_AccountServExceptionNotification{

    //variable declaration
       
    static testMethod void Test_AccountServicesExceptionNotification(){  
        
        
        
        
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        
        
        
        
        Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
        insert newreq; 
        
        Integration_Response__c IRRecord = new Integration_Response__c();
        IRRecord.Object_Id__c = String.valueof(lstNewAccounts[0].id);
        IRRecord.Status__c = 'Success';
        insert IRRecord;
        
        
        List<Retry__c> lstRetry = new List<Retry__c>();
        for( Integer i = 0 ; i<=6 ; i++)
        {
            Retry__c retry = new Retry__c();
            retry.Object__c = 'Account';
            retry.Retry_Timestamp__c = string.valueof(system.today());
            retry.Status__c = 'Failed';
            retry.Id__c = String.valueof(lstNewAccounts[0].id);
            lstRetry.add(retry);
        }    
        insert lstRetry;
        
        List<Retry__c> lstRetryReq = new List<Retry__c>();

               
        for( Integer i = 0 ; i<=6 ; i++)
        {
            Retry__c retry = new Retry__c();
            retry.Object__c = 'Req';
            retry.Retry_Timestamp__c = string.valueof(system.today());
            retry.Status__c = 'Failed';
            retry.Id__c = String.valueof(newreq.id);
            lstRetryReq.add(retry);
        }    
        insert lstRetryReq;
        
        
        AccountServicesExceptionNotification sh2 = new AccountServicesExceptionNotification();
        String sch2 = '0 0 23 * * ?';
        Test.StartTest();
        system.schedule('execute', sch2, sh2);
        Test.StopTest();
        
    }
    
}