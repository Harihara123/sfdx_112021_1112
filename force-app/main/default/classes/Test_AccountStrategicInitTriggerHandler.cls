/*******************************************************************
* Name  : Test_AccountStrategicInitTriggerHandler
* Author: Vivek Ojha(Appirio India) 
* Date  : June 23, 2015
* Details : Test class for AccountStrategicInitTriggerHandler
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@IsTest(SeeAllData=false)
private class Test_AccountStrategicInitTriggerHandler {

    static testMethod void test_AccStratInitHandler() {
        Id recordId = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Strategic Initiative').getRecordTypeId();
       
        Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
        test.startTest();
       Focus_NonFocus__c myCS2 =new Focus_NonFocus__c(name='Event Query',Query__c='select id from Event where Accountid in :AccountStrIdset AND ownerid in:strownerid');
        insert myCS2;
        
        FocusNonFocusBatchDate__c mycs1 = new FocusNonFocusBatchDate__c(name='BatchDate',BatchranDate__c=date.today());
        insert mycs1;
        //create account
        Account acc = TestDataHelper.createAccount();//name= Test Account;
        insert acc;
        // create strategic initiatives
        Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative1',Start_Date__c = system.Today() - 30,
        recordTypeId = recordId1,
        Impacted_Region__c = 'test',
            Account_Prioritization__c ='A',
        Primary_Account__c= acc.Id,
        OpCo__c='Aerotek');
        insert si;
        
        
          Event event = new Event(subject='Meeting - +i',DurationInMinutes = 5,ActivityDateTime = system.now(),whatid=acc.id);
          
        
        insert event;
        update event;
        
        Event event2 = new Event(subject='Meeting - +i',DurationInMinutes = 5,ActivityDateTime = system.now(), Priority__c='');
        insert event2;
            update event2;
        
        
         
        
        batchFocusNonFocusUpdate bclean = new batchFocusNonFocusUpdate();
        
ID batchprocessid = Database.executeBatch(bclean,200);
        
        
        
       
        //Account_Strategic_Initiative__c accStratInit = new Account_Strategic_Initiative__c();
       // accStratInit.Account__c=acc.Id;
        //accStratInit.Strategic_Initiative__c=initiatives[0].Id;
        //insert accStratInit;
       // Update accStratInit;
        //Delete accStratInit;
        
                         
        
        
        
        List<event> eventassertion =new List<event>(); 
       eventassertion= [select id, priority__c from event where Accountid=:acc.id];
        system.debug('testpriority'+eventassertion);
        for(Event evt :eventassertion){
           system.assertEquals(string.valueof(evt.priority__c),string.valueof(si.Account_Prioritization__c)); 
        }
        List<event> assertioncheck = new List<event>();
        assertioncheck=[select id,priority__c from event where id =:event2.id ];
        system.debug('testass'+assertioncheck);
        //system.assertEquals(string.valueof(assertioncheck[0].priority__c), 'Non Focus');
        
        test.stopTest();
     //   Strategic_Initiative__c accStrat = [Select Id,Primary_Account__c From Strategic_Initiative__c Where Id=:initiatives[0].Id];
        
  //      System.assertEquals(accStrat.Primary_Account__c,acc.Id);
    }
 static testMethod void test_AccStratInitHandlerBeforeAndAfterDelete() {
     
      Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
      Account acc = TestDataHelper.createAccount();//name= Test Account;
      insert acc;
      
      Account acc1 = TestDataHelper.createAccount();//name= Test Account;
      insert acc1;
     Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative2',Start_Date__c = system.Today() - 30,
        recordTypeId = recordId1,
        Impacted_Region__c = 'test2',
        Account_Prioritization__c ='A',
        Primary_Account__c= acc.Id,
        OpCo__c='Aerotek2');
        
     insert si;
     
        Account_Strategic_Initiative__c accStratInit = new Account_Strategic_Initiative__c();
        accStratInit.Account__c = acc1.Id;
        accStratInit.Strategic_Initiative__c=si.Id;
        //accStratInit.Unique_Identifier__c='222060';
    
     insert  accStratInit;
     test.startTest();
     try {
         delete accStratInit;
         System.assertEquals(Date.today(), [select Priority_Changed_Date__c from Strategic_Initiative__c where id =: si.Id].Priority_Changed_Date__c);
            
     }catch(Exception e){
         
     }
     test.stopTest();
        
 }
}