@isTest
public class Test_AccountTalentsController {
    static testmethod void Test_AccountCurrentsJSON() {
        Test.startTest();
        Account newAccount1 = new Account (name = 'AcName',
	    BillingCity ='TestCity',
	    BillingCountry ='TestCountry',
	    BillingStreet ='TestStreet',
	    BillingPostalCode ='t3stcd3');

	    insert newAccount1;
	    
        List<Talent_Work_History__c> TalentWrkList=new List<Talent_Work_History__c>();
	    Talent_Work_History__c twh=new Talent_Work_History__c();
	    twh.End_Date__c=date.today()+8;
	    twh.Start_Date__c=date.today()-4;
	    twh.Talent__c=newAccount1.id;
        twh.Client_Account_ID__c=newAccount1.id;
	    TalentWrkList.add(twh);
	    Talent_Work_History__c twh1=new Talent_Work_History__c();
	    twh1.End_Date__c=date.today()-15;
	    twh1.Start_Date__c=date.today()-20;
	    twh1.Talent__c=newAccount1.id;
        twh1.Client_Account_ID__c=newAccount1.id;
	    TalentWrkList.add(twh1);
        insert TalentWrkList;       
        
        AccountTalentsController.getAccountCurrentsJSON(newAccount1.id);       
        
        Test.stopTest();
    }
    static testmethod void Test_ContactCurrentsJSON() {
        Test.startTest();
        
        Account newAccount1 = new Account (name = 'AcName',
	    BillingCity ='TestCity',
	    BillingCountry ='TestCountry',
	    BillingStreet ='TestStreet',
	    BillingPostalCode ='t3stcd3');

	    insert newAccount1;
        
        Contact cnt= TestData.newContact(newAccount1.id,1,'Client');        
        insert cnt;
	    
        List<Talent_Work_History__c> TalentWrkList=new List<Talent_Work_History__c>();
	    Talent_Work_History__c twh=new Talent_Work_History__c();
	    twh.End_Date__c=date.today()+8;
	    twh.Start_Date__c=date.today()-4;
	    twh.Talent__c=newAccount1.id;
        twh.Client_Account_ID__c=newAccount1.id;
        twh.Hiring_Manager_Contact_ID__c=cnt.id;
	    TalentWrkList.add(twh);
	    Talent_Work_History__c twh1=new Talent_Work_History__c();
	    twh1.End_Date__c=date.today()-15;
	    twh1.Start_Date__c=date.today()-20;
	    twh1.Talent__c=newAccount1.id;
        twh1.Client_Account_ID__c=newAccount1.id;
        twh1.Hiring_Manager_Contact_ID__c=cnt.id;        
	    TalentWrkList.add(twh1);
        
        insert TalentWrkList;       
        
        AccountTalentsController.getAccountCurrentsJSON(cnt.id);       
        
        Test.stopTest();
    }
    
    static testmethod void Test_PlacedJSON() {
        Test.startTest();
        
        Account newAccount1 = new Account (name = 'AcName',
	    BillingCity ='TestCity',
	    BillingCountry ='TestCountry',
	    BillingStreet ='TestStreet',
	    BillingPostalCode ='t3stcd3');
		newAccount1.Candidate_Status__c = 'Placed';
	    insert newAccount1;
        
        Contact cnt= TestData.newContact(newAccount1.id,1,'Client');        
        insert cnt;
	    
        List<Talent_Work_History__c> TalentWrkList=new List<Talent_Work_History__c>();
	    Talent_Work_History__c twh=new Talent_Work_History__c();
	    twh.End_Date__c=date.today()+8;
	    twh.Start_Date__c=date.today()-4;
	    twh.Talent__c=newAccount1.id;
        twh.Client_Account_ID__c=newAccount1.id;
        twh.Hiring_Manager_Contact_ID__c=cnt.id;
	    TalentWrkList.add(twh);
	    Talent_Work_History__c twh1=new Talent_Work_History__c();
	    twh1.End_Date__c=date.today()-15;
	    twh1.Start_Date__c=date.today()-20;
	    twh1.Talent__c=newAccount1.id;
        twh1.Client_Account_ID__c=newAccount1.id;
        twh1.Hiring_Manager_Contact_ID__c=cnt.id;        
	    TalentWrkList.add(twh1);
        
        insert TalentWrkList;       
        
        AccountTalentsController.getAccountCurrentsJSON(cnt.id);       
        
        Test.stopTest();
    }
}