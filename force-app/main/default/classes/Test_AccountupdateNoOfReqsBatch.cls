/*******************************************************************
* Name  : Test_AccountupdateNoOfReqsBatch
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 23, 2015
* Details : Test class for AccountupdateNoOfReqsBatch
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_AccountupdateNoOfReqsBatch {
     //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');    
    static testMethod void testAccountupdateNoOfReqs() {
        // The query used by the batch job.
        String query = 'SELECT Id FROM Account Limit 1' ;
                   //,Reqs__r.Open_Positions__c,Reqs__r.No_of_Open_Positions__c,Reqs__r.No_of_Reqs__c
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();   
        Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
            insert newreq; 
                   
        Test.startTest();
        AccountupdateNoOfReqsBatch c = new AccountupdateNoOfReqsBatch(query);
        Database.executeBatch(c);
        Test.stopTest();
    }
}