/*******************************************************************
* Name  : Test_Acct_Cont_Assoc_Trigger
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 25, 2015
* Details : Test class for Acct_Cont_Assoc_Trigger
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata=false)
private class Test_Acct_Cont_Assoc_Trigger {
	static User IntegrationUser = TestDataHelper.createUser(Label.Profile_System_Integration); 
    static testMethod void testAcct_Cont_Assoc_Trigger() {
    	test.startTest();
    	//system.runAs(IntegrationUser){
        	TestData Td = new TestData(3);  
		    TestData TdCont = new TestData();
		    List<Account> NewAccounts = Td.createAccounts();
		    List<Account_Contact_Association__c> lstAssociate = new List<Account_Contact_Association__c>();
		    
		    Contact ContObj = TdCont.createcontactwithoutaccount(NewAccounts[0].Id);
		    Contact ContnewObj = TdCont.createcontactwithoutaccount(NewAccounts[1].Id);
		    
		    Account_Contact_Association__c Associate1 = new Account_Contact_Association__c(
		    Account__c = NewAccounts[0].Id, Contact__c = ContnewObj.id, Is_Primary_Account__c = false, 
		    Status__c = 'Currently With');                
		    insert Associate1; 
		    try{
		    	delete Associate1;
		    }catch(Exception e){
		    	System.debug('Exception**'+e);
		    }
		    //System.assert(Associate1==null);
    	//}
		//undelete Associate1;
    	test.stopTest();
    	
    }
}