@isTest
private class Test_ActionsForSkuid{

    static testMethod void testNextStatusForSkuid() {
    
     List<String> request = new List<String>();
     request.add('Linked');
     request.add('Major, Lindsey & Africa, LLC');
     request.add('undefined');
     
        Test.startTest();
       
         List<String> results = ActionsForSkuid.invokeApexAction(request);

        Test.stopTest();
        
        
    }
    
    static testMethod void testNextStatusForATSJoSkuid() {
    
     List<String> request = new List<String>();
     request.add('Linked');
     request.add('Allegis Global Solutions, Inc.');
     request.add('JOB-000000731');
     
        Test.startTest();
         List<String> results = ActionsForSkuid.invokeApexAction(request);
        Test.stopTest();
       
    }
}