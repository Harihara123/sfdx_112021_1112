@isTest

private class Test_ActivityTimelineRestApi {
  
  private static String domainURL = '/services/apexrest/activitytimeline';
  private static String methodType = 'GET'; 
  
  private static RestRequest req = new RestRequest(); 
  private static RestResponse res = new RestResponse();
  
  static {
    req.httpMethod = methodType; 
    req.requestURI = domainURL;  
  }
  
  static testMethod void testGetContactWithActivities() {
    req = setParameterRequest(req, '', '', '', '', ''); 
    
    RestContext.request = req;
    RestContext.response = res;
    
    try {
      Contact contact = ATS_ActivityTimelineRestApi.getContactWithActivities();
      // b/c of missing account id this should not reach here if it does it should fail 
      System.AssertEquals(4, 5);  
    } catch (Exception e) { 
      //missing account id , it should have failed  
      System.AssertEquals(e.getMessage(), 'Failed to Construct the query'); 
    }
  }

  static testMethod void testGetContactWithActivities2() {
    Account account = getAccount();
    Contact testContact = getContact(account.Id);
    
    req = setParameterRequest(req, account.Id, '', '', '', '');
    System.Debug(logginglevel.warn, '>>>> the value of req is ' + req);
    
    RestContext.request = req;
    RestContext.response = res; 
    
    try {
      Contact contact = ATS_ActivityTimelineRestApi.getContactWithActivities();
      System.AssertEquals(testContact.Id , contact.Id);
    } catch (Exception e) { 
      System.Debug(logginglevel.warn, '>>>> the value of e is ' + e); 
      // missing account id, it should have failed  
      System.AssertEquals(4, 5);
    } 
  } 
  
  static RestRequest setParameterRequest(RestRequest restRequest, String accountId, String dataobjecttoken, String sortordertoken, String loadmoretoken, String queryFilterToken) {
    restRequest.addParameter('accountid', accountId);
    restRequest.addParameter('dataobjecttoken', dataobjecttoken);
    restRequest.addParameter('sortordertoken', sortordertoken);
    restRequest.addParameter('loadmoretoken', loadmoretoken);
    restRequest.addParameter('queryFilterToken', queryFilterToken);
    
    return restRequest; 
  }
  
  private static Account getAccount() {
    Account testAccount = new Account(Name='Test_ActivityTimelineRestApi');
    insert testAccount;
    
    stageTask(testAccount);
    stageEvent(testAccount); 
    
    return testAccount; 
  }
    
  private static Contact getContact(String accountId) {
      Contact testContact = new Contact(AccountId=accountId, LastName='Test_ActivityTimelineRestApi');
      insert testContact;
      
      return testContact;
  }
  
  private static void stageTask(Account testAccount) {
    String userId = UserInfo.getUserId();
    
    Task t1 = new Task();
    t1.OwnerId = userId;
    t1.Subject = 'Open Task';
    t1.Status = 'Open';
    t1.Priority = 'Normal';
    t1.WhatId = testAccount.Id;
    insert t1;
    
    Task t2 = new Task();
    t2.OwnerId = userId;
    t2.Subject = 'Closed Task';
    t2.Status = 'Completed';
    t2.Priority = 'Normal';
    t2.WhatId = testAccount.Id;
    insert t2;  
  }
  
  private static void stageEvent(Account testAccount) {
    Event newEvent = new Event();
    newEvent.OwnerId = UserInfo.getUserId(); 
    newEvent.Subject ='Open Event';
    newEvent.WhatId = testAccount.Id;
    newEvent.StartDateTime = datetime.newInstance(2030, 1, 20);
    newEvent.EndDateTime = datetime.newInstance(2030, 1, 30);
    insert newEvent;
    
    Event newEvent2 = new Event();
    newEvent2.OwnerId = UserInfo.getUserId(); 
    newEvent2.Subject ='Closed Event';
    newEvent2.WhatId = testAccount.Id;
    newEvent2.StartDateTime = datetime.newInstance(2016, 1, 30);
    newEvent2.EndDateTime = datetime.newInstance(2016, 1, 30);
    insert newEvent2;
  }
  
}