@isTest
public with sharing class Test_AddToSelfOpportunityTeamController  {
	//For single opportunity
    static testMethod void testSingleOppMethod(){
		Test.startTest();
        Account acnt=TestData.newAccount(1);
        insert acnt;        
	    Opportunity oppr=TestData.newOpportunity(acnt.Id,1);
        insert oppr;        
		String oppListStr;
        oppListStr='['+string.valueOf(oppr.Id)+']';
		
        AddToSelfOpportunityTeamController.oppTeamModel model1;
		model1=AddToSelfOpportunityTeamController.addSelfToOppTeam(oppListStr);		
        Test.stopTest();
  	}
    
    //For more than one opportunities
    static testMethod void testMultiOppMethod(){
		Test.startTest();
        Account acnt=TestData.newAccount(1);
        insert acnt;
		list<Opportunity> opprList=new List<Opportunity>();        
	    Opportunity oppr1=TestData.newOpportunity(acnt.Id,1);
		opprList.add(oppr1);
		Opportunity oppr2=TestData.newOpportunity(acnt.Id,1);
        opprList.add(oppr2);
        insert opprList;
        
		String oppListStr;
        oppListStr='['+string.valueOf(opprList[0])+','+string.valueOf(opprList[1])+']';
		
        AddToSelfOpportunityTeamController.oppTeamModel model1;
		model1=AddToSelfOpportunityTeamController.addSelfToOppTeam(oppListStr);		
        Test.stopTest();
  	}
    //For opportunity having Team member
    static testMethod void testOppWithOppTeamMember(){
		Test.startTest();
        Account acnt=TestData.newAccount(1);
        insert acnt;
		Opportunity oppr=TestData.newOpportunity(acnt.Id,1);
        insert oppr;
        OpportunityTeamMember oppTeamMem=new OpportunityTeamMember();
        oppTeamMem.OpportunityId=oppr.id;
        oppTeamMem.UserId=UserInfo.getUserId();
        insert oppTeamMem;
        
		String oppListStr;
        oppListStr='['+string.valueOf(oppr.Id)+']';		
		
        AddToSelfOpportunityTeamController.oppTeamModel model1;
		model1=AddToSelfOpportunityTeamController.addSelfToOppTeam(oppListStr);		
        Test.stopTest();
  	}
    //For blank Opportunity id
    static testMethod void testBlankOpp(){
		Test.startTest();        
		String oppListStr;
        oppListStr='';
        AddToSelfOpportunityTeamController.oppTeamModel model1;
		model1=AddToSelfOpportunityTeamController.addSelfToOppTeam(oppListStr);
        Test.stopTest();
  	}
}