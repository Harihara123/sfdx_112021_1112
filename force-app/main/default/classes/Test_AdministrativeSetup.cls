@isTest
public class Test_AdministrativeSetup {    
   
	 static testmethod void Test_getobjects(){          
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.CloseDate= System.Today();
        opp.StageName='Prospecting';
        insert opp;
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        List<SelectOption> allObjects = asetup.getobjects();
        Test.stopTest();       
    }
    static testmethod void Test_sortSelectOptionList(){          
        List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption('US','US'));
 	 	options.add(new SelectOption('CANADA','Canada'));
 	 	options.add(new SelectOption('MEXICO','Mexico'));
        String sortOrder = 'desc';        
       
        Test.startTest();
        List<SelectOption> sortedList = AdministrativeSetup.sortSelectOptionList(options,sortOrder);
        Test.stopTest();       
    }
    static testmethod void Test_getObjectsList(){      //have to increase code coverage check line 83         
        Account acnt = new Account();
        acnt.Name = 'Test Account';
        insert acnt;
        String query = 'select id from Account where Name = \'Test Account\'';
        //AdministrativeSetup.ResponseObjWrapper ror = new AdministrativeSetup.ResponseObjWrapper('A','B');
        
        Test.startTest();
        List<AdministrativeSetup.ResponseObjWrapper> gOList = AdministrativeSetup.getObjectsList(query);
        Test.stopTest();
    }
    
    static testmethod void Test_AllowEnableFieldSection()
    {
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.isFlexi = true;
        asetup.isMulti = true;
        asetup.EnabledFieldSection();
        
        AdministrativeSetup asetup1 = new AdministrativeSetup();
        asetup1.isFlexi = true;
        asetup1.isMulti = false;
        asetup1.EnabledFieldSection();
        
        Test.stopTest();
    }
    
    static testmethod void Test_NotAllowEnableFieldSection()
    {
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.isFlexi = false;
        asetup.isMulti = false;
        asetup.EnabledFieldSection();
        
    }
    
    static testmethod void Test_describeObject(){
        
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        insert opp;                      
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.TeamMemberRole = 'Recruiter';
        otm.OpportunityId = opp.Id;
        otm.UserId = userinfo.getUserId();
        insert otm;           
             
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Update__c = true;
        at.New__c = true;
        at.Delete__c = true;
        insert at;        
        AuditTrialFields__c af = new AuditTrialFields__c();
        af.Name = 'Team Role';
        af.DataType__c = 'PICKLIST';
        af.API_Name__c = 'TeamMemberRole';
        af.AuditTrial__c = at.id;
        insert af; 
        
        Test.startTest();
      
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.objLabel = 'OpportunityTeamMember';
        asetup.objName = 'OpportunityTeamMember';
        asetup.describeObject();
                        
        Test.stopTest();       
    }
    
    static testmethod void Test_describeObjectInvalid(){
        
        Test.startTest();   
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.objLabel = 'OpportunityTeamMember';
        asetup.objName = 'OpportunityTeamMember';
        asetup.describeObject();
        
        AdministrativeSetup asetup1 = new AdministrativeSetup();
        asetup1.objName = '';
        asetup1.describeObject();
                        
        Test.stopTest();       
    }
    
    static testmethod void Test_describeChildObjects(){
        
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.objLabel = 'Opportunity';
        asetup.objName = 'Opportunity';
        asetup.describeChildObjects();
        Test.stopTest();  
    }
    
      static testmethod void Test_describeChildFields(){
        
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.describeChildFields();
        Test.stopTest();
    }
    
     static testmethod void Test_SelectAllFlexible(){
        
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.SelectAllFlexible();
        Test.stopTest();
    }
    
    static testmethod void Test_HomePage(){
        
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        PageReference pref = asetup.HomePage();
        //System.assertEquals(new PageReference('/apex/AdministrativeSetupPage'), pref);
        Test.stopTest();
    }
    
    static testmethod void Test_SelectAllMultiCopy(){
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        AdministrativeSetup.fieldContainer fc = new AdministrativeSetup.fieldContainer();
        list<AdministrativeSetup.fieldContainer> PFields = new list<AdministrativeSetup.fieldContainer>();
        PFields.add(fc);
        asetup.ParentFields = PFields;
        asetup.CheckAllMultiCopy = true;
        asetup.SelectAllMultiCopy();
        Test.stopTest();
    }
    
    static testmethod void Test_SelectAllAudit(){
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        AdministrativeSetup.fieldContainer fc = new AdministrativeSetup.fieldContainer();
        list<AdministrativeSetup.fieldContainer> PFields = new list<AdministrativeSetup.fieldContainer>();
        PFields.add(fc);
        asetup.ParentFields = PFields;
        asetup.CheckAllAudit = true;
        asetup.SelectAllAudit();
        Test.stopTest();        
    }
    
    static testmethod void Test_SelectAllChildFields(){
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        AdministrativeSetup.fieldContainer fc = new AdministrativeSetup.fieldContainer();
        list<AdministrativeSetup.fieldContainer> PFields = new list<AdministrativeSetup.fieldContainer>();
        PFields.add(fc);
        asetup.ChildFields = PFields;
        asetup.CheckAllChilds = true;
        asetup.SelectAllChildFields();
        Test.stopTest();        
    }
    
    static testmethod void Test_SaveParentObject(){
        
              
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        asetup.isAudit = true;
        asetup.isCreate = true;
        asetup.isUpdate = true;
        asetup.isDelete = true;
        asetup.objLabel = 'OpportunityTeamMember';
        asetup.objName = 'OpportunityTeamMember';
        AdministrativeSetup.fieldContainer fc = new AdministrativeSetup.fieldContainer();
        fc.IsAudit = true;
        fc.Label = 'Team Role';
        fc.api = 'TeamMemberRole';
        fc.Dtype = 'PICKLIST';
        list<AdministrativeSetup.fieldContainer> PFields = new list<AdministrativeSetup.fieldContainer>();
        PFields.add(fc);
        asetup.ParentFields = PFields;
        asetup.SaveParentObject();
        
        AdministrativeSetup asetup1 = new AdministrativeSetup();
        asetup1.isAudit = false;
        asetup1.SaveParentObject();
        Test.stopTest();
    }
    
    static testmethod void Test_SaveChildObject(){
        
        Test.startTest();
        AdministrativeSetup asetup = new AdministrativeSetup();
        AdministrativeSetup.fieldContainer fc = new AdministrativeSetup.fieldContainer();
        fc.IsMulti = true;
        list<AdministrativeSetup.fieldContainer> PFields = new list<AdministrativeSetup.fieldContainer>();
        PFields.add(fc);
        asetup.ChildFields = PFields;
        asetup.SaveChildObject(); 
        
        AdministrativeSetup asetup1 = new AdministrativeSetup();
        AdministrativeSetup.fieldContainer fc1 = new AdministrativeSetup.fieldContainer();
        fc1.IsMulti = false;
        list<AdministrativeSetup.fieldContainer> PFields1 = new list<AdministrativeSetup.fieldContainer>();
        PFields1.add(fc1);
        asetup1.ChildFields = PFields1;
        asetup1.SaveChildObject(); 
        Test.stopTest();
    }
  
    }