/**
* Author: Akshay Konijeti
* Date: 12/20/2016
* UserStory: ATS-2754
* 
* Test Class for the Apex class ApigeeOauthConnectionManager.
* 
*/
@isTest
public class Test_ApigeeOauthConnectionManager implements HttpCalloutMock {
    public static string TestService='TestService';
    
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"expiresIn":"100"}');
        res.setStatusCode(200);
        return res;
    }
    
    public static testmethod void testmethod1(){
        // Test data
        
        ApigeeOAuthSettings__c settings = new ApigeeOAuthSettings__c(Name='Test Setting',Client_Id__c='testid',Client_Secret__c='testsecretid',OAuth_Token__c='testoauthtoken',Service_Header_1__c='test:sh',Service_Header_2__c='test:sh2',Service_Header_3__c='test:sh3',Service_Http_Method__c='testshm',Service_URL__c='www.test.com/test',Token_Expiration__c=null,Token_URL__c='www.test.com/test');
        insert settings;
        
        Test.setMock(HttpCalloutMock.class, new Test_ApigeeOauthConnectionManager()); 
        ApigeeOauthConnectionManager AOAuth = new ApigeeOauthConnectionManager ('Test Setting');
        HttpRequest req = AOAuth.getServiceRequest();
    } 
}