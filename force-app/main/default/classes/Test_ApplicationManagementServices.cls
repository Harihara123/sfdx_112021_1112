@istest
public class Test_ApplicationManagementServices  {
    // Test method for the update an existing order 
    @istest
    static void  testGetDispositionDetails(){
       
       User user = TestDataHelper.createUser('System Integration');
       System.runAs(user) {
            Test.startTest();

            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            String sourceSystmId = '541555334';
            Account talAcc = CreateTalentTestData.createTalentAccount();
            Contact talCont = CreateTalentTestData.createTalentContact(talAcc);
            Job_Posting__c jobPosting = Test_ATSOrderRESTService.createJobPostingObject(sourceSystmId);
            System.debug('jobPosting '+jobPosting);

            ATSSubmittal submittal = new ATSSubmittal();
            submittal.jobPostingId = sourceSystmId;
            submittal.talentId = talCont.Id;
            submittal.externalSourceId = 'Talent_Submittal_111';
            submittal.effectiveDate = '09/04/2018';
            submittal.ecid = '';
            submittal.icid = '';
            submittal.inviterFullName = 'test name';
            submittal.vendorSegmentId = 'ved1111';
            submittal.vendorApplicationId ='CS_Application_233';
            submittal.desiredSalary = '1222';
            submittal.desiredRate = '122';
            
            Map<String, String> results = ATSOrderRESTService.doPost(submittal);
            String stats = results.get('Status');
            String recordId = results.get('recordId');
            String appEventId = results.get('appEventId');

            req.requestURI = '/services/apexrest/Application/Disposition/V1';
            req.httpMethod = 'GET';
            req.params.put('eventId',appEventId);
            RestContext.request = req;
            RestContext.response= res;
            ApplicationManagementServices.getDisposition();


            System.debug('App Event results ');

            System.assert(results != null && stats != null);
            Test.stopTest();
       }
     }
}