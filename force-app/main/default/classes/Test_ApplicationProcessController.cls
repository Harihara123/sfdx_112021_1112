@istest
public class Test_ApplicationProcessController {
	public static final string fileType ='application/pdf';
	public static final string source ='Email';
	public static final string fileName ='abc.pdf';
	public static final string status ='New';
	public static final string TalentOwnership ='TEK';
   @TestSetup
	public static void testDataSetup() {
		JobPostingTestDataFactory.northAmericaTestDataSetup();
		JobPostingTestDataFactory.emeaChannelListTestData();
		JobPostingTestDataFactory.emeaTestDataSetup();
        
        
        ApigeeOAuthSettings__c settings = new ApigeeOAuthSettings__c(Name='Resume Parser',Client_Id__c='testid',Client_Secret__c='testsecretid',OAuth_Token__c='testoauthtoken',Service_Header_1__c='test:sh',Service_Header_2__c='test:sh2',Service_Header_3__c='test:sh3',Service_Http_Method__c='testshm',Service_URL__c='www.test.com/test',Token_Expiration__c=System.now().addHours(1),Token_URL__c='www.test.com/test');
        insert settings;
        
        Global_LOV__c glob = new Global_LOV__c(LOV_Name__c ='ATSResumeFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSResumeFiletypesAllowed.006');
        insert glob;
        
        Global_LOV__c glob1 = new Global_LOV__c(LOV_Name__c ='ATSGenericFiletypesAllowed', Text_Value__c ='pdf',Source_System_Id__c='ATSGenericFiletypesAllowed.006');
        insert glob1;
        
         Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Resume Parser',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
	}
    static testMethod void testOrderUpsertforAerotek(){
        
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
       	
       
        Account a = new Account();
        a.Name='Andrew1 Dean1';
        a.recordTypeId = RecordTypeIdAccount;
        a.Talent_Ownership__c=TalentOwnership;
        insert a;
       
        Contact con1 = new Contact();
        con1.firstName='Andrew1';
        con1.LastName='Dean1';
        con1.email = 'deana1@tesco.net';
        con1.otherphone = '01406 365261';
        con1.AccountId = a.Id;
        con1.externalSourceId__c = 'ex_SourceId__1';
        con1.Peoplesoft_ID__c = 'P1soft_ID';
        con1.RecordTypeId = RecordTypeIdContact;
        insert con1;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
        Test.setFixedSearchResults(fixedSearchResults);
       
        Job_Posting__c jp = [select id, Name, Req_Division__c from Job_Posting__c where Posting_Status__c = 'Active' limit 1];
       	jp.Req_Division__c='Aerotek';
        update jp;
        
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c=fileName;
        ApplStaging.RecruiterEmail__c='test_01recruiteremail__CV@allegisgroup.com';
        ApplStaging.Source__c = source;
        ApplStaging.File_Name__c = fileName;
        ApplStaging.PostingID__c=jp.Name;
        ApplStaging.Talent_Source__c='Aerotek';  
        ApplStaging.Content_Type__c = fileType;
        insert ApplStaging;
       
        Attachment attach = new Attachment(Name=fileName,parentId=ApplStaging.id);
        attach.body=Blob.valueOf('jhlkhgleknwlk');
        attach.ContentType = fileType;
        Database.insert(attach);
        ApplStaging.Status__c = status;
        ApplStaging.Attachment_ID__c = attach.Id;
       
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_InvocableAttachmentParser());
        upsert ApplStaging;
		Test.stopTest();
       
        system.assertEquals(1,1,'pass');
    }
    
    static testMethod void testOrderUpsertforTEKsystems(){
        
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
       	
       
        Account a = new Account();
        a.Name='Andrew2 Dean2';
        a.recordTypeId = RecordTypeIdAccount;
        a.Talent_Ownership__c=TalentOwnership;
        insert a;
       
        Contact con1 = new Contact();
        con1.firstName='Andrew2';
        con1.LastName='Dean2';
        con1.email = 'deana2@tesco.net';
        con1.otherphone = '02406 365261';
        con1.AccountId = a.Id;
        con1.externalSourceId__c = 'ex_SourceId__2';
        con1.Peoplesoft_ID__c = 'Psoft2_ID';
        con1.RecordTypeId = RecordTypeIdContact;
        insert con1;
        
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
             
        Test.setFixedSearchResults(fixedSearchResults);
        Job_Posting__c jp = [select id, Name, Req_Division__c from Job_Posting__c where Posting_Status__c = 'Active' limit 1];
       	jp.Req_Division__c='TEKsystems';
        update jp;
        
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c=fileName;
        ApplStaging.RecruiterEmail__c='test2_recruiteremail__CV@allegisgroup.com';
        ApplStaging.Source__c = source;
        ApplStaging.File_Name__c = fileName;
        ApplStaging.PostingID__c=jp.Name;
        ApplStaging.Talent_Source__c='TEKsystems';  
        ApplStaging.Content_Type__c = fileType;
        insert ApplStaging;
       
        Attachment attach = new Attachment(Name=fileName,parentId=ApplStaging.id);
        attach.body=Blob.valueOf('kjj;j;kj');
        attach.ContentType = fileType;
        Database.insert(attach);
        ApplStaging.Status__c = status;
        ApplStaging.Attachment_ID__c = attach.Id;
       
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_InvocableAttachmentParser());
		Test.stopTest();
        upsert ApplStaging;
       
       
        system.assertEquals(1,1,'pass');
    }
    
    static testMethod void testOrderUpsertforAstonCarter(){
        
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
       	
       
        Account a = new Account();
        a.Name='Andrew3 Dean3';
        a.recordTypeId = RecordTypeIdAccount;
        a.Talent_Ownership__c=TalentOwnership;
        insert a;
       
        Contact con1 = new Contact();
        con1.firstName='Andrew3';
        con1.LastName='Dean3';
        con1.email = 'deana3@tesco.net';
        con1.otherphone = '03406 365261';
        con1.AccountId = a.Id;
        con1.externalSourceId__c = 'ex_SourceId__3';
        con1.Peoplesoft_ID__c = 'Psoft3_ID';
        con1.RecordTypeId = RecordTypeIdContact;
        insert con1;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
             
        Test.setFixedSearchResults(fixedSearchResults);
        Job_Posting__c jp = [select id, Name, Req_Division__c from Job_Posting__c where Posting_Status__c = 'Active' limit 1];
       	jp.Req_Division__c='Aston Carter';
        update jp;
        
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c='FirstName LastName3';
        ApplStaging.RecruiterEmail__c='test3_recruiteremail__CV@allegisgroup.com';
        ApplStaging.Source__c = source;
        ApplStaging.File_Name__c = fileName;
        ApplStaging.PostingID__c=jp.Name;
        ApplStaging.Talent_Source__c='Aston Carter';  
        ApplStaging.Content_Type__c = fileType;
        insert ApplStaging;
       
        Attachment attach = new Attachment(Name=fileName,parentId=ApplStaging.id);
        attach.body=Blob.valueOf('jhkhlhl');
        attach.ContentType = fileType;
        Database.insert(attach);
        ApplStaging.Status__c = status;
        ApplStaging.Attachment_ID__c = attach.Id;
       
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_InvocableAttachmentParser());
        upsert ApplStaging;
		Test.stopTest();
       
       
        system.assertEquals(1,1,'pass');
    }
 
    static testMethod void testprocessApplicationExcepation(){
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c=fileName;
        ApplStaging.RecruiterEmail__c='test4_recruiteremail__CV@allegisgroup.com';
        ApplStaging.Source__c = source;
        ApplStaging.Status__c = status;
        insert ApplStaging;
         
        Attachment attachObject=new Attachment();
        attachObject.body = blob.valueOf('Firstname: abcdert LastName: poiuyjklpoi JobTitle:Java Developer Skill: Java Phone Number : 0987654321 Email Id : poilkj@gmail.com');
        attachObject.Name = fileName;
        attachObject.ContentType = fileType;
        attachObject.ParentId = ApplStaging.id;  
        insert attachObject;
       Test.startTest();
        ApplicationProcessController.processApplication(ApplStaging.id);
		Test.stopTest();
       system.assertEquals(1,1,'pass');
    }
    
    static testMethod void testOrderUpsertException(){
        
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
       	User user = TestDataHelper.createUser('System Integration');
       
        Account a = new Account();
        a.Name='Andrew4 Dean4';
        a.recordTypeId = RecordTypeIdAccount;
        a.Talent_Ownership__c=TalentOwnership;
        insert a;
       
        Contact con1 = new Contact();
        con1.firstName='Andrew4';
        con1.LastName='Dean4';
        con1.email = 'deana4@tesco.net';
        con1.otherphone = '04406 365261';
        con1.AccountId = a.Id;
        con1.externalSourceId__c = 'ex_SourceId__4';
        con1.Peoplesoft_ID__c = 'Psoft4_ID';
        con1.RecordTypeId = RecordTypeIdContact;
        insert con1;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(con1.Id);
             
        Test.setFixedSearchResults(fixedSearchResults);
       
	   Test.startTest(); 
       
		
        Application_Staging__c Applstaging = new Application_Staging__c();
        ApplStaging.File_Name__c='FirstName5 LastName5';
        ApplStaging.RecruiterEmail__c='test5_recruiteremail__CV@allegisgroup.com';
        ApplStaging.Source__c = source;
        ApplStaging.File_Name__c = fileName;
        ApplStaging.PostingID__c='JP-12345678';
        ApplStaging.Talent_Source__c='Aerotek';  
        ApplStaging.Content_Type__c = fileType;
        insert ApplStaging;
       
        Attachment attach = new Attachment(Name=fileName,parentId=ApplStaging.id);
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = fileType;
        Database.insert(attach);
        ApplStaging.Status__c =status;
        ApplStaging.Attachment_ID__c = attach.Id;
       
     
        Test.setMock(HttpCalloutMock.class, new Test_InvocableAttachmentParser());
        upsert ApplStaging;
       
       
	   Test.stopTest();
        system.assertEquals(1,1,'pass');
    }
   
}