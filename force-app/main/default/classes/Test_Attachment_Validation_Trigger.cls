@isTest(seealldata = false)
public class Test_Attachment_Validation_Trigger {
    static {
        CreateTalentTestData.createAcceptedFiletypeLOVs();
    }
    
    /*
     * Method tests that disallowed formats are not saved.
     */
    static testMethod void testBadFileType() {
        Account account = CreateTalentTestData.createTalentAccount();
        Test.startTest();

        Talent_Document__c badFormatPpt = CreateTalentTestData.createAttDocument(account, true, false, false, 10000, 'Somefile.ppt', 'Resume');
        Attachment[] atts = [SELECT Id from Attachment where ParentId = :badFormatPpt.Id];
        System.assert(atts.size() == 0, 'Test Failed - PPT Resume Inserted');
        
        Talent_Document__c badFormatPptx = CreateTalentTestData.createAttDocument(account, true, false, false, 10000, 'Somefile.pptx', 'Resume');
        atts = [SELECT Id from Attachment where ParentId = :badFormatPptx.Id];
        System.assert(atts.size() == 0, 'Test Failed - PPTX Resume Inserted');
        
        Talent_Document__c badFormatXls = CreateTalentTestData.createAttDocument(account, true, false, false, 10000, 'Somefile.xls', 'Resume');
        atts = [SELECT Id from Attachment where ParentId = :badFormatXls.Id];
        System.assert(atts.size() == 0, 'Test Failed - XLSX Resume Inserted');
        
        Talent_Document__c badFormatXlsx = CreateTalentTestData.createAttDocument(account, true, false, false, 10000, 'Somefile.xlsx', 'Resume');
        atts = [SELECT Id from Attachment where ParentId = :badFormatXlsx.Id];
        System.assert(atts.size() == 0, 'Test Failed - XLS Resume Inserted');

        Talent_Document__c badFormatPdf = CreateTalentTestData.createAttDocument(account, true, false, false, 10000, 'Somefile.pdf', 'Resume');
        atts = [SELECT Id from Attachment where ParentId = :badFormatPdf.Id];
        System.assert(atts.size() == 0, 'Test Failed - PDF Resume Inserted');
        
        Test.stopTest();
    }
    
    /*
     * Method asserts that file > max allowed size is not saved.
     */
    static testMethod void testFileTooBig() {
        Account account = CreateTalentTestData.createTalentAccount();
        Test.startTest();
        Talent_Document__c bigAttachment = CreateTalentTestData.createAttDocument(account, true, false, false, 5242881, 'Somefile.docx', 'Resume');
        Attachment[] atts = [SELECT Id from Attachment where ParentId = :bigAttachment.Id];
        System.assert(atts.size() == 0, 'Test Failed - Oversized file Inserted');
        Test.stopTest();
    }

    /*
     * Method asserts that max allowed file size is saved.
     */
    static testMethod void testBiggestGoodFile() {
        Account account = CreateTalentTestData.createTalentAccount();
        Test.startTest();
        Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 5242880, 'Somefile.docx', 'Resume');
        Attachment[] atts = [SELECT Id from Attachment where ParentId = :validAttachmentDocx.Id];
        System.assert(atts.size() == 1, 'Test Failed - Max allowed size file not inserted');
    }
    
    /*
     * Method asserts that all allowed formats are saved.
     * Only testing smaller size files because using multiple large files causes hrxml flow to fail,
     * due to exception (String greater than 6000000).
     */
    static testMethod void testGoodFile() {
        Account account = CreateTalentTestData.createTalentAccount();
        Test.startTest();
        Talent_Document__c validAttachmentDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Resume');
        Attachment[] atts = [SELECT Id from Attachment where ParentId = :validAttachmentDocx.Id];
        System.assert(atts.size() == 1, 'Test Failed - Resume docx not inserted');
        
        Talent_Document__c validAttachmentDoc = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.doc', 'Resume');
        atts = [SELECT Id from Attachment where ParentId = :validAttachmentDoc.Id];
        System.assert(atts.size() == 1, 'Test Failed - Resume doc not inserted');

        Talent_Document__c validAttachmentOthDocx = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.docx', 'Other');
        atts = [SELECT Id from Attachment where ParentId = :validAttachmentOthDocx.Id];
        System.assert(atts.size() == 1, 'Test Failed - Other docx not inserted');
        
        Talent_Document__c validAttachmentOthDoc = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.doc', 'Other');
        atts = [SELECT Id from Attachment where ParentId = :validAttachmentOthDoc.Id];
        System.assert(atts.size() == 1, 'Test Failed - Other doc not inserted');

        Talent_Document__c validAttachmentOthPdf = CreateTalentTestData.createAttDocument(account, true, false, false, 1000, 'Somefile.pdf', 'Other');
        atts = [SELECT Id from Attachment where ParentId = :validAttachmentOthPdf.Id];
        System.assert(atts.size() == 1, 'Test Failed - Other pdf not inserted');
        Test.stopTest();
    }

    /*
     * Method creates and deletes an attachment to exercise the on delete section of Attachment_Trigger
     * As on 04/21/2016, this is not a real use case.
     */
    static testMethod void testAttachmentDelete() {
        Account account = CreateTalentTestData.createTalentAccount();
        Test.startTest();
        Talent_Document__c doc = CreateTalentTestData.createAttDocument(account, true, false, false, 100000, 'Somefile.docx', 'Resume');
        Attachment[] atts = [SELECT Id from Attachment where ParentId = :doc.Id];
        System.assert(atts.size() == 1, 'Test Failed - Test file not inserted');
        Id attId = atts[0].Id;
        
        delete atts[0];
        
        Attachment_Extension__c[] attExtn = [SELECT Id, Event__c from Attachment_Extension__c where Record_Id__c = :attId];
        System.assert(attExtn.size() == 1, 'Test Failed - Attachment_Extension__c not inserted');
        
        System.assert(attExtn[0].Event__c == 'Delete', 'Test Failed - Event not set to Delete');
    }
    
}