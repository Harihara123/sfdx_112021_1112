@isTest
public class Test_AuditTrialProcess {
    static testmethod void Test_isRecordForInsert(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.New__c = true;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForInsertVar = AuditTrialProcess.isRecordForInsert(objectName);
        Test.stopTest();       
    }
     static testmethod void Test_isRecordNotForInsert(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.New__c = false;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForInsertVar = AuditTrialProcess.isRecordForInsert(objectName);
        Test.stopTest();       
    }
        
    static testmethod void Test_isRecordForUpdate(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Update__c = true;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForUpdateVar = AuditTrialProcess.isRecordForUpdate(objectName);
        Test.stopTest();        
    }
     static testmethod void Test_isRecordNotForUpdate(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Update__c = false;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForUpdateVar = AuditTrialProcess.isRecordForUpdate(objectName);
        Test.stopTest();        
    }
    
    static testmethod void Test_isRecordForDelete(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Delete__c = true;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForDeleteVar = AuditTrialProcess.isRecordForDelete(objectName);
        Test.stopTest();        
    }
    static testmethod void Test_isRecordNotForDelete(){
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Delete__c = false;
        insert at;
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForDeleteVar = AuditTrialProcess.isRecordForDelete(objectName);
        Test.stopTest();        
    }
    
     static testmethod void Test_isEmpty(){
        string objectName = 'OpportunityTeamMember';
        Test.startTest();
        Boolean isRecordForInsertVar = AuditTrialProcess.isRecordForInsert(objectName);
        Boolean isRecordForUpdateVar = AuditTrialProcess.isRecordForUpdate(objectName);
        Boolean isRecordForDeleteVar = AuditTrialProcess.isRecordForDelete(objectName);
        Test.stopTest();       
    }
    
    static testmethod void Test_OnInsert(){
        
        string Objname = 'OpportunityTeamMember'; 
        string objlabel = 'OpportunityTeamMember';
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.New__c = true;
        insert at;
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        insert opp;            
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.TeamMemberRole = 'Recruiter';
        otm.OpportunityId = opp.Id;
        otm.UserId = userinfo.getUserId();
        insert otm;  
                 
    } 
       
    static testmethod void Test_OnUpdatePicklistField(){
        string Objname = 'OpportunityTeamMember'; 
        string objlabel = 'OpportunityTeamMember';
               
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Update__c = true;
        insert at;
        
        AuditTrialFields__c af1 = new AuditTrialFields__c();
        af1.Name = 'Team Role';
        af1.DataType__c = 'PICKLIST';
        af1.API_Name__c = 'TeamMemberRole';
        af1.AuditTrial__c = at.id;
        insert af1;
                      
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        system.debug('account id:-'+acnt.id);
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        insert opp;
        system.debug('opportunity id:'+opp.id);
        
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.TeamMemberRole = 'Recruiter';
        otm.OpportunityId = opp.Id;
        otm.UserId = userinfo.getUserId(); 
        insert otm;     
        
        OpportunityTeamMember utm = [Select ID,Name,TeamMemberRole from OpportunityTeamMember where TeamMemberRole = 'Recruiter' limit 1];
            utm.TeamMemberRole = 'Allocated Recruiter';
            update utm;  
        
    } 
        
    static testmethod void Test_OnUpdateBooleanField(){
        string Objname = 'OpportunityTeamMember'; 
        string objlabel = 'OpportunityTeamMember';
               
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Update__c = true;
        insert at;
        
        AuditTrialFields__c af1 = new AuditTrialFields__c();
        af1.Name = 'is DRZ Update';
        af1.DataType__c = 'BOOLEAN';
        af1.API_Name__c = 'isDRZUpdate__c';
        af1.AuditTrial__c = at.id;
        insert af1;
                      
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        insert opp;
               
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.isDRZUpdate__c = false;
        otm.OpportunityId = opp.Id;
        otm.UserId = userinfo.getUserId(); 
        insert otm;     
        
        OpportunityTeamMember utm = [Select ID,Name,isDRZUpdate__c from OpportunityTeamMember where isDRZUpdate__c = false limit 1];
            utm.isDRZUpdate__c = true;
            update utm;  
        
    }
    
        
   
    static testmethod void Test_OnDelete(){
        
        string Objname = 'OpportunityTeamMember'; 
        string objlabel = 'OpportunityTeamMember';
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'OpportunityTeamMember';
        at.Delete__c = true;
        insert at;
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        system.debug('account id:-'+acnt.id);
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        insert opp;
        system.debug('opportunity id:'+opp.id);
        
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.TeamMemberRole = 'Recruiter';
        otm.OpportunityId = opp.Id;
        otm.UserId = userinfo.getUserId();
        insert otm;    
        
        OpportunityTeamMember otd = [select id from OpportunityTeamMember where TeamMemberRole = 'Recruiter' limit 1];
        delete otd;
                       
    }
    
    
    static testmethod void Test_OnUpdateForSFDRZTextArea()
    {
        DRZSettings__c ds = new DRZSettings__c();
        ds.Name = 'DRZFieldUpdates';
        ds.Value__c = 'DRZ_Req_Due_Date__c,DRZ_Submittal_Start_Date__c,Is_Req_Exclusive__c,Req_Job_Title__c,ClientAccount__c,Start_Date_Opportunity__c,Req_Interview_Date__c';
        insert ds;
        
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'DRZ_Board_Cards__c';
        at.Update__c = true;
        insert at;
        
            
        AuditTrialFields__c af2 = new AuditTrialFields__c();
        af2.Name = 'Description';
        af2.DataType__c = 'TEXTAREA';
        af2.API_Name__c = 'Req_Job_Description__c';
        af2.AuditTrial__c = at.id;
        insert af2;
                      
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp = new Opportunity();
         opp.Name = 'TESTOPP';
            opp.CloseDate = system.today();
            opp.StageName = 'test';
            opp.accountId= acnt.Id;
            opp.Req_Job_Description__c = 'This is a test Job Description 12345';
            opp.Req_Job_Title__c = 'Sr Java Developer   ';
            opp.Location__c = 'Hanover, MD';
            opp.isDRZUpdate__c = true;
            opp.CreatedDate=system.today();
        insert opp;
        
       DRZ_Board_Cards__c dbc = new DRZ_Board_Cards__c();
        dbc.Req_Job_Description__c = 'This is a test Job Description 12345';
        dbc.Req_Division__c	= 'Communications';
        dbc.CardId__c = opp.Id;
        dbc.CardName__c = opp.Name;
        dbc.LaneName__c = 'TEST7';
        dbc.CreatedDate=system.today();
        insert dbc;
      
        
        Opportunity ot = [Select ID,Name,Req_Job_Description__c from Opportunity where StageName = 'test'];
            ot.isDRZUpdate__c = true;
            ot.Req_Job_Description__c = 'This is a test Job Description 12345678';
            update ot;       
                
        DRZ_Board_Cards__c dbu = [Select Id,LaneName__c from DRZ_Board_Cards__c where LaneName__c = 'TEST7'];
        dbu.Req_Job_Description__c = 'This is a test Job Description 12345678';
        update dbu;     
        
       }
    
    static testmethod void Test_OnUpdateForSFDRZDate()
    {
        DRZSettings__c ds = new DRZSettings__c();
        ds.Name = 'DRZFieldUpdates';
        ds.Value__c = 'DRZ_Req_Due_Date__c,DRZ_Submittal_Start_Date__c,Is_Req_Exclusive__c,Req_Job_Title__c,ClientAccount__c,Start_Date_Opportunity__c,Req_Interview_Date__c';
        insert ds;
        
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'DRZ_Board_Cards__c';
        at.Update__c = true;
        insert at;
        
        AuditTrialFields__c af1 = new AuditTrialFields__c();
        af1.Name = 'Interview Date';
        af1.DataType__c = 'DATE';
        af1.API_Name__c = 'Req_Interview_Date__c';
        af1.AuditTrial__c = at.id;
        insert af1;
                             
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp = new Opportunity();
         opp.Name = 'TESTOPP';
            opp.CloseDate = system.today();
            opp.StageName = 'test';
            opp.accountId= acnt.Id;
            opp.Req_Job_Description__c = 'This is a test Job Description 12345';
            opp.Req_Job_Title__c = 'Sr Java Developer   ';
            opp.Location__c = 'Hanover, MD';
            opp.isDRZUpdate__c = true;
            opp.Req_Interview_Date__c = System.today();
            opp.CreatedDate=system.today();
        insert opp;
        
       DRZ_Board_Cards__c dbc = new DRZ_Board_Cards__c();
        dbc.Req_Job_Description__c = 'This is a test Job Description 12345';
        dbc.Req_Division__c	= 'Communications';
        dbc.CardId__c = opp.Id;
        dbc.CardName__c = opp.Name;
        dbc.LaneName__c = 'TEST7';
        dbc.Req_Interview_Date__c= System.today();
        dbc.CreatedDate=system.today();
        insert dbc;
              
        Opportunity ot = [Select ID,Name,Req_Job_Description__c from Opportunity where StageName = 'test'];
            ot.isDRZUpdate__c = true;
            ot.Req_Interview_Date__c= System.today() + 10;
            update ot;       
                
        DRZ_Board_Cards__c dbu = [Select Id,LaneName__c from DRZ_Board_Cards__c where LaneName__c = 'TEST7'];
        dbu.Req_Interview_Date__c = System.today() + 10;
        update dbu;
                
       }
    
    static testmethod void Test_OnUpdateForSFDRZBoolean()
    {
        DRZSettings__c ds = new DRZSettings__c();
        ds.Name = 'DRZFieldUpdates';
        ds.Value__c = 'DRZ_Req_Due_Date__c,DRZ_Submittal_Start_Date__c,Is_Req_Exclusive__c,Req_Job_Title__c,ClientAccount__c,Start_Date_Opportunity__c,Req_Interview_Date__c';
        insert ds;
        
        AuditTrial__c at = new AuditTrial__c();
        at.Name = 'DRZ_Board_Cards__c';
        at.Update__c = true;
        insert at;
        
        AuditTrialFields__c af1 = new AuditTrialFields__c();
        af1.Name = 'Exclusive';
        af1.DataType__c = 'BOOLEAN';
        af1.API_Name__c = 'Is_Req_Exclusive__c';
        af1.AuditTrial__c = at.id;
        insert af1;
                             
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp = new Opportunity();
         opp.Name = 'TESTOPP';
            opp.CloseDate = system.today();
            opp.StageName = 'test';
            opp.accountId= acnt.Id;
            opp.Req_Job_Description__c = 'This is a test Job Description 12345';
            opp.Req_Job_Title__c = 'Sr Java Developer   ';
            opp.Location__c = 'Hanover, MD';
            opp.isDRZUpdate__c = true;
            opp.isExclusiveJob__c = false;
            opp.CreatedDate=system.today();
        insert opp;
        
       DRZ_Board_Cards__c dbc = new DRZ_Board_Cards__c();
        dbc.Req_Job_Description__c = 'This is a test Job Description 12345';
        dbc.Req_Division__c	= 'Communications';
        dbc.CardId__c = opp.Id;
        dbc.CardName__c = opp.Name;
        dbc.LaneName__c = 'TEST7';
        dbc.Is_Req_Exclusive__c = false;
        dbc.CreatedDate=system.today();
        insert dbc;
              
        Opportunity ot = [Select ID,Name,Req_Job_Description__c from Opportunity where StageName = 'test'];
            ot.isDRZUpdate__c = true;
            ot.isExclusiveJob__c = true;
            update ot;       
                
        DRZ_Board_Cards__c dbu = [Select Id,LaneName__c from DRZ_Board_Cards__c where LaneName__c = 'TEST7'];
        dbu.Is_Req_Exclusive__c = true;
        update dbu;
                
       }
               
        static testmethod void Test_getName(){
        Account a = new Account(Name='TestOpportunity', 
                                BillingCity='San Francisco');
        insert a;
        Test.startTest();
        String name = AuditTrialProcess.getName(a.id);
        Test.stopTest();  
         System.assertEquals('TestOpportunity', name);
    }
    
     static testmethod void Test_getObjectNameByRecordId(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.CloseDate= System.Today();
        opp.StageName='Prospecting';
        insert opp;
        Test.startTest();
        String objectName = AuditTrialProcess.getObjectNameByRecordId(opp.id);
        Test.stopTest();  
        System.assertEquals('Opportunity', objectName);
    }
}