/***************************************************************************************************************************************
* Name        - Test_BatchRWSCandidateMatch
* Description - This test class covers the Class: Batch_RWSCandidateMatch
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       3/16/2013             Created
*****************************************************************************************************************************************/



@isTest
Private class Test_BatchRWSCandidateMatch{
     
     public static testMethod void testRWSCandidateMatchSchedule() 
     {                
        Contact ct1 = new Contact(FirstName = 'Test', LastName = 'Contact1', RWS_Candidate_URL__c = '', Phone = '9999999999');
        insert ct1;
        Contact ct2= new Contact(FirstName = 'Test', LastName = 'Contact2', RWS_Candidate_URL__c = '', Email = 'FromTestClass@FromTestClass.com');
        insert ct2;        
        RWSCandidateMatchJobLastRunTime__c dtRCM = new RWSCandidateMatchJobLastRunTime__c();       
        dtRCM.LastJobRunTime__c = system.now()-200;
        dtRCM.Name = 'RWSCandidateMatchJob';
        insert dtRCM;
        RWSCandidateMatchScheduledProcess sh = new RWSCandidateMatchScheduledProcess();
        String sch = '0 0 23 * * ?';
        Test.StartTest();
        system.schedule('execute', sch, sh);
        Test.StopTest();      
     }   
}