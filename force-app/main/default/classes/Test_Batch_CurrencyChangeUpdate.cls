@isTest
public class Test_Batch_CurrencyChangeUpdate
{
     static testMethod void TestOpportunityTriggersSetRevenueField() 
    {        
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        User user = TestDataHelper.createUser('System Administrator');
        List<Opportunity> lstOpportunity = new List<Opportunity>();        
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) 
        {
            system.runAs(user) 
            {    
                
                TestData TdAcc = new TestData(1);
                testdata tdcont = new Testdata();
                // Get the TEK recordtype Id from a name        
                string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                
                CurrencyISOCodeMappings__c cs = new CurrencyISOCodeMappings__c();
                cs.name = 'EUR – Euro';
                cs.Actual_ISO_Code__c = 'EUR';
                insert cs;
                
                CurrencyISOCodeMappings__c cs1 = new CurrencyISOCodeMappings__c();
                cs1.name = 'EUR - Euro';
                cs1.Actual_ISO_Code__c = 'EUR';
                insert cs1;
                
                CurrencyType__c ct = new CurrencyType__c();
                ct.IsoCode__c = 'EUR';
                ct.ConversionRate__c = 1;
                ct.DecimalPlaces__c = .5;
                insert ct;
                
                for(Account Acc : TdAcc.createAccounts()) 
                {                     
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Currency__c = 'EUR – Euro',Req_Rate_Frequency__c = 'Hourly');  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }                
                // Insert list of Opportunity
                insert lstOpportunity; 
                
                Batch_CurrencyChangeUpdate obj = new Batch_CurrencyChangeUpdate();
                obj.query =  'Select Id,RecordTypeId,Currency__c,Amount,Req_Total_Spread__c,Retainer_Fee__c,Req_Salary_Min__c,Req_Salary_Max__c,Req_Flat_Fee__c,Req_Additional_1st_Year_Compensation__c,Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Req_Pay_Rate_Min__c,Req_Pay_Rate_Max__c,Total_Compensation_Max__c,Total_Compensation_Min__c,Amount_Multi_Currency__c,Total_Spread_Multi_Currency__c,Project_Retainer_Fee_Multi_Currency__c,Salary_Min_Multi_Currency__c,Salary_Max_Multi_Currency__c,Flat_Fee_Multi_Currency__c,Additional_1st_Year_Comp_Converted__c,Bill_Rate_Min_Multi_Currency__c,Bill_Rate_Max_Multi_Currency__c,Pay_Rate_Min_Multi_Currency__c,Pay_Rate_Max_Multi_Currency__c,Total_Compensation_Max_Converted__c,Total_Compensation_Min_Converted__c from Opportunity where stagename in: setStageName  and Currency__c != \'USD – U.S Dollar\'';
                database.executeBatch(obj);
            }
        }
    }
}