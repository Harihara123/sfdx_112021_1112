@isTest(seeAlldata=false)
private class Test_Batch_Deactivate_EngagementRule {

    static testMethod void testscheduleBatch_EmailNotificationtoOpp() { 
    
     Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u = new User(Alias = 'admin', Email='test@allegisgroup.com',
      EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = s.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='test@allegisgroup.com.devtest');
      insert u;
      
     system.runAs(u) {
        
                       ApexCommonSettings__c a = new ApexCommonSettings__c();
                          a.SoqlQuery__c  = 'SELECT Account__c,Expiration_Date__c, Active__c, Id, RecordType.DeveloperName ,Start_Date__c FROM Engagement_Rule__c where Expiration_Date__c < TODAY and Active__c = TRUE';
                          a.Name = 'Batch_Deactivate_EngagementRule';
                          insert a;
                          
                          TestData TdAcc = new TestData(1);
                          List<Account> lstNewAccounts = TdAcc.createAccounts();
                          
                           List<Engagement_Rule__c> ER = new List<Engagement_Rule__c >();
                             for(integer i = 0;i < 15;i++)
                             {
                                Engagement_Rule__c EngRul= new Engagement_Rule__c (Account__c = lstNewAccounts[0].id, Active__c = TRUE, Expiration_Date__c = date.parse('11/10/2016'), Start_Date__c= date.parse('10/5/2016'));
                                ER.add(EngRul);
                             }
                             
                             Date tomorrow = date.today().addDays(1);
                             Date dayBefore = date.today().addDays(-2);
                             for(Integer j = 0; j < 5;j++)
                             {
                                Engagement_Rule__c EngRul= new Engagement_Rule__c (Account__c = lstNewAccounts[0].id, Active__c = TRUE, Expiration_Date__c = tomorrow, Start_Date__c = date.today());
                                ER.add(EngRul);
                             }
                             insert ER;
                             
                             test.startTest();
                             Batch_Deactivate_EngagementRule objbatch= new Batch_Deactivate_EngagementRule();
                             Database.BatchableContext BC;
                             List<SObject> scopeList = new List<SObject>();
                             
                             String soqlQuery = 'SELECT Account__c,Expiration_Date__c, Active__c, Id, RecordType.DeveloperName ,Start_Date__c FROM Engagement_Rule__c  WHERE  Active__c = TRUE LIMIT 100';
                                     // Expiration_Date__c < TODAY and       
                                         
                             objbatch.QueryObject = soqlQuery;
                             Database.QueryLocator QL= objbatch.start(BC);
                             Database.QueryLocatorIterator QIT =  QL.iterator();
                              while (QIT.hasNext())
                               {
                                  scopeList.add(QIT.next());
                               }   
                             List<Engagement_Rule__c> engRules = new List<Engagement_Rule__c>();
                             for(SObject o: scopeList)
                             {
                               engRules.add((Engagement_Rule__c)o);
                             }
                             //database.executebatch(objbatch,200);
                             
                             objbatch.execute(BC,engRules);
                             objbatch.finish(BC); 
                             Schedule_Batch_Deactivate_EngagementRule obj = new Schedule_Batch_Deactivate_EngagementRule();   
                             String chron = '0 0 23 * * ?';        
                             system.schedule('Test Sched', chron, obj);         
                             test.stopTest();
        
        
        }
     
    }
}