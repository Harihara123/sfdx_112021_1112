//***************************************************************************************************************************************/
//* Name        - Test_Batch_DeleteSkillAndJobTitle
//* Description - Test class for batch Utility class to delete global LOV records
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         01/22/2018                Created
//*****************************************************************************************************************************************/
@isTest
public class Test_Batch_DeleteSkillAndJobTitle
{
    static testMethod void testBatch_DeleteSkillAndJobTitle() 
    {
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
            Test.startTest();
             
            Global_LOV__c obj = new Global_LOV__c();
            obj.LOV_Name__c = 'MatchSkillList';
            obj.Source_System_Id__c = 'TestSourceSystem';
            insert obj;
            
            Batch_DeleteSkillAndJobTitle objBatch_DeleteSkillAndJobTitle = new Batch_DeleteSkillAndJobTitle();
            objBatch_DeleteSkillAndJobTitle.QueryReq = 'select id from Global_LOV__c where LOV_Name__c in :lovNames';
            Database.executeBatch(objBatch_DeleteSkillAndJobTitle);
            
            Test.stopTest();
        }                                    
    }
    
                            
}