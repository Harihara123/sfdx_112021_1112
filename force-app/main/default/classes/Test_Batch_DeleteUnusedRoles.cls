//***************************************************************************************************************************************/
//* Name        - Test_Batch_DeleteUnusedRoles
//* Description - Test class for batch Utility class Batch_DeleteUnusedRoles.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         05/18/2017                Created
//*****************************************************************************************************************************************/
@isTest
public class Test_Batch_DeleteUnusedRoles
{
    static testMethod void testBatchDeleteUnusedRoles() 
    {        
             Test.startTest();
             
             UserRole pr = new UserRole(DeveloperName='MyParentCustomRole',Name='MyParentRole');
             insert pr;
             
             UserRole r = new UserRole(DeveloperName='MyCustomRole',Name='MyRole',ParentRoleId=pr.id);
             insert r;
            
            Batch_DeleteUnusedRoles obj = new Batch_DeleteUnusedRoles();
            obj.query =  'Select Id,userroleid from User where Isactive=false and UserType = \'Standard\' limit 25';
            Database.executeBatch(obj,200);
            Test.stopTest();              
    }  
    
    static testMethod void testBatchDeleteUnusedRoles1() 
    {        
             Test.startTest();
            
            Batch_DeleteUnusedRoles obj = new Batch_DeleteUnusedRoles();
            obj.query =  'Select Id,userroleid from User where Isactive=false and UserType = \'Standard\' limit 25';
            Database.executeBatch(obj,200);
            Test.stopTest();              
    }   
    
                                   
}