@isTest(seealldata = false)
private class Test_Batch_EDUserLicense {
    
    static testmethod void test_EDUser() {       
        
        List<User> userList = new List<user>();
        List<GroupMember> groupList = new List<GroupMember>();
        List<PermissionSetAssignment> permList = new List<PermissionSetAssignment>();
        Integer count=0;
        Id grpId;
        Id permId;
        String Description;
        List<String> errorList = new List<String>();
        
        //Insert group
        Group grp = new Group(name='Test_ED_Group');
        Insert grp;
        grpId=grp.Id;
        
        //Insert PermissionSet
        PermissionSet perm = new PermissionSet(name='Test_ED_PSet',label='Test ED PSet');
        Insert perm;
        permId=perm.Id;
             
        
        // insert 30 users
        for (Integer i=0;i<29;i++) {
            userList.add(new user(LastName='Test',
                                  FirstName='User'+i,
                                  email='testUser'+i+'@allegisgroup.com',
                                  username='testUser'+i+'@allegisgroup.com.demo',
                                  userRoleId='00EU0000000DpNR',
                                  profileId='00e24000001AcBh',
                                  alias='tuser'+i,
                                  communitynickname='TestUser'+i,
                                  TimeZoneSidKey='America/New_York',
                                  LocaleSidKey='en_US',
                                  EmailEncodingKey='UTF-8',
                                  LanguageLocaleKey='en_US'));
        }
        
        insert userList;
        
        
        
        // find the user just inserted. add Group and Permission set to  the selected users
        for (User u : userList) {
            
            count=count+1;
            //Assigning Public Group to 10 users
            if(count<21) {
                
               groupList.add(new groupMember(UserOrGroupId=u.Id,
                                             groupid=grpId));
            }
            else {
            
               permList.add(new PermissionSetAssignment(AssigneeId=u.Id,
                                                        PermissionSetId=permId));
            }
        }
        Database.SaveResult[] srList = Database.insert(groupList, false);
                // Iterate through each returned result
             for (Database.SaveResult sr : srList) {
                 if (!sr.isSuccess()){
                
                // Operation failed, so get all errors                
                   for(Database.Error err : sr.getErrors()) {  
                      errorList.add(err.getMessage());
                      Description = Description+err.getMessage();
                    }
                
              }
            
        }
        
        //insert groupList;
        insert permList;
               
        Test.startTest();
        String query='SELECT UserOrGroupId FROM GroupMember WHERE GroupId=:grpId limit 20';
        Batch_EDUserLicense bed = new Batch_EDUserLicense(query,grpId,permId);
        Id batchId = Database.executeBatch(bed);       
        Test.stopTest();
        // after the testing stops, assert records were updated properly
        System.assertEquals(groupList.size(), [select count() from PermissionSetAssignment where PermissionSetId=:permId]);
    }

}