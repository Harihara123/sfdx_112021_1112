@isTest
private class Test_Batch_EmailNotificationtoOppOwner {

    static testMethod void testscheduleBatch_EmailNotificationtoOpp() { 
    
     Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u = new User(Alias = 'admin', Email='pyadav@allegisgroup.com',
      EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = s.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='pyadav@allegisgroup.com.devtest');
      insert u;
   
    List<opportunity> opplist = new List<opportunity>();
    RecordType rt = [select id,Name from RecordType where SobjectType='Opportunity' and Name='TEKsystems Global Services' Limit 1];       
         test.starttest();
         opportunity o = new opportunity(Name='TestEmailNotification',CloseDate=system.TODAY()-5,StageName='Interest',RecordType=rt,Impacted_Regions__c='TEK MidAtlantic',ownerId=u.ID, Opco__c = 'TEKsystems, Inc.');
         insert o;
         o.ownerID=u.ID;
         opplist.add(o);
         Batch_EmailNotificationtoOppOwner objopp= new Batch_EmailNotificationtoOppOwner();
         objopp.start(null);
         objopp.execute(null,opplist);
         objopp.finish(null); 
         scheduleBatch_EmailNotificationtoOpp obj = new scheduleBatch_EmailNotificationtoOpp ();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Test Sched', chron, obj);
         Batch_EmailNotiOppOwnerwithintwodays obj1=new Batch_EmailNotiOppOwnerwithintwodays();
         obj1.start(null);
         obj1.execute(null,opplist);
         obj1.finish(null); 
         scheduleBatch_EmailNotitOppwithintwodays obj2 = new scheduleBatch_EmailNotitOppwithintwodays ();   
         String chron1 = '0 0 23 * * ?';        
         system.schedule('Test Sched2', chron1, obj2);
         test.stopTest();
    }
}