@isTest(seeAlldata=false)
private class Test_Batch_HRXML_CanLists {

    @TestSetup
    static void makeData() {
        Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'admin', Email='test@allegisgroup.com',
                          EmailEncodingKey='UTF-8', LastName='Test123', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = s.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='test@allegisgroup.com.devtest');
        insert u;

        system.runAs(u) {
            HRXML_Candidate_Lists_Update__c cs = new HRXML_Candidate_Lists_Update__c();
            cs.Name = 'HRXMLBatchLastRun';
            cs.Last_Executed_Date_Time__c = System.now()-5;
            insert cs;

            TestData td = new TestData(10);
            List<Account> accList = td.createAccounts();
            
            List<Contact> conList = new List<Contact>();
            for(integer i=0;i<10;i++){
                Contact c = new Contact(AccountID = accList[i].id, LastName = 'Contact'+i);
                conList.add(c);
            }
            insert conList;

            List<Tag_Definition__c> tagList = new List<Tag_Definition__c>();
            for(integer i=0;i<10;i++){
                Tag_Definition__c tdef = new Tag_Definition__c(Tag_Name__c = 'TD'+i);
                tagList.add(tdef);
            }
            insert tagList;
            
            List<Contact_Tag__c> ctList = new List<Contact_Tag__c>();
            for(integer i=0;i<10;i++){
                Contact_Tag__c ct = new Contact_Tag__c(Contact__c = conList[i].id, Tag_Definition__c = tagList[i].id);
                ctList.add(ct);
            }
            insert ctList;
        }
    }
    
    static testMethod void testBatchHRXMLCanLists() {
        User u = [SELECT Id from User where UserName='test@allegisgroup.com.devtest'];

        system.runAs(u) {
            List<Contact> conList = [SELECT Id, Search_Ingestion_Trigger_Timestamp__c from Contact];
            for (Contact c: conList) {
                System.assertEquals(null, c.Search_Ingestion_Trigger_Timestamp__c, 'Search_Ingestion_Trigger_Timestamp__c is not null at start!');
            }
            List<Id> conIds = new List<Id>();
            for (Contact c: conList) {
                conIds.add(c.Id);
            }
            
            Test.startTest();
            
			Batch_HRXML_CanLists batch = new Batch_HRXML_CanLists();
            batch.QueryObject = 'Select Id, Contact__r.Name, Contact__r.AccountId, LastModifiedDate from Contact_Tag__c';
            Id batchId = Database.executeBatch(batch);
            Test.stopTest();

            List<Contact> clist = [SELECT Id, Search_Ingestion_Trigger_Timestamp__c from Contact where Id in :conIds];
            
            for (Contact c: clist) {
                System.assertNotEquals(null, c.Search_Ingestion_Trigger_Timestamp__c, 'Search_Ingestion_Trigger_Timestamp__c is null at end!');
            }

            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CompletedDate
                                          FROM AsyncApexJob WHERE Id = :batchId];

            System.assertEquals('Completed', a.Status, 'Batch job status is not Completed');
            System.assertEquals(0, a.NumberOfErrors, 'Batch job NumberOfErrors is not 0');
            System.assertEquals(1, a.JobItemsProcessed, 'Batch job JobItemsProcessed is not 1');
            System.assertEquals(1, a.TotalJobItems, 'Batch job TotalJobItems is not 1');
        }
    }

    static testMethod void testBatchHRXMLCanListsScheduler() {
        String cronExp = '0 0 0 15 3 ? *';

        String jobId = System.schedule('ScheduleApexClassTest', cronExp, new Schedule_Batch_HRXML_CanLists());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cronExp, ct.CronExpression, 'CRON setup doesn not match');
        System.assertEquals(0, ct.TimesTriggered, 'CRON TimesTriggered does not match');
    }

    static testMethod void testBatchHRXMLCanListsSchedulerSetup() {
        Test.startTest();
        Schedule_Batch_HRXML_CanLists.setup();
        Test.stopTest();

        System.assertEquals(1, 1, 'Fake assert');
    }
}