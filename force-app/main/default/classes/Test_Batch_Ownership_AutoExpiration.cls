@isTest(seeAlldata=false)
private class Test_Batch_Ownership_AutoExpiration {
    static testMethod void testAutoExpirationOwnership(){
        
        Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'admin', Email='test@allegisgroup.com',
                          EmailEncodingKey='UTF-8', LastName='Test123', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = s.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='test@allegisgroup.com.devtest');
        insert u;
        
        system.runAs(u) {
            ApexCommonSettings__c a = new ApexCommonSettings__c();
            a.SoqlQuery__c  = 'SELECT ID, Account__c, Expiration_Date__c FROM Target_Account__c where Expiration_Date__c = YESTERDAY AND Account__r.RecordType.Name = \'Talent\' AND Account__r.Talent_Committed_Flag__c = TRUE';
            a.Name = 'Batch_Ownership_AutoExpiration';
            insert a;
            
            TestData td = new TestData(10);
            List<Account> accList = td.createAccounts();
            
            List<Target_Account__c> tarAccts = new List<Target_Account__c>();
            Date expDate = Date.today()-1;
            for(integer i=0;i<10;i++){
                Target_Account__c tAcc = new Target_Account__c (Account__c = accList[i].id, Expiration_Date__c = expDate, User__c = u.Id);
                tarAccts.add(tAcc);
            }
            insert tarAccts;
            
            Test.startTest();
            Batch_Ownership_AutoExpiration batch = new Batch_Ownership_AutoExpiration();
            Database.BatchableContext BC;
            List<SObject> scopeList = new List<SObject>();
                             
            String soqlQuery = 'SELECT ID, Account__c, Expiration_Date__c FROM Target_Account__c where Expiration_Date__c = YESTERDAY AND Account__r.RecordType.Name = \'Talent\' AND Account__r.Talent_Committed_Flag__c = TRUE';
            
            batch.QueryObject = soqlQuery;
            Database.QueryLocator QL= batch.start(BC);
            Database.QueryLocatorIterator QIT =  QL.iterator();
            while (QIT.hasNext())
            {
                scopeList.add(QIT.next());
            }   
            List<Target_Account__c> tAccts = new List<Target_Account__c>();
            Set<Id> accIds = new Set<Id>();
            
            for(SObject o: scopeList)
            {
                tAccts.add((Target_Account__c)o);
                accIds.add(((Target_Account__c)o).Account__c);
            }
            
            batch.execute(BC,tAccts);
            batch.finish(BC);
            //Id batchJobId = Database.executeBatch(batch);
            
            //Test Schedulable Class
            Schedule_Batch_Ownership_AutoExpiration obj = new Schedule_Batch_Ownership_AutoExpiration();   
            String chron = '0 0 11 * * ?';        
            system.schedule('Test Sched', chron, obj);
            
            Test.stopTest();
        }
        
    }

}