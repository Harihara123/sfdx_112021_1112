/*******************************************************************
* Name  : Test_Batch_ProcessOpportunityRecords 
* Author: Preetham Uppu
* Date  : Nov 11, 2016
* Details : Test class for Batch_ProcessOpportunityRecords
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest
private class Test_Batch_ProcessOpportunityRecords{
     //variable declaration
     
    //static User user = TestDataHelper.createUser('System Integration'); 
   
    static testMethod void testBatchProcessOpportunityRecords() {
          
        User user = TestDataHelper.createUser('System Integration'); 
          
        user.OpCo__c = 'Aerotek, Inc';
        insert user;
        
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
         List<Opportunity> lstOpportunity = new List<Opportunity>();
         List<Order> lstOrder = new List<Order>();
         
          if(user != Null) {
            system.runAs(user) {
                TestData TdAcc = new TestData(10);
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Developer');
                    
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     List<Order_Status__mdt> listOrderStatuses = [SELECT OrderStatusKey__c, OpportunityTotalStatus__c from Order_Status__mdt LIMIT 10];
                     
                for(Account Acc : TdAcc.createAccounts()) {       
                    for(Integer i=0;i < listOrderStatuses.size(); i++)
                    {
                    Opportunity NewOpportunity = new Opportunity(Name = 'New Opportunity'+ string.valueof(i) , Accountid = Acc.id,
                         RecordTypeId = ReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                         Req_Total_Positions__c = 1,  Req_Client_Job_Title__c = titleList[0].Id,
                         Req_Job_Description__c = 'Testing');//,OpCo__c='Aerotek, Inc'
                        
                        if(i==0){
                            NewOpportunity.Source_System_Id__c = '0020002020'+string.valueof(i)+Acc.id;
                            NewOpportunity.Req_Product__c = 'Contract'; 
                            NewOpportunity.StageName = 'Closed Won';
                            NewOpportunity.OpCo__c = 'TEKsystems, Inc.';
                            NewOpportunity.Apex_Context__c = true;
                        }else if(i==1){
                            
                            NewOpportunity.Req_Division__c = 'Global Services';
                            NewOpportunity.Legacy_Record_Type__c = 'Global Services Req';
                            NewOpportunity.OpCo__c = 'TEKsystems, Inc.';
                        
                        }       
                         
                         
                         
                        lstOpportunity.add(NewOpportunity);
                    }     
                }
                
                 Test.startTest();
                  insert lstOpportunity;
                  
               
                  
                  string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
                  integer Count = 0;
                  for(Order_Status__mdt os: listOrderStatuses)
                  {
                     
                     Order NewOrder = new Order(Name = 'New Order' + string.valueof(Count), EffectiveDate = system.today()+1,
                                                Status = 'Submitted',AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[Count].Id, recordtypeid = OrderRecordType);
                     lstOrder.add(NewOrder);
                     Count = Count + 1;
                  }
                  
             insert lstOrder;
        
              
                //insert new ProcessOpportunityRecords__c(Name='SyncOpportunityLastRunTime',LastExecutedTime__c = System.now());
                //insert new ApexCommonSettings__c(Name='ProcessOppRecordsQuery',SoqlQuery__c = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime');
                ProcessOpportunityRecords__c p = new ProcessOpportunityRecords__c();
                p.LastExecutedTime__c  = System.now();
                p.Name = 'SyncOpportunityLastRunTime';
                
                insert p;
                
                ApexCommonSettings__c a = new ApexCommonSettings__c();
                a.SoqlQuery__c  = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
                a.Name = 'ProcessOppRecordsQuery';
                insert a;
                
                Batch_ProcessOpportunityRecords batch = new Batch_ProcessOpportunityRecords();
                Database.BatchableContext BC;
                batch.dtLastBatchRunTime = DateTime.Now();
                batch.QueryAccount = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
                Iterable<SObject> itr = batch.start(BC);
                
                System.debug('Query Results'+ itr);
                
                List<SObject> scopeList = new List<SObject>();
                //Datetime dtnow = DateTime.Now();
                Datetime dtnow = DateTime.Now().addDays(-1);
                Database.QueryLocator QL= Database.getQueryLocator('Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtnow');
                 Database.QueryLocatorIterator QIT =  QL.iterator();
                   while (QIT.hasNext())
                    {
                       scopeList.add(QIT.next());
                    }   
                          
                batch.execute(BC, scopeList);
                batch.finish(BC);
              
             Test.stopTest();
            }
         }
       }
       
    static testmethod void testBatchProcessOpportunityFinish(){
         User user = TestDataHelper.createUser('System Integration'); 
         user.OpCo__c = 'Aerotek, Inc';
         insert user;
            
            DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
          
           DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
            List<Opportunity> lstOpportunity = new List<Opportunity>();
            List<Order> lstOrder = new List<Order>();
         
        if(user != Null){
            system.runAs(user) {
                   TestData TdAcc = new TestData(8);
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Developer');
                    
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     List<Order_Status__mdt> listOrderStatuses = [SELECT OrderStatusKey__c, OpportunityTotalStatus__c from Order_Status__mdt LIMIT 10];
                     
                for(Account Acc : TdAcc.createAccounts()) {       
                  for(Integer i=0;i < listOrderStatuses.size(); i++)
                  {
                    Opportunity NewOpportunity = new Opportunity(Name = 'New Opportunity'+ string.valueof(i) , Accountid = Acc.id,
                         RecordTypeId = ReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                         Req_Total_Positions__c = 1,  Req_Client_Job_Title__c = titleList[0].Id,
                         Req_Job_Description__c = 'Testing',OpCo__c='Aerotek, Inc');
                      /*
                        if(i==0){
                            NewOpportunity.Source_System_Id__c = '0020002020'+string.valueof(i)+Acc.id;
                            NewOpportunity.Req_Product__c = 'Contract'; 
                            NewOpportunity.StageName = 'Closed Won';
                            NewOpportunity.OpCo__c = 'TEKsystems, Inc.';
                            NewOpportunity.Apex_Context__c = true;
                        }else if(i==1){ 
                            NewOpportunity.Req_Division__c = 'Global Services';
                            NewOpportunity.Legacy_Record_Type__c = 'Global Services Req';
                            NewOpportunity.OpCo__c = 'TEKsystems, Inc.';
                        }else if(i==2){ 
                            NewOpportunity.Req_Division__c = 'Global Services';
                            NewOpportunity.Legacy_Record_Type__c = 'Global Services Req';
                            NewOpportunity.OpCo__c = 'TEKsystems, Inc.';
                            NewOpportunity.Req_Total_Filled__c =5;
                            NewOpportunity.Req_Total_Positions__c =1;
                        }
                          system.debug(NewOpportunity+'#Counter# '+i);
                              */  
                        lstOpportunity.add(NewOpportunity);
                  }     
                }
                  Test.startTest();
                    
                  insert lstOpportunity;
                  string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
                  integer Count = 0;
                 
                  for(Order_Status__mdt os: listOrderStatuses)
                  {
                     
                     Order NewOrder = new Order(Name = 'New Order' + string.valueof(Count), EffectiveDate = system.today()+1,
                                                Status = 'Submitted',AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[Count].Id, recordtypeid = OrderRecordType);
                     lstOrder.add(NewOrder);
                     Count = Count + 1;
                  }
                  
             insert lstOrder;
             
             ProcessOpportunityRecords__c p = new ProcessOpportunityRecords__c();
                p.LastExecutedTime__c  = System.now();
                p.Name = 'SyncOpportunityLastRunTime';
                
                insert p;
                
                ApexCommonSettings__c a = new ApexCommonSettings__c();
                a.SoqlQuery__c  = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
                a.Name = 'ProcessOppRecordsQuery';
                insert a;
                
                Batch_ProcessOpportunityRecords batch = new Batch_ProcessOpportunityRecords();
                Database.BatchableContext BC;
                batch.dtLastBatchRunTime = DateTime.Now();
                batch.QueryAccount = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
                Iterable<SObject> itr = batch.start(BC);
                
                System.debug('Query Results'+ itr);
                
                List<SObject> scopeList = new List<SObject>();
                //Datetime dtnow = DateTime.Now();
                Datetime dtnow = DateTime.Now().addDays(-1);
                Database.QueryLocator QL= Database.getQueryLocator('Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtnow');
                 Database.QueryLocatorIterator QIT =  QL.iterator();
                   while (QIT.hasNext())
                    {
                       scopeList.add(QIT.next());
                    }   
                 Test.stopTest();          
                batch.execute(BC, scopeList);
                batch.isOpportunityJobException = True;
                batch.finish(BC);
                
                
                
            }
        }
          // set<id> oppid =new set<id>();
          // for(Opportunity opp : lstOpportunity){
          //     oppid.add(opp.id);
          //     
          // }
           
          //string query ='select id,Req_HRXML_Status_Flag__c from opportunity where recordtype.name=\'req\'';

            //Id batchJobId = Database.executeBatch(new DataMigHrXMLBatch(query)); 
       
    }
   
    
}