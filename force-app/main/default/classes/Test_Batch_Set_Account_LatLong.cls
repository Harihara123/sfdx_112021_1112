@isTest(seeAlldata=false)
private class Test_Batch_Set_Account_LatLong {

    static testMethod void testscheduleBatch_EmailNotificationtoOpp() { 
    
     Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u = new User(Alias = 'admin', Email='test@allegisgroup.com',
      EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = s.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='test@allegisgroup.com.devtest');
      insert u;
      
      TestData TdAcc = new TestData(1);
      List<Account> lstNewAccounts = TdAcc.createAccounts();
   
         test.startTest();
         Batch_Set_Account_LatLong objbatch= new Batch_Set_Account_LatLong();
         objbatch.start(null);
         objbatch.execute(null,lstNewAccounts);
         objbatch.finish(null); 
         scheduleBatch_Set_Account_LatLong obj = new scheduleBatch_Set_Account_LatLong();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Test Sched', chron, obj);         
         test.stopTest();
    }
}