//***************************************************************************************************************************************/
//* Name        - Test_Batch_UpdateAccountFormerAndCurrent
//* Description - Test class for batch Utility class to update former and current fields of MLA and AP on Account
//                         based on corresponding opCo and Status.
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         03/30/2017                Created
//*****************************************************************************************************************************************/
@isTest
public class Test_Batch_UpdateAccountFormerAndCurrent
{
    static testMethod void testBatchMLARecordsLess180() 
    {
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
             Test.startTest();
             Account a =  new Account( Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
                                        
            insert a;
                                        
            string oppReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            Opportunity newOpportunity = new Opportunity(Name = 'New Opportunity' , Accountid = a.id,
                             RecordTypeId = oppReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                             Req_Total_Positions__c = 1, Req_Job_Description__c = 'Testing',OpCo__c='Major, Lindsey & Africa, LLC');
                             
            insert newOpportunity;
            
            string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
             
             Order newOrder = new Order(Name = 'New Order', EffectiveDate = system.today()+1, Source_System_LastModifiedDate__c = System.Today()-170,
                                                    AccountId = newOpportunity.AccountId, OpportunityId = newOpportunity.Id, recordtypeid = OrderRecordType,status='Started'); 
            
            insert newOrder;
            
            UpdateCompanyFormerAndCurrentDataBatch__c p = new UpdateCompanyFormerAndCurrentDataBatch__c();
            p.Name = 'CompanyFormerCurrentLastRunTime';
            p.LastExecutedTime__c = system.now().addDays(-1);
            insert p;
            
            Batch_updateCompanyFormerAndCurrentData obj = new Batch_updateCompanyFormerAndCurrentData();
            obj.query = 'Select Id,Display_LastModifiedDate__c,AccountId,Opportunity.OpCo__c from Order where Opportunity.OpCo__c in: setCompanyName and status = \'Started\'';
            Database.executeBatch(obj,200);
            Test.stopTest();
        }                                    
    }    
    
    static testMethod void testBatchMLARecordsGreater180() 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
             Test.startTest();
             Account a =  new Account( Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
                                        
            insert a;
                                        
            string oppReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            Opportunity newOpportunity = new Opportunity(Name = 'New Opportunity' , Accountid = a.id,
                             RecordTypeId = oppReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                             Req_Total_Positions__c = 1, Req_Job_Description__c = 'Testing',OpCo__c='Major, Lindsey & Africa, LLC');
                             
            insert newOpportunity;
            
            string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
             
             Order newOrder = new Order(Name = 'New Order', EffectiveDate = system.today()+1, Source_System_LastModifiedDate__c = System.Today()-200,
                                                    AccountId = newOpportunity.AccountId, OpportunityId = newOpportunity.Id, recordtypeid = OrderRecordType,status='Started'); 
            
            insert newOrder;
            
            UpdateCompanyFormerAndCurrentDataBatch__c p = new UpdateCompanyFormerAndCurrentDataBatch__c();
            p.Name = 'CompanyFormerCurrentLastRunTime';
            p.LastExecutedTime__c = system.now().addDays(-1);
            insert p;
            
            Batch_updateCompanyFormerAndCurrentData obj = new Batch_updateCompanyFormerAndCurrentData();
            obj.query = 'Select Id,Display_LastModifiedDate__c,AccountId,Opportunity.OpCo__c from Order where Opportunity.OpCo__c in: setCompanyName and status = \'Started\'';
            Database.executeBatch(obj,200);
            Test.stopTest();
        }                                    
    }   
                                 
}