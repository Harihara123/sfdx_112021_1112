//***************************************************************************************************************************************/
//* Name        - Test_Batch_UpdateReqEnterpriseSkills
//* Description - Test class for batch Utility class to update Req Enterprise Skills field
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         01/22/2018                Created
//*****************************************************************************************************************************************/
@isTest
public class Test_Batch_UpdateReqEnterpriseSkills
{
    static testMethod void testUpdateReqEnterpriseSkills() 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
             Test.startTest();
             Account a =  new Account( Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
                                        
            insert a;
            
            List<ReqSkills.Skills> lstSkill = new List<ReqSkills.Skills>();
            
            ReqSkills.Skills objSkill = new ReqSkills.Skills('Testing',true);
            lstSkill.add(objSkill);
            
                                        
            string oppReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            Opportunity newOpportunity = new Opportunity(Name = 'New Opportunity' , Accountid = a.id,
                             RecordTypeId = oppReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                             Req_Total_Positions__c = 1, Req_Job_Description__c = 'Testing',OpCo__c='Major, Lindsey & Africa, LLC',EnterpriseReqSkills__c = JSON.serialize(lstSkill),Req_Skill_Details__c='test detail');
                             
            insert newOpportunity;
            
            Req_Skill__c objSkill1 = new Req_Skill__c();
            objSkill1.Skill__c = 'Java';
            objSkill1.Level__c = 'Optional';
            objSkill1.Competency__c = 'Low';
            objSkill1.Req__c = newOpportunity.id;
            insert objSkill1;
            
            Req_Skill__c objSkill2 = new Req_Skill__c();
            objSkill2.Skill__c = 'AWS';
            objSkill2.Level__c = 'Optional';
            objSkill2.Competency__c = 'Low';
            objSkill2.Req__c = newOpportunity.id;
            insert objSkill2;
            
            Batch_UpdateReqEnterpriseSkills objBatch_UpdateReqEnterpriseSkills = new Batch_UpdateReqEnterpriseSkills();
            objBatch_UpdateReqEnterpriseSkills.QueryReq = 'select id,Req_Skill_Details__c,EnterpriseReqSkills__c,(select id,Skill__c,Level__c,Competency__c,Years_of_Experience__c,Skill_Usage_Description__c from Skills__r) from opportunity where id in (select Req__c from Req_Skill__c) and recordtype.name=\'Req\'';
            Database.executeBatch(objBatch_UpdateReqEnterpriseSkills);
            
            Test.stopTest();
        }                                    
    }
    
    static testMethod void testUpdateReqEnterpriseSkillsException() 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
             Test.startTest();
             Account a =  new Account( Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),MLA_Former_Starts__c = 10,MLA_Current_Starts__c = 10);
                                        
            insert a;
    
            ReqSkills.Skills objSkill = new ReqSkills.Skills('Testing',true);
            
                                        
            string oppReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            Opportunity newOpportunity = new Opportunity(Name = 'New Opportunity' , Accountid = a.id,
                             RecordTypeId = oppReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                             Req_Total_Positions__c = 1, Req_Job_Description__c = 'Testing',OpCo__c='Major, Lindsey & Africa, LLC',EnterpriseReqSkills__c = JSON.serialize(objSkill),Req_Skill_Details__c='test detail');
                             
            insert newOpportunity;
            
            Req_Skill__c objSkill1 = new Req_Skill__c();
            objSkill1.Skill__c = 'Java';
            objSkill1.Level__c = 'Optional';
            objSkill1.Competency__c = 'Low';
            objSkill1.Req__c = newOpportunity.id;
            insert objSkill1;
            
            Req_Skill__c objSkill2 = new Req_Skill__c();
            objSkill2.Skill__c = 'AWS';
            objSkill2.Level__c = 'Optional';
            objSkill2.Competency__c = 'Low';
            objSkill2.Req__c = newOpportunity.id;
            insert objSkill2;
            
            Batch_UpdateReqEnterpriseSkills objBatch_UpdateReqEnterpriseSkills = new Batch_UpdateReqEnterpriseSkills();
            objBatch_UpdateReqEnterpriseSkills.QueryReq = 'select id,Req_Skill_Details__c,EnterpriseReqSkills__c,(select id,Skill__c,Level__c,Competency__c,Years_of_Experience__c,Skill_Usage_Description__c from Skills__r) from opportunity where id in (select Req__c from Req_Skill__c) and recordtype.name=\'Req\'';
            Database.executeBatch(objBatch_UpdateReqEnterpriseSkills);
            
            Test.stopTest();
        }                                    
    }
         
    
                            
}