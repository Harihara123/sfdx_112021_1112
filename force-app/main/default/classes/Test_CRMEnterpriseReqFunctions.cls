@isTest(seeAllData=true)
public  class Test_CRMEnterpriseReqFunctions {


      static testMethod void testSaveEnterpriseReq() {
        Test.startTest();
        //Product test data
        TestData td=new TestData();
        Account acc=TestData.newAccount(1);
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        Contact con=td.createContactWithoutAccount(acc.id);
        
        
        Opportunity opp1=new Opportunity(Name = 'TESTOPP1',CloseDate = system.today(),StageName = 'Draft',accountId= acc.Id,Req_Division__c='Aerotek CE',OpCo__c='Aerotek, Inc',Legacy_Product__c=testProduct.id);    
        opp1.RecordTypeId='01224000000gLMyAAM';
        
       // opp1.Req_Rate_Frequency__c='--None--';
      //  opp1.Req_Duration_Unit__c='--None--';
        
        insert opp1;
        
        
                
        Opportunity opp=new Opportunity(Name = 'TESTOPP',CloseDate = system.today(),StageName = 'Draft',accountId= acc.Id,Req_Division__c='Aerotek CE',OpCo__c='Aerotek, Inc',Legacy_Product__c=testProduct.id);    
        opp.RecordTypeId='01224000000gLMyAAM';
        
        opp.Req_Rate_Frequency__c='--None--';
        opp.Req_Duration_Unit__c='--None--';
        opp.Originating_Opportunity__c=opp1.id;
        System.assert(testProduct!=null,'Product not created successfully');
        
      //  insert opp;
        String oppStr=JSON.serialize(opp);
        String prodStr=JSON.serialize(testProduct);
        String accStr=JSON.serialize(acc);
        String conStr=JSON.serialize(con);
        String skills=null;
        String Mainskillid=testProduct.id;
        String officeId=null;
        String altList='[]';
        Order proactiveSubmittal = new Order();
        
        String opId=CRMEnterpriseReqFunctions.saveEnterpriseReq(oppStr,accStr,conStr, skills, Mainskillid, officeId, altList, proactiveSubmittal);
        Opportunity opppp=[select id ,Originating_Opportunity__c from Opportunity where id=:opId];
        System.assert(opId!=null,'Error while saving opportunity');
        System.debug('opp.Originating_Opportunity__c----->'+opppp.Originating_Opportunity__c);
        Test.stopTest();
    }
    
    
    static testMethod void testGetEnterpriseReqPicklists() {
         TestData td=new TestData(1);
          List<Account> listAccount=td.createAccounts();
        List<Opportunity> listOpp=td.createOpportunities();
        Contact con = td.createContactWithoutAccount(listAccount[0].Id);
        Test.startTest();
       
      
        Opportunity opp=listOpp.get(0);
        opp.Division__c='Aerotek CE';
        opp.OpCo__c='Aerotek, Inc';
        update opp;
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        
        opp.Legacy_Product__c=testProduct.Id;
        
        System.assert(testProduct!=null,'Product not created successfully');
        CRMEntReqPicklistModel crmPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(opp.id),String.valueOf(opp.id));
        CRMEntReqPicklistModel crmAPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(listAccount[0].id),String.valueOf(listAccount[0].id));
        CRMEntReqPicklistModel crmCPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(con.id),String.valueOf(con.id));
        CRMEnterpriseReqFunctions.performSFServerCall('getDummyString',null);
        System.assert(crmPH!=null,'Enterprise Req returned is null');
       // System.assert(crmPH.opcoMappings.size()==0,'Opco picklist values are blanks ');
        Test.stopTest();
    }
    
    static testMethod void testGetsegmentReqPicklists() {
         Test.startTest();
         TestData td=new TestData(1);
         Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
         CRMEntReqPicklistModel model= CRMEnterpriseReqFunctions.getsegmentReqPicklists('Aerotek, Inc','Aerotek CE');
         CRMProductHierarchy crm=new CRMProductHierarchy();
         crm.SegmentMappings=model.SegmentMappings;
         System.assert(model.SegmentMappings!=null,'Segment Req Picklist values are not retrieved');
         Test.stopTest();
    }
    
    static testMethod void testGetEnterpriseReqPicklists1() {
         TestData td=new TestData(1);
          List<Account> listAccount=td.createAccounts();
        List<Opportunity> listOpp=td.createOpportunities();
        Contact con = td.createContactWithoutAccount(listAccount[0].Id);
        Test.startTest();
       
      
        Opportunity opp=listOpp.get(0);
        opp.Division__c='AG_EMEA';
        opp.OpCo__c='AG_EMEA';
        update opp;
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        //TEKsystems, Inc.
        opp.Legacy_Product__c=testProduct.Id;
        
        System.assert(testProduct!=null,'Product not created successfully');
        CRMEntReqPicklistModel crmPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(opp.id),String.valueOf(opp.id));
    } 
    static testMethod void testGetEnterpriseReqPicklists2() {
         TestData td=new TestData(1);
          List<Account> listAccount=td.createAccounts();
        List<Opportunity> listOpp=td.createOpportunities();
        Contact con = td.createContactWithoutAccount(listAccount[0].Id);
        Test.startTest();
        Opportunity opp=listOpp.get(0);
        opp.Division__c='TEKsystems, Inc.';
        opp.OpCo__c='TEKsystems, Inc.';
        update opp;
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        //
        opp.Legacy_Product__c=testProduct.Id;
        
        System.assert(testProduct!=null,'Product not created successfully');
        CRMEntReqPicklistModel crmPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(opp.id),String.valueOf(opp.id));
    } 
	
	/*static testMethod void testGetEnterpriseReqProactiveSubmittal() {
       // Test.startTest();
			TestData td=new TestData(1);
			List<Account> listAccount=td.createAccounts();
			Contact con =TestData.newContact(listAccount[0].Id, 1, 'Client');
			insert con;

			Account acc = CreateTalentTestData.createTalent();
			acc.Skills__c = '{"skills":["Salesforce.com","Marketing Cloud"]}';
			update acc;
			Contact ct = CreateTalentTestData.createTalentContact(acc);
		

			User usr = TestDataHelper.createUser('Single Desk 1');
			insert usr; 

			Order proactiveSubmittal = new Order();
			proactiveSubmittal.RecordTypeId = Utility.getRecordTypeId('Order', 'Proactive');
			proactiveSubmittal.ShipToContact = ct;
			proactiveSubmittal.Job_Title__c = 'Architect';
			proactiveSubmittal.Point_of_Contact_AM__c = usr.Id;
			proactiveSubmittal.Skills__c = '{"skills":["Salesforce.com","Marketing Cloud"]}';
			proactiveSubmittal.AccountId = listAccount[0].Id;
			proactiveSubmittal.Hiring_Manager__c = con.Id;
			proactiveSubmittal.Is_Created_As_Promarket__c = true;
			proactiveSubmittal.Status = 'Submitted';
			proactiveSubmittal.EffectiveDate = System.today();
        
			insert proactiveSubmittal;
		Test.startTest();
			CRMEntReqPicklistModel crmPH = CRMEnterpriseReqFunctions.getEnterpriseReqPicklists(String.valueOf(proactiveSubmittal.id),String.valueOf(proactiveSubmittal.id));
			System.assertEquals(crmPH.proactiveSubmittal.ShipToContactId, proactiveSubmittal.ShipToContactId);
        Test.stopTest();

	}*/
	 
    static testMethod void testgetJobcodeReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        CRMEntReqPicklistModel model= CRMEnterpriseReqFunctions.getJobcodeReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.JobCodeMappings=model.JobCodeMappings;
        System.assert(model!=null,'Job code Req Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetCategoryReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        CRMEntReqPicklistModel model= CRMEnterpriseReqFunctions.getCategoryReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.CategoryMappings=model.CategoryMappings;
        System.assert(model.CategoryMappings!=null,'Category Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetMainskillReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        System.debug('Adding this for debugging');
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        CRMEntReqPicklistModel model= CRMEnterpriseReqFunctions.getMainskillReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob','TestCategory');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.MainSkillMappings=model.MainSkillMappings;
        System.assert(model.MainSkillMappings!=null,'Main Skill Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetMainskillid(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        String model= CRMEnterpriseReqFunctions.getMainskillid('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob','TestCategory','testSkill');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        System.assert(model!=null, 'Skills are not retrieved');
        Test.stopTest();
    }
    
    
     private static Product2 createTestProduct(){       
        Product2 testProduct = new Product2();
        testProduct.Name='StandardProduct';
        testProduct.Category__c='TestCategory';
        testProduct.Category_Id__c ='123';
        
        testProduct.Division_Name__c='Aerotek CE';
        testProduct.Job_code__c ='TestJob';
        testProduct.Jobcode_Id__c='123';
        testProduct.OpCo__c ='Aerotek, Inc';
        testProduct.OpCo_Id__c='123';
        testProduct.Segment__c ='testSegemnt';
        testProduct.Segment_Id__c ='123';
        testProduct.Skill__c ='testSkill';
        testProduct.Skill_Id__c ='123';
        testProduct.IsActive = true;        
        insert testProduct;
        return testProduct;
        
    }
    static testMethod void testSaveEnterpriseReqPart2() {
        Test.startTest();
        TestData td=new TestData();
        Account acc=TestData.newAccount(1);
        Product2 testProduct=Test_CRMEnterpriseReqFunctions.createTestProduct();
        Contact con=td.createContactWithoutAccount(acc.id);
        Opportunity opp1=new Opportunity(Name = 'TESTOPP1',CloseDate = system.today(),StageName = 'Draft',accountId= acc.Id,Req_Division__c='Aerotek CE',OpCo__c='TEKsystems, Inc.',Legacy_Product__c=testProduct.id);    
        opp1.RecordTypeId='01224000000gLMyAAM';
        insert opp1;
                
        Opportunity opp=new Opportunity(Name = 'TESTOPP',CloseDate = system.today(),StageName = 'Draft',accountId= acc.Id,Req_Division__c='Aerotek CE',OpCo__c='TEKsystems, Inc.',Legacy_Product__c=testProduct.id);    
        opp.RecordTypeId='01224000000gLMyAAM';
        opp.Req_TGS_Requirement__c ='No';
        opp.Req_Rate_Frequency__c='--None--';
        opp.Req_Duration_Unit__c='--None--';
        opp.Originating_Opportunity__c=opp1.id;
        System.assert(testProduct!=null,'Product not created successfully');
        
      //  insert opp;
        String oppStr=JSON.serialize(opp);
        String prodStr=JSON.serialize(testProduct);
        String accStr=JSON.serialize(acc);
        String conStr=JSON.serialize(con);
        String skills=null;
        String Mainskillid=testProduct.id;
        String officeId=null;
        String altList='[]';
        
         Order proactiveSubmittal = new Order();
              
        String opId=CRMEnterpriseReqFunctions.saveEnterpriseReq(oppStr,accStr,conStr, skills, Mainskillid, officeId, altList, proactiveSubmittal);
        Opportunity opppp=[select id ,Originating_Opportunity__c from Opportunity where id=:opId];
        System.assert(opId!=null,'Error while saving opportunity');
        System.debug('opp.Originating_Opportunity__c----->'+opppp.Originating_Opportunity__c);
        Test.stopTest();
    }
    static testMethod void testgetProgressBarDetails(){
        
        list<ReqProgressBar__c> requiredFields = new list<ReqProgressBar__c>();
        ReqProgressBar__c rp1 = new ReqProgressBar__c(Name= 'Account Name', Active__c= TRUE , draft__c= TRUE, qualified__c= TRUE,
                                                      error_var__c='AccountVarName',field_api__c='account.Name', Field_Api_Edit__c='rec.LDS_Account_Name__c', 
                                                      label_Id__c='label-account', Full_Name__c='Account Name', opco__c='AG_EMEA',aura_id__c ='acctId', Sort_Order__c= 1);
        requiredFields.add(rp1);
        ReqProgressBar__c rp2 = new ReqProgressBar__c(Name= 'Duration Unit - Contract to Hire', Active__c= TRUE , draft__c= FALSE, qualified__c= TRUE, Contract_to_Hire__c=TRUE,
                                                      error_var__c='durationUnitValMsg',field_api__c='req.Req_Duration_Unit__c', Field_Api_Edit__c='Req_Duration_Unit__c', 
                                                      label_Id__c='label-durationUnitId', Full_Name__c='Duration Unit', opco__c='AG_EMEA',aura_id__c ='durationUnitIdCTH', Sort_Order__c= 17);
        
        requiredFields.add(rp2);
        ReqProgressBar__c rp3 = new ReqProgressBar__c(Name= 'Duration Unit - Contract', Active__c= TRUE , draft__c= FALSE, qualified__c= TRUE,Contract__c= TRUE,
                                                      error_var__c='durationUnitValMsg',field_api__c='req.Req_Duration_Unit__c', Field_Api_Edit__c='Req_Duration_Unit__c', 
                                                      label_Id__c='label-durationUnitId', Full_Name__c='Duration Unit', opco__c='AG_EMEA',aura_id__c ='durationUnitIdCTH', Sort_Order__c= 17);
        
        requiredFields.add(rp3);
        ReqProgressBar__c rp4 = new ReqProgressBar__c(Name= 'CurrencyPerm', Active__c= TRUE , draft__c= TRUE, qualified__c= TRUE,Permanent__c= TRUE,
                                                      error_var__c='CurrencyVarName',field_api__c='req.Currency__c', Field_Api_Edit__c='Currency__c', 
                                                      label_Id__c='label-currencyId', Full_Name__c='Currency', opco__c='AG_EMEA',aura_id__c ='currencyIdPerm', Sort_Order__c= 20);
        
        requiredFields.add(rp4);
        test.startTest();
        //user usr = [SELECT id , CompanyName FROM user WHERE CompanyName = 'Allegis Group, Inc.' AND email = 'snayini@allegisgroup.com.invalid' LIMIT 1];
       /* User usr=new User();
       // usr.Name='testName';
        usr.OPCO__c='TEKsystems, Inc.';
        usr.Email='test@teksystems.com';
        usr.CompanyName='TEKsystems, Inc.';
        usr.Username='test@teksystems.com';
        usr.LastName='testLastName';
        usr.Alias='test';
        usr.ProfileId='00e24000001AcBhAAK';
        insert usr;*/
        
        List<User> usr=TestDataHelper.createUsers(1,'Single Desk 1');
        user us=usr[0];
        us.CompanyName='TEKsystems, Inc.';
        us.OPCO__c='TEKsystems, Inc.';
        update us;
           // system.runAs(usr[0]){
           system.runAs(us){
                 list<ReqProgressBar__c> reqProgress = CRMEnterpriseReqFunctions.getProgressBarDetails('Draft','Contract','Yes');
                 list<ReqProgressBar__c> reqProgress1= CRMEnterpriseReqFunctions.getProgressBarDetails('Draft','Permanent','Yes');
                 list<ReqProgressBar__c> reqProgress2= CRMEnterpriseReqFunctions.getProgressBarDetails('Draft','Contract to Hire','Yes');
                 list<ReqProgressBar__c> reqProgress3= CRMEnterpriseReqFunctions.getProgressBarDetails('Qualified','Contract','Yes');
                 list<ReqProgressBar__c> reqProgress4= CRMEnterpriseReqFunctions.getProgressBarDetails('Qualified','Permanent','Yes');
    		   list<ReqProgressBar__c> reqProgress5= CRMEnterpriseReqFunctions.getProgressBarDetails('Qualified','Contract to Hire','Yes');
            }
        
        	system.assert( requiredFields.size() != null, 'The size of list should not equal null');
       	test.stopTest();
    }
    static testMethod void testgetEmailMessage() {
        test.startTest();
        Object emailMessage=CRMEnterpriseReqFunctions.getEmailMessage('test');
        system.assert( emailMessage == null, 'Returned null email message');
        test.stopTest();
    }
    static testMethod void testfetchSkillSpecialtyClassifier() {
        test.startTest();
        try{
            CRMEnterpriseReqFunctions.fetchSkillSpecialtyClassifier('Developer','Java programmer');
            system.assertEquals(true, true);
        }
        catch(Exception error){
            system.assertEquals(true, true);
        }
        test.stopTest();
    }   
    static testMethod void getOppsOnSearchTest() {
        test.startTest();
        try {            
            String searchText = 'TES%';
            List<Opportunity> opps = new List<Opportunity>();
            opps = [Select Id, Name FROM Opportunity WHERE Req_Qualifying_Stage__c != 'Staging' AND 
                    Req_Merged_with_VMS_Req__c = false AND Req_VMS__c = false AND 
                    (Name LIKE :searchText OR Account.Name LIKE :searchText OR Owner.Name LIKE :searchText OR StageName LIKE :searchText) LIMIT 10];
            List<CRMEnterpriseReqFunctions.RecentlyViewedWrapper> res = new List<CRMEnterpriseReqFunctions.RecentlyViewedWrapper>();
            res = CRMEnterpriseReqFunctions.getOppsOnSearch(searchText);
            system.assertEquals(res.size(), opps.size(), 'missing opps on search');
            
            Set<Id> oppIds = new Set<Id>();
            for(RecentlyViewed r: [SELECT Id, Name FROM RecentlyViewed WHERE Type IN ('Opportunity') ORDER BY LastViewedDate DESC]) {
                    oppIds.add(r.Id);
            }
            opps = [Select Id, Name FROM Opportunity WHERE Id IN :oppIds AND Req_Qualifying_Stage__c != 'Staging' AND 
                    Req_Merged_with_VMS_Req__c = false AND Req_VMS__c = false ORDER BY LastViewedDate DESC LIMIT 10];            
            res = CRMEnterpriseReqFunctions.getOppsOnSearch('');            
                        
        } catch(Exception ex) {            
        }
        test.stopTest();
    }
    static testMethod void getOppSummaryTest() {
        test.startTest();
        try {
            Opportunity opp = [Select Id From Opportunity LIMIT 1];
            Opportunity res = CRMEnterpriseReqFunctions.getOppSummary(opp.Id);
            system.assertEquals(opp.AccountId, res.AccountId, 'opp account not same');
        } catch(Exception ex) {            
        }
        test.stopTest();
    }
    static testMethod void mergeVMSOppTest() {
        Test.startTest();
        try {
            List<Opportunity> updateOppList=new List<Opportunity>();
            TestData td=new TestData(2);
        	List<Opportunity> listOpp=td.createOpportunities();
            Opportunity opp=listOpp.get(0);
        	opp.Division__c='Aerotek CE';
        	opp.OpCo__c='Aerotek, Inc';
            opp.StageName='Draft';
        	updateOppList.add(opp);            
            Opportunity vmsopp=listOpp.get(1);
        	vmsopp.Division__c='Aerotek CE';
        	vmsopp.OpCo__c='Aerotek, Inc';
            vmsopp.StageName='Staging';
            vmsopp.Req_VMS__c = true;
            updateOppList.add(vmsopp);
        	
            update updateOppList;            
            Map<Id,Id> mergeIds = new Map<Id,Id>();
            mergeIds.put(updateOppList[1].Id, updateOppList[0].Id);
            Boolean flag = CRMEnterpriseReqFunctions.MergeVMSReq(mergeIds);
            system.assertEquals(flag, true, 'merge failed');
        } catch(Exception ex) {
            
        }
        Test.stopTest();
    }
    static testMethod void testSkillSpecialityWrapper() {
        Test.startTest();
        String payload='{"id":"abc123","req_job_title__c": "Senior Java Developer","req_job_description__c": "Amazing opportunity team","req_qualification__c": "Java, spring, GAE, GCP","enterprisereqskills__c":"","req_skill_details__c": "","opco__c": "TEKsystems, Inc.","suggested_job_titles__c": ""}';
        try{
            CRM_DSIAServiceRespWrapper DSIAWrapper=new CRM_DSIAServiceRespWrapper();
        	DSIAWrapper = CRMEnterpriseReqFunctions.fetchSkillSpecialtyWrapper(payload);            
            system.assertEquals(true, true);
        }
        catch(Exception error){
            system.assertEquals(true, true);
        }
        
        
        Test.stopTest();
    }

    @isTest static void testfetchClonedOppDetails() {
        Account acc = new Account(
            Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com');
        insert acc;

        Test.startTest();
        string tgsRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Profile p = [SELECT id, Name FROM Profile WHERE Name ='System Administrator' LIMIT 1 ];        
        User u = new User(CompanyName = 'TestComp',FirstName = 'first',LastName = 'last',LocaleSidKey = 'en_CA',UserName = 'testfirst.last@allegisgroup.com',
            TimeZoneSidKey = 'America/Indiana/Indianapolis', Email = 'testfrt.last@allegisgroup.com',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US',
            Alias = 't12345',Team__c = 'Birmingham AC Perm',OPCO__c = 'TEM',Region__c='POPULUS',Region_Code__c='POPUL',ProfileId = p.Id,IsRegionalDirector__c = true
        );
        insert u;        

        User_Organization__c org = new User_Organization__c();
        org.Name = 'Tek';
        insert org;
                      
        Opportunity newOpp = new Opportunity();
        newOpp.Name = 'New ReqOpportunity';
        newOpp.Accountid = acc.Id;
        newOpp.RecordTypeId = tgsRecordTypeId;         
        newOpp.Apex_Context__c = true;
        Date closingdate = system.today();
        newOpp.CloseDate = closingdate.addDays(25);                                                        
        newOpp.Regional_Director__c = u.Id;         
        newOpp.BusinessUnit__c = 'Board Practice';
        newOpp.Req_Product__c = 'Permanent';
        newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
        newOpp.Req_Worksite_Street__c = '987 Hidden St';
        newOpp.Req_Worksite_City__c = 'Baltimore';
        newOpp.Req_Worksite_Postal_Code__c = '21228';
        newOpp.Req_Worksite_Country__c = 'United States';
        newOpp.Organization_Office__c = org.Id;
        newOpp.Currency__c = 'USD - U.S Dollar';
        newOpp.StageName = 'Draft';
        
        insert newOpp;
        Opportunity opp = CRMEnterpriseReqFunctions.fetchClonedOppDetails(newOpp.Id);        
        system.assertEquals(opp.Id, newOpp.Id, 'Record did not match');
        Test.stopTest();
    }
}