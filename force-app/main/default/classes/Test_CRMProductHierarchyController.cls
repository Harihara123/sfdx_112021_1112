@isTest
public class Test_CRMProductHierarchyController {

    
    static testMethod void testSaveEnterpriseReq() {
        Test.startTest();
        //Product test data
        Account acc=TestData.newAccount(1);
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        
        
        Opportunity opp=new Opportunity(Name = 'TESTOPP',CloseDate = system.today(),StageName = 'Draft',accountId= acc.Id,Req_Division__c='Aerotek CE',OpCo__c='Aerotek, Inc',Legacy_Product__c=testProduct.id);    
        
       
        System.assert(testProduct!=null,'Product not created successfully');
        
        insert opp;
        String oppStr=JSON.serialize(opp);
        String prodStr=JSON.serialize(testProduct);
		String oppId=CRMProductHierarchyController.saveEnterpriseReq( oppStr, prodStr);
        opportunity oppId1=CRMProductHierarchyController.getReqProductdata( oppStr, prodStr);
        System.assert(oppId!=null,'Error while saving opportunity');
        
        Test.stopTest();
    }
    
    
    static testMethod void testGetEnterpriseReqPicklists() {
        Test.startTest();
        TestData td=new TestData(1);
        List<Account> listAccount=td.createAccounts();
        List<Opportunity> listOpp=td.createOpportunities();
        Opportunity opp=listOpp.get(0);
        opp.Division__c='Aerotek CE';
        opp.OpCo__c='Aerotek, Inc';
       // opp.Product__c='test';        
        //update opp;
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        opp.Product__c= testProduct.Id;
        opp.Legacy_Product__c=testProduct.Id;
        opp.Req_Draft_Reason__c= testProduct.Id;
        update opp;
        System.assert(testProduct!=null,'Product not created successfully');
        try{
            CRMProductHierarchy crmPH = CRMProductHierarchyController.getEnterpriseReqPicklists(String.valueOf(opp.id));
            System.assert(crmPH!=null,'Enterprise Req returned is null');
             
            
        }
        catch(exception e){
            
        }
        
        
       // System.assert(crmPH.opcoMappings.size()==0,'Opco picklist values are blanks ');
        Test.stopTest();
    }
    
    static testMethod void testGetsegmentReqPicklists() {
         Test.startTest();
         TestData td=new TestData(1);
         Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
         CRMEntReqPicklistModel model= CRMProductHierarchyController.getsegmentReqPicklists('Aerotek, Inc','Aerotek CE');
         CRMProductHierarchy crm=new CRMProductHierarchy();
         crm.SegmentMappings=model.SegmentMappings;
         System.assert(model.SegmentMappings!=null,'Segment Req Picklist values are not retrieved');
         Test.stopTest();
    }
    
    static testMethod void testgetJobcodeReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        CRMEntReqPicklistModel model= CRMProductHierarchyController.getJobcodeReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.JobCodeMappings=model.JobCodeMappings;
        System.assert(model!=null,'Job code Req Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetCategoryReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        CRMEntReqPicklistModel model= CRMProductHierarchyController.getCategoryReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.CategoryMappings=model.CategoryMappings;
        System.assert(model.CategoryMappings!=null,'Category Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetMainskillReqPicklists(){
        Test.startTest();
        TestData td=new TestData(1);
        System.debug('Adding this for debugging');
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        CRMEntReqPicklistModel model= CRMProductHierarchyController.getMainskillReqPicklists('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob','TestCategory');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        crm.MainSkillMappings=model.MainSkillMappings;
        System.assert(model.MainSkillMappings!=null,'Main Skill Pick list is not retrieved');
        Test.stopTest();
    }
    
    static testMethod void testgetMainskillid(){
        Test.startTest();
        TestData td=new TestData(1);
        Product2 testProduct=Test_CRMProductHierarchyController.createTestProduct();
        String model= CRMProductHierarchyController.getMainskillid('Aerotek, Inc','Aerotek CE','testSegemnt','TestJob','TestCategory','testSkill');
        CRMProductHierarchy crm=new CRMProductHierarchy();
        //System.assert(model!=null, 'Skills are not retrieved');
        Test.stopTest();
    }
    
    
     private static Product2 createTestProduct(){    	
    	Product2 testProduct = new Product2();
    	testProduct.Name='StandardProduct';
    	testProduct.Category__c='TestCategory';
    	testProduct.Category_Id__c ='123';
        
    	testProduct.Division_Name__c='Aerotek CE';
    	testProduct.Job_code__c ='TestJob';
    	testProduct.Jobcode_Id__c='123';
    	testProduct.OpCo__c ='Aerotek, Inc';
    	testProduct.OpCo_Id__c='123';
    	testProduct.Segment__c ='Architecture & Engineering';
    	testProduct.Segment_Id__c ='123';
    	testProduct.Skill__c ='testSkill';
    	testProduct.Skill_Id__c ='123';
    	testProduct.IsActive = true;    	
        insert testProduct;
    	return testProduct;
    	
    }
    
}