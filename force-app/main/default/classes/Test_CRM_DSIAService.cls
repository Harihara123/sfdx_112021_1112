@isTest
public class Test_CRM_DSIAService {
	@isTest
    public static void test_get_access_token() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_CRM_DSIAServiceMock());       
        String payload='{"id":"abc123","req_job_title__c": "Senior Java Developer","req_job_description__c": "Amazing opportunity team","req_qualification__c": "Java, spring, GAE, GCP","enterprisereqskills__c":"","req_skill_details__c": "","opco__c": "TEKsystems, Inc.","suggested_job_titles__c": ""}';
        CRM_DSIAServiceRespWrapper DSIAWrapper=new CRM_DSIAServiceRespWrapper();
        DSIAWrapper=CRM_DSIAService.callDSIAService(payload);        
        Test.stopTest();
    }    
}