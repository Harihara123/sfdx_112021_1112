@isTest
global class Test_CRM_DSIAServiceMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {   
        
        String StrResponse='{"RVT":{"Experience_level":"Expert Level","Experience_level_confidence_value":"1.0","Occupation":"15-100 Developer",';
	StrResponse=StrResponse+'"Occupation_Group":"Information Technology","Occupation_confidence_value":"0.999999999999958",';
	StrResponse=StrResponse+'"RVT_detail":{"active_count":"892.0","active_count_without_outlier":"1337.0","total_placements_wo_outlier":"5744"},';
	StrResponse=StrResponse+'"message":"success","status":"OK"},"id":"abc123","message":"successful","onet_job_code":{"confidence":0.5121086239814758,"message":"successful",';
	StrResponse=StrResponse+'"onet_soc_code":"15-1255.00","onet_soc_description":"Web and Digital Interface Designers",';
	StrResponse=StrResponse+'"status":"OK"},"skill_specialty":{"label":"Development","message":"successful",';
	StrResponse=StrResponse+'"score":"0.94041","status":"OK"},"skillset":{"message":"success","skillset_classifier":{"app development":0.649367236223915,';
	StrResponse=StrResponse+'"java development":1.0,"version control systems":0.9213121283796281},"status":"OK"},"status":"OK"}';
        
        
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(StrResponse);
        response.setStatusCode(200);
        return response; 
    }
}