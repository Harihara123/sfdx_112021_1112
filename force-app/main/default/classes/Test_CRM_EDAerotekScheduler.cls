@isTest
public class Test_CRM_EDAerotekScheduler {
    
    static testmethod void CRM_EDScoreUpdateBatch_Aerotek_Test(){                
          
        Test.startTest();
        ed_insights__SDDPredictionConfig__c ed1 = new ed_insights__SDDPredictionConfig__c();
        ed1.Name = 'ED Write Back Aerotek CE';
        ed1.ed_insights__Batch_Update_Query__c = 'Select Id,ED_Time_from_First_Submittal_to_Close__c , Req_Days_Open__c from Opportunity where isclosed = false and recordtype.name = \'Req\' and opco__c = \'Aerotek, Inc\' and Req_Division__c = \'Aerotek CE\' and Req_Days_Open__c != null';
        insert ed1;
        CRM_EDAerotekScheduler sh1 = new CRM_EDAerotekScheduler();
        String sch = '23 30 8 20 4 ?';
        String jobId  = system.schedule('CRM_EDAerotekScheduler Test', sch, sh1); 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(sch, ct.CronExpression);
        Test.stopTest();       
        
    }
    static testmethod void CRM_EDScoreUpdateBatch_AerotekScientific_Test(){                
          
        Test.startTest();
        ed_insights__SDDPredictionConfig__c ed1 = new ed_insights__SDDPredictionConfig__c();
        ed1.Name = 'ED Write Back Aerotek Scientific';
        ed1.ed_insights__Batch_Update_Query__c = 'Select Id,ED_RefreshCount__c,ED_Outcome__c,Req_Division__c , Req_Days_Open__c from Opportunity where isclosed = false and recordtype.name = \'Req\' and opco__c = \'Aerotek, Inc\' and Req_Division__c = \'Aerotek Scientific, LLC\' and Req_Days_Open__c != null';
        insert ed1;
        CRM_EDAerotekScientificScheduler scientificscheduler = new CRM_EDAerotekScientificScheduler();
        String sch = '23 30 8 20 4 ?';
        String jobId  = system.schedule('CRM_EDAerotekScheduler Test', sch, scientificscheduler); 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(sch, ct.CronExpression);
        Test.stopTest();       
        
    }

}