@isTest
public class Test_CRM_EDScoreUpdateBatch {
	static testMethod void CRM_EDScoreUpdateMethod() {        
        List<opportunity> Opplst=new list<opportunity>();        
        Account act=TestData.newAccount(1);
        insert act;
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+act.name , Accountid = act.id,
                                                         ED_RefreshCount__c=2,stagename = 'Draft',
                                                     OPCO__c ='Aerotek, Inc', CloseDate = system.today()+1);  
        Opplst.add(NewOpportunity);       
        insert Opplst;        
        Test.startTest();
        String strQuery = 'Select Id,ED_Time_from_First_Submittal_to_Close__c , Req_Days_Open__c,ED_RefreshCount__c from Opportunity where Id = \''+Opplst[0].Id+'\'';
		CRM_EDScoreUpdateBatch obj = new CRM_EDScoreUpdateBatch(strQuery);
        DataBase.executeBatch(obj);
        System.assertEquals(1, Opplst.size());            
        Test.stopTest();
    }
}