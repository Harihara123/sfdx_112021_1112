@isTest
public class Test_CRM_EDTeksystemsScheduler {
    
    static testmethod void CRM_EDScoreUpdateBatchTeksystems_Test(){               
          
        Test.startTest();
        ed_insights__SDDPredictionConfig__c ed1 = new ed_insights__SDDPredictionConfig__c();
        ed1.Name = 'ED Write Back Teksystems Application';
        ed1.ed_insights__Batch_Update_Query__c = 'select Id,ED_Time_from_First_Submittal_to_Close__c , Req_Days_Open__c from opportunity where isclosed = false and recordtype.name = \'Req\' and opco__c = \'TEKsystems, Inc.\' and Req_Division__c = \'Applications\' and Req_Days_Open__c != null';
        insert ed1;
        CRM_EDTeksystemsScheduler sh1 = new CRM_EDTeksystemsScheduler();
        String sch = '23 30 8 20 4 ?'; 
        String jobId  = system.schedule('CRM_EDTeksystemsScheduler Test', sch, sh1);              
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(sch, ct.CronExpression);
        Test.stopTest();      
        
    }

}