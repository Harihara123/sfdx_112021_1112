@isTest
public class Test_CRM_OpportunityToRVTData{
    
    static testMethod void testFetchRVTData() {
    	List<Opportunity> lstOpportunity = new List<Opportunity>();
        RVTdata__c rvtdata = new RVTdata__c( 
										job_level__c = 'Expert Level',
										metro__c = 'Boise City',
										occupation_code__c	='17-100 Mechanical Engineer',
										RVT_Average_Bill_Rate__c = 65.80,
										RVT_Median_Bill_Rate__c =  58.585);
		insert rvtdata;								

        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;  
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Account newClientAcc = new Account(Name='Test Client',ShippingStreet = 'Address St',ShippingCity='City', ShippingCountry='USA');
        Database.insert(newClientAcc);
        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'Aerotek CE',
                                    Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
                                    Jobcode_Id__c  ='700009',  OpCo_Id__c='ONS', Segment_Id__c  ='A_AND_E',  Segment__c = 'Architecture and Engineering',
                                    OpCo_Status__c = 'A', Skill_Id__c  ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity' , Accountid = newClientAcc.id, Req_Total_Positions__c = 12,
                RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='TEKsystems, Inc.',BusinessUnit__c ='EASI',
                stagename = 'Draft',Impacted_Regions__c='TEK APAC', Access_To_Funds__c = 'abc', CloseDate = system.today()+1,
                Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,
                Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,
                Req_Pay_Rate_Min__c = 1,Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly', Req_Total_Filled__c = 3, 
                Req_Total_Lost__c = 2 , Req_Total_Washed__c = 2,Legacy_Product__c = prd.id,				
				Req_RVT_Occupation_Code__c = '17-100 Mechanical Engineer',
				Req_RVT_Occupation_Code_Confidence__c = 0.80,
				Metro__c = 'Boise City',
				Req_Job_Level__c = 'Expert Level' );
			
        
		lstOpportunity.add(NewOpportunity);
		
		insert lstOpportunity;
        
        List<CRM_OpportunityToRVTData.DataContainer> dcList = new List<CRM_OpportunityToRVTData.DataContainer>();		
		CRM_OpportunityToRVTData.DataContainer dc = new CRM_OpportunityToRVTData.DataContainer ();
		dc.reqJobLevel = lstOpportunity[0].Req_Job_Level__c;
		dc.metro = lstOpportunity[0].metro__c;
		dc.reqRvtOccupationCode = lstOpportunity[0].Req_RVT_Occupation_Code__c;
		dc.recordId = lstOpportunity[0].Id;
		dc.startDate = lstOpportunity[0].CreatedDate;
		dc.ownerId = lstOpportunity[0].OwnerId;

		dcList.add(dc);
		
		
		CRM_OpportunityToRVTData.fetchRVTData(dcList);
        dcList[0].startDate = System.Now();
        CRM_OpportunityToRVTData.fetchRVTData(dcList);
    }
}