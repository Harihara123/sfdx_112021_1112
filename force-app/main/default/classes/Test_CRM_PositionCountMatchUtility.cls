@isTest(seealldata = true)
public class Test_CRM_PositionCountMatchUtility {

    static testmethod void Test_washPositions(){
        
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test0071';
        opp1.CloseDate= System.Today();
        opp1.StageName='Draft';
        opp1.RecordTypeId = ReqRecordType;
        opp1.Req_Total_Positions__c = 4;
        insert opp1;                
                                   
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        oppMap.put(opp1.Id, opp1);
                
        Test.StartTest();
        CRM_PositionCountMatchUtility.washPositions(oppMap);
        System.assertEquals(4, opp1.Req_Total_Positions__c);
        Test.stopTest();
               
    } 
}