@isTest
public class Test_CRM_ReqMetrics {
    static testMethod void Test_mapReqMetrics(){
        Test.StartTest();
        
        Account acnt=TestData.newAccount(1,'Client');
        insert acnt;
        Opportunity opp=TestData.newOpportunity(acnt.id,1);
        //string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        insert opp;        
        
        Map<String,Object> metObject = new Map<String,Object>();
        metObject.put('Skill_Speciality_From_Model__c','Development');
        metObject.put('F_NF_From_Model__c','Functional');
        metObject.put('SOCONET__c','15-1299.09');
        metObject.put('Job_Title__c','test analyst');
        metObject.put('Job_Description__c','technical lead and analyst with lead manager');
        metObject.put('OpptyId__c',opp.Id);
        
        List<Req_metrics__c> saveMetricList=CRM_ReqMetrics.mapReqMetrics(metObject);
        system.assertEquals(saveMetricList.size()>0, true);
        
        String reqMetricsData='{"sobjectType":"Req_metrics__c","Skill_Speciality_From_Model__c":"Development","F_NF_From_Model__c":"Functional","SOCONET__c":"15-1299.09","Job_Title__c":"test analyst","Job_Description__c":"technical lead and analyst with lead manager","OpptyId__c":"'+opp.Id+'"}';
        CRM_ReqMetrics.saveReqMetrics(reqMetricsData);
        Test.StopTest();       
    }
}