@isTest
public class Test_CRM_UtilityClass {
	static testMethod void testmethod1() {
        Test.startTest();
        String str = CRM_UtilityClass.describeAvailableQuickAction('New_Task');
        CRM_UtilityClass.retrieveUserTaskPreferences('Task');
        CRM_UtilityClass.retrieveThisRecordValues('UserPreference','','');
        CRM_UtilityClass.retrieveRelatedRecords('','UserPreference','','');
        
        Id objectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Req').getRecordTypeId();
        CRM_UtilityClass.isRecordTypeAvailable(String.valueOf(objectRecordTypeId),'Opportunity');
       // CRM_UtilityClass.retrieveSearchLayout('Opportunity');
        
        Account acc=TestData.newAccount(1);
        insert acc;
        //CRM_UtilityClass.performSearch('TESTACCT','Account','','Phone = \'2345\'',1);
        
        String obj='Opportunity';
        List<String> referenceTo=new List<String>();
        referenceTo.add(obj);
        CRM_UtilityClass.getRecursiveFieldAccess(referenceTo,referenceTo);
        String obj2='Account';
        referenceTo.add(obj2);
         CRM_UtilityClass.getRecursiveFieldAccess(referenceTo,referenceTo);
        system.assert(str != NULL);
        Test.stopTest();
       
    }    
    
    static testMethod void testmethod2() {
        
        CRM_UtilityClass.saveResultList sR = new CRM_UtilityClass.saveResultList();
        sR.errorMsg ='Error';
        
        CRM_UtilityClass.searchColumns sC = new CRM_UtilityClass.searchColumns();
        sC.field='A';
        sC.format='B' ;
        sC.Label='Label';
        sC.Name='cName';
        
        CRM_UtilityClass.searchResult sRes = new CRM_UtilityClass.searchResult();
        sRes.errorMsg= 'Error msg';
        sRes.label='Label';
        sRes.limitRows='rows';
        sRes.objectType='types';    
        
        String responseJSON = '{ "statusCode": "400 - Success", "statusMessage": "Request Successfully Processed"}';
        String contentType = 'application/json';
        Integer statusCode = 400;
        
        calloutMockResponse mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        Test.setMock(HttpCalloutMock.class, mkResponse);
        
        mkResponse = new calloutMockResponse(responseJSON,contentType,statusCode);
        
        Test.startTest();
        String str1 = CRM_UtilityClass.retrieveSearchLayout('Opportunity');
        system.assert(str1!=NULL);
        Test.stopTest();
    }
    static testMethod void utilityTestMethod3() {
        Account acc=TestData.newAccount(1);
        insert acc;
        string jsonString;
		jsonString = JSON.Serialize(acc);
        Test.startTest();
        CRM_UtilityClass.saveThisRecord('Account',jsonString,'Account','Update');
        CRM_UtilityClass.saveThisRecord('Account',jsonString,'Account','insert');        
        string str2 = CRM_UtilityClass.performSearch('','Account','DUNS__c,OwnerId.Name',null,1);
        system.assert(str2 !=NULL);
        Test.stopTest();
    }
}