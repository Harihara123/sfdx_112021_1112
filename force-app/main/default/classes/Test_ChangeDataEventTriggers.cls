@isTest
public class Test_ChangeDataEventTriggers {
    
    public testMethod static void Method1(){
        
        CDC_Data_Export_Flow__c serviceSettings = new CDC_Data_Export_Flow__c(name ='DataExport',
                                                                            Change_Event_Topic_Name__c='ingest-digestx',
                                                                            Data_Export_All_Fields__c=true,
                                                                            Sobject_Topic_Name__c ='topic x'
                                                                         );
        
        insert serviceSettings;
        
         Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
        
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        TestData TdAcc = new TestData(1);
        
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        insert talentAcc;
        
        Contact talentCon = TestData.newContact(talentAcc.id, 1, 'Talent');
        insert talentCon;        
        
        Test.enableChangeDataCapture();        
            Account clientAcc = TestData.newAccount(1, 'Client');
            insert clientAcc;
            Test.getEventBus().deliver();
            Contact con = TestData.newContact(clientAcc.id, 1, 'Recruiter');
            Insert con; 
            Test.getEventBus().deliver();
        
            //Contact c1 = [select id,firstName from Contact where id=:con.Id];
            con.FirstName ='firstName';
        	con.PrefCentre_Aerotek_OptOut_Email__c = 'Yes';
        	con.PrefCentre_Aerotek_OptOut_Mobile__c = 'Yes';
            update con;
            Test.getEventBus().deliver();
        
        
            String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

            Opportunity newOpp = new Opportunity();
            newOpp.Name = 'New ReqOpportunities';
            newOpp.Accountid = clientAcc.Id;
            newOpp.RecordTypeId = reqRecordTypeID;
            Date closingdate = system.today();
            newOpp.CloseDate = closingdate.addDays(25);
            newOpp.StageName = 'Draft';
            newOpp.Req_Total_Positions__c = 2;
            newOpp.Req_Job_Title__c = 'Salesforce Architect';
            newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
            newOpp.Req_Skill_Details__c = 'Salesforce';
            newOpp.Req_Job_Description__c = 'Testing';
            newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
            newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
            newOpp.Req_Bill_Rate_Max__c = 300000;
            newOpp.Req_Bill_Rate_Min__c = 100000;
            newOpp.Currency__c = 'USD - U.S Dollar';
            newOpp.Req_EVP__c = 'test EVP';
            newOpp.Req_Job_Level__c = 'Expert Level';
            newOpp.Req_Rate_Frequency__c = 'Hourly';
            newOpp.Req_Duration_Unit__c = 'Year(s)';
            newOpp.Req_Qualification__c = 'Additional Skill - database';
            newOpp.Req_Hiring_Manager__c = con.Name;
            newOpp.Opco__c = 'TEKsystems';
            newOpp.BusinessUnit__c = 'Board Practice';
            newOpp.Req_Product__c = 'Permanent';
            newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
            newOpp.Req_Worksite_Street__c = '987 Hidden St';
            newOpp.Req_Worksite_City__c = 'Baltimore';
            newOpp.Req_Worksite_Postal_Code__c = '21228';
            newOpp.Req_Worksite_Country__c = 'United States';
            newOpp.Req_Duration_Unit__c = 'Day(s)';
            insert newOpp;
            Test.getEventBus().deliver();
        
            Job_Posting__c jp = new Job_Posting__c();
            jp.Job_Title__c  = 'Test Administrator';
            jp.Skills__c = 'Core Java and Python';
            jp.Allegis_Employment_Type__c = 'Contract';
            jp.Industry_Sector__c ='Accounting';
            jp.External_Job_Description__c = 'Test Administrator'; 
            jp.Opportunity__c = newOpp.Id;
            jp.Status_Id__c=12280.0;
            insert jp;
            Test.getEventBus().deliver();
        
            Talent_Work_History__c twh = new Talent_Work_History__c(
            Start_Date__c = System.today(),
            End_Date__c = System.today() + 5,
            Talent_Recruiter__c = 'peoplesoftId',
            Talent_Account_Manager__c = 'peoplesoftId2',
            Job_Title__c = 'Green Building Architect',
            Main_Skill__c='Green Building',
            Talent__c = talentAcc.Id,
            SourceCompany__c='APPLUS TECHNOLOGIES INC',
            SourceCompanyId__c='270255');
            insert twh;
            Test.getEventBus().deliver();
        
            Talent_Experience__c texpedu = new Talent_Experience__c(
                Current_Assignment__c = false,
                Notes__c='Testing Education ',
                PersonaIndicator__c ='Recruiter',
                Talent__c=talentAcc.Id,
                Graduation_Year__c   = '2019',
                Allegis_Placement__c = false,
                Degree_Level__c='Diploma',
                Honor__c='Communication',
                Major__c='Electronics Communications',
                Organization_Name__c='SRM University',
                Talent_Committed_Flag__c=true,
                Type__c='Education'
                 );
        
            insert texpedu;
            Test.getEventBus().deliver();
        
            Talent_Document__c talentDocument = new Talent_Document__c();
            talentDocument.Talent__c = talentAcc.Id;
            talentDocument.Document_Name__c = 'test.txt';
            talentDocument.Document_Type__c = 'Resume'; 
            talentDocument.Default_Document__c = false;
            talentDocument.Internal_Document__c = true;
            talentDocument.Committed_Document__c = true;
            talentDocument.Mark_For_Deletion__c =  false;
            insert talentDocument;
            Test.getEventBus().deliver();
        
            Event ev =new Event(Whoid = talentCon.id, 
                            Whatid = talentAcc.id,
                            Subject = 'Out of Office', 
                            Type='Out of Office',
                            startDateTime =  system.Now(),
                             EndDateTime =   system.Now().addMinutes(10),
                            ActivityDate =  system.today(),
                            Pre_Meeting_Notes__c='Testing',
                            DurationInMinutes  = 10);
            insert ev;
            Test.getEventBus().deliver();
        
            Task t = new Task(Whoid = talentCon.id, 
                            Whatid = talentAcc.id,
                            Subject = 'Call', 
                            Type='Call',
                            ActivityDate =  system.today(),
                            Pre_Meeting_Notes__c='Testing'
                            );
            insert t;
            Test.getEventBus().deliver();
        
            Reqs__c rq = new Reqs__c();
            rq.Account__c= clientAcc.Id;
            rq.Max_Salary__c=100;
            rq.Min_Salary__c=75;
            rq.Bill_Rate_Max__c=100;
            rq.Bill_Rate_Min__c=75;
            rq.Positions__c=10;
            rq.Filled__c=2;
            rq.Wash__c=0;
            insert rq;
            Test.getEventBus().deliver();
            
            Tag_Definition__c td = new Tag_Definition__c();
            insert td;
            Test.getEventBus().deliver();
        
            Contact_Tag__c ctag = new Contact_Tag__c();
            ctag.Contact__c=talentCon.Id;
            ctag.Tag_Definition__c=td.id;
            insert ctag;
            Test.getEventBus().deliver();
        
            
        
            OpportunityContactRole ocr = new OpportunityContactRole();
            ocr.OpportunityId= newOpp.Id;
            ocr.Role='member';
            ocr.ContactId= con.Id;
            insert ocr;
            Test.getEventBus().deliver();
        
            Talent_Recommendation__c tr = new Talent_Recommendation__c();
            tr.Talent_Contact__c=talentCon.Id;
            insert tr;
            Test.getEventBus().deliver();
        
        	//for delete contact event
        	delete con; 
            Test.getEventBus().deliver();
        
    }
    public static testMethod void method2(){
        CDC_Data_Export_Flow__c serviceSettings = new CDC_Data_Export_Flow__c(name ='DataExport',
                                                                            Change_Event_Topic_Name__c='ingest-digestx',
                                                                            Data_Export_All_Fields__c=true,
                                                                            Sobject_Topic_Name__c ='topic x'
                                                                         );
        
        insert serviceSettings;
         Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
        
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Account> accList = new List<Account>();
        
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
       
        List<Contact> talentConList = new List<Contact>();
        for(Integer i=0; i<10 ; i++) {
            Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
            ct.Other_Email__c = 'other'+i+'@testemail.com';
            ct.Title = 'test title'+i;
            ct.MailingState = 'test text'+i;
            
            talentConList.add(ct);
        }
        insert talentConList;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        insert opp1;
        
        
        Test.enableChangeDataCapture(); 
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = talentConList[0].Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Applicant';
        o.EffectiveDate = System.today();
        insert o;
        Test.getEventBus().deliver();
        
        
    }
    
    public static testMethod void method3(){
        CDC_Data_Export_Flow__c serviceSettings = new CDC_Data_Export_Flow__c(name ='DataExport',
                                                                            Change_Event_Topic_Name__c='ingest-digestx',
                                                                            Data_Export_All_Fields__c=true,
                                                                            Sobject_Topic_Name__c ='topic x'
                                                                         );
        
        insert serviceSettings;
         Connected_Data_Export_Switch__c serviceSetting2 = new Connected_Data_Export_Switch__c(Name='DataExport',
                                                                                                   PubSub_URL__c='Https://www.google.com');
        insert serviceSetting2;
        
        Test.enableChangeDataCapture();  
        User usr = TestDataHelper.createUser('System Integration');
            insert usr;
            User u1 =[Select id,firstName from User where Id=: usr.Id];
            u1.FirstName='CDCTest';
            update u1;
            Test.getEventBus().deliver();
            
        
            User_Search_Feedback__c usf = new User_Search_Feedback__c();
            insert usf;
            Test.getEventBus().deliver();
        
            User_Organization__c uo = new User_Organization__c();
            insert uo;
            Test.getEventBus().deliver();
        
            Pipeline__c pl = new Pipeline__c();
            insert pl;
            Test.getEventBus().deliver();
            
            Job_Title__c jt = new Job_Title__c();
            jt.Active__c=true;
            jt.Name ='Java';
            insert jt;
            Test.getEventBus().deliver();
        
        
            User_Activity__c ua = new User_Activity__c();
            insert ua;
            Test.getEventBus().deliver();
        
            Search_Alert_Criteria__c sac = new Search_Alert_Criteria__c();
            sac.Alert_Criteria__c='criteria';
            sac.Alert_Frequency__c='Daily';
            sac.Alert_Trigger__c='Talent Created';
            insert sac;
            Test.getEventBus().deliver();

            Careersite_Application_Details__c detailObject = new Careersite_Application_Details__c();
            detailObject.OPCO__c = 'Easi';
            detailObject.Application_Source__c ='Phenom';
            detailObject.Application_Date_Time__c = System.now();  
            detailObject.Adobe_Ecvid__c = '434';
            detailObject.Posting_Id__c = '6959608';
            detailObject.Vendor_Application_Id__c = 'JA2H1YB7187QBTGGXFN3';
            detailObject.Retry_Status__c ='PENDING';
            detailObject.Application_Status__c ='PENDING';
            insert detailObject;
            Test.getEventBus().deliver();
        
            Global_LOV__c gv = new Global_LOV__c();
            gv.LOV_Name__c='TestLov';
            gv.Source_System_Id__c='cvbnmlj';
            insert gv;
            Test.getEventBus().deliver();
        
            gv.Source_System_Id__c='xyz';
            update gv;
            Test.getEventBus().deliver();
        
            delete gv;
        
        
    }
}