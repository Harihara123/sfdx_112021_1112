/*******************************************************************
* Name  : Test_Comm_Team_Memb_Trigger
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 25, 2015
* Details : Test class for Comm_Team_Memb_Trigger
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
Nidish                        6/18/2015                     added TestDataHelper.createOACaculatorData(); to line 25
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata=false)
private class Test_Comm_Team_Memb_Trigger {
   // static User user = TestDataHelper.createUser('Aerotek AM');
    static User user = TestDataHelper.createUser('System Administrator'); 
    static testMethod void testComm_Team_Memb_Trigger() {
        List<Target_Account__c> lsttargetAccounts = new List<Target_Account__c>();
        List<opportunity> lstOpportunity = new List<opportunity>();
        List<Commission_Team_Member__c> lstCommissionTeamMember = new List<Commission_Team_Member__c>();
        
        Test.startTest(); 
        
        if(user != Null) {
            system.runAs(user) {
                TestData TdAcc = new TestData(10);
                TestDataHelper.createOACaculatorData();
                Comm_Team_Memb_TriggerHandler handler = new Comm_Team_Memb_TriggerHandler();
                for(Account Acc : TdAcc.createAccounts()) {                     
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                        Formal_Decision_Criteria__c = 'Defined', 
                        Response_Type__c = 'RFI' , stagename = 'Interest', 
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1);  
                        
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }  
                // insert opportunity
                insert lstOpportunity;
                
                for(integer i=0;i<100;i++) {
                    Commission_Team_Member__c Commission = new Commission_Team_Member__c(
                    Commission__c = 1, Opportunity_Name__c = lstOpportunity[0].id, Role__c = 'BDM', Team_Member__c = user.id);
                    lstCommissionTeamMember.add(Commission);
                }  
                try { 
                // Insert Commission Team member             
                insert lstCommissionTeamMember; 
                //adding error messages
                handler.addErrorMessages(lstCommissionTeamMember);
                //delete the Commission Member 
                delete lstCommissionTeamMember;
                //System.assertequals(lstCommissionTeamMember==null);
                } catch(Exception e) {
                    system.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION,e.getDmlType(0));
                 }                                         
            }
        }  
      
    }
}