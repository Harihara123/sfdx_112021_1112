@isTest
public with sharing class Test_ConnectedToDRZCallableInterface implements HttpCalloutMock {
  
   public HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(searchResopnse());
        res.setStatusCode(200);  
        System.debug('Respond to Request - ' + req.getEndpoint());
        return res;
  }

  private static string searchResopnse(){

    String jsonResponse;
    jsonResponse = '{\r\n \"_id\": \"00625000006sElWAAU\",\r\n   \"_index\": \"ats-job-master-20180123.jan18-v2\",\r\n   \"_score\": 13.801794,\r\n   \"_source\": null,\r\n   \"city_name\": \"PHILADELPHIA\",\r\n   \"country_code\": \"UNITED STATES\",\r\n   \"country_sub_division_code\": \"PA\",\r\n   \"duration\": null,\r\n   \"duration_unit\": null,\r\n   \"education_requirement\": null,\r\n   \"geolocation\": {\r\n      \"lon\": -76.7661,\r\n      \"lat\": 39.5157\r\n   },\r\n   \"job_category_code\": null,\r\n   \"job_create_date\": \"2018-01-29\",\r\n   \"job_id\": \"00625000006sElWAAU\",\r\n   \"opco_name\": \"Allegis Partners, LLC\",\r\n   \"opportunity_name\": \"REQ - Test Sudheer Parent Test - group tester\",\r\n   \"organization_account_id\": \"1-1016VMO\",\r\n   \"organization_id\": \"0012400000pqWihAAE\",\r\n   \"organization_name\": \"Test Sudheer Parent Test\",\r\n   \"owner_id\": \"00524000005q3agAAA\",\r\n   \"owner_name\": \"Mark Brady\",\r\n   \"owner_psid\": \"Dataload78\",\r\n   \"position_description\": \"test description\",\r\n   \"position_end_date\": null,\r\n   \"position_id\": \"O-0823916\",\r\n   \"position_offering_type_code\": \"Permanent\",\r\n   \"position_open_quantity\": \"1\",\r\n   \"position_start_date\": \"2018-01-23\",\r\n   \"position_street_name\": \"TEST STREET\",\r\n   \"position_title\": \"group tester\",\r\n   \"postal_code\": \"21071\",\r\n   \"practice_area\": null,\r\n   \"qualifications_summary\": null,\r\n   \"remuneration_basis_code\": \"--None--\",\r\n   \"remuneration_bill_rate_max\": 0,\r\n   \"remuneration_bill_rate_min\": 0,\r\n   \"remuneration_interval_code\": null,\r\n   \"remuneration_pay_rate_max\": 0,\r\n   \"remuneration_pay_rate_min\": 0,\r\n   \"remuneration_salary_max\": 0,\r\n   \"remuneration_salary_min\": 0,\r\n   \"req_stage\": \"Interviewing\",\r\n   \"search_tags\": null,\r\n   \"skills\": \"\"\r\n}' ;
    
        return jsonResponse;
  }
  
  
     static testMethod void validateMatchJobs(){

    String queryString = 'size=30&from=0&advanced=true&explainHits=full&semanticSkills=false&type=talent2job&flt.group_filter=AGS%2CRPO%2CMLA%2CAP%2CAG_EMEA&docId=00124000016EQUCAA4&facetFlt.req_stage=draft%7Cqualified%7Cpresenting%7Cinterviewing%7Cclosed%20won%20(partial)&facetOption.req_stage=true&facetOption.owner_name=false&facetOption.organization_name=false&facetOption.job_skills=false&facetOption.opco_name=false&facetOption.opco_business_unit=false'; 
        String location = '{\"location\":{\"lat\":\"39.20371439999999\",\"lng\":\"-76.86104619999999\"}}'; 
    String requestBody = '{\r\n   \"params\": {\r\n      \"advanced\": \"true\",\r\n      \"docId\": \"00124000016EQUCAA4\",\r\n      \"explainHits\": \"full\",\r\n      \"facetFlt.req_stage\": \"draft|qualified|presenting|interviewing|closed won (partial)\",\r\n      \"facetOption.job_skills\": \"false\",\r\n      \"facetOption.opco_business_unit\": \"false\",\r\n      \"facetOption.opco_name\": \"false\",\r\n      \"facetOption.organization_name\": \"false\",\r\n      \"facetOption.owner_name\": \"false\",\r\n      \"facetOption.req_stage\": \"true\",\r\n      \"flt.group_filter\": \"AGS,RPO,MLA,AP,AG_EMEA\",\r\n      \"from\": \"0\",\r\n      \"index\": \"job\",\r\n      \"semanticSkills\": \"false\",\r\n      \"size\": \"30\",\r\n      \"type\": \"talent2job\"\r\n   }\r\n}';

        setOauthToken('Match to Req Service POST');
        setOauthToken('Match to Req Service GET');
        
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockSearchController());

    List<String> httptypeList = new List<String>();
    
      
     Map<String,Object> args = new Map<String,Object>();
     args.put('queryString',queryString);
     args.put('httpRequestType','POST');
     args.put('requestBody',requestBody);
     args.put('location',location);
     
     Callable callableSearchController = (Callable) Type.forName('Drz_MatchtalentCallble').newInstance(); 
        
    
    Test.stopTest();
  }
  
  
  private static void setOauthToken(String serviceName){
    ApigeeOAuthSettings__c apigeeOauth = new ApigeeOAuthSettings__c();
    apigeeOauth.Name = serviceName;
    apigeeOauth.Client_Id__c = '1QYcoEvo4ueqvG42PyHP7TE9TxjeuKJM';
    apigeeOauth.Service_Http_Method__c = 'GET'; 
    apigeeOauth.Service_URL__c = 'https://api-lod.allegistest.com/v1/search/ats/'; 
    apigeeOauth.Token_URL__c = 'https://api-lod.allegistest.com/v1/oauth/tokens';
    apigeeOauth.OAuth_Token__c = 'MyJ1VU9hG1OgVPWxe3GnTjQeQ75u'; 

    insert apigeeOauth; 
  }

}