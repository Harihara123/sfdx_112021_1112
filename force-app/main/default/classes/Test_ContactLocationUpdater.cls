@isTest
public class Test_ContactLocationUpdater {
	static testMethod void validateContactLocationUpdater() {
        Test.startTest();

        Account testAccount = CreateTalentTestData.createTalent();

		ContactLocationUpdater locUpdater = new ContactLocationUpdater('SELECT Id, Talent_State_Text__c, Talent_Country_Text__c, MailingState, MailingCountry FROM Contact where RecordType.Name=\'Talent\' AND AccountId=\'' + testAccount.Id + '\'');
        Database.executeBatch(locUpdater);
        
        Test.stopTest();
    }
}