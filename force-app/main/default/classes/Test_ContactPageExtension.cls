@isTest(seeAlldata= true)
private class Test_ContactPageExtension {
static testMethod void test_ConPageExtension() {
        User usr = TestDataHelper.createUser('System Administrator');
       
        //getting record type info
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact;
       //create account
        Account acc = TestDataHelper.createAccount();//name= Test Account;
        insert acc;
        // create Contact 
        Contact con1 = TestDataHelper.createContact();
        insert con1;
    contact con2=TestDataHelper.createContact();
    
        
        System.runAs(usr){
        Test.startTest();
            
           // PageReference PageRef = new PageReference ('/apex/Contact_New_Page_Layout?retURL=%2F'+acc.ID+'&accid='+acc.ID+'&RecordType=012U0000000DWoy&ent=Contact&save_new=1&sfdc.override=1');
            PageReference PageRef = Page.Contact_New_Page_Layout;
            PageRef.getParameters().put('accid', acc.ID);
            PageRef.getParameters().put('saveAndNew', 'save');
            system.debug('!!!!!!!!!!!!!!!!' +PageRef);
            Test.setCurrentPage(PageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(con1);
            ContactPageExtension ctrl = new ContactPageExtension(sc);
            pagereference pg = ctrl.Save1();
            //System.assertNotEquals(null,pg);
            
          Test.stopTest();
            
            
            PageReference PageRef0 = Page.Contact_New_Page_Layout;
            PageRef.getParameters().put('accid', acc.ID);
            PageRef0.getParameters().put('saveAndNew', 'save');
            system.debug('!!!!!!!!!!!!!!!!' +PageRef0);
            Test.setCurrentPage(PageRef0);
            ApexPages.StandardController sc0 = new ApexPages.StandardController(con2);
            ContactPageExtension ctr0 = new ContactPageExtension(sc0);
            pagereference pg0 = ctr0.Save1();
         
         
           
            PageReference PageRef2 = Page.Contact_New_Page_Layout;
            PageRef.getParameters().put('accid', NULL);
            PageRef2.getParameters().put('saveAndNew', 'save');
            system.debug('!!!!!!!!!!!!!!!!' +PageRef);
            Test.setCurrentPage(PageRef);
            ApexPages.StandardController sc2 = new ApexPages.StandardController(con1);
            ContactPageExtension ctrl2 = new ContactPageExtension(sc2);
            pagereference pg2 = ctrl2.Save1();
            
            //System.assertNotEquals(null,pg2);
        }
         
    }        
}