@isTest
public class Test_ConvertCurrencyISOCode
{
    static testMethod void TestCurrencyTypeCustom() 
    {        
        User user = TestDataHelper.createUser('System Administrator');
        List<Opportunity> lstOpportunity = new List<Opportunity>();        
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) 
        {
            system.runAs(user) 
            {    
                 CurrencyISOCodeMappings__c cs = new CurrencyISOCodeMappings__c();
                cs.name = 'EUR – Euro';
                cs.Actual_ISO_Code__c = 'EUR';
                insert cs;
                
                CurrencyISOCodeMappings__c cs1 = new CurrencyISOCodeMappings__c();
                cs1.name = 'EUR - Euro';
                cs1.Actual_ISO_Code__c = 'EUR';
                insert cs1;
                
                CurrencyType__c ct = new CurrencyType__c();
                ct.IsoCode__c = 'EUR';
                ct.ConversionRate__c = 1;
                ct.DecimalPlaces__c = .5;
                insert ct;
                
                Map<Id,String> mapOpenOpportunitiesWithCurrency = new Map<Id,string>();
                mapOpenOpportunitiesWithCurrency.put(cs.Id,'EUR – Euro');
                
                
                //ConvertCurrencyISOCode obj = new ConvertCurrencyISOCode();
                ConvertCurrencyISOCode.getMapCurrencyTypeCustom(mapOpenOpportunitiesWithCurrency);
            }
            
        }
        
    }
    
    static testMethod void TestMapDatedCurrencyTypeCustom() 
    {        
        User user = TestDataHelper.createUser('System Administrator');
        List<Opportunity> lstOpportunity = new List<Opportunity>();        
        
        // insert OA Calculator Data
        TestDataHelper.createOACaculatorData();
        Test.startTest(); 
       
        if(user != Null) 
        {
            system.runAs(user) 
            {    
                 CurrencyISOCodeMappings__c cs = new CurrencyISOCodeMappings__c();
                cs.name = 'EUR – Euro';
                cs.Actual_ISO_Code__c = 'EUR';
                insert cs;
                
                CurrencyISOCodeMappings__c cs1 = new CurrencyISOCodeMappings__c();
                cs1.name = 'EUR - Euro';
                cs1.Actual_ISO_Code__c = 'EUR';
                insert cs1;
                
                DatedConversionRate__c ct = new DatedConversionRate__c();
                ct.IsoCode__c = 'EUR';
                ct.ConversionRate__c = 1;
                ct.StartDate__c = System.today() -15;
                ct.NextStartDate__c = System.today() +15;
                insert ct;
                
                Map<String,Date> mapDateWithCurrency = new Map<String,Date>();
                mapDateWithCurrency.put('EUR – Euro',System.Today());
                
                Map<Id,map<string,date>> mapClosedOpportunitiesWithCurrencyAndDate = new Map<Id,map<string,date>>();
                mapClosedOpportunitiesWithCurrencyAndDate.put(cs.Id,mapDateWithCurrency);
                
                //ConvertCurrencyISOCode obj = new ConvertCurrencyISOCode();
                ConvertCurrencyISOCode.getMapDatedCurrencyTypeCustom(mapClosedOpportunitiesWithCurrencyAndDate);
            }
            
        }
        
    }
}