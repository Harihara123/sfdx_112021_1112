/*******************************************************************
* Name  : Test_CreateMultipleReqsController
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 14, 2015
* Details : Test class for CreateMultipleReqsController
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_CreateMultipleReqsController {

    //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');    
    static testMethod void Test_CreateMultipleReqs() {   
         
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id);
        Account_National_Owner_Fields__c cusSett = new Account_National_Owner_Fields__c();
        cusSett.Name = 'Aerotek_SSO_Owner__c';
        insert cusSett ;
        Product2 testProduct=TdAcc.createTestProduct();
        insert testProduct;
        Test.startTest();    
        if(user != Null) {
            Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
            insert newreq;                                 
            
            newreq.Primary_Contact__c = lstNewContact.id;
            newreq.Account__c = lstNewAccounts[0].Id;
            newreq.National_Account_Manager__c = userinfo.getuserid();
            newReq.Filled__c = 10;
            newReq.Bill_Rate_Max__c = 50; 
            newReq.Bill_Rate_Min__c = 25;
            newReq.Standard_Burden__c = 2;
            newReq.Duration__c = 5;
            newReq.Duration_Unit__c = 'days';
            newReq.Job_category__c = 'admin';
            newReq.Req_priority__c = 'green';
            newReq.Pay_Rate_Max__c = 10;
            newReq.Pay_Rate_Min__c = 5;
            newReq.Job_description__c = 'test';
            newReq.Organizational_role__c = 'Centre Manager';
            newReq.Product__c= testProduct.Id;
            newReq.stage__c =  'Qualified';
      
            update newreq;
            
            // Call CreateMultipleReqsController             
            ApexPages.currentPage().getParameters().put('id',string.valueof(newreq.id));
            CreateMultipleReqsController Reqmultiple = new CreateMultipleReqsController();
            Reqmultiple.saveReqs();
            System.assert(Reqmultiple.reqsToCreate!=null);
            newReq.Req_Origination_System__c='SIEBEL';
            update newreq;
             // Call CreateMultipleReqsController             
            ApexPages.currentPage().getParameters().put('id',string.valueof(newreq.id));
            Reqmultiple = new CreateMultipleReqsController();
            Reqmultiple.saveReqs();
            System.assert(Reqmultiple.reqsToCreate!=null);
            
            newReq.Req_Origination_System__c='Fieldglass';
            update newreq;
             // Call CreateMultipleReqsController             
            ApexPages.currentPage().getParameters().put('id',string.valueof(newreq.id));
            Reqmultiple = new CreateMultipleReqsController();
            Reqmultiple.saveReqs();
            System.assert(Reqmultiple.reqsToCreate!=null);
            Reqmultiple.cancel();
        }
        Test.stopTest();
    }
   
    
    
    
}