@isTest(seeAlldata= false)
private class Test_CreatePosLog {
    final static string SYSTEM_SOURCEREQ = 'SIEBEL';
    final static string FIELDGLASS_SOURCEREQ = 'Fieldglass';
    final static string SFDC_SOURCEREQ = 'SALESFORCE';
    static testMethod void test_CreatePosLog() {
        
        //Create an Account record with siebel Id.
        Account NewAccounts = new Account(
            Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts;
        system.assertnotequals(NewAccounts.Siebel_ID__c,null);
         //Create an Account for update to Req__c.
        Account NewAccounts2 = new Account(
            Name = 'TESTACCT2',Siebel_ID__c = '1-93WP-1',
            Phone= '234567',ShippingCity = 'Testshipcity12',ShippingCountry = 'Testshipcountry12',
            ShippingPostalCode = 'TestshipCode24',ShippingState = 'Testshipstate23',
            ShippingStreet = 'Testshipstreet12',BillingCity ='TestBillstreet12',
            BillingCountry ='TestBillcountry12',BillingPostalCode ='TestBillCode12',
            BillingState ='TestBillState12',BillingStreet ='TestBillStreet12',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts2;
                Contact con = new Contact(LastName='Test',Email='abc@xyz.com', AccountId = NewAccounts2.id);
                 //assign the recruiter record type id
                 con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Recruiter').getRecordTypeId();
                 con.Contact_Active__c = true;
                 insert con;
        
        Test.startTest();
        
        //Create a Req record
        Reqs__c recRequisition = new Reqs__c(
        Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,SourceReq__c = 'R-101123',
        Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition;
        //Create a Req record
        Reqs__c recRequisition1 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1122', Max_Salary__c = 5000.00, Min_Salary__c = 2000.00,Duration__c = 22,SourceReq__c = 'R-101134',
        Job_Description__c = 'Sample test',  City__c = 'Sample test', state__c = 'Sample test', Zip__c = 'Sample test',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition1;
        
        recRequisition.Account__c = NewAccounts2.Id;
        recRequisition.Primary_Contact__c = con.Id;
        update recRequisition;
        delete recRequisition;
        undelete recRequisition;
        recRequisition1.Primary_Contact__c = con.Id;
        update recRequisition1;
        
        ReqsTriggerHandler handler = new ReqsTriggerHandler();
        List<Req_Team_Member__c> reqTeamsList = [SELECT ID FROM Req_Team_Member__c ];
        try{
            handler.addTeamMembersToCollection(reqTeamsList,'R-101123','R-101134',reqTeamsList);
        }catch(Exception e){
            System.debug('Exception is**'+e);
        }
        Test.stopTest();
    }
     static testMethod void test_ReqsTriggerrun2() {
        
        //Create an Account record with siebel Id.
        Account NewAccounts = new Account(
            Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert NewAccounts;
        system.assertnotequals(NewAccounts.Siebel_ID__c,null);
         //Create an Account for update to Req__c.
        Account NewAccounts2 = new Account(
            Name = 'TESTACCT2',Siebel_ID__c = '1-93WP-1',
            Phone= '234567',ShippingCity = 'Testshipcity12',ShippingCountry = 'Testshipcountry12',
            ShippingPostalCode = 'TestshipCode24',ShippingState = 'Testshipstate23',
            ShippingStreet = 'Testshipstreet12',BillingCity ='TestBillstreet12',
            BillingCountry ='TestBillcountry12',BillingPostalCode ='TestBillCode12',
            BillingState ='TestBillState12',BillingStreet ='TestBillStreet12',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts2;
        
        Contact con = new Contact(LastName='Test',Email='abc@xyz.com', AccountId = NewAccounts2.id);
                 //assign the recruiter record type id
                 con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Recruiter').getRecordTypeId();
                 con.Contact_Active__c = true;
                 insert con;
        
        
        
        //Create a Req record
        Reqs__c recRequisition = new Reqs__c(
        Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),close_date__c=date.today(),       
        Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,SourceReq__c = 'R-101123',
        Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition;
        //Create a Req record
        Reqs__c recRequisition1 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),close_date__c=date.today(),       
        Siebel_ID__c = 'TEST1122', Max_Salary__c = 5000.00, Min_Salary__c = 2000.00,Duration__c = 22,SourceReq__c = 'R-101134',
        Job_Description__c = 'Sample test',  City__c = 'Sample test', state__c = 'Sample test', Zip__c = 'Sample test',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition1;
        
        //Create a Req record
        Reqs__c recRequisition3 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 30, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(), close_date__c=date.today(),      
        Siebel_ID__c = 'TEST1133', Max_Salary__c = 7000.00, Min_Salary__c = 3000.00,Duration__c = 33,SourceReq__c = 'R-101135',
        Job_Description__c = 'Sample test33',  City__c = 'Sample test33', state__c = 'Sample test33', Zip__c = 'Sample test33',System_SourceReq__c = SFDC_SOURCEREQ);
        insert recRequisition3;
        
        //Create a Req record
        Reqs__c recRequisition4 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 40, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(), close_date__c=date.today(),       
        Siebel_ID__c = 'TEST1144', Max_Salary__c = 8000.00, Min_Salary__c = 5000.00,Duration__c = 44,SourceReq__c = 'R-101136',
        Job_Description__c = 'Sample test44',  City__c = 'Sample test44', state__c = 'Sample test44', Zip__c = 'Sample test44',System_SourceReq__c = FIELDGLASS_SOURCEREQ);
        insert recRequisition4;
        
        Test.starttest();
        
        recRequisition.Account__c = NewAccounts2.Id;
       recRequisition.wash__c=3;
       recRequisition.Loss__c=1;
       recRequisition.Filled__c=1;
 
        update recRequisition;
        recRequisition1.Primary_Contact__c = con.Id;
        recRequisition1.Filled__c=1;
        update recRequisition1;
        recRequisition3.Job_Description__c = 'test44Update';
        recRequisition3.Loss__c=1;
        update recRequisition3;
        recRequisition4.state__c = 'test44Update';
        recRequisition4.Loss__c=2;
        recRequisition4.Filled__c=2;
         recRequisition4.wash__c=2;
        update recRequisition4;
        
        recRequisition4.state__c = 'test44Update';
        recRequisition4.Loss__c=3;
        recRequisition4.Filled__c=3;
         recRequisition4.wash__c=3;
        update recRequisition4;
        
        
        //delete recRequisition;
        //undelete recRequisition;
        List<Req_Position_Log__c> RpLogList=new List<Req_Position_Log__c>();
         List<Req_Position_Log__c> NewRpLogList1=new List<Req_Position_Log__c>();
        set<id> rplogset=new set<id>();
        rplogset.add(recRequisition.id);
        rplogset.add(recRequisition1.id);
        rplogset.add(recRequisition3.id);
        
        
         RpLogList=[SELECT ID,Num_Changed_Positions__c FROM Req_Position_Log__c WHERE REQ__C in: rplogset  ];
         List<Req_Position_Log__c> rpl= [SELECT ID,Num_Changed_Positions__c FROM Req_Position_Log__c WHERE REQ__C =: recRequisition4.id   ];
         
         if(RpLogList.size()>0){
         DELETE RpLogList;
         
         }
         system.debug('rpl'+rpl);
         if(rpl.size()>0){
         delete rpl;
         }
         
   Req_Position_Log__c positionLog = new Req_Position_Log__c();
  positionLog.Position_Change_Type__c='Wash';
  positionLog.Req__c=recRequisition4.id;
   positionLog.Reason__c='';
   
   positionlog.Num_Changed_Positions__c=1;
   NewRpLogList1.add(positionLog);
   insert NewRpLogList1;
         
         CreatePosLog b = new CreatePosLog(); 
b.queryreq='select id,Close_Date__c,lastmodifieddate,Filled__c,Loss__c,Positions__c,Wash__c from reqs__c where id ='+'\''+recRequisition.id+'\'';

Id batchJobId = Database.executeBatch(b);

          CreatePosLog b1 = new CreatePosLog(); 
b1.queryreq='select id,Close_Date__c,lastmodifieddate,Filled__c,Loss__c,Positions__c,Wash__c from reqs__c where id ='+'\''+recRequisition4.id+'\'';

Id batchJobId1 = Database.executeBatch(b1);

         
        
        ReqsTriggerHandler handler = new ReqsTriggerHandler();
        List<Req_Team_Member__c> reqTeamsList = [SELECT ID FROM Req_Team_Member__c ];
        try{
            handler.addTeamMembersToCollection(reqTeamsList,'R-101123','R-101134',reqTeamsList);
        }catch(Exception e){
            System.debug('Exception is**'+e);
        }
        Test.stopTest();
    }
}