/*******************************************************************
* Name  : Test_Credit_App_Trigger
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 27, 2015
* Details : Test class for Credit_App_Trigger
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_Credit_App_Trigger {
    //variable declaration
   // static User user = TestDataHelper.createUser('Aerotek AM');
    static User user = TestDataHelper.createUser('System Administrator');
    static User testAdminUser= TestDataHelper.createUser('System Administrator');   
    static string ContactCSARecType = Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get(Label.Contact_CSA_Recordtype_Name).getRecordTypeId();
    static testMethod void testCredit_App() {
       List<Credit_App__c> lstnewCreditApp = new List<Credit_App__c>();
        List<Credit_App__c> lstnewCreditAppAgain = new List<Credit_App__c>();
        Test.startTest();  
        id orgRecordType;
        Schema.DescribeSObjectResult accRes = Schema.SObjectType.User_Organization__c;

        Map<String,Schema.RecordTypeInfo> rtMapByNameAcc = accRes.getRecordTypeInfosByName();
        
        if(rtMapByNameAcc.containsKey('Region')){
        
            orgRecordType = rtMapByNameAcc.get('Region').getRecordTypeId();
        
        }

        user.OPCO__c='test'; 
        user.Office_Code__c='abc';
        insert user;
        User_Organization__c org= new User_Organization__c(RecordTypeId=orgRecordType,Name='Test', Office_Code__c='abc',Active__c=true,
        Region_Name__c='test1',OpCo_Code__c='test');
        insert org;

        org.Region_Name__c='test2';
        org.OpCo_Code__c='test';
        update org;
        
        if(user != Null) {        
            system.runAs(user) {
                
                TestData Td = new TestData(10);

                for(Account acc : Td.createAccounts()) {
                    
                    Credit_App__c newCreditApp = new Credit_App__c(Account__c = acc.Id,
                    Level_of_Government__c = 'Federal',Is_this_project_subject_to_SCA_Davis__c = 'yes',
                    OpCo__c= 'Aerotek',
                    Organization_Office__c=org.Id , Project_Length__c=2, Region__c='Aerotek Central', Division__c='Aerotek CE',Term_Details__c='test',
                     Project_Length_Type__C='Day', No_of_Approved__c=0, 
                    Is_prime_or_subcontractor_holder__c = 'Prime',No_Of_Positions__c = 10,
                    Start_Date__c = system.today()+1, Expiration_Date__c = system.today()+2,Bill_Rate__c=5,
                    DP_Fee__c=100,
                    Monthly_Revenue__c=200,

                    Government_Agency_is_Customer_Support__c =  acc.Id);
                    
                    system.assertequals(newCreditApp.Start_Date__c <= newCreditApp.Expiration_Date__c,True);                    
                    
                    // Load Credit App into list                
                    lstnewCreditApp.add(newCreditApp);
                }   
                
                // Gulzar's fix
                    List<Account> accList = Td.createAccounts();
                    Contact ContObj = Td.createcontactwithoutaccount(accList[0].Id);
                    ContObj.recordtypeId = ContactCSARecType;
                    ContObj.Contact_Active__c = true;
                    update ContObj;
                    
                    for(Credit_App__c ca:lstnewCreditApp)
                    {
                        ca.CSA__c = ContObj.Id; 
                        lstnewCreditAppAgain.add(ca);  
                    }
                           
                
                // Insert list of Credit App                                          
                insert lstnewCreditAppAgain;
                update lstnewCreditAppAgain;
                delete lstnewCreditAppAgain;
                
                
            }
            system.runAs(testAdminUser) {
                delete org;
                undelete org;
            }
        }
        Test.stopTest(); 
    }
}