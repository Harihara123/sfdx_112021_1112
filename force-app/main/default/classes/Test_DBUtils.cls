/**
* Author: Akshay Konijeti
* Date: 12/20/2016
* UserStory: ATS-2754
* 
* Test Class for the Apex class DBUtils.
* 
*/
@isTest
public class Test_DBUtils {
    
    public static testmethod void testmethod1 () {
        
        Account testacc;
        DButils.InsertRecord(testacc, true);
        DButils.UpdateRecord(testacc, true);
        
        testacc = new Account();
        DButils.InsertRecord(testacc, true);
        testacc = new Account(Name='Test');
        DButils.InsertRecord(testacc, true);
        
        List<Account> testAccounts = new List<Account>();
        DButils.InsertRecords(testAccounts, true);
        testAccounts.add(new Account(Name='test2'));
        DButils.InsertRecords(testAccounts, true);
        
        testacc.Name='';
        DButils.UpdateRecord(testacc, true);
        testacc.Name='TESTUpdate';
        DButils.UpdateRecord(testacc, true);
        
        DButils.UpdateRecords(testAccounts, true);
        
    }

}