@isTest
public class Test_DRZEvents {
    
    static testmethod void eventPublisherTest(){
        
        Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Update Acc1;

        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c   ='700009',  OpCo_Id__c='ONS', Segment_Id__c ='A_AND_E', Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c   ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.');
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        
        Opportunity oppty = new Opportunity( Name =  'accName', LDS_Account_Name__c = Acc1.id, Accountid = Acc1.id,                
                    RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc',
                    Division__c = 'EASi Engineering', Legacy_Product__c = prd.Id, BusinessUnit__c ='EASI', Response_Type__c = 'Formal RFx',
                    stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required',
                    Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                    Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required',
                    Services_Capabilities_Alignment__c = 'TEK frequently provides', Sales_Resource_Requirements__c = 'Less than',
                    CloseDate = system.today()+1,Req_Job_Description__c = 'Test', Req_Qualification__c = 'Test',
                    Req_Product__c = 'Contract', Req_Bill_Rate_Max__c=10, Req_Bill_Rate_Min__c=1, Req_Duration_Unit__c = 'Day(s)',
                    Req_Duration__c=10, Req_Pay_Rate_Max__c = 10, Req_Pay_Rate_Min__c = 1, Req_Standard_Burden__c = 10, Req_Rate_Frequency__c='Hourly',Req_Skill_Specialty__c='Communications');
        insert oppty;
        
        List<String> oppIds = new List<String>();
        oppIds.add(oppty.Id);
        DRZEventPublisher.publishOppEvent(oppIds, 'BoardId', 'NEW_OPPORTUNITY');
        DRZEventPublisher.syncToDrz(oppty.Id);
        DRZEventPublisher.getOpportunityTeamJson(oppIds);
    }    
    
    static testmethod void submittalEventPublisherTest(){
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Aerotek, Inc';
        opp1.Req_Skill_Specialty__c='Communications';
        insert opp1;

        Contact ct = TestData.newContact(lstNewAccounts[0].id, 1, 'Talent');  
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.');        
         DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Id recTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();

        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = lstNewAccounts[0].id;
        o.ShipToContactId = ct.Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Submitted';
        o.EffectiveDate = System.today();
        o.RecordTypeId = recTypeId;
        o.isDRZUpdate__c = true;
        try{
            insert o;
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
        list<Order> OrderList = new list<Order>();
        //OrderList.add(o);
        List<Id> orderIds = new List<Id>();
        orderIds.add(o.Id);
        Test.startTest();
        //OrderList.add(o);
        DRZEventPublisher.publishOrderEvent(orderIds, 'BoardId', 'NEW_ORDER');
        DRZEventPublisher.ReOpenOpportunityOrderEvents(orderIds, 'BoardId', 'REOPEN_ORDER');
        test.stopTest();        
    }        
    static testmethod void testLookup(){
        
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;
        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        insert TestOpp;
        Order testOrder = new Order();
        testOrder.AccountId=newAccount.Id;
        testorder.EffectiveDate= mydate;
        testOrder.Status='Draft';
        testOrder.isDRZUpdate__c=true;
        testOrder.OpportunityId = TestOpp.Id;
        testOrder.Bill_Rate__c = 123.0;
        insert testOrder;
        
        insert new User_Organization__c(Name='TEKsystems Hanover',Active__c=true,OpCo_Name__c='Teksystems, Inc.');
        
        
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        insert new DRZSettings__c(Name='Office',Value__c = 'Aerotek, Inc;TEKsystems, Inc.');
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        insert new OrderDeletePlatformEvent__c(Name='OrderDeletePlatformEvent',Disable__c = True);
        Map<String, String> requests = new Map<String, String>();
        requests.put('account','test');
        requests.put('office','Teksystems');
        requests.put('division','');
        
        Test.startTest();
        
        for(String str:requests.keySet()){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            
            req.requestURI = '/services/apexrest/DRZLookup';
            req.addParameter('type', str);
            req.addParameter('value', requests.get(str));
            req.addParameter('size', '5');
            req.addParameter('page', '1');
            req.httpMethod = 'GET';
            req.addHeader('Content-Type', 'application/json'); 
            System.debug(req);
            RestContext.request = req;
            RestContext.response= res;
            
            DRZLookup.getDRZLookup();
        }        
        
        delete testOrder;
        Test.stopTest();  
    }
    
    
    static testmethod void testDRZPublishChatterEvents(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        FeedItem p  = new FeedItem();
        p.ParentId = UserInfo.getUserId();
        p.Body = 'This is amazing';
        Database.SaveResult sr = Database.Insert(p);
        
        //check to make sure that it was converted 
        List<FeedItem> fi = [select Id, ParentId, Body from FeedItem where Id = :sr.getId()];
        
        List<FeedComment> lstFc = new List<FeedComment>();
        FeedComment fc = new FeedComment();
        fc.feedItemId = fi[0].Id;
        fc.CommentBody = 'wow is the word';
        Database.SaveResult sr2 = Database.Insert(fc);
        lstFc.add(fc);  
        
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        Test.startTest();
        
        DRZPublishChatterEvents drz = new DRZPublishChatterEvents();
        
        drz.createChatterCommentEvent( lstFc, 'insert');
        drz.createChatterPostEvent(fi, 'update' );
        Test.stopTest();  
    }
    
    static testmethod void testDRZPublishNewOpportunity(){
        
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;
        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        TestOpp.Req_Skill_Specialty__c='Communications';
        insert TestOpp;
        
        List<String> oppId = new List<String>();
        oppId.add( TestOpp.id );
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Test.startTest();    
        DRZPublishNewOpportunity obj = new DRZPublishNewOpportunity();
        DRZPublishNewOpportunity.publishNewOpportunityEvent(oppId);
        Test.stopTest();  
    }
    
    static testmethod void testDRZPublishUpdatedOpportunity(){
        Account newAccount = new Account(
            Name = 'test lookup',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert newAccount;
        
        opportunity  TestOpp=new opportunity ();
        date mydate = date.today();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=mydate;
        TestOpp.AccountId = newAccount.Id; 
        TestOpp.OpCo__c='Aerotek, Inc';
        TestOpp.Req_Skill_Specialty__c='Communications';
        insert TestOpp;
        
        List<String> oppId = new List<String>();
        oppId.add( TestOpp.id );
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Test.startTest();    
        DRZPublishUpdatedOpportunity.publishUpdatedOpportunityEvent(oppId);
        Test.stopTest(); 
        
    }
    
    static testmethod void testDRZPublishUpdatedOrder(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Id rType = [Select Id From RecordType WHERE Name = 'Talent' AND SobjectType='Account' LIMIT 1].Id;
        Account a = new Account();
        a.Name = 'Test User';
        a.RecordTypeId = rType;
        Database.insert(a);
        
        
        Order o = new Order();
        o.AccountId = a.Id;
        o.EffectiveDate = Date.Today();
        o.Status = 'Offer Accepted';
        List<RecordType> rList = [select Id from RecordType where DeveloperName='OpportunitySubmission' LIMIT 1];
        o.RecordTypeId = rList[0].Id;
        Database.insert(o);
        
        List<String> orderIds = new List<String>();
        orderIds.add(o.id);
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        DRZPublishUpdatedOrder.publishUpdatedOrderEvent(orderIds);
    }
    
    static testmethod void testDRZToolingAPIforValidate(){
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Id rType = [Select Id From RecordType WHERE Name = 'Talent' AND SobjectType='Account' LIMIT 1].Id;
        Account a = new Account();
        a.Name = 'Test User';
        a.RecordTypeId = rType;
        Database.insert(a);
        
        
        Order o = new Order();
        o.AccountId = a.Id;
        o.EffectiveDate = Date.Today();
        o.Status = 'Offer Accepted';
        List<RecordType> rList = [select Id from RecordType where DeveloperName='OpportunitySubmission' LIMIT 1];
        o.RecordTypeId = rList[0].Id;
        Database.insert(o);
        
        List<String> orderIds = new List<String>();
        orderIds.add(o.id);
        insert new DRZSettings__c(Name='OpCo',Value__c = 'Aerotek, Inc;TEKsystems, Inc.'); 
        DRZ_ToolingApiforValidation.DRZ_ReqOpp_Validations();
        DRZ_ToolingApiforValidation.DRZ_ValidationWrapper m = new DRZ_ToolingApiforValidation.DRZ_ValidationWrapper();
    }
    
    static testmethod void batchProcessingTest(){
        
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Draft';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Aerotek, Inc';
        opp1.Req_Skill_Specialty__c='Communications';
        insert opp1;

        
        try{
           Test.startTest();    
           Id batchJobId = Database.executeBatch(new Batch_ProcessLegacyMultiCurrency('Select id, StageName from Opportunity'), 2);
        Test.stopTest(); 

        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
                
    } 
    static testmethod void batchProcessingExceptionTest(){
        
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Closed Won';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today().addDays(-4);
        opp1.OpCo__c = 'Aerotek, Inc';
        opp1.Req_Skill_Specialty__c='Communications';
        insert opp1;

        
        try{
           Test.startTest();    
           Id batchJobId = Database.executeBatch(new Batch_ProcessLegacyMultiCurrency('Select id, StageName from Opportunity'), 2);
        Test.stopTest();
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
                
    } 
    
    static testmethod void batchProcessingTest3(){
        
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        
        Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Update Acc1;

        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c   ='700009',  OpCo_Id__c='ONS', Segment_Id__c ='A_AND_E', Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c   ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Draft';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today().addDays(-4);
        opp1.OpCo__c = 'Aerotek, Inc';
        opp1.Currency__c = 'GBP - British Pound';
        //opp1.Req_Rate_Frequency__c = 'Weekly';
        opp1.Req_Total_Positions__c = 0;
        opp1.RecordtypeId = ReqRecordTypeId;
        opp1.Legacy_Product__c = prd.Id;
        opp1.Amount = 100;
        opp1.Req_Additional_1st_Year_Compensation__c = 50;
        opp1.Req_Bill_Rate_Min__c = 50;
        opp1.Req_Bill_Rate_Max__c = 100;
        opp1.Req_Flat_Fee__c = 12;
        opp1.Req_Skill_Specialty__c='Communications';
        insert opp1;

        
        try{
            Test.startTest();    
           Id batchJobId = Database.executeBatch(new Batch_ProcessLegacyMultiCurrency('Select id, Req_Rate_Frequency__c, Req_Pay_Rate_Min__c,Req_Flat_Fee__c, StageName, CloseDate, Currency__c,Req_Total_Spread__c,Retainer_Fee__c,Req_Salary_Min__c,Req_Salary_Max__c,Req_Additional_1st_Year_Compensation__c,Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Total_Compensation_Max__c, Amount,Total_Compensation_Min__c,Req_Pay_Rate_Max__c from Opportunity'), 2);
        Test.stopTest();
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
                
    } 
    static testmethod void batchProcessingTest2(){
        
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        TestData TestCont = new TestData();
        testdata tdConts = new TestData();
        
        Account Acc1 = CreateTalentTestData.createTalent();
        Contact talCont = CreateTalentTestData.createTalentContact(Acc1);
        Acc1.recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Update Acc1;

        Product2 prd = new product2(OpCo__c = 'Aerotek, Inc',Division_Name__c = 'EASi Engineering',
            Name = 'TIVOLI', Category_Id__c = 'FIN_RM', Category__c = 'Finance Resource Management', Job_Code__c = 'Developer',
            Jobcode_Id__c   ='700009',  OpCo_Id__c='ONS', Segment_Id__c ='A_AND_E', Segment__c = 'Architecture and Engineering',
            OpCo_Status__c = 'A', Skill_Id__c   ='TIVOLI', Skill__c = 'Tivoli' );
        insert prd;
        
        string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TEKsystems Global Services').getRecordTypeId();
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Draft';
        opp1.AccountId = lstNewAccounts[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today().addDays(-4);
        opp1.OpCo__c = 'Aerotek, Inc';
        opp1.Currency__c = 'GBP - British Pound';
        //opp1.Req_Rate_Frequency__c = 'Weekly';
        opp1.Req_Total_Positions__c = 0;
        opp1.RecordtypeId = ReqRecordTypeId;
        opp1.Legacy_Product__c = prd.Id;
        opp1.Amount = 100;
        opp1.Req_Additional_1st_Year_Compensation__c = 50;
        opp1.Req_Bill_Rate_Min__c = 50;
        opp1.Req_Bill_Rate_Max__c = 100;
        opp1.Req_Flat_Fee__c = 12;
        opp1.Req_Skill_Specialty__c='Communications';
        insert opp1;

        
        try{
            Test.startTest();    
           Id batchJobId = Database.executeBatch(new Batch_ProcessLegacyMultiCurrency('Select id, Req_Rate_Frequency__c, Req_Pay_Rate_Min__c, StageName, CloseDate, Currency__c,Req_Total_Spread__c,Retainer_Fee__c,Req_Salary_Min__c,Req_Salary_Max__c,Req_Additional_1st_Year_Compensation__c,Req_Bill_Rate_Min__c,Req_Bill_Rate_Max__c,Total_Compensation_Max__c, Amount,Total_Compensation_Min__c,Req_Pay_Rate_Max__c from Opportunity'), 2);
        Test.stopTest();
        }
        catch(DMLException e){
            System.Debug('Order Creation Error '+e);
        }
                
    }
    static testmethod void testPicklistEntryWrapper(){
        PicklistEntryWrapper wrapper = new PicklistEntryWrapper();
        wrapper.active          = 'hello';
        wrapper.defaultValue    = 'default';
        wrapper.label           = 'label';
        wrapper.value           = 'value';
        wrapper.validFor        = 'validFor';
        
    }
}