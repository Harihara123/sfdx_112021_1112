@isTest(seealldata = false)
public class Test_DataMigHrXMLBatch{
    static testMethod Void Test_DeleteReqTeamMembers() {
        Connected_Data_Export_Switch__c ConnectedDataExportSwitch = new Connected_Data_Export_Switch__c(Data_Extraction_Insert_Flag__c = False, Name = 'DataExport',PubSub_URL__c = 'http://www.allegisgroup.com');
        insert ConnectedDataExportSwitch;
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();

        TestData TestCont = new TestData();

        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;

        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        
        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                lstOpportunitys.add(newOpp);
            }     
        }
        insert lstOpportunitys;
        
        Profile p1 = [select id from profile where name='System Administrator'];
        list<user> userlist = new list<user>();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            for(integer i=0;i<=1;i++){
                String orgId = UserInfo.getOrganizationId();
                String dateString = String.valueof(DateTime.Now().format('ddmmyyHHmmss'));
                Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
                String uniqueName = orgId + dateString + randomInt;
                String tmp = uniqueName.right(20);
                User user1= new User(firstname = 'Batch',
                                lastName = 'Integration',
                                email = uniqueName + '@allegisgroup.com' ,
                                Username = uniqueName + '@allegisgroup.com',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = p1.Id) ;  
                userlist.add(user1);
            }   
            insert userlist; 
        }
                
        system.runAs(userlist[0]){
            list <Opportunity> Opps = [select id, Req_Job_Description__c from Opportunity];
            for(Opportunity x:Opps){
                x.Req_Job_Description__c = 'Testing HRXML';
            }    
            update Opps;
            
            DateTime dtLastBatchRunTime = system.today()-1;
            DataMigHrXMLBatch batch = new DataMigHrXMLBatch(dtLastBatchRunTime);
    
            test.starttest();
            database.executebatch(batch,1);
            test.stopTest(); 
        }
    }

}