@isTest

public class Test_DeleteReqController {

   // static User user = TestDataHelper.createUser('Aerotek AM');
    static User user = TestDataHelper.createUser('System Administrator');
    public static testMethod void testDeleteReqController() {
    
        insert user;
        user.Peoplesoft_Id__c = 'abc123wef12';
        update user; 
       system.runAs(user)
       {  
    //Hareesh-Added record type in the below account creation
          Account NewAccounts = new Account(
                    Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = 'TEST-1111',
                    Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
                    ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
                    ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
                    BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
                    BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
                );
                insert NewAccounts;
                
        User_Organization__c newOffice = new User_Organization__c();
        newOffice.Office_Name__c = 'TEST';
        newOffice.Active__c = true;
        insert newOffice ;       
                
    
         Reqs__c recRequisition = new Reqs__c(
                Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
                Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
                Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,
                Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample', Organization_Office__c = newOffice.id);
                insert recRequisition;
       
               
        Contact con = TestDataHelper.createContact();
        Req_Team_Member__c reqteamMember = new Req_Team_Member__c();
        reqteamMember.Contact__c = con.id;
        reqteamMember.Requisition__c = recRequisition.id;        
        insert reqteamMember;
          
       
        ApexPages.StandardController delReqTeamSC = new ApexPages.StandardController(reqteamMember);
        DeleteReqTeamController delReqTeam = new DeleteReqTeamController(delReqTeamSC);
        delReqTeam.redirect();
        recRequisition.Organization_Office__c = null;
        update recRequisition;
       
       
        Req_Team_Member__c reqteamMember2 = new Req_Team_Member__c();
        reqteamMember2.Contact__c = con.id;
        reqteamMember2.Requisition__c = recRequisition.id;        
        insert reqteamMember2;
       
       
        ApexPages.StandardController delReqTeamSC2 = new ApexPages.StandardController(reqteamMember2);
        DeleteReqTeamController delReqTeam2 = new DeleteReqTeamController(delReqTeamSC2);
        delReqTeam2.redirect();
        
        
        delReqTeam.redirect();     
        
        
            
                
         ApexPages.StandardController sc = new ApexPages.StandardController(recRequisition);
         DeleteReqController delReq = new DeleteReqController(sc); 
         delReq.redirect();
         delReq.BacktoReq();
      }  
    }
    
}