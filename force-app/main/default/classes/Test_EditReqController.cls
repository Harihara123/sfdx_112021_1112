@istest
public class Test_EditReqController {
	static User user = TestDataHelper.createUser('System Administrator');
	
	public static testMethod void testEditReqController() {
		insert user;         
       system.runAs(user)
       {  
           //Hareesh-added recordtypeid feild
		 	Account NewAccounts = new Account(
	                    Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = 'TEST-1111',
	                    Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
	                    ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
	                    ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
	                    BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
	                    BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
	                );
	     	insert NewAccounts;
	                
	        User_Organization__c newOffice = new User_Organization__c();
	        newOffice.Office_Name__c = 'TEST';
	        newOffice.Active__c = true;
	        insert newOffice ;   
	    
	         Reqs__c recRequisition = new Reqs__c(
	                Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
	                Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
	                Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,
	                Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample', Organization_Office__c = newOffice.id);
	         insert recRequisition;
	        //Case 1: Draft/Qualified Req
	        ApexPages.StandardController editReqTeamSC = new ApexPages.StandardController(recRequisition);
	        EditReqController editReqTeam = new EditReqController(editReqTeamSC);
	        editReqTeam.redirect();
	        //Case 2: Staging Req
	        recRequisition.Stage__c ='Staging';
	        update recRequisition;
	        editReqTeamSC = new ApexPages.StandardController(recRequisition);
	        editReqTeam = new EditReqController(editReqTeamSC);
	        editReqTeam.redirect();
       }
        
	}
}