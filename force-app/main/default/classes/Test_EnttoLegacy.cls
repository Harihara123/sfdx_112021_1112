@isTest(seealldata = false)
public class Test_EnttoLegacy 
{
    static testMethod Void Test_EnttoLegacy() 
    {
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();

        TestData TestCont = new TestData();

        testdata tdConts = new TestData();
        Contact TdContObj = TestCont.createcontactwithoutaccount(lstNewAccounts[0].Id); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;

        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        string rectype='Talent';
        List<Contact> TalentcontLst = tdConts.createContacts(rectype);


        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing is done at all stages of opportunity to make sure everything works as expected';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                lstOpportunity.add(newOpp);
            }     
        }


        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Qualified';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing is done at all stages of opportunity to make sure everything works as expected';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';
                newOpp.Req_Duration_Unit__c = 'Hour(s)';

                lstOpportunity.add(newOpp);
            }     
        }
        
        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';
                newOpp.Req_Duration_Unit__c = 'week(s)';
                newOpp.Req_Additional_Information__c = 'Test';
                newOpp.Req_RRC_Open_Date__c = closingdate;
                newOpp.Req_RRC_Positions__c = 1;
                newOpp.Req_Business_Challenge__c = 'Test';
                newOpp.Req_Competition_Info__c = 'Test';
                newOpp.Req_External_Job_Description__c = 'Test';
                newOpp.Req_Impact_to_Internal_External_Customer__c = 'Test';
                newOpp.Req_Pay_Rate_Max__c = 100;
                newOpp.Req_Pay_Rate_Min__c = 50;
                newOpp.Req_Bill_Rate_Max__c = 1000000;
                newOpp.Req_Bill_Rate_Min__c = 1000000;
                lstOpportunity.add(newOpp);
            }     
        }
        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Closed Lost';
                newOpp.Req_Total_Positions__c = 0;
                newOpp.Req_Total_Lost__c = 2;
                newOpp.Req_Loss_Wash_Reason__c = 'Test Wash';
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing is done at all stages of opportunity to make sure everything works as expected';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                //newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';
                newOpp.Req_Duration_Unit__c = 'Month(s)';
                newOpp.Req_Secondary_Delivery_Office__c = 'Global Talent Aqn Center';
                newOpp.Req_Delivery_Type__c = 'Retail';
                newOpp.Req_Client_working_on_Req__c = 'Yes';
                newOpp.Req_Date_Client_Opened_Position__c = System.Date.Today();
                newOpp.Req_Decline_Reason__c = 'Req already entered manually';
                newOpp.Req_EVP__c = 'tesinscghsch';
                newOpp.Req_Interview_Date__c = System.Date.Today();
                newOpp.Req_Interview_Information__c = 'tesingedsf';
                newOpp.Req_Max_Submissions_Per_Supplier__c = 10;
                newOpp.Req_Merged_Req_ID__c = '318745365467536';
                newOpp.Req_Merged_Req_System__c = '342342545365';
                newOpp.Req_Performance_Expectations__c = 'testingscffcsh';
                newOpp.Req_Project_Stage_Lifecycle_Info__c = 'tegshafhfhfhh';
                newOpp.Req_Approval_Process__c = 'fadgdahchjadh';
                newOpp.Req_Service_Product_Offering__c = 'Staffing';
                newOpp.Req_SourceReq__c = 'testingdfdfs';
                newOpp.Req_System_SourceReq__c = 'tesingfdsfhfdsh';
                newOpp.Req_Internal_External_Customer__c = 'testingscfsg';
                newOpp.Req_Why_Position_Open_Details__c = 'testingscfsg';
                //newOpp.Req_Work_Environment__c = 'testingscfsg';
                //newOpp.Req_Proactive_Req_Link__c = 'testingsfsf';
                newOpp.Req_Draft_Reason__c = 'Other';
                newOpp.OT_Multiplier__c = 'x2';
                newOpp.Req_Worksite_State__c = 'texaswashingtonnew';
                newOpp.Req_Worksite_Country__c = 'UnitedStatesOfAmericaUSAUSAmericaUnitedStatesOfAmerica';
                newOpp.Req_Skill_Details__c = 'testingskills';
                lstOpportunity.add(newOpp);
            }     
        }       
        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Closed Wash';
                newOpp.Req_Total_Positions__c = 0;
                newOpp.Req_Loss_Wash_Reason__c = 'Test Reason';
                newOpp.Req_Total_Washed__c = 2;
                newOpp.Req_Total_Lost__c = 0;
                newOpp.Req_Total_Filled__c = 0;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                //newOpp.Req_Draft_Reason__c = 'VMS';
                newOpp.OT_Multiplier__c = '2';
                newOpp.Req_Worksite_State__c = 'texas';
                string descr = '';
                for(integer k=0; k<32050;k++)
                {
                    descr = descr + 'a';
                }
                newOpp.Req_Job_Description__c = descr + 'Testing is done at all stages of opportunity to make sure everything works as expected';
                
                string skill = '';
                for(integer l=0; l<2100;l++)
                {
                    skill = skill + 'a';
                }
                newOpp.Req_Skill_Details__c = skill;
                
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '212285678943673214567890343363373535';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';
                newOpp.Req_Duration_Unit__c = 'Year(s)';
                lstOpportunity.add(newOpp);
            }     
        }       


        insert lstOpportunity;

        ProcessLegacyReqs__c p = new ProcessLegacyReqs__c();
        p.Name ='BatchEntReqLastrunTime';
        p.LastExecutedBatchTime__c = Datetime.newInstance(2018, 02, 22);
        insert p;

        string QueryReq = System.Label.ProcessEntReqs;
        BatchCopyEnttoLegacy batchJob = new BatchCopyEnttoLegacy();
        batchjob.QueryReq = 'SELECT Id FROM Opportunity';
        database.executebatch(batchJob,200);

       /*
        update lstOpportunity;

        BatchCopyEnttoLegacy batchJob2 = new BatchCopyEnttoLegacy();
        database.executebatch(batchJob2,200);
       */
    }
    
    
}