/*******************************************************************
* Name  : Test_EventTrigger
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 27, 2015
* Details : Test class for EventTrigger
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_EventTrigger {
	//variable declaration
   // static User user = TestDataHelper.createUser('Aerotek AM');
    static User user = TestDataHelper.createUser('System Administrator');
   /* static testMethod void test_EventTrigger() {
        Test.startTest(); 
        if(user != Null) {
            system.runAs(user) {    
                TestData TdAcc = new TestData(1);
                
                // Create list of Accounts
                List<Account> lstNewAccounts = TdAcc.createAccounts();
                // Load Events
                List<Event> lstEvents = new List<Event>();
                 
                for(Account Acc : TdAcc.createAccounts()) {
                    Event NewEvent = new Event(Whatid = Acc.id, Subject = 'Call'+1, 
                        startDateTime =  system.Now(),DurationInMinutes  = 10);
                        system.assertequals(NewEvent.startDateTime <= system.Now(),True);
                        //Load the Event into the list
                        lstEvents.add(NewEvent);
                }  
                // Insert the list of Events        
                insert lstEvents;
				List<Event> evtList= new List<Event>();				
				for(Event evt : [Select Id from Event]){
					evt.Subject = 'test subject';
					evt.Description = 'Test Description';
					evt.Completed_Flag__c = false;
					evtList.add(evt);
				}
				System.debug('evtList----'+evtList);

                update evtList;
				System.debug('evtList----'+[select id,Subject, Description, Completed_Flag__c from event ]);

                delete lstEvents;
                undelete lstEvents;
            }
        }    
        Test.stopTest();     
    }
	*/ 
	static testMethod void test_EventTrigger1() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='True');
        insert DRZSettings1;
		App_Prioritization_Settings__c settings = new App_Prioritization_Settings__c(Name = 'AppPriority',ByPassAsyncTrigger__c=false);
		insert settings;
		Test.startTest();
			Account talent = CreateTalentTestData.createTalentAccount();
			Contact talentContact = CreateTalentTestData.createTalentContact(talent);
			Job_Posting__c jb = Test_ATSOrderRESTService.createJobPostingObject('124');
			System.debug('JobPostingController'+jb);
			List<Event> testEventList = CreateTalentTestData.createTalentEventWithJobPosting(talentContact, talent, jb);
			set<String> eventIds = new Set<String>();
			System.debug('testEventList:'+testEventList);
			if(testEventList != null && testEventList.size() > 0) {
				for(Event testEvent : testEventList) {
					
					testEvent.Description = 'Test Description';
					testEvent.Completed_Flag__c = false;
					testEvent.Disposition_Code__c = '12375';
				}

				update testEventList;
				Event testEvtMst = testEventList.get(0);
				EventTriggerHandler.logExceptionMesg(testEvtMst, testEvtMst.id, 'test exception log message');
				testEvtMst.WhatId = null;
				EventTriggerHandler.logExceptionMesg(testEvtMst, testEvtMst.id, 'test exception log message');//covering if what id null
				ID recordIDTest = testEvtMst.Id;
				testEvtMst = null;
				EventTriggerHandler.logExceptionMesg(testEvtMst, recordIDTest, 'test exception log message');//covering if what id null

				Delete testEventList;

				Undelete testEventList;
			}

			if(testEventList != null && testEventList.size() > 0) {
				for(Event testEvent : testEventList) {
					testEvent.Application_Id__c = 'testApplicant';
					testEvent.Activity_Type__c = 'Applicant';
					eventIds.add(testEvent.Id);
				}
				update testEventList;
			}
			 if(eventIds.size() > 0) {
				EventTriggerHandler trghandler = new EventTriggerHandler();
				trghandler.OnAfterInsertAsync(eventIds);
			 }
			 
			
		Test.stopTest();
	}
}