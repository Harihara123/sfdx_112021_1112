@isTest(SeeallData=False)
public  class Test_Event_UpdateLastActivityDate {
static User user = TestDataHelper.createUser('System Administrator');
    public static testMethod void Test_Event_UpdateLastActivityDate() { 
        if(user != Null) {        
            system.runAs(user) {
        TestData Td = new TestData(2);
        List<Account> lstNewAccounts = Td.createAccounts();
        id userid;
        userid = UserInfo.getUserId();
       
        Target_Account__c iTarget = new Target_Account__c(Account__c  = lstNewAccounts[0].id, user__c=userid );
        insert iTarget;
        iTarget = [Select Name__c from Target_Account__c where id = :iTarget.Id]; 
       // system.assertEquals(iTarget.Name__c,userInfo.getName());
        
                       
        TestData TdCont = new TestData(1); 
        Contact ContObj = TdCont.createcontactwithoutaccount(lstNewAccounts[0].Id); 
        
        List<Event> events = new List<Event>();
        for(Event eve : TestDataHelper.getEvents())
        {
              eve.whoId = contObj.id;
              eve.whatId = lstNewAccounts[0].Id;
              events.add(eve);
        }
        // insert events
        insert events;
        //query the contact infomation
        contObj = [select Last_Event__c from Contact where id = :contObj.Id];
        try{
            //system.assertEquals(contObj.Last_Event__c,system.Today());
        }catch(Exception e)
        {   
               
        }
        List<Task> tasks = new List<Task>();
        for(Task tsk : TestDataHelper.getTasks())
        {
              tsk.whoId = contObj.id;
              tsk.whatId = lstNewAccounts[0].Id;
              tasks.add(tsk);
        }
        insert tasks;    
        
        delete iTarget;
        }
    }
    }
}