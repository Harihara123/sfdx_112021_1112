@isTest
public class Test_FixEventOwner {
	static testMethod void validateFixEventOwnerBatch() {
        Test.startTest();
        setupTestEvent();
        FixEventOwner feo = new FixEventOwner();
        Database.executeBatch(feo);
        Test.stopTest();
    }
    
    static Event setupTestEvent() {
        Event event = new Event(
                            Subject = 'Offer Accepted', 
                            startDateTime =  system.Now(),
                            DurationInMinutes  = 10,
                            Type = 'Offer Accepted');
        
        insert event;
        return event;
    }
}