@isTest
public class Test_FyreSyncTriggerHandler {
    public static testMethod void onAfterInsertMethod(){
        Test.startTest();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        
        FyreSync__Requisition__c fyreSyncRec=new FyreSync__Requisition__c();
        fyreSyncRec.FyreSync__Account__c=lstNewAccounts[0].Id;
        fyreSyncRec.FyreSync__ReqDetails_AddressLine1__c='Hill View, Discovery';
        fyreSyncRec.FyreSync__ReqDetails_AddressLine2__c='New York, United States, 123456';
        fyreSyncRec.FyreSync__Rates_BillRateMax__c=2;
        fyreSyncRec.FyreSync__Rates_BillRateMin__c=2;
        fyreSyncRec.FyreSync__ReqDetails_City__c='New york';
        fyreSyncRec.FyreSync__ReqDetails_Comments__c='Req details Comments';
        fyreSyncRec.FyreSync__BuyerDetails_Company__c='Facebook';
        fyreSyncRec.FyreSync__ReqDetails_CountryCode__c='United States';
        fyreSyncRec.FyreSync__ReqDetails_Description__c='Test Description';
        fyreSyncRec.FyreSync__Status__c='active';
        fyreSyncRec.FyreSync__submittalMax__c=10;
        fyreSyncRec.FyreSync__ReqDetails_Title__c='';
        fyreSyncRec.FyreSync__ReqDetails_TotalOpenings__c=6;
        fyreSyncRec.FyreSync__VmsType__c='BEELINE';
        fyreSyncRec.Name='Testing T-01';
        
        insert fyreSyncRec;
        Test.stopTest();
    }
}