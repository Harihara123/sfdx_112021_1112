@isTest
global class Test_GeoLocationMockResponse implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {        	
    	String responseString ='{"token_type":"Bearer","expires_in":"3599","ext_expires_in":"3599","expires_on":"1585229230","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            					+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS",'
                				+'"response":{"header": {"status": "OK"},'
                				+'"result": ['
                				+'{"id": "0061k000008DiGbAAK","latlong": "40.8882","longitude": "-74.0503","message": ""}'
               					 +']}}';  
                
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(responseString);
        res.setStatusCode(200);
        return res;
    }
}