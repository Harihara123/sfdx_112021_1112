@isTest
private class Test_GetGeoLocationFromMulesoft {
	static testMethod void GetGeoLocationTest1(){
    	Test.startTest();
        Account accnt=TestData.newAccount(1,'Client');
        system.debug('****accnt**'+accnt);                          
        Product2 testProduct=Test_GetGeoLocationFromMulesoft.createTestProduct();
             
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='GetGeoService',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Service_Method__c='POST',
                                                                            Resource__c ='44d4b52e-190e-4ac3-a7cb-8547d4de9d42',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.setMock(HttpCalloutMock.class, new Test_GeoLocationMockResponse());
        new GetGeoLocationFromMulesoft();
        Id recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        Opportunity opp1=new Opportunity(Name = 'TESTOPP1',CloseDate = system.today(),StageName = 'Draft',
                                              accountId= accnt.Id,Req_Division__c='	Aerotek',OpCo__c='AG_EMEA',
                                             Legacy_Product__c=testProduct.id);    
        opp1.RecordTypeId=recordtypeid;
        opp1.Req_Worksite_Country__c='United States';
        opp1.Req_Worksite_Postal_Code__c='07601';
        opp1.isDRZUpdate__c=true;       
        insert opp1;
        Test.stopTest();
	}    
    private static Product2 createTestProduct(){       
    	Product2 testProduct = new Product2();
        testProduct.Name='StandardProduct';
        testProduct.Category__c='TestCategory';
        testProduct.Category_Id__c ='123';        
        testProduct.Division_Name__c='Aerotek CE';
        testProduct.Job_code__c ='TestJob';
        testProduct.Jobcode_Id__c='123';
        testProduct.OpCo__c ='Aerotek, Inc';
        testProduct.OpCo_Id__c='123';
        testProduct.Segment__c ='testSegemnt';
        testProduct.Segment_Id__c ='123';
        testProduct.Skill__c ='testSkill';
        testProduct.Skill_Id__c ='123';
        testProduct.IsActive = true;        
        insert testProduct;
        return testProduct;        
    }
}