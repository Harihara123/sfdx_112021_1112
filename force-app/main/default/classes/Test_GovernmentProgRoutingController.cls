/*******************************************************************
* Name  : Test_GovernmentProgRoutingController
* Author: Vivek Ojha(Appirio India) 
* Date  : June 22, 2015
* Details : Test class for GovernmentProgRoutingController
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_GovernmentProgRoutingController{
    static testMethod void test_RoutingForGovernmentProg() {
        User usr = TestDataHelper.createUser('System Administrator');
        // create Government Programs
        Government_Program__c govtProg= new Government_Program__c();
        govtProg.Name = 'test';
        govtProg.OpCo__c='Aerotek';
        insert govtProg;
        System.runAs(usr ){
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(govtProg);
            GovernmentProgRoutingController ctrl = new GovernmentProgRoutingController(sc);
            pagereference pg = ctrl.getRouter();
            Test.stopTest();
            System.assertNotEquals(null,pg);
        }
         
    }
}