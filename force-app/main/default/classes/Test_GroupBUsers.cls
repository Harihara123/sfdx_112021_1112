@isTest
public with sharing class Test_GroupBUsers{
    
    static testMethod void UserLogSearches(){
        Test.startTest();
		User testUser = [SELECT id, Name, Profile.Name, OPCO__c, Profile.PermissionsModifyAllData, Peoplesoft_Id__c FROM User WHERE Id = :UserInfo.getUserId()];
		String  userId = testUser.id;
        List<Object> outputSearchList = new List<Object>();
        Map<String, Object> deserializeType=new Map<String, Object>();
        Map<String, Object> deserializeType1=new Map<String, Object>();
        deserializeType.put('engine','ES');
        deserializeType.put('type','R2C');
        deserializeType.put('execTime','123456');       
        outputSearchList.add(JSON.serialize(deserializeType));
        for(integer i=0;i<400;i++){
            deserializeType1.put('execTime'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('pageSize'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('offset'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('execTime1'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('pageSize1'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('offset1'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('execTime2'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('pageSize2'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('offset2'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('execTime3'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('pageSize3'+i,'12345678901234567890123456789123456789');
            deserializeType1.put('offset3'+i,'12345678901234567890123456789123456789');
        }
        outputSearchList.add(JSON.serialize(deserializeType1));
        Map<String, Object> outMap = new Map<String, Object>();
        outMap.put('searches', outputSearchList);
        String R2TRemovedString = JSON.serialize(outMap);      
        
		System.runAs(testUser){ 
            User_Search_Log__c insRecord = new User_Search_Log__c();            
            if (R2TRemovedString.length() > 131072) {
            	insRecord.Search_Params__c = '"' + R2TRemovedString.substring(0, 131070) + '"';
            	insRecord.Search_Params_Continued__c = '"' + R2TRemovedString.substring(131070) + '"';
        	}
            else {
            	insRecord.Search_Params__c = R2TRemovedString;
            	insRecord.Search_Params_Continued__c = null;
        	}
            insRecord.Type__c  = 'AllSavedSearches';
        	insRecord.User__c = testUser.Id;            
            insert insRecord;            
            GroupBUsers AddEngine = new GroupBUsers();
			Id batchId = Database.executeBatch(AddEngine);
        }
        Test.stopTest();
	}    
    static testMethod void UserLogWithoutContinued(){
        Test.startTest();
		User testUser = [SELECT id, Name, Profile.Name, OPCO__c, Profile.PermissionsModifyAllData, Peoplesoft_Id__c FROM User WHERE Id = :UserInfo.getUserId()];
		String  userId = testUser.id;
        List<Object> outputSearchList = new List<Object>();
        Map<String, Object> deserializeType=new Map<String, Object>();
        //deserializeType.put('engine','ES');
        //deserializeType.put('type','R2C');
        deserializeType.put('execTime','123456789');
        deserializeType.put('pageSize','123456789');
        
        outputSearchList.add(JSON.serialize(deserializeType));
        Map<String, Object> outMap = new Map<String, Object>();
        outMap.put('searches', outputSearchList);
        String R2TRemovedString = JSON.serialize(outMap);        
        
		System.runAs(testUser){ 
            User_Search_Log__c insRecord = new User_Search_Log__c();
            insRecord.Search_Params__c = R2TRemovedString;
            insRecord.Search_Params_Continued__c = null;
        	insRecord.Type__c  = 'AllLastSearches';
        	insRecord.User__c = testUser.Id;            
            insert insRecord;
			List<String> useridlist=new List<String>();
 			useridlist.add(testUser.id);            
            GroupBUsers AddEngine = new GroupBUsers(useridlist);
			Id batchId = Database.executeBatch(AddEngine);
        }
        Test.stopTest();
	}
}