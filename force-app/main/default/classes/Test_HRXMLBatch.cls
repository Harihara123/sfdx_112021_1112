@isTest
public class Test_HRXMLBatch {
	static testMethod void validateHRXMLBatch() {
        Account testAccount = CreateTalentTestData.createTalent();
        
        Test.startTest();
        HRXMLBatch hrxml = new HRXMLBatch('SELECT Id from Account where RecordType.Name=\'Talent\' and Id=\'' + testAccount.Id + '\'',false);
        Database.executeBatch(hrxml);
        Account hrxmlAct = [SELECT Id FROM Account WHERE Id = :testAccount.Id];
		// Commenting out for R1 since this assertion fails when the validation is run on Prod.
        //System.assert(hrxmlAct.Talent_HRXML__c != null, 'Generated HRXML is null!');
        Test.stopTest();
    }

    static testMethod void validateHRXMLBatch2() {
        Account testAccount = CreateTalentTestData.createTalent();
        
        Test.startTest();
        HRXMLBatch hrxml = new HRXMLBatch('SELECT Id from Account where RecordType.Name=\'Talent\' and Id=\'' + testAccount.Id + '\'',true);
        Database.executeBatch(hrxml);
        Account hrxmlAct = [SELECT Id FROM Account WHERE Id = :testAccount.Id];
        // Commenting out for R1 since this assertion fails when the validation is run on Prod.
        //System.assert(hrxmlAct.Talent_HRXML__c != null, 'Generated HRXML is null!');
        Test.stopTest();
    }
}