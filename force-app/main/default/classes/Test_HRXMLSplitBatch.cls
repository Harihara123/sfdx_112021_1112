/**
* Author: Akshay Konijeti
* Date: 12/20/2016
* UserStory: ATS-2754
* 
* Test Class for the Apex class HRXMLSplitBatch.
* 
*/
@isTest
public class Test_HRXMLSplitBatch {
    
    public static testmethod void testmethod1(){
        HRXMLSplitBatch.run('Select ID from Account ',10,true);
        
        
    }

    public static testmethod void testmethod2(){
        HRXMLSplitBatch.run('Select ID from Account ',10,false);
        
        
    }
    
}