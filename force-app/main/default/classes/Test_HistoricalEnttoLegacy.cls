@isTest(seealldata = false)
public class Test_HistoricalEnttoLegacy 
{
    static testMethod Void Test_HistoricalEnttoLegacy () 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();

        TestData TestCont = new TestData();

        testdata tdConts = new TestData();
        Contact TdContObj = TestCont.createcontactwithoutaccount(lstNewAccounts[0].Id); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;

        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        string rectype='Talent';
        List<Contact> TalentcontLst = tdConts.createContacts(rectype);


        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';

                lstOpportunity.add(newOpp);
            }     
        }


        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Qualified';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Test Data should not be less than 25. Please ensure you have Description field not less than 25';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';

                lstOpportunity.add(newOpp);
            }     
        }
        
        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 2; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newopp.START_DATE__C=closingdate;
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Qualification__c = 'tttt';
                newOpp.Req_Salary_Max__c =12345;
                newOpp.Req_Salary_Min__c=00001;
                newOpp.Req_Flat_Fee__c=1111;
                newOpp.EnterpriseReqSkills__c = '[{"name":"defense litigation","favorite":false}]';
                
                lstOpportunity.add(newOpp);
            }     
        }
        
        insert lstOpportunity;

        ProcessLegacyReqs__c p = new ProcessLegacyReqs__c();
        p.Name ='BatchEntReqLastrunTime';
        p.LastExecutedBatchTime__c = Datetime.newInstance(2018, 02, 22);
        insert p;

        string QueryReq = System.Label.ProcessEntReqs;
        Batch_HistoricalCopyEntReqToLegacy  batchJob = new Batch_HistoricalCopyEntReqToLegacy();
        batchjob.QueryReq = 'Select Id from Opportunity where Id != null and lastmodifiedby.name!=\'Batch Integration\'';
        database.executebatch(batchJob,200);

       
    }
    
    
}