@isTest(seealldata = false) //dt changed from seealldata = true

private class Test_IRCleanUpScheduledProcess
{
    /****************************************************************************************
    Method Name :  IRCleanupProcessTest()
    Methods covered : 
    ****************************************************************************************/
    static testMethod void IRCleanupProcessTest() 
    {
        IRCleanUpScheduledProcess sh = new IRCleanUpScheduledProcess();
        String sch = '0 0 23 * * ?';
        Test.StartTest();  
        system.schedule('execute', sch, sh );
        Test.StopTest();    
    }
    
    static testMethod void Batch_RetryOutOfSyncRecordsTest() 
    {
        RetryOuter_Scheduled_Click testBatch = new  RetryOuter_Scheduled_Click();
        String sch = '0 0 23 * * ?';
        Test.StartTest();       
        system.schedule('execute', sch, testBatch );        
        Test.StopTest();    
    }
    
    
    
}