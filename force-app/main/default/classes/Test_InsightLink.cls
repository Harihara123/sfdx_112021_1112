@isTest(seeAlldata= false)
public class Test_InsightLink {


    private static testMethod void TestInsightLink() {
    
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        
        Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'Interest', closedate = system.today()+1, Impacted_Regions__c = 'Aerotek Central');
            insert newopportunity; 
            Boolean isEnterprise = false;
            
             InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Global Account Plans',isEnterprise);
             InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Global Account Events',isEnterprise);
             InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Global Account Opportunities',isEnterprise);
             InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Global Account Reqs',isEnterprise);
             InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Associated Contacts',isEnterprise);
              InsightLinkController.geturlGlobalAccountPlans(lstNewAccounts[0].id,'Placed Candidates',isEnterprise);
             
             }
             
             private static testMethod void TestStrategicPlanLinks() {
              User TEKAMuser = TestDataHelper.createUser('System Administrator');
              TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        
        Opportunity newopportunity = new Opportunity(Name = 'Sample Opportunity', Accountid = lstNewAccounts[0].id, 
            Proposal_Support_Count__c = null, stagename = 'Interest', closedate = system.today()+1, Impacted_Regions__c = 'Aerotek Central');
            insert newopportunity;
            
            Id recordId = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Strategic Initiative').getRecordTypeId();
       
        Id recordId1 = Schema.SObjectType.Strategic_Initiative__c.getRecordTypeInfosByName().get('Account Plan').getRecordTypeId();
        test.startTest();
       Focus_NonFocus__c myCS2 =new Focus_NonFocus__c(name='Event Query',Query__c='select id from Event where Accountid in :AccountStrIdset AND ownerid in:strownerid');
        insert myCS2;
        
        FocusNonFocusBatchDate__c mycs1 = new FocusNonFocusBatchDate__c(name='BatchDate',BatchranDate__c=date.today());
        insert mycs1;
        //create account
        Account acc = TestDataHelper.createAccount();//name= Test Account;
        insert acc;
        // create strategic initiatives
        Strategic_Initiative__c si = new Strategic_Initiative__c(Name = 'Initiative1',Start_Date__c = system.Today() - 30,
        recordTypeId = recordId1,
        Impacted_Region__c = 'test',
            Account_Prioritization__c ='A',
        Primary_Account__c= acc.Id,
        OpCo__c='Aerotek');
        insert si;
        
        Strategic_Initiative_Member__c st = new Strategic_Initiative_Member__c( Strategic_Initiative__c=si.id,User__c=TEKAMuser.id);
        insert st;
            
            StrategicPlanLinksController.getStrategicReportUrl(si.id,'Related Account Activities');
            StrategicPlanLinksController.getRelatedAccountsWithReqsUrl(si.id,'Related Account Reqs');
            StrategicPlanLinksController.getTeamMembersActivityUrl(si.id,'Team Member Activities');
            StrategicPlanLinksController.getRelatedAccountOppReportUrl(si.id,'Related Account Opportunities');
            StrategicPlanLinksController.getTeamMembersActivityAtAccountsUrl(si.id,'Team Member Activities at Related Accounts');
            StrategicPlanLinksController.OppsOwnedURL(si.id,'Opportunities owned by Team Members at Related Account');
            
            
            
            
             }
             
             }