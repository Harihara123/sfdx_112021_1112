@isTest

private class Test_IntegrationResponseScheduledProcess
{
    /****************************************************************************************
    Method Name :  IntegrationResponseLogicTest()
    Methods covered : IntegrationResponseLogic of IntegrationResponseOuter_Scheduled_Click Apex Class 
    ****************************************************************************************/
    static testMethod void IntegrationResponseLogicTest() 
    {
        
        Account acc =  TestData.newAccount(1);
        acc.AccountServicesChange__c = system.now();
        insert acc;
        Account acc2 =  TestData.newAccount(1);
        acc2.AccountServicesChange__c = system.now();
        insert acc2;

        Reqs__c newreq = new Reqs__c (Account__c =  acc.Id, stage__c = 'Draft', Status__c = 'Open',                       
                Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
        insert newreq; 
        Reqs__c newreq1 = new Reqs__c (Account__c =  acc.Id, stage__c = 'Draft', Status__c = 'Open',                       
                Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
        insert newreq1;

       
        Integration_Response__c intResp = new Integration_Response__c();
        intResp.Error_Code__c = 'Random';
        intResp.Error_Description__c = 'Random';
        intResp.Object__c = 'Account';
        intResp.Object_Id__c = acc.id; 
        intResp.Status__c = 'Failed';
        intResp.Object_Last_Modified__c = system.now() ;
        insert intResp;

        Integration_Response__c intResp1 = new Integration_Response__c();
        intResp1.Error_Code__c = 'Random';
        intResp1.Error_Description__c = 'Random';
        intResp1.Object__c = 'Req';
        intResp1.Object_Id__c = newreq.id; 
        intResp1.Status__c = 'Failed';
        intResp1.Object_Last_Modified__c = system.now() ;
        insert intResp1;
        ProcessIntegrationResponseRecords__c pIR = new ProcessIntegrationResponseRecords__c();        
        pIr.Name = 'IdentifyOutOfSyncLastRunTime';
        pIr.Start_Time__c = system.now()-200;
        insert pIr;
        
        Test.StartTest();
        IntegrationResponseOuter_Scheduled_Click.execute();
        Test.stopTest();        
    }
    
    /****************************************************************************************
    Method Name :  RetryLogicTest()
    Methods covered : RetryLogic of RetryOutOfSyncRecords  Apex Class 
    ****************************************************************************************/
    
    static testMethod void RetryLogicTest()
    {        
        Account acc =  TestData.newAccount(1);
        acc.AccountServicesChange__c = system.now();
        insert acc;
        Reqs__c newreq = new Reqs__c (Account__c =  acc.Id, stage__c = 'Draft', Status__c = 'Open',                       
                Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
        insert newreq; 
        Retry__c retry = new Retry__c();
        retry.Id__c = acc.id; 
        retry.Status__c = 'Open';
        retry.Object__c = 'Account';       
        insert retry;
        Retry__c retry1 = new Retry__c();
        retry1.Id__c = newreq.id; 
        retry1.Status__c = 'Open';
        retry1.Object__c = 'Req';       
        insert retry1;
        
        Test.startTest();
        RetryOuter_Scheduled_Click.execute();
        Test.StopTest();
    }
    
    public static testMethod void testretryschedule() 
    {        
        RetryOuter_Scheduled_Click sh1 = new RetryOuter_Scheduled_Click();
        String sch1 = '0 0 23 * * ?';
        Test.StartTest();
        system.schedule('execute', sch1, sh1);
        Test.stopTest();
        
     }
     public static testMethod void testIntRespschedule() 
     {        
        ProcessIntegrationResponseRecords__c pIR = new ProcessIntegrationResponseRecords__c();       
        pIr.Start_Time__c = system.now()-200;
        pIr.Name = 'IdentifyOutOfSyncLastRunTime';
        insert pIr;
        IntegrationResponseOuter_Scheduled_Click sh2 = new IntegrationResponseOuter_Scheduled_Click();
        String sch2 = '0 0 23 * * ?';
        Test.StartTest();
        system.schedule('execute', sch2, sh2);
        Test.StopTest();
     }   
    
    
    
}