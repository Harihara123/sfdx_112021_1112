@isTest(seeAlldata=false)
public class Test_LTRProvisioner {
    @TestSetup
    static void makeData() {
        Profile s = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile sd = [SELECT Id FROM Profile WHERE Name='Single Desk 1'];
        User u = new User(Alias = 'admin', Email='test@allegisgroup.com',
            EmailEncodingKey='UTF-8', LastName='Test123', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = s.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='admin@allegisgroup.com.devtest'
        );
        insert u;

        User tu = new User(Alias = 'tu', Email='testuser@allegisgroup.com',
            EmailEncodingKey='UTF-8', LastName='TestU123', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = sd.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='testuser@allegisgroup.com.devtest' 
        );
        insert tu;
    }
    
    static testMethod void testLTRProvisionerCustomQuery() {
        User u = [SELECT Id from User where UserName='admin@allegisgroup.com.devtest'];
        Integer before = [select count() from PermissionSetAssignment where PermissionSet.Label = 'User Group B'];

        system.runAs(u) {
            Test.startTest();
            
            LTRProvisioner batch = new LTRProvisioner('SELECT Id from User where UserName=\'testuser@allegisgroup.com.devtest\'');
            Id batchId = Database.executeBatch(batch);
            Test.stopTest();

            Integer after = [select count() from PermissionSetAssignment where PermissionSet.Label = 'User Group B'];
            System.assertEquals(1, after - before, 'Provisioned user count is not equal to 1');
            
            AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CompletedDate
                                            FROM AsyncApexJob WHERE Id = :batchId];

            System.assertEquals('Completed', a.Status, 'Batch job status is not Completed');
            System.assertEquals(0, a.NumberOfErrors, 'Batch job NumberOfErrors is not 0');
            System.assertEquals(1, a.JobItemsProcessed, 'Batch job JobItemsProcessed is not 1');
            System.assertEquals(1, a.TotalJobItems, 'Batch job TotalJobItems is not 1');
        }
    }

}