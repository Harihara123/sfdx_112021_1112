@isTest
public class Test_LWCSubmittalController {
    @TestSetup
    static void testSetup() {
        
        insert new DRZSettings__c(Name = 'OpCo', Value__c='Aerotek, Inc;TEKsystems, Inc.');
        Submittal_Status_Matrix__mdt ssm = new Submittal_Status_Matrix__mdt();
        List<String> lst = SubmittalStatusController.getAvailableStatuses('Allegis Global Solutions, Inc.', 'Applicant', True);
        
        List<Account> accList = new List<Account>();
        List<Opportunity> opplist = new List<Opportunity>();
        List<Order> orderList = new List<Order>();
        Account clientAcc = TestData.newAccount(1, 'Client');
        clientAcc.Talent_Ownership__c = 'AP';
        clientAcc.Talent_Committed_Flag__c = true;
        accList.add(clientAcc);
        
        Account talentAcc = TestData.newAccount(1, 'Talent');
        talentAcc.Talent_Ownership__c = 'AP';
        talentAcc.Talent_Committed_Flag__c = true;      
        accList.add(talentAcc);
        
        Insert accList;
        
        Contact ct = TestData.newContact(accList[1].id, 1, 'Talent'); 
        ct.Other_Email__c = 'other0@testemail.com';
        ct.Title = 'test title';
        ct.MailingState = 'test text';
        
        insert ct;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'TESTOPP1';
        opp1.StageName = 'Interviewing';
        opp1.AccountId = accList[0].id;
        opp1.Req_Product__c = 'Permanent';
        opp1.CloseDate = System.today();
        opp1.OpCo__c = 'Major, Lindsey & Africa, LLC';
        opplist.add(opp1);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'TESTOPP2';
        opp2.StageName = 'Offer Accepted';
        opp2.AccountId = accList[0].id;
        opp2.Req_Product__c = 'Permanent';
        opp2.CloseDate = System.today();
        opp2.OpCo__c = 'Major, Lindsey & Africa, LLC';
        opplist.add(opp2);
        insert opplist;
        
        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;
        
        Order o = new Order();
        o.Name = 'TestOrder';
        o.AccountId = accList[0].id;
        o.ShipToContactId = ct.Id;
        o.OpportunityId = opp1.Id;
        o.Status = 'Linked';
        o.EffectiveDate = System.today();
        o.RecordTypeId = recTypeId;
        orderList.add(o);
        
        Order o2 = new Order();
        o2.Name = 'TestOrder';
        o2.AccountId = accList[0].id;
        o2.ShipToContactId = ct.Id;
        o2.OpportunityId = opp2.Id;
        o2.Status = 'Linked';
        o2.EffectiveDate = System.today();
        o2.RecordTypeId = recTypeId;
        orderList.add(o2);
        insert orderList;
        
        String orderStrg = JSON.serialize(o);
        
        DateTime startDateTime = Datetime.now();
        DateTime endDateTime = startDateTime.addHours(1);
        
        Event evt = new Event();
        evt.StartDateTime = Datetime.now();
        evt.EndDateTime = Datetime.now().addHours(1);
        evt.Submittal_Interviewers__c = 'AKR';
        evt.Activity_Type__c = 'Interviewing';
        evt.WhoId = ct.Id;
        evt.WhatId = o.Id;
        insert evt;
    }
    
    @isTest
    public static void getOrderRecords() {
        Test.startTest();
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];
        Opportunity opp1 = [select Id, name from opportunity where Name='TESTOPP1' limit 1];
        
		
        LWCSubmittalController.getOrderRecords(con.Id, '','Submittal');
        LWCSubmittalController.getOrderRecords('xyz','','Submittal');
		LWCSubmittalController.getOrderRecords(opp1.Id,'Opportunity','application');
        LWCSubmittalController.getOrderRecords(opp1.Id,'Opportunity','Submittal');
        Test.stopTest();
        
    }
    
    @isTest
    public static void updateSubmittalRecord() {
        Test.startTest();
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];
		Opportunity opp1 = [select Id, name from opportunity where Name='TESTOPP1' limit 1];
        
        Order o =[select id from Order where ShipToContactID =: con.Id limit 1];
        
        
        LWCSubmittalController.updateSubmittalRecord(o, con.Id,'',3,'Submittal');
		LWCSubmittalController.updateSubmittalRecord(o, opp1.Id,'Opportunity',3,'Submittal');
        LWCSubmittalController.updateSubmittalRecord(o, opp1.Id,'Opportunity',3,'application');
        Test.stopTest();
        
    }
    
    @isTest
    public static void getOrderHistoryRecord() {
        
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];
        Order o =[select id from Order where ShipToContactID =: con.Id limit 1];
        
        Test.startTest();
        LWCSubmittalController.getOrderHistoryRecords(o.Id);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void updateOrderHistoryRecord() {
        Test.startTest();
        Contact con = [SELECT Id FROM Contact WHERE Other_Email__c = 'other0@testemail.com' LIMIT 1];
        Order o =[select id from Order where ShipToContactID =: con.Id limit 1];
        Event ev =[select id from Event where WhatId =: o.Id limit 1];
        
        
        LWCSubmittalController.updateOrderHistoryRecord(ev);
        LWCSubmittalController.updateOrderHistoryRecord(null);
        
        Test.stopTest();
        
    }
    
}