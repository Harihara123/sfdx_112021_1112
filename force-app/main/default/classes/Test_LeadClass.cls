@isTest(seeAlldata=true)
private class Test_LeadClass {

    static User user = TestDataHelper.createUser('Aerotek AM');    
    
    static testMethod void Test_Lead()
    {
        Test.startTest();    
        if(user != Null)
        {
            system.runAs(user)
            {         
                Lead newLead = new Lead(LastName = 'Test',Company = 'Test', City = 'Test', 
                    Country = 'Test',PostalCode = '2345', State = 'Test', Street = 'Test' );
                insert newLead;
                Validate_LeadConversion.ValidateConversion(newLead.Id);  
                
                Lead errLead = new Lead(LastName = 'Test',Company = 'Test', City = 'Test', 
                Country = 'Test',PostalCode = '2345',
                Street = 'Test' );
                insert errLead;    
                
                try {
                    Validate_LeadConversion.ValidateConversion(errLead.Id);  
                } catch(exception e) {
                    }                
            }
        }
    }
}