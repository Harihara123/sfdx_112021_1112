/***************************************************************************************************************************************
* Name        - Test_MergeContacts
* Description - This test class covers the logic in the trigger : trg_ContactMerge_UpdateTMeetingsTMeals
*               It also covers the trigger : trg_Req_UpdateTotalFilledPositions_OnContact
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       3/26/2013             Created
* Dhruv                       4/01/2013             Modified to cover trg_Req_UpdateTotalFilledPositions_OnContact
*****************************************************************************************************************************************/


@isTest
Private class Test_MergeContacts{
     
     public static testMethod void testContactMerge() 
     {                
        Contact ct1 = new Contact(FirstName = 'Test1', LastName = 'Contact1',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        insert ct1;
        Contact ct2= new Contact(FirstName = 'Test2', LastName = 'Contact2',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        insert ct2;        
        Id  UserId = UserInfo.getUserId();
        
        List<Event> lstEvents = new List<Event>();
        List<Reqs__c> lstReqs = new List<Reqs__c>(); 
        
        Event ev1 = new Event(Description = 'This is a completed Event1', WhoId = ct1.Id, OwnerId = UserId, StartDateTime = System.now() - 30, EndDateTime = System.now() - 20, Subject = 'Test Meeting', Type = 'Meeting');
        //Event ev2 = new Event(Description = 'This is a completed Event2', WhoId = ct1.Id, OwnerId = UserId, StartDateTime = System.now() - 30, EndDateTime = System.now() - 20, Subject = 'Test Lunch', Type = 'Lunch');
        Event ev3 = new Event(Description = 'This is a completed Event3', WhoId = ct2.Id, OwnerId = UserId, StartDateTime = System.now() - 30, EndDateTime = System.now() - 20, Subject = 'Test Meeting', Type = 'Lunch');
        //Event ev4 = new Event(Description = 'This is a completed Event4', WhoId = ct2.Id, OwnerId = UserId, StartDateTime = System.now() - 30, EndDateTime = System.now() - 20, Subject = 'Test Lunch', Type = 'Lunch');
        
        Account acc1 = new Account(Name = 'Test Account 1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        insert acc1;
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account 1'];
        
        Reqs__c req1 = new Reqs__c(Account__c = acc.Id,Primary_Contact__c = ct1.Id ,Address__c = 'Test Address',City__c = 'Test City', State__c = 'Test State', Zip__c = '99999', Stage__c = 'Draft', Draft_Reason__c = 'Proactive Req', Positions__c = 10  );
        Reqs__c req2 = new Reqs__c(Account__c = acc.Id,Primary_Contact__c = ct1.Id ,Address__c = 'Test Address',City__c = 'Test City', State__c = 'Test State', Zip__c = '99999', Stage__c = 'Draft', Draft_Reason__c = 'Proactive Req', Positions__c = 10  );
        //Reqs__c req2 = new Reqs__c(Account__c = acc.Id,Primary_Contact__c = ct1.Id,Address__c = 'Test Address',City__c = 'Test City', State__c = 'Test State', Zip__c = '99999', Stage__c = 'Draft', Draft_Reason__c = 'Proactive Req', Positions__c = 10 );
        //Reqs__c req3 = new Reqs__c(Account__c = acc.Id,Primary_Contact__c = ct2.Id,Address__c = 'Test Address',City__c = 'Test City', State__c = 'Test State', Zip__c = '99999', Stage__c = 'Draft', Draft_Reason__c = 'Proactive Req', Positions__c = 10);
        //Reqs__c req4 = new Reqs__c(Account__c = acc.Id,Primary_Contact__c = ct2.Id,Address__c = 'Test Address',City__c = 'Test City', State__c = 'Test State', Zip__c = '99999', Stage__c = 'Draft', Draft_Reason__c = 'Proactive Req', Positions__c = 10);
        lstReqs.Add(req1);
        lstReqs.Add(req2);
        //lstReqs.Add(req3);
        //lstReqs.Add(req4);
        insert lstReqs;
        
        req1.Filled__c = 3;
        req1.Primary_contact__c = ct2.id;
        req2.Primary_contact__c = ct2.id;
        //req2.Filled__c = 2;
        //req3.Filled__c = 4;
        //req4.Filled__c = 3;
        
        update lstReqs;
        
        req1.Filled__c = 4;
        update req1;
       
        lstEvents.Add(ev1);
        //lstEvents.Add(ev2);
        lstEvents.Add(ev3);
        //lstEvents.Add(ev4);
         
        insert lstEvents;
               
        Test.StartTest();
        TriggerStopper.updateContactstopper = False;
        merge ct1 ct2;
        delete req1;
        undelete req1;
        Test.StopTest();      
     }   
}