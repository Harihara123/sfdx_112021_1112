@isTest(seealldata = false)
public class Test_OpportunityTeamMemberTriggerHandler{
    static testMethod Void Test_DeleteReqTeamMembers() {
        Connected_Data_Export_Switch__c ConnectedDataExportSwitch = new Connected_Data_Export_Switch__c(Data_Extraction_Insert_Flag__c = False, Name = 'DataExport',PubSub_URL__c='https://pubsub.googleapis.com');
        insert ConnectedDataExportSwitch;
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Opportunity> lstOpportunitys = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();

        TestData TestCont = new TestData();

        testdata tdConts = new TestData();
        string recTypes = 'Recruiter';
        list<Contact> TdContObj = TestCont.createContacts(recTypes); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;

        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];

        for(Account Acc : TdAccts.createAccounts()) {       
            for(Integer i=0;i < 1; i++){
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunities'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = TdContObj[0].Name;
                newOpp.Opco__c = 'Aerotek, Inc.';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Req_Duration_Unit__c = 'Day(s)';
                lstOpportunitys.add(newOpp);
            }     
        }


        insert lstOpportunitys;
        
        List<OpportunityTeamMember> lstOTMs = new List<OpportunityTeamMember>();
        
        for(Opportunity opp: lstOpportunitys){
             OpportunityTeamMember otm = new OpportunityTeamMember();
              otm.OpportunityId = opp.id;
              otm.teammemberrole  = 'Recruiter';
              otm.userid = Userinfo.getuserid();
              lstOTMs.add(otm);
        }
        insert lstOTMs;
        
        User_Organization__c newOffices = new User_Organization__c();
            newOffices.Office_Name__c = 'TEST';
            newOffices.Active__c = true;
            insert newOffices ; 
        
        Reqs__c recRequisitions = new Reqs__c(
            Account__c = lstNewAccounts[0].Id, Address__c = 'US', Stage__c = 'Qualified', 
            Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
            Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,EnterpriseReqId__c = lstOpportunitys[0].id,
            Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample', Organization_Office__c = newOffices.id);
            insert recRequisitions;
            
        
        Req_Team_Member__c rtms = new Req_Team_Member__c();
            rtms.Requisition__c = recRequisitions.id;
            rtms.Requisition_Team_Role__c = 'Recruiter';
            rtms.User__c = Userinfo.getuserid();
            rtms.Contact__c=TdContObj[0].id;
            insert rtms;
            
            TriggerStopper.deleteRecursiveOpportunityTeamMembersStopper = true;
            Test.StartTest();
            try{
                Delete lstOTMs[0];
                Delete  rtms;
            }
            catch(Exception ee)
            {}
        Test.StopTest();
    }

}