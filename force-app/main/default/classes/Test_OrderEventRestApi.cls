@isTest
private class Test_OrderEventRestApi {

    private static String domainURL = '/services/apexrest/getorderevent';
    private static String methodType = 'GET'; 
    
    private static RestRequest req = new RestRequest(); 
    private static RestResponse res = new RestResponse();
    
    static {
        req.httpMethod = methodType; 
        req.requestURI = domainURL;  
    }
    
    static testMethod void testGetOrderEventNoParam() {
        req.addParameter('orderId', '');
       
        RestContext.request = req;
        RestContext.response = res;
        
        try {
        	Event event = ATS_OrderEventRestApi.getOrderEvent();
            System.AssertEquals(5, 4);  
        } catch (Exception e) { 
          	System.AssertEquals(5, 5);
        }
	}

	static testMethod void testGetOrderEvent() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        Account account =  createAccount();
        //Contact contact = createContact( account);
        Order order = getJobOrderId(account ); 
        order = createSubmitAssociated(order);
        String jobOrderId = createOrderEventNotProceeding(order, account);

        req.addParameter('orderId', jobOrderId);
       
        RestContext.request = req;
        RestContext.response = res;
        
        try {
        	Event event = ATS_OrderEventRestApi.getOrderEvent();
            // System.Debug(logginglevel.warn, '>>>> The event what Id Is :'  +  event);
            //System.Debug(logginglevel.warn, '>>>> The event  Id Is :'  +  event.Id);

            System.AssertEquals(event.whatId, jobOrderId);  
        } catch (Exception e) { 
             
          System.AssertEquals(e.getMessage(), 'Failed to Construct the query'); 
          System.AssertEquals(4, 5);
        }
	}

    static Account createAccount() {
    	Account account = CreateTalentTestData.createTalentAccount('RPO'); 
     	Contact contact = CreateTalentTestData.createTalentContact(account); 

     	return account; 
  	}

  	static Order getJobOrderId(Account account) {
       	ATS_Job__c job     = CreateTalentTestData.createATSJob();
       	Order      order   = CreateTalentTestData.associateTalentToJob(account, job);
       	return order; 
 	}
  
	static Order createSubmitAssociated(Order order) {
        order  = CreateTalentTestData.submitAssociated(order);
     	return order; 
  	}
  
  	static String createOrderEventNotProceeding(Order order,  Account talent) {
  		order = CreateTalentTestData.createOrderEventNotProceeding(Order, talent);
      	return order.Id;
  	}
}