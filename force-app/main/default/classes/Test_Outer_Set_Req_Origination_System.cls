@isTest

private class Test_Outer_Set_Req_Origination_System{
    /****************************************************************************************
    Method Name :  Batch_Set_Req_Origination_System()
    Methods covered : 
    ****************************************************************************************/
    static testMethod void Outer_Set_Req_Origination_System() 
    {
        Outer_Set_Req_Origination_System sh = new Outer_Set_Req_Origination_System();
        String sch = '0 0 23 * * ?';
        Test.StartTest();  
        system.schedule('execute', sch, sh );
        Test.StopTest();    
    }
    
 }