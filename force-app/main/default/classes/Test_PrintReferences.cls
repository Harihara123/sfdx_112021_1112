@isTest
private class Test_PrintReferences {
    static testMethod void validatePrintReferences() {
        
        // Create new Account
        Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        insert testAccount;
        
        // Create new Contact
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.Id;
        testContact.Email = 'testContact@email.com';
        testContact.LastName = 'testLastName';
        testContact.FirstName = 'testFirstName';
        insert testContact;
        
        // Create new Talent_Recommendation__c
        Talent_Recommendation__c testReference = new Talent_Recommendation__c();
        testReference.Talent_Contact__c = testContact.Id;
        insert testReference;
        
        // Set the Apex Page
        PageReference pageRef = Page.PrintReferencesAsPdf;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('refIds', testReference.Id);
        
        // Create instance of PrintReferences
        PrintReferences printRef = new PrintReferences();
        List<Talent_Recommendation__c> references = printRef.getReferences();
        
        // Ensure references list is non-null
        System.assertNotEquals(null, references);
        
        // Ensure there is a reference in the list
        Talent_Recommendation__c reference = references.get(0);
        System.assertNotEquals(null, reference.Id);
        
        // Ensure the reference returned matches our testReference
        System.assertEquals(testReference.Id, reference.Id);
    }
}