/*******************************************************************
* Name  : Test_ProcessEnterpriseReqHRXMLBatch
* Author: Preetham Uppu 
* Date  : Feb 06, 2017
* Details : Test class for ProcessEnterpriseReqHRXML Batch
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_ProcessEnterpriseReqHRXMLBatch {
    
    //variable declaration
        static User testAdminUser= TestDataHelper.createUser('System Administrator'); 
        
    static testMethod void test_InsertHRXMLForEnterpriseReq(){
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        //Create Custom Setting
        List<Opportunity> lstOpportunity = new List<Opportunity>();
         
        ProcessReqHRXML__c p = new ProcessReqHRXML__c();
        p.LastExecutedTime__c = System.now();
        p.Name = 'HRXMLLastRunTime';
        
        Test.starttest();
        insert p;
        
          if(testAdminUser != Null) {
            system.runAs(testAdminUser) {
                TestData TdAccts = new TestData(2);
                testdata tdConts = new TestData();
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Admin');
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     string rectype='Talent';
                     List<Contact> TalentcontLst = tdConts.createContacts(rectype);
                     
                for(Account Acc : TdAccts.createAccounts()) {       
                  for(Integer i=0;i < 2; i++)
                  {
                    Opportunity newOpp = new Opportunity();
                    newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                    newOpp.Accountid = Acc.Id;
                    newOpp.RecordTypeId = ReqRecordType;
                    Date closingdate = system.today();
                    newOpp.CloseDate = closingdate.addDays(25);
                    newOpp.StageName = 'Draft';
                    newOpp.Req_Total_Positions__c = 2;
                    newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                    newOpp.Req_Job_Description__c = 'Testing';
                    newOpp.Req_HRXML_Field_Updated__c = true;
                    newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                    newOpp.Opco__c = 'Allegis Partners, LLC';
                    newOpp.BusinessUnit__c = 'Board Practice';
                    newOpp.Req_Product__c = 'Permanent';
                    newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                    newOpp.Req_Worksite_Street__c = '987 Hidden St';
                    newOpp.Req_Worksite_City__c = 'Baltimore';
                    newOpp.Req_Worksite_Postal_Code__c = '21228';
                    newOpp.Req_Worksite_Country__c = 'United States';
                   /* (Name = 'New ReqOpportunity'+ string.valueof(i) , Accountid = Acc.id,
                         RecordTypeId = ReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                         Req_Total_Positions__c = 1,  Req_Client_Job_Title__c = titleList[0].Id,
                         Req_Job_Description__c = 'Testing', Req_HRXML_Field_Updated__c = true,
                         Req_Hiring_Manager__c = TalentcontLst[0].); */
                         
                        lstOpportunity.add(newOpp);
                  }     
                }
                  insert lstOpportunity; 
                    
                  Batch_ProcessEnterpriseReqHRXML b = new Batch_ProcessEnterpriseReqHRXML();
                  Database.executeBatch(b);
                 Test.stopTest();    
           } // End of User Scope
           
         }
    } 
    
     static testMethod void test_BatchFinishMethodForEnterpriseReq(){
          DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        //Create Custom Setting
        List<Opportunity> lstOpportunity = new List<Opportunity>();
         
        ProcessReqHRXML__c p = new ProcessReqHRXML__c();
        p.LastExecutedTime__c = System.now();
        p.Name = 'HRXMLLastRunTime';
        
        Test.starttest();
        insert p;
        
          if(testAdminUser != Null) {
            system.runAs(testAdminUser) {
                TestData TdAccts = new TestData(2);
                testdata tdConts = new TestData();
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Admin');
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     string rectype='Talent';
                     List<Contact> TalentcontLst = tdConts.createContacts(rectype);
                     
                for(Account Acc : TdAccts.createAccounts()) {       
                  for(Integer i=0;i < 2; i++)
                  {
                    Opportunity newOpp = new Opportunity();
                    newOpp.Name = 'Passed Opp'+ string.valueof(i);
                    newOpp.Accountid = Acc.Id;
                    newOpp.RecordTypeId = ReqRecordType;
                    Date closingdate = system.today();
                    newOpp.CloseDate = closingdate.addDays(25);
                    newOpp.StageName = 'Draft';
                    newOpp.Req_Total_Positions__c = 2;
                    newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                    newOpp.Req_Job_Description__c = 'Sending Error Message for Passed Test';
                    newOpp.Req_HRXML_Field_Updated__c = true;
                    newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                    newOpp.Opco__c = 'Allegis Partners, LLC';
                    newOpp.BusinessUnit__c = 'Board Practice';
                    newOpp.Req_Product__c = 'Permanent';
                    newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                    newOpp.Req_Worksite_Street__c = '987 Hidden St';
                    newOpp.Req_Worksite_City__c = 'Baltimore';
                    newOpp.Req_Worksite_Postal_Code__c = '21228';
                    newOpp.Req_Worksite_Country__c = 'United States';
                   
                         
                        lstOpportunity.add(newOpp);
                  }     
                }
                  insert lstOpportunity; 
                    
                  Batch_ProcessEnterpriseReqHRXML b = new Batch_ProcessEnterpriseReqHRXML();
                  b.isHRXMLJobException = true;
                  Database.executeBatch(b);
                 Test.stopTest();    
           } // End of User Scope
           
         }
    } 
   
}