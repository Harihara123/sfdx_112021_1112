@isTest(seealldata = false)
private class Test_ProcessEnterpriseReqHRXMLSchedular
{
    //variable declaration
    static User user = TestDataHelper.createUser('System Integration'); 
    
    static testMethod void ProcessEnterpriseReqHRXMLTest() 
    {
         DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
         Test.startTest();
         List<Opportunity> lstOpportunity = new List<Opportunity>();
         List<Order> lstOrder = new List<Order>();
         
           //Create Custom Setting
         
        ProcessReqHRXML__c p = new ProcessReqHRXML__c();
        p.LastExecutedTime__c = System.now();
        p.Name = 'HRXMLLastRunTime';
        
        insert p;
         
          if(user != Null) {
           
                  TestData TdAccts = new TestData(2);
                  testdata tdConts = new TestData();
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Admin');
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     string rectype='Talent';
                     List<Contact> TalentcontLst = tdConts.createContacts(rectype);
                     
                for(Account Acc : TdAccts.createAccounts()) {       
                  for(Integer i=0;i < 2; i++)
                  {
                    Opportunity newOpp = new Opportunity();
                    newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                    newOpp.Accountid = Acc.Id;
                    newOpp.RecordTypeId = ReqRecordType;
                    Date closingdate = system.today();
                    newOpp.CloseDate = closingdate.addDays(25);
                    newOpp.StageName = 'Draft';
                    newOpp.Req_Total_Positions__c = 2;
                    newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                    newOpp.Req_Job_Description__c = 'Testing';
                    newOpp.Req_HRXML_Field_Updated__c = true;
                    newOpp.Req_Hiring_Manager__c = TalentcontLst[0].Name;
                    newOpp.Opco__c = 'Allegis Partners, LLC';
                    newOpp.BusinessUnit__c = 'Board Practice';
                    newOpp.Req_Product__c = 'Permanent';
                    newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                    newOpp.Req_Worksite_Street__c = '987 Hidden St';
                    newOpp.Req_Worksite_City__c = 'Baltimore';
                    newOpp.Req_Worksite_Postal_Code__c = '21228';
                    newOpp.Req_Worksite_Country__c = 'United States';
                 
                        lstOpportunity.add(newOpp);
                  }     
                }
                  insert lstOpportunity; 
                  
                  
                ScheduleBatch_ProcessEnterpriseReqHRXML testSchedular = new  ScheduleBatch_ProcessEnterpriseReqHRXML();
                 String sch = '0 0 23 * * ?';
                 system.schedule('Execute', sch, testSchedular);        
                 
             Test.stopTest();
    
       }
   }
   
}