@isTest(seealldata = false)
private class Test_ProcessOpportunityRecordsSchedular
{
    //variable declaration
    static User user = TestDataHelper.createUser('System Integration'); 
    
    static testMethod void ProcessOpportunityRecordsSchedularTest() 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
		DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1;
        Test.startTest();
         List<Opportunity> lstOpportunity = new List<Opportunity>();
         List<Order> lstOrder = new List<Order>();
         
          if(user != Null) {
            system.runAs(user) {
                TestData TdAcc = new TestData(10);
                 // Get the Req recordtype Id from a name        
                     string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                     
                     Job_Title__c jt = new Job_Title__c(Name ='Developer');
                    
                     insert jt;
                     
                     List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
                     List<Order_Status__mdt> listOrderStatuses = [SELECT OrderStatusKey__c, OpportunityTotalStatus__c from Order_Status__mdt];
                     
                for(Account Acc : TdAcc.createAccounts()) {       
                  for(Integer i=0;i < listOrderStatuses.size(); i++)
                  {
                    Opportunity NewOpportunity = new Opportunity(Name = 'New Opportunity'+ string.valueof(i) , Accountid = Acc.id,
                         RecordTypeId = ReqRecordType, CloseDate = system.today()+5,StageName = 'Draft',
                         Req_Total_Positions__c = 1,  Req_Client_Job_Title__c = titleList[0].Id,
                         Req_Job_Description__c = 'Testing');
                         
                        lstOpportunity.add(NewOpportunity);
                  }     
                }
                  insert lstOpportunity;
                  
                  integer Count = 0;
                  Id OSID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();//added RecType Condition by akshay ATS - 3344
        
                  for(Order_Status__mdt os: listOrderStatuses)
                  {
                     
                     Order NewOrder = new Order(Name = 'New Order' + string.valueof(Count), EffectiveDate = system.today()+1,
                                                Status = os.OrderStatusKey__c,AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[Count].Id,RecordtypeId = OSID);
                     lstOrder.add(NewOrder);
                     Count = Count + 1;
                  }
                  
             insert lstOrder;
        
              
                
                ProcessOpportunityRecords__c p = new ProcessOpportunityRecords__c();
                p.LastExecutedTime__c  = System.now();
                p.Name = 'SyncOpportunityLastRunTime';
                
                insert p;
                
                ApexCommonSettings__c a = new ApexCommonSettings__c();
                a.SoqlQuery__c  = 'Select Id, OpportunityId from Order where OpportunityId != null and SystemModstamp > :dtLastBatchRunTime';
                a.Name = 'ProcessOppRecordsQuery';
                insert a;
                
                BatchProcessOpportunityRecordsScheduler testSchedular = new  BatchProcessOpportunityRecordsScheduler();
                 String sch = '0 0 23 * * ?';
                 system.schedule('Execute', sch, testSchedular);        
                 
             Test.stopTest();
       
           }
 
       }
    }
}