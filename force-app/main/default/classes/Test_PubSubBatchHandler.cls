@isTest
public class Test_PubSubBatchHandler  {

     private static final Integer accountRecordSize = 2; 
     private static List<Account> listAccounts  = new List<Account>();
     private static List<Contact> listContact  = new List<Contact>();
     private static RecordType rtcontact = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Contact' limit 1];
     private static RecordType rtAccount = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1];


    public static testmethod void testInsertDataExteractionRecord() {

        stageData(false);
        createAccount();

        String recordName = 'Account';
        List<Account> lisOb = listAccounts;

        PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 

        List<SObject> lisSObFromQuery = Database.query('Select Name, Id, Object_Ref_ID__c, Object_Name__c from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 

        System.assert(lisSObFromQuery.size() == lisOb.size());

    }
    
    public static testmethod void testInsertDataExteractionRecordById() {
        stageData(false);
        createAccount();

        String recordName = 'Account';
        List<Id> listId = new List<Id>();
        for (Account a: listAccounts) {
            listId.add(a.Id);
        }

        PubSubBatchHandler.insertDataExtractionRecordByIds(listId, recordName); 

        List<SObject> lisSObFromQuery = Database.query('Select Name, Id, Object_Ref_ID__c, Object_Name__c from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 

        System.assert(lisSObFromQuery.size() == listId.size());
    }

    public static testmethod void testInsertDataExteractionRecordOnFailedPubSubCalls() {

               stageData(false);
               createAccount();

               String recordName = 'Account';
               List<Account> lisOb = listAccounts;

               Set<String> strList = new Set<String>();


               PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 
               PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 
         
               for (Account act : lisOb) {
                   strList.add(act.Id);
               }


               Integer recordCount = PubSubBatchHandler.getAvailableRecordsTobeProcessed(false);
                            System.debug(logginglevel.ERROR, ' Failed recordCount  '  + recordCount);

               System.assert(recordCount == 6);

               PubSubBatchHandler.insertDataExteractionRecordOnFailedPubSubCalls(strList, recordName);

               recordCount = PubSubBatchHandler.getAvailableRecordsTobeProcessed(false);
               System.assert(recordCount == 6);

               strList.add('someArbtIdsvalue'); //this should be the only record that gets inserted

               PubSubBatchHandler.insertDataExteractionRecordOnFailedPubSubCalls(strList, recordName);

               List<SObject> lisSObFromQuery = Database.query('Select Name, Id, Object_Ref_ID__c, Object_Name__c from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 

               System.assert(lisSObFromQuery.size() == 7);

               lisSObFromQuery = Database.query('Select Name, Id, Object_Ref_ID__c, Object_Name__c from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\' and Not_Processed__c = \'1\' '); 
               System.assert(lisSObFromQuery.size() == 1);

    }


    public static testmethod void testGetQueuedAsyncApexJob(){
         stageData(false);
         Integer co = PubSubBatchHandler.getQueuedAsyncApexJob();
         Decimal defaultMax  = Connected_Data_Export_Switch__c.getInstance('DataExport').Max_Allowed_Queued_Job__c;
         System.assert(co < defaultMax);


      }


    public static testmethod void testAllowedDMLRows(){

        stageData(false);

        Integer allowedDMLRow =  PubSubBatchHandler.getAllowedDMLRows(800);
        System.assert(allowedDMLRow == 800); 

        allowedDMLRow =  PubSubBatchHandler.getAllowedDMLRows(15000);
        System.assert(allowedDMLRow == 9999);

        allowedDMLRow =  PubSubBatchHandler.getAllowedDMLRows(9999);
        System.assert(allowedDMLRow == 9999);


        allowedDMLRow =  PubSubBatchHandler.getAllowedDMLRows(0);
        System.assert(allowedDMLRow == 0);

        stageData(false);
        createAccount();

        allowedDMLRow =  PubSubBatchHandler.getAllowedDMLRows(10000);
        System.assert(allowedDMLRow == 9988);

    }

    public static testmethod void testGetAvailableRecordsTobeProcessed() {

        stageData(false);
        createAccount();
        String recordName = 'Account';
        List<Account> lisOb = listAccounts;

        PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 
        // just to make sure we hae dupliate 
        PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 

        Integer recordCount = PubSubBatchHandler.getAvailableRecordsTobeProcessed(false);
        System.assert(recordCount == 6);

        Set<String> strList = new Set<String>();

        for (Account act : lisOb) {
                   strList.add(act.Id);
        }

        strList.add('someArbtIdsvalue'); //this should be the only record that gets inserted

        PubSubBatchHandler.insertDataExteractionRecordOnFailedPubSubCalls(strList, recordName);

        recordCount = PubSubBatchHandler.getAvailableRecordsTobeProcessed(false);
        System.assert(recordCount == 6);

        recordCount = PubSubBatchHandler.getAvailableRecordsTobeProcessed(true);
        System.assert(recordCount == 7);

    }


    public static testmethod void testGetQueryStr() {

       stageData(false);

       String queryWithoutFailedRecords = 'Select Object_Ref_ID__c from Data_Extraction_EAP__c where Object_Name__c =:objName  AND Not_Processed__c = null order by CreatedDate ASC limit ' + String.valueOf(PubSubBatchHandler.BatchQueryMaxCount);
       String queryWithFailedRecords    = 'Select Object_Ref_ID__c from Data_Extraction_EAP__c where Object_Name__c =:objName  order by CreatedDate ASC limit ' + String.valueOf(PubSubBatchHandler.BatchQueryMaxCount);

       String queryStr = PubSubBatchHandler.getQueryStr(false);
       System.assert(queryStr == queryWithoutFailedRecords);

       queryStr = PubSubBatchHandler.getQueryStr(true);
       System.assert(queryStr == queryWithFailedRecords);

    }
     

    public  static void stageData(boolean includeFailRecords) {

         Connected_Data_Export_Switch__c cDES = new Connected_Data_Export_Switch__c();
         cDES.Name = 'DataExport';
         cDES.Data_Extraction_Insert_Flag__c = true; 
         cDES.Google_Pub_Sub_Call_Flag__c = false; 
         cDES.Data_Extraction_Query_Limit__c = 200; 
         cDES.Data_Extraction_Query_Flag__c = true; 
         cDES.Max_Allowed_Queued_Job__c = 10;
         cDES.PubSub_URL__c = 'URL';
         insert cDES;         
   }


   private static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
   }


   private  static String createPreferenceJSON() {
        //return '{"lastModified":{"modifiedBy":{"userId":"005J0000005d90PIAQ","name":"Nagesh Akula"},"modifiedDate":"2016-04-14T15:45:13.171Z"},"candidateOverview":"Candidate Overview","employabilityInformation":{"reliableTransportation":true,"eligibleIn":[],"drugTest":true,"backgroundCheck":true,"securityClearance":true},"qualifications":{"skills":["ABC","Test","Skill1","Skiil3","ABC Skill4","Java"],"languages":["english"]},"geographicPrefs":{"willingToRelocate":true,"desiredLocation":{"city":"Ellicott City","state":"Maryland","country":"United States"},"commuteLength":{"distance":"20","unit":"miles"},"nationalOpportunities":true},"employmentPrefs":{"goalsAndInterests":["Goals","Goal2","Gial3"],"desiredRate":{"amount":"55","rate":"hourly"},"desiredPlacementType":"contract","desiredSchedule":"full-time","mostRecentRate":{"amount":"55","rate":"hourly"}}}';
        return '{"languages":[],"geographicPrefs":{"nationalOpportunities":null,"geoComments":null,"desiredLocation":[{"country":null,"state":null,"city":null}],"commuteLength":{"unit":null,"distance":null}},"employabilityInformation":{"securityClearance":null,"reliableTransportation":null,"eligibleIn":[],"drugTest":null,"backgroundCheck":null}}';
    }


   public static List<Account> getAccounts(){
       createAccount();
       return listAccounts;

   }

   public static List<Account> getAccounts(Integer numbRecords){
       createAccount(numbRecords);
       return listAccounts;

   }

    private  static void createAccount(Integer numbRecords) {
        Integer counter = 0; 
        String randomStr = generateRandomString(5); 
        for (;counter < numbRecords; ++counter) { 
             Account testAccount = new Account(
                RecordTypeId = rtAccount.id,
                Name = randomStr + String.valueof(counter),
                Talent_Preference_Internal__c = createPreferenceJSON(),
                Desired_Rate_Frequency__c = 'hourly',
                Desired_Rate__c =counter
             );
             listAccounts.add(testAccount);
        }
        insert listAccounts; 
    }



   private  static void createAccount() {
        Integer counter = 0; 
        String randomStr = generateRandomString(5); 
        for (;counter <= accountRecordSize; ++counter) { 
             Account testAccount = new Account(
                RecordTypeId = rtAccount.id,
                Name = randomStr + String.valueof(counter),
                Talent_Preference_Internal__c = createPreferenceJSON(),
                Desired_Rate_Frequency__c = 'hourly',
                Desired_Rate__c =counter
             );
             listAccounts.add(testAccount);
        }
        insert listAccounts; 
    }


     private static void createContact() {

        Integer counter = 10; 
        
        for (Account acc : listAccounts) { 
            ++counter; 

            Contact testContact = new Contact(
                RecordTypeId = rtcontact.Id,
                AccountId = acc.Id,
                Email = acc.name + '_testContact@email.com',
                LastName = acc.name + '_testLastName',
                FirstName = acc.name + '_testFirstName',
                Title = 'ContactTitle',
                MobilePhone =  '96969696' + counter,
                HomePhone = '12345698' + counter,
                Phone =    '98765432' + counter,
                OtherPhone = '55555555'  + counter,
                Other_Email__c = acc.name + '_bca@bca.bca',
                MailingStreet = acc.name + ' ABC Way',
                MailingCity = acc.name +  ' Test City',
                MailingState = 'US.MD',
                MailingCountry = 'US',
                MailingLatitude = 33.386532,
                MailingLongitude = -112.215578
           );

           listContact.add(testContact);

        }

        insert listContact; 
    }

}