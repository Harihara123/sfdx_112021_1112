@isTest
public class Test_PubSubBatchRESTJSON  {

  private class PubSubRESTJSONResponse {
        public Integer AvilableRecorsToBeProcsses;
        public Integer BatchRecorsSizeToBeProcsses;
        public Integer NumberOfQueuedJobs;
        public Boolean DataExtractProcess_ON;
        public Integer TransactionTimeMilSec;
 }


  public static testMethod void testDoPost() {

      mockReqRes();

      Test.startTest();

         stageData(false, false);
         List<Account> lisOb = Test_PubSubBatchHandler.getAccounts();
         PubSubBatchHandler.insertDataExteractionRecord(lisOb, 'Account'); 
         PubSubBatchRESTJSON.doPost(); 

         String body = RestContext.response.responseBody.toString();
         PubSubRESTJSONResponse pubSubRESTJSONResponse = (PubSubRESTJSONResponse)JSON.deserialize(body, PubSubRESTJSONResponse.class);

         System.assert(pubSubRESTJSONResponse.AvilableRecorsToBeProcsses == 3); 
         System.assert(pubSubRESTJSONResponse.BatchRecorsSizeToBeProcsses == 200); 
         System.assert(pubSubRESTJSONResponse.TransactionTimeMilSec >  0); 
         System.assert(pubSubRESTJSONResponse.NumberOfQueuedJobs == 0); 
         System.assert(pubSubRESTJSONResponse.DataExtractProcess_ON == false); 

         Test.stopTest();

  }



  public static testmethod void testIsAllowedToCallFutureMethod() {
     stageData(true, true);

     Boolean isAllowedtoCallFuture = PubSubBatchRESTJSON.isAllowedToCallQueuable(0, 0); 
     System.assert(!isAllowedtoCallFuture); 

     isAllowedtoCallFuture = PubSubBatchRESTJSON.isAllowedToCallQueuable(1, 10);
     System.assert(!isAllowedtoCallFuture); 

     isAllowedtoCallFuture = PubSubBatchRESTJSON.isAllowedToCallQueuable(1, 9);
     System.assert(isAllowedtoCallFuture); 

     isAllowedtoCallFuture = PubSubBatchRESTJSON.isAllowedToCallQueuable(0, 5);
     System.assert(!isAllowedtoCallFuture); 

     stageData(true, false);
     isAllowedtoCallFuture = PubSubBatchRESTJSON.isAllowedToCallQueuable(1, 5);
     System.assert(!isAllowedtoCallFuture); 
  }


  private static String getJSONParam() {
    // return  '{"sObjectName":"JustATempObjectName","ignoreFields":[]}';

   return  '{"objectList":["Account","Contact","Opportunity","Reqs__c","User_Search_Feedback__c"],"processFailedRecord":false}';
  }

  private static void mockReqRes() {

         RestRequest req = new RestRequest(); 
         RestResponse res = new RestResponse();

         req.requestURI = '/services/apexrest/GooglePubSubBatchRESTHelper/';  //Request URL
         req.httpMethod = 'POST';//HTTP Request Type
         req.requestBody = Blob.valueof(getJSONParam());
         RestContext.request = req;
         RestContext.response= res;

  }


  private  static void stageData(boolean includeFailRecords, Boolean processRecords) {

         Connected_Data_Export_Switch__c cDES = new Connected_Data_Export_Switch__c();
         cDES.Name = 'DataExport';
         cDES.Data_Extraction_Insert_Flag__c = true; 
         cDES.Google_Pub_Sub_Call_Flag__c = false; 
         cDES.Data_Extraction_Query_Limit__c = 200; 
         cDES.Data_Extraction_Query_Flag__c = processRecords; 
         cDES.Max_Allowed_Queued_Job__c = 10;
         cDES.PubSub_URL__c = 'url';
         insert cDES;         
   }




}