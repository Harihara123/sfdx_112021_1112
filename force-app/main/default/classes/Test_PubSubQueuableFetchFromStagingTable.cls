@isTest
public class Test_PubSubQueuableFetchFromStagingTable  {

     private static final Integer accountRecordSize = 2; 
     private static List<Account> listAccounts  = new List<Account>();
     private static List<Contact> listContact  = new List<Contact>();
     private static RecordType rtcontact = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Contact' limit 1];
     private static RecordType rtAccount = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1];



     public static testmethod void testPubSubQueuableFetchFromStagingTableExcecute(){

        Integer count = 0; 
        Test.startTest();        
            stageData(false);

            String recordName = 'Account';
            List<Account> lisOb =  Test_PubSubBatchHandler.getAccounts();

            PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 
            // just to make sure we hae dupliate 
            PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 

            count = Database.countQuery('Select count() from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 
            System.assert(count == 6);

            String[] objectList = new String[] {'Account','Contact','Opportunity','Reqs__c','User_Search_Feedback__c'}; 
            Boolean processFailedRecord = false; 

            System.enqueueJob(new PubSubQueuableFetchFromStagingTable(objectList, processFailedRecord, 'test'));
         Test.stopTest();


          count = Database.countQuery('Select count() from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 
          System.assert(count == 0);


     }
      

      public static testmethod void testPubSubQueuable(){

        Integer count = 0; 
        Test.startTest();        
            stageData(false);

            String recordName = 'Account';
            List<Account> lisOb =  Test_PubSubBatchHandler.getAccounts(1);

            PubSubBatchHandler.insertDataExteractionRecord(lisOb, recordName); 

             Map<String, set<String>> objIdMap = new Map<String, Set<String>>(); 
            Set<String> setIds = new Set<String>();
            for (Account ac : lisOb) {
                 setIds.add(ac.Id);
            }
            objIdMap.put(recordName, setIds);
           

            count = Database.countQuery('Select count() from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\'');
             System.debug(logginglevel.WARN, ' Farid count '  + count);
 
            System.assert(count == 1);

             String[] objectList = new String[] {'Account','Contact','Opportunity','Reqs__c','User_Search_Feedback__c'}; 
            Boolean processFailedRecord = false; 


            System.enqueueJob(new PubSubQueuableFetchFromStagingTable(objectList, processFailedRecord, 'test'));


         Test.stopTest();


          count = Database.countQuery('Select count() from Data_Extraction_EAP__c where Object_Name__c = \'' + recordName + '\''); 
           System.debug(logginglevel.WARN, ' Farid count '  + count);
          System.assert(count == 0);


     }


  
     private  static void stageData(boolean googleCallenabled) {

         Connected_Data_Export_Switch__c cDES = new Connected_Data_Export_Switch__c();
         cDES.Name = 'DataExport';
         cDES.Data_Extraction_Insert_Flag__c = true; 
         cDES.Google_Pub_Sub_Call_Flag__c = googleCallenabled; 
         cDES.Data_Extraction_Query_Limit__c = 200; 
         cDES.Data_Extraction_Query_Flag__c = true; 
         cDES.Max_Allowed_Queued_Job__c = 10;
         cDES.PubSub_URL__c = 'https://pubsub.googleapis.com/v1/projects/connected-ingest-test/topics/ingest10x-load:publish?access_token=';
         insert cDES;         
   }


}