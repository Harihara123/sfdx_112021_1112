@isTest
public with sharing class Test_QueableJobPostingOppUpdate {
    @IsTest
    public static void method1() {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
            insert DRZSettings;
            DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
            List<Opportunity> lstOpportunitys = new List<Opportunity>();
            Account clientAcc = TestData.newAccount(1, 'Client');
            insert clientAcc;

            Contact con = TestData.newContact(clientAcc.id, 1, 'Recruiter');
            Insert con; 
        
            String reqRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();

            Opportunity newOpp = new Opportunity();
            newOpp.Name = 'New ReqOpportunities';
            newOpp.Accountid = clientAcc.Id;
            newOpp.RecordTypeId = reqRecordTypeID;
            Date closingdate = system.today();
            newOpp.CloseDate = closingdate.addDays(25);
            newOpp.StageName = 'Draft';
            newOpp.Req_Total_Positions__c = 2;
            newOpp.Req_Job_Title__c = 'Salesforce Architect';
            newOpp.EnterpriseReqSkills__c = '[{"name":"Salesforce.com","favorite":false},{"name":"Lightning","favorite":false},{"name":"Apex","favorite":false}]';
            newOpp.Req_Skill_Details__c = 'Salesforce';
            newOpp.Req_Job_Description__c = 'Testing';
            newOpp.Pay_Rate_Max_Multi_Currency__c = 900;
            newOpp.Pay_Rate_Min_Multi_Currency__c = 901;
            newOpp.Req_Bill_Rate_Max__c = 300000;
            newOpp.Req_Bill_Rate_Min__c = 100000;
            newOpp.Currency__c = 'USD - U.S Dollar';
            newOpp.Req_EVP__c = 'test EVP';
            newOpp.Req_Job_Level__c = 'Expert Level';
            newOpp.Req_Rate_Frequency__c = 'Hourly';
            newOpp.Req_Duration_Unit__c = 'Year(s)';
            newOpp.Req_Qualification__c = 'Additional Skill - database';
            newOpp.Req_Hiring_Manager__c = con.Name;
            newOpp.Opco__c = 'TEKsystems';
            newOpp.BusinessUnit__c = 'Board Practice';
            newOpp.Req_Product__c = 'Permanent';
            newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
            newOpp.Req_Worksite_Street__c = '987 Hidden St';
            newOpp.Req_Worksite_City__c = 'Baltimore';
            newOpp.Req_Worksite_Postal_Code__c = '21228';
            newOpp.Req_Worksite_Country__c = 'United States';
            newOpp.Req_Duration_Unit__c = 'Day(s)';
            insert newOpp;
        
            Job_Posting__c jp = new Job_Posting__c();
            jp.Job_Title__c  = 'Test Administrator';
            jp.Skills__c = 'Core Java and Python';
            jp.Allegis_Employment_Type__c = 'Contract';
            jp.Industry_Sector__c ='Accounting';
            jp.External_Job_Description__c = 'Test Administrator'; 
            jp.Opportunity__c = newOpp.Id;
            jp.Status_Id__c=12280.0;
            insert jp;
        
            Job_Posting__c jp1 = new Job_Posting__c();
            jp1.Job_Title__c  = 'Test Administrator';
            jp1.Skills__c = 'Core Java and Python';
            jp1.Allegis_Employment_Type__c = 'Contract';
            jp1.Industry_Sector__c ='Accounting';
            jp1.External_Job_Description__c = 'Test Administrator'; 
            jp1.Opportunity__c = newOpp.Id;
            jp1.Status_Id__c=12281.0;
            insert jp1;
        
            List<Job_Posting__c> jobpostingList = new List<Job_Posting__c>();
            jobpostingList.add(jp);
            jobpostingList.add(jp1);
            Test.startTest();
                System.enqueueJob(new QueableJobPostingOpportunityUpdate(jobpostingList));
            Test.stopTest();
    }
}