@isTest
private class Test_RWSContactSearch{
     static testmethod void buildRWSURL()
     {
           Test.startTest();
           //create test data
           Account acc = TestDataHelper.createAccount();
           insert acc;
           Contact con = TestDataHelper.createContact();
           con.AccountId = acc.Id;
           insert con;
           //create data in customsettings
           //Integration_Endpoints__c endpoint = new Integration_Endpoints__c(Name = Label.RWS_App_URL,Endpoint__c = 'http://rwstest.com');
           //insert endpoint;
           Integration_URL_Pattern__c contactSearch = new Integration_URL_Pattern__c(Name = 'Search Candidate',QueryString__c = '/RWS/SearchCandidate?ContactName={!contactName}');
           insert contactSearch;
           //create an instance of the current VF page
           PageReference pageRef = Page.IntegrationOpenPage;
           Test.setCurrentPage(pageRef);
           // set the parameters for candidate search
           ApexPages.CurrentPage().getParameters().put('searchType','Search Candidate');
           ApexPages.CurrentPage().getParameters().put('contactName',con.FirstName + con.LastName);
           IntegrationOpenPageController controller = new IntegrationOpenPageController();
           controller.redirect();
           Test.stopTest();
     }    
}