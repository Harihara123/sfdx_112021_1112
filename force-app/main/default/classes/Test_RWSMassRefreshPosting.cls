@isTest
public class Test_RWSMassRefreshPosting {

     @isTest
    static void test_RefreshError(){
      Test.startTest();
        
      Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      ApexPages.StandardController stdController =new  ApexPages.StandardController(jpc1);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      Continuation conti = (Continuation)controller.massRefresh();
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
      system.assert(requests.get(controller.requestLabel) != null);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='ERROR';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse(controller.requestLabel, response);
        
      Object result = Test.invokeContinuationMethod(controller, conti);

      System.assertEquals(result, result);
        

      Test.stopTest();
    }
    
    @isTest
    static void test_Inactivate(){
        Test.startTest();
        Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      ApexPages.StandardController stdController =new  ApexPages.StandardController(jpc1);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      Continuation conti = (Continuation)controller.massRefresh();
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
      system.assert(requests.get(controller.requestLabel) != null);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='SUCCESS';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
      sPosting.code='SUCCESS';
      sPosting.message='Posting(s) refreshed successfully.';
      sPosting.postingId='6414951';
      rwsResponse.postingIdList.add(sPosting);
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse(controller.requestLabel, response);
        
      Object result = Test.invokeContinuationMethod(controller, conti);

      System.assertEquals(result, result);
        

        
        Test.stopTest();
    }
    
    @isTest
    static void test_MassRefresh(){
      Test.startTest();
        
      Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      Job_Posting__c jpc2=new Job_Posting__c();
      jpc2.Source_System_id__c='22222';
      insert jpc2;
        
      List<Job_Posting__c> jpcList=new List<Job_Posting__c>();
      jpcList.add(jpc1);
      jpcList.add(jpc2);
        
      ApexPages.StandardSetController stdController =new  ApexPages.StandardSetController(jpcList);
      stdController.setSelected(jpcList);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      Continuation conti = (Continuation)controller.massRefresh();
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
      system.assert(requests.get(controller.requestLabel) != null);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='SUCCESS';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
      sPosting.code='SUCCESS';
      sPosting.message='Posting(s) refreshed successfully.';
      sPosting.postingId='6414951';
      rwsResponse.postingIdList.add(sPosting);
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse(controller.requestLabel, response);
        
      Object result = Test.invokeContinuationMethod(controller, conti);

      System.assertEquals(result, result);
        

        
        
        Test.stopTest();
    }
    
    @isTest
    static void test_MassInactivate(){
        
        Test.startTest();
        
      Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      Job_Posting__c jpc2=new Job_Posting__c();
      jpc2.Source_System_id__c='22222';
      insert jpc2;
        
      List<Job_Posting__c> jpcList=new List<Job_Posting__c>();
      jpcList.add(jpc1);
      jpcList.add(jpc2);
        
      ApexPages.StandardSetController stdController =new  ApexPages.StandardSetController(jpcList);
      stdController.setSelected(jpcList);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      Continuation conti = (Continuation)controller.massInActivate();
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
      system.assert(requests.get(controller.requestLabel) != null);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='SUCCESS';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
      sPosting.code='SUCCESS';
      sPosting.message='Posting(s) refreshed successfully.';
      sPosting.postingId='6414951';
      rwsResponse.postingIdList.add(sPosting);
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse(controller.requestLabel, response);
        
      Object result = Test.invokeContinuationMethod(controller, conti);

      System.assertEquals(result, result);
        
       Test.stopTest();
        
    }
    
    @isTest
    static void test_MassInactivate_Negative(){
        
        Test.startTest();
        
      
        
      List<Job_Posting__c> jpcList=new List<Job_Posting__c>();
        
      ApexPages.StandardSetController stdController =new  ApexPages.StandardSetController(jpcList);
      stdController.setSelected(jpcList);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      System.PageReference page = (System.PageReference)controller.massInActivate();
    
      System.assertEquals(page, page);
        
       Test.stopTest();
        
    }
    
    @isTest
    static void test_MassRefresh_Negative(){
        
      Test.startTest();
        
      List<Job_Posting__c> jpcList=new List<Job_Posting__c>();
        
      ApexPages.StandardSetController stdController =new  ApexPages.StandardSetController(jpcList);
      stdController.setSelected(jpcList);
      CRM_RWSMassRefreshPosting controller=new CRM_RWSMassRefreshPosting(stdController);
      
      System.PageReference page = (System.PageReference)controller.massRefresh();
    
      System.assertEquals(page, page);
        
      Test.stopTest();
        
    }

}