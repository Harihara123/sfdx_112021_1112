@isTest
public class Test_RWSTransferPosting {

    
        @isTest
    static void test_Refresh(){
        
        Test.startTest();
               Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      ApexPages.StandardController stdController =new  ApexPages.StandardController(jpc1);
      CRM_RWSTransferPosting controller=new CRM_RWSTransferPosting(stdController);
        
      CRM_RWSTransferPosting.validateTransfer();
      
     CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),'');   
        
     Continuation conti = (Continuation)CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),String.valueOf(jpc1.id));
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='SUCCESS';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
      sPosting.code='SUCCESS';
      sPosting.message='Posting(s) refreshed successfully.';
      sPosting.postingId='6414951';
      rwsResponse.postingIdList.add(sPosting);
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse('Cont1', response);
        
      List<string> lblList=new List<string>();
      lblList.add('Cont1');
        
      
        
        
        Continuation conti1 = (Continuation)CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),String.valueOf(jpc1.id));
        
        CRM_RWSTransferPosting.processResponse(lblList,conti1);
        
    //  Object result = Test.invokeContinuationMethod(controller, conti);

     // System.assertEquals(result, result);
        

        
        Test.stopTest();
        
    }
    
    @isTest
    static void test_errorTransfer(){
          Test.startTest();
               Job_Posting__c jpc1=new Job_Posting__c();
      jpc1.Source_System_id__c='111111';
      insert jpc1;
        
      ApexPages.StandardController stdController =new  ApexPages.StandardController(jpc1);
      CRM_RWSTransferPosting controller=new CRM_RWSTransferPosting(stdController);
        
      CRM_RWSTransferPosting.validateTransfer();
      
     CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),'');   
        
     Continuation conti = (Continuation)CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),String.valueOf(jpc1.id));
      
      // Verify that the continuation has the proper requests
      Map<String, HttpRequest> requests = conti.getRequests();
      system.assert(requests.size() == 1);
        
      HttpResponse response = new HttpResponse();
      
      RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
      rwsResponse.responseMsg='ERROR';
      rwsResponse.responseDetail='Posting(s) refreshed successfully.';
      rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
      RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
      sPosting.code='ERROR';
      sPosting.message='Posting(s) refreshed successfully.';
      sPosting.postingId='6414951';
      rwsResponse.postingIdList.add(sPosting);
      String jsonResponse=JSON.serialize(rwsResponse);   
        
      response.setBody(jsonResponse);  
        
      Test.setContinuationResponse('Cont1', response);
        
      List<string> lblList=new List<string>();
      lblList.add('Cont1');
        
      
        
        
        Continuation conti1 = (Continuation)CRM_RWSTransferPosting.massTransfer(UserInfo.getUserId(),String.valueOf(jpc1.id));
        
        CRM_RWSTransferPosting.processResponse(lblList,conti1);
        
    //  Object result = Test.invokeContinuationMethod(controller, conti);

     // System.assertEquals(result, result);
        

        
        Test.stopTest();
        
      
    }
}