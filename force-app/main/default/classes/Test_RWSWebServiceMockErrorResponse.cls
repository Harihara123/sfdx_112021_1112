@isTest
global class Test_RWSWebServiceMockErrorResponse implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
        
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
    			RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
        		rwsResponse.responseMsg='SUCCESS';
                rwsResponse.responseDetail='Posting(s) refreshed successfully.';
                rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
                RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
                rwsResponse.postingIdList.add(sPosting);
        		String jsonResponse=JSON.serialize(rwsResponse);
        
                res.setBody(jsonResponse);
                res.setStatusCode(200);
                return res;

    }
}