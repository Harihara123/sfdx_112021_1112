@isTest
global class Test_RWSWebServiceMockPartialResponse implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
        
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
    			RWSJobPostingResponse rwsResponse= new RWSJobPostingResponse();   
        		rwsResponse.responseMsg='PARTIAL';
                rwsResponse.responseDetail='Some postings failed to inactivate.';
                rwsResponse.postingIdList=new List<RWSJobPostingResponse.PostingIdList>();
               
                RWSJobPostingResponse.PostingIdList sPosting=new RWSJobPostingResponse.PostingIdList();
                sPosting.code='SUCCESS';
                sPosting.message='Posting(s) refreshed successfully.';
                sPosting.postingId='6414951';
                rwsResponse.postingIdList.add(sPosting);
                sPosting=new RWSJobPostingResponse.PostingIdList();
                sPosting.code='ERROR';
                sPosting.message='The Posting does not present in RWS.';
                sPosting.postingId='6414951';
                rwsResponse.postingIdList.add(sPosting);
        		String jsonResponse=JSON.serialize(rwsResponse);
                res.setBody(jsonResponse);
                res.setStatusCode(200);
                return res;

    }
}