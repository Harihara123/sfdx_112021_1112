@isTest(seeAlldata=false)
public class Test_ReqCompletionBatchUpdate {


    public static testMethod void testBatch() {
        TestData tdd=new TestData();
        List<Opportunity> oppList=tdd.createOpportunities();
        system.debug('Size of list is---->'+oppList.size());
        system.debug('oppList----->'+oppList);
        List<Contact> conList=tdd.createContacts('Talent');
        boolean switchFlag=false;
        for(Opportunity opp:oppList){
           opp.stageName='Draft';
            opp.Req_Hiring_Manager__c=conList[0].id;
            opp.Req_Product__c='Contract';
            opp.Req_Total_Positions__c=1;
            opp.Currency__c='USD';
            opp.Req_Job_Title__c='titletest';
            opp.Req_Terms_of_engagement__c='Retained Exclusive';
            opp.Req_Worksite_Street__c='test';
            opp.Req_Worksite_City__c='test';
            opp.Req_Worksite_Country__c='USA';
            opp.Req_Worksite_Postal_Code__c='00000';
            if(switchFlag){
            	opp.OpCo__c='Allegis Partners, LLC';
            }else{
                switchFlag=true;
                opp.OpCo__c='Aerotek, Inc';
            }
            opp.Req_Division__c='Corporate';
            opp.Req_Bill_Rate_Min__c=1;
            opp.Req_Bill_Rate_Max__c=1;
            opp.Req_Pay_Rate_Min__c=1;
            opp.Req_Pay_Rate_Max__c=1;
            opp.Req_Standard_Burden__c=1;
            opp.Req_Job_Description__c='This is description';
            opp.Req_Qualification__c='This is qualification';
        }
        update oppList;
        
        Test.startTest();
        Batch_ReqCompletionUpdate brcu=new Batch_ReqCompletionUpdate();
        brcu.qualifierStr1=' where stageName in (\'Draft\',\'Qualified\',\'Presenting\',\'Interviewing\') and OpCo__c!=\'Aerotek, Inc\' and OpCo__c!=\'TEKsystems, Inc.\'';
        Database.executeBatch(brcu);
        Test.stopTest();
        
        List<Opportunity> resultList=[SELECT Id,ReqCompletition__c ,stageName FROM Opportunity where id in :oppList];
        
        System.assert(resultList.size()>0);
        
    }
    
    

    
}