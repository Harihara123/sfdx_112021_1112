//***************************************************************************************************************************************/
//* Name        - Test_Batch_UpdateReqEnterpriseSkills
//* Description - Test class for batch Utility class to update Req Enterprise Skills field
//* Modification Log :
//* ---------------------------------------------------------------------------
//* Developer                   Date                   Description
//* ---------------------------------------------------------------------------
//* Krishna Chitneni         01/22/2018                Created
//*****************************************************************************************************************************************/
@isTest
public class Test_ReqSkills
{
    static testMethod void testReqSkills() 
    {
        User user = TestDataHelper.createUser('System Integration'); 
        
         system.runAs(user) 
         {
             Test.startTest();
            
            List<ReqSkills.Skills> lstSkill = new List<ReqSkills.Skills>();
            
            ReqSkills.Skills objSkill = new ReqSkills.Skills('Testing',true);
            lstSkill.add(objSkill);
            
            ReqSkills.Skills objSkill1 = new ReqSkills.Skills('AWS',false);
            lstSkill.add(objSkill1);
            
            string inputJson = System.JSON.Serialize(lstSkill);
            
            ReqSkills.getAllParsedSkills(inputJson);
            ReqSkills.getFavouriteSkills(inputJson);
            ReqSkills.getNonFavouriteSkills(inputJson);
            
            
            Test.stopTest();
        }                                    
    }
      
    
                            
}