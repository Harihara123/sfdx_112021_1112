/******************************************************
 * Description      :  Test method for ReqTeamMemberNotification Trigger
 * @author          :  Rajkumar
 * @Date            :  April, 2013   
 * Modification Log :
 * -----------------------------------------------------------------------------
 * Developer                   Date                          Description
 * ----------------------------------------------------------------------------  
 * Vivek Ojha(Appirio)		  27/2/15					To increase code coverage
******************************************************/

@isTest(seeAlldata= false)
private  class Test_ReqTeamMemberNotification {
    
    static string contactRecruiterRecType = Schema.sObjectType.Contact.getRecordTypeInfosByName().get(Label.Contact_Recruiter_Recordtype_Name).getRecordTypeId(); 
    static testMethod void Test_ReqTeamNotificationHelper()   
    {
        
            test.startTest();       
            //insert test data
            TestData TdAcc = new TestData(1);
            list<User> lstUser=[Select Id from User where isActive=true limit 1];
            string userId;
            if(lstUser.size()==0){          
                User u = TestDataHelper.createUser('Aerotek AM');
                userId= u.Id;
            }else{
                userId = lstUser[0].Id;
            }
            
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            List<Reqs__c> reqs = new List<Reqs__c>();
            
            for(integer i=0;i<100;i++) {
                Reqs__c req = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
                    Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                    Positions__c = i+10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()); 
                reqs.add(req);                
            }                
            insert reqs; 
            
            //create a team member
            //Req_Team_Member__c Teammemer = new Req_Team_Member__c(User__c= userId, Requisition__c = reqs[0].Id);
            //insert Teammemer;
            
            //Update the Req            
            Reqs__c aReq=[Select Id from Reqs__c where Id=: reqs[0].Id];
            update aReq;
    
        Req_Team_Member__c aReqTeamMem=[SELECT Requisition__c,Send_Req_Update_Notification__c FROM Req_Team_Member__c where Requisition__c =: reqs[0].Id]; //id=:Teammemer.Id
        if(aReqTeamMem.Send_Req_Update_Notification__c !=null){
            //Convert datetime into Date for assert
            DateTime dateT = aReqTeamMem.Send_Req_Update_Notification__c;
            
            //Check Req teammember gets updated when Req record is getting update
            system.assertEquals(date.newinstance(dateT.year(), dateT.month(), dateT.day()),system.today());
        }
   }    

}