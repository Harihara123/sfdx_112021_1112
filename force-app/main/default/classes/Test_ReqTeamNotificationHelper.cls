/*******************************************************************
* Name  : Test_ReqTeamNotificationHelper
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 23, 2015
* Details : Test class for ReqTeamNotificationHelper
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_ReqTeamNotificationHelper {

    static testMethod void testReqTeamNotification() {
    		TestData TdAcc = new TestData(1);
    		User u = TestDataHelper.createUser('Aerotek AM');
            List<Account> lstNewAccounts = TdAcc.createAccounts();
            List<Reqs__c> reqs = new List<Reqs__c>();
            
            for(integer i=0;i<100;i++) {
                Reqs__c req = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
                    Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                    Positions__c = i+10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()); 
                reqs.add(req);                
            }                
            insert reqs; 
            
            //create a team member
            //Req_Team_Member__c Teammemer = new Req_Team_Member__c(User__c= u.Id, Requisition__c = reqs[0].Id);
            //insert Teammemer;
            system.runAs(u){
	            test.startTest();
	            ReqTeamNotificationHelper.notifyReqTeam(reqs[0].Id);
	    		test.stopTest();
            }
            System.assert(reqs!=null);
    }
    
    
     static testMethod void testException() {
    		User u = TestDataHelper.createUser('Aerotek AM');
    		TestData TdAcc = new TestData(1);
    		 system.runAs(u){
	            test.startTest();
	            	ReqTeamNotificationHelper.notifyReqTeam(u.Id);
	            
	    		test.stopTest();
            }
     }
}