/*******************************************************************
* Name  : Test_ReqTeamsExtension
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 15, 2015
* Details : Test class for ReqTeamsExtension
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_ReqTeamsExtension {

    

    //variable declaration
    static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');    
    static testMethod void Test_ReqTeamsExtension() {   
         
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id);
        Account_National_Owner_Fields__c cusSett = new Account_National_Owner_Fields__c();
        cusSett.Name = 'Aerotek_SSO_Owner__c';
        insert cusSett ;
        Product2 testProduct=TdAcc.createTestProduct();
        insert testProduct;
        Test.startTest();    
        if(user != Null) {
            Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 );             
            insert newreq;                                 
            
            newreq.Primary_Contact__c = lstNewContact.id;
            newreq.Account__c = lstNewAccounts[0].Id;
            newreq.National_Account_Manager__c = userinfo.getuserid();
            newReq.Filled__c = 10;
            newReq.Bill_Rate_Max__c = 50; 
            newReq.Bill_Rate_Min__c = 25;
            newReq.Standard_Burden__c = 2;
            newReq.Duration__c = 5;
            newReq.Duration_Unit__c = 'days';
            newReq.Job_category__c = 'admin';
            newReq.Req_priority__c = 'green';
            newReq.Pay_Rate_Max__c = 10;
            newReq.Pay_Rate_Min__c = 5;
            newReq.Job_description__c = 'test';
            newReq.Organizational_role__c = 'Centre Manager';
            newReq.Product__c= testProduct.Id;
            newReq.stage__c =  'Qualified';
      
            update newreq;
            
              // Insert Recruiter Team
            Recruiter_Team__c RTeam = new Recruiter_Team__c(Name = 'SampleData', public__c = True);
            insert Rteam;
            try {
                Team_Members__c RTeamMemberExp = new Team_Members__c(Contact__c = lstNewContact.id,Recruiter_Team_Default__c = Rteam.id);
                insert RTeamMemberExp;
            } catch(exception err) {
                system.assert(err.getmessage().contains('FIELD_FILTER_VALIDATION_EXCEPTION'));
            }
            
            lstNewContact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(label.Contact_Recruiter_Recordtype_Name).getRecordTypeId();
            lstNewContact.Contact_Active__c = true;
            update lstNewContact;
             // Insert Recruiter Team Memeber's contact with 'Recruiter' Contact.
            Team_Members__c RTeamMember = new Team_Members__c(Contact__c = lstNewContact.id,Recruiter_Team_Default__c = Rteam.id);
            insert RTeamMember;         
            
            
            // Call ReqTeamsExtension             
            ApexPages.currentPage().getParameters().put('id',string.valueof(newreq.id));
            ReqTeamsExtension ReqExt = new ReqTeamsExtension();
            ReqExt.getDefaultRecruiterTeams();
            ReqExt.selectedRecruiterTeam=Rteam.Id; 
            ReqExt.saveTeamMembers();
             System.assert(ReqExt.selectedRecruiterTeam!=null);
             ReqExt.Cancel();
        }
        Test.stopTest();
    }
   
    
}