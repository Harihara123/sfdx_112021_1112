@isTest
public class Test_ReqValidationController{
    public static testMethod void testReq() {
      //  User u = TestDataHelper.createUser('Aerotek AM');
        User u = TestDataHelper.createUser('System Administrator');
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id);
        
        if(u!= null){
            system.runas(u){      
                Test.StartTest();  
                ReqValidationController req = new ReqValidationController();
                Reqs_Validation__c reqval = new Reqs_Validation__c(Name='test',Field_API__c='test',Placement_Type__c='test',
                                                        Record_Type_Name__c='test',Stage_Name__c='Qualified');
                insert reqval;
                    
                Reqs__c newreq = new Reqs__c (Account__c =  lstNewAccounts[0].Id, stage__c = 'Draft', Status__c = 'Open',                       
                Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
                Positions__c = 10,Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9 ); 
                            
                insert newreq;
                
                newReq.Draft_Other_Reason__c = NULL;
                newReq.Draft_Reason__c = 'OTHER';        
                newreq.Primary_Contact__c = lstNewContact.id;
                newreq.Account__c = NULL;
                newreq.National_Account_Manager__c = userinfo.getuserid();
                newReq.Filled__c = 10;
                newReq.Bill_Rate_Max__c = 50;
                newReq.Bill_Rate_Min__c = 25;
                newReq.Standard_Burden__c = 2;
                newReq.Duration__c = 5;
                newReq.Duration_Unit__c = 'days';
                newReq.Job_category__c = 'admin';
                newReq.Req_priority__c = 'green';
                newReq.Pay_Rate_Max__c = 10;
                newReq.Pay_Rate_Min__c = 5;
                newReq.Job_description__c = 'test';
                newReq.Organizational_role__c = 'Centre Manager';
               
                
                newReq.stage__c =  'Qualified';
                newReq.Placement_Type__c ='Contract';
          
                //update newreq;
                Map<string,string> newmap = new Map<string,string>();
                newmap.put('test1', 'test2');
                ReqValidationController.validateReqs(newreq,true,newmap);
                //String boo;
                //ReqValidationController.processErrorMessage(boo);
                
                               
                Test.stopTest();
                }
         }
     }
}