/*******************************************************************
* Name  : Test_ReqsTrigger
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 27 2015
* Details : Test class for ReqsTrigger
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
  Nidish                      6/18/2015                     updated line 40 to insert Account
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_ReqsTrigger {
    final static string SYSTEM_SOURCEREQ = 'SIEBEL';
    final static string FIELDGLASS_SOURCEREQ = 'Fieldglass';
    final static string SFDC_SOURCEREQ = 'SALESFORCE';
    static testMethod void test_ReqsTrigger() {
        
        //Create an Account record with siebel Id.
        Account NewAccounts = new Account(
            Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts;
        system.assertnotequals(NewAccounts.Siebel_ID__c,null);
         //Create an Account for update to Req__c.
        Account NewAccounts2 = new Account(
            Name = 'TESTACCT2',Siebel_ID__c = '1-93WP-1',
            Phone= '234567',ShippingCity = 'Testshipcity12',ShippingCountry = 'Testshipcountry12',
            ShippingPostalCode = 'TestshipCode24',ShippingState = 'Testshipstate23',
            ShippingStreet = 'Testshipstreet12',BillingCity ='TestBillstreet12',
            BillingCountry ='TestBillcountry12',BillingPostalCode ='TestBillCode12',
            BillingState ='TestBillState12',BillingStreet ='TestBillStreet12',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts2;
                Contact con = new Contact(LastName='Test',Email='abc@xyz.com', AccountId = NewAccounts2.id);
                 //assign the recruiter record type id
                 con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Recruiter').getRecordTypeId();
                 con.Contact_Active__c = true;
                 insert con;
        
        Test.startTest();
        
        //Create a Req record
        Reqs__c recRequisition = new Reqs__c(
        Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,SourceReq__c = 'R-101123',
        Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition;
        //Create a Req record
        Reqs__c recRequisition1 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1122', Max_Salary__c = 5000.00, Min_Salary__c = 2000.00,Duration__c = 22,SourceReq__c = 'R-101134',
        Job_Description__c = 'Sample test',  City__c = 'Sample test', state__c = 'Sample test', Zip__c = 'Sample test',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition1;
        
        recRequisition.Account__c = NewAccounts2.Id;
        recRequisition.Primary_Contact__c = con.Id;
        update recRequisition;
        delete recRequisition;
        undelete recRequisition;
        recRequisition1.Primary_Contact__c = con.Id;
        update recRequisition1;
        
        ReqsTriggerHandler handler = new ReqsTriggerHandler();
        List<Req_Team_Member__c> reqTeamsList = [SELECT ID FROM Req_Team_Member__c ];
        try{
            handler.addTeamMembersToCollection(reqTeamsList,'R-101123','R-101134',reqTeamsList);
        }catch(Exception e){
            System.debug('Exception is**'+e);
        }
        Test.stopTest();
    }
     static testMethod void test_ReqsTriggerrun2() {
        
        //Create an Account record with siebel Id.
        Account NewAccounts = new Account(
            Name = 'TESTACCTSTARTCTRLWS',Siebel_ID__c = '1-4W6J-1',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                          
        );
        insert NewAccounts;
        system.assertnotequals(NewAccounts.Siebel_ID__c,null);
         //Create an Account for update to Req__c.
        Account NewAccounts2 = new Account(
            Name = 'TESTACCT2',Siebel_ID__c = '1-93WP-1',
            Phone= '234567',ShippingCity = 'Testshipcity12',ShippingCountry = 'Testshipcountry12',
            ShippingPostalCode = 'TestshipCode24',ShippingState = 'Testshipstate23',
            ShippingStreet = 'Testshipstreet12',BillingCity ='TestBillstreet12',
            BillingCountry ='TestBillcountry12',BillingPostalCode ='TestBillCode12',
            BillingState ='TestBillState12',BillingStreet ='TestBillStreet12',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId()                                           
        );
        insert NewAccounts2;
        
        Contact con = new Contact(LastName='Test',Email='abc@xyz.com', AccountId = NewAccounts2.id);
                 //assign the recruiter record type id
                 con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Recruiter').getRecordTypeId();
                 con.Contact_Active__c = true;
                 insert con;
        
        Test.startTest();
        
        //Create a Req record
        Reqs__c recRequisition = new Reqs__c(
        Account__c = NewAccounts.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1111', Max_Salary__c = 2000.00, Min_Salary__c = 1000.00,Duration__c = 12,SourceReq__c = 'R-101123',
        Job_Description__c = 'Sample',  City__c = 'Sample', state__c = 'Sample', Zip__c = 'Sample',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition;
        //Create a Req record
        Reqs__c recRequisition1 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 10, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1122', Max_Salary__c = 5000.00, Min_Salary__c = 2000.00,Duration__c = 22,SourceReq__c = 'R-101134',
        Job_Description__c = 'Sample test',  City__c = 'Sample test', state__c = 'Sample test', Zip__c = 'Sample test',System_SourceReq__c = SYSTEM_SOURCEREQ);
        insert recRequisition1;
        
        //Create a Req record
        Reqs__c recRequisition3 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 30, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1133', Max_Salary__c = 7000.00, Min_Salary__c = 3000.00,Duration__c = 33,SourceReq__c = 'R-101135',
        Job_Description__c = 'Sample test33',  City__c = 'Sample test33', state__c = 'Sample test33', Zip__c = 'Sample test33',System_SourceReq__c = SFDC_SOURCEREQ);
        insert recRequisition3;
        
        //Create a Req record
        Reqs__c recRequisition4 = new Reqs__c(
        Account__c = NewAccounts2.Id, Address__c = 'US', Stage__c = 'Qualified', 
        Positions__c = 40, Placement_Type__c = 'Permanent', Duration_Unit__c = 'Permanent', Start_Date__c = system.today(),       
        Siebel_ID__c = 'TEST1144', Max_Salary__c = 8000.00, Min_Salary__c = 5000.00,Duration__c = 44,SourceReq__c = 'R-101136',
        Job_Description__c = 'Sample test44',  City__c = 'Sample test44', state__c = 'Sample test44', Zip__c = 'Sample test44',System_SourceReq__c = FIELDGLASS_SOURCEREQ);
        insert recRequisition4;
        
        recRequisition.Account__c = NewAccounts2.Id;
        update recRequisition;
        recRequisition1.Primary_Contact__c = con.Id;
        update recRequisition1;
        recRequisition3.Job_Description__c = 'test44Update';
        update recRequisition3;
        recRequisition4.state__c = 'test44Update';
        update recRequisition4;
        
        delete recRequisition;
        undelete recRequisition;
       
        ReqsTriggerHandler handler = new ReqsTriggerHandler();
        List<Req_Team_Member__c> reqTeamsList = [SELECT ID FROM Req_Team_Member__c ];
        try{
            handler.addTeamMembersToCollection(reqTeamsList,'R-101123','R-101134',reqTeamsList);
        }catch(Exception e){
            System.debug('Exception is**'+e);
        }
        Test.stopTest();
    }
}