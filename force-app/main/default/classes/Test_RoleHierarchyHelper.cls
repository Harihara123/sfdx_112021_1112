@isTest(seeAlldata= FALSE)
private class Test_RoleHierarchyHelper {
    
    static testMethod void Test_Rolehierarchy(){
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        User user = TestDataHelper.createUser('System Administrator');
        insert new RoleHierarchyExecuters__c(SetupOwnerId=UserInfo.getOrganizationId(), Execute__c=TRUE);
        List<User> usrList = new List<User>();
        
        TopNodeOpcoCodes__c codeTemp = new TopNodeOpcoCodes__c();
        codeTemp.Name ='Test';
        codeTemp.Jobcode__c='1234';
        insert codeTemp;
        
        User u = new User();
        u.firstName = 'Columbo';
        u.LastName = 'Henry';
        u.Alias = 'hcolu';
        u.Email = 'hcolum'  +'@allegisgroup.com';
        u.username = 'hcolum'  +random+'@allegisgroup.com.crmup0'+random;
        u.TimeZoneSidKey ='Europe/London' ;
        u.LocaleSidKey = 'en_GB' ;
        u.EmailEncodingKey = 'ISO-8859-1' ;
        u.LanguageLocaleKey ='en_US' ;
        u.OPCO__c = 'PUR';
        u.UserPermissionsInteractionUser = true;
        u.UserPermissionsJigsawProspectingUser = false;
        u.UserPermissionsMobileUser = true;
        u.profileId = '00e24000000JERj' ;
        u.IsActive = true;
        u.Peoplesoft_Id__c = 'RNR2345000';
        u.Supervisor_PS_Id__c = 'RNR2345000';
        u.CompanyName = 'EMEA';
        u.ODS_Jobcode__c = 'testtttt';
        usrList.add(u);
        // }
        User u1 = new User();
        u1.firstName = 'Columbo';
        u1.LastName = 'Henry';
        u1.Alias = 'hcolu';
        u1.Email = 'hcolum'  +'@allegisgroup.com';
        u1.username = 'hcolum'  +random+'@allegisgroup.com.crmup1'+random;
        u1.TimeZoneSidKey ='Europe/London' ;
        u1.LocaleSidKey = 'en_GB' ;
        u1.EmailEncodingKey = 'ISO-8859-1' ;
        u1.LanguageLocaleKey ='en_US' ;
        u1.OPCO__c = 'PUR';
        u1.UserPermissionsInteractionUser = true;
        u1.UserPermissionsJigsawProspectingUser = false;
        u1.UserPermissionsMobileUser = true;
        u1.profileId = '00e24000000JERj' ;
        u1.IsActive = true;
        u1.Peoplesoft_Id__c = 'RNR23451111';
        u1.Supervisor_PS_Id__c = 'RNR2345000';
        u1.CompanyName = 'EMEA';
        u1.ODS_Jobcode__c = 'testtttt';
        usrList.add(u1);
        
        User u2 = new User();
        u2.firstName = 'Columbo';
        u2.LastName = 'Henry';
        u2.Alias = 'hcolu';
        u2.Email = 'hcolum'  +'@allegisgroup.com';
        u2.username = 'hcolum'  +random+'@allegisgroup.com.crmup2'+random;
        u2.TimeZoneSidKey ='Europe/London' ;
        u2.LocaleSidKey = 'en_GB' ;
        u2.EmailEncodingKey = 'ISO-8859-1' ;
        u2.LanguageLocaleKey ='en_US' ;
        u2.OPCO__c = 'PUR';
        u2.UserPermissionsInteractionUser = true;
        u2.UserPermissionsJigsawProspectingUser = false;
        u2.UserPermissionsMobileUser = true;
        u2.profileId = '00e24000000JERj' ;
        u2.IsActive = true;
        u2.Peoplesoft_Id__c = 'RNR23452222';
        u2.Supervisor_PS_Id__c = 'RNR23451111';
        u2.CompanyName = 'EMEA';
        u2.ODS_Jobcode__c = 'testtttt';
        usrList.add(u2);
        
        User u3 = new User();
        u3.firstName = 'Columbo';
        u3.LastName = 'Henry';
        u3.Alias = 'hcolu';
        u3.Email = 'hcolum'  +'@allegisgroup.com';
        u3.username = 'hcolum'  +random+'@allegisgroup.com.crmup3'+random;
        u3.TimeZoneSidKey ='Europe/London' ;
        u3.LocaleSidKey = 'en_GB' ;
        u3.EmailEncodingKey = 'ISO-8859-1' ;
        u3.LanguageLocaleKey ='en_US' ;
        u3.OPCO__c = 'PUR';
        u3.UserPermissionsInteractionUser = true;
        u3.UserPermissionsJigsawProspectingUser = false;
        u3.UserPermissionsMobileUser = true;
        u3.profileId = '00e24000000JERj' ;
        u3.IsActive = true;
        u3.Peoplesoft_Id__c = 'RNR23453333';
        u3.Supervisor_PS_Id__c = 'RNR23452222';
        u3.CompanyName = 'EMEA';
        u3.ODS_Jobcode__c = 'testtttt';
        usrList.add(u3);
        
        User u4 = new User();
        u4.firstName = 'Columbo';
        u4.LastName = 'Henry';
        u4.Alias = 'hcolu';
        u4.Email = 'hcolum'  +'@allegisgroup.com';
        u4.username = 'hcolum'  +random+'@allegisgroup.com.crmup4'+random;
        u4.TimeZoneSidKey ='Europe/London' ;
        u4.LocaleSidKey = 'en_GB' ;
        u4.EmailEncodingKey = 'ISO-8859-1' ;
        u4.LanguageLocaleKey ='en_US' ;
        u4.OPCO__c = 'PUR';
        u4.UserPermissionsInteractionUser = true;
        u4.UserPermissionsJigsawProspectingUser = false;
        u4.UserPermissionsMobileUser = true;
        u4.profileId = '00e24000000JERj' ;
        u4.IsActive = true;
        u4.Peoplesoft_Id__c = 'RNR23454444';
        u4.Supervisor_PS_Id__c = 'RNR23452222';
        u4.CompanyName = 'EMEA';
        u4.ODS_Jobcode__c = 'testtttt';
        usrList.add(u4);
        System.debug('UserList'+ usrList);
        
        User u5 = new User();
        u5.firstName = 'Columbo';
        u5.LastName = 'Henry';
        u5.Alias = 'hcolu';
        u5.Email = 'hcolum'  +'@allegisgroup.com';
        u5.username = 'hcolum'  +random+'@allegisgroup.com.crmup5'+random;
        u5.TimeZoneSidKey ='Europe/London' ;
        u5.LocaleSidKey = 'en_GB' ;
        u5.EmailEncodingKey = 'ISO-8859-1' ;
        u5.LanguageLocaleKey ='en_US' ;
        u5.OPCO__c = 'PUR';
        u5.UserPermissionsInteractionUser = true;
        u5.UserPermissionsJigsawProspectingUser = false;
        u5.UserPermissionsMobileUser = true;
        u5.profileId = '00e24000000JERj' ;
        u5.IsActive = true;
        u5.Peoplesoft_Id__c = '';
        u5.Supervisor_PS_Id__c = '';
        u5.CompanyName = 'EMEA';
        u5.ODS_Jobcode__c = 'testtttt';
        usrList.add(u5);
        
        User u6 = new User();
        u6.firstName = 'Columbo';
        u6.LastName = 'Henry';
        u6.Alias = 'hcolu';
        u6.Email = 'hcolum'  +'@allegisgroup.com';
        u6.username = 'hcolum'  +random+'@allegisgroup.com.crmup6'+random;
        u6.TimeZoneSidKey ='Europe/London' ;
        u6.LocaleSidKey = 'en_GB' ;
        u6.EmailEncodingKey = 'ISO-8859-1' ;
        u6.LanguageLocaleKey ='en_US' ;
        u6.OPCO__c = 'RNR';
        u6.UserPermissionsInteractionUser = true;
        u6.UserPermissionsJigsawProspectingUser = false;
        u6.UserPermissionsMobileUser = true;
        u6.profileId = '00e24000000JERj' ;
        u6.IsActive = true;
        u6.Peoplesoft_Id__c = 'PUR58590000';
        u6.Supervisor_PS_Id__c = 'PUR58591111';
        u6.CompanyName = 'EMEA';
        u6.ODS_Jobcode__c = 'testtttt';
        usrList.add(u6);
        
        System.debug('UserList'+ usrList);
        Test.startTest();
        insert usrList;
        Test.stopTest(); 
    }
    
    
    static testMethod void Test_Rolehierarchy1()
    {
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        User user = TestDataHelper.createUser('System Administrator');
        insert new RoleHierarchyExecuters__c(SetupOwnerId=UserInfo.getOrganizationId(), Execute__c=TRUE);
        List<User> usrList = new List<User>();
        
        TopNodeOpcoCodes__c codeTemp = new TopNodeOpcoCodes__c();
        codeTemp.Name ='Test';
        codeTemp.Jobcode__c='1234';
        insert codeTemp;
        
        User u = new User();
        u.firstName = 'Columbo';
        u.LastName = 'Henry';
        u.Alias = 'hcolu';
        u.Email = 'hcolum'  +'@allegisgroup.com';
        u.username = 'hcolum'  +random+'@allegisgroup.com.crmup0'+random;
        u.TimeZoneSidKey ='Europe/London' ;
        u.LocaleSidKey = 'en_GB' ;
        u.EmailEncodingKey = 'ISO-8859-1' ;
        u.LanguageLocaleKey ='en_US' ;
        u.OPCO__c = 'PUR';
        u.UserPermissionsInteractionUser = true;
        u.UserPermissionsJigsawProspectingUser = false;
        u.UserPermissionsMobileUser = true;
        u.profileId = '00e1p000000Ycyw' ;
        u.IsActive = true;
        u.Peoplesoft_Id__c = '234';
        u.Supervisor_PS_Id__c = '234';
        u.ODS_Jobcode__c = 'testtttt';
        //u.CompanyName = 'EMEA';

        Test.startTest();
            insert u;
        Test.stopTest(); 
    }
    
}