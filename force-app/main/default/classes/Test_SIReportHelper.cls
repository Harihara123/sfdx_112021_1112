/*******************************************************************
* Name  : Test_SIReportHelper
* Author: Vivek Ojha(Appirio India) 
* Date  : Feb 23, 2015
* Details : Test class for SIReportHelper
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
  Nidish                      6/18/2015                     Added line 27 to insert value into opco__c field
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata=true)
private class Test_SIReportHelper {

    static testMethod void test_SIReport() {
        Set<String> ids= new Set<String>();
        string firstId;
       test.startTest();
       SIReportHelper testReportHelper = new SIReportHelper();
       SIReportHelper.SIReportFilters sRep = new SIReportHelper.SIReportFilters('Where Id != Null','SELECT ID FROM Account');
       //testReportHelper.SIReportFilters = sRep;
        List<Strategic_Initiative__c> stratInit = TestDataHelper.getInitiativeRecords(); //getTeamMembers(set<string> siTeams)
       List<SIReportHelper.SIReportTimeFrame> tfList = testReportHelper.getSIReportTimeFrames();
       System.assertEquals(TRUE, tfList.size() > 0);
        
       List<SIReportHelper.SIReport_DependentQuery> reportQuery = testReportHelper.getSIReportDependentQuery();
       System.assertEquals(TRUE, reportQuery.size() > 0); 
    
       List<SIReportHelper.SIReport_RelatedObjectDateFilterFields> relatedDateFilters = testReportHelper.getSIReportDateFilterFields();
       System.assertEquals(TRUE, relatedDateFilters.size() > 0); 
       
       List<SIReportHelper.SIReport_RelatedObjects> relatedObjects = testReportHelper.getSIReportRelatedObjects();
       System.assertEquals(TRUE, relatedObjects.size() > 0); 
        
       List<SIReportHelper.SIReport_SObjectAssociatedFieldInfo> associatedFields = testReportHelper.getSObjectAssociatedFieldInfo();
       System.assertEquals(TRUE, associatedFields.size() > 0); 
        
       List<SIReportHelper.SIReport_RelatedObjectQuery> relatedObjQuery = testReportHelper.getRelatedObjectQuery();
       System.assertEquals(TRUE, relatedObjQuery.size() > 0); 
      SIReportHelper.retrieveSIMembers('Test');

        for(Strategic_Initiative__c strat : stratInit){
        firstId= strat.Id;
        ids.add(strat.Id);
       // strat.Region__c = 'Aerotek Canada';
        strat.opco__c = 'Aerotek';
        strat.Impacted_Region__c ='test';
       }
       insert stratInit;
       SIReportHelper.retrieveSIRelatedAccounts(firstId,true);
       SIReportHelper.retrieveSIRelatedAccounts(firstId,false);
       SIReportHelper.retrieveSIsInRegion('Aerotek Canada',true);
       SIReportHelper.retrieveSIsInRegion('Aerotek Canada',false);
       test.stopTest();
    }
}