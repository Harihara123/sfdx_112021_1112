@istest
public class Test_ScheduledCleanups {
    static TestMethod void testScheduledAccountCleanup() {
        Test.startTest();
        
        String jobId = ScheduledAccountCleanup.setup();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		// Verify the expressions are the same
      	System.assertEquals(ScheduledAccountCleanup.SCHEDULE, ct.CronExpression);

      	// Verify the job has not run
      	System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }

    static TestMethod void testScheduledContactCleanup() {
        Test.startTest();
        
        String jobId = ScheduledContactCleanup.setup();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		// Verify the expressions are the same
      	System.assertEquals(ScheduledContactCleanup.SCHEDULE, ct.CronExpression);

      	// Verify the job has not run
      	System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }

    static TestMethod void testScheduledTalentDocCleanup() {
        Test.startTest();
        
        String jobId = ScheduledTalentDocCleanup.setup();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		// Verify the expressions are the same
      	System.assertEquals(ScheduledTalentDocCleanup.SCHEDULE, ct.CronExpression);

      	// Verify the job has not run
      	System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }

    static TestMethod void testScheduledTalentExperienceCleanup() {
        Test.startTest();
        
        String jobId = ScheduledTalentExperienceCleanup.setup();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		// Verify the expressions are the same
      	System.assertEquals(ScheduledTalentExperienceCleanup.SCHEDULE, ct.CronExpression);

      	// Verify the job has not run
      	System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }
}