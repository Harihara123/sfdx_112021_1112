@isTest
public class Test_SearchAlertResultHistoryBatch {
    
    static testMethod void testMethod1()
    {
        
        Search_Alert_Criteria__c criteria1 = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='Test', Send_Email__c= true, Query_String__c = 'xyz' );
        insert criteria1;
        
        String jsonResult1 = '{"candidateId":"0030D000009F3YvQAK","givenName":"Valarie","familyName":"Peterson","candidateStatus":"Current","city":"Meridian","state":"Mississippi","country":"United States","createdDate":"2019-01-24T15:08:24.000Z","alertId":"'+criteria1.Id+'","teaser":"This is a resume teaser for this alert match","alertTrigger":null,"alertMatchTimestamp":"2019-02-13T15:08:24.000Z"}';
        Search_Alert_Result__c result1 = new Search_Alert_Result__c( Alert_Criteria__c = criteria1.Id , Results__c = jsonResult1 , Date__c = System.today()-1, Version__c='v2');
        insert result1;
        
        Search_Alert_Criteria__c criteria2 = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='ReqTest', Alert_Trigger__c='Req Created', Send_Email__c= true);
        insert criteria2;
        
        String jsonResult2 = '{ "workRemote": true, "state": "MD", "skills": "java", "reqDivision": "Applications", "positionTitle": null, "positionId": null, "ownerId": "005240000080cM8AAI", "opcoName": "TEKsystems, Inc.", "jobStage": null, "jobOwner": "API Autouser", "jobMinRate": "80.0", "jobMaxRate": "100.0", "jobId": "0067A000007oEvCQAU", "jobDurationUnit": "Month(s)", "jobDuration": "4.0", "jobCreatedDate": null, "country": "UNITED STATES", "city": "Baltimore", "canUseApprovedSubVendor": true, "alertTrigger": [ "Req Created" ], "alertMatchTimestamp": "2020-07-06T16:30:44.305Z", "alertId":"'+criteria2.id+'" "accountName": null }';
        Search_Alert_Result__c result2 = new Search_Alert_Result__c( Alert_Criteria__c = criteria2.Id , Results__c = jsonResult1 , Date__c = System.today()-1, Version__c='v2');
        insert result2;
        result2.Date__c =Date.newInstance(2019, 12, 9);
        upsert result2;
        Test.startTest();
        SearchAlertResultHistoryBatch sh1 = new SearchAlertResultHistoryBatch();
        String sch = '0 0 2 * * ?'; 
        system.schedule('SearchAlertClenup', sch, sh1); 
        Test.stopTest();
    }
    
}