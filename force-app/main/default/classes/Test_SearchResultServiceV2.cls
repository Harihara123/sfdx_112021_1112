@isTest
public class Test_SearchResultServiceV2 {
    
    public static testMethod void doPostTalentAlertResultTest(){
		
        Search_Alert_Criteria__c criteria1 = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', 
                                                                          Name='Test', Send_Email__c= true, 
                                                                          Query_String__c = 'xyz' );
        insert criteria1;    
        
        Test.startTest();
			RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
        	String requestJSONBody = '{ "results":[{ "candidateId" : "0031p00001d0iI4AAI", "jobTitle" : "Production Damager",'
                					+'"givenName" : "Walter", "familyName" : "Bosch", "candidateStatus" : "Current", "city" : "Charleston",'
                					+'"state" : "South Carolina", "country" : "United States", "createdDate" : "2019-01-24T15:08:24.000Z",'
                					+'"alertId" : "'+criteria1.Id+'", "teaser" : "This is a resume teaser for this alert match",'
                					+'"alertTrigger" : ["Talent Created" ], "alertMatchTimestamp" : "2019-01-24T15:08:24.000Z" }]}';
            
            req.requestURI = '/services/apexrest/alertresults/v1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            ATSSearchAlertResultsService_v2.doPost();
        
        	requestJSONBody = '{ "results":[{ "candidateId" : "0031p00001d0iI4AAI", "jobTitle" : "Production Damager",'
                					+'"givenName" : "Walter", "familyName" : "Bosch", "candidateStatus" : "Current", "city" : "Charleston",'
                					+'"state" : "South Carolina", "country" : "United States", "createdDate" : "2019-01-24T15:08:24.000Z",'
                					+'"alertId" : "'+criteria1.Id+'", "teaser" : "This is a resume teaser for this alert match",'
                					+'"alertTrigger" : ["Talent Created" ], "alertMatchTimestamp" : "2020-01-24T15:08:24.000Z" }]}';
            
            req.requestBody = Blob.valueOf(requestJSONBody);
        	RestContext.request = req;
            RestContext.response= res;
    		ATSSearchAlertResultsService_v2.doPost();
        Test.stopTest();

	}
    
    public static testMethod void doPostReqAlertResultTest(){
		
        Search_Alert_Criteria__c criteria1 = new Search_Alert_Criteria__c( Alert_Criteria__c = 'Test', Name='Test',
                                                                          Alert_Trigger__c='Req Created',Send_Email__c= true, 
                                                                          Query_String__c = 'xyz' );
        insert criteria1;    
        
        Test.startTest();
			RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
        	String requestJSONBody = '{ "results":[{ "workRemote": true, "state": "MD", "skills": "java", "reqDivision": "Applications",'
                					+'"positionTitle": null, "positionId": null, "ownerId": "005240000080cM8AAI", "opcoName": "TEKsystems, Inc.",'
                					+'"jobStage": null, "jobOwner": "API Autouser", "jobMinRate": "80.0", "jobMaxRate": "100.0",'
                					+'"jobId": "0067A000007oEvCQAU", "jobDurationUnit": "Month(s)", "jobDuration": "4.0", "jobCreatedDate": null,'
                					+'"country": "UNITED STATES", "city": "Baltimore", "canUseApprovedSubVendor": true,'
                					+'"alertTrigger": [ "Req Created" ], "alertMatchTimestamp": "2020-07-06T16:30:44.305Z",'
                					+'"alertId": "'+criteria1.Id+'", "accountName": null } ]}';
            
            req.requestURI = '/services/apexrest/alertresults/v1/*';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(requestJSONBody);
            RestContext.request = req;
            RestContext.response= res;
            ATSSearchAlertResultsService_v2.doPost();
        
        	requestJSONBody = '{ "results":[{ "workRemote": true, "state": "MD", "skills": "java", "reqDivision": "Applications",'
                					+'"positionTitle": null, "positionId": null, "ownerId": "005240000080cM8AAI", "opcoName": "TEKsystems, Inc.",'
                					+'"jobStage": null, "jobOwner": "API Autouser", "jobMinRate": "80.0", "jobMaxRate": "100.0",'
                					+'"jobId": "0067A000007oEvCQAU", "jobDurationUnit": "Month(s)", "jobDuration": "4.0", "jobCreatedDate": null,'
                					+'"country": "UNITED STATES", "city": "Baltimore", "canUseApprovedSubVendor": true,'
                					+'"alertTrigger": [ "Req Created" ], "alertMatchTimestamp": "2020-08-06T16:30:44.305Z",'
                					+'"alertId": "'+criteria1.Id+'", "accountName": null } ]}';
            req.requestBody = Blob.valueOf(requestJSONBody);
        	RestContext.request = req;
            RestContext.response= res;
    		ATSSearchAlertResultsService_v2.doPost();
        Test.stopTest();

	}
}