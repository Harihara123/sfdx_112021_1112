/******************************************************************************************************************************
* Name        - Test_Staging_CreateTeamMember
* Description - test class for Staging_CreateTeamMember. 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Vivek Ojha(Appirio)        02/17/2015             Created
********************************************************************************************************************************/
@isTest(seeAlldata=false)
private class Test_Staging_CreateTeamMember {

     //variable declaration
   // static User user = TestDataHelper.createUser('Aerotek AM'); 
    static User user = TestDataHelper.createUser('System Administrator'); 
    static User testAdminUser= TestDataHelper.createUser('System Administrator');    
    static testMethod void testStaging() {
     
        TestData TdAcc = new TestData(1);
        List<Account> lstNewAccounts = TdAcc.createAccounts();
        Contact lstNewContact = TdAcc.createContactWithoutAccount(lstNewAccounts[0].id);
        Account_National_Owner_Fields__c cusSett = new Account_National_Owner_Fields__c();
        cusSett.Name = 'Aerotek_SSO_Owner__c';
        insert cusSett ;
        Product2 testProduct=TdAcc.createTestProduct();
        insert testProduct;
           
        if(user != Null) {
            Test.startTest(); 
            Reqs__c newreq;
             system.runAs(testAdminUser) {  
	            newreq = new Reqs__c (Account__c =  null, stage__c = 'Staging', Status__c = 'Open',                       
	            Address__c = 'Sample', City__c = 'Sample', State__c = 'Sample', Zip__c = '12345', Placement_Type__c = 'Contract',
	            Positions__c = 10, Draft_Reason__c = 'Proactive Req',Start_Date__c = system.today()+1,Filled__c= 9,OwnerId = testAdminUser.Id );             
	            insert newreq;
	            System.assertEquals(null,newreq.Account__c);
	            System.assertEquals('Staging',newreq.Stage__c );
	            //create a team member
	            //Req_Team_Member__c Teammemer = new Req_Team_Member__c(User__c= testAdminUser.Id, Requisition__c = newreq.Id);
	            //insert Teammemer;
             }
            system.runAs(user) {  
            	newreq.Primary_Contact__c = lstNewContact.id;
                newreq.Account__c = lstNewAccounts[0].Id;
                newreq.National_Account_Manager__c = userinfo.getuserid();
                newReq.Filled__c = 10;
                newReq.Bill_Rate_Max__c = 50; 
                newReq.Bill_Rate_Min__c = 25;
                newReq.Standard_Burden__c = 2;
                newReq.Duration__c = 5;
                newReq.Duration_Unit__c = 'days';
                newReq.Job_category__c = 'admin';
                newReq.Req_priority__c = 'green';
                newReq.Pay_Rate_Max__c = 10;
                newReq.Pay_Rate_Min__c = 5;
                newReq.Job_description__c = 'test';
                newReq.Organizational_role__c = 'Centre Manager';
                newReq.Product__c= testProduct.Id;
                newReq.stage__c =  'Staging';//'Qualified';
                System.debug('newReq.OwnerId=='+newReq.OwnerId);
                newReq.OwnerId = user.Id;
                System.debug('newReq.OwnerId=='+newReq.OwnerId);
                //newReq.Account__c = lstNewAccounts[0].Id;
               System.debug('newreq=58='+newreq.Account__c);
               TriggerStopper.isStagingTeamMemberTrigger=false;
                update newreq;
            }
            
            Reqs__c Reqs = [Select Id , Account__c From Reqs__c Where Id=:newReq.Id];
            System.assertNotEquals(null,newreq.Account__c);
            System.debug('Reqs.Account__c=='+Reqs.Account__c);
            Test.stopTest();    
             Reqs = [Select Id , Account__c From Reqs__c Where Id=:newReq.Id];  
             System.debug('Reqs.Account__c after stop=='+Reqs.Account__c);   
             
        }    
                    
    }
}