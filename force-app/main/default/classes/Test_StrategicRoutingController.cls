/*******************************************************************
* Name  : Test_StrategicRoutingController
* Author: Vivek Ojha(Appirio India) 
* Date  : June 22, 2015
* Details : Test class for StrategicRoutingController
* Modification Log :
* -----------------------------------------------------------------------------
* Developer                   Date                          Description
* ----------------------------------------------------------------------------  
*************************************************************************/
@isTest(seeAlldata= false)
private class Test_StrategicRoutingController{
    static testMethod void test_RoutingForSI() {
        User usr = TestDataHelper.createUser('System Administrator');
        usr.opco__c= 'ONS';
        usr.office__c= 'ONS';
        OPCO_Picklist_Mapping__c settings = new OPCO_Picklist_Mapping__c();
        List<OPCO_Picklist_Mapping__c> recList = new List<OPCO_Picklist_Mapping__c>();
        settings = new OPCO_Picklist_Mapping__c();
        settings.Name = 'AGS';
        settings.Picklist_Value__c = 'Allegis Global Solutions, Inc.';       
        recList.add(settings);
        settings = new OPCO_Picklist_Mapping__c();
        settings.Name = 'ONS';
        settings.Picklist_Value__c = 'Aerotek, Inc.';       
        recList.add(settings);
        settings = new OPCO_Picklist_Mapping__c();
        settings.Name = 'TEK';
        settings.Picklist_Value__c = 'TEKsystems, Inc.';       
        recList.add(settings);
        settings = new OPCO_Picklist_Mapping__c();
        settings.Name = 'TEM';
        settings.Picklist_Value__c = 'Allegis Group, Inc.';       
        recList.add(settings);
        insert recList;
        Routing_Controller_Settings__c settings1 = new Routing_Controller_Settings__c();
        List<Routing_Controller_Settings__c> FieldList = new List<Routing_Controller_Settings__c>();
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'GovernmentContractVehicleID';
        settings1.FieldID__c= 'CF00NK0000001lANJ_lkid';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'GovernmentContractVehicleName';
        settings1.FieldID__c= 'CF00NK0000001lANJ';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'OfficeName';
        settings1.FieldID__c= 'CF00NK0000001lANO';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'ParentAccountPlanID';
        settings1.FieldID__c= 'CF00NK0000001lANQ_lkid';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'ParentAccountPlanName';
        settings1.FieldID__c= 'CF00NK0000001lANQ';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'ParentStrategicInitiativeID';
        settings1.FieldID__c= 'CF00NK0000001lANR_lkid';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'ParentStrategicInitiativeName';
        settings1.FieldID__c= 'CF00NK0000001lANR';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'PrimaryAccountID';
        settings1.FieldID__c= 'CF00NK0000001lANU_lkid';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'PrimaryAccountName';
        settings1.FieldID__c= 'CF00NK0000001lANU';       
        FieldList.add(settings1);
        settings1 = new Routing_Controller_Settings__c();
        settings1.Name = 'Opp_Opco';
        settings1.FieldID__c= '00N8E000000lAbZ';       
        FieldList.add(settings1);
        
        
        insert FieldList;
        //getting record type info
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Strategic_Initiative__c; 
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = RecordTypeInfo.get('Strategic Initiative').getRecordTypeId();
       // string  rtId1 = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Complex').getRecordTypeId();

       //create account
        Account acc = TestDataHelper.createAccount();//name= Test Account;
        acc.name= 'Test Account';
        insert acc;
        // create strategic initiatives
        List<Strategic_Initiative__c> initiatives = TestDataHelper.getInitiativeRecords();
        initiatives[0].Primary_Account__c= acc.Id;
        initiatives[0].OpCo__c = 'Aerotek';
        initiatives[0].recordtypeid=rtId ;
        initiatives[0].Impacted_Region__c='test';
        insert initiatives[0];
        
        //Create opportunity
        Opportunity opp1=new opportunity(Name='test1',StageName='Interest',accountid=acc.id,recordtypeid=Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Strategic Opportunity').getRecordTypeId(),closeDate=date.today());
        insert opp1;
        
        System.runAs(usr ){
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(initiatives[0]);
            StrategicRoutingController ctrl = new StrategicRoutingController(sc);
             // create a new page reference, set it as the current page, 
            // and use the ApexPages.currentPage() method to put the values
           // PageReference PageRef = new PageReference ('/?ent=01IU0000000FsiE&retURL='+initiatives[0].Id+'&save_new_url=a1X');
            PageReference PageRef = new PageReference ('/apex/StrategicRecords?CF00NK0000001lANJ'+'='+initiatives[0].Name+'&CF00NK0000001lANJ_lkid'+'='+initiatives[0].id+'&RecordType='+rtId);            
            Test.setCurrentPage(PageRef);
            pagereference pg = ctrl.getRouter();
            System.assertNotEquals(null,pg);
           
           ApexPages.StandardController sc0 = new ApexPages.StandardController(initiatives[0]);
           StrategicRoutingController ctr0 = new StrategicRoutingController(sc0);
             // create a new page reference, set it as the current page, 
            // and use the ApexPages.currentPage() method to put the values
           // PageReference PageRef = new PageReference ('/?ent=01IU0000000FsiE&retURL='+initiatives[0].Id+'&save_new_url=a1X');
            PageReference PageRef0 = new PageReference ('/apex/StrategicRecords?CF00NK0000001lANR'+'='+initiatives[0].Name+'&CF00NK0000001lANR_lkid'+'='+initiatives[0].id+'&RecordType='+rtId);            
            Test.setCurrentPage(PageRef0);
            pagereference pg0 = ctrl.getRouter();
            System.assertNotEquals(null,pg0);
           
            ApexPages.StandardController sc1 = new ApexPages.StandardController(initiatives[0]);
            StrategicRoutingController ctrl1 = new StrategicRoutingController(sc1);
             // create a new page reference, set it as the current page, 
            // and use the ApexPages.currentPage() method to put the values
           // PageReference PageRef = new PageReference ('/?ent=01IU0000000FsiE&retURL='+initiatives[0].Id+'&save_new_url=a1X');
            PageReference PageRef1 = new PageReference ('/apex/StrategicRecords?CF00NK0000001lANU'+'='+acc.Name+'&CF00NK0000001lANU_lkid'+'='+acc.id);            
            Test.setCurrentPage(PageRef1);
            pagereference pg1 = ctrl1.getRouter();
            
            System.assertNotEquals(null,pg1);
            
            
            ApexPages.StandardController op1 = new ApexPages.StandardController(opp1);
            OpportunityRoutingController ORctrl1 = new OpportunityRoutingController(op1);
             // create a new page reference, set it as the current page, 
            // and use the ApexPages.currentPage() method to put the values
           // PageReference PageRef = new PageReference ('/?ent=01IU0000000FsiE&retURL='+initiatives[0].Id+'&save_new_url=a1X');
            PageReference PageRef2 = new PageReference ('/apex/OpportunityRecords?accid=Accountid' );            
            Test.setCurrentPage(PageRef2);
            pagereference pg2 = ORctrl1.getRouter();
            
            System.assertNotEquals(null,pg2);
            PageReference PageRef3 = new PageReference ('/apex/OpportunityRecords' );
            Test.setCurrentPage(PageRef3);
            pagereference pg3 = ORctrl1.getRouter();
            
            Test.stopTest();
        }
         
    }
}