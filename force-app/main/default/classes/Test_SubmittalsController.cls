/******************************************************************************************************************************
* Name        - Test_SubmittalsController 
* Description - Test method for Submittals controller 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pavan                       03/01/2012             Created
* Dhruv                       03/27/2012             Code for calling Contoller functions
* Ganesh                      15/06/2012             Refactored 
* Nidish                      4/1/2015               updated to increase code coverage
********************************************************************************************************************************/

@isTest
private class Test_SubmittalsController{
     static User user = TestDataHelper.createUser('System Administrator');
    static testMethod void StartsControllerTest() {
        Test.startTest();
     if(user != Null) {
            system.runAs(user) {       
                //Create an Account record
                TestData Td = new TestData(1);
                List<Account> lstAccounts = Td.createAccounts();
                
                lstAccounts[0].Siebel_ID__c='1-4W6J-1';
                update lstAccounts[0]; 
                system.assertnotequals(lstAccounts[0].Siebel_ID__c,null);
                
               //Create a Req record
                Reqs__c recRequisition = new Reqs__c( Account__c = lstAccounts[0].Id, Max_Salary__c = 2000.00,
                Address__c = 'US', Stage__c = 'Qualified',  Placement_Type__c = 'Permanent',
                Positions__c = 10, Duration_Unit__c = 'Permanent', Siebel_ID__c = '1-4W6J-1', 
                Job_Description__c = 'Sample', City__c = 'Sample', state__c = 'Sample',
                Zip__c = 'Sample', Min_Salary__c = 1000.00,Duration__c = 12,Start_Date__c = system.today());
                insert recRequisition;
                system.assertnotequals(recRequisition.Siebel_ID__c,null);
                
                //Call functions in the SubmittalsController class
                ApexPages.StandardController sc = new ApexPages.StandardController(recRequisition);
                SubmittalsController clsSubmittal = new SubmittalsController(sc);
                                
                List<SubmittalsController.SubmittalWrapper> listSubmittalsFull = new List<SubmittalsController.SubmittalWrapper>();
                List<SubmittalsController.SubmittalWrapper> listSubmittalsFullSorted = new List<SubmittalsController.SubmittalWrapper>();
                listSubmittalsFull = clsSubmittal.getSubmittals('SIEBEL',recRequisition.Siebel_ID__c);
                system.debug('***listSubmittalsFull: '+listSubmittalsFull);
                if(listSubmittalsFull != null && listSubmittalsFull.size()>0)
                {
                    for(Integer i = 1 ; i <= 7 ; i++)    
                    {                     
                          listSubmittalsFullSorted = clsSubmittal.sortRecords(listSubmittalsFull,SubmittalsController.DESCEND,i);
                    }
                }
                clsSubmittal.submittalsInformation();
                clsSubmittal.startRequest();
                clsSubmittal.sortAction();       
                clsSubmittal.getPreviousButtonEnabled();
                clsSubmittal.getNextButtonEnabled();
                clsSubmittal.firstBtnClick();
                clsSubmittal.previousBtnClick();
                clsSubmittal.nextBtnClick();
                clsSubmittal.lastBtnClick();
                WS_TIBCO_Submittal wsSub = new WS_TIBCO_Submittal();
               //  WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint testSub = new  WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                 
                //Assign a null value to account's Siebel_ID field and call logErrors function    
                recRequisition.Siebel_ID__c = Null;
                update recRequisition;
                sc = new ApexPages.StandardController(recRequisition);
                clsSubmittal = new SubmittalsController(sc);
            }
        }        
        Test.stopTest();
   }   
   
   
   static testMethod void StartsControllerTest1() {
        Test.startTest();
     if(user != Null) {
            system.runAs(user) {       
                //Create an Account record
                TestData Td = new TestData(1);
                List<Account> lstAccounts = Td.createAccounts();
                
                lstAccounts[0].Siebel_ID__c='1-4W6J-1';
                update lstAccounts[0]; 
                system.assertnotequals(lstAccounts[0].Siebel_ID__c,null);
                
               //Create a Req record
                Reqs__c recRequisition = new Reqs__c( Account__c = lstAccounts[0].Id, Max_Salary__c = 2000.00,
                Address__c = 'US', Stage__c = 'Qualified',  Placement_Type__c = 'Permanent',
                Positions__c = 10, Duration_Unit__c = 'Permanent', Siebel_ID__c = '1-4W6J-1', 
                Job_Description__c = 'Sample', City__c = 'Sample', state__c = 'Sample',
                Zip__c = 'Sample', Min_Salary__c = 1000.00,Duration__c = 12,Start_Date__c = system.today());
                insert recRequisition;
                system.assertnotequals(recRequisition.Siebel_ID__c,null);
                
                //Call functions in the SubmittalsController class
                ApexPages.StandardController sc = new ApexPages.StandardController(recRequisition);
                SubmittalsController clsSubmittal = new SubmittalsController(sc);
                
                 /*               
                List<SubmittalsController.SubmittalWrapper> listSubmittalsFull = new List<SubmittalsController.SubmittalWrapper>();
                List<SubmittalsController.SubmittalWrapper> listSubmittalsFullSorted = new List<SubmittalsController.SubmittalWrapper>();
                listSubmittalsFull = clsSubmittal.getSubmittals('SIEBEL',recRequisition.Siebel_ID__c);
                system.debug('***listSubmittalsFull: '+listSubmittalsFull);
                if(listSubmittalsFull != null && listSubmittalsFull.size()>0)
                {
                    for(Integer i = 1 ; i <= 7 ; i++)    
                    {                     
                          listSubmittalsFullSorted = clsSubmittal.sortRecords(listSubmittalsFull,SubmittalsController.DESCEND,i);
                    }
                } */
                clsSubmittal.submittalsInformation();
                clsSubmittal.startRequest();
                clsSubmittal.exceptionMessage = 'test';
                clsSubmittal.exceptionDescription = 'test';
                clsSubmittal.sortAction();
                clsSubmittal.sortAction(); 
                clsSubmittal.prvSortColIndex =1; 
                clsSubmittal.sortColIndex =1;     
                clsSubmittal.getPreviousButtonEnabled();
                clsSubmittal.getNextButtonEnabled();
                clsSubmittal.firstBtnClick();
                clsSubmittal.previousBtnClick();
                clsSubmittal.nextBtnClick();
                clsSubmittal.lastBtnClick();
                WS_TIBCO_Submittal wsSub = new WS_TIBCO_Submittal();
                // WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint testSub = new  WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                 
                //Assign a null value to account's Siebel_ID field and call logErrors function    
                recRequisition.Siebel_ID__c = Null;
                update recRequisition;
                sc = new ApexPages.StandardController(recRequisition);
                clsSubmittal = new SubmittalsController(sc);
            }
        }        
        Test.stopTest();
   }  
   
   
}