@isTest
public class Test_TalentCreationService {
    @isTest static void testCreateTalent() {
        RestRequest request = new RestRequest();
        request.requestUri = URL.getSalesforceBaseUrl().toExternalForm()+'Talent/TalentInfo/V1/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"status": "Working"}');
        RestContext.request = request;
        TalentCreationService.TalentCreationRespWrapper wrapperResponse = TalentCreationService.createTalent();
        System.assertEquals(wrapperResponse.message, 'Talent creation/update successful');
    }
}