@isTest
public class Test_TalentDeactivateRESTService{
    static testMethod void testDoPost(){
    RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Person/TalentDeactivate/V1';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        Account  a = Test_TalentDeactivateRESTService.createTalentAccount();
        Contact c = Test_TalentDeactivateRESTService.createTalentContact(a);
        TalentDeactivateRESTService obj = new TalentDeactivateRESTService();
        TalentDeactivateRESTService.doPost(c.id,true);
         TalentDeactivateRESTService.doPost(null,null);
        Test.stopTest();

     }
    static testMethod void testDoPostFalse(){
    RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Person/TalentDeactivate/V1';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        Account  a = Test_TalentDeactivateRESTService.createTalentAccount();
       
        Contact c = Test_TalentDeactivateRESTService.createTalentContact(a);
        TalentDeactivateRESTService obj = new TalentDeactivateRESTService();
        TalentDeactivateRESTService.doPost(c.id,false);
        //a.Talent_Ownership__c = 'INACTIVE';
        TalentDeactivateRESTService.doPost(c.id,true);
        Test.stopTest();

     }
    public static Account createTalentAccount() {
        Account testAccount = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount',Talent_Ownership__c = 'TEK'
            );
        insert testAccount;
        testAccount.Talent_Ownership__c = 'RWS';
        update testAccount;
        return testAccount;
    }
     public static Account createTalentCandidateAccount() {
        Account testAccount = new Account(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName Not IN('Candidate') AND SObjectType = 'Account' limit 1][0].Id,
            Name = 'testAccount1'
            );
        insert testAccount;
        return testAccount;
    }
    
    static testMethod void testFailedTest(){
        Test.startTest();
        Account  a = Test_TalentDeactivateRESTService.createTalentCandidateAccount();
        Contact c = Test_TalentDeactivateRESTService.createTalentContact(a);
      /*  c.RecordTypeId = a.RecordTypeId;
        update c;*/
        TalentDeactivateRESTService obj = new TalentDeactivateRESTService();
        TalentDeactivateRESTService.doPost(c.id,false);
        Test.stopTest();
    }
     static testMethod void testInvalidTest(){
        Test.startTest();
        Account  a = Test_TalentDeactivateRESTService.createTalentAccount();
        Contact c = Test_TalentDeactivateRESTService.createTalentContact(a);
      /*  c.RecordTypeId = a.RecordTypeId;
        update c;*/
         try{
        c.id = c.id+'0';
             
        TalentDeactivateRESTService obj = new TalentDeactivateRESTService();
        TalentDeactivateRESTService.doPost(c.id,false);
         }Catch(Exception ex )
        {
            string error = 'System.StringException';
            System.debug('ex::'+ex);
        // System.assertEquals(error, ex);
        }
        Test.stopTest();
    }


    public static Contact createTalentContact(Account testAccount) {
        Contact testContact = new Contact(
            RecordTypeId = [select Id, DeveloperName from RecordType where DeveloperName = 'Talent' AND SObjectType = 'Contact' limit 1][0].Id,
            AccountId = testAccount.Id,
            Email = 'testContact@email.com',
            LastName = 'testLastName',
            FirstName = 'testFirstName',
            Title = 'ContactTitle',
            MobilePhone = '9696969696',
            HomePhone = '1234569870',
            Phone = '9876543210',
            OtherPhone = '5555555550',
            Other_Email__c = 'bca@bca.bca',
            MailingStreet = '312 ABC Way',
            MailingCity = 'Test City',
            MailingState = 'US.MD',
            MailingCountry = 'US',
            MailingLatitude = 33.386532,
            MailingLongitude = -112.215578
        );
        insert testContact;
        System.debug('Test Account ' + testContact);
        return testContact;
    } 
}