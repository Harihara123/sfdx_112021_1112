@isTest
public with sharing class Test_TalentFirstListController {
    
    @TestSetup
    static void makeData(){
        // create 1 account
        Account acct1 = TestData.newAccount(1, 'Talent');
        Account acct2 = TestData.newAccount(1, 'Client');
        insert new List<Account>{acct1, acct2};
        // creat contact
        Contact cont1 = TestData.newContact(acct1.Id, 1, 'Talent');
        Contact cont2 = TestData.newContact(acct2.Id, 1, 'Client');
        insert new List<Contact> {cont1, cont2};
        
        Opportunity opp = new Opportunity(Name='TESTOPP',
			recordtypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Talent First Opportunity').getRecordTypeId(),            
            CloseDate = system.today(),
            StageName = 'test',
            accountId = acct1.Id,            
            Req_Job_Title__c = cont1.Title,
            Location__c = 'Hanover, MD',
            CreatedDate=system.today()
        );
        insert opp;    
        
        OpportunityContactRole ocr = new OpportunityContactRole(
			ContactId = cont1.Id, 
            OpportunityId=opp.Id
        );
		insert ocr;

    }

    @isTest 
    public static void getListData() {
        Account acct = [SELECT Id FROM Account WHERE RecordType.Name = 'Talent'];
        Contact cont = [SELECT Id FROM Contact WHERE RecordType.Name = 'Talent'];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE RecordType.Name = 'Talent First Opportunity'];

        Map<Id, String> contIdMap = new Map<Id, String>();
        contIdMap.put(cont.Id, 'ContactId');
        TalentFirstOpportunity.CreateTalentFirstOpportunity(contIdMap,null);
        List<TalentFirstListController.TalentFirstOpp> tfos = TalentFirstListController.getListViewData(cont.Id);
        system.assertEquals(false, tfos.isEmpty(), 'No Talent First Records retrieved for talent');
        system.assertNotEquals(opp.Id, tfos[0].id, 'Opportunity Id matches TFO Opportunity Id but should be different Opportunities');
    }
}
