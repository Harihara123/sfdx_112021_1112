@isTest
public class Test_TalentFirstOpportunity {
    
    @testSetup static void setup() {
        List<Account> accs = new List<Account>();
        Account acc = new Account(
            Name = 'TESTACCT',
            Phone= '2345',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Talent').getRecordTypeId());
        accs.add(acc);
        acc = new Account(
            Name = 'TESTACCT1',
            Phone= '23451',ShippingCity = 'Testshipcity',ShippingCountry = 'Testshipcountry',
            ShippingPostalCode = 'TestshipCode',ShippingState = 'Testshipstate',
            ShippingStreet = 'Testshipstreet',BillingCity ='TestBillstreet',
            BillingCountry ='TestBillcountry',BillingPostalCode ='TestBillCode',
            BillingState ='TestBillState',BillingStreet ='TestBillStreet',AccountSource='Data.com',
            recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        accs.add(acc);
        insert accs;
        
        List<Contact> cons = new List<Contact>();
    	Contact con = new Contact();
        con.FirstName = 'First';
        con.LastName = 'Last';
        con.Title = 'Java Developer';
        con.AccountId = accs[0].Id;
        con.Phone = '1111111';
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Talent').getRecordTypeId();
        cons.add(con);
        con = new Contact();
        con.FirstName = 'First';
        con.LastName = 'Last1';
        con.Title = 'Java Developer';
        con.AccountId = accs[1].Id;
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        con.Phone = '1111111';
        cons.add(con);
        insert cons;
                
        Opportunity opp = new Opportunity(Name='TESTOPP',
			recordtypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Talent First Opportunity').getRecordTypeId(),            
            CloseDate = system.today(),
            StageName = 'test',
            accountId = cons[0].AccountId,            
            Req_Job_Title__c = cons[0].Title,
            Location__c = 'Hanover, MD',
            CreatedDate=system.today());
        insert opp;    
        
        OpportunityContactRole ocr = new OpportunityContactRole(
			ContactId = cons[1].Id, OpportunityId=opp.Id);
		insert ocr;
    }
    
    public static testmethod void testPerformSFServerCall(){        
        TestData tstData=new TestData(1);
        List<Contact> contList=tstData.createContacts('Talent');
        Map<String, Object> parameters=new Map<String, Object>();
        parameters.put('ContactId',contList[0].id);
        List<Id> TFOIdList=TalentFirstOpportunity.PerformSFServerCall('CreateTalentFirstOpportunity',parameters);
        System.assertEquals(TFOIdList.size()>0, true);
        
        List<OpportunityContactRole> oppContRoles=new List<OpportunityContactRole>();
        oppContRoles=TalentFirstOpportunity.getContactRoles((String)TFOIdList[0]);
        System.assertEquals(oppContRoles.size(), 0);
    }
    
    @isTest static void getContactRolesTest() {
		List<Opportunity> opps = [Select Id, AccountId From Opportunity];        
        List<OpportunityContactRole> ocrs = [Select Id From OpportunityContactRole];
        List<OpportunityContactRole> ocrs1 = TalentFirstOpportunity.getContactRoles(opps[0].Id);
        system.assertEquals(ocrs1.size(), ocrs.size());    
        Contact con = TalentFirstOpportunity.matchReq(opps[0].Id);
        COntact con1 = [Select Id, AccountId From Contact Where AccountId=:opps[0].AccountId LIMIT 1];
        system.assertEquals(con.AccountId, con1.AccountId);
    }
}