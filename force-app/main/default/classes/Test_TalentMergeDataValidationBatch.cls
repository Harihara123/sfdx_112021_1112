@isTest
public class Test_TalentMergeDataValidationBatch  {
    
    @isTest
    public static void testMergeDataValidationBatch() {
        Test.startTest();
		Account acc1 = CreateTalentTestData.createTalent();
		Contact con1 = CreateTalentTestData.createTalentContact(acc1);
        Account acc2 = CreateTalentTestData.createTalentAccount();
        Contact con2 = CreateTalentTestData.createTalentContact(acc2);
        List<Contact> dupCons = new List<Contact>();
        dupCons.add(con2);
        CreateTalentTestData.createWorkHistory(acc1);
        CreateTalentTestData.createTalentTask(con1);
        Talent_Referral__c tr = new Talent_Referral__c(First_Name__c = 'Test', Last_Name__c = 'Test', Talent__c=acc1.Id, Phone__c = '1111111111');
        Insert tr;
        /*
        Case tCase = new Case(
                              RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('JobApplication').getRecordTypeId(),
                              Origin = 'Web',
                              IsVisibleInSelfService = true,
                              Description__c='Test',
        					  accountId = acc1.Id);
        insert tCase;*/
        Assessment__c assessmentObj = new Assessment__c(Assessment_Name__c = 'Test', Talent__c = con1.Id);
        Insert assessmentObj;
        AccountTeamMember atm = new AccountTeamMember(AccountId = acc1.Id, UserId = UserInfo.getUserId());
        Insert atm;
        Test.stopTest();
        Opportunity opp = new Opportunity(Name = 'TESTOPP', StageName = 'Offer Accepted', AccountId = acc1.id, Req_Product__c = 'Permanent', CloseDate = System.today(), OpCo__c = 'Major, Lindsey & Africa, LLC');
        insert opp;

        Id recTypeId = [SELECT id FROM RecordType WHERE Name = 'OpportunitySubmission'].Id;

        Order o = new Order(Name = 'TestOrder',AccountId = acc1.id,ShipToContactId = con1.Id,OpportunityId = opp.Id,Status = 'Linked',EffectiveDate = System.today(),RecordTypeId = recTypeId);
        //insert o;
        
        Talent_Recommendation__c tRec = new Talent_Recommendation__c(Talent_Contact__c = con1.Id);
        Insert tRec;
        
        Tag_Definition__c td = new Tag_Definition__c(Tag_Name__c='Test');
        Insert td;
        
        Contact_Tag__c ct = new Contact_Tag__c(Contact__c = con1.Id,Tag_Definition__c=td.Id);
        Insert ct;
        
        Talent_Merge_Mapping__c tmm = new Talent_Merge_Mapping__c(Account_Victim_Id__c = acc2.Id, Account_Survivor_Id__c = acc1.Id, Contact_Victim_Id__c = con2.Id, Contact_Survivor_Id__c = con1.Id, Batch_Id__c=16);
		Insert tmm;

		Double preBatchId = 16;
		String preMergeState = 'PRE';
		String preQuery = 'Select Id,Batch_Id__c,Account_Victim_Id__c,Account_Survivor_Id__c,Contact_Victim_Id__c,Contact_Survivor_Id__c from Talent_Merge_Mapping__c where Batch_Id__c  = '+ preBatchId;
		TalentMergeDataValidationBatch preObj = new TalentMergeDataValidationBatch(preQuery, preMergeState);
		Database.executeBatch(preObj,200);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/talent/update/accountcontact/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        String result = AtsComTalentMergeUpdateController.doPost(acc1.Id, acc2.Id, con1.Id, con2.Id, tmm.Id);
        
        //Database.merge(con1, dupCons, false);

		Double postBatchId = 16;
		String postMergeState = 'POST';
		String postQuery = 'Select Id,Batch_Id__c,Account_Victim_Id__c,Account_Survivor_Id__c,Contact_Victim_Id__c,Contact_Survivor_Id__c from Talent_Merge_Mapping__c where Batch_Id__c  = '+ postBatchId;
		TalentMergeDataValidationBatch postObj = new TalentMergeDataValidationBatch(postQuery, postMergeState);
		Database.executeBatch(postObj,200);
    }
}