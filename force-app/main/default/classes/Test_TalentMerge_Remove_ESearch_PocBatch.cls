@isTest
public class Test_TalentMerge_Remove_ESearch_PocBatch  {


   private static final Decimal NumberQueueable = 100; 
    

  public static testmethod void testBatch() {


     createRecords(100); 
     String query = 'SELECT Id, ESearch_Dup_Id__c, ESearch_Fetched__c FROM TalentMerge_RemoveDup__c'; 
     Test.StartTest();
        ID batchprocessid = Database.executeBatch(new TalentMerge_Remove_ESearch_PocBatch(query) , 100);
     Test.StopTest();

     System.assert(true);

  }
   

 


  private static List< TalentMerge_RemoveDup__C> createRecords(Integer numberOfRecords) {

    List<TalentMerge_RemoveDup__C> listTMRD = new List< TalentMerge_RemoveDup__C>(); 

     for (Integer i = 0; i < numberOfRecords; ++i ) {
         TalentMerge_RemoveDup__C tmRD = new  TalentMerge_RemoveDup__C( ESearch_Dup_Id__c = generateRandomString(18), Merge_Mapping_Id__c =  generateRandomString(18)); 
         listTMRD.add (tmRD);
     }
     insert listTMRD;
     return  listTMRD;

  }


  private static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
   }




}