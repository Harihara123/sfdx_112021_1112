@isTest
public class Test_TalentWrapper {
	static testMethod void testParse() {
    String json = '{'+
    '\"sourceTracking\":{'+
    '\"source\":\"SourceBreaker\",'+
    '\"vendor\":\"CVLibrary\",'+
    '\"transactionId\":\"AB1234Z\",'+
    '\"externalCandidateId\":\"SBx213\"'+
    '},'+
    '\"recruiter\":{'+
    '\"firstName\":\"Praveen\",'+
    '\"lastName\":\"Chinnappa\",'+
    '\"email\":\"pchinnappa@testmail.com\"'+
    '},'+
    '\"talent\":{'+
    '\"profile\":{'+
    '\"firstName\":\"TestFirstName\",'+
    '\"lastName\":\"TestLastName\",'+
    '\"jobTitle\":\"JavaDeveloper\",'+
    '\"desiredSalary\":\"$100000\",'+
    '\"desiredRate\":\"$50\",'+
    '\"desiredFrequency\":\"Daily\",'+
    '\"workEligibility\":\"Eligible to work in France\",'+
    '\"geographicPreference\":\"Within France\",'+
    '\"highestEducation\":\"Bachelor\'s Degree in Mathematics\",'+
    '\"securityClearance\":\"Highest Secturity\"'+
    '},'+
    '\"contact\":{'+
    '\"phone\":{'+
    '\"mobile\":\"+43123421 54\",'+
    '\"work\":\"112 4324 6543\",'+
    '\"home\":\"\",'+
    '\"unspecified\":['+
    '\"1223212233\",'+
    '\"152431233\"'+
    ']'+
    '},'+
    '\"email\":{'+
    '\"primary\":\"\",'+
    '\"work\":\"\",'+
    '\"other\":\"\",'+
    '\"unspecified\":['+
    '\"testa@testmail.com\",'+
    '\"teat123@teatmail.com\"'+
    ']'+
    '},'+
    '\"address\":{'+
    '\"streetAddress\":\"\",'+
    '\"city\":\"\",'+
    '\"stateProvince\":\"\",'+
    '\"country\":\"GB\",'+
    '\"postalCode\":\"CB123\"'+
    '},'+
    '\"resume\":{'+
    '\"fileType\":\"pdf\",'+
    '\"base64\":\"aswsdf\"'+
    '},'+
    '\"attachment\":{'+
    '\"fileType\":\"doc\",'+
    '\"base64\":\"awesdf\"'+
    '}'+
    '}'+
    '}'+
    '}';
    TalentWrapper obj = TalentWrapper.parse(json);
    System.assert(obj != null);
    }
}