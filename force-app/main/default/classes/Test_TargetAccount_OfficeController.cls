@isTest
private class Test_TargetAccount_OfficeController{
    
    static User user = TestDataHelper.createUser('Aerotek AM');
    
    static testmethod void test_TargetAccountByOffice() {
        
        user.Office_Code__c = '12345';
        user.Office__c = 'Sample test data';        
        User_Organization__c office = new User_Organization__c(name = 'Sample',active__c = true,Office_Code__c = '12345');
        insert office;
               
        system.runAs(user) {
            Group grp = new Group(Name = 'Test Queue', Type = 'QUEUE');
            insert grp;
            QueuesObject queue = new QueuesObject (queueid = grp.id, sobjectType = 'Target_Account__c');
            insert queue;
            TestData TdAcc = new TestData(1);
            List<Account> lstNewAccounts = TdAcc.createAccounts();      
                  
            Test.startTest();
            
            PageReference pageRef = Page.TargetAccount_Offices;
            Test.setCurrentPage(pageRef); 
            Apexpages.currentPage().getParameters().put('accountId',lstNewAccounts[0].Id);
            TargetAccount_OfficeController controller = new TargetAccount_OfficeController();
            
            for(TargetAccount_OfficeController.officeInformation ofc : controller.offices) {
               ofc.selected = true;               
            }
            
            PageReference pg = controller.createTargetAccounts();            
            system.assertEquals('/'+lstNewAccounts[0].Id,pg.getUrl());
        } 
    }
}