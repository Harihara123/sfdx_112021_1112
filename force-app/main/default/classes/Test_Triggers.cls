@isTest(SeeallData=False)
private class Test_Triggers
{
      static User user = TestDataHelper.createUser('System Administrator');
    // test method for target accounts
       static testmethod void testTargetAccountTriggers()
       {
       
       if(user != Null) {        
            system.runAs(user) {       
       Account a = new Account();
              a.name='abc';
               a.RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
              insert a;
             
              //create an instance for target Account
              Target_Account__c tAccount = TestDataHelper.createTargetAccount();
             List<Task>  tsk =TestDataHelper.getTasks2();
             
              insert tAccount;
              insert tsk;
              Boolean beforetargetconst=false;
              Boolean beforetarget=tAccount.target__c;
              System.assertEquals(beforetargetconst,beforetarget);
              // Test for deletion
              
              List<AccountTeamMember> StdTeam =[Select id, AccountId, userid from AccountTeamMember where Accountid=:tAccount.Account__c];
              system.assertequals(StdTeam[0].userid,tAccount.user__c);
              
              tAccount.MDM__c=true;
              tAccount.Account__c=a.id;
              update taccount;
              List<AccountTeamMember> StdTeam2 =[Select id, AccountId, userid from AccountTeamMember where Accountid=:taccount.Account__c];
              system.assertnotequals(StdTeam[0].AccountId,tAccount.Account__c);
              
             
              
              
              delete tAccount;
              undelete taccount;
              
              
              }
         }
         }
}