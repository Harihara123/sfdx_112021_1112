@isTest
public class Test_UncommittedObjectsCleanupBatch {
	static testMethod void validateCleanupBatch() {
        Account testAccount = CreateTalentTestData.createTalent();
        
        Test.startTest();
        UncommittedObjectsCleanupBatch clBatch1 = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.ACCOUNT, 'SELECT Id from Account where RecordType.Name=\'Talent\' and Id=\'' + testAccount.Id + '\'');
        Database.executeBatch(clBatch1);
        // Account hrxmlAct = [SELECT Id, Talent_HRXML__c FROM Account WHERE Id = :testAccount.Id];
		// Commenting out for R1 since this assertion fails when the validation is run on Prod.
        //System.assert(hrxmlAct.Talent_HRXML__c != null, 'Generated HRXML is null!');
        
        UncommittedObjectsCleanupBatch clBatch2 = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.ACCOUNT, null);
        Database.executeBatch(clBatch2);

        UncommittedObjectsCleanupBatch clBatch3 = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.CONTACT, null);
        Database.executeBatch(clBatch3);

        UncommittedObjectsCleanupBatch clBatch4 = new UncommittedObjectsCleanupBatch(UncommittedObjectsCleanupBatch.CLEANUP_TARGET.TALENT_DOCUMENT, null);
        Database.executeBatch(clBatch4);

        Test.stopTest();
    }
}