/***************************************************************************************************************************************
* Name        - Test_UpdateLastEventContactOpCo 
* Description - This test class covers the trigger : trg_Event_UpdateLastEventOpCo_OnContact_Temp
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       3/16/2013             Created
*****************************************************************************************************************************************/


@isTest(SeeallData=false)
private Class Test_UpdateLastEventContactOpCo {
    public static testMethod void testInsertEventContactOpCo()
     {
          Profile userProfile = [select Name from Profile where Name = 'Aerotek AM'];
          long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
       Integer random = Math.Round(Math.Random() * timeDiff );
         
          User usr = new User (
       alias = 'test', 
       email='testuser@allegisgroup.com',
       emailencodingkey='UTF-8', 
       lastname= 'TesterLastanme', 
       languagelocalekey='en_US',
       localesidkey='en_US', 
       profileid = userProfile.Id,
       timezonesidkey='America/Los_Angeles', 
       OPCO__c = 'test',
       Region_Code__c = 'test',
       Office_Code__c = '10001',
       // Peoplesoft_Id__c = string.valueOf(random * 143),
       username='testusr'+random+'@allegis.com.dev',
       CommunityNickname='tlevopco');
       insert usr;
      
        system.debug('UserCreated: ' + usr.CompanyName);
         System.runAs(usr) {
       Contact ct = new Contact( FirstName='Test',LastName='Contact',Last_Event_OpCo__c='',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId(),email='test@gmail.com');
       insert ct;
      
       system.debug('Contact_OpCo_before: ' + ct.Last_Event_OpCo__c);
      
      
      
      
      
      
         
       //create the event and insert it to fire the trigger
        Event evnt = new Event (Subject = 'TestTest',OwnerID= usr.Id, WhoId = ct.Id, Description = 'Test',DurationInMinutes=0,ActivityDateTime =system.now());
        
        test.startTest();
        insert evnt;
        test.stopTest();
        
        // This will test if the contact's eventOpCo has been updated
        ct = [Select Last_Event_OpCo__c from Contact where Id = :ct.Id];
       
        system.debug('Contact_OpCo_after: ' + ct.Last_Event_OpCo__c);
        System.assertEquals(usr.CompanyName, ct.Last_Event_OpCo__C);
         }
     }
     
     public static testMethod void testUpdateEventContactOpCo()
     {
          Profile userProfile = [select Name from Profile where Name = 'Aerotek AM'];
         long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
       Integer random = Math.Round(Math.Random() * timeDiff );
      
         User usr1 = new User (
       alias = 'test2', 
       email='testuser2@allegisgroup.com',
       emailencodingkey='UTF-8', 
       lastname= 'Tester2Lastanme', 
       languagelocalekey='en_US',
       localesidkey='en_US', 
       profileid = userProfile.Id,
       timezonesidkey='America/Los_Angeles', 
       OPCO__c = 'test',
       Region_Code__c = 'test',
       Office_Code__c = '10001',
       // Peoplesoft_Id__c = string.valueOf(random * 143),
       username='test2usr'+random+'@allegis.com.dev',
       CommunityNickname='tlevopco',companyname='testcompany');
       insert usr1;
      
       system.debug('UserCreated: ' + usr1.CompanyName);
         
         System.runAs(usr1) {
       Contact ct = new Contact( FirstName='Test2',LastName='Contact2',Last_Event_OpCo__c='',recordtypeid=Schema.SObjectType.contact.getRecordTypeInfosByName().get('Client').getRecordTypeId(),email='test@gmail.com');
       insert ct;
      
       system.debug('Contact_OpCo_before: ' + ct.Last_Event_OpCo__c);
      
      
      
       
       
         
       //create the event and update it to fire the trigger
        Event newevent = new Event(Subject = 'Test2Test',OwnerID= usr1.Id, WhoId = ct.Id, Description = '',DurationInMinutes=0,ActivityDateTime =system.now());
        insert newevent;
        Event evnt = new Event();
        evnt = [SELECT Description FROM Event WHERE Id = :newevent.Id];
        evnt.Description = 'This is a Completed Event';
        test.startTest();
        update evnt;
        test.stopTest();
        
        // This will test if the contact's eventOpCo has been updated
        ct = [SELECT Last_Event_OpCo__c FROM Contact WHERE Id = :ct.Id];
       
        system.debug('Contact_OpCo_after: ' + ct.Last_Event_OpCo__c);
        System.assertEquals(usr1.CompanyName, ct.Last_Event_OpCo__C);
         }
     }
      
 }