@isTest(seeAlldata=false)
private class Test_UpdateLegacyOrderBatch{

//static User user = TestDataHelper.createUser('System Administrator');
static User user = TestDataHelper.createUser('System Integration');

static testMethod void SourceSystemFieldsUpdate(){
     DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
     if(user != Null) {
            system.runAs(user) {
                List<Opportunity> lstOpportunity = new List<Opportunity>();
                
                TestData TdAcc = new TestData(1);
                testdata tdcont = new Testdata();
                // Get req recordtype Id from a name        
                string ReqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
                
                for(Account Acc : TdAcc.createAccounts()) {                     
                    Opportunity NewOpportunity = new Opportunity( Name = 'New Opportunity'+Acc.name , Accountid = Acc.id,
                         RecordTypeId = ReqRecordTypeId, Formal_Decision_Criteria__c = 'Defined', Opco__c ='Aerotek, Inc.',BusinessUnit__c ='EASI',
                        Response_Type__c = 'Formal RFx' , stagename = 'Draft', Champion_Exec_as_Key_Decision_Maker__c ='Solution is required, pain is verified and felt',
                        Customers_Application_or_Project_Scope__c = 'Precisely Defined',
                        Impacted_Regions__c='Option1',Access_To_Funds__c = 'abc', Aligned_to_defined_GS_Objective__c = 'Yes', Compelling_Event__c = 'Solution is required, pain is verified and felt',
                        Services_Capabilities_Alignment__c = 'TEK frequently provides this service; same vertical; proof points are available',
                        Sales_Resource_Requirements__c = 'Less than standard effort/light investment',CloseDate = system.today()+1,
                                                                 Req_Job_Description__c = 'Test',Req_Qualification__c = 'Test',Req_Product__c = 'Contract',Req_Bill_Rate_Max__c=10,
                                                                 Req_Bill_Rate_Min__c=1,Req_Duration_Unit__c = 'Day(s)',Req_Duration__c=10,Req_Pay_Rate_Max__c = 10,Req_Pay_Rate_Min__c = 1,
                                                                 Req_Standard_Burden__c = 10,Req_Rate_Frequency__c='Hourly',Req_Worksite_Postal_Code__c='28201',Req_Worksite_Country__c='United States');  
                      
                    
    
                        // Load opportunity data
                        lstOpportunity.add(NewOpportunity);
                }                
                // Insert list of Opportunity
                insert lstOpportunity;  
                
                string rectype='Talent';
                 List<Contact> TalentcontLst=tdcont.createContacts(rectype);
                
                string OrderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
                
                 Order NewOrder = new Order(Name = 'New Order', EffectiveDate = system.today()+1,
                                                Status = 'Draft',AccountId = lstOpportunity[0].AccountId, OpportunityId = lstOpportunity[0].Id,shiptocontactid=TalentcontLst[0].id,recordtypeid = OrderRecordType);
                insert NewOrder;
                
                Test.StartTest();

                  UpdateLegacyOrderBatch legacyOrderBatch = new UpdateLegacyOrderBatch();
                  ID batchprocessid =  Database.executeBatch(legacyOrderBatch);
                   
                Test.StopTest();   
                
        }
       } 
 
 }
}