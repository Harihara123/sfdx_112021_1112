@isTest(seealldata = false)
public class Test_UpdateTalentLastModifiedTriggers {

    @isTest static void setLinkedInMock(){
    LinkedInAuthHandlerForAts tHandler = new LinkedInAuthHandlerForAts(LinkedInEnvironment.DEV);
        LinkedInAuthHandlerForATS.AuthJSON mReturnJson = new LinkedInAuthHandlerForATS.AuthJSON();
        mReturnJson.access_token = 'lAQV_Ug1iRzPsUdUJ2yNjkFe2IoJFv1KwS0PEmH2F_OYeb9W7J3_cL-JIYpDj7-Fp9Xn9Wg042Vo0m4di2-B5wrDk6f357P2JQdAzyWCaj5t35RgZtKPB7rxGVvY8ZSkgDra';
        mReturnJson.expires_in = '1799';
        LinkedInIntegrationHttpMock tMock = new LinkedInIntegrationHttpMock(mReturnJson, 200);
        Test.setMock(HttpCalloutMock.class, tMock);
    
    
    }

    static testMethod void testEventAddEditAndDelete() {
        Test.startTest();
        Account account = CreateTalentTestData.createTalentAccount();
        setLinkedInMock();
        
        Contact contact = CreateTalentTestData.createTalentContact(account);
        List<Event> events = CreateTalentTestData.createTalentEvent(contact,account);
        Event event  = events[0];
        event.Pre_Meeting_Notes__c = 'Adding some test notes';
        update event;
        
        delete event;
        
        Test.stopTest();
    }

    static testMethod void testTaskAddEditAndDelete() {
        Test.startTest();
        Account account = CreateTalentTestData.createTalentAccount();
        setLinkedInMock();
        Contact contact = CreateTalentTestData.createTalentContact(account);
        List<Task> tasks = CreateTalentTestData.createTalentTask(contact);
        
        Task task = tasks[0];
        task.Pre_Meeting_Notes__c = 'Adding some test notes';
        update task;
        
        delete task;
        
        Test.stopTest();
    }

    static testMethod void testTalentExperienceAddEditAndDelete() {
        
        Account account = CreateTalentTestData.createTalentAccount();
        setLinkedInMock();
        Contact contact = CreateTalentTestData.createTalentContact(account);
        Talent_Experience__c education = CreateTalentTestData.createEducation(account);
        
        User differentContext = TestDataHelper.createUser('System Administrator');
        Test.startTest();
        
        System.runAs(differentContext) {
            TalentExperienceTriggerHandler.run = true;
            education.Notes__c = 'Adding some test notes';
            education.Current_Assignment__c = true;
            update education;
        }
        
        TalentExperienceTriggerHandler.run = true;
        delete education;
        
        Test.stopTest();
    }

    static testMethod void testRecommendationAddEditAndDelete() {
        Test.startTest();
        Account account = CreateTalentTestData.createTalentAccount();
        setLinkedInMock();
        
        Contact contact = CreateTalentTestData.createTalentContact(account);
        Talent_Recommendation__c recco = CreateTalentTestData.createTalentRecommendation(account);
        
        recco.Talent_Job_Duties__c = 'Adding some test duties';
        update recco;
        
        delete recco;
        
        Test.stopTest();
    }
}