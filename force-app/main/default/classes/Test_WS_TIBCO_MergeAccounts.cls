/*************************************************************************************************
Apex Class Name :  Test_WS_TIBCO_MergeAccounts
Version         : 1.0 
Created Date    : 
Function        : Test Method for Reqs Extension
                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   		Description
* ----------------------------------------------------------------------------                 
* ***					            					            Original Version
* Rajkumar 					04/06/2013				Fixing test method failure issues
**************************************************************************************************/ 
@isTest(seeAlldata=true)
private class Test_WS_TIBCO_MergeAccounts 
{

    static testMethod void Test_WS_doMergeAccounts()
    {
        //List<Account> accntList = [Select id,name,Siebel_ID__c from Account limit 2 ];
        
        Account SF_Win = TestData.newAccount(0); 
        SF_Win.No_of_Reqs__c = 4; 
        
        //SF_Win.Siebel_ID__c = accntList[0].Siebel_ID__c ;
        
        Account SF_Loose = TestData.newAccount(0);  
        SF_Loose.No_of_Reqs__c = 6;
       
       // SF_Loose .Siebel_ID__c = accntList[1].Siebel_ID__c;//[Dhruv 11/20/2012]
        
        Insert SF_Win;
        Insert SF_Loose;
        
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win.id,SF_Loose.id,null,null);
        SF_Win.Siebel_Id__c = string.valueof('Test12345');
        update SF_Win;
        
        
        
        Account SF_Loose_Child = TestData.newAccount(0);
        SF_Loose_Child.ParentId = SF_Win.id;
        SF_Loose_Child.Siebel_Id__c = string.valueof('Test111');
        
        
        Insert SF_Loose_Child; 
        
        
        
        
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win.id,SF_Loose.id,SF_Win.Siebel_Id__c,SF_Win.Siebel_Id__c);
       
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win.id,SF_Loose.id,SF_Loose_Child.Siebel_Id__c,SF_Loose_Child.Siebel_Id__c);
        //WS_TIBCO_MergeAccounts.WS_doMergeAccounts( null,null,null,null);
        
        
        Account SF_Win2 = TestData.newAccount(0);
        SF_Win2.Siebel_ID__c = string.valueof('Test123');
        Insert SF_Win2;
        
        Account SF_Loose2 = TestData.newAccount(0);
        SF_Loose2.Siebel_ID__c = string.valueof('Test234');
        Insert SF_Loose2;
        
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win2.id,SF_Loose2.id,string.valueof('Test123'),string.valueof('Test234'));
        
        Account SF_Win3 = TestData.newAccount(0);
        SF_Win2.Siebel_ID__c = null;
        Insert SF_Win3;
        
        Account SF_Loose3 = TestData.newAccount(0);
        SF_Loose2.Siebel_ID__c = string.valueof('Test236');
        Insert SF_Loose3;
        
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win3.id,SF_Loose3.id,string.valueof('Test123'),string.valueof('Test234'));
        
        Account SF_Win4 = TestData.newAccount(0);
        SF_Win2.Siebel_ID__c = null;
        Insert SF_Win4;
        
        Account SF_Loose4 = TestData.newAccount(0);
        SF_Loose2.Siebel_ID__c = string.valueof('Test235');
        Insert SF_Loose4;
        
        WS_TIBCO_MergeAccounts.WS_doMergeAccounts( SF_Win4.id,SF_Loose4.id,string.valueof('Test123'),string.valueof('Test234'));
    }

}