/******************************************************************************************************************************
* Name        - Test_WS_User 
* Description - Test method for User WSDL class. 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Dhruv                      03/28/2012             Created
  Nidish                     06/18/2015             updated line 20, 217, 465, 562, 638, 662 to insert Account
********************************************************************************************************************************/
@isTest(seeAlldata= TRUE)
private class Test_WS_User {
    
  
    static User user = TestDataHelper.createUser('System Administrator');
    
    static testMethod void Ws_UserCreationTest() {
    long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
    Integer random = Math.Round(Math.Random() * timeDiff );
    Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A;
    Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
    
    // @mikang
    ConTest.Recruiter_Peoplesoft_Id__c = String.valueof(random).Substring(0,5);
    //ConTest.Recruiter_Peoplesoft_Id__c = String.valueof(random).Substring(0,5);
    
   Integration_App_Role__c  newCustomSetting = new Integration_App_Role__c();
   newCustomSetting.Name ='Recruiter1';
   newCustomSetting.Create_Contact_Record__c = true;
   newCustomSetting.Autoactive_User__c = false;
   newCustomSetting.Contact_Record_Type__c = 'Recruiter1';
   
   insert newCustomSetting;
    
    System.debug('ConTest.Recruiter_Peoplesoft_Id__c'+ConTest.Recruiter_Peoplesoft_Id__c);
        
        if(user != Null) 
        {
            system.runAs(user) {
                WS_User usrCreation = new WS_User();
                try
                {
                
                    UserRole usrRoleTest = [Select Name from UserRole LIMIT 1];
                    WS_User.mapUsrRoleInfo.put('2222222',usrRoleTest);
                }
                catch(exception e)
                {
                    
                    UserRole tstUsrRole = new UserRole();
                    tstUsrRole.Name = 'TestUserRole';
                    insert tstUsrRole;
                    WS_User.mapUsrRoleInfo.put('2222222',tstUsrRole);  
                }
                
                List<WS_User.UserInformation> usrInformationList = new List<WS_User.UserInformation>();
                List<WS_User.UserInformation> TstRoleList = new List<WS_User.UserInformation>();
                List<WS_User.UserRoleInformation> usrRoleInformationList = new  List<WS_User.UserRoleInformation>();
                List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
                Test.startTest(); 
                for(integer i=0; i<100;i++) {
                    WS_User.UserRoleInformation usrRoleInformation = new WS_User.UserRoleInformation(); 
                    usrRoleInformation.OperatingCompanyName = 'Set'+i;
                    usrRoleInformation.RegionName = 'Los Angeles,CA'+i;
                    usrRoleInformation.OperatingCompanyCode = '00000023'+i;
                    usrRoleInformation.RegionCode = '000234'+i;
                    usrRoleInformation.OfficeCode = '0000862'+i; 
                    usrRoleInformationList.add(usrRoleInformation);         
                }     
                WS_User.upsertUserOrganization(usrRoleInformationList); 
              //  Test.stopTest();      
               //system.debug('Print the value of passing list' + usrRoleInformationList);
                
               
                
                WS_User.UserInformation usrRoleInformationTest =  new WS_User.UserInformation();
                usrRoleInformationTest.lastName = 'tested'; 
                usrRoleInformationTest.OperatingCompanyCode = 'TEK';
                
                usrRoleInformationTest.OfficeCode = '00560';
                usrRoleInformationTest.FirstName = 'Tester';
                usrRoleInformationTest.PeopleSoftId = '7668766';
                usrRoleInformationTest.SuperVisorId = '1341321';
                usrRoleInformationTest.OperatingCompanyName = 'yihjg';
                usrRoleInformationTest.LevelDescr = 'TOP';
                usrRoleInformationTest.ActiveStatus = 'A';
                TstRoleList.add(usrRoleInformationTest);
                WS_User.upsertUserRole(TstRoleList );
                
                
                WS_User.UserInformation usrRoleInformationTest1 =  new WS_User.UserInformation();
                usrRoleInformationTest1.lastName = 'tested'; 
                usrRoleInformationTest1.OperatingCompanyCode = 'TEK';
                
                usrRoleInformationTest1.OfficeCode = '00560';
                usrRoleInformationTest1.FirstName = 'Tester';
                usrRoleInformationTest1.PeopleSoftId = '7668766';
                usrRoleInformationTest1.SuperVisorId = '1341321';
                usrRoleInformationTest1.OperatingCompanyName = 'yihjg';
                usrRoleInformationTest1.LevelDescr = 'LEAF';
                usrRoleInformationTest1.Level = '4';
                usrRoleInformationTest1.ActiveStatus = 'I';
                TstRoleList.add(usrRoleInformationTest1);
                WS_User.upsertUserRole(TstRoleList );
                
                
                
                WS_User.UserInformation usrRoleInformationTest2 =  new WS_User.UserInformation();
                usrRoleInformationTest2.lastName = 'tested'; 
                usrRoleInformationTest2.OperatingCompanyCode = 'TEST';
                
                usrRoleInformationTest2.OfficeCode = '00560';
                usrRoleInformationTest2.FirstName = 'Tester';
                usrRoleInformationTest2.PeopleSoftId = '11110111';
                usrRoleInformationTest2.SuperVisorId = '22202222';
                usrRoleInformationTest2.OperatingCompanyName = 'yihjg';
                usrRoleInformationTest2.LevelDescr = '';
                usrRoleInformationTest2.Level = '4';
                usrRoleInformationTest2.ActiveStatus = 'I';
                TstRoleList.add(usrRoleInformationTest2);
               // WS_User.upsertUserRole(TstRoleList );
                Test.stopTest();
                
                
               
                
                
                
                
                WS_User.UserInformation usrInformationUpdate = new WS_User.UserInformation();
                usrInformationUpdate.EmailAddress = 'testeduser@allegistest.com';
                usrInformationUpdate.PeoplesoftId = user.PeopleSoft_Id__c;
                usrInformationUpdate.LastModifiedDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformationList.add(usrInformationUpdate);  
                //WS_User.upsertUser(usrInformationList);   
                

                for(integer i=0; i<100;i++) {
                    WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                    usrInformation.lastName = 'tested'+i;               
                    usrInformation.OperatingCompanyCode = 'TEK';
                    usrInformation.OfficeCode = '00560'+i;
                    usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
                    usrInformation.PeoplesoftId = '0233221'+i;
                    usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                    usrInformation.AppRoleName = 'REC';
                    UsrContactInformationList.add(usrInformation);              
                }
                WS_User.upsertContact(UsrContactInformationList);
                
                
                WS_User.UserInformation conatactUpdate = new WS_User.UserInformation();
                conatactUpdate.lastName = 'tested';    
                
                // @mikang
                conatactUpdate.PeoplesoftId = String.Valueof(ConTest.Recruiter_Peoplesoft_Id__c);                                      
                //conatactUpdate.PeoplesoftId = ConTest.Recruiter_Peoplesoft_Id__c;
                
                System.debug('------'+ConTest.Recruiter_Peoplesoft_Id__c);
                UsrContactInformationList.add(conatactUpdate);
                WS_User.upsertContact(UsrContactInformationList);
                 
                
                try {
                    for(integer i=0; i<201;i++) {
                        WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                        usrInformation.lastName = 'tested';             
                        usrInformation.OperatingCompanyCode = 'TEK';
                        usrInformation.OfficeCode = '00560';
                        usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
                        usrInformation.PeoplesoftId = '02332213';
                        usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                        usrInformation.AppRoleName = 'Recruiter';
                        UsrContactInformationList.add(usrInformation);              
                    }
                WS_User.upsertContact(UsrContactInformationList);
                } catch(exception e) {
                
                system.debug('*******exception************'+e);
                     
                }
            }
        }                    
         
    }
    //test method to create Users
    testmethod static void validateUsers()
    {
        Test.startTest();
        List<WS_User.UserInformation> usrInformationList = new List<WS_User.UserInformation>();
        for(integer i=0; i<100;i++) 
        {
                WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                usrInformation.lastName = 'tested'+i;               
                usrInformation.OperatingCompanyCode = 'TEK';
                usrInformation.OfficeCode = '00560'+i;
                usrInformation.EmailAddress = 'testeduser'+i+'@allegistest.com';
                usrInformation.PeoplesoftId = '02332213'+i;
                usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformation.AppRole = 'AM';
                usrInformation.TerminationDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformation.LastModifiedDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformationList.add(usrInformation);             
        }  
        //commented by hareesh due to long running
        //WS_User.upsertUser(usrInformationList);
        Test.stopTest();
    }
    
    
    
    //test method to test 
    testmethod static void runTest()
    {
        
        system.runAs(user) {
        
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        a.ShippingStreet='xyz';
        a.ShippingCity='xyz';
        a.ShippingCountry='USA';
        a.ShippingPostalCode='08811';
        insert A;
        Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        Integration_App_Role__c  newCustomSetting = new Integration_App_Role__c();
        newCustomSetting.Name ='CSD3';
        newCustomSetting.Create_Contact_Record__c = true;
        newCustomSetting.Autoactive_User__c = true;
        newCustomSetting.Marketing_User__c = true;
        newCustomSetting.Contact_Record_Type__c = 'CSD';
        insert newCustomSetting;
        Integration_App_Role__c  newCustomSetting1 = new Integration_App_Role__c();
        newCustomSetting1.Name ='test';
        newCustomSetting1.Create_Contact_Record__c =false;
        newCustomSetting1.Autoactive_User__c = false;
        newCustomSetting1.Marketing_User__c = false;
        newCustomSetting1.Contact_Record_Type__c = 'test';
        insert newCustomSetting1;
       
        if(user != Null) {
            
                WS_User usrCreation = new WS_User();
                try
                {
                
                    UserRole usrRoleTest = [Select Name from UserRole LIMIT 1];
                    WS_User.mapUsrRoleInfo.put('2222222',usrRoleTest);
                }
                catch(exception e)
                {
                    
                    UserRole tstUsrRole = new UserRole();
                    tstUsrRole.Name = 'TestUserRole';
                    insert tstUsrRole;
                    WS_User.mapUsrRoleInfo.put('2222222',tstUsrRole);  
                }
                
                List<WS_User.UserInformation> usrInformationList = new List<WS_User.UserInformation>();
                List<WS_User.UserInformation> TstRoleList = new List<WS_User.UserInformation>();
                List<WS_User.UserRoleInformation> usrRoleInformationList = new  List<WS_User.UserRoleInformation>();
                List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
                Test.startTest(); 
                for(integer i=0; i<5000;i++) {
                    WS_User.UserRoleInformation usrRoleInformation = new WS_User.UserRoleInformation(); 
                    usrRoleInformation.OperatingCompanyName = 'Set'+i;
                    usrRoleInformation.RegionName = 'Los Angeles,CA'+i;
                    usrRoleInformation.OperatingCompanyCode = '00000023'+i;
                    usrRoleInformation.RegionCode = '000234'+i;
                    usrRoleInformation.OfficeCode = '0000862'+i; 
                    usrRoleInformationList.add(usrRoleInformation);         
                }     
                try{
                    WS_User.upsertUserOrganization(usrRoleInformationList); 
                    
                }catch(Exception e){
                    System.debug('Cehck Exception '+e);
                }
              //system.debug('Print the value of passing list' + usrRoleInformationList);
                
               
                
               
                for(integer i=0; i<100;i++) {
                    WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                    usrInformation.lastName = 'tested'+i;               
                    usrInformation.OperatingCompanyCode = 'TEK';
                    usrInformation.OfficeCode = '00560'+i;
                    usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
                    usrInformation.PeoplesoftId = '0233221'+i;
                    usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                    usrInformation.AppRoleName = 'REC';
                    UsrContactInformationList.add(usrInformation);              
                }
                WS_User.upsertContact(UsrContactInformationList);
          
                WS_User.UserInformation conatactUpdate = new WS_User.UserInformation();
                
                conatactUpdate.lastName = 'tested';    
                
                // @mikang
                conatactUpdate.PeoplesoftId = String.Valueof(ConTest.Recruiter_Peoplesoft_Id__c);                                      
                //conatactUpdate.PeoplesoftId = ConTest.Recruiter_Peoplesoft_Id__c;
                
                System.debug('------'+ConTest.Recruiter_Peoplesoft_Id__c);
                UsrContactInformationList.add(conatactUpdate);
            //    WS_User.upsertContact(UsrContactInformationList);
                
                
                try {
                    for(integer i=0; i<201;i++) {
                        WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                        usrInformation.lastName = 'tested';             
                        usrInformation.OperatingCompanyCode = 'TEK';
                        usrInformation.OfficeCode = '00560';
                        usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
                        usrInformation.PeoplesoftId = '023323';
                        usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                        usrInformation.AppRoleName = 'Recruiter';
                        UsrContactInformationList.add(usrInformation);              
                    }
             //   WS_User.upsertContact(UsrContactInformationList);
                } catch(exception e) {
                
                system.debug('*******exception************'+e);
                     
                }
            }
                            
         
       
        WS_User.getUserActiveStatus('CSD');
        WS_User.getMarketingUserStatus('CSD');
        WS_User.getAppRoleStatus('test');
        WS_User.getMarketingUserStatus('test');
        WS_User.getUserActiveStatus('test');
        Test.stopTest();
       
        }     
            
    }
    
    //test method to create Users and to cover remaining code of lines
    static testMethod void Ws_UserCreationTst() {
        
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        Account A1 = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A1;
        Contact ConTest1 = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A1.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        
        // @mikang
        ConTest1.Recruiter_Peoplesoft_Id__c = '0358';
        insert ConTest1;
        //ConTest.Recruiter_Peoplesoft_Id__c = String.valueof(random).Substring(0,5);
        
       Integration_App_Role__c  newCustomSetting = new Integration_App_Role__c();
       newCustomSetting.Name ='Recruiter0';
       newCustomSetting.Create_Contact_Record__c = true;
       newCustomSetting.Autoactive_User__c = false;
       newCustomSetting.Contact_Record_Type__c = 'Recruiter';
       
       insert newCustomSetting;
       
       
        
       System.debug('ConTest1.Recruiter_Peoplesoft_Id__c'+ConTest1.Recruiter_Peoplesoft_Id__c);
            
        if(user != Null) 
        {
            system.runAs(user) {
                WS_User usrCreation = new WS_User();
                try
                {
                
                    UserRole usrRoleTest = [Select Name from UserRole LIMIT 1];
                    WS_User.mapUsrRoleInfo.put('2222222',usrRoleTest);
                    WS_User.mapUsrRoleInfo.put('1341321',usrRoleTest);
                }
                catch(exception e)
                {
                    
                    UserRole tstUsrRole = new UserRole();
                    tstUsrRole.Name = 'TestUserRole';
                    insert tstUsrRole;
                    WS_User.mapUsrRoleInfo.put('2222222',tstUsrRole);  
                    WS_User.mapUsrRoleInfo.put('1341321',tstUsrRole);  
                }
                
                List<WS_User.UserInformation> usrInformationList = new List<WS_User.UserInformation>();
                List<WS_User.UserInformation> TstRoleList = new List<WS_User.UserInformation>();
                List<WS_User.UserRoleInformation> usrRoleInformationList = new  List<WS_User.UserRoleInformation>();
                List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
                Test.startTest(); 
                for(integer i=0; i<100;i++) {
                    WS_User.UserRoleInformation usrRoleInformation = new WS_User.UserRoleInformation(); 
                    usrRoleInformation.OperatingCompanyName = 'Set';//+i
                    usrRoleInformation.RegionName = 'Los Angeles,CA'; //+i
                    usrRoleInformation.OperatingCompanyCode ='00000023' ;//+i
                    usrRoleInformation.RegionCode = '000234';//+i
                    usrRoleInformation.OfficeCode = '0000862'; //+i
                    usrRoleInformation.OfficeActiveStatus = 'A';
                    usrRoleInformationList.add(usrRoleInformation);         
                }     
                WS_User.upsertUserOrganization(usrRoleInformationList); 
                     
               //system.debug('Print the value of passing list' + usrRoleInformationList);
                
               
                
                WS_User.UserInformation usrRoleInformationTest =  new WS_User.UserInformation();
                usrRoleInformationTest.lastName = 'tested'; 
                usrRoleInformationTest.OperatingCompanyCode = 'TEK';
                
                usrRoleInformationTest.OfficeCode = '00560';
                usrRoleInformationTest.FirstName = 'Tester';
                usrRoleInformationTest.PeopleSoftId = '03585203';
                usrRoleInformationTest.SuperVisorId = '1341321';
                usrRoleInformationTest.OperatingCompanyName = 'yihjg';
                usrRoleInformationTest.LevelDescr = 'MID';
                usrRoleInformationTest.ActiveStatus = 'A';
                usrRoleInformationTest.AppRoleName = 'CSD';
                TstRoleList.add(usrRoleInformationTest);
                WS_User.upsertUserRole(TstRoleList );
               Test.stopTest();  
               
                WS_User.UserInformation usrInformationUpdate = new WS_User.UserInformation();
                usrInformationUpdate.EmailAddress = 'testeduser@allegistest.com';
                usrInformationUpdate.PeoplesoftId = user.PeopleSoft_Id__c;
                usrInformationUpdate.LastModifiedDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformationList.add(usrInformationUpdate); 
                //commented by Hareesh due to long running deployments
                //WS_User.upsertUser(usrInformationList);   
                
                WS_User.UserInformation usrInformation = new WS_User.UserInformation();
                usrInformation.lastName = 'tested';               
                usrInformation.OperatingCompanyCode = 'TEK';
                usrInformation.OfficeCode = '00560';
                usrInformation.EmailAddress = 'testcontuser@allegistest.com';
                usrInformation.PeoplesoftId = '035852030';
                usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
                usrInformation.AppRoleName = 'REC';
                UsrContactInformationList.add(usrInformation);              
               
                WS_User.upsertContact(UsrContactInformationList);
                
                
                WS_User.UserInformation conatactUpdate = new WS_User.UserInformation();
                conatactUpdate.lastName = 'tested';    
                
                // @mikang
                conatactUpdate.PeoplesoftId = String.Valueof(ConTest1.Recruiter_Peoplesoft_Id__c);                                      
                //conatactUpdate.PeoplesoftId = ConTest.Recruiter_Peoplesoft_Id__c;
                
                System.debug('------'+ConTest1.Recruiter_Peoplesoft_Id__c);
                UsrContactInformationList.add(conatactUpdate);
                WS_User.upsertContact(UsrContactInformationList);
                
                
              
              //  Test.stopTest(); 
                
              
            }
        }                    
         
    
    }
    
      static testMethod void Ws_UserCreationTst2() {
        
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A;
        Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        List<WS_User.UserInformation> TstRoleList = new List<WS_User.UserInformation>();
        // @mikang
        ConTest.Recruiter_Peoplesoft_Id__c = '99190';
        insert ConTest;
        //ConTest.Recruiter_Peoplesoft_Id__c = String.valueof(random).Substring(0,5);
        Integration_App_Role__c  newCustomSetting1 = new Integration_App_Role__c();
        newCustomSetting1.Name ='CSD1';
        newCustomSetting1.Create_Contact_Record__c = false;
        newCustomSetting1.Autoactive_User__c = false;
        newCustomSetting1.Contact_Record_Type__c = 'CSD';
        insert newCustomSetting1;
        
        try{
            UserRole usrRoleTest = [Select Name from UserRole LIMIT 1];
            WS_User.mapUsrRoleInfo.put('2222222',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('1341321',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('03585209',usrRoleTest);
         }catch(exception e){
            UserRole tstUsrRole = new UserRole();
            tstUsrRole.Name = 'TestUserRole';
            insert tstUsrRole;
            WS_User.mapUsrRoleInfo.put('2222222',tstUsrRole);  
            WS_User.mapUsrRoleInfo.put('1341321',tstUsrRole);  
            WS_User.mapUsrRoleInfo.put('03585209',tstUsrRole); 
         }
        
        WS_User.UserInformation usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK';
        usrRoleInformationTest.OfficeCode = '00560';
        usrRoleInformationTest.FirstName = 'Tester';
        usrRoleInformationTest.PeopleSoftId = '03585203';
        usrRoleInformationTest.SuperVisorId = '1341321';
        usrRoleInformationTest.OperatingCompanyName = 'yihjg';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'CSD';
        TstRoleList.add(usrRoleInformationTest);

        usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested2'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK';
        usrRoleInformationTest.OfficeCode = '00560';
        usrRoleInformationTest.FirstName = 'Tester';
        usrRoleInformationTest.PeopleSoftId = '03585209';
        usrRoleInformationTest.SuperVisorId = '1341321';
        usrRoleInformationTest.OperatingCompanyName = 'yihjg';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'CSD';
        TstRoleList.add(usrRoleInformationTest);

        usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested3'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK';
        usrRoleInformationTest.OfficeCode = '00560';
        usrRoleInformationTest.FirstName = 'Tester';
        usrRoleInformationTest.PeopleSoftId = '03585209';
        usrRoleInformationTest.SuperVisorId = '1341321';
        usrRoleInformationTest.OperatingCompanyName = 'yihjg';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'CSR';
        TstRoleList.add(usrRoleInformationTest);

        usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested3'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK';
        usrRoleInformationTest.OfficeCode = '00560';
        usrRoleInformationTest.FirstName = 'Tester';
        usrRoleInformationTest.PeopleSoftId = '03585219';
        usrRoleInformationTest.SuperVisorId = '1343321';
        usrRoleInformationTest.OperatingCompanyName = 'yihjg';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'CSD';
        TstRoleList.add(usrRoleInformationTest);


        test.startTest();
        WS_User.mapLevelRoleRecord.put(2,TstRoleList);
        try{
            WS_User.insertRoleLists(2,5);
        }catch(Exception e){
            System.debug('Exception cehck**'+e);
        }
        test.stopTest();
        
        
      }
   static testMethod void Ws_UserCreationTst3() {
        
        long timeDiff = system.now().getTime() - (system.now() - 1).getTime();
        Integer random = Math.Round(Math.Random() * timeDiff );
        Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A;
        Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        List<WS_User.UserInformation> TstRoleList = new List<WS_User.UserInformation>();
        // @mikang
        ConTest.Recruiter_Peoplesoft_Id__c = '89823';
        insert ConTest;
        //ConTest.Recruiter_Peoplesoft_Id__c = String.valueof(random).Substring(0,5);
        Integration_App_Role__c  newCustomSetting1 = new Integration_App_Role__c();
        newCustomSetting1.Name ='CSD2';
        newCustomSetting1.Create_Contact_Record__c = false;
        newCustomSetting1.Autoactive_User__c = false;
        newCustomSetting1.Contact_Record_Type__c = 'CSD';
        insert newCustomSetting1;
        
        try{
            UserRole usrRoleTest = [Select Name from UserRole LIMIT 1];
            WS_User.mapUsrRoleInfo.put('2222222',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('1341321',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('1341322',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('1341323',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('03585203',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('1341324',usrRoleTest);
            WS_User.mapUsrRoleInfo.put('03585209',usrRoleTest);
         }catch(exception e){
            UserRole tstUsrRole = new UserRole();
            tstUsrRole.Name = 'TestUserRole';
            insert tstUsrRole;
            WS_User.mapUsrRoleInfo.put('2222222',tstUsrRole);  
            WS_User.mapUsrRoleInfo.put('1341321',tstUsrRole);  
            WS_User.mapUsrRoleInfo.put('1341322',tstUsrRole); 
            WS_User.mapUsrRoleInfo.put('1341323',tstUsrRole); 
            WS_User.mapUsrRoleInfo.put('03585203',tstUsrRole);
            WS_User.mapUsrRoleInfo.put('1341324',tstUsrRole); 
            WS_User.mapUsrRoleInfo.put('03585209',tstUsrRole); 
         }
        
        WS_User.UserInformation usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK4';
        usrRoleInformationTest.OfficeCode = '00560';
        usrRoleInformationTest.FirstName = 'Tester1';
        usrRoleInformationTest.PeopleSoftId = '03585203';
        usrRoleInformationTest.SuperVisorId = '1341324';
        usrRoleInformationTest.OperatingCompanyName = 'yihjgk';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'MSD';
        TstRoleList.add(usrRoleInformationTest);

        usrRoleInformationTest =  new WS_User.UserInformation();
        usrRoleInformationTest.lastName = 'tested2'; 
        usrRoleInformationTest.OperatingCompanyCode = 'TEK3';
        usrRoleInformationTest.OfficeCode = '00561';
        usrRoleInformationTest.FirstName = 'Tester2';
        usrRoleInformationTest.PeopleSoftId = '03585210';
        usrRoleInformationTest.SuperVisorId = '1341321';
        usrRoleInformationTest.OperatingCompanyName = 'yihjgh';
        usrRoleInformationTest.LevelDescr = 'MID';
        usrRoleInformationTest.ActiveStatus = 'A';
        usrRoleInformationTest.AppRoleName = 'MSD';
        TstRoleList.add(usrRoleInformationTest);

    

        test.startTest();
        WS_User.mapLevelRoleRecord.put(2,TstRoleList);
        WS_User.insertRoleLists(2,5);
        test.stopTest();
        
        
      }
      
      static testMethod void test_upsertContact() {
        List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
        Test.startTest(); 
        Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A;
        Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        ConTest.Recruiter_Peoplesoft_Id__c = '02331';
        insert ConTest;
        
        for(integer i=0; i<100;i++) {
            WS_User.UserInformation usrInformation = new WS_User.UserInformation();
            usrInformation.lastName = 'tested'+i;               
            usrInformation.OperatingCompanyCode = 'TEK';
            usrInformation.OfficeCode = '00560'+i;
            usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
            usrInformation.PeoplesoftId = '0233221'+i;
            usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
            usrInformation.AppRoleName = 'REC';
            usrInformation.ActiveStatus =  'A';
            UsrContactInformationList.add(usrInformation);              
         }
        WS_User.upsertContact(UsrContactInformationList);
        Test.stopTest();        
      }
     static testMethod void test_upsertContact2() {
       User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
         
         System.runAs ( thisUser ) {
       
        List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
        Test.startTest(); 
        Account A = new Account(Name = 'A1',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId());
               insert A;
        Contact ConTest = new Contact(LastName = 'Smith',FirstName = 'Bob',MobilePhone = '1111111111',Email='bsmith@test.com', AccountID= A.id,recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId());
        ConTest.Recruiter_Peoplesoft_Id__c = '0233221';
        insert ConTest;
        
        User userPeople = TestDataHelper.createUser('Aerotek AM');
        userPeople.Peoplesoft_Id__c = '0233221';
        userPeople.IsActive = true;
        insert userPeople;
        
        for(integer i=0; i<1;i++) {
            WS_User.UserInformation usrInformation = new WS_User.UserInformation();
            usrInformation.lastName = 'tested'+i;               
            usrInformation.OperatingCompanyCode = 'TEK';
            usrInformation.OfficeCode = '00560'+i;
            usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
            usrInformation.PeoplesoftId = '0233221';
            usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
            usrInformation.AppRoleName = 'REC';
            usrInformation.ActiveStatus =  'I';
            usrInformation.LastModifiedDate= System.now();
            UsrContactInformationList.add(usrInformation);              
         }
         //commented by Hareesh for deployment
        WS_User.upsertUser(UsrContactInformationList);
        Test.stopTest();        
         }
      }
    
    static testMethod void test_UpsertUser_Negative(){
        
        List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
        for(integer i=0; i<1;i++) {
            WS_User.UserInformation usrInformation = new WS_User.UserInformation();
            usrInformation.lastName = 'tested'+i;               
            usrInformation.OperatingCompanyCode = 'TEK';
            usrInformation.OfficeCode = '00560'+i;
            usrInformation.EmailAddress = 'testcontuser'+i+'@allegistest.com';
            usrInformation.PeoplesoftId = '0233221'+i;
            usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
            usrInformation.AppRoleName = 'REC';
            usrInformation.ActiveStatus =  'I';
            usrInformation.LastModifiedDate= System.now();
            UsrContactInformationList.add(usrInformation);              
        }
        WS_User.upsertUser(UsrContactInformationList);
    }
    
    static testMethod void test_UpsertUser_Positive(){
        
        List<WS_User.UserInformation> UsrContactInformationList = new List<WS_User.UserInformation>();
       
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs ( thisUser ) {
        test.starttest();
             User userPeople = TestDataHelper.createUser('Aerotek AM');
            userPeople.Peoplesoft_Id__c = '0239991';
            userPeople.IsActive = true;
            insert userPeople;
            test.stoptest();
        }
         	WS_User.UserInformation usrInformation = new WS_User.UserInformation();
            usrInformation.lastName = 'tested1234';               
            usrInformation.OperatingCompanyCode = 'TEK';
            usrInformation.OfficeCode = '005600';
            usrInformation.EmailAddress = 'testcontuser1290@allegistest.com';
            usrInformation.PeoplesoftId = '0239991';
            usrInformation.HireDate = datetime.newInstance(2008, 12, 1, 12, 30, 2);
            usrInformation.AppRoleName = 'REC';
            usrInformation.ActiveStatus =  'I';
            usrInformation.LastModifiedDate= System.now();
            UsrContactInformationList.add(usrInformation);              
        	WS_User.upsertUser(UsrContactInformationList);
            
            
    }
     static testMethod void updateUserManagerTest() 
    {
        User user = TestDataHelper.createUser('Aerotek AM');
        user.Supervisor_PS_Id__c = user.Peoplesoft_Id__c;
        User user1 = TestDataHelper.createUser('Aerotek AM');
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];         
        System.runAs(thisUser) 
        {       
            List<User> userList = new List<User>();
            userList.add(user);
            userList.add(user1);
            insert userList;
            Test.startTest();
            User retUser = [SELECT ID,Peoplesoft_Id__c FROM User 
                            WHERE Id =: user.Id];
            user1.Supervisor_PS_Id__c = retUser.Peoplesoft_Id__c;
            update user1;
            Test.stopTest();
        }
    }
    
    
    
    
}