/***************************************************************************
Name        : TimemachineActionEvent
Created By  : Ncains
Date        : 27 Feb 2018
Purpose     : Standard Class to help create Timemachine platform events
*****************************************************************************/
public without sharing class TimemachineActionEvent {
    
    public void  RaiseActionEvent(String Action, String What1Type, String What1Id, String What2Type, String What2Id, String What3Type, String What3Id, String What4Type, String What4Id) {
        RaiseActionEvent(Action,What1Type,What1Id,What2Type,What2Id,What3Type,What3Id,What4Type,What4Id,false,'','','','');
    }
    
    public void  RaiseActionEvent(String Action, String What1Type, String What1Id, String What2Type, String What2Id, String What3Type, String What3Id) {
        RaiseActionEvent(Action,What1Type,What1Id,What2Type,What2Id,What3Type,What3Id,'','',false,'','','','');
    }
    
    public void  RaiseActionEvent(String Action, String What1Type, String What1Id, String What2Type, String What2Id) {
        RaiseActionEvent(Action,What1Type,What1Id,What2Type,What2Id,'','','','',false,'','','','');
    }
    
    public void  RaiseActionEvent(String Action, String What1Type, String What1Id) {
        RaiseActionEvent(Action,What1Type,What1Id,'','','','','','',false,'','','','');
    }
    
    public void  RaiseActionEventWithData(String Action, String What1Type, String What1Id, String Data) {
        RaiseActionEventWithData( Action,  What1Type,  What1Id,  '', '',   '', '', '', '',  Data);
    }
    
    public void  RaiseActionEventWithData(String Action, String What1Type, String What1Id, String What2Type, String What2Id,  String Data) {
        RaiseActionEventWithData( Action,  What1Type,  What1Id,  What2Type,  What2Id,  '', '', '', '',  Data);
    }
    
    public void  RaiseActionEventWithData(String Action, String What1Type, String What1Id, String What2Type, String What2Id, String What3Type, String What3Id, String Data) {
        RaiseActionEventWithData( Action,  What1Type,  What1Id,  What2Type,  What2Id,  What3Type,  What3Id, '', '',  Data);
    }
    
    public void  RaiseActionEventWithData(String Action, String What1Type, String What1Id, String What2Type, String What2Id, String What3Type, String What3Id, String What4Type, String What4Id, String Data) {
        
        List <String> dataOut = new List <String>();
        Integer dataCounter = 1;
        Integer dataFieldLength = 131072; //Data Field Maximum Size
        Boolean Data_Truncated_Error = false;
        
        for (Integer i = 1; i*dataFieldLength <= Math.ceil(double.valueOf(Data.length())/double.valueOf(dataFieldLength)) * dataFieldLength; i++) {
            
            if (((i-1)*dataFieldLength) + dataFieldLength < Data.length()-1 ) {
                dataOut.add(Data.substring((i-1)*dataFieldLength,i*dataFieldLength));
            } else {
                dataOut.add(Data.substring((i-1)*dataFieldLength));
            }
        } 
        
        //Size Issue - need to raise error, but we still log what we can -> 
        //If a regular occurance adjust event to accomodate more Data 
        //fields, consumers would need to be updated accordingly - 4Fields is ~512k of data 
        
        if (dataOut.size() > 4) {     System.debug('RaiseActionEventWithData: Data Size more than 4 fields---');
                                 Data_Truncated_Error = true;
                                }
        
        String data1 = '';
        String data2 = '';
        String data3 = '';
        String data4 = '';
        
        Integer j = 0;
        while (j< dataOut.size() && j < 4){
            if (j==0) {
                data1 = dataOut[j];
            } else if (j ==1) {
                data2 = dataOut[j];
            }else if (j ==2) {
                data3 = dataOut[j];
            }else if (j ==3) {
                data4 = dataOut[j];
            }
            j=j+1;
        }
        
        RaiseActionEvent(Action,What1Type,What1Id,What2Type,What2Id,What3Type,What3Id,What4Type,What4Id,Data_Truncated_Error,data1,data2,data3,data4);
    }
    
    public void RaiseObjectChangeEvent(String Action, String What1Type, String What1Id, sObject obj) {
        try {
            List<Timemachine_Settings__c> mcs = Timemachine_Settings__c.getall().values();
            if (mcs != null) {
                if (mcs.size() > 0) {
                    if (mcs[0].TriggerEnabled__c) {
                        boolean cont = false;
                        if (UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration) {
                            if (mcs[0].TriggerEnabledBulk__c) {
                                cont = true;
                            }
                        } else {
                            cont = true;
                        }
                        if (cont) {
                            String data = JSON.serialize(obj);
                        	RaiseActionEventWithData(Action, What1Type, What1Id, data);
                        }
                    }
                }
            }
            
        } catch (Exception e){
            // Here to ensure failure doesn't affect trigger flows
            System.debug(e);
        }
    }
    
    private void  RaiseActionEvent(String Action, String What1Type, String What1Id, String What2Type, String What2Id, String What3Type, String What3Id, String What4Type, String What4Id, Boolean Data_Truncated, String Data1, String Data2, String Data3, String Data4) {
        
        Timemachine_Action__e actionEvent = new Timemachine_Action__e(  Action__c = Action, 
                                                                      Action_By__c = UserInfo.getUserId() ,
                                                                      Session_ID__c = UserInfo.getSessionId() ,
                                                                      Environment_ID__c = UserInfo.getOrganizationId(), 
                                                                      What_1_Type__c = What1Type, What_1_ID__c = What1Id,
                                                                      What_2_Type__c = What2Type, What_2_ID__c = What2Id,
                                                                      What_3_Type__c = What3Type, What_3_ID__c = What3Id,
                                                                      What_4_Type__c = What4Type, What_4_ID__c = What4Id,
                                                                      Data_Truncated_Error__c = Data_Truncated,
                                                                      Data_1__c = Data1,
                                                                      Data_2__c = Data2,
                                                                      Data_3__c = Data3,
                                                                      Data_4__c = Data4);
        
        
        // Call method to publish events
        Database.SaveResult result = EventBus.publish(actionEvent);
        
        // Inspect publishing result for each event
        if (result.isSuccess()) {
            System.debug('Successfully published action event.');
        } else {
            for(Database.Error err : result.getErrors()) {
                System.debug('Error returned: ' +
                             err.getStatusCode() +
                             ' - ' +
                             err.getMessage());
            }
        }       
        
        
    }
}