@isTest
public class TimemachineActionEvent_Test {
    static testMethod void validateRaiseObjectChangeEvent(){
        Account a = new Account();
        a.BillingStreet = '123';
        a.BillingCity = 'Catonsville';
        a.BillingCountry = 'USA';
        a.BillingState ='MD';
        a.BillingPostalCode = '21228';
        a.Name = 'Test Account';
        
        Insert(a);
        
        TimemachineActionEvent b = new TimemachineActionEvent();
        b.RaiseObjectChangeEvent('Insert', 'Account', a.Id, a);
       
        // No current internal consumption of events
       System.assertEquals(1, 1);
    }
     static testMethod void validateRaiseActionEvent1() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 1 what parameter - will fail as type longer than 30 characters');

       // raise event
       b.RaiseActionEvent('test event','Test1234567890123456789012345678901234567890','123');
       
       System.debug('Event Fired');
       
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }

    static testMethod void validateRaiseActionEvent2() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 1 what parameter');

       // raise event
       b.RaiseActionEvent('test event','Test','123');
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }

         static testMethod void validateRaiseActionEvent3() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 3 what parameter');

       // raise event
       b.RaiseActionEvent('test event','Test','123','Test','123','Test','123');
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }
             static testMethod void validateRaiseActionEvent4() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 4 what parameter');

       // raise event
       b.RaiseActionEvent('test event','Test','123','Test','123','Test','123' ,'Test','123' );
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }

    static testMethod void validateRaiseActionEvent5() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 2 what parameter');

       // raise event
       b.RaiseActionEvent('test event','Test','123','Test','123');
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }



        static testMethod void validateRaiseActionEventWithData1() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event without data 1 what parameter and Data');

       String num1 = '1';     
            
       // raise event
       b.RaiseActionEventWithData('test event','Test','123','Test','123',num1.repeat(131072));
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }

            static testMethod void validateRaiseActionEventWithData2() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event with data 1- 4 what parameter and Really Long Data - should not truncate');

       // raise event
       // 		
       // 
       // 
       			String num1 = '1';
                string num2 = '2';
                String num3 = '3';
                String num4 = '4';                
                
                //String reallyLongString = generateString('1',131072)+ generateString('2',131072) + generateString('3',131072)+ generateString('4',131072); 
                
                String reallyLongString = num1.repeat(131072)+num2.repeat(131072)+num3.repeat(131072)+num4.repeat(131072);
                System.debug('Really Long String Length: ' + reallyLongString.length());
                
                
                
                
                b.RaiseActionEventWithData('test event','Test','123','Test','123',reallyLongString);
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }
    
                static testMethod void validateRaiseActionEventWithData3() {
       TimemachineActionEvent b = new TimemachineActionEvent();
       System.debug('Insert Event with data 1- 4 what parameter and Really Long Data - should truncate');

       // raise event
       // 		
       // 
       // 
       			String num1 = '1';
                string num2 = '2';
                String num3 = '3';
                String num4 = '4';                
                
                //String reallyLongString = generateString('1',131072)+ generateString('2',131072) + generateString('3',131072)+ generateString('4',131072); 
                
                String reallyLongString = num1.repeat(131072)+num2.repeat(131072)+num3.repeat(131072)+'ARMADILLO'+num4.repeat(131072);
                System.debug('Really Long String Length: ' + reallyLongString.length());
                
                
                
                
                b.RaiseActionEventWithData('test event','Test','123','Test','123',reallyLongString);
       
       System.debug('Event Fired');
       
           
                     

       // No current internal consumption of events
       System.assertEquals(1, 1);
    }

    
        @isTest static void testValidEvent() {
        
        // Create a test event instance
        Timemachine_Action__e actionEvent = new Timemachine_Action__e(  Action__c = 'test event', 
                                                                Action_By__c = 'TestUser123' ,
                                                                Session_ID__c = 'TestSession123' ,
                                                                Environment_ID__c = 'TestEnvironment123', 
                                                                What_1_Type__c = 'TestType', What_1_ID__c = 'TestId');
        
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(actionEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());

    }
    
    @isTest static void testInvalidEvent() {
        
        // Create a test event instance with invalid data.
        // We assume for this test that the Action__c field is required.
        // Publishing with a missing required field should fail.
        // Create a test event instance
         Timemachine_Action__e actionEvent = new Timemachine_Action__e(   
                                                                Action_By__c = 'TestUser123' ,
                                                                Session_ID__c = 'TestSession123' ,
                                                                Environment_ID__c = 'TestEnvironment123', 
                                                                What_1_Type__c = 'TestType', What_1_ID__c = 'TestId');
         
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(actionEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value - isSuccess should be false
        System.assertEquals(false, sr.isSuccess());
        
        // Log the error message
        for(Database.Error err : sr.getErrors()) {
            System.debug('Error returned: ' +
                        err.getStatusCode() +
                        ' - ' +
                        err.getMessage()+' - '+err.getFields());
        }

    }
}