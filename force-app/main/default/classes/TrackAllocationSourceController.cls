public with sharing class TrackAllocationSourceController {

    /**
     * Takes a list of OpportunityTeamMember records and inserts into Req_Allocation_Track__c
     * IF the team memeber role = 'Allocated Recruiter'
     */
    public static void trackAllocatedRecruiters(List<OpportunityTeamMember> OTMs) {
        try {
            List<OpportunityTeamMember> validOtms = new List<OpportunityTeamMember>();
            
            for (OpportunityTeamMember otm : OTMs) {
                if (otm.TeamMemberRole == 'Allocated Recruiter') {
                    validOtms.add(otm);
                }
            }

            if (validOtms.size() > 0) {
                List<Req_Allocation_Track__c> allocatedTrackers = new List<Req_Allocation_Track__c>();
                for(OpportunityTeamMember otm : validOtms) {
                    Req_Allocation_Track__c allocatedTracker = new Req_Allocation_Track__c();
                    allocatedTracker.Allocated_Recruiter__c = otm.UserId;
                    allocatedTracker.Allocation_Timestamp__c = otm.CreatedDate;
                    allocatedTracker.Opportunity__c = otm.OpportunityId;
                    allocatedTracker.Recruiter_Allocation_Source__c = !String.isBlank(otm.Source__c) ? otm.Source__c : 'Opportunity';
					allocatedTracker.Recommendation_Id__c = !String.isBlank(otm.Recommendation_Id__c) ? otm.Recommendation_Id__c : '';
                    allocatedTrackers.add(allocatedTracker);
                }

                // Insert items
                if (!allocatedTrackers.isEmpty())
                    insert allocatedTrackers;
            }
        }
        catch (Exception ex) {
            ConnectedLog.LogException('TrackAllocationSourceController', 'trackAllocatedRecruiters', ex);
        }
    }

    /**
     * Takes a list of Support_Request__c records and inserts into Req_Allocation_Track__c
     * Queries the data so we can story the display value of Alternate_Delivery_Office__c
     */
    public static void trackSupportRequests(List<Support_Request__c> supportRequests) {
        try {
            List<Id> idsToQuery = new List<Id>();
            for (Support_Request__c sr : supportRequests) {
                idsToQuery.add(sr.Id);
            }

            List<Support_Request__c> supportRequestsWithLabels = [SELECT Id, Opportunity__c, toLabel(Alternate_Delivery_Office__c), Source__c
                                                                 FROM Support_Request__c
                                                                 WHERE Id IN :idsToQuery];

            if (!supportRequestsWithLabels.isEmpty()) {
                List<Req_Allocation_Track__c> allocatedTrackers = new List<Req_Allocation_Track__c>();
                for (Support_Request__c sr : supportRequestsWithLabels) {
                    Req_Allocation_Track__c allocatedTracker = new Req_Allocation_Track__c();
                    allocatedTracker.Opportunity__c = sr.Opportunity__c;
                    allocatedTracker.Support_Request__c = sr.Id;
                    allocatedTracker.Support_Request_Delivery_Center__c = sr.Alternate_Delivery_Office__c;
                    allocatedTracker.Support_Request_Source__c = !String.isBlank(sr.Source__c) ? sr.Source__c : 'Opportunity';
                    allocatedTrackers.add(allocatedTracker);
                }

                // Insert items
                if (!allocatedTrackers.isEmpty())
                    insert allocatedTrackers;
            }
        }
        catch (Exception ex) {
            ConnectedLog.LogException('TrackAllocationSourceController', 'trackSupportRequest', ex);
        }
    }
    public static void trackAllocatedRecruitersFromIds(List<String> recs, String source) {}
}