@IsTest 
public with sharing class TrackAllocationSourceController_Test {
    public static User usr;
    public static Opportunity oppr;
    public static OpportunityTeamMember otm;
	public static Support_Request__c sr;
    
	static void setup(String profile, String status, String teamMemeberRole) {
        // Insert test user
        usr = TestDataHelper.createUser(profile);
        insert usr;
        
        User u = [Select Id, Name, Email from User where id =: usr.Id];
        
        // Insert test account
        Account acnt = TestData.newAccount(1);
        insert acnt;
        
        // Insert test oppy
        oppr = TestData.newOpportunity(acnt.Id, 1);
        oppr.StageName = 'Qualifying';
        oppr.status__c = status;
        insert oppr;
        
        // Insert test OTM
        otm = new OpportunityTeamMember(
            UserId = u.Id,
            teammemberrole = teamMemeberRole,
            OpportunityId = oppr.Id,
            OpportunityAccessLevel = 'Edit'
        );
        insert otm;
        
        // Insert test Support Center
        String srRecordTypeId = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get('Delivery Center').getRecordTypeId();
        sr = new Support_Request__c(
            Alternate_Delivery_Office__c = 'MDC - Communications',
            Alternate_Delivery_Positions_approved__c = 2,
            Opportunity__c = oppr.Id,
            Opportunity_OpCo__c = 'TEKsystems, Inc.',
            RecordTypeId = srRecordTypeId,
            Comments_Reason_for_Compensations__c = '',
            Completed_Date__c = null,
            Source__c = 'Opportunity'
        );
        insert sr;
    }
    
    @IsTest
    static void trackAllocationsByIdMatch() {
        setup('Single Desk 1', 'Open', 'Allocated Recruiter');
        
        System.runAs(usr) {
            if (otm.Id != null) {
            	TrackAllocationSourceController.trackAllocatedRecruiters(new List<OpportunityTeamMember>{otm});
                
                List<Req_Allocation_Track__c> tracker = [SELECT Id FROM Req_Allocation_Track__c WHERE Opportunity__c = :otm.OpportunityId];
                System.assert(tracker.size() > 0);
            }
        }
    }
    
    @IsTest
    static void trackAllocationsByIdMisMatch() {
        setup('Single Desk 1', 'Open', 'Recruiter');
        
        System.runAs(usr) {
            if (otm != null) {
            	TrackAllocationSourceController.trackAllocatedRecruiters(new List<OpportunityTeamMember>{otm});
                
                List<Req_Allocation_Track__c> tracker = [SELECT Id FROM Req_Allocation_Track__c WHERE Opportunity__c = :otm.OpportunityId];
                System.assert(tracker.size() == 0);
            }
        }
    }
    
    @IsTest
    static void trackSupportById() {
        setup('Single Desk 1', 'Open', 'Allocated Recruiter');
        
        System.runAs(usr) {
            if (sr.Id != null)
            	TrackAllocationSourceController.trackSupportRequests(new List<Support_Request__c>{sr});
                
                List<Req_Allocation_Track__c> tracker = [SELECT Id FROM Req_Allocation_Track__c WHERE Opportunity__c = :sr.Opportunity__c];
                System.assert(tracker.size() > 0);
        }
    }
}