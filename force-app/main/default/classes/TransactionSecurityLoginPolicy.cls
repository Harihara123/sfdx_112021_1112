global without sharing class TransactionSecurityLoginPolicy implements TxnSecurity.EventCondition {
	//users logged in with below profile Ids will not be considered for email notification
	//profile Ids are related to System Administrator, ISAdmin, System Integration.
	public static Set<String> allowedProfileIds = new Set<String>{'00eU0000000p8wwIAA','00eU0000000RMGaIAO','00eU0000000RJf1IAG'};
	//public static Set<String> userTypeDetails = new Set<String>{'CspLitePortal'};

    public boolean evaluate(SObject event) {
        switch on event{
            when LoginEvent loginEvent {
                return verifyLogin(loginEvent);
            }
            when null {
                 return false;   
            }
            when else{
                return false;
            }
        }   

    }

    private boolean verifyLogin(LoginEvent loginEvent) {
		///users logged in with other profiles and login URL doesnt contain allegis group.
		if(verifyProfile(loginEvent.userId)) {
			if(loginEvent.UserType!= null) {//User type CspLitePortal belongs to community users should not be sent notification
            if(loginEvent.UserType.equals('CspLitePortal')) {                
                return false;
            }
			}
            if(!loginEvent.LoginUrl.containsIgnoreCase('allegisgroup')) {
				return true;
        	}
		}
		return false;
    }
    
    public static boolean verifyProfile(Id userId) {
        
        List<User> userDetails = [SELECT Id FROM user WHERE Id =: userId AND ProfileId IN :TransactionSecurityLoginPolicy.allowedProfileIds];
        if(userDetails.size() > 0)
            return false;
        
        return true;
    }

}