@isTest
public class TransactionSecurityLoginPolicyTest {

   static testMethod void testLoginEventwithSysAdminProfile() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {

            //List<Profile> profileList = [SELECT Id, Name, UserType FROM Profile WHERE Name='System Administrator' OR Name = 'Single Desk 1' OR Name='Aerotek Lightning Community User'];
			//System.debug('profileList:'+profileList);
			User user1 = TestDataHelper.createUser('System Administrator');
			insert user1;

            assertOnProfile(user1);

			User user2 = TestDataHelper.createUser('Single Desk 1');
			insert user2;
			System.runAs (user2) {
				assertOnProfile(user2);
			}
			
        }
    } 

	static testMethod void testLoginEventCommUser() {
			Profile prfole = [select Id,name from Profile where UserType='CspLitePortal' LIMIT 1];

			User user3 = TestDataHelper.createUser(prfole.name);
			Account acc = TestDataHelper.createAccount();
			insert acc;
			Contact con = TestDataHelper.createContact();
			con.AccountId = acc.Id;
			insert con;

			user3.ContactId = con.Id;
			insert user3;
			 System.runAs (user3) {
				LoginEvent testEvent = new LoginEvent();
				testEvent.UserId = user3.Id;
				testEvent.LoginUrl = 'test.salesforce.com';
				testEvent.UserType = 'CspLitePortal';

				TransactionSecurityLoginPolicy eventCondition = new TransactionSecurityLoginPolicy();
				eventCondition.evaluate(testEvent);
			 }
            
	}
   
	static testMethod void testLoginEventTestUrl() {
			User user2 = TestDataHelper.createUser('Single Desk 1');
			insert user2;

			 System.runAs (user2) {
				LoginEvent testEvent = new LoginEvent();
				testEvent.UserId = user2.Id;
				testEvent.LoginUrl = 'test.salesforce.com';

				TransactionSecurityLoginPolicy eventCondition = new TransactionSecurityLoginPolicy();
				eventCondition.evaluate(testEvent);
			 }
            
	}

    static void assertOnProfile(User user){
        // set up our event and its field values
        LoginEvent testEvent = new LoginEvent();
        testEvent.UserId = user.Id;
        testEvent.LoginUrl = 'allegisgroup--junedev19.cs30.my.salesforce.com';

		//ApiEvent testEventelse = new ApiEvent();
		SObject sObj = new Contact();
       
	    // test that the Apex returns true for this event
        TransactionSecurityLoginPolicy eventCondition = new TransactionSecurityLoginPolicy();
		eventCondition.evaluate(testEvent);		
		eventCondition.evaluate(null);
		eventCondition.evaluate(sObj);
        //System.assertEquals(expected, eventCondition.evaluate(testEvent));  
        //System.assertEquals(!expected, eventCondition.evaluate(testEvent2)); 
    }    

}