public class TransactionStats {
    
	@AuraEnabled
    public static string getTransactionStats() {
        
        statsData statsData = new statsData();
        List<clsInnerJSON> statsDataObj = new List<clsInnerJSON>();
        clsInnerJSON innerJSON = new clsInnerJSON();         
        
        innerJSON.maxLimit = string.valueOf(Limits.getLimitQueries());
        //innerJSON.used = string.valueOf(Limits.getQueries());
        innerJSON.percentage = string.valueOf((Limits.getQueries() / Limits.getLimitQueries()) * 100);
		innerJSON.type = 'SOQL';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();        
        innerJSON.maxLimit = string.valueOf(Limits.getLimitDmlStatements());
        //innerJSON.used = string.valueOf(Limits.getDmlRows());
        innerJSON.percentage = string.valueOf((Limits.getDmlRows() / Limits.getLimitDmlStatements()) * 100);
        innerJSON.type = 'DML_STMT';        
        statsDataObj.add(innerJSON);
                
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitCpuTime());
        //innerJSON.used = string.valueOf(Limits.getCpuTime());
        innerJSON.percentage = string.valueOf((Limits.getCpuTime() / Limits.getLimitCpuTime()) * 100);
        innerJSON.type = 'CPU';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitDMLRows());
        //innerJSON.used = string.valueOf(Limits.getDMLRows());
        innerJSON.percentage = string.valueOf((Limits.getDMLRows() / Limits.getLimitDMLRows()) * 100);
        innerJSON.type = 'DML_ROWS';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitHeapSize());
       // innerJSON.used = string.valueOf(Limits.getHeapSize());
        innerJSON.percentage = string.valueOf((Limits.getHeapSize() / Limits.getLimitHeapSize()) * 100);
        innerJSON.type = 'HEAP';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitSoslQueries());
       // innerJSON.used = string.valueOf(Limits.getSoslQueries());
        innerJSON.percentage = string.valueOf((Limits.getSoslQueries() / Limits.getLimitSoslQueries()) * 100);
        innerJSON.type = 'SOSL';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitAggregateQueries());
      //  innerJSON.used = string.valueOf(Limits.getAggregateQueries());
        innerJSON.percentage = string.valueOf((Limits.getAggregateQueries() / Limits.getLimitAggregateQueries()) * 100);
        innerJSON.type = 'AGG_QUERIES';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitFutureCalls());
       // innerJSON.used = string.valueOf(Limits.getFutureCalls());
        innerJSON.percentage = string.valueOf((Limits.getFutureCalls() / Limits.getLimitFutureCalls()) * 100);
        innerJSON.type = 'FUTURE';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitCallouts());
       // innerJSON.used = string.valueOf(Limits.getCallouts());
        innerJSON.percentage = string.valueOf((Limits.getCallouts() / Limits.getLimitCallouts()) * 100);
        innerJSON.type = 'CALLOUTS';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitQueueableJobs());
      //  innerJSON.used = string.valueOf(Limits.getQueueableJobs());
        innerJSON.percentage = string.valueOf((Limits.getQueueableJobs() / Limits.getLimitQueueableJobs()) * 100);
        innerJSON.type = 'QUEUEABLE';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitEmailInvocations());
       // innerJSON.used = string.valueOf(Limits.getEmailInvocations());
        innerJSON.percentage = string.valueOf((Limits.getEmailInvocations() / Limits.getLimitEmailInvocations()) * 100);
        innerJSON.type = 'EMAIL_INVOCATIONS';        
        statsDataObj.add(innerJSON);
        
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitQueryLocatorRows());
        //innerJSON.used = string.valueOf(Limits.getQueryLocatorRows());
        innerJSON.percentage = string.valueOf((Limits.getQueryLocatorRows() / Limits.getLimitQueryLocatorRows()) * 100);
        innerJSON.type = 'QUERY_LOCATOR_ROWS';        
        statsDataObj.add(innerJSON);
                                 
        innerJSON = new clsInnerJSON();
        innerJSON.maxLimit = string.valueOf(Limits.getLimitFindSimilarCalls());
       // innerJSON.used = string.valueOf(Limits.getFindSimilarCalls());
        innerJSON.percentage = string.valueOf((Limits.getFindSimilarCalls() / Limits.getLimitFindSimilarCalls()) * 100);
        innerJSON.type = 'SIMILAR_CALLS';        
        statsDataObj.add(innerJSON);
               
        statsData.transactionStat = statsDataObj;
        //system.debug('JSON:'+JSON.serialize(statsData));
        return JSON.serialize(statsData);             
    }
        
    
    public class statsData {
        public List<clsInnerJSON> transactionStat;
    }
    
    public class clsInnerJSON {
        public String maxlimit { get; set;}
        //public String used  { get; set;}
        public String percentage  { get; set;}
        public String type { get; set;}
    }
}