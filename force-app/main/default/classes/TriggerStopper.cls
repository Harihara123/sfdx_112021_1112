public class TriggerStopper {
    //boolean variable to stop the trigger
    public static boolean createdReqPositions = false;
    
    //boolean variable to stop the trigger "Contact_formerlyWithAccount" from being executed
    public static boolean isContactUpdated = false;
    
    // boolean variable to keep track of the triggering event
    public static boolean triggeredFromContact = false;
    
    // boolean variable to stop the execution of TGS Oppty triggers
    public static boolean submittedTGSOpportunities = false;
    
    // boolean variable to stop the execution of geolocation update triggers
      public static boolean GeoLocationUpdateTrigger = false;
      
    // boolean variable to stop the execution of Open Reqs and No Of Reqs Calculation on Account
    public static boolean accountUpdatedWithReqOpenPositions = false;
    
    //boolean variable to stop the trigger "trg_ContactMerge_UpdateTMeetingsTMeals" from being executed
    public static boolean updateContactstopper = false;
    
    //boolean variable set to True if the event is caused due to Merge
    public static boolean isMergeEvent = false;
    
    //boolean variable to stop the workflow field update trigger
    public static boolean isReqTeamMemberUpdate = false;
    
   //boolean variable to stop the workflow field update trigger
    public static boolean isReqValidationTrigger = false;
    
    //boolean variable to stop the recurisive trigger excute for staging team member logic
    public static boolean isStagingTeamMemberTrigger = false;
 
     //boolean variable to stop the recurisive trigger excute for staging team member logic
    public static boolean Unflag_Req_Template = false;
    
    public static boolean ContactIntelHistory = false;
    
    public static boolean AccountIntelHistory = false;
    
    public static boolean StrategicAccount= false;
    
    public static boolean TargetAccountUpdate= false;
    
    public static boolean TargetAccountUserOwnerUpdate= false;
    
    public static boolean TaskUpdateTalentLastModified = false;
    
    public static boolean EventUpdateTalentLastModified = false;
    
    public static boolean RoleHierarchyStopperInsert= false;
    
    public static boolean RoleHierarchyStopperUpdate= false;
    
    Public static Boolean TargetActivityStopper =false;
    
    Public static Boolean FocusNonFocusStopper=false;
    
    Public static Boolean EventTriggerstop1 =false;
    
    Public static Boolean ContUpdateStopper=false;
    
    public static boolean sumscorecardstopper=false;
	
    
    Public static boolean AccountBeforeUpdateTrigger=false;
    
    public static boolean hrxmlUpdateStopper = false;
    
    public static boolean updateLatLongStopper = false;
    
    public static boolean updateReqSummaryNumberValue= false;

    
    public static boolean revenueUpdateOnOpportunityStopper = false;
    
    public static boolean currencyFieldsUpdateOnOpportunityStopper = false;
    
    public static boolean copyDescriptionAndQualificationToTextField = false;
    
    public static boolean updateReqSkillField = false;
    public static boolean updateQualifiedDateField = false;
    public static boolean allegisPlacedStopped = false;
    
    public static boolean orderRecursiveUpdateStopper = false;
    
    public static boolean Move_Other_Submittals_to_Not_Proceeding = false; 

    public static boolean Move_Other_Submittals_to_Old_Status_On_NotProceeding = false;  
	
    public static boolean Opportunity_SOA_RVT_Callout = false; 
    
    public static boolean Opportunity_Populate_Metro = false;     
    
    public static boolean opportunityAggregateTotalStopper = false;
    
    public static boolean updateProactiveStopper = false;
    
    public static boolean deleteRecursiveOpportunityTeamMembersStopper = false;
    
    public static boolean deleteRecursiveReqTeamMembersStopper = false;
    
    //boolean variable to stop the OAcalculator for ATS as it is causing SOQL101 limit error - Akshay
    public static boolean bypassOAcalculator = false;
    
    public static boolean opportunityUpdateAnalyticsStopper = false;

	//Boolean variable to stop recursion of updating contact field PrefCentre_Aerotek_OptOut_Mobile__c from tdc_tsw__SMS_Opt_out__c.
	public static boolean sms360PrefcenterAerotekStopper = false;

    public static boolean positionRecordsManagement = false;    
    //Boolean variable to stop recursion engagementrule trigger calling Account trigger 
    public static boolean engagementRule = false;  
    //boolean variable to stop the trigger after updating Last_Moved_By__c
    public static boolean alreradyUpdatedLastMovedBy = false;
    
    public static boolean alreradyMarkedNotProceeding = false;
    
    public static boolean submittalInterviewStopper = false;

	//boolean variable to stop the second trigger after proactive submittal
	public static boolean proactiveSubmittalStopper = false;

	public static boolean attachementExtensionStopper = false;
    
	public static boolean autoCaseCreation = false;   
  
  public static boolean OpportunityBeforeUpdateTrigger=false;

  public static boolean OpportunityAfterUpdateTrigger=false;
}