@isTest
public class UTMAsyncTriggerTest {
    @isTest
    public static void test() {
        
        insert new CDC_Data_Export_Flow__c(Name = 'DataExport', Change_Event_Topic_Name__c = 'topicx',  Data_Export_All_Fields__c = false, Sobject_Topic_Name__c = 'ingest10x');
        Test.startTest();
        Test.enableChangeDataCapture();
        Account acc1 = TestData.newAccount(1, 'Talent');
        insert acc1;
        Contact con1 = TestData.newContact(acc1.Id, 1, 'Talent');
        insert con1;
        Account acc2 = TestData.newAccount(1, 'Talent');
        insert acc2;
        Contact con2 = TestData.newContact(acc2.Id, 1, 'Talent');
        insert con2;
        
        Unified_Merge__c um = new Unified_Merge__c(Input_ID_1__c = acc1.Id, Input_ID_2__c = acc2.Id);
        insert um;
        Test.getEventBus().deliver();
        Test.stopTest();
        System.assertEquals(1, 1, 'Error inserting UTM record!');        
    }
}