@RestResource(urlMapping='/UTM/DataServices/FetchData/*')
global with sharing class UTMDataServices {
	private static String defRecordStatus = 'Ready';
	private static String fetchQuery = 'SELECT Id,Input_ID_1__c,Input_ID_2__c,Transaction_Batch_ID__c FROM Unified_Merge__c WHERE Process_State__c =';// + '\'Ready\'';
    private static String CONNECTED_LOG_ID = 'UTM/DataService/';
    private static String CLASS_NAME='UTMDataServices';
    @HttpPost
    global static void doPost(String recordStatus, Integer numberOfRecords, String transactionId,String sourceSystem, Integer recLimit,List<String> ignoreIds) {
        String selectQuery = fetchQuery;
        if(recordStatus == null || recordStatus == ''){
            recordStatus = defRecordStatus;            
        }        
        selectQuery = fetchQuery + ('\'' +recordStatus+'\'');
        if(sourceSystem != null && sourceSystem.length()>0){
            selectQuery = selectQuery + ' AND Request_Source_Name__c =' +('\'' +sourceSystem+'\'');
        }// Handle picklist values check
        if(transactionId != null && transactionId.length()>0){
            selectQuery = selectQuery + ' AND Transaction_Batch_ID__c  =' +('\'' +transactionId+'\'');
        }// SOQL injection 
        if(ignoreIds != null && ignoreIds.size()>0){
            selectQuery = selectQuery + ' AND ID not in :ignoreIds';
        }
        if(recLimit != null){
        	selectQuery = selectQuery + ' limit '+recLimit;    
        }
        
        System.debug('selectQuery ' + selectQuery);
        RestContext.response.addHeader('Content-Type', 'application/json');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        List<Unified_Merge__c> reccords= Database.query(selectQuery);
        system.debug(reccords);
        gen.writeObjectField('recordList', reccords); 
        gen.writeEndObject();
        RestContext.response.responseBody = Blob.valueOf(gen.getAsString()); 
   }
}