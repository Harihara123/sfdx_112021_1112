@isTest
public class UTMDataServicesTest {
    
	static testMethod void  callPostMethod(){
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/UTM/DataServices/FetchData/*';
        req.httpMethod = 'POST';
        /*
	    req.addParameter('recordStatus', 'Ready');
        req.addParameter('numberOfRecords', '4');
        req.addParameter('transactionId', 'tr1');
        req.addParameter('sourceSystem', 'ATS');
        req.addParameter('recLimit', '10');   */     
                
        RestContext.request = req;
        RestContext.response= res;
        UTMDataServices.doPost('Ready', 4, 'tr1', 'ATS', 6, new List<String>());
        System.debug(res);
        System.assert(res!=null);
        Test.stopTest();
    }
}