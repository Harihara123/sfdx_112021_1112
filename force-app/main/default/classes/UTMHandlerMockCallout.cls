@isTest 
global class UTMHandlerMockCallout implements HttpCalloutMock{

	global HttpResponse respond(HttpRequest request) {
		HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
		response.setBody('The Merge will be started.');
		response.setStatusCode(200);
		return response;
	}
}