global class UTMScheduledBatch implements Schedulable {

    Static String CONNECTED_UTM_BATCH_LOG_TYPE_ID = 'UTM/Batch';
    Static String CONNECTED_UTM_BATCH_EXCEPTION_LOG_TYPE_ID = 'UTM/BatchException';
    Static String CONNECTED_LOG_CLASS_NAME = 'ATSUnifiedTalentMergeBatch';
	Static String CONNECTED_LOG_METHOD = 'UnifiedTalentMergeSchedulable';

	global void execute(SchedulableContext sc) {
		ATSUTMHandler batchSchedule = new ATSUTMHandler();
		//schedule only if there are merge objects in ready state
		List<Unified_Merge__c> rdyMerges = [SELECT Id FROM Unified_Merge__c WHERE Process_State__c = 'Ready' limit 1];
		UTMBatchSettings__c settings = UTMBatchSettings__c.getValues('UTMSchedule');

		if( rdyMerges.size() > 0){
			ConnectedLog.LogInformation(CONNECTED_UTM_BATCH_LOG_TYPE_ID, CONNECTED_LOG_CLASS_NAME,CONNECTED_LOG_METHOD, ('Successful Schedule running on ' + rdyMerges.size() + ' Unified_Merge__c Objects.'));
            if(Test.isRunningTest()){
                ATSUTMHandler.callUTMHeroku(5, '');
            }
            else{
				ATSUTMHandler.callUTMHeroku((Integer)settings.BatchSize__c, '');
            }
		}
		else{
			ConnectedLog.LogInformation(CONNECTED_UTM_BATCH_LOG_TYPE_ID, CONNECTED_LOG_CLASS_NAME,CONNECTED_LOG_METHOD, 'NO UNIFIED_MERGE__C OBJECTS IN PROCESS STATE \'READY\'');
		}
	}
}