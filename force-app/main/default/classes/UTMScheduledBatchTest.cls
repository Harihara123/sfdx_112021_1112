@isTest 
private class UTMScheduledBatchTest {

	@isTest
	private static void testExecute(){ 
		Account acc1 = TestData.newAccount(1, 'Talent');
		Account acc2 = TestData.newAccount(2, 'Talent');
        insert acc1;
        insert acc2;

		Contact c1 = TestData.newContact(acc1.Id, 1, 'Talent');
		Contact c2 = TestData.newContact(acc2.Id, 2, 'Talent');
        insert c1;
        insert c2;

		Unified_Merge__c mergeObj = TestData.createMergeObject(c1.Id, c2.Id);
        insert mergeObj;
		List<Unified_Merge__c> rdyMerges = [SELECT Id FROM Unified_Merge__c WHERE Process_State__c = 'Ready'];
		System.debug(rdyMerges);
		 Test.setMock(HttpCalloutMock.class, new UTMHandlerMockCallout());
	     test.starttest();
			 UTMScheduledBatch myClass = new UTMScheduledBatch();   
			 String chron = '0 0 23 * * ?';        
			 system.schedule('Test Sched', chron, myClass);
         test.stopTest();
        
        List<Unified_Merge__c> merged = [SELECT Id, Process_State__c FROM Unified_Merge__c];

		 
	}
}