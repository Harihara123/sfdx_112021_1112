public class UTMTriggerHandler {
	private static boolean hasBeenProccessed = false;
    public void publishUpdates(List<Unified_Merge__c> updatedRecords){
    	
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed) {
        	hasBeenProccessed = true;
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UTMTriggerHandler', 'publishUpdates', 
                    'Triggered ' + updatedRecords.size() + ' Unified_Merge__c records: ' + CDCDataExportUtility.joinObjIds(updatedRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(updatedRecords, 'Unified_Merge__c');
        }
    }
}