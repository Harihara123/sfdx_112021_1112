global class UncommittedObjectsCleanupBatch implements Database.Batchable<sObject> {

    global Enum CLEANUP_TARGET {
        ACCOUNT, 
        CONTACT, 
        TALENT_DOCUMENT,
        TALENT_EXPERIENCE,
        REFERENCE_ACCOUNT
    }

    String query;
    CLEANUP_TARGET target;
    
    global UncommittedObjectsCleanupBatch(CLEANUP_TARGET inp, String qry) {
        target = inp;
        // Timestamp one hour before now.
        String dtHrBack = Datetime.newInstance(Datetime.now().getTime() - 3600000).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
        if (!String.isBlank(qry)) {
            query = qry;
        } else if (inp == CLEANUP_TARGET.ACCOUNT) {
            query = 'SELECT Id FROM Account WHERE RecordType.Name = \'Talent\' AND Talent_Committed_Flag__c = false and CreatedDate < ' + dtHrBack + ' and CreatedDate = LAST_N_DAYS:10';
        } else if (inp == CLEANUP_TARGET.CONTACT) {
            query = 'SELECT Id FROM Contact WHERE RecordType.Name = \'Talent\' and CreatedDate < ' + dtHrBack + ' and CreatedDate = LAST_N_DAYS:10 and Talent_Committed_Flag__c = false';
        } else if (inp == CLEANUP_TARGET.TALENT_DOCUMENT) {
            query = 'SELECT Id FROM Talent_Document__c WHERE (Mark_For_Deletion__c = true OR Committed_Document__c = false) AND CreatedDate < ' + dtHrBack + ' and CreatedDate = LAST_N_DAYS:10';
        } else if (inp == CLEANUP_TARGET.TALENT_EXPERIENCE) {
            query = 'SELECT Id FROM Talent_Experience__c WHERE Talent_Committed_Flag__c = false AND CreatedDate < ' + dtHrBack + ' and CreatedDate = LAST_N_DAYS:10';
        }else if (inp == CLEANUP_TARGET.REFERENCE_ACCOUNT) {
            query = 'SELECT Id, Error__c FROM Account WHERE RecordType.Name = \'Reference\' AND Error__c=\'Delete\' ' ;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        try {
            delete scope;
        } catch (DMLException de) {
            Integer numErrors = de.getNumDml();
            System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug(logginglevel.WARN,'getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug(logginglevel.WARN,'getDmlMessage=' + de.getDmlMessage(i));
            }
        } catch (Exception e) {
            System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());
            System.debug(logginglevel.WARN,'Message: ' + e.getMessage());
            System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
            System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());
            System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext BC){
       // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       String subject = '';
       if (target == CLEANUP_TARGET.ACCOUNT) {
           subject = 'Uncommitted Talent Accounts cleanup ';
       } else if (target == CLEANUP_TARGET.CONTACT) {
           subject = 'Uncommitted Talent Contacts cleanup ';
       } else if (target == CLEANUP_TARGET.TALENT_DOCUMENT) {
           subject = 'Uncommitted and Deleted Talent Documents cleanup ';
       } else if (target == CLEANUP_TARGET.TALENT_EXPERIENCE) {
           subject = 'Uncommitted Talent Experience cleanup ';
       }else if (target == CLEANUP_TARGET.REFERENCE_ACCOUNT) {
           subject = 'Uncommitted Orphan Reference Account cleanup ';
       }
       mail.setSubject(subject + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}