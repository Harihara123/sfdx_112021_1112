global class UpdateContactInsightsBatch  implements Database.Batchable<sObject>
{
    String query;
    global UpdateContactInsightsBatch(String query)
    {
        this.query = query;
    }
    global Database.QueryLocator start(Database.BatchableContext BC)  
    { 
        //String clientRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        //return Database.getQueryLocator([SELECT Id FROM Contact WHERE RecordTypeId =: clientRecordTypeId]);
		return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope)
    {
			String reqRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
            List<Contact_Insights__c> contactInsightsToInsert = new List<Contact_Insights__c>(); 
            Map<Id,Contact> contMap = new Map<Id,Contact>([SELECT Id,
                                                           (SELECT Id FROM Opportunities2__r 
                                                            WHERE RecordTypeId =:reqRecordTypeId)
                                                           FROM Contact
                                                          WHERE Id IN:scope]);
            for(Contact var:contMap.values())
            {
                if(!var.Opportunities2__r.isEmpty())
                {
                    Contact_Insights__c contInsighVar = new Contact_Insights__c();
                    contInsighVar.ContactID__c = var.Id;
                    contInsighVar.Total_Number_of_Reqs__c = var.Opportunities2__r.size();
                    contactInsightsToInsert.add(contInsighVar);
                }
            }
            System.debug('contactInsightsToInsert::'+contactInsightsToInsert);
            if(!contactInsightsToInsert.isEmpty())
            {
                Database.insert(contactInsightsToInsert,FALSE);
            }
    }
    
    global void finish(Database.BatchableContext BC)
    {
    }
    
}