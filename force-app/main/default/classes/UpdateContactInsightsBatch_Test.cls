@isTest
public class UpdateContactInsightsBatch_Test 
{
    public static testMethod void Opportunity_TH_Test() 
    {
        DRZSettings__c DRZSettings = new DRZSettings__c(Name = 'OpCo',Value__c='Aerotek, Inc;TEKsystems, Inc.');
        insert DRZSettings;
        DRZSettings__c DRZSettings1 = new DRZSettings__c(Name = 'Feature_Flag_TM_Update',Value__c='False');
        insert DRZSettings1; 
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        TestData TdAccts = new TestData(1);
        List<Account> lstNewAccounts = TdAccts.createAccounts();
        
        TestData TestCont = new TestData();
        
        TestData tdConts = new TestData();
        Contact TdContObj = TestCont.createcontactwithoutaccount(lstNewAccounts[0].Id); 
        string ReqRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Req').getRecordTypeId();
        string ReqRecordType1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_Tek_System_RecordType_for_Assessment_Forms).getRecordTypeId();
        string ReqRecordType2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(label.Opportunity_EASI_RecordType_for_Assessment_Forms).getRecordTypeId();
        Job_Title__c jt = new Job_Title__c(Name ='Admin');
        insert jt;
        
        List<Job_Title__c> titleList = [select Id, Name from Job_Title__c LIMIT 2];
        string rectype='Talent';
        List<Contact> TalentcontLst = tdConts.createContacts(rectype);
        string clientRectype='Client';
        List<Contact> clientContLst = tdConts.createContacts(clientRectype);
        Product2 prod = new Product2(Name = 'New Product', 
                                     Division_Name__c = 'Aerotek',
                                     OpCo__c = 'Aerotek, Inc',
                                     Category_Id__c = '000025',
                                     Category__c = 'Welder',
                                     Job_Code__c = 'Welder',
                                     OpCo_Id__c = 'ONS',
                                     Segment_Id__c = 'POWER',
                                     Segment__c = 'Traditional Power',
                                     Skill_Id__c = '000926',
                                     Skill__c = 'Welder - Submerged',
                                     Jobcode_Id__c = '600091');
        
        insert prod;
        
        TGS_OpportunityStagesForAutoApproval__c tgs = new TGS_OpportunityStagesForAutoApproval__c(Name = 'Closed Won');
        Insert tgs;
        
        for(Account Acc : TdAccts.createAccounts()) 
        {       
            for(Integer i=0;i < 5; i++)
            {
                Opportunity newOpp = new Opportunity();
                newOpp.Name = 'New ReqOpportunity'+ string.valueof(i);
                newOpp.Accountid = Acc.Id;
                newOpp.RecordTypeId = ReqRecordType;
                if(i==3) newOpp.RecordTypeId = ReqRecordType1;
                if(i==4) {
                    newOpp.RecordTypeId = ReqRecordType2;
                    newOpp.StageName = 'Closed Won';
                    newOpp.Apex_Context__c = true;
                }
                Date closingdate = system.today();
                newOpp.CloseDate = closingdate.addDays(25);
                newOpp.Req_Division__c = 'Aerotek';
                newOpp.StageName = 'Draft';
                newOpp.Req_Total_Positions__c = 2.0;
                newOpp.Req_Client_Job_Title__c = titleList[0].Id;
                newOpp.Req_Job_Description__c = 'Testing';
                newOpp.Req_HRXML_Field_Updated__c = true;
                newOpp.Req_Hiring_Manager__c = clientContLst[0].Id;
                if(i==0) newOpp.Opco__c = 'Allegis Partners, LLC';
                if(i!=0) newOpp.Opco__c = 'Aerotek, Inc';
                newOpp.BusinessUnit__c = 'Board Practice';
                newOpp.Req_Product__c = 'Permanent';
                newOpp.Req_Terms_of_engagement__c = 'Retained Exclusive';
                newOpp.Req_Worksite_Street__c = '987 Hidden St';
                newOpp.Req_Worksite_City__c = 'Baltimore';
                newOpp.Req_Worksite_Postal_Code__c = '21228';
                newOpp.Req_Worksite_Country__c = 'United States';
                newOpp.Legacy_Product__c = prod.Id;
                if(i!=2) newOpp.Req_Exclusive__c = true;
                if(i!=2) newOpp.EnterpriseReqSkills__c = '[{"name":"AS/400","favorite":true},{"name":" RPG","favorite":true},{"name":" Cobol","favorite":true}]';
                newOpp.Currency__c = 'USD - U.S Dollar';
                newOpp.Req_Job_Title__c = 'Clinical Program Administrator';
                newOpp.Req_Qualification__c = 'test';
                if(i==1) {
                    newOpp.Req_Product__c = 'Contract'; 
                    newOpp.StageName = 'Closed Won';
                    newOpp.Apex_Context__c = true;
                }
                if(i==2) { 
                    newOpp.StageName = 'Closed Won';
                    newOpp.Apex_Context__c = true;
                }
                lstOpportunity.add(newOpp);
            }
        }
        
        insert lstOpportunity;
        String clientRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Test.startTest();
        UpdateContactInsightsBatch batchToRun = new UpdateContactInsightsBatch('SELECT Id FROM Contact WHERE RecordTypeId = \''+ clientRecordTypeId + '\' ');
        Database.executeBatch(batchToRun);         
        Test.stopTest();
        
    }
    
}