public class UpdateLegacyOrderBatch implements Database.Batchable <sObject> {

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select Id,recordtypeId,Source_System_LastModifiedBy__c,LastModifiedBy.Profile.Name,Source_System_LastModifiedDate__c,LastModifiedDate,LastModifiedById  from Order ');
    }
    
     public void execute(Database.BatchableContext BC, List<Order> scope) {
        List<Order> orders = new List<Order>();
        ID OppID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('OpportunitySubmission').getRecordTypeId();
        for (Order o : scope){
             o.recordtypeid = OppID;
             
             if (!(o.LastModifiedBy.Profile.Name.equalsIgnoreCase('System Integration'))) {
                o.Source_System_LastModifiedBy__c = o.LastModifiedById;
                o.Source_System_LastModifiedDate__c = o.LastModifiedDate;
            }
            orders.add(o);
        }
        Database.Update(orders,false);
    }

    public void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                          FROM AsyncApexJob WHERE Id = :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('UpdateLegacyOrderBatch Updates ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
}