public class UpdateTalentMailingLatLong  {
	@InvocableMethod(label='Update Mailing Lat Long Mule' description='Update Lat & Long from Mulesoft.')
    public static void updateMailingLatLong(List<Id> accountIds) {
		
		System.debug('limits:'+ Limits.getQueueableJobs());
		if (Limits.getQueueableJobs() == 0) {
			ID jobID = System.enqueueJob(new QueueableLatLong(accountIds));
		}

	}

	public class QueueableLatLong implements Queueable, Database.AllowsCallouts {
		
		public List<ID> accntIds; 
        public QueueableLatLong(List<ID> accIds){
            accntIds = accIds;
		}

		public void execute(QueueableContext QC) {
			//S-96258 - Get GeoLocation from SOA - Karthik
			List<Id> acctIdList = new List<Id>();
			for (Contact c : [SELECT Id, AccountId, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Talent_Country_Text__c, MailingLatitude, MailingLongitude, GoogleMailingLatLong__Latitude__s, GoogleMailingLatLong__Longitude__s FROM Contact WHERE Contact.AccountId IN : accntIds]){
				System.debug('MailingLatitude:: ' + c.MailingLatitude + ', MailingLongitude::' + c.MailingLongitude+', Talent_Country_Text__c::' +c.Talent_Country_Text__c);
				if(c.MailingLatitude == null && c.MailingLongitude == null && c.MailingCountry != null && c.Talent_Country_Text__c != null &&
					(c.MailingCity != null || c.MailingPostalCode != null)) {
						acctIdList.add(c.AccountId);
				}				
			}

			if (acctIdList.size() > 0) {
				List<Contact> contactList = GetLatLongFromSOA.getLatLongFromMulesoft(acctIdList);
				System.debug('UpdatedContactList::'+contactList);
				updateMailingLatLong(contactList);
			}
			

		}

		public void updateMailingLatLong (List<Contact> cList)  {
			List<Database.SaveResult> results;
			if(!cList.isEmpty()){
				System.debug(logginglevel.WARN,'Contacts to update GeoLocation - ' + cList.size());
				results = Database.update(cList, false);
				System.debug(logginglevel.WARN,'results :' + results);
			}
		}
		


	}
    //D-17963 this method will only call on contact update
    public class UpdateQueueableLatLong implements Queueable, Database.AllowsCallouts {
		public List<ID> accntIds; 
        public UpdateQueueableLatLong(List<ID> accIds){
            accntIds = accIds;
		}

		public void execute(QueueableContext QC) {
			if (accntIds.size() > 0) {
				List<Contact> contactList = GetLatLongFromSOA.getLatLongFromMulesoft(accntIds);
                QueueableLatLong latlong=new QueueableLatLong(accntIds);
				latlong.updateMailingLatLong(contactList);
			}
			
		}

	}

}