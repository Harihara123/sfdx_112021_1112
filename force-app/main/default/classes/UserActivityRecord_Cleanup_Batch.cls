global class UserActivityRecord_Cleanup_Batch implements Database.Batchable<SObject> {
	
	global UserActivityRecord_Cleanup_Batch() { }
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
     *  This batch class will be set to run every Friday 9M - Saturyday early morning 
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id FROM User_Activity__c where CreatedDate  <=  YESTERDAY '); 
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<User_Activity__c> scope) {
        
        delete scope;
        DataBase.emptyRecycleBin(scope);       
	 
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}