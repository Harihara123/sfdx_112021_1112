public with sharing class UserActivityTriggerHandler  {

 private static boolean hasBeenProccessed = false;

   public void OnAfterInsert(List<User_Activity__c> newRecords) {
       callDataExport(newRecords);
    }
     
     
    public void OnAfterUpdate (List<User_Activity__c> newRecords) {        
       callDataExport(newRecords);
    }


    private static void callDataExport(List<User_Activity__c> newRecords) {
        Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
        if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserActivityTriggerHandler', 'callDataExport', 
                    'Triggered ' + newRecords.size() + ' User_Activity__c records: ' + CDCDataExportUtility.joinObjIds(newRecords));
            }
            PubSubBatchHandler.insertDataExteractionRecord(newRecords, 'User_Activity__c');
            hasBeenProccessed = true; 
        }

    } 


}