@isTest
global class UserMockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
         String endpoint = req.getEndpoint();
         
         if(endpoint == 'https://sampleSearchRequest.com/search?1234'){
        System.assertEquals('https://sampleSearchRequest.com/search?1234', req.getEndpoint());
        }else{
        System.assertEquals('https://sampleSearchRequest.com/search?1234/positions', req.getEndpoint());
        }
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(generateResopnse());
        res.setStatusCode(200);
        return res;
    }
     public string generateResopnse(){
        string jsonRes;
        jsonRes = '{'+
        '  \"EmployeeID\": \"02434580\",'+
        '  \"FirstName\": \"Nicholas\",'+
        '  \"LastName\": \"Howard\",'+
        '  \"MiddleName\": \"Pearl\",'+
        '  \"NamePrefix\": \"\",'+
        '  \"Phone\": ['+
        '    {'+
        '      \"Type\": \"CELL\",'+
        '      \"Phone\": \"864/243-6326\"'+
        '    },'+
        '    {'+
        '      \"Type\": \"HOME\",'+
        '      \"Phone\": \"864/243-6326\"'+
        '    },'+
        '    {'+
        '      \"Type\": \"OFFICE\",'+
        '      \"Phone\": \"\"'+
        '    }'+
        '  ],'+
        '  \"Address\": ['+
        '    {'+
        '      \"Type\": \"\",'+
        '      \"Address\": \"100 Pilger Place\",'+
        '      \"City\": \"Simpsonville\",'+
        '      \"State\": \"SC\",'+
        '      \"Postal\": \"29681\",'+
        '      \"Country\": \"USA\"'+
        '    }'+
        '  ]'+
        '}';    
        return jsonRes;
    }
}