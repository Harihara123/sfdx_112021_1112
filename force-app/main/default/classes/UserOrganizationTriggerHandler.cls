/***************************************************************************
Name        : UserOrganizationTriggerHandler
Created By  : Vivek Ojha(Appirio)
Date        : 24 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Class that contains all of the functionality called by the
              User_Organization_Trigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class UserOrganizationTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';
    Map<Id, User_Organization__c> oldMap = new Map<Id, User_Organization__c>();
    Map<Id, User_Organization__c> newMap = new Map<Id, User_Organization__c>();
    

  //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<User_Organization__c> newRec) {

    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, User_Organization__c> newMap,List<User_Organization__c> newRec) {
        TC_UpdateCommunityManager.updateCMonAccount(afterInsert,oldMap,newMap);

        if (!Test.isRunningTest()) {
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserOrganizationTriggerHandler', 'OnAfterInsert', 
                        'Triggered ' + newRec.size() + ' User_Organization__c records: ' + CDCDataExportUtility.joinObjIds(newRec));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newRec, 'User_Organization__c');

            }
        }
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, User_Organization__c>oldMap, Map<Id, User_Organization__c>newMap) {

    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, User_Organization__c>oldMap, Map<Id, User_Organization__c>newMap) {
        if(BypassLogic.shouldNotBypass('UpdateUserRegion')) {
            Organization_updateUserRegion(afterUpdate, oldMap,newMap);
        }        

        TC_UpdateCommunityManager.updateCMonAccount(afterUpdate,oldMap,newMap);
        if (!Test.isRunningTest()) {
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserOrganizationTriggerHandler', 'OnAfterUpdate', 
                        'Triggered ' + newMap.values().size() + ' User_Organization__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'User_Organization__c');
            }
       }

    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, User_Organization__c>oldMap) {

    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, User_Organization__c>oldMap) {

        if (!Test.isRunningTest()) {
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserOrganizationTriggerHandler', 'OnAfterDelete', 
                        'Triggered ' + oldMap.values().size() + ' User_Organization__c records: ' + CDCDataExportUtility.joinObjIds(oldMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(oldMap.values(), 'User_Organization__c');
            }
        }

    }

  //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, User_Organization__c>oldMap) {

    }

    // Orignal Header : trigger Organization_updateUserRegion on User_Organization__c (after update)
    public void Organization_updateUserRegion(String context , Map<Id, User_Organization__c>oldMap, Map<Id, User_Organization__c>newMap) {
        Map<String,List<User>> officeUserMap = new Map<String,List<User>>();
        List<User> lstUser = [Select Id,OPCO__c,Office_Code__c,Region__c,Region_Code__c from User where UserType in ('Standard','CsnOnly') And (Not Peoplesoft_Id__c LIKE 'null%')]; // Added condition in query to exclude community users - Nidish
        List<User> lstUpdateUser = new List<User>();
        for(User usr : lstUser){
            if(officeUserMap.containskey(usr.Office_Code__c+usr.OPCO__c)){
                officeUserMap.get(usr.Office_Code__c+usr.OPCO__c).add(usr);
            }
            else{
                List<User> usrList = new List<User>();
                usrList.add(usr);
                officeUserMap.put( usr.Office_Code__c+usr.OPCO__c, usrList);
            }
        }
        system.debug('*******'+officeUserMap);
        for(User_Organization__c usrOrg : newMap.values()){
            if(usrOrg.Region_Name__c != oldmap.get(usrOrg.id).Region_Name__c && usrOrg.OpCo_Code__c == oldmap.get(usrOrg.id).OpCo_Code__c ){
                if(officeUserMap.containskey(usrOrg.Office_Code__c+usrOrg.OpCo_Code__c)){
                    List<User> usrOfficeList = new List<User>();
                    usrOfficeList = officeUserMap.get(usrOrg.Office_Code__c+usrOrg.OpCo_Code__c);
                    //iterate over the list and change each one's region name to this value.
                    for(User usr : usrOfficeList){
                        usr.Region__c = usrOrg.Region_Name__c;
                        usr.Region_Code__c = usrOrg.Region_Code__c;
                        lstUpdateUser.add(usr);
                        System.debug('I am there'+lstUpdateUser);
                    }
                }
            }
        }
        System.debug('Ready for update');
        if(lstUpdateUser.size()>0){
            Core_Data.booleanSetter();
            database.update(lstUpdateUser,false);
        }
    }

}