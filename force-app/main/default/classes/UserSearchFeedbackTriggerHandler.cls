public with sharing class UserSearchFeedbackTriggerHandler {
	private static boolean hasBeenProccessed = false;
    
    // On after insert trigger method
    public void OnAfterInsert(List<User_Search_Feedback__c> feedback) {
        if (!Test.isRunningTest()){
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserSearchFeedbackTriggerHandler', 'OnAfterInsert', 
                        'Triggered ' + feedback.size() + ' User_Search_Feedback__c records: ' + CDCDataExportUtility.joinObjIds(feedback));
                }
                PubSubBatchHandler.insertDataExteractionRecord(feedback, 'User_Search_Feedback__c');
                hasBeenProccessed = true;
            }
        }
    } 
    
    // On after update trigger method
    public void OnAfterUpdate (Map<Id, User_Search_Feedback__c>oldMap, Map<Id, User_Search_Feedback__c>newMap) {        
        if (!Test.isRunningTest()){
            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'UserSearchFeedbackTriggerHandler', 'OnAfterUpdate', 
                        'Triggered ' + newMap.values().size() + ' User_Search_Feedback__c records: ' + CDCDataExportUtility.joinObjIds(newMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(newMap.values(), 'User_Search_Feedback__c');
                hasBeenProccessed = true;
           } 
        }
    }
}