@isTest
public class UserSearchFeedbackTriggerHandler_Test {
@testSetup
  static void setupTestData(){
    test.startTest();
    User_Search_Feedback__c user_search_feedback_Obj = new User_Search_Feedback__c();
    user_search_feedback_Obj.Name = 'test';
    Insert user_search_feedback_Obj; 
    Data_Extraction_EAP__c data_extraction_eap_Obj = new Data_Extraction_EAP__c(Object_Name__c = 'Objec454', Object_Ref_ID__c = 'Objec228');
    
    Insert data_extraction_eap_Obj;
    test.stopTest();
  }
     static testMethod void testInsertuser(){
         List<User_Search_Feedback__c> userList = new List<User_Search_Feedback__c>();
         for(Integer i = 0 ; i<10 ; i++){
              User_Search_Feedback__c user = new User_Search_Feedback__c();
              userList.add(user);
         }
         if(userList != null){
          insert userList; 
        }
          Test.startTest();
          update userList;
          Test.stopTest(); 
     }
      static testMethod void testInsertuserdata(){
         List<Connected_Data_Export_Switch__c> dataList = new List<Connected_Data_Export_Switch__c>();
         for(Integer i = 0 ; i<10 ; i++){
              Connected_Data_Export_Switch__c dataexport = new Connected_Data_Export_Switch__c();
              dataexport.Insert_Allowed_All_Users__c =false;
              dataexport.Name='test12';
              dataexport.Google_Pub_Sub_Call_Flag__c =false;
              dataexport.Data_Extraction_Insert_Flag__c = false;
              dataexport.Data_Extraction_Query_Flag__c = false;
              dataexport.Max_Allowed_Queued_Job__c  =3;
             
              dataexport.PubSub_URL__c = 'https://pubsub.googleapis.com/v1/projects/connected-ingest-dev/topics/ingest10x:publish?access_token=';
              dataList.add(dataexport);
         }
         if(dataList != null){
          insert dataList; 
        }
          Test.startTest();
          upsert dataList;
          Test.stopTest(); 
     }
   
   
}