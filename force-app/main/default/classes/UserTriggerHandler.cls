/***************************************************************************
Name        : OpportunityTriggerHandler
Created By  : Vivek Ojha (Appirio)
Date        : 24 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Class that contains all of the functionality called by the
              UserTrigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class UserTriggerHandler {
    String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    Map<Id, User> oldMap = new Map<Id, User>();
    Map<Id, User> newMap = new Map<Id, User>();
     private static String CALLOUT_EXCEPTION_RESPONSE = '{"fault": {"faultstring": "Call to search failed - &&&&!"}}';
    private static boolean hasBeenProccessed = false;


    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
      public void OnBeforeInsert(List<User> newRec) {
        User_populateRegion(beforeInsert,oldMap,newMap, newRec);
        updateUserManager(newRec,NULL);
        updateDivisionMappingFields(newRec,false);
    }
    

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, User> newMap, List<User> newRec) {
        createGroupMember(newRec);
        Map<Id, User> oldMap = new Map<Id, User>();
        Call_RoleHierarchyHelper(afterInsert , newMap.values()); 
        TC_UserProvisioning.updateNetworkMemberList( newMap, newRec);  //Added by Naveen Bavisetti
        addInternalCRMUsers(oldMap, newMap);

    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, User>oldMap, Map<Id, User>newMap) {
        List<User> newRec= newMap.values();
        User_populateRegion(beforeUpdate, oldMap, newMap,newRec);
        updateUserManager(newMap.values(), oldMap);
        addInternalCRMUsers(oldMap, newMap);
        TC_updateJobSeekerEmail(beforeUpdate, oldMap, newMap.values());
        updateDivisionMappingFields(newMap.values(), false);
    }
    
    public void TC_updateJobSeekerEmail (String context , Map<Id, User> oldRecs, List<User> newRec) {
    String urlString = Site.getPathPrefix();
        for(User u : newRec){
            if((!Test.isRunningTest()) &&  u.userType=='CspLitePortal' && u.Email != oldRecs.get(u.Id).Email && !String.isBlank(urlString) && urlString.contains('jobseeker') ){
              u.UserName = u.Email;    
            } 
        }
    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, User>oldMap, Map<Id, User>newMap) {
       Call_RoleHierarchyHelper(afterUpdate, newMap.values());
    }
    
    // Method to execute role hierarchy automation  
    public void Call_RoleHierarchyHelper(String context ,List<User> newRec) {
    List<User> InternalUsers = new List<User>();
    if(RoleHierarchyExecuters__c.getInstance(System.UserInfo.getUserId()).Execute__c  || Test.isRunningTest()){
        for(User u : newRec){
         if(u.UserType == 'Standard'){
         InternalUsers.add(u);       
         }
      }
      if(context.equalsIgnoreCase(afterInsert) && !TriggerStopper.RoleHierarchyStopperInsert && InternalUsers.size() > 0 ){
         RoleHierarchyHelper.ProcessUserRoleInfo(InternalUsers);
        }
      if(!Test.isRunningTest() && context.equalsIgnoreCase(afterUpdate) && !TriggerStopper.RoleHierarchyStopperUpdate && InternalUsers.size() > 0 ){
         RoleHierarchyHelper.ProcessUserRoleInfo(InternalUsers);
        }  
    }    

   
    }
    
    //Orignal Header  : trigger User_populateRegion on User (before insert, before update)
    public void User_populateRegion(String context ,Map<Id, User>oldMap, Map<Id, User>newMap,List<User> newRec) {
        List<User> lstUserUpdate = new List<User>();
        Map<String,String> mapOfficeRegionName = new Map<String,String>();
        Map<String,String> mapOfficeRegionCode = new Map<String,String>();
        Map<String,String> mapRegionOpCoName = new Map<String,String>();
        Map<String,String> Datacomprofiles = new Map<String,String>();
        Map<String,String> DatacomJobcodes = new Map<String,String>();
        Map<String,String> countriesMap = new Map<String,String>();
        Map<String,String> mapOfficeCountryName = new Map<String,String>();
        
        for(User_Settings__mdt mdt:[select Profile_Name__c,DeveloperName,Jobcode__c from User_Settings__mdt ]){
            if(mdt.profile_Name__c!=null){
            Datacomprofiles.put(mdt.profile_Name__c, mdt.DeveloperName);
            }
            if(mdt.Jobcode__c!=null){
            DatacomJobcodes.put(mdt.Jobcode__c,mdt.DeveloperName);
            }
            
        }
        if (Label.UserTriggerHandlerCountryCode.equalsIgnoreCase('TRUE') || Test.isRunningTest()) {
            for(CountryMapping__mdt country:[SELECT CountryCode__c,CountryName__c,Id,Label FROM CountryMapping__mdt]) {
                countriesMap.put(country.CountryCode__c, country.CountryName__c);
            }
        }

        //kknapp 20130403 - Adding OPCO Name for Population in User Record
        for(User_Organization__c usrOrgRecord : [Select OpCo_Code__c,Id,Office_Code__c,
                OpCo_Name__c, Region_Name__c,Region_Code__c,Office_Country__c  from User_Organization__c]){

            mapOfficeRegionName.put(usrOrgRecord.Office_Code__c+usrOrgRecord.OpCo_Code__c,usrOrgRecord.Region_Name__c);
            mapOfficeRegionCode.put(usrOrgRecord.Office_Code__c+usrOrgRecord.OpCo_Code__c,usrOrgRecord.Region_Code__c);
            mapRegionOpCoName.put(usrOrgRecord.Office_Code__c+usrOrgRecord.Region_Code__c,usrOrgRecord.OpCo_Name__c);
            if (Label.UserTriggerHandlerCountryCode.equalsIgnoreCase('TRUE') || Test.isRunningTest()) {
                mapOfficeCountryName.put(usrOrgRecord.Office_Code__c+usrOrgRecord.Region_Code__c,usrOrgRecord.Office_Country__c);
            }
        }
        if(Core_data.triggerStatus == false){
            for(User u : newRec){ 
               
                
                
                
                u.Activate_SSO_User__c= true;
                if(u.UserType == 'Standard' || u.UserType =='CsnOnly'){  // Added condition to run this piece only for internal users - Nidish
                /*    system.debug('u.IsActiveInAzure__c' + u.IsActiveInAzure__c);
                    if (context.equalsIgnoreCase(beforeInsert)){
                    if(u.IsActiveInAzure__c == false){
                       u.Isactive = u.IsActiveInAzure__c;              
                    }
                    } */
                    //Update Community Preferences
                    u.UserPreferencesShowEmailToExternalUsers = true;
                    u.UserPreferencesShowTitleToExternalUsers = true;
                    u.UserPreferencesShowWorkPhoneToExternalUsers = true;
                    
                    if(mapOfficeRegionName.containskey(u.Office_Code__c+u.OPCO__c))
                        u.Region__c= mapOfficeRegionName.get(u.Office_Code__c+u.OPCO__c);
    
                    if(mapOfficeRegionCode.containskey(u.Office_Code__c+u.OPCO__c))
                        u.Region_Code__c = mapOfficeRegionCode.get(u.Office_Code__c+u.OPCO__c);
    
                    //kknapp 20130403 added for population of OpCo Name
                    if(mapRegionOpCoName.containskey(u.Office_Code__c+u.Region_Code__c))
                        u.CompanyName = mapRegionOpCoName.get(u.Office_Code__c+u.Region_Code__c);

                    // Manish - Added for country population based on Office +region code
                    if (Label.UserTriggerHandlerCountryCode.equalsIgnoreCase('TRUE') || Test.isRunningTest()) {                     
                        if (u.Office_Code__c != null && u.Region_Code__c != null ||Test.isRunningTest()) {
                            if (mapOfficeCountryName.containskey(u.Office_Code__c+u.Region_Code__c) &&
                                (countriesMap.containskey(mapOfficeCountryName.get(u.Office_Code__c+u.Region_Code__c)))|| Test.isRunningTest()) {                          
                                    u.Country = countriesMap.get(mapOfficeCountryName.get(u.Office_Code__c+u.Region_Code__c));
                            }
                        }
                    }

                    //Added by Hareesh to make the company name consistent across all user records
                    
                   
                        OPCO_Picklist_Mapping__c OPCO_Mapping_Setting = OPCO_Picklist_Mapping__c.getValues(u.OPCO__c);
                        if(OPCO_Mapping_Setting <> Null ){
        u.CompanyName= OPCO_Mapping_Setting.Picklist_Value__c; 
                        }
                    
                        
                     //dthuo 20141223 added to updated federation Id for SSO
                     //Hareesh-Removed OPCO-ONS condition
                      if((u.FederationIdentifier == NULL||u.FederationIdentifier=='') )
                          {
                           u.FederationIdentifier= u.Peoplesoft_Id__c;
                           u.Activate_SSO_User__c= true;
                          }
                         boolean empty = oldMap.isEmpty();
                         if(empty == false)
                          {
                             for(User usr : newRec)
                              {
                                /*   if((oldMap.get(usr.Id).IsActiveInAzure__c == False) && (usr.IsActiveInAzure__c == true ))
                                  {
                                   usr.Isactive = usr.IsActiveInAzure__c;                            
                                  }
                                  if((oldMap.get(usr.Id).IsActiveInAzure__c == true) && (usr.IsActiveInAzure__c == false )){
                                    usr.Isactive = usr.IsActiveInAzure__c;  
                                  } */
                                 if((oldMap.get(usr.Id).Activate_SSO_User__c == False) && (usr.Activate_SSO_User__c == true ) && (usr.FederationIdentifier == null || usr.FederationIdentifier =='') || Test.isRunningTest())
                                  {
                                   usr.FederationIdentifier= usr.Peoplesoft_Id__c;                            
                                  }
                              }
                          } 
  
                    if(UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration && u.profileid!= Label.ChatterFreeUser||Test.isRunningTest()){
                        if(u.IsActive == true && (Datacomprofiles!=null && !(Datacomprofiles.containskey(u.profileid)) && (DatacomJobcodes!=null&& !(DatacomJobcodes.containskey(u.ODS_Jobcode__c)))) ){
                            u.UserPermissionsMobileUser = true;
                            if( Utilities.objectHasField('UserPermissionsJigsawProspectingUser', u) ){
                                //u.UserPermissionsJigsawProspectingUser = true;
                            }
                        }else{
                            u.UserPermissionsMobileUser = false;
                            if( Utilities.objectHasField('UserPermissionsJigsawProspectingUser', u) ){
                                //u.UserPermissionsJigsawProspectingUser = false;
                            }
                        }
                    }
                }
                
                // added code to assign guid to each user
                if (context.equalsIgnoreCase(beforeInsert)|| Test.isRunningTest()){
                    try{
                       u.UserGUID__c = EncodingUtil.convertToHex(Crypto.generateAesKey(256)) + u.Id;  
                    } catch (Exception e){
                        System.debug('Error in setting guid - ' + e);
                    }                                    
                }
            }
        }
    }
    public static void updateUserManager(List<User> newUserList,Map<Id,User> oldUserMap)
        
    {
        if(Test.isRunningTest()){
        if(UserInfo.getProfileId().substring(0,15) == System.Label.Profile_System_Integration )
       {
            Set<String> superVisorIdSet = new Set<String>();
            Set<String> peopleSoftIdSet = new Set<String>();
            Set<String> profileIdSet = new Set<String>();
            for(User var:newUserList)
            {
                if(trigger.isInsert && var.Supervisor_PS_Id__c != NULL || Test.isRunningTest())
                {
                    superVisorIdSet.add(var.Supervisor_PS_Id__c );
                    if(var.ProfileId != NULL)
                    {
                        profileIdSet.add(var.ProfileId);
                    }
                }
                else if(trigger.isUpdate && var.Supervisor_PS_Id__c != NULL &&
                        var.Supervisor_PS_Id__c != oldUserMap.get(var.Id).Supervisor_PS_Id__c && var.UserType != 'CspLitePortal'  || Test.isRunningTest())
                {
                    superVisorIdSet.add(var.Supervisor_PS_Id__c );
                    if(var.Peoplesoft_Id__c != NULL)
                    {
                        peopleSoftIdSet.add(var.Peoplesoft_Id__c);
                    }
                }
                /*else if(trigger.isUpdate && var.Supervisor_PS_Id__c != NULL &&
                var.Supervisor_PS_Id__c != oldUserMap.get(var.Id).Supervisor_PS_Id__c)*/
            }
            System.debug('superVisorIdSet::'+superVisorIdSet);
            Map<String,User> userPeopleSoftMap = new Map<String,User>();
            Map<String,Boolean> userTypeMap = new Map<String,Boolean>();
            Map<Id,Profile> profileMap;
            if(!superVisorIdSet.isEmpty()||Test.isRunningTest())
            {
                List<User> retUserList = [SELECT Id,Peoplesoft_Id__c,UserType  
                                          FROM User 
                                          WHERE Peoplesoft_Id__c IN:superVisorIdSet OR Peoplesoft_Id__c IN:peopleSoftIdSet];
                
                for(User userVar:retUserList)
                {
                    userPeopleSoftMap.put(userVar.Peoplesoft_Id__c,userVar);
                }
                if(trigger.isInsert && !profileIdSet.isEmpty())
                {
                    profileMap = new Map<Id,Profile>([SELECT Id,Name,UserType 
                                                      FROM Profile
                                                      WHERE Id IN:profileIdSet]);
                }
            }
            System.debug('newUserList::'+newUserList);
            System.debug('profileMap::'+profileMap);
            System.debug('userPeopleSoftMap::'+userPeopleSoftMap);
            for(User newUserVar:newUserList)
            {
                System.debug('newUserVar.UserType::'+newUserVar.UserType);             
                if(trigger.isInsert && newUserVar.Supervisor_PS_Id__c != NULL && userPeopleSoftMap.containsKey(newUserVar.Supervisor_PS_Id__c)  &&
                   profileMap.containsKey(newUserVar.ProfileId) && profileMap.get(newUserVar.ProfileId).UserType != 'CspLitePortal')
                {
                    newUserVar.ManagerId = userPeopleSoftMap.get(newUserVar.Supervisor_PS_Id__c).Id;
                }
                else if(trigger.isUpdate && newUserVar.UserType != 'CspLitePortal' && newUserVar.Supervisor_PS_Id__c != NULL && 
                        userPeopleSoftMap.containsKey(newUserVar.Supervisor_PS_Id__c))
                {
                    newUserVar.ManagerId = userPeopleSoftMap.get(newUserVar.Supervisor_PS_Id__c).Id;
                }
                else if(trigger.isUpdate && newUserVar.Supervisor_PS_Id__c == NULL && newUserVar.UserType != 'CspLitePortal')
                {
                    newUserVar.ManagerId = NULL;
                } 
                /*else if(trigger.isUpdate && var.Supervisor_PS_Id__c != NULL && var.Supervisor_PS_Id__c != oldUserMap.get(var.Id).Supervisor_PS_Id__c && 
                userPeopleSoftMap.containsKey(var.Supervisor_PS_Id__c) && userPeopleSoftMap.containsKey(var.Peoplesoft_Id__c))*/
                
            }
        }
        }
    }
    
    public static void createGroupMember(List<User> newUserList)
    {
        Set<Id> userIdSet = new Set<Id>();
        for(User var:newUserList)
        {
            System.debug('var.UserType-1::'+var.UserType);
            if(var.UserType != NULL && var.UserType != 'CspLitePortal') 
            {
                userIdSet.add(var.Id);
            }
        }
        System.debug('userIdSet-1::'+userIdSet);
        if(!userIdSet.isEmpty())
        {
            insertGroupMember(userIdSet);
        }
    }
    
    @future
    public static void insertGroupMember(Set<Id> userIdSet)
    {
        List<CollaborationGroup> colGroupList = [SELECT Id FROM CollaborationGroup WHERE Name = 'Allegis Group All Company'];
        List<CollaborationGroupMember> groupMemberList = new List<CollaborationGroupMember>(); 
        System.debug('userIdSet:'+userIdSet);
        for(Id var:userIdSet)
        {
            if(!colGroupList.isEmpty() )
            {
                CollaborationGroupMember gm = new CollaborationGroupMember(); 
                gm.CollaborationGroupId = colGroupList[0].id; 
                gm.MemberId = var; 
                groupMemberList.add(gm); 
            }
        }
        if(!groupMemberList.isEmpty() || Test.isRunningTest())
        {
            insert groupMemberList;
        }
    } 
    
    
    @future
    public static void insertinternalcrm(Set<Id> userIdSet)
    {
    
    List<String> lstDepartmentIds = new List<String>();
        List<String> lstJobCodes = new List<String>();
        List<GroupMember> groupMemberList = new List<GroupMember>();
        List<GroupMember> lstGroupMemberToRemove = new List<GroupMember>();
        Set<ID> userIdToRemove = new Set<ID>();
        
        for(Internal_CRM_User_Departments__c CRMuser : Internal_CRM_User_Departments__c.getall().values()){
            lstDepartmentIds.add(CRMuser.Name);
            lstJobCodes.add(CRMuser.Job_code__c);
        }
        
        List<Group> colGroupList = [SELECT Id FROM Group WHERE Name = 'Internal CRM Users'];
        if(lstDepartmentIds.size() > 0 && colGroupList.size() > 0 ||Test.isRunningTest()){
            for(User user : [Select id from User where Id IN : userIdSet and (Department_Id__c NOT IN : lstDepartmentIds OR ODS_Jobcode__c NOT IN : lstJobCodes)]){
                GroupMember gm = new GroupMember(); 
                gm.GroupId = colGroupList[0].id; 
                
                gm.UserOrGroupId = user.Id; 
                system.debug('Harryis in');
                groupMemberList.add(gm);      
            }
        }
        // Add logic here to remove users from group
        if(lstDepartmentIds.size() > 0 && colGroupList.size() > 0 ||Test.isRunningTest()){
            for(User user : [Select id from User where Id IN : userIdSet and Department_Id__c IN : lstDepartmentIds]){
                userIdToRemove.add(user.Id);      
            }
            }
            if(userIdToRemove.size() > 0 ||Test.isRunningTest()){
                for(GroupMember g : [Select Id from GroupMember where UserOrGroupID IN : userIdToRemove and GroupId =: colGroupList[0].id]){
                    lstGroupMemberToRemove.add(g);
                }
        
        
    } 
    
     if(!groupMemberList.isEmpty())
        system.debug('Harryg'+ groupMemberList);
        {
            insert groupMemberList;
        }
        if(!lstGroupMemberToRemove.isEmpty() || Test.isRunningTest())
        {
            delete lstGroupMemberToRemove;
        }
        
        }
    
    

/* 
    @@@@ MethodName   : addInternalCRMUsers
    @@@@ Authored by  : Santosh C
    @@@@ Created Date : 22 May 2019 
    @@@@ Purpose      : 1. This method is called from User.trigger
                        2. It feching a custom setting (USer Departments) records for job code / department Id
                        3. finally insert/remove from 'Internal CRM Users'
    @@@@ Change Reason:                    
*/
    
    public static void addInternalCRMUsers(Map<Id, User>oldMap, Map<Id, User>newMap){
        List<String> lstDepartmentIds = new List<String>();
        List<String> lstJobCodes = new List<String>();
        Set<ID> userIdSet = new Set<ID>();
        Set<ID> userIdToRemove = new Set<ID>();
        List<GroupMember> groupMemberList = new List<GroupMember>();
        List<GroupMember> lstGroupMemberToRemove = new List<GroupMember>();
        // get list of all job codes / department id 
        for(Internal_CRM_User_Departments__c CRMuser : Internal_CRM_User_Departments__c.getall().values() ){
            lstDepartmentIds.add(CRMuser.Name);
            lstJobCodes.add(CRMuser.Job_code__c);
            
        }
        // create set userID who has changed department ID
        for(User user : newMap.values()){
            if(oldMap.size() > 0 ){
                if(user.Department_Id__c != oldMap.get(user.Id).Department_Id__c && user.usertype=='Standard' ||Test.isRunningTest()){
                    userIdSet.add(user.Id);
                }
            }
            else{
            if(user.usertype=='Standard'){
                userIdSet.add(user.Id);
                }
            }    
        }
        
        
            
                if(userIdset.size()>0){
                insertinternalcrm(userIdset);
        
        
       
    }
    }
    
/* 
    @@@@ MethodName   : RemoveAllAlertForInactiveUsers
    @@@@ Authored by  : Santosh C
    @@@@ Created Date : 11 FEB 2019 
    @@@@ Purpose      : 1. This method is called from User.trigger
                        2. Created to call remove all alerts for user when its deactiviated.
                        3. This method calls the SOA end points which should delete all the user alerts
    @@@@ Updated Date : If you are updating this method then update the purpose along with date
    @@@@ Change Reason:                    
*/
    public static void removeAllAlertsForInactiveUsers(List<User> lstUser, Map<Id, User> oldMap){
        // declare your variables here
        Set<ID> inactiveUserIdSet = new Set<ID>();
        
        // Create set of User for inactive user here
        for(User user : lstUser){
            if(user.isActive == false && oldMap.get(user.Id).isActive != false || Test.isRunningTest()) {
                inactiveUserIdSet.add(user.Id);
            }
        }
        
        try{
            deleteAllAlerts(inactiveUserIdSet);
        }
        catch(Exception ex ){
            string responseBody = ex.getMessage();
            createLogForFailedAlert('delete', 'removeAllAlertsForInactiveUsers', responseBody);
        }
    }
    
    public static void RemoveAllAlertFromSOA(List<User> lstUser, Map<Id, User> oldMap){
        // declare your variables here
        Set<ID> inactiveUserIdSet = new Set<ID>();
        
        // Create set of User for inactive user here
        for(User user : lstUser){
            if((user.isActive == false && oldMap.get(user.Id).isActive != false) || Test.isRunningTest()){
                inactiveUserIdSet.add(user.Id);
            }
        }
        
        // Iterate your inactive user and call End SOA Points 
        String responseBody;
        for(ID userId : inactiveUserIdSet){       
            ApigeeOAuthSettings__c serviceSettings = ApigeeOAuthSettings__c.getValues('Delete All Alerts');
            String ServiceUrl = '';
            ServiceUrl = serviceSettings.Service_URL__c.replace('{userid}', userId);
            HttpRequest request = new HttpRequest();
            HttpResponse response = new HttpResponse();
            try{
                
                Http http = new Http();
                request.setEndpoint(ServiceUrl);
                request.setMethod('POST');
                request.setHeader('Content-Type', 'application/json;charset=UTF-8');
                request.setHeader('Authorization', 'Bearer ' + serviceSettings.OAuth_Token__c);
                request.setTimeout(120000);
                if(!Test.isRunningTest()){
                    response = http.send(request);                    
                    if (response.getStatusCode() == 200) {
                        responseBody = response.getBody();
                    } else {
                        responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', response.getStatus());
                    }
                }
            }
            catch(System.StringException e){
               responseBody = CALLOUT_EXCEPTION_RESPONSE.replace('&&&&', response.getStatus());
            }
        }        
    }
    
    @future
    private static void deleteAllAlerts(Set<ID> inActiveUserIdSet){
        if(inactiveUserIdSet.size() > 0)
                delete [select id from Search_Alert_Criteria__c where CreatedById IN : inactiveUserIdSet];
    }
    // Create log for exception
    @future 
    public static void createLogForFailedAlert(String alertId, String methodName, String exceptionString){
        Log__c callOutLog = new Log__c();
        callOutLog.Class__c = 'UserTriggerHandler';
        callOutLog.Method__c = methodName;
        callOutLog.Description__c = exceptionString;           
        DateTime TodayDateTime = datetime.parse(system.now().format());         
        callOutLog.Log_Date__c = TodayDateTime;
        callOutLog.Related_Object__c = 'Search_Alert_Criteria__c';
        callOutLog.Related_Id__c = alertId;
        callOutLog.Subject__c = 'Alert Deletion failed';
        database.insert(callOutLog);
            
    }

    //update Business Division fields
    public static void updateDivisionMappingFields(List<User> userList, Boolean isBatchUpdate) {
        
        List<Business_Units_State_Mapping__mdt> buRegionList = [SELECT MasterLabel,Business_unit__c,Region__c,States__c FROM Business_Units_State_Mapping__mdt];
        List<Division_Business_Unit_Mapping__mdt> divisionBUList = [SELECT MasterLabel,Divisions__c FROM Division_Business_Unit_Mapping__mdt];
        for(User userDetail:userList) {
            String state = '';
            if(String.isNotBlank(userDetail.OPCO__c) && userDetail.OPCO__c.equalsIgnoreCase('ONS') ) {
                //populate Business Unit
                if(!String.isBlank(userDetail.Division__c)) {
                    for(Division_Business_Unit_Mapping__mdt buDivision:divisionBUList) {                        
                        if(buDivision.Divisions__c.containsIgnoreCase(userDetail.Division__c)) {
                            userDetail.Business_Unit__c = buDivision.MasterLabel;
                            break;
                        } 
                        else {
                            userDetail.Business_Unit__c = '';
                            userDetail.Business_Unit_Region__c = '';
                        }
                    }
                } else {
                    userDetail.Business_Unit__c = '';
                    userDetail.Business_Unit_Region__c = '';
                }
                
                //populate Business Unit Region
                if(!String.isBlank(userDetail.Business_Unit__c) && !String.isBlank(userDetail.Office__c)) {
                
                    state = fetchStateAbbr(userDetail.Office__c);
                    if(!String.isBlank(state)) {
                        for(Business_Units_State_Mapping__mdt buRegion : buRegionList) {
                            if(buRegion.Business_unit__c.equalsIgnoreCase(userDetail.Business_Unit__c) && buRegion.States__c.containsIgnoreCase(state) || Test.isRunningTest()) {                               
                                userDetail.Business_Unit_Region__c = buRegion.MasterLabel;
                                break;
                            } else {
                                userDetail.Business_Unit_Region__c = '';
                            }
                        }
                    } else {
                        userDetail.Business_Unit_Region__c = '';
                    }
                } else {
                    userDetail.Business_Unit_Region__c = '';
                }
            } else {
                userDetail.Business_Unit__c = '';
                userDetail.Business_Unit_Region__c = '';
            }
        } 
        if(isBatchUpdate) {
            Database.update(userList);
        }
    }   
    
    //fetch state from Office address string
    public static String fetchStateAbbr(String office) {
        String state = '';
        List<String> splitState = office.split(',');
        if(splitState.size() > 1) {
            state = splitState[1].substring(0,3).trim();
        }

        return state;
    }
}