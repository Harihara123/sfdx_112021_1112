/**
 * Thin wrapper around the Salesforce User class. Initially created to facilitate unit testing 
 * (by allowing the wrapped methods to be mocked). Because the purpose of this class is to allow 
 * for mocking, the contained methods are NON-static.
 */
public class UserWrapper {

	public UserWrapper() {

	}
	
	/**
	 * Return the value of the LastLoginDate property from the given User object. The 
	 * LastLoginDate property is not writable, which precludes unit tests that rely on 
	 * specific values for LastLoginDate.
	 */
	public Datetime retrieveLastLoginDate(User userObj) {
		Datetime result = userObj.LastLoginDate;
		return result;
	}

	/**
	 * Return the value of the UserType property from the given User object. The 
	 * UserType property is not writable, which precludes unit tests that rely on 
	 * specific values for UserType.
	 */
	public String retrieveUserType(User userObj) {
		String result = userObj.UserType;
		return result;
	}

}