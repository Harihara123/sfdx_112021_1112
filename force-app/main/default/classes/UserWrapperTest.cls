@isTest
private class UserWrapperTest {

    /**
     * Contrived little test method just to get code coverage. The class being tested only exists 
     * to facilitate unit testing, so the unit tests on it, itself, aren't going to be very 
     * satisfying.
     */
    @isTest
    static void testRetrieveLastLoginDate() {
        Test.startTest();
        Datetime expected = null;
        User userObj = new User(); 
        UserWrapper testSubjectImpl = new UserWrapper();
        Datetime result = testSubjectImpl.retrieveLastLoginDate(userObj);
        System.assertEquals(expected, result, 
            'LastLoginDate will always be null here, as Salesforce will not allow it to be writable.');
        Test.stopTest();
    }

    /**
     * Contrived little test method just to get code coverage. The class being tested only exists 
     * to facilitate unit testing, so the unit tests on it, itself, aren't going to be very 
     * satisfying.
     */
    @isTest
    static void testRetrieveUserType() {
        Test.startTest();
        String expected = null;
        User userObj = new User(); 
        UserWrapper testSubjectImpl = new UserWrapper();
        String result = testSubjectImpl.retrieveUserType(userObj);
        System.assertEquals(expected, result, 
            'UserType will always be null here, as Salesforce will not allow it to be writable.');
        Test.stopTest();
    }

}