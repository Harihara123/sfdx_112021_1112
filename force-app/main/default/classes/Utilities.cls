/**
 * Using without sharing as need to run query on Organization table
 */
public without sharing class Utilities {
	private static final String PROD = 'allegis group';
	private static final String LOAD = 'load';
	private static final String TEST = 'test';
	private static final String DEV = 'dev';



	//Get instance from INSTANCE.visual.force.com page so we can build
	public Static String getInstance() {
		String instance = '';
		Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
		String orgType = o.OrganizationType;
		String insName = o.InstanceName;
		if (orgType == 'Developer Edition' || insName.startsWithIgnoreCase('cs')) {
			List<String> parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
			instance = parts[parts.size() - 4] + '.';
		}
		return instance;
	}
	//Needed in cases if current org is sandbox
	public static String getSubdomainPrefix() {
		Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
		String orgType = o.OrganizationType;
		String insName = o.InstanceName;
		if (insName.startsWithIgnoreCase('cs')) {
			return UserInfo.getUserName().substringAfterLast('.') + '-';
		}
		return '';
	}

	/**
	 *Might be only need isSandbox and Instance name. Other fields just in case.
	 */
	public static Map<String, String> environmentInfo() {
		Map<String, String> envInfoMap = new Map<String, String> ();
		Organization org = [SELECT id, Name, State, Street, City, Country, PostalCode, Phone, InstanceName, IsSandbox, OrganizationType FROM Organization limit 1];


		envInfoMap.put('ID', org.id);
		envInfoMap.put('Name', org.Name);
		envInfoMap.put('Street', org.Street);
		envInfoMap.put('State', org.State);
		envInfoMap.put('Country', org.Country);
		envInfoMap.put('PostalCode', org.PostalCode);
		envInfoMap.put('Phone', org.Phone);
		envInfoMap.put('InstanceName', org.InstanceName);
		envInfoMap.put('IsSandbox', String.valueOf(org.IsSandbox));
		envInfoMap.put('OrganizationType', org.OrganizationType);

		return envInfoMap;
	}

	public static String getSFEnv() {
		Map<String, String> envInfoMap = environmentInfo();
		if (envInfoMap.get('Name').toLowerCase().contains(PROD)) {
			return 'prod';
		}
		else if (envInfoMap.get('Name').toLowerCase().contains(LOAD)) {
			return 'load';
		}
		else if (envInfoMap.get('Name').toLowerCase().contains(TEST)) {
			return 'test';
		}
		else if (envInfoMap.get('Name').toLowerCase().contains(DEV)) {
			return 'dev';
		}
		else {
			return envInfoMap.get('Name').toLowerCase();
		}
	}
	//retrurning true if env is Sandbox
	public static Boolean isEnvSandbox() {
		Map<String, String> envInfoMap = environmentInfo();
		return Boolean.valueOf(envInfoMap.get('IsSandbox'));
	}

	//Retruning profile on basis of profile Name
	public static ID getProfileID(String profileName) {
		Profile prfl = [select id from Profile where name = :profileName];
		return prfl.Id;
	}

	@AuraEnabled
	public static Boolean userHasAccessPermission(String permissionName) {
		Boolean hasAccess = false;
		Boolean hasCustomPermission = FeatureManagement.checkPermission(permissionName);
		System.debug('hasCustomPermission' + hasCustomPermission);
		return hasCustomPermission;
	}

	public static List<String> picklistValues(String objectName, String fieldName) {
		List<String> values = new List<String> { };

		List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String> { objectName });

		for (Schema.DescribeSobjectResult res : results) {
			for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
				if (entry.isActive()) {
					values.add(entry.getValue());
				}
			}
		}
		return values;
	}

	/* Checks if the field exists in the object */
	public static boolean objectHasField(String fieldName, SObject obj) {
		return obj.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName.toLowerCase());
	}
	/*
		@author : Neel Kamal
		@Description : This method is checking the field level access for the log-in user.
	*/
	public static String fieldsAccessCheck(SObject recs) {
		List<SObject> objList = new List<SObject>();
		objList.add(recs);
		String fieldAccess = fieldsAccessCheck(objList);
		return fieldAccess;
	}
	/*
		@author : Neel Kamal
		@Description : This method is checking the field level access for the log-in user.
	*/
	public static String fieldsAccessCheck(List<SObject> recs) {
		SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.CREATABLE, recs, false);
		//Map<String, String> fieldsAccessMap = securityDecision.getRemovedFields();
		String fieldAccess ='';
		if(!securityDecision.getRemovedFields().isEmpty()) {
			fieldAccess = 'User :: ' + UserInfo.getUserName() + '::UserID::' + UserInfo.getUserId() + ':: No access on fields ::::'+securityDecision.getRemovedFields();
		}
		
		return fieldAccess;
	}
	/*
		@author : Neel Kamal
		@Description : this method will return opco in short version as there are more than one version of opco name.
	*/
	public static String userOpcoOnDivision() {
		return userOpcoOnDivision(UserInfo.getUserId());
	}
	/*
		@author : Neel Kamal
		@Description : this method will return opco in short version as there are more than one version of opco name.
					   Parameter as user id			   					   
	*/
	public static String userOpcoOnDivision(Id usrId) {
		User usr = [select Id, CompanyName, OPCO__c, Division__c, Division_Code__c  from User where id = :usrId];
		System.debug(usr);
		return getOpcoUsingDivision(usr.CompanyName, usr.Division_Code__c);
	}
	/*
		@author : Neel Kamal
		@Description : this method will return opco in short version as there are more than one version of opco name.
						Parameter are Company Name and Division					   
	*/
	public static String getOpcoUsingDivision(String company, String division) {
		String opco = null;
		List<OPCO_Mapping__mdt> opcoMappingList = [Select DeveloperName, Company__c, Division__c, Priority__c from OPCO_Mapping__mdt Order by Priority__c];
		System.debug(opcoMappingList);
		for (OPCO_Mapping__mdt opcoMapping : opcoMappingList) {			
			if (company.containsIgnoreCase(opcoMapping.Company__c)) {
				System.debug('param div---'+ String.isNotBlank(division));
				if(String.isNotBlank(opcoMapping.Division__c) && String.isNotBlank(division)) {
					for (String div : opcoMapping.Division__c.split(';')) {
						if (div.trim() == division.trim()) {
							opco = opcoMapping.DeveloperName;
							break;
						}
					}
				}
				else if(String.isBlank(opcoMapping.Division__c)){
					opco = opcoMapping.DeveloperName;
				}
			}
			if (opco != null) break;
		}
		
		return opco == null ? 'TEK' : opco;
	}

	/*
	@author : Neel Kamal
	@Description : This method will return the map of sate and country code. 
	*/
	public static Map<String, String> stateCountryCode(String state, String Country) {
		Map<String, String> csCodeMap = new Map<String, String> ();
		String countryCode = countryCode(country);
		csCodeMap.put('country_code', countryCode);
		countryCode += '%';
		System.debug(countryCode);

		if (String.isNotBlank(state)) {
			List<Global_LOV__c> gLovList = [SELECT Text_Value_4__c, Text_Value_3__c FROM Global_LOV__c WHERE LOV_Name__c = 'StateList' and Text_Value_2__c like :countryCode and Text_Value_3__c = :state];
			System.debug(gLovList);
			if (gLovList.size() > 0) {
				if (String.isNotBlank(gLovList[0].Text_Value_4__c)) {
					csCodeMap.put('state_code', gLovList[0].Text_Value_4__c);
				}
				else {
					//If no state abbreviation then put full name of Country and state
					csCodeMap.put('state_code', gLovList[0].Text_Value_3__c);
					csCodeMap.put('country_code', Country);
				}


			}
		}
		return csCodeMap;
	}
	/*
	 @author : Neel Kamal
	 @Description : This method will return countryCode on passing of country name as parameter.
	*/
	public static String countryCode(String country) {
		String countryCode = '';

		if (String.isNotBlank(country)) {
			List<Global_LOV__c> gLovList = [SELECT Text_Value__c FROM Global_LOV__c WHERE LOV_Name__c = 'CountryList' and Text_Value_3__c = :country];

			if (gLovList.size() > 0) {
				countryCode = gLovList[0].Text_Value__c;
			}
		}
		return countryCode;
	}
	
	/*
	@author : Neel Kamal
	@Description :This method will return picklist values. Taking object and field as parameter
	*/
	public static map<String, String> getPicklistValues(String objectname, String fieldname) {
		map<String, String> onetMap = new map<String, String> ();

		List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String> { objectName });

		for (Schema.DescribeSobjectResult res : results) {
			for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
				if (entry.isActive()) {
					onetMap.put(entry.getValue(), entry.getLabel());
				}
			}
		}
		return onetMap;
	}
	/*@Author Neel Kamal
	  @Description :This method will return dependent picklist make parent parent picklist values as key of map.
					Parameter : object with field. Ex: Job_Posting__c.Job_Subcategory__c

	*/
	public static Map<String, List<String>> getDependentPicklistValues(Schema.sObjectField dependToken) {
		Schema.DescribeFieldResult depend = dependToken.getDescribe();
		Schema.sObjectField controlToken = depend.getController();
		if (controlToken == null) {
			return new Map<String, List<String>> ();
		}

		Schema.DescribeFieldResult control = controlToken.getDescribe();
		List<Schema.PicklistEntry> controlEntries;
		if (control.getType() != Schema.DisplayType.Boolean) {
			controlEntries = control.getPicklistValues();
		}

		String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
		Map<String, List<String>> dependentPicklistValues = new Map<String, List<String>> ();
		for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
			if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
				List<String> base64chars =
				String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
				for (Integer index = 0; index<(controlEntries != null ? controlEntries.size() : 2); index++) {
					Object controlValue =
					(controlEntries == null
					 ? (Object) (index == 1)
					 : (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
					);
					Integer bitIndex = index / 6;
					if (bitIndex> base64chars.size() - 1) {
						break;
					}
					Integer bitShift = 5 - Math.mod(index, 6);
					if (controlValue == null || (base64map.indexOf(base64chars[bitIndex]) & (1 < < bitShift)) == 0)
					continue;
					if (!dependentPicklistValues.containsKey((String) controlValue)) {
						dependentPicklistValues.put((String) controlValue, new List<String> ());
					}
					dependentPicklistValues.get((String) controlValue).add(entry.getLabel());
				}
			}
		}
		return dependentPicklistValues;
	}
}