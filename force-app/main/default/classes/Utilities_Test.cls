@isTest(seealldata = true)
private class Utilities_Test {
    @isTest
    private static void getInstance(){
        String instance ;
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;

        //Set this header to test it
        ApexPages.currentPage().getHeaders().put('Host', insName+'.visual.force.com');

        if(orgType == 'Developer Edition' || insName.startsWithIgnoreCase('cs')){
            List<String> parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
            instance = parts[parts.size() - 4] + '.';
        }

        System.assertEquals(instance, Utilities.getInstance());
    }
    @isTest
    private static void getSubdomainPrefix(){
    string domain;
        Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
        String orgType = o.OrganizationType;
        String insName = o.InstanceName;
        if(insName.startsWithIgnoreCase('cs')){
            domain = UserInfo.getUserName().substringAfterLast('.')+ '-';
        }
        
        //This will always be empty unless it's sandbox
        
        System.assertEquals(domain, Utilities.getSubdomainPrefix());
    }

	@IsTest
	private static void isEvnSandbox() {
		Utilities.isEnvSandbox();
	}

	@IsTest
	private static void getuserHasAccessPermissionTest() {
		Utilities.userHasAccessPermission('Text_Consent');
	}

	@IsTest
	private static void getobjectHasFieldTest() {
		Utilities.objectHasField('Candidate_Status__c', new Account());
	}

	@IsTest
	private static void picklistValuesTest() {
		Utilities.picklistValues('Account', 'Candidate_Status__c');
	}
	
	
	@IsTest
	private static void getSFEnv_Test() {
		Utilities.getSFEnv();
	}

	@IsTest
	private static void getProfileID() {
		Test.startTest();
			Utilities.getProfileID('Single Desk 1');		
		Test.stopTest();
	}

	@IsTest
	private static void userOpcoOnDivision() {
		Test.startTest();
			Utilities.userOpcoOnDivision();		
			Utilities.userOpcoOnDivision(UserInfo.getUserId());
		Test.stopTest();
	}

	@IsTest
	private static void userOpcoOnDivision_Tek() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Single Desk 1', 'TEK', 'Applications');
			System.runAs(usr){
				Utilities.userOpcoOnDivision();		
				Utilities.userOpcoOnDivision(UserInfo.getUserId());
			}
		Test.stopTest();
	}

	@IsTest
	private static void userOpcoOnDivision_Aero() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Single Desk 1', 'ONS', '');
			System.runAs(usr){
				Utilities.userOpcoOnDivision();		
				Utilities.userOpcoOnDivision(UserInfo.getUserId());
			}
		Test.stopTest();
	}

	@IsTest
	private static void userOpcoOnDivision_Aston() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Single Desk 1', 'ONS', 'aston');
			System.runAs(usr){
				Utilities.userOpcoOnDivision();		
				Utilities.userOpcoOnDivision(UserInfo.getUserId());
			}
		Test.stopTest();
	}

	@IsTest
	private static void userOpcoOnDivision_EASi() {
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Single Desk 1', 'ONS', 'easi');
			System.runAs(usr){
				Utilities.userOpcoOnDivision();		
				Utilities.userOpcoOnDivision(UserInfo.getUserId());
			}
		Test.stopTest();
	}
	@IsTest
	private static void userOpcoOnDivision_CompanyDivision() {
		Test.startTest();
			String opco = Utilities.getOpcoUsingDivision('Aerotek, LLC', null);		
			System.assertEquals(opco, 'AERO'); 
			opco = Utilities.getOpcoUsingDivision('TEKsystems, LLC', '232');		
			System.assertEquals(opco, 'TEK'); 
			opco = Utilities.getOpcoUsingDivision('Aerotek, LLC', '16');		
			System.assertEquals(opco, 'ASTON');
			opco = Utilities.getOpcoUsingDivision('Aerotek, LLC', '110');	
			System.assertEquals(opco, 'ENS');
			//opco = Utilities.getOpcoUsingDivision('Aerotek, LLC', null);	
		Test.stopTest();
	}
	@IsTest
	private static void stateCountry_Test() {
		List<Global_LOV__c> glList = new List<Global_LOV__c>();

		Global_LOV__c gl = new Global_LOV__c();
		gl.LOV_Name__c = 'CountryList';
		gl.Text_Value_2__c = 'IND';
		gl.Text_Value_3__c = 'India';
		gl.Text_Value__c = 'IN';
		gl.Source_System_Id__c = 'CountryList.12';
		glList.add(gl);

		gl = new Global_LOV__c();
		gl.LOV_Name__c = 'CountryList';
		gl.Text_Value_2__c = 'USA';
		gl.Text_Value_3__c = 'United States';
		gl.Text_Value__c = 'US';
		gl.Source_System_Id__c = 'CountryList.13';
		glList.add(gl);

		gl = new Global_LOV__c();
		gl.LOV_Name__c = 'StateList';
		gl.Text_Value_2__c = 'US.12';
		gl.Text_Value_3__c = 'Maryland';
		gl.Text_Value_4__c = 'MD';
		gl.Source_System_Id__c = 'StateList.12';
		glList.add(gl);

		gl = new Global_LOV__c();
		gl.LOV_Name__c = 'StateList';
		gl.Text_Value_2__c = 'IN.12';
		gl.Text_Value_3__c = 'Bihar';
		gl.Text_Value_4__c = '';
		gl.Source_System_Id__c = 'StateList.14';
		glList.add(gl);

		insert glList;

		 Map<String, String> scMap = Utilities.stateCountryCode('Maryland', 'United States');
		 System.assertEquals('MD', scMap.get('state_code'));
		 Utilities.stateCountryCode('Bihar', 'India');
		 Utilities.countryCode('India');
	}

	@istest
	static void getDependentPicklistValues_Test(){
		Map<String, List<String>> depnPiclisyMap = Utilities.getDependentPicklistValues(Job_Posting__c.Job_Subcategory__c);
		System.assert(depnPiclisyMap.size() > 0);
	}

	@isTest
	static void objectFieldsAccessCheck_Test(){
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Aerotek AM', 'ONS', 'easi');
			System.runAs(usr){
				Job_Posting__c jp = new Job_Posting__c(Job_Title__c='Architecture' , eQuest_Draft_Id__c='79908');
				
				String fldAccessInfo = Utilities.fieldsAccessCheck(jp);
				System.assertEquals(true, fldAccessInfo.contains('Job_Title__c'));
			}
		Test.stopTest();
	}

	@isTest
	static void objectFieldsAccessCheckList_Test(){
		Test.startTest();
			User usr = CreateTalentTestData.createUser('Aerotek AM', 'ONS', 'easi');
			System.runAs(usr){
				Job_Posting__c jp = new Job_Posting__c(Job_Title__c='Architecture' , eQuest_Draft_Id__c='79908');
				List<Job_Posting__c> jpList = new List<Job_Posting__c>();
				jpList.add(jp);

				String fldAccessInfo = Utilities.fieldsAccessCheck(jpList);
				System.assertEquals(true, fldAccessInfo.contains('Job_Title__c'));
			}
		Test.stopTest();
	}
}