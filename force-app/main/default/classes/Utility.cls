/************************************************************
(c) 2014 Appirio, Inc. 

Description    : Genric utility class.

CreatedDate : May 29, 2014

Created By  : Urminder Singh (Appirio)
Modified By : Vivek Ojha(Appirio)
Modified Date : 09, 2015
Modified By : Vivek Ojha(Appirio) (T-409455)
Modified Date : June 10, 2015
**************************************************************/
public with sharing class Utility {
    
    /* this method tracks history for fields which are metioned in custom setting*/
    
    public static void createHistoryRecord(map<Id, sObject> oldMap, map<Id, sObject> newmap, String sObjectName) {
    if (UserInfo.getProfileId().substring(0,15) != system.label.Profile_System_Integration){
        list<Account_Intel_History__c>  accountHistoryList = new list<Account_Intel_History__c>();
        List<Contact_Intel_History__c>  contactHistoryList = new List<Contact_Intel_History__c >();
        list<String> updatedFields = new list<String>();
        List<Id> lookUpIds = new List<Id>();
        
        Map<Id,String> idNameMap = new Map<Id,String>();
        //fetching field details for Account Object.
        Map<String, Schema.SObjectType>     globalDescribe          = Schema.getGlobalDescribe(); 
        Schema.SObjectType                  objectTypeAccount              = globalDescribe.get('Account');
        Schema.DescribeSObjectResult        objectDescribeResultA    = objectTypeAccount.getDescribe();
        Map<String, Schema.SObjectField>    accountFieldMap          = objectDescribeResultA.fields.getMap();
        //fetching field details for Contact Object.
        Schema.SObjectType                  objectTypeContact              = globalDescribe.get('Contact');
        Schema.DescribeSObjectResult        objectDescribeResultC    = objectTypeContact.getDescribe();
        Map<String, Schema.SObjectField>    contactFieldMap          = objectDescribeResultC.fields.getMap();
       string conrecordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
       string Actrecordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        
        
        if(oldMap == null || newMap == null) return; //new map and old map must exists to track history.
        
        //getting fields for which we need to track history from custom setting
        for(History_Record_Setting__c setting : [select Name, Field_Name__c from  History_Record_Setting__c where Object_Name__c = :sObjectName]) {
            System.debug(' ====setting.Field_Name__c===' + setting.Field_Name__c);
            updatedFields.add(setting.Field_Name__c); 
        }
        // if no fields are found for this object then do nothing.
        if(updatedFields.isEmpty()) return;
        
        List<String> fieldList = new List<String>();
        String query =' ';
        
        for(sObject newRecord : newMap.values()) {
            //getting record from old Map.
            sObject oldRecord = oldMap.get(newRecord.Id);
            if(oldrecord.get('recordtypeid')==conrecordtypeid||oldrecord.get('recordtypeid')==Actrecordtypeid){
            //tracking history for fields
            for(String field : updatedFields) {
                System.debug(field +' ========(String)newRecord====' + newRecord+'oldRecord'+oldRecord);
                //System.debug(oldRecord.get('OtherAddress') +' ========(String)newRecord====' + newRecord.get('OtherAddress'));
                if(oldRecord.get(field) <> newRecord.get(field)) {
                    if(sObjectName == 'Account'){
                        
                        Schema.SObjectField                 objectField             = accountFieldMap.get(field);
                        Schema.DescribeFieldResult          fieldDescription        = objectField.getDescribe();
                        if(String.valueOf(fieldDescription.getType()) == 'REFERENCE'){
                            if(oldRecord.get(field) != '' || oldRecord.get(field) != null){
                                lookUpIds.add((Id)oldRecord.get(field));
                                lookUpIds.add((Id)newRecord.get(field));
                            }
                        }
                        
                    }else if(sObjectName == 'Contact'){
                        Schema.SObjectField                 objectField             = contactFieldMap.get(field);
                        Schema.DescribeFieldResult          fieldDescription        = objectField.getDescribe();
                        if(String.valueOf(fieldDescription.getType()) == 'REFERENCE'){
                            if(oldRecord.get(field) != '' || oldRecord.get(field) != null){
                                lookUpIds.add((Id)oldRecord.get(field));
                                lookUpIds.add((Id)newRecord.get(field));
                            }
                        }
                        
                    }
                }
            }
        }
        }
        idNameMap = getNameFromId(lookUpIds);
        
        for(sObject newRecord : newMap.values()) {
        
        
        
            //getting record from old Map.
            sObject oldRecord = oldMap.get(newRecord.Id);
            if(oldrecord.get('recordtypeid')==conrecordtypeid||oldrecord.get('recordtypeid')==Actrecordtypeid){
            //tracking history for fields
            for(String field : updatedFields) {
                System.debug(field +' ========(String)newRecord====' + newRecord);
                if(oldRecord.get(field) <> newRecord.get(field)) {
                    if(sObjectName == 'Account'){
                        Account_Intel_History__c history = new Account_Intel_History__c();
                        history.Field_API_Name__c = field;
                        history.Field__c = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field).getDescribe().getLabel();
                        history.Account__c = newRecord.Id;
                        
                        Schema.SObjectField                 objectField             = accountFieldMap.get(field);
                        Schema.DescribeFieldResult          fieldDescription        = objectField.getDescribe();
                        if(String.valueOf(fieldDescription.getType()) == 'REFERENCE'){
                            if(idNameMap.get(String.valueOf(oldRecord.get(field))) != Null ){
                                history.Old_Value__c = idNameMap.get(String.valueOf(oldRecord.get(field)));
                            }else{
                                    history.Old_Value__c = '';
                            }
                            if(idNameMap.get(String.valueOf(newRecord.get(field))) != Null ){
                                    history.New_Value__c = idNameMap.get(String.valueOf(newRecord.get(field)));
                                }else{
                                    history.New_Value__c = '';
                                }
                        }else{
                            history.Old_Value__c = String.valueOf(oldRecord.get(field));
                            history.New_Value__c = String.valueOf(newRecord.get(field));
                        }
                    
                        history.User__c = Userinfo.getUserId();
                        accountHistoryList.add(history);    
                    }else if(sObjectName == 'Contact'){
                        Contact_Intel_History__c history = new Contact_Intel_History__c();
                        history.Field_API_Name__c = field;
                        history.Field__c = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field).getDescribe().getLabel();
                        history.Contact__c = newRecord.Id;
                        
                        Schema.SObjectField                 objectField             = contactFieldMap.get(field);
                        Schema.DescribeFieldResult          fieldDescription        = objectField.getDescribe();
                        if(String.valueOf(fieldDescription.getType()) == 'REFERENCE'){
                            if(idNameMap.get(String.valueOf(oldRecord.get(field))) != Null ){
                                history.Old_Value__c = idNameMap.get(String.valueOf(oldRecord.get(field)));
                            }else{
                                    history.Old_Value__c = '';
                            }
                            if(idNameMap.get(String.valueOf(newRecord.get(field))) != Null ){
                                    history.New_Value__c = idNameMap.get(String.valueOf(newRecord.get(field)));
                                }else{
                                    history.New_Value__c = '';
                                }
                        }else{
                            history.Old_Value__c = String.valueOf(oldRecord.get(field));
                            history.New_Value__c = String.valueOf(newRecord.get(field));
                        }
                        
                        history.User__c = Userinfo.getUserId();
                        contactHistoryList.add(history);    
                    }
                    
                    
                }
            }   
        }
        }
        
        
        if(sObjectName == 'Account'){
            System.debug('accountHistoryList'+accountHistoryList);
            if(!accountHistoryList.isEmpty()) insert accountHistoryList;
        }else if(sObjectName == 'Contact'){
            System.debug('contactHistoryList'+contactHistoryList);
            if(!contactHistoryList.isEmpty()) insert contactHistoryList;
        }
    }
    }
    
    public Static Map<Id,String> getNameFromId(List<Id> recordIds){ 
        Map<Id,String> IdNameMap = new Map<Id,String>(); 
        List<String> ObjectApiName = new List<String>(); 
        if(recordIds.size() > 0){
            for(Id rec : recordIds ){ 
                if( rec == null)
                    continue;
                System.debug('rec=='+rec);
                ObjectApiName.add( rec.getSObjectType().getDescribe().getName()); 
            } 
            for(String ObjectName:ObjectApiName){ 
                String query = 'Select Id,Name From '+ObjectName +' Where ID IN : recordIds' ; 
                List<sObject> sobjList = Database.query(query); 
                for(sObject sObj : sobjList){ 
                    IdNameMap.put(sObj.Id,String.valueOf(sObj.get('Name'))); 
                } 
            } 
        }
        return IdNameMap; 
    }
    
    //Method to get the recordTypeId 
    public static Id getRecordTypeId(string obj,string recName){
        Id recTypeId;
        if(obj!= null && recName != null){
            recTypeId= Schema.getGlobalDescribe().get(obj).getDescribe().getRecordTypeInfosByName().get(recName).getRecordTypeId();
        }  
        return recTypeId;  
    }
    
    //Method to get the map of recordType Name as key and recordTypeId as value 
    public static Map<String,Id> getRecordTypeMap(string obj){
        Map<String,Id> recTypeNameWithIdMap=new Map<String,Id>();
        if(obj!= null){
            for(Schema.RecordTypeInfo recInfo : Schema.getGlobalDescribe().get(obj).getDescribe().getRecordTypeInfosByName().values()){
                recTypeNameWithIdMap.put(recInfo.getName(),recInfo.getRecordTypeId());
            }
        }
        return recTypeNameWithIdMap;
    } 
    // Validate a String URL is a valid Linked In or not
    public static Boolean validateLinkedInURL(String linkedinURL){
       // return ((linkedinURL.contains('linkedin')) && (linkedinURL.contains('.com'));

        String urlRegex = '((?i).*(linkedin\\.com\\/)).*';
        Pattern urlPattern = Pattern.compile(urlRegex);
        Matcher urlMatcher = urlPattern.matcher(linkedinURL);
        return urlMatcher.matches();
    }
    
    //Get the refresh token if expired.
    public static void getNewAccessToken(String ServiceName,  String queryString){        
        TC_User_Provisioning_Connector t=new TC_User_Provisioning_Connector(ServiceName);
        iF(!Test.isRunningTest()){
            t.getAccessToken();
            SearchController.searchReqs(serviceName, queryString);
        }
    }
}