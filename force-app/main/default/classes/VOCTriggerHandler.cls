/***************************************************************************
Name        : VOCTriggerHandler
Created By  : Vivek Ojha(Appirio)
Date        : 24 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Class that contains all of the functionality called by the
              VOC_Trigger. All contexts should be in this class.
*****************************************************************************/
public with sharing class VOCTriggerHandler {
  String beforeInsert = 'beforeInsert';
    String afterInsert  = 'afterInsert';
    String beforeUpdate = 'beforeUpdate';
    String afterUpdate  = 'afterUpdate';
    String beforeDelete = 'beforeDelete';
    String afterDelete  = 'afterDelete';
    String afterUndelete  = 'afterUndelete';

    //-------------------------------------------------------------------------
    // On before insert trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeInsert(List<VOC__c> newRec) {
    }

    //-------------------------------------------------------------------------
    // On after insert trigger method
    //-------------------------------------------------------------------------
    public void OnAfterInsert(Map<Id, VOC__c> newMap) {
        List<VOC__c> newRec = newMap.values();
        VOC_Update_NPS_Scores(afterInsert,newMap,newRec);
        updateEmailAddressRec(newRec);
        
    }

    //-------------------------------------------------------------------------
    // On before update trigger method
   //-------------------------------------------------------------------------
    public void OnBeforeUpdate(Map<Id, VOC__c>oldMap, Map<Id, VOC__c>newMap) {

    }

    //-------------------------------------------------------------------------
    // On after update trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUpdate (Map<Id, VOC__c>oldMap, Map<Id, VOC__c>newMap) {


    }

    //-------------------------------------------------------------------------
    // On before delete trigger method
    //-------------------------------------------------------------------------
    public void OnBeforeDelete (Map<Id, VOC__c>oldMap) {

    }

    //-------------------------------------------------------------------------
    // On after delete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterDelete (Map<Id, VOC__c>oldMap) {

    }

    //-------------------------------------------------------------------------
    // On after undelete trigger method
    //-------------------------------------------------------------------------
    public void OnAfterUnDelete (Map<Id, VOC__c>oldMap) {

    }

    /*************************************************************************************************
    // Orignal Header : trigger VOC_Update_NPS_Scores on VOC__c (after insert)
    Apex Class Name :  VOC_Update_NPS_Scores
    Version         : 1.0
    Created Date    : 11th Nov 2013
    Function        : Trigger used to perform sliding logic for VOC. When a new VOC record is entered;
    corresponding Contact's NPS scores get updated. In here, NPS2 gets updated to NPS3 and NPS3 gets updated
    to NPS2 and new scores/dates get updated to NPS1.

    Modification Log :
    -----------------------------------------------------------------------------
    * Developer                   Date                          Description
    * ----------------------------------------------------------------------------
    * Sudheer                      11/11/2013             Original Version
    **************************************************************************************************/
    public void VOC_Update_NPS_Scores(String context ,Map<Id, VOC__c> newMap,List<VOC__c> newRec) {
      Set<Id> VOCIds = new set<Id>();
        Set<Id> ContactIds = new set<Id>();
        Map<Id, Contact> map_ContactIds = new Map<Id, Contact>();
        List<Contact> lstContactUpdates = new List<Contact>();

      // get a list of all newly entered VOCs and associated Contacts
        for(VOC__c aVOC: newRec){
            if(aVOC.Contact__c!=null && aVOC.NPS_Score__c!=null && aVOC.Survey_Date__c!=null){
              VOCIds.add(aVOC.Id);
              ContactIds.add(aVOC.Contact__c);
            }
        }

        // get a map of all impacted Contacts
        for(Contact aContact:[Select Id,NPS_Score_1__c,NPS_Score_2__c,NPS_Survey_Date1__c,NPS_Survey_Date2__c From Contact where Id=:ContactIds]){
            map_ContactIds.put(aContact.id , aContact);
        }

        // for all newly entered VOCs, find corresponding Contacts and perform sliding logic
        for(VOC__c aVOC:newMap.values()){
            if (map_ContactIds.containsKey(aVOC.Contact__c)){
                Contact updateContact = map_ContactIds.get(aVOC.Contact__c);
                updateContact.NPS_Survey_Date3__c = updateContact.NPS_Survey_Date2__c;
                updateContact.NPS_Score_3__c = updateContact.NPS_Score_2__c;
                updateContact.NPS_Survey_Date2__c = updateContact.NPS_Survey_Date1__c;
                updateContact.NPS_Score_2__c = updateContact.NPS_Score_1__c;
                updateContact.NPS_Score_1__c = aVOC.NPS_Score__c;
                updateContact.NPS_Survey_Date1__c = aVOC.Survey_Date__c;
                lstContactUpdates.add(updateContact);
            }
        }
        // perform mass update on all Contacts with VOC scores
        if(lstContactUpdates.size()>0)
            database.upsert(lstContactUpdates);
    }
    public void updateEmailAddressRec(List<VOC__c> newRec) {
        Map<Id,String>vocId_TalentRecMap = new map<Id,String>();
        Map<Id,String>vocId_TalentComManMap = new map<Id,String>();
        Map<Id,String>vocId_TalentBillerMap = new map<Id,String>();
        Map<Id,String>vocId_TalentCSAMap = new map<Id,String>();
        Map<String,String>officeId_LeaderEmailMap = new map<String,String>();
        Map<String,String>officeId_LeaderNameMap = new map<String,String>();
        Map<String,String>conId_talentOfficeIdMap = new map<String,String>();
        Set<Id>vocIds = new Set<Id>();
        Set<Id>conIds = new Set<Id>();
        system.debug('++++++vocIds1+++++++'+newRec);
        for(VOC__c aVOC: newRec){
           vocIds.add(aVOC.Id); 
           conIds.add(aVOC.Contact__c);
        }
        system.debug('++++++vocIds+++++++'+vocIds);
        List<VOC__c> vocLst= [Select id,Talent_Recruiter__c,Consultant_Recruiter__c,Consultant_Account_Manager__c,Consultant_Recruiter__r.Email,Consultant_Biller__r.Email,Consultant_CSA__r.Email,Talent_Account_Manager__c,Consultant_Account_Manager__r.Email, Contact__r.Account.Talent_Recruiter__r.Email,Contact__r.Account.Talent_Biller__r.Email,Contact__r.Account.Talent_CSA__r.Email, Contact__r.Account.Talent_Community_Manager__r.Email, Contact__r.Account.Talent_Account_Manager__r.Email from VOC__c where Id IN: vocIds];
        List<OfficeLeader__mdt> officeleaderList = [SELECT Leader_Email__c,Leader_Name__c,Office_code__c from OfficeLeader__mdt];
        List<User>talentlst = new List<User>();
        if(conIds.size()>0){
            talentlst = [Select Id, ContactId ,Office_Code__c from User where ContactId IN: conIds];
        }
        for(User usr: talentlst ){
            if(!String.isBlank(usr.Office_Code__c)){
                conId_talentOfficeIdMap.put(usr.contactId, usr.Office_Code__c);
            }
        }
        for(OfficeLeader__mdt ol: officeleaderList){
            officeId_LeaderEmailMap.put(ol.Office_code__c ,ol.Leader_Email__c);
            officeId_LeaderNameMap.put(ol.Office_code__c ,ol.Leader_Name__c);
        }
        for(VOC__c v: vocLst){
            String officeCode;
             if(conId_talentOfficeIdMap.containsKey(v.contact__c)){
                 officeCode = conId_talentOfficeIdMap.get(v.contact__c);
             }
             if(!String.isBlank(officeCode) && officeId_LeaderEmailMap.Keyset().contains(officeCode)){
                 v.Leader_Email__c = officeId_LeaderEmailMap.get(officeCode);
                 v.Leader_Name__c = officeId_LeaderNameMap.get(officeCode);
             }
             if(v.Consultant_Account_Manager__r.Email != null && v.Consultant_Account_Manager__r.Email != ''){
                 vocId_TalentComManMap.put(v.Id, v.Consultant_Account_Manager__r.Email);        
             }else{
                 vocId_TalentComManMap.put(v.Id, v.Contact__r.Account.Talent_Community_Manager__r.Email);
             }
             if(v.Consultant_Biller__r.Email != null && v.Consultant_Biller__r.Email != ''){
                 vocId_TalentBillerMap.put(v.Id, v.Consultant_Biller__r.Email);        
             }
             if(v.Consultant_CSA__r.Email != null && v.Consultant_CSA__r.Email != ''){
                 vocId_TalentCSAMap.put(v.Id, v.Consultant_CSA__r.Email);        
             }
             vocId_TalentRecMap.put(v.Id, v.Consultant_Recruiter__r.Email);
        }    
        for(VOC__c aVOC: vocLst){
            if(vocId_TalentRecMap.containsKey(aVOC.Id) && vocId_TalentRecMap.get(aVOC.Id) != null){
               aVOC.Talent_Recruiter__c = vocId_TalentRecMap.get(aVOC.Id);
            }
            if(vocId_TalentComManMap.containsKey(aVOC.Id) && vocId_TalentComManMap.get(aVOC.Id) != null){ 
               aVOC.Talent_Account_Manager__c = vocId_TalentComManMap.get(aVOC.Id);
            } 
            if(vocId_TalentBillerMap.containsKey(aVOC.Id) && vocId_TalentBillerMap.get(aVOC.Id) != null){ 
               aVOC.Talent_Biller__c = vocId_TalentBillerMap.get(aVOC.Id);
            }
            if(vocId_TalentCSAMap.containsKey(aVOC.Id) && vocId_TalentCSAMap.get(aVOC.Id) != null){ 
               aVOC.Talent_CSA__c = vocId_TalentCSAMap.get(aVOC.Id);
            } 
        }      
        if(vocLst.size()>0)
        database.upsert(vocLst);
    }
}