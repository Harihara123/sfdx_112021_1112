/* Parameters -  salesforce eighteen digit id of the record in context
* Return type - String ( success / error message).
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pavan                       07/17/2012             Created
*******************************************************************************************************************************/
global class Validate_LeadConversion
{
    Webservice static String ValidateConversion(String leadId)
    {
        
        //Dynamic Query
        String query = 'select ';
        set<string> optionalFields = new set<string>();
        // Describe the context object to get all the fields
        Map<String,Schema.SObjectField> fieldMap = Schema.SObjectType.Lead.fields.getMap();
        // build the query
        for(String fld : fieldMap.keyset())
        {
          if(query == 'select ')
              query = query +fld;
          else
             query = query +','+fld;
        }
        query = query +' from Lead where id = \''+leadId+'\'';
        //Query the lead record
        lead leadrecord = database.query(query);
    
        String message = 'Please enter a value for ';
        Integer temp = message.length();
        for(Lead_Convert__c fieldsSpecified : Lead_Convert__c.getall().values())
        {
              if(fieldsSpecified.isOptional__c)
              {
                 optionalFields.add(fieldsSpecified.Field__c); 
              }
              else if(leadRecord.get(fieldsSpecified.Field__c) == Null)
              {
                  // describe the field to get the label
                  message = message+'\''+fieldMap.get(fieldsSpecified.Field__c).getDescribe().getLabel()+'\',';
              } 
        }
        integer counter = 0;
        // check for optional fields
        for(string optFlds : optionalFields)
        {
           if(leadRecord.get(optFlds) == Null)
               counter++;
           else
              continue;    
        }
        string optFld = '(';
        // if all the optional fields are blank
        if(counter == optionalFields.size())
        {
           for(string fld : optionalFields)
           {
              if(optFld == '(')
                  optFld = optFld + '\'' + fld + '\'';
              else
                  optFld = optFld + ' or \'' + fld + '\'';
           }
           optFld += ')';
        }
        if(optFld != '(')
            message += optFld; 
        if(temp != message.length())
        {
            message = message.subString(0,message.length());
            message += ' to proceed with conversion';
        }
        else
        {
         message = 'Success';
        }
      return message;
    }
}