public with sharing class ViewAllSubmittalsController
{
    public string opportunityId {get;set;}
    public string requisitionId {get;set;}
    
    public ViewAllSubmittalsController(ApexPages.StandardController controller) 
    {   
        Reqs__c Requisition = (Reqs__c) controller.getRecord();
        requisitionId = Requisition.Id;
        //Requisition = [Select Origination_System_Id__c, Origination_System__c from Reqs__c where Id =: Requisition.Id];
        
        
     } 
     
     public Pagereference showAllSubmittals()
     {
         Reqs__c Requisition = [Select Origination_System_Id__c, Req_Origination_System__c, Orig_Partner_System_Id__c,EnterpriseReqId__c from Reqs__c where Id =: requisitionId];
        
        
        if(!string.isBlank(Requisition.EnterpriseReqId__c))
        {
            if(Requisition.EnterpriseReqId__c.length() == 18)
            opportunityId = Requisition.EnterpriseReqId__c.substring(0,Requisition.EnterpriseReqId__c.length()-3);
            else
            opportunityId= Requisition.EnterpriseReqId__c;
        }
        else
        {
            List<Opportunity> lstOpportunity = [select id from opportunity where Req_Legacy_Req_Id__c=:Requisition.Id];
            if(!lstOpportunity.isEmpty())
            {
                string oppId = string.valueof(lstOpportunity[0].id);
                
                if(oppId.length() == 18)
                opportunityId = oppId.substring(0,oppId.length()-3);
                else
                opportunityId = oppId;
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No Opportunity is yet associated with the Req'));
                return null;
            }
        }
        return null;
     }
}