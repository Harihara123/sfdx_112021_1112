public class ViewReqRWSOpportunityController
{
     private final ApexPages.StandardController controller;
     public string url {get;set;}
     
   
    public ViewReqRWSOpportunityController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        url = '';
    }
    
    public void onLoad()
    {
        System.debug('Inside onLoad method');
         Opportunity opp = [Select id,OpCo__c,Req_origination_System_Id__c,Req_Origination_Partner_System_Id__c From Opportunity Where Id = :ApexPages.currentPage().getParameters().get('id')];
         
         if(opp.OpCo__c == 'TEKsystems, Inc.' || opp.OpCo__c == 'Aerotek, Inc')
         {
             System.debug('Inside if condition');
             url = '/apex/IntegrationOpenPage?searchType=RWS_VIEW_REQ&OriginationSystemId=' + opp.Req_origination_System_Id__c +'&OriginationSystem='+ opp.Req_Origination_Partner_System_Id__c;
         }
         
         
         
         System.debug('url:::: ' + url);
    }
}