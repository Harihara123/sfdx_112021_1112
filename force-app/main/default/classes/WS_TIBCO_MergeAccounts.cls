/***************************************************************************************************************************************
* Name        - WS_TIBCO_MergeAccounts 
* Description - This class holds the logic for merging two accounts in Salesforce                 
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       10/30/2012             Created
* Pratz                       1/15/2013              Modified
* Pratz                       1/29/2013              Modified to fix defect #27650
* Pratz                       3/13/2013              Modified to return an Integer with possible 3 values 
*                                                    (0 - Merge Success, 1 - System Exception, 2- Winning/Losing Account does not exist)
*****************************************************************************************************************************************/

global class WS_TIBCO_MergeAccounts {
       
    /*
    * Name: WS_doMergeAccounts
    * Description: WebService Method to Merge two accounts in Salesforce
    *
    * Parameters:
    *    String SF_MasterAccountId: Salesforce id of the Winning Account
    *    String SF_AccountToMergeId: Salesforce id of the Losing Account
    *    String SB_MasterAccountId: Seibel id of the Winning Account
    *    String SB_AccountToMergeId: Seibel id of the Losing Account
    */
          
  WebService static Integer WS_doMergeAccounts(String SF_MasterAccountId,String SF_AccountToMergeId,String SB_MasterAccountId,String SB_AccountToMergeId){
  
             //Declare variables
                          
             //Declare Integer variable to store Merge Success Code
             Integer numMergeSuccessCode = 0;

             Integer WinningAccount_NumberOfOpenPositions;
             Integer LosingAccount_NumberOfOpenPositions;
             system.debug('Salesforce Winning Account:' + SF_MasterAccountId);
             system.debug('Salesforce Losing Account:'+ SF_AccountToMergeId);
             system.debug('Siebel Winning Account:' + SB_MasterAccountId);
             system.debug('Siebel Losing Account:'+ SB_AccountToMergeId);
             List<Log__c> errors = new List<Log__c>();
             
             try {
             //Declare object of CheckIfValidId Class
             CheckIfValidId checkId = new CheckIfValidId(SF_MasterAccountId, SF_AccountToMergeId, SB_MasterAccountId, SB_AccountToMergeId);
             
             //Check if all the required Ids are valid
             Boolean isValid = checkId.CheckIfValid();
             system.debug('isValid:' +isValid);
            
             //If all the required Ids are valid
             if(isValid == True)
             { 
                 //Get the Salesforce Winning Account Id as a string
                 String str_MasterAccountId =   checkId.ReturnWinningAccountId(SF_MasterAccountId,SB_MasterAccountId,SF_AccountToMergeId,SB_AccountToMergeId);
                 
                 //Get the Salesforce Losing Account Id as a string
                 String str_AccountToMergeId =   checkId.ReturnLosingAccountId(SF_MasterAccountId,SB_MasterAccountId,SF_AccountToMergeId,SB_AccountToMergeId);
                 
                 system.debug('Winning Account: ' + str_MasterAccountId);
                 system.debug('Losing Account: '+ str_AccountToMergeId);
                    
                 //Query the Salesforce Accounts using the Salesforce Winning and Losing Account Ids and populate a map of accounts 
                 String QueryAccounts = 'SELECT Id FROM Account WHERE Id = :str_MasterAccountId OR Id = :str_AccountToMergeId';
                 Map<Id, Account> accountIdMap = new Map<Id, Account>((List<Account>)Database.query(QueryAccounts));
                 
                 //Get the Winning account and Losing accounts as individual SObjects                                  
                 Account WinningAccount = accountIdMap.get(str_MasterAccountId);
                 Account LosingAccount = accountIdMap.get(str_AccountToMergeId);  
                                         
                 system.debug('Winning: ' + SF_MasterAccountId);
                 system.debug('Losing: ' + SF_AccountToMergeId);
                                                   
                 //Start building the Dynamic SOQL Query
                 String Query = 'SELECT ';
                      
                 //Get the sObject describe result for the Account object
                 Schema.DescribeSObjectResult r = Account.sObjectType.getDescribe();
                 
                 //Generate a Map of the fields 
                 Map<String, Schema.SObjectField> M = r.fields.getMap(); 
                                    
                 //Now loop through the list of Fields        
                 for (String fieldName : M.keySet()){
                      
                     // Continue building your dynamic query string
                     Query += FieldName + ',';                                                  
                 }   
                   
                 // Trim last comma                  
                 Query = Query.substring(0,Query.length() - 1);
                   
                 // Finalize query string                   
                 Query += ' FROM Account WHERE Id = :str_MasterAccountId OR Id =:str_AccountToMergeId';
                 system.debug(Query);
                                              
                 // Make your dynamic call                
                 Map<Id, Account> accountMap = new Map<Id, Account>((List<Account>)Database.query(Query)); 
                 
                 //Get the accounts as individual SObjects                
                 SObject s1 = accountMap.get(str_MasterAccountId);
                 SObject s2 = accountMap.get(str_AccountToMergeId);
                 
                  
                        
                                                
                 //Now loop through the list of Fields        
                 for (String fieldName : M.keySet()){
                     system.debug(fieldName + ':' + string.valueOf(s1.get(FieldName)));
                     system.debug(fieldName + ':' + string.valueOf(s2.get(FieldName)));
                     
                     //Defect #27650
                     //Declare a Boolean variable
                     Boolean Flag1 = False; 
                             
                     if(fieldName == 'parentid' || fieldName == 'global_ultimate_account__c')
                     {
                                                                    
                                  //Defect #27650                                
                                  //Loop through the List of Child accounts of the Winning account to check 
                                  //if the Parent of the Losing account is a child of the Winning account
                                  for(Account wca : [SELECT Id, ParentId FROM Account WHERE  ParentId = :str_MasterAccountId])
                                  {   
                                                                            
                                      if( string.valueOf(s2.get(fieldName)) == string.valueOf(wca.id) )
                                      {                                               
                                           Flag1 = True;
                                           Break;
                                      }  
                          
                                  }
                                  
                                  
                                  //If Winning Account ParentId or global_ultimate_account__c Value is null and Losing Record ParentId/global_ultimate_account__c
                                  //Value is not null and the Losing Account's ParentId/global_ultimate_account__c is not same as, the Winning Account Id or the Winning Account's Child                          
                                  system.debug('S1 Global Ultimate Account:'+ string.valueOf(s1.get(fieldName)));
                                  system.debug('S2 Global Ultimate Account:'+ string.valueOf(s2.get(fieldName)));
                                  if(Flag1 != True && string.valueOf(s1.get(fieldName)) == Null && (string.valueOf(s2.get(fieldName)) != Null && string.valueOf(s2.get(fieldName)) != str_MasterAccountId))
                                  {
                                     
                                     s1.put(fieldName,s2.get(fieldName));
                                     system.debug('S1 Global Ultimate Account:'+ string.valueOf(s1.get(fieldName)));
                                     system.debug('S2 Global Ultimate Account:'+ string.valueOf(s2.get(fieldName)));
                                  }
                          
                                                                                                                                      
                     }
                     else { 
                              
                              
                            //If Winning Account Field Value is null and Losing Record Field Value is not null 
                            //then assign the Losing record field's value to the Winning Master Record field
                            //Example: WinningAccount - Website = Null & Do_Not_Contact__c = False
                            //         LosingAccount - Website = www.google.com & Do_Not_Contact__c = True
                            if((string.valueOf(s1.get(fieldName)) == Null || string.valueOf(s1.get(fieldName)) == 'False') && (string.valueOf(s2.get(fieldName)) != Null || string.valueOf(s2.get(fieldName)) == 'True'))
                            {
                                    
                                    Schema.DescribeFieldResult F = M.get(fieldName).getDescribe();
                                    
                                    //If the Field is updatetable and is not Siebel_ID__c
                                    if(F.isUpdateable() && fieldName != 'Siebel_ID__c')
                                    {
                                        system.debug('Master -'+fieldName+'-'+string.valueOf(s1.get(fieldName)));
                                        system.debug('Slave -'+fieldName+'-'+string.valueOf(s2.get(fieldName)));
                                        s1.put(fieldName,s2.get(fieldName));                                    
                                        system.debug('Master -'+fieldName+'-'+string.valueOf(s1.get(fieldName)));
                                    }
                                    
                                    
                                    //If the field No_of_Open_Positions__c is null in the Winning Account Record then update 
                                    // the winning Account's No_of_Open_Positions__c field by summing the Winning and Losing Account's 
                                    // value for No_of_Open_Positions__c field values
                                    If(fieldName == 'No_of_Open_Positions__c')
                                    {
                                                                                                                           
                                         Integer SumOfNoOfOpenPositions = Integer.valueOf(s2.get(fieldName));
                                         
                                         system.debug('Total Open Positions:' + SumOfNoOfOpenPositions);
                                         s1.put(fieldName,SumOfNoOfOpenPositions); 
                                         
                                    }
                                    
                                    
                                    
                                    //This section of code has been commented out following the discussion with Sudheer so as to ensure that
                                    //Siebel_ID__c field value of Losing account is not copied to the Winning Account's Siebel_ID__c even if it is null
                                    //If the Field is Updateable and is Siebel_ID__c then update the Losing Account's Siebel Id to '0'
                                    //This update of Siebel_ID__c field is neccessary as Siebel_ID__c is unique field and 
                                    //it is not possible to have both the Winning and Losing Accounts with same Siebel_ID__c feld value
                                    //if(F.isUpdateable() && fieldName == 'Siebel_ID__c')
                                    //{
                                        //system.debug('Master -'+fieldName+'-'+string.valueOf(s1.get(fieldName)));
                                        //system.debug('Slave -'+fieldName+'-'+string.valueOf(s2.get(fieldName)));                                        
                                        //s1.put(fieldName,s2.get(fieldName));     
                                        //s2.put(fieldName,'0');                                                                        
                                        //system.debug('Master -'+fieldName+'-'+string.valueOf(s1.get(fieldName)));
                                        //system.debug('Slave -'+fieldName+'-'+string.valueOf(s2.get(fieldName)));
                                    //}
                                                                         
                            }
                            else
                            {
                                    //If the field No_of_Reqs__c is not null in the Winning Account Record then update 
                                    // the winning Account's No_of Reqs__c field by summing the Winning and Losing Account's 
                                    // value for No_of_Reqs__c field
                                    If(fieldName == 'No_of_Reqs__c' && string.valueOf(s1.get(fieldName))!= Null && string.valueOf(s2.get(fieldName))!= Null)
                                    {
                                         Integer SumOfNoOfReqs = Integer.valueOf(s1.get(fieldName)) + Integer.valueOf(s2.get(fieldName));
                                         s1.put(fieldName,SumOfNoOfReqs); 
                                         
                                    }
                                    
                                    //If the field No_of_Open_Positions__c is not null in the Winning Account Record then update 
                                    // the winning Account's No_of_Open_Positions__c field by summing the Winning and Losing Account's 
                                    // value for No_of_Open_Positions__c field
                                    If(fieldName == 'No_of_Open_Positions__c'&& string.valueOf(s1.get(fieldName))!= Null && string.valueOf(s2.get(fieldName))!= Null)
                                    {
                                                                                                                         
                                         Integer SumOfNoOfOpenPositions = Integer.valueOf(s1.get(fieldName)) + Integer.valueOf(s2.get(fieldName));
                                         system.debug('Total Open Positions:' + SumOfNoOfOpenPositions);
                                         s1.put(fieldName,SumOfNoOfOpenPositions); 
                                         
                                    }
                            } 
                         
                         
                        
                        }    
                                                                                  
                     }
                   
                                                                                                                        
               
                    //This section of code has been commented out following the discussion with Sudheer so as to ensure that
                    //Siebel_ID__c field value of Losing account is not copied to the Winning Account's Siebel_ID__c even if it is null
                    //Update the Losing Account
                    //update s2; 
                    
                    //Update the Winning Account 
                    update s1;
                    
                    //Reparent the Child accounts of the Losing account to point to the Winning account
                    List<Account> LSChildAccounts = new List<Account>();
                    List<Account> LSChildAccountsToUpdate = new List<Account>();
                    List<Account> LSLosingAccount = new List<Account>();
                    Map<Id,Account> LSLosingMap = new Map<Id,Account>([SELECT Id, ParentId FROM Account WHERE Id = :str_AccountToMergeId]);
                    
                    //Get a list of Ids of all accounts which have Losing Account's Id as the Parent Id
                    LSChildAccounts = [SELECT Id, ParentId FROM Account WHERE  ParentId = :str_AccountToMergeId];
                 
                    for(Account ca: LSChildAccounts) {
                    
                    //If the Child Account is not the Winning Account
                        If(string.valueOf(ca.Id).left(15) != string.valueOf(str_MasterAccountId).left(15))
                        {
                            system.debug('Within If Child Account' + string.valueOf(ca.Id));
                            system.debug('Within If Winning Account' + str_MasterAccountId);
                            ca.ParentId = str_MasterAccountId;
                            LSChildAccountsToUpdate.add(ca);
                        }
                        
                         //This code is being added to replicate the Siebel behaviour when Winning account was a child 
                         //of the Losing account and Losing account has a parent account   
                         //If the Child Account is the Winning Account and the Losing account has a parent account
                         //Code to Fix Defect# 27576 
                         
                         //LSLosingAccount = [SELECT Id, ParentId FROM Account WHERE Id = :str_AccountToMergeId];
                         for(Account la: LSLosingMap.values()) {
                            If((string.valueOf(ca.Id).left(15) == string.valueOf(str_MasterAccountId).left(15)) && (la.parentid != Null))
                            {
                              system.debug('Child Losing Account: ' + string.valueOf(s2.Id));
                              system.debug('Grandchild Winning Account: ' + ca.Id);
                              ca.ParentId = la.ParentId;
                              LSChildAccountsToUpdate.add(ca);
                            }
                         }      
                          
                        
                    }
                    
                    //Update Child Accounts of the losing Account with the Parent Id of the Surviving account
                    update LSChildAccountsToUpdate;

                    // Check for the Target_Account__C and dont carry the existing user__C
                    List<Target_Account__c> Target_Accounts = new List<Target_Account__c>();
                    List<Target_Account__c> LosingTarget_Accounts = new List<Target_Account__c>();
                    List<Target_Account__c> Target_AccountsDelete = new List<Target_Account__c>();
                    Map<String,Id> WinningUserMap = new Map<String,Id>();
                    String WinningID = WinningAccount.ID;
                    String LosingID = LosingAccount.ID;
                    System.debug('!!!Winning and loosing' +WinningID +LosingID);
                    Target_Accounts = [Select id, Account__c, User__c from Target_Account__c WHERE Account__c = :WinningID OR Account__c = :LosingID];
                    System.debug('!!!!!Target_Accounts ' +Target_Accounts);
                     if(Target_Accounts.size() > 0){
                        for(Target_Account__c TA: Target_Accounts){
                            if(TA.Account__c == WinningID){
                                WinningUserMap.put(TA.User__c, TA.Id);
                            }else if(TA.Account__c ==LosingID){
                                LosingTarget_Accounts.add(TA);
                            }
                        }
                        if(LosingTarget_Accounts.size()>0){
                            for(Target_Account__c WTA: LosingTarget_Accounts){
                                if(WinningUserMap.size()>0){
                                    if(WinningUserMap.containsKey(WTA.User__c))
                                    Target_AccountsDelete.add(WTA);
                                }
                            }
                        }
                        if(Target_AccountsDelete.size()>0){
                            try{
                                system.debug('!!!!Deleting Target Records ' +Target_AccountsDelete);
                                Delete Target_AccountsDelete;
                            }catch(System.Exception e){
                                System.debug('Target Account Delete Exception' +e);              
                            } 
                        }

                     } 
                    // Merge Losing account with the Winning Account                                    
                    merge WinningAccount LosingAccount;
              
                  }
               
               
               //If all the required Ids are not valid
               else
               {
                    system.debug('Both SF and Siebel account Ids of either Winning or Losing accounts are incorrect');
              
                    //Set MergeSuccessCode = 2 if one of the 2 Accounts (Winning/Losing) is not present in Salesforce                    
                    numMergeSuccessCode = 2;                                                    
                     
               }
               
         }   
                        
         catch(System.Exception e){
                                                 
                    // Process exception here and Dump to Log__c object                                        
                    errors.add(Core_Log.CustomException(e.getMessage()));
                    
                    //If there is an Exception,return MergeSuccessCode as 1                    
                    numMergeSuccessCode = 1;              
               } 
         
         //check if there are error records. 
         if(errors.size() > 0) { 
        
         //insert the error records.              
         Database.SaveResult[] MySaveResult = database.insert(errors,false);
         for(Database.SaveResult sr:MySaveResult){
           
              System.debug('Success:' +sr.isSuccess());
              if(!sr.isSuccess())
                  system.debug(sr.getErrors()[0].getMessage());
            }  
                     
         }
       
       //Return MergeSuccessCode indicating Success or Failure of Account Merge
       return numMergeSuccessCode;           
     }
            
}