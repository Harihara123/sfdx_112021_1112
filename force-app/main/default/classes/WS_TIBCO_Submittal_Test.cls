/******************************************************
 * Description      : Test Class for WS_TIBCO_Submittal 
 * @author          : Nausheen
 * @since           : September 05, 2012  
 ******************************************************/
 
 
 
@isTest
private class WS_TIBCO_Submittal_Test
        {/*
          static testMethod void WS_TIBCO_Submittal_method()
                  {
                      Integration_Endpoints__c custom_setting_obj1 = new Integration_Endpoints__c();
                      Integration_Certificate__c custom_setting_obj2=new Integration_Certificate__c();
                      
                      custom_setting_obj1.name='TIBCO.Submittals';
                      custom_setting_obj1.Endpoint__c= 'Certification';
                      insert custom_setting_obj1;
                      
                      custom_setting_obj2.name='Certificate';
                      custom_setting_obj2.Certificate_Name__c='Certification';
                      insert custom_setting_obj2;
                      // Set mock callout class 
                      Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
                      WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint obj1 = new  WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                      obj1.PullSubmittalLogSF('556034','120938','898766','filter','true','left_to_right');
                      //HttpResponse res = CalloutClass.getInfoFromExternalService();
                  } */
            private static testMethod void coverMethods(){
                  Integration_Endpoints__c custom_setting_obj1 = new Integration_Endpoints__c();
                  Integration_Certificate__c custom_setting_obj2=new Integration_Certificate__c();
                  
                  custom_setting_obj1.name='TIBCO.Submittals';
                  custom_setting_obj1.Endpoint__c= 'https://sf2allegisdcmSit.allegistest.com:20011/ESFAccount';
                  insert custom_setting_obj1;
                  
                  custom_setting_obj2.name='Certificate';
                  custom_setting_obj2.Certificate_Name__c='Sf2allegissi';
                  insert custom_setting_obj2;
                  
                  Test.setMock(WebServiceMock.class, new WebServiceMockImplCheck_Submittal());
                  test.startTest();
                      List<WS_TIBCO_SubmittalType.SubmittalLogData2_element> resp = new List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>();
                      WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint obj1 = new  WS_TIBCO_Submittal.SubmittalLogServiceSFHTTPEndpoint();
                      resp = obj1.PullSubmittalLogSF('556034','120938','898766','filter','true','left_to_right');
                      
                      //Unused classes..
                      WS_TIBCO_SubmittalType.SubmittalLogDataType newTestrowData = new WS_TIBCO_SubmittalType.SubmittalLogDataType ();
                      WS_TIBCO_SubmittalType.SubmittalLogSearchResponseType newTestrow = new WS_TIBCO_SubmittalType.SubmittalLogSearchResponseType ();
                      WS_TIBCO_SubmittalType.SubmittalLogSearchRequestType newTestrow1 = new WS_TIBCO_SubmittalType.SubmittalLogSearchRequestType ();
                      
                       AsyncWS_TIBCO_Submittal.AsyncSubmittalLogServiceSFHTTPEndpoint WSSubmittal = new AsyncWS_TIBCO_Submittal.AsyncSubmittalLogServiceSFHTTPEndpoint(); 
    
                       AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future WS_Resp_toProcess = new AsyncWS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2Future(); 
                                         
                       List<WS_TIBCO_SubmittalType.SubmittalLogData2_element> SubmittalsToProcess = new List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>(); 
                      
                      
                  test.stopTest();
                
            }
                          

        }