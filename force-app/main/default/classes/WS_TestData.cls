public class WS_TestData{

/************************Test Data for submittals ************************************/
    public static WS_TIBCO_SubmittalType.SubmittalLogData2_element getSubmittalRecord() {
        WS_TIBCO_SubmittalType.SubmittalLogData2_element retElement = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();      
        retElement.BillRate='123';
        retElement.CandidateFirstName='123';
        retElement.CandidateGUID='123';
        retElement.CandidateLastName='123';
        retElement.CandidateMiddleName='123';
        retElement.CandidateName='123';
        retElement.DL='123';
        retElement.DateSubmitted='2015-10-02T10:27:12-04:00';
        retElement.HRUserID='123';
        retElement.PartnerCandidateID='123';
        retElement.PartnerCandidateSysID='123';
        retElement.PartnerReqID='123';
        retElement.PartnerReqSubmittalID='123';
        retElement.PartnerReqSubmittalSysID='123';
        retElement.PartnerReqSysID='123';
        retElement.ReqGUID='123';
        retElement.ReqSubmittalGUID='123';
        retElement.SubmittalStatus='123';
        retElement.SubmittedBy='123';
        return retElement;  
    }
    
/************************Test Data for starts************************************/
  /*public static WS_TIBCO_StartType.ESFAccountRowType2 getStartsRecord() {
    WS_TIBCO_StartType.ESFAccountRowType2 retElement = new WS_TIBCO_StartType.ESFAccountRowType2();
    
    retElement.AccountID='1-10A-4075';
    retElement.BillRate=14.5;
    retElement.BurdenTotalSpread='55';
    retElement.CandidateFirstName='WEUNTSO';
    retElement.CandidateID='12898655';
    retElement.CandidateLastName='SAETEURN';
    retElement.CandidateStatus='Current';
    retElement.CustomerID=null;
    retElement.EndDate='2008-10-18T00:00:00-04:00'; 
    retElement.EsfID='5191';
    retElement.EsfVersionNumber='1';
    retElement.HomePhone=null;
    retElement.JobTitle='Recieving/Data Entry';
    retElement.MobilePhone=null;
    retElement.OpCo='ONS';
    retElement.PrimaryEmail='meyes717@hotmail.com';
    retElement.ReqID='1-8VBHF1';
    retElement.StartDate='2008-08-18T00:00:00-04:00';
    retElement.WorkPhone=null;
    retElement.WorksiteContactEmail=null;
    retElement.WorksiteContactFirstName='Ashutosh';
    retElement.WorksiteContactLastName='Verma';
    retElement.WorksiteContactPhone='9911160346';
    retElement.WorksiteContactTitle='Ops Manager';
    
    return retElement;
  }    */

}