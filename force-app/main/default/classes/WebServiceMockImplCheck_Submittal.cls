@isTest
                        
global class WebServiceMockImplCheck_Submittal implements WebServiceMock{
    // Implement this interface method
    global void doInvoke(
                           Object stub,
                           Object request,
                           Map<String, Object> response,
                           String endpoint,
                           String soapAction,
                           String requestName,
                           String responseNS,
                           String responseName,
                           String responseType) 
       {   
           list<WS_TIBCO_SubmittalType.SubmittalLogData2_element> lstSubmit = new  List<WS_TIBCO_SubmittalType.SubmittalLogData2_element>();
           WS_TIBCO_SubmittalType.SubmittalLogData2_element newRow = new WS_TIBCO_SubmittalType.SubmittalLogData2_element();
           newRow.ReqSubmittalGUID = 'Mock response';
           lstSubmit.add(newRow);
           WS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2 respElement = 
               new WS_TIBCO_SubmittalType.SubmittalLogSearchResponseType2();
               
             respElement.SubmittalLogData2 = lstSubmit;
             response.put('response_x', respElement); 
      }
}