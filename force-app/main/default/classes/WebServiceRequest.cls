public class WebServiceRequest implements WebServiceRequestInterface {
    private final Integer TIMEOUT = 100000;
    private final String JSON_CONTENT_TYPE = 'application/json';

    public WebServiceResponse putJson(String endpoint, String jsonBody, HttpRequest request) {
        request.setMethod('PUT');
        request.setHeader('Content-Type', this.JSON_CONTENT_TYPE);
        request.setTimeout(this.TIMEOUT);
        request.setEndpoint(endpoint);
        request.setBody(jsonBody);

        HttpResponse response = new HttpResponse(); 
        Http http = new Http();
        try {
            response = http.send(request);
            WebServiceResponse responseString = new WebServiceResponse(response.getStatus(), response.getStatusCode(), response.getBody());
            System.debug('put response'+JSON.serialize(responseString));
            return responseString;
            //System.assertEquals('kaavya','raj');
        } catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug('Callout response: ' + response.toString());
            return new WebServiceResponse(null, response.getStatusCode(), e.getMessage());
        }
    }

    public WebServiceResponse getJson(String endpoint, HttpRequest request) {
        request.setMethod('GET');
        request.setHeader('Content-Type', this.JSON_CONTENT_TYPE);
        request.setTimeout(this.TIMEOUT);
        request.setEndpoint(endpoint);

        HttpResponse response = new HttpResponse();
        Http http = new Http();
        try {
            response = http.send(request);
            WebServiceResponse responseString = new WebServiceResponse(response.getStatus(), response.getStatusCode(), response.getBody());
            System.debug('get response'+JSON.serialize(responseString));
            return responseString;
        } catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug('Callout response: ' + response.toString());
            return new WebServiceResponse(null, response.getStatusCode(), e.getMessage());
        }
    }
    public WebServiceResponse deleteJson(String endpoint, HttpRequest request){
    	request.setMethod('DELETE');
        request.setEndpoint(endpoint);
        System.debug('Inside of LinkedIn DeleteCall, calling ' + endpoint);
        HttpResponse response = new HttpResponse();

        Http http = new Http();
        try {
            response = http.send(request);
            System.debug('Raw Response from LinkedIn Delete ' + response);
            WebServiceResponse responseString = new WebServiceResponse(response.getStatus(), response.getStatusCode(), response.getBody());
            System.debug('get response'+JSON.serialize(responseString));
            return responseString;
        } catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug('Callout response: ' + response.toString());
            return new WebServiceResponse(null, response.getStatusCode(), e.getMessage());
        }
    }
}