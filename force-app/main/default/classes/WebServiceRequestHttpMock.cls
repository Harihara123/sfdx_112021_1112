global class WebServiceRequestHttpMock implements HttpCalloutMock{
    
	private LinkedInIntegrationHttpMock delegateMock;
    public WebServiceRequestHttpMock(LinkedInAuthHandlerForATS.AuthJSON response, Integer statusCode){
		 this.delegateMock = new LinkedInIntegrationHttpMock(response, statusCode);
	}   
    global HTTPResponse respond(HTTPRequest req) {
        string endpoint= req.getEndpoint();	
		if(endpoint.contains('callout:LinkedInRecruiterEndpoint/atsCandidates')){
			String contactId = endpoint.substringBetween('atsCandidateId=','&ids[0].dataProvider=');
			HttpResponse res = new HttpResponse();
			if (req.getMethod() == 'PUT') {
				// Create a fake response
				res.setHeader('Content-Type', 'application/json');
				res.setBody('{\"results\":{\"atsCandidateId='+contactId+'&dataProvider=ATS&integrationContext=urn%3Ali%3Aorganization%3A18405827\":{\"status\":204}},\"errors\":{}}');
				res.setStatusCode(200);		
			} else if(req.getMethod() == 'GET'){
				// Create a fake response
				res.setHeader('Content-Type', 'application/json');
				res.setBody('{\"statuses\":{},\"results\":{\"atsCandidateId='+contactId+'&dataProvider=ATS&integrationContext=urn%3Ali%3Aorganization%3A18405827\":{\"atsCreatedAt\":1522425326000,\"lastName\":\"Karanam\",\"firstName\":\"Kaavya\",\"addresses\":[],\"matchedMembers\":[\"urn:li:person:KbCAqJ7Gqf\",\"urn:li:person:dRH6psK35t\"],\"emailAddresses\":[\"kkaranam@allegisgroup.com\",\"kkaranam@acumensolutions.com\",\"test@test.com\"],\"atsLastModifiedAt\":1522704630000,\"externalProfileUrl\":\"https:\\/\\/www.linkedin.com\\/in\\/kaavya-karanam-37544b160\\/\",\"phoneNumbers\":[],\"atsCandidateId\":\"'+contactId+'\"}},\"errors\":{}}');
				res.setStatusCode(200);
			}else if(req.getMethod()=='DELETE'){
				// Create a fake response
				res.setHeader('Content-Type', 'application/json');
				res.setBody('{\"statuses\":{},\"results\":{\"atsCandidateId='+contactId+'&dataProvider=ATS&integrationContext=urn%3Ali%3Aorganization%3A18405827\":{\"atsCreatedAt\":1522425326000,\"lastName\":\"Karanam\",\"firstName\":\"Kaavya\",\"addresses\":[],\"matchedMembers\":[\"urn:li:person:KbCAqJ7Gqf\",\"urn:li:person:dRH6psK35t\"],\"emailAddresses\":[\"kkaranam@allegisgroup.com\",\"kkaranam@acumensolutions.com\",\"test@test.com\"],\"atsLastModifiedAt\":1522704630000,\"externalProfileUrl\":\"https:\\/\\/www.linkedin.com\\/in\\/kaavya-karanam-37544b160\\/\",\"phoneNumbers\":[],\"atsCandidateId\":\"'+contactId+'\"}},\"errors\":{}}');
				res.setStatusCode(200);
			}
			return res;
		} else {
			return delegateMock.respond(req);
		}	
    }
}