public interface WebServiceRequestInterface {
	WebServiceResponse getJson(String endpoint, HttpRequest request);
	WebServiceResponse putJson(String endpoint, String jsonBody, HttpRequest request);
	WebServiceResponse deleteJson(String endpoint, HttpRequest request);
}