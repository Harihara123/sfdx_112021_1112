public class WebServiceResponse  {
	public String Status { get; set; }
	public Integer StatusCode { get; set; }
	public String Response { get; set; }
  
	public WebServiceResponse(String status, Integer statusCode, String response) {
		this.Status = status;
		this.StatusCode = statusCode;
		this.Response = response;
	}
}