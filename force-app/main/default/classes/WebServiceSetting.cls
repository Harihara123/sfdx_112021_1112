/*
 * @author : Neel Kamal
 * @date : 04/07/2020
 * @desc : This class have methods related to web service. 
*/
public class WebServiceSetting {

	/*this method will return oAuth. On first call, get oAuth using callout and store the oAuth into Platform cache.
	  On subsequent call it first check the cache return oAuth without callout. 
	  Since Muelsoft oAuth is valid for only 1 hr so need to reset cache with new oAuth token.*/
	public static Map<String, String> getMulesoftOAuth(String customSettingName) {
		Datetime now = System.now();
		Datetime tokenExpTime = now.addMinutes(55); //Since Meuelsoft token will expire in 1hr.
		String oAuthToken = null;
		Map<String, String> tokenInfoMap = new Map<String, String> ();
		Map<String, String> oAuthCalloutMap = new Map<String, String> ();
		String tokenKey = customSettingName.deleteWhitespace(); //Custom setting name as the key of partition
		Cache.OrgPartition orgPartition = Cache.Org.getPartition('local.qaeRestCache');
		tokenInfoMap = (Map<String, String>) orgPartition.get(tokenKey);

		if (tokenInfoMap == null) { //This block will execute first time or if Session will automatically expire.
			tokenInfoMap = new Map<String, String> ();

			oAuthCalloutMap = requestOAuthToken(customSettingName);
			if (oAuthCalloutMap.size() > 0) {
				tokenInfoMap.put('oToken', oAuthCalloutMap.get('oAuthToken'));
				tokenInfoMap.put('endPointUrl', oAuthCalloutMap.get('endPointUrl'));
				tokenInfoMap.put('expTime', String.valueOfGmt(tokenExpTime));

				orgPartition.put(tokenKey, tokenInfoMap);
			}
		}
		else {
			Datetime sessExpTime = Datetime.valueOfGmt(tokenInfoMap.get('expTime'));
			if (Test.isRunningTest()) {
				sessExpTime = sessExpTime.addHours(- 1);
			}

			if (now > sessExpTime) { //Token need to reinitalize because of it is 55 min older.
				tokenInfoMap = new Map<String, String> ();

				oAuthCalloutMap = requestOAuthToken(customSettingName);
				if (oAuthCalloutMap.size() > 0) {
					tokenInfoMap.put('oToken', oAuthCalloutMap.get('oAuthToken'));
					tokenInfoMap.put('endPointUrl', oAuthCalloutMap.get('endPointUrl'));
					tokenInfoMap.put('expTime', String.valueOfGmt(tokenExpTime));

					orgPartition.remove(tokenKey);
					orgPartition.put(tokenKey, tokenInfoMap);
				}

			}
		}



		return tokenInfoMap;
	}

	//Callout to get oAuth token
	public static Map<String, String> requestOAuthToken(String customSettingName) {
		Map<String, String> oAuthMap = new Map<String, String> ();
		String respBody = '';
		HttpRequest req = new HttpRequest();
		HttpResponse resp = null;

		Mulesoft_OAuth_Settings__c settings = Mulesoft_OAuth_Settings__c.getValues(customSettingName);

		req.setEndpoint(settings.Token_URL__c);
		req.setMethod('POST');
		req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

		String payload = 'client_id=' + EncodingUtil.urlEncode(settings.ClientID__c, 'UTF-8') +
						'&client_secret=' + EncodingUtil.urlEncode(settings.Client_Secret__c, 'UTF-8') +
						'&resource=' + EncodingUtil.urlEncode(settings.Resource__c, 'UTF-8') +
						'&grant_type=client_credentials';
		req.setBody(payload);

		try {
			resp = new Http().send(req);
		} catch(CalloutException cExp) {
			ConnectedLog.LogException('WebServiceSetting', 'requestOAuthToken', cExp);
		}

		if (resp != null && resp.getStatusCode() == 200) {
			if (!Test.isRunningTest()) {
				respBody = resp.getBody();

			} else {
				respBody = '{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
				+ '"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
			}
			Map<String, String> respMap = (Map<String, String>) JSON.deserialize(respBody, Map<String, String>.class);
			oAuthMap.put('oAuthToken', respMap.get('access_token'));
			oAuthMap.put('endPointUrl', settings.Endpoint_URL__c);
		}
		else {
			//Logging the error
			String errorBody;
			try { //Logging as exception for better tracking in 'new relic'
				errorBody = resp.getBody();
				throw new AllegisException(errorBody);
			} catch(AllegisException aExp) {
				ConnectedLog.LogException('WebServiceSetting/oAuth/exception', 'WebServiceSetting', 'requestOAuthToken', aExp);
			}
		}

		return oAuthMap;
	}

	/*
	* @author : Neel Kamal
	* @Date : 05/03/2021
	* @Description : This method return the oAuth token. Almost all the service provider have 1 hr time for token expiration.
	*				 Saving the token in org cache for 1hr in place of callout for token for each time. It will save lots of callout for token.  
	*/
	public static Map<String, Object> oAuthTokenUsingMDT(String oAuthSettingName) {
		Map<String, Object> oAuthCalloutMap = new Map<String, Object> ();

		Cache.OrgPartition orgPartition = Cache.Org.getPartition('local.qaeRestCache');
		oAuthCalloutMap = (Map<String, Object>) orgPartition.get(oAuthSettingName);
		//This conversion because of the weired behaviour of Datetime.
		Datetime currGMTTime = Datetime.valueOfGmt(String.valueOfGmt(System.now()));
		Datetime sessExpTime;
	
		if (oAuthCalloutMap != null && oAuthCalloutMap.size() > 0) {			
			sessExpTime = (Datetime) oAuthCalloutMap.get('expTime');
			ConnectedLog.LogInformation('ws/token','WebServiceSetting', 'oAuthTokenUsingMDT', 'Toekn already exist. Current Time:::'+currGMTTime + ':::Expiry Time::'+sessExpTime);
		}

		if (oAuthCalloutMap == null || currGMTTime > sessExpTime) { //This block will execute first time or if Session will automatically expire.
			
			oAuthCalloutMap = new Map<String, Object> ();
			oAuthCalloutMap.putAll(oAuthTokenOnUsernamePassword(oAuthSettingName));

			//1 minute buffer time to regeneration of token again.
			Integer expireIn = Integer.valueOf(oAuthCalloutMap.get('expires_in')) - 60;
			DateTime expTime = currGMTTime.addSeconds(expireIn);
			oAuthCalloutMap.put('expTime', expTime);
			
			if (!oAuthCalloutMap.isEmpty()) {
				orgPartition.remove(oAuthSettingName);
			}
			orgPartition.put(oAuthSettingName, oAuthCalloutMap);			
			ConnectedLog.LogInformation('ws/token','WebServiceSetting', 'oAuthTokenUsingMDT', 'Fresh token generating. Current Time:::'+currGMTTime + ':::Expiry Time::'+sessExpTime);
		}
		return oAuthCalloutMap;
	}

	//Callout to get oAuth token
	public static Map<String, Object> oAuthTokenOnUsernamePassword(String oAuthSettingName) {
		Map<String, Object> respMap = new Map<String, Object> ();
		String respBody = '';
		HttpRequest req = new HttpRequest();
		HttpResponse resp = null;

		oAuth_Setting__mdt oAuthSetting = oAuth_Setting__mdt.getInstance(oAuthSettingName);
		//Mulesoft_OAuth_Settings__c settings = Mulesoft_OAuth_Settings__c.getValues(customSettingName);

		req.setEndpoint(oAuthSetting.Token_URL__c);
		req.setMethod(oAuthSetting.Service_Method__c);
		req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

		String reqBody = 'client_id=' + EncodingUtil.urlEncode(oAuthSetting.Client_Id__c, 'UTF-8') +
						'&client_secret=' + EncodingUtil.urlEncode(oAuthSetting.Client_Secret__c, 'UTF-8') +
						'&grant_type=' + oAuthSetting.Grant_Type__c;
		//In future if there will be different kind of authorization then add another grant type
		switch on oAuthSetting.Grant_Type__c {
			when 'password' {
				reqBody += '&username=' + EncodingUtil.urlEncode(oAuthSetting.Username__c, 'UTF-8') +
						   '&password=' + EncodingUtil.urlEncode(oAuthSetting.Password__c, 'UTF-8');
			}
		}
		req.setBody(reqBody);

		resp = new Http().send(req);


		if (resp != null && resp.getStatusCode() == 200) {
			if (!Test.isRunningTest()) {
				respBody = resp.getBody();
			} else {
				respBody = '{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
				+ '"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
			}
			respMap = (Map<String, Object>) JSON.deserialize(respBody, Map<String, String>.class);
			String timeout = String.valueOf(oAuthSetting.Timeout_In_Second__c);
			respMap.put('endPointUrl', oAuthSetting.Endpoint_URL__c); //This URL will be use for callout
			respMap.put('timeout', timeout); //This time for callout after getting token; 
			respMap.put('apievent_key',oAuthSetting.API_Event_Key__c);			
		}
		else {
			//Logging the error			
			throw new AllegisException('Status::' + resp.getStatus() + ':::Status Code::' + resp.getStatusCode() + ':::Error ::' + resp.getBody());
		}
		return respMap;
	}	
}