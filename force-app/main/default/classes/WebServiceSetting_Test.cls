@isTest
global class WebServiceSetting_Test  {
	public static testMethod void getMulesoftOAuthTest(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Job Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
       	 	Test.setMock(HttpCalloutMock.class, new OAuthMockResponse());
			Map<String, String> oTokenMap = WebServiceSetting.getMulesoftOAuth('Phenom Failed Job Application');
			System.debug(oTokenMap);
			System.assertEquals('eyJ0eXAiOiJKV1QiLCJhbGciOiJS', oTokenMap.get('oToken'));
        Test.stopTest();
    }

	public static testMethod void getMulesoftOAuthNegativeTest(){
        Mulesoft_OAuth_Settings__c serviceSettings = new Mulesoft_OAuth_Settings__c(name ='Phenom Failed Job Application',
                                                                            Endpoint_URL__c='https://api-tst.allegistest.com/v1/failedApplication',
                                                                            OAuth_Token__c='123456788889',
                                                                            ClientID__c ='abe1234',
                                                                         	Client_Secret__c ='hkbkheml',
                                                                            Resource__c ='f8548eb5-e772-4961-b132-0aa3dd012cba',
                                                                            Token_URL__c = 'https://api-tst.allegistest.com/v1/oauth/tokens?grant_type=client_credentials'
                                                                        );
        insert serviceSettings;
        Test.startTest();
       	 	Test.setMock(HttpCalloutMock.class, new OAuthMockResponseError());
			Map<String, Object> oTokenMap = WebServiceSetting.getMulesoftOAuth('Phenom Failed Job Application');
			System.assertNotEquals('eyJ0eXAiOiJKV1QiLCJhbGciOiJS', oTokenMap.get('oToken'));
        Test.stopTest();
    }

	
	global class OAuthMockResponse implements HttpCalloutMock{
		global HTTPResponse respond(HTTPRequest req) {
			String responseString ='{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            						+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
		
		
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody(responseString);
			res.setStatusCode(200);
			res.setStatus('OK');
			return res;
		}
	}

	global class OAuthMockResponseError implements HttpCalloutMock{
		global HTTPResponse respond(HTTPRequest req) {
			String responseString ='{"token_type":"Bearer","expires_in":"35","ext_expires_in":"35","expires_on":"15","not_before":"1585225330","resource":"f8548eb5-e772-4961-b132-0aa3dd012cba",'
            						+'"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJS"}';
		
		
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setBody(responseString);
			res.setStatusCode(401);
			res.setStatus('Error');
			return res;
		}
	}


	public static testMethod void oAuthTokenUsingMDTTest(){
        
        Test.startTest();
			String oAuthTokenMDTName = 'eQuestDraftNonProd';
			if (Utilities.getSFEnv() == 'prod') {
				oAuthTokenMDTName = 'eQuestDraftProd';
			}
       	 	Test.setMock(HttpCalloutMock.class, new OAuthMockResponse());
			Map<String, Object> oTokenMap = WebServiceSetting.oAuthTokenUsingMDT(oAuthTokenMDTName);
			System.assertEquals('eyJ0eXAiOiJKV1QiLCJhbGciOiJS', oTokenMap.get('access_token'));
			System.debug(oTokenMap);
        Test.stopTest();
    }

	

}