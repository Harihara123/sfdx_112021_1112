public class autoFollowHelper{
     public static void addSubscriptions(Map<string,set<string>> parentIds)
     {
            List<Log__c> logRecords                = new List<Log__c>();              // Collection to maintain log record information
            List<EntitySubscription> subscriptions = new List<EntitySubscription>();

            //loop over the subscription information
            for(string parent : parentIds.keyset())
            {
                //create subscription records
                for(string subscriber : parentIds.get(parent))
                {
                     EntitySubscription subscription = new EntitySubscription(ParentId = parent,SubscriberId = subscriber);
                     subscriptions.add(subscription);
                }
            }
            // insert the subscription records accordingly
            if(subscriptions.size() > 0)
            {
                  for(Database.SaveResult sr : Database.insert(subscriptions,false))
                  { 
                      if(!sr.isSuccess())
                      { 
                          // Add logic to insert a log record, invoke method Core_Log.logException(ex) to create the log record
                          logRecords.add(Core_Log.logException(sr.getErrors()[0]));
                      }
                  }
            }
         
          //insert log records accordingly
          if(logRecords.size() > 0)
            database.insert(logRecords,false);       
     }
     
     /*******************************************************************************
     Method to remove subscriptions
     ***********************************************************************************/
     public static void removeSubscriptions(Map<string,set<string>> parentIds)
     {
         List<EntitySubscription>subscriptionsToDelete = new List<EntitySubscription>();
         // query the subscription records for deletion
         try{
         for(EntitySubscription subscription : [Select parentId,SubscriberId from EntitySubscription where ParentId in :parentIds.keyset() limit 10000])
         {
             if(parentIds.get(subscription.parentId).contains(subscription.SubscriberId))
                 subscriptionsToDelete.add(subscription);
         }
        //delete the subscription records accordingly
        if(subscriptionsToDelete.size() > 0)
            database.delete(subscriptionsToDelete,false);
            }catch(Exception e){}
     }
}