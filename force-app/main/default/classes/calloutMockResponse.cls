@isTest
global class calloutMockResponse implements HttpCalloutMock{

String responseJSON;
String ContentType;
Integer statusCode;

  global calloutMockResponse(String responseJSON,String ContentType,Integer statusCode) {
       this.responseJSON = responseJSON;
	   this.ContentType = ContentType;
	   this.statusCode = statusCode;
    }
  global HTTPResponse respond(HTTPRequest req) {
       HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', ContentType);
        res.setBody(responseJSON);
        res.setStatusCode(statusCode);
        return res;
    }
}