/******************************************************************************************************************************
* Name        - clsRecalculation
* Description - Helper class with Methods used to Re-calculate the Total Number of Meetings, Total Number of Meals 
*               and Total Number of Filled Positions on Contact Record
*
* Modification Log :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Pratz                       03/26/2012             Created
********************************************************************************************************************************/

Public class clsRecalculation{
    
    final static set<string> ContRecSet =new set<string>{'012U0000000DWoyIAG','012U0000000DWoxIAG','012U0000000DWozIAG'};
   final static string Actrcdtypeid = string.valueof(label.Account_Client_RecordTypeID);

    /**********************STATIC VARIABLES NO LONGER REQUIRED, TRIGGERSTOPPER IS USED INSTEAD**********************************/
    //Static variables are local to the context of this method call therefore, this variable will be initialized as false   
    //at the beginning. 
    //public static boolean isRecalculationComplete = false;
     
    //public static boolean checkIfRecalculationIsComplete() {
    //    return isRecalculationComplete;
    //}
    
    //public static void setRecalculationIsComplete() {
    //    isRecalculationComplete = true;
    //}
    /**********************STATIC VARIABLES NO LONGER REQUIRED, TRIGGERSTOPPER IS USED INSTEAD**********************************/
    
    /***************************************************************************************************************************
    *Method to Re-calculate the Total Number of Meetings, Total Number of Meal, Total Number of Filled Positions fields on 
    *Winning Contact record after Merge OR Undelete
    ***************************************************************************************************************************/
    public static void Recalculate_TMeetings_TMeals_TNumFilledPos(Set<Id> conset)
    {       
        Decimal sumFilledPositions = 0;
        List<Contact> rtnlstContactsToBeUpdatedTMealsTMeetings = new List<Contact>();  
        List<Contact> lstContactsToBeUpdatedTFilledPositions = new List<Contact>();      
        system.debug('conset :' +conset );
        
        //Recalculate Total Number of Meetings & Total Number of Meals on every contact record passed as paramenter
        rtnlstContactsToBeUpdatedTMealsTMeetings = RecalculateTMealsTMeetings(conset);
        
        system.debug('rtnlstContactsToBeUpdatedTMealsTMeetings :'+rtnlstContactsToBeUpdatedTMealsTMeetings);
        
        //Hareesh-Added record type condition to by pass communities and ATS
         
        for(Contact c:[SELECT Id, (SELECT Id, Primary_Contact__c, Filled__c FROM Requisitions__r), Total_Filled__c FROM Contact WHERE recordtypeid IN :ContRecSet AND  Id IN :conset])         
        {
             //Loop over the associated Reqs and recalculate the Total_Filled__c field on Contact Record 
             for(Reqs__c req: c.Requisitions__r)           
              {                  
                     system.debug('Reqs__c req: '+req);                 
                     sumFilledPositions = sumFilledPositions + req.Filled__c;             
              } 
            //Added by Hareesh to remove null pointer exceptions
            
            if(c!=null){
              c.Total_Filled__c = sumFilledPositions;
              lstContactsToBeUpdatedTFilledPositions.add(c);
            }
        }     
                                             
         system.debug('lstContactsToBeUpdatedTFilledPositions :'+lstContactsToBeUpdatedTFilledPositions);
         //Check if this trigger is being executed 2nd time in the same context, then don't update the Contacts
         //if(!checkIfRecalculationIsComplete())
         
         //{   
             //setRecalculationIsComplete();
         //} 
         
                  
         //Set TriggerStopper static variables to True to prevent execution of "trg_ContactMerge_UpdateTMeetingsTMeals"
         //& "Contact_formerlyWithAccount" respectively
         TriggerStopper.isContactUpdated = True;
         TriggerStopper.updateContactstopper = True;   
         update rtnlstContactsToBeUpdatedTMealsTMeetings;
         update lstContactsToBeUpdatedTFilledPositions;
                 
    }
    
    /*************************************************************************************************************************************
    *Method to Re-calculate the Total Number of Meetings, Total Number of Meals fields on Contact record after Event Create/Update/Delete
    **************************************************************************************************************************************/    
    public static void Recalculate_TMeetings_TMeals(Set<Id> conset)
    {        
        
            List<Contact> rtnlstContactsToBeUpdatedTMealsTMeetings = new List<Contact>(); 
            List<Log__c> errorLogs = new List<Log__c>();           
            system.debug('conset :' +conset );
            
            //Recalculate Total Number of Meetings & Total Number of Meals on every contact record passed as paramenter
            rtnlstContactsToBeUpdatedTMealsTMeetings = RecalculateTMealsTMeetings(conset);       
            system.debug('rtnlstContactsToBeUpdatedTMealsTMeetings :'+rtnlstContactsToBeUpdatedTMealsTMeetings);
            
            system.debug('rtnlstContactsToBeUpdatedTMealsTMeetings :'+rtnlstContactsToBeUpdatedTMealsTMeetings);
            
            //Set TriggerStopper static variables to True to prevent execution of "trg_ContactMerge_UpdateTMeetingsTMeals"
            //& "Contact_formerlyWithAccount" respectively
            TriggerStopper.isContactUpdated = True;
            TriggerStopper.updateContactstopper = True;  
            if              
            (rtnlstContactsToBeUpdatedTMealsTMeetings.size() > 0)         
            {          
              for (database.saveResult result : database.update(rtnlstContactsToBeUpdatedTMealsTMeetings,false))         
              {         
                // insert the log record accordingly         
                if(!result.isSuccess() )         
                  {         
                   errorLogs.add(Core_Log.logException(result.getErrors() [0]));         
                  }         
               }         
             }                   
            if (errorLogs.size() >0)         
            database.insert(errorLogs,false);      
                
    }
    
    /*************************************************************************************************************************************
    *Method to Recalculate Total Meals & Total Meetings on Contact Records passed as Set of Contact Ids
    **************************************************************************************************************************************/
     public static List<Contact> RecalculateTMealsTMeetings(Set<Id> conset)
     {
        List<Contact> lstContactsToBeUpdatedTMealsTMeetings = new List<Contact>();   
         //Hareesh-Added extra record type condition to by pass communities and ATS

        for(Contact c:[SELECT Id, (SELECT Id, Description, Type, WhoId FROM Events WHERE(isDeleted=false)) FROM Contact WHERE recordtypeid IN :ContRecSet AND Id IN :conset ALL ROWS ])  // We decided to remove this for now : where Post_Meeting_Notes__c != null OR Post_Meeting_Notes__c != ''
        {
              Integer i = 0;
              Integer j = 0;              
              system.debug('c :' + c);
              
              //Loop over the associated events and recalcualte the Total_Meetings__c and Total_Meals__c field on Contact Record
              for(Event e : c.Events)
              {
                 system.debug('Event e: '+e);
                 system.debug('WhoId :' + e.WhoId);
                 system.debug('Description :' + e.Description);
                 if(string.valueOf((e.WhoId)).startsWith('003')&& e.Description!= null)   
                 {   
                 
                 //Hareesh-Added additional meeting types for CRMX-273
                     if(e.type == 'Meeting' || e.Type == 'Out of Office' || e.Type == 'Contractor Meeting'||e.type=='QBR' ||  e.type=='Trial Close/Presentation'|| e.type=='Skype/Video'|| e.type=='Performance Feedback')
                     i = i + 1;
                     else if(e.type == 'Lunch' || e.type == 'Breakfast' || e.type == 'Dinner'|| e.type == 'Meal') 
                     j = j + 1;  
                 }      
              }   
              system.debug('i : '+i); 
              system.debug('j : '+j);  
              c.Total_Meetings__c =i ;
              c.Total_Meals__c = j ; 
              // the Apex_Contest__C willset the flag to bypass the Contact Work_Mobile_Mailing validation
              c.Apex_Context__c = true;  
              System.debug('Total Meetings'+c.Total_Meetings__c);
              System.debug('Total Meals'+c.Total_Meals__c); 
                                
              lstContactsToBeUpdatedTMealsTMeetings.add(c);     
         }

         return lstContactsToBeUpdatedTMealsTMeetings;

     }   
    
}