public with sharing class creditAPP_Extensions {
    string queryString = '/'+Label.Credit_App_Record_Prefix+'nooverride=1';
    string cancelUrl;
    Map<string,string> opcoMapping = new Map<string,string>{'ONS' => 'Aerotek','TEK' => 'TEKsystems'};
    Map<string,string> reverseOpcoMapping = new Map<string,string>{'Aerotek' => 'ONS', 'TEKsystems' =>  'TEK'};
    
    public creditAPP_Extensions(ApexPages.StandardController controller) 
    {
           // Get the logged in user OPCO and Division 
            User loggedInUser   = [Select OPCO__c,Division__c,Office_Code__c,Region_Code__c from User where id = :userInfo.getUserId()];
            User_Organization__c org,region;
             
             // get the office information for the logged in user
             org = OfficeInformationHelper.getOffice(loggedInUser);
             
              // check for Region
            if(loggedInUser.OPCO__c != Null && loggedInUser.Region_Code__c != Null)
            {
                    string orgCompositeCode = loggedInUser.OPCO__c + '-'+ loggedInUser.Region_Code__c;
                    string orgCode = loggedInUser.OPCO__c + '-'+ loggedInUser.Region_Code__c;
                    if(opcoMapping.get(loggedInUser.OPCO__c) != Null)
                       orgCompositeCode = opcoMapping.get(loggedInUser.OPCO__c) + '-'+ loggedInUser.Region_Code__c;
                   
                    if(reverseOpcoMapping.get(loggedInUser.OPCO__c) != Null)
                       orgCode = reverseOpcoMapping.get(loggedInUser.OPCO__c) + '-'+ loggedInUser.Region_Code__c;
                   
                    region = OfficeInformationHelper.getRegion(loggedInUser.OPCO__c + '-' + loggedInUser.Region_Code__c);
                    
                    // check if the region is still blank 
                   if(region == Null)
                     region =  OfficeInformationHelper.getRegion(orgCompositeCode);
                    // check if the region is still blank
                   if(region == Null)
                     region = OfficeInformationHelper.getRegion(orgCode); 
            }      
            // check if the user opco needs any updation
              if(opcoMapping.get(loggedInUser.OPCO__c) != Null)
                 loggedInUser.OPCO__c = opcoMapping.get(loggedInUser.OPCO__c);
                
            
            //assign opco
            queryString = queryString + '&' + Label.Credit_App_Opco_Field_Id + '=' + encodeString(loggedInUser.OPCO__c);
            //assign Division
            queryString = queryString + '&' + Label.CREDIT_APP_USER_DIVISION_PICKLIST + '=' + encodeString(loggedInUser.Division__c);
            
           //get the return URL
           string returnURL = ApexPages.currentPage().getParameters().get('retURL');
           cancelUrl = returnURL;
           //remove special characters if any from the string
           returnURL = returnURL.replaceAll('/','');
           // check if the origination is from Account
           if(returnURL != Null && returnURL.Length() >= 15 && !returnURL.contains('?') && returnURL.subString(0,3) == Label.AccountIdPrefix)
           { 
               
               returnURL = returnURL.subString(0,15);
               
               // get the account Name
               Account acc = [select Name from Account where id = :returnURL];
               queryString = queryString + '&' + Label.CREDIT_APP_ACCOUNT_ID_LOOKUP +'_lkid=' + acc.Id;
               queryString = queryString + '&' + Label.CREDIT_APP_ACCOUNT_ID_LOOKUP +'=' + encodeString(acc.Name); 
           }
           // populate office
           if(org != Null)
           {
               queryString = queryString + '&' + Label.Credit_App_Office +'_lkid=' + org.Id;
               queryString = queryString + '&' + Label.Credit_App_Office +'=' + encodeString(org.Name);
           }
           //populate region
           if(region != Null)
           {
               queryString = queryString + '&' + Label.Credit_App_Region +'_lkid=' + region.Id;
               queryString = queryString + '&' + Label.Credit_App_Region +'=' + encodeString(region.Name);
           }
           // append return URL
           queryString = queryString + '&retURL=' + cancelUrl;
    }
        /************************************************************************
        Method for Redirection
        **************************************************************************/
        public pageReference redirect()
        {
           pageReference creditAppPage = new PageReference(queryString);
           creditAppPage.setRedirect(true);
           return creditAppPage;
        }
        /********************************************************************************
        Method to encode the query string parameters
        *********************************************************************************/
        private string encodeString(string paramValue)
        {
           if(paramValue != Null)
               paramValue = EncodingUtil.urlEncode(paramValue,'UTF-8');                
           return paramValue;
        }
}