public with sharing class globalLOVSearch {

    @AuraEnabled(cacheable=true)
    public static List<Global_LOV__c> globalLOVSearch(String whereCondition, String LOVName, String sortCondition, String searchString) {
        String key = searchString + '%';
        /*String queryString = 'SELECT Text_Value__c ' + 
                             'FROM Global_Lov__c ' +
                             'WHERE LOV_Name__c = \'' + LOVName + '\' AND Text_Value_2__c = \'' + whereCondition + 
                                                                 '\' AND Text_Value__c LIKE \'' + key +
                                                                 '\' ORDER BY ' + sortCondition;*/
        
        List<Global_LOV__c> values = new List<Global_LOV__c>();

        if(searchString != null && searchString.length() >= 3){
            values = [SELECT Id, Text_Value__c
                      FROM Global_LOV__c
                      WHERE LOV_Name__c = :LOVName AND Text_Value_2__c = :whereCondition
                                                   AND Text_Value__c LIKE :key
                                                   ORDER BY Number_Value__c DESC];
        }
        return values;
    }
}
