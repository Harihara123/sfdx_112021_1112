@isTest
private class globalLOVSearch_Test {
    static testmethod void testGlobalLOVSearch() {
        String searchString = 'TEST';
        String whereCondition = 'Test Global LOV';
        String lovName = 'ApexTestLOVS';

        List<Global_LOV__c> lovList = new List<Global_LOV__c>();
        Global_LOV__c lov1 = new Global_LOV__c (LOV_Name__c = lovName, Text_Value__c = 'TEST 1', 
                                                Source_System_Id__c='testlov1', Text_Value_2__c = whereCondition);

        Global_LOV__c lov2 = new Global_LOV__c (LOV_Name__c = lovName, Text_Value__c = 'TEST 2', 
                                                Source_System_Id__c='testlov2', Text_Value_2__c = whereCondition);
                                                
        Global_LOV__c lov3 = new Global_LOV__c (LOV_Name__c = lovName, Text_Value__c = 'TEST 3', 
                                                Source_System_Id__c='testlov3', Text_Value_2__c = whereCondition);

        lovList.add(lov1);
        lovList.add(lov2);
        lovList.add(lov3);
        insert lovList;

        List<Global_LOV__c> valuesReturned = new List<Global_LOV__c>();
        valuesReturned = globalLOVSearch.globalLOVSearch(whereCondition, lovName, ' ', searchString);
        System.assertEquals(valuesReturned.size(), 3);
        System.assertEquals(valuesReturned[0].Text_Value__c, 'TEST 1');

    }
}
