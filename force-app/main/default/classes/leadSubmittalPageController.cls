/*************************************************************************************************
Apex Class Name     :   leadSubmittalPageController
Version             :   1.0 
Created Date        :   15 FEB 2012
Function            :   This Controller controls the LeadSubmittalPage used by the internal Users to enter the 
                        Lead Information. It includes the logic of bringing the User's Office and division 
                        details and redirects the user to a ThankYou page once the Lead is submitted.                    
Modification Log :
-----------------------------------------------------------------------------
* Developer                  Date                   Description
* ----------------------------------------------------------------------------                 
* Dhruv Gupta                02/15/2012             Original Version
* D. Mazumdar                02/17/2012             Updated the logic for population of account
*                                                   managers based on the office
* Pavan                      05/16/2012             Refactored code
**************************************************************************************************/

public  class leadSubmittalPageController {
    
    /* Variable Declarations */
    ApexPages.StandardController stdController;
    public  string accountManager               {get;set;}
    public  string office                       {get;set;}
    public  List<SelectOption>  accountManagers {get;set;}
    private list<SelectOption>  offices;
    public  string  ErrorMsg                    {get; private set;}
    public  boolean bErrorMsg                   {get; private set;}
    Map<string,string> officeAssociationMap = new Map<string,string>();
    List<User_Organization__c> officeList = new List<User_Organization__c>();
    
    /* Default Constructor */
    public leadSubmittalPageController(ApexPages.StandardController controller) {
        stdController = controller;
        //retrieve offices from the org
        retrieveOfficeInformation();                         
        populateAccountManagers();
        
    }
    /*
        @name           retrieveOfficeInformation
        @description    method to get the offices that has office code in the org
    */
    private void retrieveOfficeInformation()
    {
           for(User_Organization__c org : [select Name,OpCo_Code__c,Office_Code__c,Office_Name__c,Office_Composition_Id__c from User_Organization__c where Office_Code__c != Null and Office_Composition_Id__c != Null and Active__c = true order by OpCo_Code__c,Office_Code__c,Office_Name__c limit 999])
           {
               officeList.add(org);
               officeAssociationMap.put(org.Office_Composition_Id__c,org.Id);
           }
    }
    
    public leadSubmittalPageController(){           
    }
    
    /*
        @name           leadSave
        @description    method to create a new lead and redirect the user to the thank you page
    */
    public PageReference leadSave()
    {
        pageReference redirect;
        Lead le                   = ((Lead) stdcontroller.getRecord());
        if(leadValidated(le))
        {
            le.LeadSource             = 'Recruiter';
            le.Organization_Office__c = officeAssociationMap.get(office);
            le.Account_Manager__c     = accountManager;
            le.OwnerId                = accountManager;
            redirect = new pageReference('/apex/thankYouPage');
            stdController.save();
        }
            return redirect;
    }
    
    /*
        @name           validateLead
        @description    validate that all the required lead fields are entered properly
        @returns        boolean true|false
    */  
    private boolean leadValidated(Lead lead){
        boolean isValidated = true;
        string errorMsg = 'Please enter the lead ';
        integer len = errorMsg.length();             
        //validate lead last name and company name
        if(lead.LastName == Null)
        {
           errorMsg = errorMsg + '\'\'Last Name\'\'';
        }
        if(lead.Company == Null)
        {
            if(len < errorMsg.length())
                errorMsg = errorMsg + ',\'\'Company\'\'';
            else
                errorMsg = errorMsg + '\'\'Company\'\'';
   
        }
        //  validate that Office and Account manager is entered
        if(office == Null)
        {
            if(len < errorMsg.length())
                errorMsg = errorMsg + ',\'\'Office\'\'';
            else
                errorMsg = errorMsg + '\'\'Office\'\'';
      
        }
        if(accountManager == Null)
        {         
            if(len < errorMsg.length())
                errorMsg = errorMsg + ',\'\'Account Manager\'\'';
            else
                errorMsg = errorMsg + '\'\'Account Manager\'\'';
      
        } 
        if(lead.LeadSource_Description__c == Null || (lead.LeadSource_Description__c != Null && string.valueOf(lead.LeadSource_Description__c).Trim().Length() == 0))
        {
            if(len < errorMsg.length())
                errorMsg = errorMsg + ',\'\'Recruiter UserName\'\'';
            else
                errorMsg = errorMsg + '\'\'Recruiter UserName\'\'';
    
        }
        if(len < errorMsg.length())
        {
            errorMsg = errorMsg + ' for submission.'; 
            Apexpages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,errorMsg));
            isValidated = false;
        }              
        return isValidated;
    } 
    
         
    /*
        @name           getOffices
        @description    creates a list of office 
        @returns        List of offices
    */
    public List<SelectOption> getOfficeList(){
        
        if(offices == Null)
        {
            offices = new List<SelectOption> ();
            offices.add(new SelectOption('','--None--'));
            //loop over the office Map
            for(User_Organization__c ofc : officeList)
            {
                   offices.add(new SelectOption(ofc.Office_Composition_Id__c,ofc.Name));
            }
        }    
        return offices; 
    }
    
    /*
        @name           populateAccountManagers
        @description    creates a list of account managers based on the selected office
    */
    public void populateAccountManagers(){      
        String oUserNameAndRole = '';
        accountManagers = new List<SelectOption>();
        if(office != Null)
        {     
                string officeCode = office.subString(office.indexOf('-') + 1);
                for(User oUser: [Select     Name, UserRole.Name from User WHERE Office_Code__c = :officeCode 
                                 ORDER BY   Name
                                 LIMIT 20])
                {
                    if(oUser.UserRole.Name <> Null)              
                        oUserNameAndRole = oUser.Name + '(' + oUser.UserRole.Name+')';
                    else 
                        oUserNameAndRole = oUser.Name;          
                    accountManagers.add(new SelectOption(oUser.Id, oUserNameAndRole));
                }  
        }             
    }
    
}