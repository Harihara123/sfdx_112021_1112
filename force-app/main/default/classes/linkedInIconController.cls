public with sharing class linkedInIconController  {

  @AuraEnabled
  public static void updateTalentLinkedinStatus(String contactId) {                
	 Boolean isSandbox = LinkedInUtility.isSandbox(); 
     LinkedInEnvironment envVariable = LinkedInUtility.getEnvironmentName(isSandbox); 
     LinkedInIntegration__mdt configItems = LinkedInUtility.getLinkedInConfiguration(envVariable);	 
     String linkedinKey = LinkedInCandidateSyncFunctions.getLinkedinKey(contactId,configItems.IntegrationContext__c);
     LinkedInIntegrationWebserviceCallout.getTalentStatusFromLinkedin(linkedinKey,isSandbox);

  }

           

}