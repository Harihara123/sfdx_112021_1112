public class oAuth_Connector {
    public string token_url;
    public string client_Id;
    public string client_Secret;
    public string access_Token;
    public DateTime expires_In;
	public oAuth_Connector getAccessToken(oAuth_Connector settings){
        HttpRequest req = new HttpRequest();
		req.setEndpoint(settings.token_url);
		req.setMethod('POST');
        String authHeader = EncodingUtil.base64Encode(Blob.valueOf(settings.client_Id + ':' + settings.client_Secret));
        System.debug(authHeader);
        req.setHeader('Authorization', authHeader);        
        req.setHeader('Content-Length', '0');
        HttpResponse resp = new Http().send(req);
        JSONParser parser = JSON.createParser(resp.getBody());
        while (null != parser.nextToken()) {
            if (parser.getCurrentName() == 'accessToken') {
                parser.nextToken();
                settings.access_Token = parser.getText();
            } else if (parser.getCurrentName() == 'expiresIn') {
                parser.nextToken();
                Integer seconds = Integer.valueOf(parser.getText());
                Datetime newExpiry = System.now().addSeconds(Integer.valueOf(seconds * 0.75));
                settings.expires_In = newExpiry;
            }
        }
         return settings;
    }
}