global class scheduleBatchEDUserLicense implements Schedulable {
    
    global void execute(SchedulableContext sc){
        
        String q=system.label.Group_TEK;
        Permissionset pset = [SELECT Id FROM PermissionSet WHERE Name=:system.label.ED_user];
        Batch_EDUserLicense batch_ed = new Batch_EDUserLicense(q,'00G1p000002GK8M',pset.Id);
        Id BatchId=Database.executeBatch(batch_ed);
        
    }

}