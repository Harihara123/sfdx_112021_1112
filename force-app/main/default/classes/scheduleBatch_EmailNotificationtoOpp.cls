global class scheduleBatch_EmailNotificationtoOpp implements Schedulable{

     global void execute(SchedulableContext sc) {
         
        Batch_EmailNotificationtoOppOwner b = new Batch_EmailNotificationtoOppOwner(); 
        
        database.executebatch(b,200);
     }
  }