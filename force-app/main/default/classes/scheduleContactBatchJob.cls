global class scheduleContactBatchJob implements Schedulable{
   global void execute(SchedulableContext sc) {
      AccountSearchControlBatch batchJob = new AccountSearchControlBatch('Select Account_Search__c, Account.Name from Contact where recordtypeid in (\'012U0000000DWoyIAG\',\'012U0000000DWoxIAG\',\'012U0000000DWozIAG\')'); // updated query to query only CRM record types -Nidish. 
      database.executebatch(batchJob,100);
   }
}