global class scheduleDeleteAccountsBatchJob implements Schedulable {

	global void execute(SchedulableContext sc) {
		DeleteAccountsWithNoContactBatch batchJob = new DeleteAccountsWithNoContactBatch();
		database.executebatch(batchJob,200);
	}
}