public class searchResponseParser {
    public class Hits_Z {
        public Integer total {get;set;} 
        public Double max_score {get;set;} 
        public List<Hits> hits {get;set;} 
        public Hits_Z(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'total') {
                            total = parser.getIntegerValue();
                        } else if (text == 'max_score') {
                            max_score = parser.getDoubleValue();
                        } else if (text == 'hits') {
                            hits = arrayOfHits(parser);
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Response {
        public Integer took {get;set;} 
        public Boolean timed_out {get;set;} 
        public Hits_Z hits {get;set;} 

        public Response(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'took') {
                            took = parser.getIntegerValue();
                        } else if (text == 'timed_out') {
                            timed_out = parser.getBooleanValue();
                        } else if (text == 'hits') {
                            hits = new Hits_Z(parser);
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public class Hits {
        public String x_index {get;set;} // in json: _index
        public String x_type {get;set;} // in json: _type
        public String x_id {get;set;} // in json: _id
        public Double x_score {get;set;} // in json: _score
        public Source x_source {get;set;} // in json: _source
        public Highlight highlight {get;set;} 

        public Hits(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == '_index') {
                            x_index = parser.getText();
                        } else if (text == '_type') {
                            x_type = parser.getText();
                        } else if (text == '_id') {
                            x_id = parser.getText();
                        } else if (text == '_score') {
                            x_score = parser.getDoubleValue();
                        } else if (text == '_source') {
                            x_source = new Source(parser);
                        } else if (text == 'highlight') {
                            highlight = new Highlight(parser);
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Highlight {
        public List<String> skills {get;set;} 

        public Highlight(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'skills') {
                            skills = arrayOfString(parser);
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Header header {get;set;} 
    public Response response {get;set;} 

    public searchResponseParser(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'header') {
                        header = new Header(parser);
                    } else if (text == 'response') {
                    system.debug('---------');
                        response = new Response(parser);
                    } else {
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Header {
        public Integer status {get;set;} 
        public Request request {get;set;} 

        public Header(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'status') {
                            status = parser.getIntegerValue();
                        } else if (text == 'request') {
                            request = new Request(parser);
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Request {
        public String path {get;set;} 

        public Request(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'path') {
                            path = parser.getText();
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public class Source {
        
        public String contact_id {get;set;} 
        

        public Source(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'contact_id') {
                            contact_id = parser.getText();
                        } else {
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static searchResponseParser parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new searchResponseParser(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    


    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }
    private static List<Hits> arrayOfHits(System.JSONParser p) {
        List<Hits> res = new List<Hits>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Hits(p));
        }
        return res;
    }

}