import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from "lightning/uiRecordApi";

import ACCOUNTID_FIELD from "@salesforce/schema/Contact.AccountId";

export default class ATS_DisplayMultiSelectLookup extends LightningElement {
    
    @api recordId;
    @track selectedItemsToDisplay = '';     //to display items in comma-delimited way
    @track values = [];                     //stores the labels in this array
    @track isItemExists = false;            //flag to check if message can be displayed
    @track extrafilter;                     //to query the records based on an extra filter field, for e.g: AccountId 
    @track dynamicField;                    // Field value to be retrieve based on recordId
    
    //executes on page load
    connectedCallback(){
        console.log('11--->');
        this.buildCustomQuery();
    }
    @wire(getRecord, {
        recordId: "$recordId",
        fields: '$dynamicField'
      })
      wireContacts({error, data}){
          if(data){
            console.log('3--->' + JSON.stringify(data));
            // get AccountId values if the component in tagged on Contact Record
            if(this.dynamicField == 'Contact.AccountId'){
                let accountId = getFieldValue(data, ACCOUNTID_FIELD)
                if(accountId.substring(0,3) == '001'){
                    this.extrafilter = ' AND AccountId = \''+accountId+'\'';
                }
            }
            
          }
          if(error){
            
          }
      }

    //this method completes the dynamic query from the Controller by checking for the extrafilter
    buildCustomQuery(){
        //if the current record page is Account, retrieves all Contact records related to that current Account record
        if(this.recordId != undefined) {
            if(this.recordId.substring(0,3) == '001'){
                this.extrafilter = ' AND AccountId = \''+this.recordId+'\'';
                this.dynamicField = 'Account.Id';
            }
            //if the current record page is Contact, retrieves all Contact records related to that Contact's Account if it exists
            else if(this.recordId.substring(0,3) == '003'){
                this.dynamicField = 'Contact.AccountId';
            }
        } else {
            this.extrafilter = '';
        }
        
    }
    //captures the retrieve event propagated from lookup component
    selectItemEventHandler(event){
        let args = JSON.parse(JSON.stringify(event.detail.arrItems));
        this.displayItem(args);        
    }

    //captures the remove event propagated from lookup component
    deleteItemEventHandler(event){
        let args = JSON.parse(JSON.stringify(event.detail.arrItems));
        this.displayItem(args);
    }

    //displays the items in comma-delimited way
    displayItem(args){
        this.values = []; //initialize first
        args.map(element=>{
            this.values.push(element.label);
        });

        this.isItemExists = (args.length>0);
        this.selectedItemsToDisplay = this.values.join(', ');
    }
}