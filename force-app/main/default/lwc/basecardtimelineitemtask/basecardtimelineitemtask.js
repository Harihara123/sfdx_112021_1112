import { LightningElement,track, api} from 'lwc';

import ATS_SUBJECT from '@salesforce/label/c.ATS_SUBJECT';
import ATS_COMMENT from '@salesforce/label/c.ATS_COMMENT';
import ATS_PRE_NOTES from '@salesforce/label/c.ATS_PRE_NOTES';
import ATS_POST_NOTES from '@salesforce/label/c.ATS_POST_NOTES';

import ATS_CLIENT_COMMENT from '@salesforce/label/c.ATS_CLIENT_COMMENT';
import ATS_CANDIDATE_COMMENT from '@salesforce/label/c.ATS_CANDIDATE_COMMENT';

import ATS_INTERVIEW_DATE from '@salesforce/label/c.ATS_INTERVIEW_DATE';
import Interviewers from '@salesforce/label/c.Interviewers';
import Interview_Status from '@salesforce/label/c.Interview_Status';

import ATS_NOT_PROCEEDING_REASON from '@salesforce/label/c.ATS_NOT_PROCEEDING_REASON';

import ATS_COMMENTS from '@salesforce/label/c.ATS_COMMENTS';
import view_emaildt from '@salesforce/label/c.view_emaildt';

import ATS_TALENT_CREATED_CBP from '@salesforce/label/c.ATS_TALENT_CREATED_CBP';
import ATS_EDIT from '@salesforce/label/c.ATS_EDIT';
import ATS_DELETE from '@salesforce/label/c.ATS_DELETE';




export default class Basecardtimelineitemtask extends LightningElement
 {
   
    @api recordPosition;
    @api taskItem;
    @api enableActionMenu;
    @api recType;
    @track showActionPopOver = true;
    @api openmodel;
    @api currentUser;
    //@track isEventRecordType;
    //@track isTaskRecordType;
    @api enableCondensedLabels;
    
    label = {
        ATS_SUBJECT, 
        ATS_COMMENT,
        ATS_PRE_NOTES,
        ATS_POST_NOTES,   
        ATS_CLIENT_COMMENT,
        ATS_CANDIDATE_COMMENT,
        ATS_INTERVIEW_DATE,
        Interviewers,
        Interview_Status,
        ATS_NOT_PROCEEDING_REASON,
        ATS_COMMENTS,
        ATS_TALENT_CREATED_CBP,
		ATS_EDIT,
		ATS_DELETE,
		view_emaildt

    };
    /*
    @api 
    get taskItem(){
        return this.taskItem;
    }
    set taskItem(value){
        //this.taskItem = value;
    }*/
    /*
    @api
    get recType(){
        return this.recordType;
    }
    set recType(value){        
       // this.isEventRecordType = (value != undefined && (value==='Event'));
       // this.isTaskRecordType = (value != undefined && (value==='Task'));
       // this.recordType = value;
    }*/
	//console.log(taskItem.RecordId);
    

    get isEventRecordType(){
        return (this.recType != undefined && (this.recType==='Event'));
    }
    get isTaskRecordType(){
        return (this.recType != undefined && (this.recType==='Task'));
    }

    get getDateValue(){
        if(this.isEventRecordType){
            return this.taskItem.EventStartDateTime;
        }else{
            return this.taskItem.taskStartDateTime;
        }
    }
    get iconName(){
        if(this.taskItem != undefined && this.taskItem.sprite != undefined && this.taskItem.icon != undefined){
            return this.taskItem.sprite + ':' + this.taskItem.icon;            
        }else{
            return '';
        }
    }

    get assingedTo(){
        return this.taskItem.Assigned + ' >';
    }

    get  preMeetingNotesSize(){
        return (this.taskItem == undefined && this.taskItem.PreMeetingNotes != undefined ? this.taskItem.PreMeetingNotes.length : 0);
    }

    get  postMeetingNotesSize(){
        return (this.taskItem == undefined && this.taskItem.PostMeetingNotes != undefined ? this.taskItem.PostMeetingNotes.length : 0);
    }

    get clientComments(){
        if(this.taskItem != undefined && this.taskItem.SubmittalDescription != undefined && this.taskItem.SubmittalDescription.length >0){
            try{
                var description = this.taskItem.SubmittalDescription;
                description = description.replace(/(?:\r\n)/g, '<br>');
                var submittalDesc = JSON.parse(description);            
                return submittalDesc.ClientComments;
            }
            catch(error){
                return '';
            }
        }else{
            return '';
        }
    }
    get clientCommentsLength(){
        if(this.taskItem == undefined){return '';}
        if(this.taskItem.SubmittalDescription != undefined && this.taskItem.SubmittalDescription.length >0){ 
            try{
                var description = this.taskItem.SubmittalDescription;
                var description = description.replace(/(?:\r\n)/g, '<br>');
                var submittalDesc = JSON.parse(description);
                return (submittalDesc.ClientComments != undefined ? submittalDesc.ClientComments.length :0);
            }          
            catch(error){                
                return 0;
            }
        }else{
            return 0;
        }
    }

    get commentsLength(){
        return (this.taskItem == undefined && this.taskItem.Detail != undefined ? this.taskItem.Detail.length : 0);
    }

    get candidateComments(){
        if(this.taskItem == undefined){return '';}
        if(this.taskItem.SubmittalDescription != undefined && this.taskItem.SubmittalDescription.length >0){
            try{
                var description = this.taskItem.SubmittalDescription;
                description = description.replace(/(?:\r\n)/g, '<br>');
                var submittalDesc = JSON.parse(description);            
                return submittalDesc.CandidateComments;
            }catch(error){
                return '';    
            }
        }else{
            return '';
        }
    }
    get candidateCommentsLength(){
        if(this.taskItem == undefined){return 0;}
        if(this.taskItem.SubmittalDescription != undefined && this.taskItem.SubmittalDescription.length >0){
            try{
                var description = this.taskItem.SubmittalDescription.replace(/(?:\r\n)/g, '<br>');
                var submittalDesc = JSON.parse(description);
                return (submittalDesc.CandidateComments != undefined ? submittalDesc.CandidateComments.length :0);
            }catch(error){
                return 0;
            }         
        }else{
            return 0;
        }
    }
    
    get subjectLength(){
        return (this.taskItem !=undefined && this.taskItem.Subject != undefined ? this.taskItem.Subject.length : 0);
    }
    get isDeletable(){

        return (this.taskItem !=undefined && this.taskItem.showDeleteItem != undefined && this.taskItem.showDeleteItem === true);
    }

    get enableActions(){
    
        if (!this.enableActionMenu.includes("false")) {
            if (this.taskItem !== undefined && this.taskItem.ActivityType != undefined) {
                // this.currentUser.Profile.Name !== 'System Administrator'
                let currentUser1 = (((this.currentUser || {}).Profile || '').Name || ''),
                    currentUser2 = ((this.currentUser || {}).profileName || '');
                let allowActions = !((this.currentUser !== undefined && (currentUser1 !== 'System Administrator' || currentUser2 !== 'System Administrator')) &&
                    (this.taskItem.ActivityType === 'Finish' || this.taskItem.ActivityType === 'G2' || this.taskItem.ActivityType === 'Reference Check' || this.taskItem.Subject === 'New Talent Created Through Career Builder Process') &&
                    (this.taskItem.Status === 'Completed'));
                return allowActions && (this.taskItem.ActivityType !== 'Profile Updated');
            }
            return false
        }
        
        return false;
       
    }
    get isSubmittalEvent(){
        return (this.taskItem != undefined && this.taskItem.SubmittalEvent);
    }
    get isNotProceedingActivity(){
        return (this.taskItem != undefined && this.taskItem.ActivityType === 'Not Proceeding');
    }
    get isInterviewing(){
        return  (this.taskItem != undefined && this.taskItem.ActivityType==='Interviewing');
    }
    get notProceedingLength(){
        return (this.taskItem != undefined && this.taskItem.NotProceedingReason != undefined) ?(this.taskItem.NotProceedingReason.length):0;
    }
    fireEditEvent(){
        const oneventeditevvent = new CustomEvent('eventedit', {                        
            detail : {"itemID":this.taskItem.RecordId,"itemType": (this.recType==="Task" ?"task":"event")}
        });
        this.dispatchEvent(oneventeditevvent);  
        this.showActionPopOver = false;      
    }

    showActionItems(){
        this.showActionPopOver = !this.showActionPopOver;
    }

    handleActionsMouseOver(){
        this.showActionPopOver = true;
    }
    handleActionsMouseOut(){
        this.showActionPopOver = false;
    }
    showConfirmationMoal(){
        this.openmodel = true;
    }
    closeModal(evt){
        this.openmodel = false;

		///S-190757 - Make delete button in the actions dropdown of the callsheet JAWS compatible - focus back actions dropdown when the confirmation modal is cancelled/closed
        const actionsmenu=this.template.querySelector('[data-id="actionsmenu"]');
		actionsmenu.focus();
    }
    confirmmodalsave(evt){
        this.openmodel = false;
        const ondeleterecordevt = new CustomEvent('deleterecordevt', {                        
            detail : {"recordId":this.taskItem.RecordId}
        });
        this.dispatchEvent(ondeleterecordevt);          
    }
}