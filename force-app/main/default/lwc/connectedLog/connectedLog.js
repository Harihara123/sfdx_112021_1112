import { LightningElement, api } from 'lwc'
import {ConnectedLogApi} from 'c/connectedLogUtils';

const connectedLogAPI = new ConnectedLogApi();

export default class ConnectedLog extends LightningElement {
    @api reference
    @api getAPI() {
        return connectedLogAPI;
    }
	@api getLOGAPI() {
		console.log('Inside getLOGAPI-----------------');
		return connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/pagerefresh', Method: 'connectedCallback', Module: 'talentLanding'});
    }
}