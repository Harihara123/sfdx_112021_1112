import {getUserDetails, sendLog, getRefCmp, getRecordId, constructInfoLog, constructErrLog, sanitizeLogItem, constructSource} from './connectedLogUtils'

export default class ConnectedLogApi {
    constructor() {
        if (ConnectedLogApi.instance) {
            return ConnectedLogApi.instance
        }

        this.currentUser = null;
        this.logs = [];
        this.inProgress = false
        this.queue = []
        /*this.t0 = 0;
        this.tf = 0;*/
        this.sourceObj = constructSource();       

        ConnectedLogApi.instance = this
    }
    logInfo = async (reference, {RecordId=null, LogTypeId=null, AdditionalData={}, ...obj}) => {
        const userDetails = this.currentUser || await this.getUserDetails();
        //const t0 = performance.now()

        const ClassName = getRefCmp(reference)
        const recordId = RecordId || getRecordId()
        // AdditionalData.Region is added for S-185932. Added by Rajib Maity
        if (userDetails) {
            AdditionalData.OpCo = userDetails.usr.OPCO__c;
            AdditionalData.Region = userDetails.usr.Region__c || null;
            AdditionalData.Division = userDetails.usr.Division__c || null;
        }
        
        const logInfoObject = constructInfoLog({
            ...obj,
            ClassName, 
            RecordId: recordId, 
            LogTypeId,
            AdditionalData,            
        })
        const sanitizedObject = sanitizeLogItem(logInfoObject);

        //const tf = performance.now()
        //sanitizedObject.Duration = tf-t0;
        this.addToLogs(sanitizedObject);
        this.publishLogs()

        //console.log(sanitizedObject, JSON.parse(JSON.stringify(this.logs)))        
    }
    logError = async (reference, {RecordId=null, LogTypeId=null, AdditionalData={}, ...obj}) => {
        const userDetails = this.currentUser || await this.getUserDetails();
        //const t0 = performance.now()

        const ClassName = getRefCmp(reference)
        const recordId = RecordId || getRecordId()
        // AdditionalData.Region is added for S-185932. Added by Rajib Maity
        if (userDetails) {
            AdditionalData.OpCo = userDetails.usr.OPCO__c;
            AdditionalData.Region = userDetails.usr.Region__c || null;
            AdditionalData.Division = userDetails.usr.Division__c || null;
        }
        
        const logErrObject = constructErrLog({
            ...obj,
            ClassName, 
            RecordId: recordId, 
            LogTypeId,
            AdditionalData,            
        })
        const sanitizedObject = sanitizeLogItem(logErrObject);

        //const tf = performance.now()
        //sanitizedObject.Duration = tf-t0;
        this.addToLogs(sanitizedObject);
        this.publishLogs()

        //console.log(sanitizedObject, JSON.parse(JSON.stringify(this.logs)))        
    }
    async getUserDetails() {
        const details = await getUserDetails();
        this.currentUser = details;
        return details
    }
    addToLogs(log) {
        if (this.inProgress) {
            this.queue = [...this.queue, log]
        } else {            
            this.logs = [...this.logs, log]
        }
    }
    constructPayload() {
        return {
            LogHeader: this.sourceObj,
            logs: [...this.logs]
        }
    }
    publishLogs = async () => {
        if (!this.inProgress) {
            this.inProgress = true
            const payload = this.constructPayload()
            this.logs = []
            this.queue = []
            try {
                const response = await sendLog(payload);            
                if (response.status === 'success') {
                    this.inProgress = false
                    if (this.queue.length > 0) {
                        this.logs = [...this.queue]                        
                        this.publishLogs()
                    }
                }
            } catch (err) {
                console.log(err)
                this.inProgress = false
                if (this.queue.length > 0) {
                    this.logs = [...this.queue]                    
                    this.publishLogs()
                }
            }
        }
    }
}