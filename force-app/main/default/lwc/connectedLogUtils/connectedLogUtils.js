import {FULL_URL, LOG_ITEM_SCHEMA} from './vars'
import ConnectedLogApi from './connectedLogApi'
import userId from '@salesforce/user/Id'
import getCurrentUser from '@salesforce/apex/BaseController.getUserInfo';
import {getEnv} from 'c/lwcUtilities';

export {ConnectedLogApi}

export const getUserDetails = () => {
    return new Promise((resolve, reject) => {
        getCurrentUser().then(response => {            
            resolve(response || null)
        }).catch(err => {
            reject(err);
        })
    })
}

export const sendLog = (data) => {
    const body = JSON.stringify(data)
    return new Promise((resolve, reject) => {
        fetch(FULL_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body,
        }).then(res => {            
            return res.json()
        }).then(data => {
            resolve(data)
        }).catch(err => {reject(err)})
    })
}

export const getSession = () => {
    const sessionObj = localStorage.getItem("sessionObject");
    if (sessionObj) {
        return JSON.parse(sessionObj)
    }
}

export const getRefCmp = (ref) => {   
    if (ref) {
        if (ref.getName) {            
            return ref.getName() //Aura component
        } else if (ref.host) {
            return ref.host.tagName //LWC       
        }
        return null
    } else {
        return null
    }
}

export const getRecordId = () => {
    const urlPathname = window.location.pathname;
    const regexRecordId = /(00[a-zA-Z0-9]+)/;
    
    const recordId = regexRecordId.exec(urlPathname);
    if (recordId) {
        return recordId[1]; 
    }
    return null
}

export const constructInfoLog = (obj) => ({
        LogItemType: 'Information',
        LogTypeId: null,
        ExceptionOccured: false,
        ClassName: null,
        Method: null,
        RecordId: null,
        Message: null,
        TimeStamp: new Date().getTime(),
        UserId: userId,
        Duration: null,
        TransactionId: null,
        CorrelationId: null,
        AdditionalData: {},
        ...obj    
})

export const constructErrLog = (obj) => ({
    LogItemType: 'Error',
    LogTypeId: null,
    ExceptionOccured: true,
    ClassName: null,
    Method: null,
    RecordId: null,
    Message: null,
    TimeStamp: new Date().getTime(),
    UserId: userId,
    Duration: null,
    TransactionId: null,
    CorrelationId: null,
    AdditionalData: {},
    ...obj    
})

export const sanitizeLogItem = (logItem) => {    
    let sanitizedLogItem = {};
    LOG_ITEM_SCHEMA.forEach(item => {
        if (typeof logItem[item.name] === item.type) {
            sanitizedLogItem[item.name] = logItem[item.name];
        } else {
            sanitizedLogItem[item.name] = null;
        }
    })
    return sanitizedLogItem;
}

// export const getEnv = () => {
//     const hostname = window.location.hostname;
//     const regexEnv = /(dev|test|load)/;
    
//     const env = regexEnv.exec(hostname);
//     if (env) {
//         return env[1].toUpperCase(); 
//     }
//     return 'PROD'
// }

export const constructSource = () => ({ 
    SourceEnvironment :  getEnv(),
    SourceProgram : "Connected",
    SourcePlatform : "SFDCEvents",
    SourceAppName : "SFDCFrontendLogger",
    SourceModule :  "",
    SourceTeam : "Connected",
    SourcePayloadType : "",
    SourcePriority  : "",
    SourceEncryptionEnabled : "",
    SourceMaskEnabled : "",
    SourceTimestamp : "",
    SourcePluginType : ""
})

    // name: 'SFFrontendLogAPI', //hardcoded
    // environment: getEnv(), //Possible environments: dev, test, load, prod.
    // platform: 'SFDC', //hardcoded
    // team: 'Connected', //hardcoded