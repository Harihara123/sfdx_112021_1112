import CONNECTED_LOG_API_URL from '@salesforce/label/c.CONNECTED_LOG_API_URL';
import CONNECTED_LOG_API_URL_PROD from '@salesforce/label/c.CONNECTED_LOG_API_URL_PROD';
import CONNECTED_LOG_API_PROD_KEY from '@salesforce/label/c.CONNECTED_LOG_API_PROD_KEY';
import {getEnv} from 'c/lwcUtilities';

export const FULL_URL = getEnv() === 'PROD' ? CONNECTED_LOG_API_URL_PROD+CONNECTED_LOG_API_PROD_KEY : CONNECTED_LOG_API_URL
export const LOG_ITEM_SCHEMA = [
    {name: 'LogItemType', type: 'string'},
    {name: 'LogTypeId', type: 'string'},
    {name: 'ExceptionOccured', type: 'boolean'},
    {name: 'Module', type: 'string'},
    {name: 'ClassName', type: 'string'},
    {name: 'Method', type: 'string'},
    {name: 'RecordId', type: 'string'},
    {name: 'Message', type: 'string'},
    {name: 'TimeStamp', type: 'number'},
    {name: 'UserId', type: 'string'},
    {name: 'Duration', type: 'number'},
    {name: 'TransactionId', type: 'string'},
    {name: 'CorrelationId', type: 'string'},
    {name: 'AdditionalData', type: 'object'}
]