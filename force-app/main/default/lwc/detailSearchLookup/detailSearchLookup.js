import { LightningElement, wire, api, track } from 'lwc';
import searchClientContacts from '@salesforce/apex/DetailSearchLookupController.searchResult';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class DetailSearchLookup extends LightningElement {
    @api tableColumns;
    @api qcondition;
    @api sortCondition;
	@api searchString;
	@api searchInFields;
	@api returningFields;
	@api featureItem;
	@api objectName;

    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track clearIconFlag = false;
    @track iconFlag = true;
    @track statusOptions;
    @track statusValue;
    @track tableData;
    @track objName;
    @track loading = false;
    
    accQueryFields = [];
    selectedRows;

    //Filter
    @track keywordFilterValue;
    @track locatonFilterValue;
    orignalResult = [];
    globalfilterResults = [];
    isKeywordFilter = false;
    isLocationFilter = false;
    isStatusFilter = false;
    keywordFilterKey;
    locationFilterKey;
	statusFilterKey;

	@api clearData() {

        this.selectedRows = null;
      
		this.orignalResult = [];
		this.tableData = [];
		this.globalfilterResults = [];
		this.qcondition = [];

		//this.statusValue = "All Statuses";
		this.keywordFilterValue = '';
        this.locatonFilterValue = '';

		this.statusFilterKey = '';
		this.keywordFilterKey = '';
		this.locationFilterKey = '';

		this.isKeywordFilter = false;
		this.isLocationFilter = false;
		this.isStatusFilter = false;
    } 

	@api initModal() {
		this.template.querySelector('[data-id=statusinput]').value = "All Statuses";
		window.clearTimeout(this.delayTimeout);

		 this.delayTimeout = setTimeout(() => {
			this.handleSearch();
        }, 300);
	}

	renderedCallback() {
		this.statusValue = "All Statuses";
	}
	
	//initialRender;
    constructor() {
        super();
       this.statusOptions = [
			{ "label": "All Statuses", "value": "All Statuses" },
			{ "label": "Currently Has Talent", "value": "Currently Has Talent" },
            { "label": "Previously Had Talent", "value": "Previously Had Talent" },
            { "label": "Never had Talent", "value": "Never had Talent" }
        ];
    }

    
    handleChange(event) {
        this.searchString = event.detail.value;
    }

    handleSearch() {
		this.keywordFilterValue = '';
		this.template.querySelector('[data-id=keywordinput]').value = '';
		this.locatonFilterValue = '';
		this.template.querySelector('[data-id=locationinput]').value = '';
		 if (this.searchString !== undefined && this.searchString.length > 0) {           
            
            this.loading = true;
			searchClientContacts({'featureItem':this.featureItem, 'searchString': this.searchString, 'searchInFields': this.searchInFields, 'returningFields' : this.returningFields, 'targetObject': this.objectName, 'condition': this.qcondition, 'sortString': this.sortCondition }).then(result => {
                if (result !== undefined && result != null) {
                    let results = this.updateResults(result[0]);
                    this.orignalResult = results;
                    this.tableData = results;
                    this.globalfilterResults = results;
                }
                this.loading = false;

            }).catch(error => {
                console.log('error****'+JSON.stringify(error));
                this.loading = false;
            });
        }
    }

    handleRowSelection(event) {
        this.selectedRows = event.detail.selectedRows;
    }

    @api handleSave() {
        if (this.selectedRows !== null && JSON.stringify(this.selectedRows) !== '[]' && this.selectedRows) {
			const rowselectedevent = new CustomEvent('rowselectedevent', {
                detail: { "selectedrow": this.selectedRows, "objectName": this.objectName }
            });

            this.dispatchEvent(rowselectedevent);
            this.tableData = '';
        } else {
            this.showNotification();
        }
    }

    @api handleCancelModal(event) {
        const cancelEvent = new CustomEvent('cancleclickevent');
        this.dispatchEvent(cancelEvent);
    }

    handleKeywordFilter(event) {
        this.keywordFilterKey = event.target.value;
        this.keywordFilterKey = this.keywordFilterKey.replace(/[\/\\|\+\[\*()?]/g,'');
        this.isKeywordFilter = true;
        this.buildFilteredResult();    
    }

    handleLocationFilter(event) {
        this.locationFilterKey = event.target.value;
		this.locationFilterKey = this.locationFilterKey.replace(/[\/\\|\+\[\*()?]/g,'');
        this.isLocationFilter = true;
        this.buildFilteredResult();
    }

    handleOptionChange(event) {
        this.statusFilterKey = event.target.value;
        this.isStatusFilter = true;
        this.buildFilteredResult();
    }

    keywordFilterr() {
        let filterKey = this.keywordFilterKey.toUpperCase();

        let myArray = this.globalfilterResults;
        if (filterKey === '') {
            this.isKeywordFilter = false;
            this.buildFilteredResult();
        } else {
            let filteredResults = [];
            for (let i = 0; i < myArray.length; i++) {
                let nameStr = myArray[i].Name.toUpperCase();
                if (nameStr.match(filterKey)) {
                    filteredResults.push(myArray[i]);
                    continue;
                }

                if (myArray[i].Last_Activity_Date__c !== undefined) {
                    let lastActDateVal = myArray[i].Last_Activity_Date__c.toUpperCase();
                    if (lastActDateVal.match(filterKey)) {
                        filteredResults.push(myArray[i]);
                        continue;
                    }
                }

                if (this.objectName === 'Account') {
                    if (myArray[i].No_of_Reqs__c !== undefined) {
                        let reqsVal = myArray[i].No_of_Reqs__c.toString();
                        if (reqsVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }

                    if (myArray[i].Siebel_ID__c !== undefined) {
                        let siebelIdVal = myArray[i].Siebel_ID__c.toUpperCase();
                        if (siebelIdVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }

                    if (myArray[i].Master_Global_Account_Id__c !== undefined) {
                        let globalIdVal = myArray[i].Master_Global_Account_Id__c.toUpperCase();
                        if (globalIdVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                } else { //Hiring Manager
                    if (myArray[i].Title !== undefined) {
                        let titleVal = myArray[i].Title.toUpperCase();
                        if (titleVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    if (myArray[i].Email !== undefined) {
                        let emailVal = myArray[i].Email.toUpperCase();
                        if (emailVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    if (myArray[i].Phone !== undefined) {
                        let phoneVal = myArray[i].Phone.toUpperCase();
                        if (phoneVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    if (myArray[i].Related_Account_Name__c !== undefined) {
                        let accountVal = myArray[i].Related_Account_Name__c.toUpperCase();
                        if (accountVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                }

            }
            this.tableData = filteredResults;
            this.globalfilterResults = filteredResults;
        }
    }

    locationFilter() {
        let filterKey = this.locationFilterKey.toUpperCase();
        let myArray = this.globalfilterResults;

        if (filterKey === '') {
            this.isLocationFilter = false;
            this.buildFilteredResult();
        } else {
            let filteredResults = [];
            for (let i = 0; i < myArray.length; i++) {
                if (this.objectName === 'Account') {
                    //Street
                    if (myArray[i].BillingStreet !== undefined) {
                        let streetVal = myArray[i].BillingStreet.toUpperCase();
                        if (streetVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    //City
                    if (myArray[i].BillingCity !== undefined) {
                        let cityVal = myArray[i].BillingCity.toUpperCase();
                        if (cityVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    //State
                    if (myArray[i].BillingState !== undefined) {
                        let stateVal = myArray[i].BillingState.toUpperCase();
                        if (stateVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                } else {
                    //Street
                    if (myArray[i].MailingStreet !== undefined) {
                        let streetVal = myArray[i].MailingStreet.toUpperCase();
                        if (streetVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    //City
                    if (myArray[i].MailingCity !== undefined) {
                        let cityVal = myArray[i].MailingCity.toUpperCase();
                        if (cityVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    //state
                    if (myArray[i].MailingState !== undefined) {
                        let stateVal = myArray[i].MailingState.toUpperCase();
                        if (stateVal.match(filterKey)) {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                }
            }
            this.tableData = filteredResults;
            this.globalfilterResults = filteredResults;
        }
    }



    statusFilter() {
        let filterKey = this.statusFilterKey;
        let myArray = this.globalfilterResults;

        if (filterKey === 'All Statuses') {
            this.isStatusFilter = false;
            this.buildFilteredResult();
        } else {
            let filteredResults = [];
            for (let i = 0; i < myArray.length; i++) {
                if (myArray[i].Customer_Status__c !== undefined) {
                    let statusVal = myArray[i].Customer_Status__c;
                    if (statusVal.match(filterKey)) {
                        filteredResults.push(myArray[i]);
                    }
                }
            }
            this.tableData = filteredResults;
            this.globalfilterResults = filteredResults;
        }
    }

    buildFilteredResult() {

        if (!this.isKeywordFilter && !this.isLocationFilter && !this.isStatusFilter) {
            this.tableData = this.orignalResult;
        }

        this.globalfilterResults = this.orignalResult

        if (this.isKeywordFilter) {
            this.keywordFilterr();
        }

        if (this.isLocationFilter) {
            this.locationFilter();
        }

        if (this.isStatusFilter) {
            this.statusFilter();
        }


    }

    get accountObject() {
        return this.objectName === 'Account' ? "Account Name" : "Hiring Manager";
    }

    updateResults(results) {

        for (let i = 0; i < results.length; i++) {
            let status;
            if (results[i].Customer_Status__c !== undefined) {
                status = results[i].Customer_Status__c.toUpperCase();
                if (status.match('RED')) {
                    results[i].Customer_Status__c = 'Never had Talent';
                }
                if (status.match('GREEN')) {
                    results[i].Customer_Status__c = 'Currently Has Talent';
                }
                if (status.match('YELLOW')) {
                    results[i].Customer_Status__c = 'Previously Had Talent';
                }

            }
            if (results[i].AccountId !== undefined) {
                results[i].AccountId = '/' + results[i].AccountId;
            }
            if (results[i].Account !== undefined) {
                results[i].EmployerName = results[i].Account.Name;
            }
        }
        return results;
    }

    showNotification() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: 'Please select at least one record',
            variant: 'error'
        });
        this.dispatchEvent(evt);
    }

	handleClick(event){
		this.selectedRows =this.tableData.find(({Id}) => Id === event.currentTarget.value);
	}

	get isAccount() {
		return this.objectName === 'Account';
	}

	handleKeyUp(event) {
		var key = 'which' in event ? event.which : event.keyCode;
		if (key == 13) {
			this.handleSearch();
		}
	}
}