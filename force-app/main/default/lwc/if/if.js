import { LightningElement, api } from 'lwc';
import temp from './if.html';
import empty from './empty.html'
import notif from './else.html'


export default class If extends LightningElement {
    @api a
    @api b
    @api comp
    @api useElse

    compOps = {
        '===': (a,b) => a === b,
        '!==': (a,b) => a !== b,
        '>': (a,b) => a > b,
        '<': (a,b) => a < b,
        '>=': (a,b) => a >= b,
        '<=': (a,b) => a <= b
    }
    check() { 
    if (this.compOps[this.comp]) {
        return this.compOps[this.comp](this.a, this.b)
    }
       
        console.log('Please use one of the string values for comparison operator attribute "compop"');
        console.log({
            'equals': '===',
            'not equals': '!==',
            'greater': '>',
            'less': '<',
            'greater or equals': '>=',
            'less or equals': '<='
       })
       return false
    }
    render() {
        if (this.check()) {
            return temp
        }
        if(this.useElse) {
            return notif
        }
        return empty        
    }
}