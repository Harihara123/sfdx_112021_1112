import { LightningElement,track, api} from 'lwc';
// Import custom labels
import ATS_TASK from '@salesforce/label/c.ATS_TASK';
export default class LWC_BaseCardTimelineItem extends LightningElement {
    @api currentUser;
    @api itemDetail;
    @api itemEditEventName;
    @api itemDeleteEventName;
    @api taskViewAction;
    @api expandAll;
    @api detailPage;
    @api actionMenu;
    @api recType;

    @api enableCondensedLabels;
    
    label = {
        ATS_TASK,        
    };
    
    // constructor() {
    //     super();
    //     //this.addEventListener('oneventedit', this.handleEventEdit.bind(this));
    // }
    handleEventEdit(evt){
        
        const itemeditevent = new CustomEvent('itemeditevent', {                        
            detail : {"eventData":evt.detail}
        });
        this.dispatchEvent(itemeditevent);
    }
    handleDeleteEvent(evt){
        // console.log('IWC DELETE EVENT >> '+JSON.stringify(evt.detail));
        const onitemedeleteevent = new CustomEvent('itemedeleteevent', {                        
            detail : {"eventData":evt.detail}
        });
        this.dispatchEvent(onitemedeleteevent);
    }
    get checkRecValid(){        
        return this.itemDetail != null;
    }

    get toggleActionMenu(){
        return this.actionMenu;
    }
}