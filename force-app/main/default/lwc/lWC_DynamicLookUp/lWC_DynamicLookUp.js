import { LightningElement,wire,api,track  } from 'lwc';
import searchClientContacts from '@salesforce/apex/DupeSearchController.searchClientContacts';

export default class LWC_DynamicLookUp extends LightningElement {
    
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track clearIconFlag = false;
    @track iconFlag =  true;
    @track searchString = ''
    @track tableData;
    @track targetObject = 'Account';

    @track tableColumns = [];
    

    accQueryFields = [];

    constructor() {
        super();
        this.accColumns = [
            { label: 'Name', fieldName: 'Name'},
            { label: 'State', fieldName: 'BillingState'},
            { label: 'City', fieldName: 'BillingCity'},
            { label: 'Street', fieldName: 'BillingStreet'},
            { label: 'RecordId', fieldName: 'Id'},
        ];

        this.contColumns = [
            { label: 'Name', fieldName: 'Name'},
            { label: 'Account', fieldName: 'AccountId'},
            { label: 'Email', fieldName: 'email'},            
        ];  
        
        this.tableColumns = this.accColumns;
    }
    get objectionOptions() {
        return [
            { label: 'Account', value: 'Account' },
            { label: 'Contact', value: 'Contact' },
        ];
    }
    targetObjectChange(event){
        
        this.targetObject = event.detail.value;
        this.tableColumns = (this.targetObject === 'Account') ? this.accColumns :this.contColumns;
        this.handleSearch();
    }
    searchField(event) {
        var currentText = event.target.value;
        console.log(currentText);
    }
    handleSearch(){
        const searchElem = this.template.querySelector('[data-id=userinput]');
        if(searchElem != undefined && searchElem.value.length>0){
            var arrFields = [];
        if(this.targetObject === 'Account'){
            this.accColumns.forEach(function(queryField){
                arrFields.push(queryField['fieldName']);               
            });
        }else{
            this.contColumns.forEach(function(queryField){
                arrFields.push(queryField['fieldName']);               
            });
        }
        
        

        
        searchClientContacts({'searchString': searchElem.value, 'searchFields' : arrFields.join(','), 'targetObject':this.targetObject}).then(result => {
            if(result != undefined){
                if(result != null){
                    this.tableData = result[0];
                    console.log('arrRecords ' + JSON.stringify(this.tableData));
                }                
            }
        }).catch(error => {
            console.log(JSON.stringify(error));
        });            
        }                
    }
}