import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
// importing Custom Label
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {cbOptions, cols, activityFilter, contactTypeFilter, statusFilter, labelConst} from 'c/lwcActivityTableHelper';

import saveUserPref from '@salesforce/apex/MyActivitiesController.saveUserActivityPreferences';

export default class MyActivities extends NavigationMixin(LightningElement) {
	//@track showSpinner - check;
	@api tableData;
	@api tableCol;
	@api actions;
	@api resetCheckbox;
	@api cbValue;// = 'MyDefault'; // filter combo box
	@track options = cbOptions; // filter combo box
	@track cols = cols;
	@track activityFilterOptions;
	@track contactTypeFilterOptions;
	@track displaySelectedActivityFilters = [];
	@track statusFilterOptions;
	@track selectedFilters = [];
	@track disableApplyButton = true;	
	@track keywordFilterKey;
	@track activityFilterText;
	@track contactTypeFilterText;
	@track statusFilterText;
	@api noDataDisplay;

    @api originalTableData = [];
	@api startDate;
	@api endDate;
	@api rangeFilterValue;
	@api 
	get userPrefValue() {
		return this.cbValue;
	}
	set userPrefValue(val) {
		if(val !== 'NO_UESER_PREF') { //there is no option 'NO_USER_PREF'in combo box so set the prev value.
			this.cbValue = val;
		}
	}

	sortableField='dueDate';
	sortOrder='asc';

	@api
	get initFilters() {
		return this.defaultFilterData; 
	}  
	set initFilters(value) {//hide/display 
		this.defaultFilterData = value;
		this.assignInitialFilter();	
	}

	get displaySavePrefButton() {
		let userPrefSelectedOption = this.template.querySelector('[data-id=userprefcombo]');
		if(!userPrefSelectedOption) {
			return true;
		}
		else if((userPrefSelectedOption && userPrefSelectedOption.cbValue) || this.cbValue === 'MyDefault' ) {
			return true;
		}
		else {
			return false;
		}

		//return (!userPrefSelectedOption) ? true : (userPrefSelectedOption && userPrefSelectedOption.cbValue == 'MyDefault') ? true : false;
	}
	/*set displaySavePrefButton(val) {
		this.displaySavePrefButton = val;
	}*/

	defaultFilterData;
	activityFilterFieldAPI;
	contactFilterFieldAPI;
	statusFilterFieldAPI;
	selectedUserPref = 'MyDefault';
	label = labelConst
	//@track runOnce = false;

	//label = {ATS_NO_MY_ACTIVITIES,ATS_Filter_This_Table,ATS_APPLY,ATS_Action,ATS_Mass_Update_Tasks,ATS_Delete_Selected,ATS_RESET,ATS_SELECT,ATS_CANCEL,};
	
	connectedCallback() {
		this.activityFilterText = 'All Activity Types';
		this.contactTypeFilterText = 'All Contact Types';
		this.statusFilterText = 'All Statuses';
	}
	
	getRelativeDate(days) {
		const today = new Date().getTime();
        const ms = today + (days * 24 * 60 * 60 * 1000)
        const isoString = new Date(ms).toISOString()
        return isoString.split('T')[0];   
	}
	

	handleEdit(event) {}

	handleUserPrefChange(event) {
		this.selectedUserPref = event.target.value;
		this.sendToAura(event);
	}
	handleRowAction(event) {
		this.dispatchEvent(new CustomEvent('rowDetailsEvent', {
			detail: event.detail
		}));
	}

	handleDatabaseSorting(event){
		let sortInfo = event.detail;
		this.sortableField = sortInfo.sortField;
		this.sortOrder = sortInfo.sortOrder;
		this.selectedUserPref  = 'NO_UESER_PREF';
		this.sendToAura(event);
	}

	handleKeywordFilter(event) {
		let filterKey = event.target.value;
		filterKey = filterKey.replace(/[\/\\|\+\[\*()?]/g, '');

		filterKey = filterKey.toUpperCase();
		let myArray = this.originalTableData;
		if (filterKey === '' || filterKey === ' ') {
			this.isKeywordFilter = false;
			this.tableData = this.originalTableData;
			
		} else {
			let filteredResults = [];

			for (let i = 0; i < myArray.length; i++) {

				let cellArray = myArray[i].cells;
				let flag = false;
				for (let j = 0; j < cellArray.length; j++) {
					let cellName = cellArray[j].name;
					let cellvalue = cellArray[j].value;

					if (cellvalue) {
						cellvalue = cellvalue.toUpperCase();

						switch (cellName) {
							case "contactName":
							case "type":
							case "subject":
							case "relatedTo":
								if (cellvalue.match(filterKey)) {
									filteredResults.push(myArray[i]);
									flag = true;
								}
						}
					}
					if (flag) break;
				}
			}
			this.tableData = filteredResults;
		}
	}
	//End of Key Word filter	

	assignInitialFilter() {
		
		this.activityFilterOptions = activityFilter;
		this.contactTypeFilterOptions = contactTypeFilter;
		this.statusFilterOptions = statusFilter;
		this.activityFilterFieldAPI = [{'objname':'Event','fieldName':'Type'}, {'objname':'Task','fieldName':'Type'}];
		this.contactFilterFieldAPI = [{'objname':'Both','fieldName':'who.RecordType.Name'}, {'objname':'Both','fieldName':'who.RecordType.Name'}]; 
		this.statusFilterFieldAPI = [{'objname':'Event','fieldName':'Description'}, {'objname':'Task','fieldName':'Status'}]; 

		let activityType='';
		let status = '';
		let contactFilter = '';

		this.defaultFilterData.forEach((filter) => {			
			if(filter.fieldName === 'Type') {
				if(activityType === '')
					activityType = filter.fieldLabel;					
				else {
					if(filter.fieldLabel != '')
						activityType += ','+filter.fieldLabel;
				}

			} else if((filter.fieldName === 'Description' && filter.objName === 'Event') || (filter.fieldName === 'Status' && filter.objName === 'Task')) {
				if(status === '')
					status = filter.fieldValue;
				else {		
					if(filter.fieldLabel != '')			
						status += ','+filter.fieldValue;
				}	

			} else if(filter.fieldName === 'who.RecordType.Name') {
				contactFilter = filter.fieldValue;
			}
		});
		let activityTypeArr = activityType.split(',');
		let statusArr = status.split(',');
		let contactFilterArr = contactFilter.split(',');

		this.assignToRelatedFilter(this.contactTypeFilterOptions, 'contactType', contactFilter);
		this.assignToRelatedFilter(this.activityFilterOptions, 'activityType', activityType);
		this.assignToRelatedFilter(this.statusFilterOptions, 'statusType', status);
		this.activityFilterText = this.displayFilterText(activityTypeArr, 'activityType');
		this.contactTypeFilterText = this.displayFilterText(contactFilterArr, 'contactType');
		this.statusFilterText = this.displayFilterText(statusArr, 'statusType');

	}

	
	@track activityPopover = false;
	@track contactTypePopover = false;
	@track statusPopover = false;

	enablePopover(event) {
		let targetId = event.target.name;
		if (targetId === 'ActivityType') {
			if (!this.activityPopover) {
				this.activityPopover = true;
				this.contactTypePopover = false;
				this.statusPopover = false;
			} else
				this.activityPopover = false;
		} else if (targetId === 'ContactType') {
			if (!this.contactTypePopover) {
				this.activityPopover = false;
				this.contactTypePopover = true;
				this.statusPopover = false;
			} else
				this.contactTypePopover = false;
		} else if (targetId === 'statusType') {
			if (!this.statusPopover) {
				this.activityPopover = false;
				this.contactTypePopover = false;
				this.statusPopover = true;
			} else
				this.statusPopover = false;

		}
	}

	sendToAura(event) {
		
		this.disableApplyButton = true;
		this.keywordFilterKey = '';
		this.activityPopover = false;
		this.contactTypePopover = false;
		this.statusPopover = false;
		this.selectedFilters = [];
		let value;		

		if(event.target.getAttribute('data-action') === 'apply') {
			this.selectedUserPref  = 'NO_UESER_PREF';
		}
		
		this.getFilteredObject(this.activityFilterOptions, this.activityFilterFieldAPI);
		this.getFilteredObject(this.contactTypeFilterOptions, this.contactFilterFieldAPI);
		this.getFilteredObject(this.statusFilterOptions, this.statusFilterFieldAPI);

		value = this.selectedFilters;

		if (JSON.stringify(value) == undefined || JSON.stringify(value) == '') {
			const value = '';
		}
		
		const valueChangeEvent = new CustomEvent("valuechange", {
			detail: {
				"actionType":"FILTER",
				"picklistfilter": value,
				"rangeFilterValue" : this.rangeFilterValue,
				"startDate": this.startDate,
				"endDate": this.endDate,
				"selectedUserPref" : this.selectedUserPref,
				"sortableField" : this.sortableField,
				"sortOrder" : this.sortOrder
			}
		});
		this.setSpinner = false;
		// Fire the custom event
		this.dispatchEvent(valueChangeEvent);
	}

	handleReset(event) {
		let tabName = 'My_Activities_LWC';
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: tabName
			}
		});
	}

	getFilteredObject(filterList, fieldNameList) {
		filterList.forEach((options) => {
			let filterData = {};
			let filterDataNotProceeding = {};
			filterData.isfilteronResult = options.isfilteronResult;
			if (options.type.includes('Task')) {
				filterData.objName = 'Task';
			}
			else if (options.type.includes('Event')) {
				filterData.objName = 'Event';
			}
			else {
				filterData.objName = 'Both';
			}

			options.values.forEach(function(option) {
				if (option.selected) {	
					if (filterData.fieldValue === undefined || filterData.fieldValue === '') {
						if(filterData.objName == 'Event' && option.label=='Not Proceeding'){
							filterDataNotProceeding.objName = 'Event';
							filterDataNotProceeding.fieldValue = option.label +'%';							
							filterDataNotProceeding.fieldLabel = option.value;
						}else{
							filterData.fieldValue = option.label;
							filterData.fieldLabel = option.value;
						}
					} else {
						if(filterData.objName == 'Event' && option.label=='Not Proceeding'){
							filterDataNotProceeding.objName = 'Event';
							filterDataNotProceeding.fieldValue = option.label +'%';	
							filterDataNotProceeding.fieldLabel = option.value;
						}else{						
							filterData.fieldValue += ',' + option.label;
							filterData.fieldLabel += ',' + option.value;
						}
					}
				}

			});			
			filterData.fieldName = this.getFieldAPIName(fieldNameList,filterData.objName);
			//for Not Proceeding
			if(filterDataNotProceeding != undefined){
				filterDataNotProceeding.operator = ' '+'LIKE';
				if (filterDataNotProceeding.fieldValue != undefined){
					filterDataNotProceeding.fieldName = this.getFieldAPIName(fieldNameList,filterData.objName);
					filterDataNotProceeding.isfilteronResult  = options.isfilteronResult;
					this.selectedFilters.push(filterDataNotProceeding);
				}
			}
			//for Not Proceeding
			filterData.operator = 'IN';
			if (filterData.fieldValue == undefined){
				filterData.fieldValue = '';
				filterData.fieldLabel = '';
			} 
			
			this.selectedFilters.push(filterData);
		});

	}

	getFieldAPIName(fieldNameList, objName) {
		let fieldName='';
		fieldNameList.forEach((fieldAPIName)=>{
			if(objName === fieldAPIName.objname) {
				fieldName = fieldAPIName.fieldName;
			}
		});
		return fieldName;
	}
	displayActivityApply = true;
	displayContactTypeApply = true;
	displayStatusApply = true;

	displaySelectedFilters(event) {
		this.activityPopover = false;
		this.contactTypePopover = false;
		this.statusPopover = false;
		let selectedElements = event.detail.selectedValue;
		let filterType = event.detail.filterType;		
		
		if (filterType === 'activityType') {
			this.activityFilterText = this.displayFilterText(selectedElements, 'activityType');
			if (selectedElements.length > 0)
				this.displayActivityApply = false;
			else
				this.displayActivityApply = true;

			this.assignToRelatedFilter(this.activityFilterOptions, filterType, selectedElements);
		} else if (filterType === 'contactType') {
			this.contactTypeFilterText = this.displayFilterText(selectedElements, 'contactType');
			if (selectedElements.length > 0)
				this.displayContactTypeApply = false;
			else
				this.displayContactTypeApply = true;
			this.assignToRelatedFilter(this.contactTypeFilterOptions, filterType, selectedElements);
		} else if (filterType === 'statusType') {
			this.statusFilterText = this.displayFilterText(selectedElements, 'statusType');
			if (selectedElements.length > 0)
				this.displayContactTypeApply = false;
			else
				this.displayContactTypeApply = true;
			this.assignToRelatedFilter(this.statusFilterOptions, filterType, selectedElements);
		}		
		if(filterType != '') {
			if (this.displayActivityApply || this.displayContactTypeApply || this.displayStatusApply) {
				this.disableApplyButton = false;
			} else {
				this.disableApplyButton = true;
			}
		}
		
	}

	displayFilterText(selectedElements,filterType) {
		let filterText = '';		
		let taskarr = this.activityFilterOptions[0];
		let eventarr = this.activityFilterOptions[1];
		let activityFilterCount = taskarr.values.length + eventarr.values.length;
		
		let statusTaskArr = this.statusFilterOptions[0];
		let statusEventArr = this.statusFilterOptions[1];
		let statusFilterCount = statusTaskArr.values.length + statusEventArr.values.length;

		if(selectedElements.length === 1){
			filterText = selectedElements[0];			
			var s =filterText;
			var n = s.indexOf('_');
			s = s.substring(0, n != -1 ? n : s.length);
			filterText = s;
		}
		else if(selectedElements.length > 1) {
			filterText = selectedElements.length;
			if(filterType === 'activityType' && filterText != activityFilterCount){
				filterText += ' Types Selected';
			}else if(filterType === 'activityType' && filterText ===activityFilterCount){
				filterText = 'All Activity Types';
			}
			else if (filterType === 'contactType'){
				filterText = 'All Contact Types';
			}
			else if(filterType === 'statusType' && filterText != statusFilterCount){
				filterText += ' Statuses Selected';
			}else if(filterType === 'statusType' && filterText === statusFilterCount){
				filterText = 'All Statuses';
			}
		}else {
			if(filterType === 'activityType')
				filterText = 'All Activity Types';
			else if (filterType === 'contactType')
				filterText = 'All Contact Types';
			else if(filterType === 'statusType')
				filterText = 'All Statuses';
		}
		return filterText;
	}

	assignToRelatedFilter(filterOptions, filterType, selectedElements) {
		let selectedFilters = [];
		filterOptions.forEach((options) => {
			let filterData = {},
				filterOptions = [];
			let counter = 0;
			filterData.type = options.type;
			filterData.isfilteronResult = options.isfilteronResult;
			//set maincheckbox to true
			options.values.forEach(function(option) {				
				let filterOption = {},
					selectFilterOption = {};
				filterOption.label = option.label;
				filterOption.value = option.value;
				
				if (selectedElements.includes(option.value)) {
					filterOption.selected = true;
					counter++;
				} else
					filterOption.selected = false;
				filterOptions.push(filterOption);				

			});
			//set selectAll checkbox here
			if(options.values.length == counter)
				filterData.selected = true;
			else
				filterData.selected = false;

			filterData.values = filterOptions;
			selectedFilters.push(filterData);

		});

		if (filterType === 'activityType') {
			this.activityFilterOptions = selectedFilters;
		} else if (filterType === 'contactType')
			this.contactTypeFilterOptions = selectedFilters;
		else if (filterType === 'statusType')
			this.statusFilterOptions = selectedFilters;
	}

	handleDateChange(event) {
		this.startDate = event.detail.from;
		this.endDate = event.detail.to;
		this.rangeFilterValue = event.detail.rangeFilter;
		if(this.disableApplyButton)
			this.disableApplyButton = false;
	}

	handleSelect(e) {
		const actionMap = {
			cancel: 'handleClick',
			select: 'handleSelectClick'
		}
		const type = e.target.getAttribute('data-type');
		const action = e.target.getAttribute('data-action');
		if (type) {
			let filter = this.template.querySelector(`[data-id="${type}"]`);
			filter[actionMap[action]]();
		}
	}
	handleMassUpdate(){
		let selectedRows = this.template.querySelector('c-lwc-data-table').getSelected();
		let editableTasksIds=[];
		for(let i=0; i < selectedRows.length; i++) {
			let task = selectedRows[i];
			if(Boolean(task.isMassEditable)) {
				editableTasksIds.push(task.id);
			}
		}

		if(editableTasksIds.length > 0) {
			const massUpdateEvent = new CustomEvent("massactionevent",{detail:{"taskIdList":editableTasksIds, "actionType":"MassUpdate"}});
			this.dispatchEvent(massUpdateEvent);
		}
		else {
			this.showToast('', labelConst.ATS_SELECT_ONE_TASK , 'error');
		}
		
	}

	handleMassDelete(){
		let selectedRows = this.template.querySelector('c-lwc-data-table').getSelected();
		let delEvtIds = [];
		let delTskIds = [];
		let numOfRec=0;
		for(let i=0; i < selectedRows.length; i++) {
			let activity = selectedRows[i];
			if(Boolean(activity.isMassDeletable)) {
				if(activity.activityType === 'Task') {
					delTskIds.push(activity.id);
					numOfRec = numOfRec + 1;
				}
				else {
					delEvtIds.push(activity.id);
					numOfRec = numOfRec + 1;
				}
			}
		}

		if(numOfRec > 0) {
			this.disableApplyButton = true;
			const massUpdateEvent = new CustomEvent("massactionevent",{detail:{"delTskIds":delTskIds, "delEvtIds":delEvtIds, "numOfRec":numOfRec, "actionType":"MassDelete"}});
			this.dispatchEvent(massUpdateEvent);
		}
		else {
			this.showToast('', labelConst.ATS_SELECT_ONE_ACT, 'error');
		}
	}

	showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

	saveUserPreferences() {
		let selectedFilter;		
		this.selectedFilters = [];
		this.getFilteredObject(this.activityFilterOptions, this.activityFilterFieldAPI);
		this.getFilteredObject(this.contactTypeFilterOptions, this.contactFilterFieldAPI);
		this.getFilteredObject(this.statusFilterOptions, this.statusFilterFieldAPI);

		selectedFilter = this.selectedFilters;

		if (JSON.stringify(selectedFilter) == undefined || JSON.stringify(selectedFilter) == '') {
			selectedFilter = '';
		}

		const valueChangeEvent = new CustomEvent("valuechange", {
			detail: {
				"actionType":"USER_PREF",
				"picklistfilter": selectedFilter,
				"rangeFilterValue" : this.rangeFilterValue,
				"startDate": this.startDate,
				"endDate": this.endDate,
				"sortableField" : this.sortableField,
				"sortOrder" : this.sortOrder
			}
		});
		this.dispatchEvent(valueChangeEvent);
		this.cbValue = 'MyDefault';
	}
}