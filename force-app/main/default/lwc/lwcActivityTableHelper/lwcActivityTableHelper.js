import ATS_NO_MY_ACTIVITIES from '@salesforce/label/c.ATS_NO_MY_ACTIVITIES';
import ATS_Filter_This_Table from '@salesforce/label/c.ATS_Filter_This_Table';
import ATS_APPLY from '@salesforce/label/c.ATS_APPLY';
import ATS_Action  from '@salesforce/label/c.ATS_Action';
import ATS_Mass_Update_Tasks  from '@salesforce/label/c.ATS_Mass_Update_Tasks';
import ATS_Delete_Selected  from '@salesforce/label/c.ATS_Delete_Selected';
import ATS_RESET from '@salesforce/label/c.ATS_RESET';
import ATS_SELECT_ONE_TASK from '@salesforce/label/c.ATS_SELECT_ONE_TASK';
import ATS_SELECT_ONE_ACT from '@salesforce/label/c.ATS_SELECT_ONE_ACT';
import ATS_SELECT from '@salesforce/label/c.ATS_SELECT';
import ATS_CANCEL from '@salesforce/label/c.ATS_CANCEL';

import ATS_DUE_DATE from '@salesforce/label/c.ATS_DUE_DATE';
import ATS_Contact from '@salesforce/label/c.ATS_Contact';
import ATS_TYPE  from '@salesforce/label/c.ATS_TYPE';
import ATS_SUBJECT from '@salesforce/label/c.ATS_SUBJECT';
import ATS_RELATED_TO from '@salesforce/label/c.ATS_RELATED_TO';
import ATS_STATUS  from '@salesforce/label/c.ATS_STATUS';
import ATS_PRIORITY  from '@salesforce/label/c.ATS_PRIORITY';
import ATS_TASK_EVENT  from '@salesforce/label/c.ATS_TASK_EVENT';
import ATS_Contact_Type  from '@salesforce/label/c.ATS_Contact_Type';
import My_Default from '@salesforce/label/c.My_Default';
import My_Call_Sheet from '@salesforce/label/c.My_Call_Sheet';
import My_Task_This_Week from '@salesforce/label/c.My_Task_This_Week';
import My_Event_This_Week from '@salesforce/label/c.My_Event_This_week';

const labelConst = {ATS_NO_MY_ACTIVITIES, ATS_Filter_This_Table, ATS_APPLY, ATS_Action, ATS_Mass_Update_Tasks, ATS_Delete_Selected, ATS_RESET, ATS_SELECT_ONE_TASK, ATS_SELECT_ONE_ACT, ATS_SELECT, ATS_CANCEL};

const cbOptions = [
            { label: My_Default, value: 'MyDefault' },
            { label: My_Call_Sheet, value: 'MyCallSheet' },
            { label: My_Task_This_Week, value: 'MyTaskThisWeek' },
			{ label: My_Event_This_Week, value: 'MyEventThisWeek' }
        ];

const cols = [
		{ label: ATS_DUE_DATE, field: 'dueDate', sort: true, style: 'width: 120px;' },
		{ label: ATS_Contact, field: 'contactName',	sort: true },
		{ label: ATS_TYPE, field: 'type', sort: true },		
		{ label: ATS_SUBJECT, field: 'subject', sort: true },
		{ label: ATS_RELATED_TO, field: 'relatedTo', sort: true	},
		{ label: ATS_STATUS, field: 'status', sort: true, style: 'width: 120px;' },
		{ label: ATS_PRIORITY, field: 'priority', sort: true, style: 'width: 120px;' },
		{ label: ATS_TASK_EVENT, field: 'activityType', sort: true, style: 'width: 120px;' },
		{ label: ATS_Contact_Type, field: 'contactType', sort: true, style: 'width: 120px;' }		
	];

	const activityFilter = [
		{
			type: 'Task Types',
			selected: true,
			isfilteronResult:false,
			values: [					
				{ label: 'Attempted Contact', value: 'AttemptedContact_taskType', selected: true },
				{ label: 'BD Call', value: 'BDCall_taskType', selected: true },
				{ label: 'Call', value: 'Call_taskType', selected: true },
				{ label: 'Email', value: 'Email_taskType', selected: true },
				{ label: 'G2', value: 'G2_taskType', selected: true },
				{ label: 'To Do', value: 'ToDo_taskType', selected: true },
				{ label: 'Performance Feedback', value: 'PerformanceFeedback_taskType', selected: true },
				{ label: 'Reference Check', value: 'ReferenceCheck_taskType', selected: true },
				{ label: 'Service Touchpoint', value: 'ServiceTouchpoint_taskType', selected: true },
				{ label: 'Other', value: 'Other_taskType', selected: true }
			]
		},
		{
			type: 'Event Types',
			selected: true,
			isfilteronResult:false,
			values: [
				{ label: 'Meeting', value: 'Meeting_eventType', selected: true },
				{ label: 'Meal', value: 'Meal_eventType', selected: true },
				{ label: 'Out Of Office', value: 'OutOfOffice_eventType', selected: true },
				{ label: 'Service Touchpoint', value: 'ServiceTouchpoint_eventType', selected: true },
				{ label: 'Performance Feedback', value: 'PerformanceFeedback_eventType', selected: true },
				{ label: 'Presentation', value: 'Presentation_eventType',	selected: true },
				{ label: 'Other', value: 'Other_eventType', selected: true }, 
				{ label: 'Internal Interview', value: 'InternalInterview_eventType', selected: true },
				{ label: 'Submitted', value: 'Submitted_eventType', selected: true },
				{ label: 'Interviewing', value: 'Interviewing_eventType',	selected: true },
				{ label: 'Offer Accepted', value: 'OfferAccepted_eventType', selected: true },
				{ label: 'Started', value: 'Started_eventType', selected: true },
				{ label: 'Not Proceeding', value: 'NotProceeding_eventType', selected: true }					
			]
		}
	];

	const contactTypeFilter = [
		{
			type: 'Contact Type',
			selected: true,
			isfilteronResult:false,
			values: [
				{ label: 'Client', 	value: 'Client', selected: true },
				{ label: 'Talent', value: 'Talent', selected: true }
			]
		}
	];

	const statusFilter = [
		{
			type: 'Task Statuses',
			selected: true,
			isfilteronResult:false,
			values: [
				{ label: 'Not Started',	value: 'Not Started', selected: true },
				{ label: 'In Progress', value: 'In Progress', selected: true },
				{ label: 'Completed', value: 'Completed', selected: true }
			]
		},
		{
			type: 'Event Statuses',
			selected: true,
			isfilteronResult:true,
			values: [
				{ label:'With Notes', value:'With Notes', selected:true },
				{ label:'Without Notes', value:'Without Notes',	selected:true }
			]
		}
	];

export {cbOptions, cols, activityFilter, contactTypeFilter, statusFilter, labelConst};