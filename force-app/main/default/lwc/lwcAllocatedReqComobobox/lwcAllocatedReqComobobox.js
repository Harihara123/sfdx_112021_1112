import { LightningElement, api } from 'lwc';

export default class LwcAllocatedReqComobobox extends LightningElement {
    openDropdown = false;
    dropDown;
    _value;
    _options;
    _label;

    @api
    get options() {
        return this._options;
    }
    set options(value) {
        if (value) {
            this._options = value;
        }
    }

    @api
    get label() {
        return this._label;
    }
    set label(value) {
        if (value) {
            this._label = value;
        }
    }

    @api
    get value() {
        return this._value;
    }
    set value(val) {
        if (val) {
            this._value = val;
        }
    }

    connectedCallback() {
        
    }

    handleChange(event) {
        this.dropDown = event.currentTarget.dataset.value;
        this.openDropdown = true;
    }


   handleSelect(event) {
        this.openDropdown = false;
        this._value = event.target.value;
        const dropDownvalue = {'label':this._label, 'value':this._value}
        const selectEvent = new CustomEvent('dropdownselect', {
            detail: dropDownvalue
        });
        this.dispatchEvent(selectEvent);
    }

}