import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { publish, MessageContext } from 'lightning/messageService';
import triggerModal from '@salesforce/messageChannel/triggerActivityMessageChannel__c';

export default class LwcAllocatedReqItem extends LightningElement {
    @api matchData;
    @api matchIndex;

    _handleOnMousedown;
    _handleOnMouseup;
    doBlur = false;
    blockBlur = false;

    cssPillClass = 'pill ';
    cssClassEvenOdd = 'slds-grid slds-p-vertical_xxx-small slds-p-horizontal_small slds-border_bottom resultItem ';

    @track phoneShowPopover = false;
    @track emailShowPopover = false;

    phoneKeepOpen = false;
    emailKeepOpen = false;

    phoneHasMultiple = false;
    emailHasMultiple = false;
    
    phoneEmpty = false;
    emailEmpty = false;

    phoneCallClick = 'javascript:void(0)';
    emailCallClick = 'javascript:void(0)';

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        let isEven = this.matchIndex % 2 == 0;
        this.cssClassEvenOdd += isEven ? 'even' : 'odd';

        this.cssPillClass += this.matchData.candidate_status.toLowerCase();

        this.checkAvailablePhones();
        this.checkAvailableEmails();

        this._handleOnMousedown = this.handleOnMousedown.bind(this);
        this.template.addEventListener('mousedown', this._handleOnMousedown);
        this._handleOnMouseup = this.handleOnMouseup.bind(this);
        this.template.addEventListener('mouseup', this._handleOnMouseup);
    }

    disconnectedCallback() {
        this.template.removeEventListener('mousedown', this._handleOnMousedown);
    }

    checkAvailablePhones() {
        let iPhoneCount = 0;
        if (this.matchData.home_phone) iPhoneCount++;
        if (this.matchData.work_phone) iPhoneCount++;
        if (this.matchData.other_phone) iPhoneCount++;
        if (this.matchData.mobile_phone) iPhoneCount++;
        if (iPhoneCount > 1) {
            this.phoneHasMultiple = true;
        }
        else if (iPhoneCount == 1) {
            if (this.matchData.home_phone) {
                this.phoneCallClick = 'tel:' + this.matchData.home_phone;
            }
            else if (this.matchData.work_phone) {
                this.phoneCallClick = 'tel:' + this.matchData.work_phone;
            }
            else if (this.matchData.other_phone) {
                this.phoneCallClick = 'tel:' + this.matchData.other_phone;
            }
            else if (this.matchData.mobile_phone) {
                this.phoneCallClick = 'tel:' + this.matchData.mobile_phone;
            }
        }
        else {
            this.phoneEmpty = true;
        }
    }

    checkAvailableEmails() {
        let iEmailCount = 0;
        if (this.matchData.comm_email) iEmailCount++;
        if (this.matchData.work_email) iEmailCount++;
        if (this.matchData.comm_other_email) iEmailCount++;
        if (iEmailCount > 1) {
            this.emailHasMultiple = true;
        }
        else if (iEmailCount == 1) {
            if (this.matchData.comm_email) {
                this.emailCallClick = 'mailto:' + this.matchData.comm_email;
            }
            else if (this.matchData.work_email) {
                this.emailCallClick = 'mailto:' + this.matchData.work_email;
            }
            else if (this.matchData.comm_other_email) {
                this.emailCallClick = 'mailto:' + this.matchData.comm_other_email;
            }
        }
        else {
            this.emailEmpty = true;
        }
    }

    togglePopover(event) {
        let popoverType = event.currentTarget.dataset.popoverid;
        
        if (this[popoverType + 'Empty']) {
            if (popoverType == 'phone')
                this.showToast('No Phone Number Available');
            if (popoverType == 'email')
                this.showToast('No Email Address Available');
        } else if (this[popoverType + 'HasMultiple']) {
            //this[popoverType + 'ShowPopover'] = !this[popoverType + 'ShowPopover'];
            this.sendOpenPopoverEvent(popoverType);
        }
    }

    handleOnMousedown(event) {
        if (event.target.dataset.blockblur) {
            this.doBlur = false;
            this.blockBlur = true;
        }
        else {
            this.doBlur = true;
            this.blockBlur = false;
        }
    }

    handleOnMouseup(event) {
        if (this.doBlur) {
            setTimeout(() => {
                this.phoneShowPopover = false;
                this.emailShowPopover = false;
                this.doBlur = false;
                this.sendBlurEvent();
            }, 50);
        }
    }

    handleBlur(event) {
        if (!this.blockBlur) {
            this.doBlur = true;
        }
        else {
            this.blockBlur = false;
        }
    }

    showToast(message) {
        const toast = new ShowToastEvent({
            message: message,
            variant: 'error',
            mode: 'dismissable'
        });
        
        this.dispatchEvent(toast);
    }

    sendOpenPopoverEvent(type) {
        let closePopoverEvent = new CustomEvent('openpopover', { detail : this.matchData.id + '-' + type });
        this.dispatchEvent(closePopoverEvent);
    }

    sendBlurEvent() {
        let blurEvent = new CustomEvent('blur');
        this.dispatchEvent(blurEvent);
    }

    @api
    closePopovers(detail) {
        let id = detail.split('-')[0];
        let type = detail.split('-')[1];

        this.phoneShowPopover = false;
        this.emailShowPopover = false;

        if (this.matchData.id == id) {
            this[type + 'ShowPopover'] = true;
        }
    }

    @api
    closeAllPopovers() {
        this.phoneShowPopover = false;
        this.emailShowPopover = false;
    }


    onViewResume(event){
        let id = event.target.id.split('-')[0];
        event.preventDefault(); 
        let params = {'Id':id, 'contact_id':this.matchData.contact_id,'tab':'Resume'};
        const selectedEvent = new CustomEvent('selected', { detail: params });
        this.dispatchEvent(selectedEvent);
    }

    onViewActivities(event){
        let selectedId = event.target.id.split('-')[0];
        event.preventDefault();
        let params = {'contact_id':selectedId, 'Id':this.matchData.id,'tab':'Activities'};
        const selectedEvent = new CustomEvent('selected', { detail: params });
        this.dispatchEvent(selectedEvent);
    }

    addActivity(event) {
        event.stopPropagation();
        
        let contactId = event.target.dataset.contactid;

        const payload = { talentId: contactId };
        publish(this.messageContext, triggerModal, payload);
    }
  
}