import { api, LightningElement, track } from 'lwc';
import rsrc from "@salesforce/resourceUrl/ATS_Empty_State_Images";
import USER_ID from '@salesforce/user/Id';


export default class LwcAllocatedReqModal extends LightningElement {


    @api showModal = false;
    tabContent = '';
    error;
    @track resumesloading = false;
    @api selectedResume;
    currentuserid;

    showActivities = false;

    constructor() {
        super();
        this.currentuserid = USER_ID;
    }
    @api get resume() {
        return this._resume;
    }
    set resume(res) {
        this._resume = res;
        if (this._resume) {
            this.resumesloading = true;
        }
    }

    @api get activities() {
        return this._activities;
    }
    set activities(res) {
        this._activities = res;
        this.showActivities = this._activities.length > 0;
    }


    @api get activetab(){
        return this._activetab;
    }

    set activetab(value){
        this._activetab = value;
    }

    /** 
  *renders resume when it is available form response;
     */
    renderedCallback() {

        if(this._activetab=='Resume'){
            const container = this.template.querySelector('.resumecontainer');
            if (container) {
                container.innerHTML = this._resume;
            }
        }
        
    }


    get imageUrl() {
        return `${rsrc}/slds-empty-state-images/startImage.svg`;
    }

    handleActive(event) {
        this.activetab = event.target.value;

        if(this._activetab=='Resume'){
            
            setTimeout(() => {
                const container = this.template.querySelector('.resumecontainer');
                if (container) {
                    container.innerHTML = this._resume;
                }
            }, 10);
            
            
        }
    }

    /** 
      *close modal popup in lwcallocatedreq
         */
    handleCloseModal(event) {
        this.showModal = false;
        event.preventDefault();
        const selectedEvent = new CustomEvent('closemodal', { detail: this.showModal });
        this.dispatchEvent(selectedEvent);
    }

   
}