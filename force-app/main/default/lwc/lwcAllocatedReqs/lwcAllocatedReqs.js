import { LightningElement, wire, track, api } from 'lwc';

import getContActivites from '@salesforce/apex/BaseController.performServerCall';
import getAllocatedOptions from '@salesforce/apex/AllocatedReqController.getAllocatedOptions';
import getTalentMatches from '@salesforce/apex/AllocatedReqController.getTalentMatches';
import getContactActivity from '@salesforce/apex/AllocatedReqController.getContactActivity';
import getPermissions from '@salesforce/apex/AllocatedReqController.getPermissions';
import getTopMatches from '@salesforce/apex/AllocatedReqController.getTopMatches';

import getSearchResume from '@salesforce/apex/SearchController.search';

import rsrc from "@salesforce/resourceUrl/ATS_Empty_State_Images"

import CONNECTED_Summer18URL from '@salesforce/label/c.CONNECTED_Summer18URL';

import Id from '@salesforce/user/Id';
import { getRecord } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/User.Name';
//import COMPANY_FIELD from '@salesforce/schema/User.CompanyName';

import hasRecentlyAppliedPermission from '@salesforce/customPermission/ShowRecentlyAppliedTabWidget';

export default class LwcAllocatedReqs extends LightningElement {
    radiusValue = '30';
    kmsvalue = 'mi';
    resultContainerClass = 'resultsContainer';
    resultContainerRecentlyClass = 'resultsContainer'

    radiusOptions = [
        { label: 5, value: 5, selected: false },
        { label: 10, value: 10, selected: false },
        { label: 20, value: 20, selected: false },
        { label: 30, value: 30, selected: false },
        { label: 40, value: 40, selected: false },
        { label: 50, value: 50, selected: false },
        { label: 75, value: 75, selected: false },
        { label: 100, value: 100, selected: false },
    ];

    kmsOptions = [
        { label: 'MI', value: 'mi', selected: true },
        { label: 'KM', value: 'km', selected: false }
    ];

    _matches = [];
    get matches() {
        return this._matches;
    }
    set matches(value) {
        this._matches = value;
        this.dedupe();
    }

    allApplied = [];
    recentlyAppliedOutput = [];

    _recentlyApplied = [];
    get recentlyApplied() {
        return this._recentlyApplied;
    }
    set recentlyApplied(value) {
        this._recentlyApplied = value;
        this.dedupe();
    }
    
    get showRecentlyApplied() {
        return hasRecentlyAppliedPermission;
    }

    checkRadius() {
        let radius = localStorage.getItem('AllocatedReqradius');
        for (let opt of this.radiusOptions) {
            if (radius && opt.value == radius) {
                opt.selected = true;
                this.radiusValue = opt.value;
                break;
            } else if (!radius && opt.value == 30) {
                opt.selected = true;
                this.radiusValue = opt.value;
                break;
            } else {
                opt.selected = false;
                this.radiusValue = opt.value;
            }
        }
    }

    //to get user information
    userData;
    @wire(getRecord, { recordId: Id, fields: [NAME_FIELD,'User.CompanyName'] }) 
    wiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading user',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.userData = data;
            let companyName = data.fields.CompanyName.value;
            let radiusUnit = localStorage.getItem('AllocatedReqradiusUnit');
            for (let opt of this.kmsOptions) {
                if (radiusUnit && opt.value == radiusUnit) {
                    opt.selected = true;
                    this.kmsvalue = radiusUnit;
                    break;
                } else if (companyName != 'AG_EMEA' && !radiusUnit && opt.value == 'mi') {
                    opt.selected = true;
                    this.kmsvalue = 'mi';
                    break;
                } else if(companyName == 'AG_EMEA'  && !radiusUnit && opt.value == 'km'){
                    opt.selected = true;
                    this.kmsvalue = 'km';
                    break;
                }else {
                    opt.selected = false;
                }
            }
        }
    }


    userId = Id;
    viewAll = '';
    trackingGuid = '';

    allocatedReqs;
    selectedReq;
    selectedReqUrl;

    resumeAccountIds = [];
    resumeAccountIdsRecent = [];
    logicCalled = '';
    showMatches = false;
    showRecentMatches = false;
    hideTalentmatches = false;
    error;

    isLoading = false;
    loadingCounter = 0;
    isEmpty = true;

    jsonResponse;
    resumeResponse;
    resumeResponseRecent;
    activetab = 'topMatches';
    changeTab = true;
    showModal = false;
    selectedResume;
    mapActivities = new Map();
    @track activities;
    waitForActivities = false;

    get imageUrl() {
        return `${rsrc}/slds-empty-state-images/startImage.svg`;
    }

    connectedCallback() {
        this.isLoading = true;
        this.setTrackingGuid();
        this.showMatches = false;
        this.showRecentMatches = false;
        this.checkRadius();
    }

    @wire(getPermissions) permissions;

    @wire(getAllocatedOptions)
    allocatedReqsWired({ error, data }) {
        if (data) {
            this.allocatedReqs = data;
            this.error = undefined;

            if (data.length > 0) {
                this.isEmpty = false;
                this.selectedReq = data[0].Opportunity.Id;
                this.selectedReqUrl = '/' + this.selectedReq;
                this.viewMatchesUrl = '';
            }
            else {
                this.isEmpty = true;
            }

            this.getTalentMatches();
            this.getTopMatches();
        } else if (error) {
            this.error = error;
            this.allocatedReqs = undefined;
        }
    }

    handleOpportunityChange(event) {
        this.changeTab = true;
        this.getTalentMatches(event);
        this.getTopMatches();
    }

    /**
     * Get Matches from API
     */
    getTalentMatches(event) {
        if (event) {
            this.selectedReq = event.target.value;
            this.selectedReqUrl = '/' + this.selectedReq;
            this.resumeAccountIds = [];
            this.logicCalled = '';
        }

        let context = this;
        context.isLoading = true;

        let qs = this.createQueryString(this.selectedReq, true);

        getTalentMatches({ queryString: qs }).then((result) => {
            let jsonData = JSON.parse(result);
            //for resume filtering
            this.jsonResponse = jsonData;

            // User doesn't have permission
            if (jsonData == undefined) {
                context.activetab = 'recentlyApplied';
                context.showMatches = false;
                context.showRecentMatches = false;
                context.hideTalentmatches = false;
                context.isLoading = false;
                context.matches = [];
                return;
            }
            else {
                if (context.changeTab)
                    context.activetab = 'topMatches';
            }

            if (jsonData.fault != undefined) {
                context.faultString = jsonData.fault.faultstring;
                context.showMatches = false;
                context.showRecentMatches = false;
                context.hideTalentmatches = true;
                context.isLoading = false;
                context.matches = [];
                return;
            }

            if (jsonData.response.hits.hits.length > 0) {
                if(jsonData.response.hits.hits.length <= 4) {
                    this.resultContainerClass = 'resultsContainer resultsContainerOneItem';
                } else {
                    this.resultContainerClass = 'resultsContainer';
                }
                context.showMatches = false;
                context.showRecentMatches = false;
                context.isLoading = false;
                this.parseData(jsonData, 'matches');
            }

        }).catch((error) => {
            context.isLoading = false;            
        });
    }

    parseData(jsonData, outputVar) {
        let context = this; 

        let accountIds = [];// for resumes
        let contactList = [];
        let tempResults = [];

        function pastDays (obj) {
            let {value, raw} = obj;
            if(value === null || raw <= 0) return null;
            let today = new Date().toISOString();
            let d2 = new Date(today.substring(0,4), parseInt(today.substring(5,7)) - 1, today.substring(8,10));
            let d1 = new Date(value.substring(0,4), parseInt(value.substring(5,7)) - 1, value.substring(8,10));
            const result = Math.ceil(Math.abs(d2 - d1) / (1000 * 60 * 60 * 24));
            if (result > 90) return null
            return {
                string: result === 0 ? 'today' : (value > today ? `in ${result} days` : `${result} days`), 
                bool: result === 0,
            }
        }

        jsonData.response.hits.hits.forEach(function (item) {
            let newItem = {};
            newItem.id = item._id;
            newItem.name = item._source.given_name?.toLowerCase() + ' ' + item._source.family_name?.toLowerCase() ;
            newItem.contact_id = item._source.contact_id;
            newItem.contact_url = '/' + item._source.contact_id;
            newItem.position_title = item._source.employment_position_title ? item._source.employment_position_title.toLowerCase() : '';
            
            newItem.countrycode = '';
            newItem.lastActivitydate = item._source.qualifications_last_activity_date;
            
            // iterating to resume account Ids
            context.resumeAccountIds.forEach(key => {
                if (key === item._id) {
                    if(item._source.qualifications_last_resume_modified_date) {
                        let resumeDate = item._source.qualifications_last_resume_modified_date.split('T')[0];
                        let datearray = resumeDate.split("-");
                        let formattedResumedate = datearray[1] + '/' + datearray[2] + '/' + datearray[0].substr(-2);
                        newItem.lastResumeUpdatedDate = ' (Updated ' + formattedResumedate + ')';
                    }

                }
            });
            
            // iterating to resume account Ids
            context.resumeAccountIdsRecent.forEach(key => {
                if (key === item._id) {
                    if(item._source.qualifications_last_resume_modified_date) {
                        let resumeDate = item._source.qualifications_last_resume_modified_date.split('T')[0];
                        let datearray = resumeDate.split("-");
                        let formattedResumedate = datearray[1] + '/' + datearray[2] + '/' + datearray[0].substr(-2);
                        newItem.lastResumeUpdatedDate = ' (Updated ' + formattedResumedate + ')';
                    }

                }
            });
            
            newItem.candidate_status = item._source.candidate_status;
            if (item._source.skills)
                newItem.skillset =  item._source.skills.join(', ');

            // Phone and email
            newItem.home_phone = item._source.communication_home_phone;
            newItem.other_phone = item._source.communication_other_phone;
            newItem.mobile_phone = item._source.communication_mobile_phone;
            newItem.work_phone = item._source.communication_work_phone;

            newItem.comm_email = item._source.communication_email;
            newItem.work_email = item._source.communication_work_email;
            newItem.comm_other_email = item._source.communication_other_email;
            newItem.recent_submittal = pastDays(item.fields.candidate_interest_insight[0].details.recent_submittal);
            newItem.recent_resume_update = pastDays(item.fields.candidate_interest_insight[0].details.recent_resume_update);
            newItem.recent_applications = pastDays(item.fields.candidate_interest_insight[0].details.recent_applications);
            newItem.close_to_end_date = pastDays(item.fields.candidate_interest_insight[0].details.close_to_end_date);
            newItem.available_soon = pastDays(item.fields.candidate_interest_insight[0].details.available_soon);
            newItem.in_pipeline = item.fields.candidate_interest_insight[0].details.in_pipeline.value;
            newItem.g2_comment_eagerness = pastDays(item.fields.candidate_interest_insight[0].details.g2_comment_eagerness);

            let locationString = '';
            if (item._source.city_name != null && item._source.country_sub_division_code != null) {
                locationString = item._source.city_name.toLowerCase() + ', ';
                locationString += item._source.country_sub_division_code.length == 2 ? item._source.country_sub_division_code.toUpperCase() : item._source.country_sub_division_code.toLowerCase();
            }
            else {
                if (item._source.city_name != null) {
                    locationString = item._source.city_name.toLowerCase();
                }
                else if (item._source.country_sub_division_code != null) {
                    locationString = item._source.country_sub_division_code.length == 2 ? item._source.country_sub_division_code.toUpperCase() : item._source.country_sub_division_code.toLowerCase();
                }
            }

            if (item._source.country_code != null)
                newItem.location = locationString + (item._source.country_code.toLowerCase() == 'united states' ? ', USA' : ', ' + item._source.country_code.toLowerCase());
            else 
                newItem.location = locationString;

            contactList.push(item._source.contact_id);
            tempResults.push(newItem);
            accountIds.push(item._id); //for resumes
        });

        if (outputVar == 'recentlyAppliedOutput') {
            if(tempResults.length <= 4) {
                this.resultContainerRecentlyClass = 'resultsContainer resultsContainerOneItemRecently';
            } else {
                this.resultContainerRecentlyClass = 'resultsContainer';
            }
        }

        context.getContactActivities(tempResults, contactList, outputVar);
        
        if (accountIds.length > 0 && this.logicCalled === '' && outputVar != 'recentlyAppliedOutput' ) {
            this.getResumesBasedOnTopMacthes(accountIds); //getting all resumes based on top matches .. 
        } else {
            context.isLoading = false;
        }
    }

    getContactActivities(matches, cIds, outputVar) {
        let context = this;

        getContactActivity({listOfContactIds:cIds}).then((result) => {
            let jsonData = result;

            matches.forEach(function(item){

                // Get matching activity
                let activity = jsonData.filter(function(act) {
                    return act.Id == item.contact_id;
                });

                if (activity[0] && activity[0].ActivityHistories && activity[0].ActivityHistories[0]) {
                    item.activity_date = activity[0].ActivityHistories[0].ActivityDate;
                    item.activity_name = activity[0].ActivityHistories[0].CreatedBy.Name;
                }
            });
            
			if(matches != null && matches != ''){
				context[outputVar] = matches;
                context.showMatches = true;
                context.hideTalentmatches =true;
			}
        }).catch((error) => {
            console.log(error);
        });
    }

    /**
     * Create query string for match request
     */
    createQueryString(oppId, isTopMatches) {
        let secondsSinceEpoch = Math.round(new Date().getTime() / 1000);

        let qs = '';
        qs += isTopMatches ? 'size=10' : 'size=30'; // If there's more than 10, user can use the 'View All' link
        qs += '&from=0'; // Always start at 0
        qs += '&type=job2talent'; // req2talent
        qs += '&docId=' + oppId; // Selected Opportunity Id
        qs += '&appId=ATS_AllocatedReqs'; // Unique value for this feature
        qs += '&explainDimension=true';
        qs += '&insight=full';
        qs += '&requestId=' + this.trackingGuid; // Set to GUID created the first time this loads
        qs += '&transactionId=' + this.trackingGuid + '_' + secondsSinceEpoch; // Use request GUID plus timestamp to get unique ID
        //qs += '&rescore=true';
				
        const req = this.allocatedReqs.filter(rec => rec.OpportunityId === oppId);
        if(req[0]?.Opportunity?.Req_GeoLocation__Latitude__s!=null && req[0]?.Opportunity?.Req_GeoLocation__Longitude__s!=null){
            qs += '&location=[{"latlon":['+encodeURI(req[0]?.Opportunity?.Req_GeoLocation__Latitude__s)+','+encodeURI(req[0]?.Opportunity?.Req_GeoLocation__Longitude__s)+'],"geoRadius":"'+encodeURI(this.radiusValue)+'","geoUnitOfMeasure":"'+encodeURI(this.kmsvalue)+'","filter":false}]'
        }
        
        //console.log('alex-- qs', qs);

        return qs;
    }

    handleActive(event) {
        this.activetab = event.target.value;
    }
    /**
     * Navigate to main match search results
     */
    navigateToMatches(event) {
        event.stopPropagation();
        let selectedOppy = this.allocatedReqs.filter(function(opp) {
            return opp.Opportunity.Id == this.selectedReq;
        }.bind(this));

        let viewAll = CONNECTED_Summer18URL + '/n/ATS_CandidateSearchOneColumn' +
            '?c__matchJobId=' + selectedOppy[0].Opportunity.Id +
            '&c__srcName=' + selectedOppy[0].Opportunity.Name +
            '&c__srcLoc=' + this.buildLocationStr(selectedOppy[0].Opportunity) + 
            '&c__noPush=true' +
            '&c__radius=' + this.radiusValue +
            '&c__radiusUnit=' + this.kmsvalue +
            '&c__sourceType=W2C' +
            '&c__tabType=' +this.activetab+
            '&c__srcTitle=' + selectedOppy[0].Opportunity.Req_Job_Title__c +
            '&c__srcLat=' + selectedOppy[0].Opportunity.Req_GeoLocation__Latitude__s +
            '&c__srcLong=' + selectedOppy[0].Opportunity.Req_GeoLocation__Longitude__s;

        window.open(viewAll);
    }

    buildLocationStr(opp) {
        let srcLocStr = opp.Req_Worksite_City__c ? opp.Req_Worksite_City__c + ', ' : '';
        srcLocStr = srcLocStr + (opp.Req_Worksite_State__c ? opp.Req_Worksite_State__c + ', ' : '');
        srcLocStr = srcLocStr + (opp.Req_Worksite_Country__c ? opp.Req_Worksite_Country__c : '');
        return srcLocStr;
    }

    /**
     * Handled when popover is opened
     */
    handleOpenPopover(event) {
        let openedPopover = event.detail;
        let resultItems = this.template.querySelectorAll('c-lwc-allocated-req-item');

        resultItems.forEach(function (item) {
            item.closePopovers(openedPopover);
        });
    }

    /**
     * Handled when blur event is called from lwcAllocatedReqItem
     */
    handleBlur() {
        let resultItems = this.template.querySelectorAll('c-lwc-allocated-req-item');

        resultItems.forEach(function (item) {
            item.closeAllPopovers();
        });
    }

    /**
     * Sets this.trackingGuid to a new GUID for API requests.
     */
    setTrackingGuid() {
        let guid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });

        this.trackingGuid = guid;
    }


    /**
         *opens modal for the top match and display resume
         */
    viewSelectedResume(event) {
        const selectedRecord = event.detail;
        console.log('selectedRecord--471>'+JSON.stringify(selectedRecord));
        console.log('resumeresponse--472>'+JSON.stringify(this.resumeResponseRecent));
        if (this.resumeResponse != undefined && this.resumeResponse.response.hits.hits.length > 0) {
            let resumes = this.resumeResponse.response.hits.hits.find(item => item._id === selectedRecord.Id);
            if (resumes) {
                this.selectedResume = resumes._source.resume_html;
            } else if (this.resumeResponseRecent != undefined && this.resumeResponseRecent.response.hits.hits.length > 0){
                let resumesRecents = this.resumeResponseRecent.response.hits.hits.find(item => item._id === selectedRecord.Id);
                if (resumesRecents) {
                    this.selectedResume = resumesRecents._source.resume_html;
                } else {
                
                    this.selectedResume = undefined;
                }
            } else {
			
				this.selectedResume = undefined;
			}
        }
        // For Recent applied matches 
        if (this.resumeResponse === undefined && this.resumeResponseRecent != undefined && this.resumeResponseRecent.response.hits.hits.length > 0) {
            let resumes = this.resumeResponseRecent.response.hits.hits.find(item => item._id === selectedRecord.Id);
            if (resumes) {
                this.selectedResume = resumes._source.resume_html;
            } else {
			
				this.selectedResume = undefined;
			}
        }
        
        const selectedContactId = selectedRecord.contact_id; //this is for activity
        if(this.mapActivities!=null && this.mapActivities.has(selectedContactId)) {
            this.waitForActivities = false;
            this.activities = this.mapActivities.get(selectedContactId);
        }else{
            this.waitForActivities = true;
            this.getContactActivitesforTopMacthes(selectedContactId);
        }
                    
        this.activetab = selectedRecord.tab;//this is selectedtab

        if (!this.waitForActivities)
            this.showModal = true;

    }

    onCloseModal(event) {
        const modalpopup = event.detail;
        this.showModal = modalpopup;
    }


    /** 
         * gets all resumes based on topmatches accountIds
         */
    async getResumesBasedOnTopMacthes(accountIds) { //get 
        let resumeIds = [];
        let qs = this.createQueryStringforResumes(accountIds);
        console.log('accountIds---493-->'+accountIds);
        
        getSearchResume({ serviceName: 'Resume Highlight', queryString: qs, })
            .then((result) => {
                this.isLoading = true;
                let resumeHtml = JSON.parse(result);
                this.resumeResponse = resumeHtml;
                console.log('resumeHtml499--->'+JSON.stringify(resumeHtml));
                if (resumeHtml.response.hits.hits.length > 0) {
                    resumeHtml.response.hits.hits.forEach(function (item) {
                        if(item._source.resume_html != null && item._source.resume_html != 'null'){
                            resumeIds.push(item._id);
                         }
                    });
                }
                this.resumeAccountIds = resumeIds;
                this.getTalentMatches();
                this.logicCalled = 'Yes';
                this.loadingCounter++;
                if (this.loadingCounter > 1) {
                    this.isLoading = false;
                }
            }).catch((error) => {
                this.resumeResponse = undefined;
                this.isLoading = false;
                console.log('getSearchResume error', error);
            });
    }


    /** 
      *created a querystring for all accountIds in the topmatches for API requests.
         */
    createQueryStringforResumes(accountIds) {
        let qs = '';
        qs += 'id=' + accountIds.join(',');
        qs += '&highlight.fields=resume_html&flt.group_filter=all&size=' + accountIds.length + '&_source_includes=resume_html';
        return qs;
    }

    //getting activities based in Selected Id
    async getContactActivitesforTopMacthes(selectedContactId) {
        let params = { 'recordId': selectedContactId, 'maxItems': '25', 'filterCriteria': '', 'getActivityIcons': true }
        getContActivites({ className:'ATS', subClassName:'TalentActivityFunctions', methodName: 'getTalentPastActivities', parameters: params })
            .then((result) => {
                this.mapActivities.set(selectedContactId,result);
                this.activities = this.mapActivities.get(selectedContactId);
                if (this.waitForActivities) {
                    this.showModal = true;
                }
            }).catch((error) => {
                console.log('activities error', error);
                this.activities = undefined;
            });
    }

    handleDropdownSelect(event) {
        this.changeTab = false;
        let data = event.detail;
        console.log(data)
        if (data.label === 'radius') {
            localStorage.setItem('AllocatedReqradius', data.value);
            this.radiusValue = data.value;
            this.getTalentMatches();
            this.getTopMatches();
        } else {
            localStorage.setItem('AllocatedReqradiusUnit', data.value);
            this.kmsvalue = data.value;
            this.getTalentMatches();
            this.getTopMatches();
        }
    }

    /**
     * Get matches recently applied
     */
    getTopMatches() {
        let context = this;
        let qs = this.createQueryString(this.selectedReq, false);
        qs += '&flt.recent_application_date=%5Bnow-45d%2Fd%2Cnow-0d%2Fd%5D';

        getTopMatches({ queryString: qs }).then((result) => {
            let jsonData = JSON.parse(result);

            context.allApplied = JSON.parse(JSON.stringify(jsonData));
            context.recentlyApplied = JSON.parse(JSON.stringify(jsonData));
            
        }).catch((error) => {
            console.log('alex-- error', error);
        });
    }


    /** 
     * gets all resumes based on recently added accountIds
     */
    async getResumesBasedOnrecentlyAddedMacthes(accountIds) { //get 
        let resumeIds = [];
        let qs = this.createQueryStringforResumes(accountIds);
        //this.isLoading = true;
        getSearchResume({ serviceName: 'Resume Highlight', queryString: qs, })
            .then((result) => {
                this.isLoading = true;
                let resumeHtml = JSON.parse(result);
                this.resumeResponseRecent = resumeHtml;
                console.log('resumeHtml596--->'+JSON.stringify(resumeHtml));
                if (resumeHtml.response.hits.hits.length > 0) {
                    resumeHtml.response.hits.hits.forEach(function (item) {
                        if(item._source.resume_html != null && item._source.resume_html != 'null'){
                           resumeIds.push(item._id);
                        }
                    });
                }
                this.resumeAccountIdsRecent = resumeIds;
                this.parseData(this._recentlyApplied, 'recentlyAppliedOutput');
                this.isLoading = false;
            }).catch((error) => {
                this.resumeResponseRecent = undefined;
                this.isLoading = false;
                console.log('getSearchResume error', error);
            });
    }

    /**
     * De dupe with top matches
     */
     dedupe() {
        let newAccountIds = [];
        // Only dedupe if both lists exist
        if (this.matches.length > 0 && this.allApplied.response.hits.hits.length > 0) {
            let topMatchesIds = [];
            topMatchesIds = this.matches.map(m => m.id);

            let newApplied = [];

            this.allApplied.response.hits.hits.forEach((ra) => {
                if (!topMatchesIds.includes(ra._id)) {
                    if (newApplied.length < 20) {
                        newApplied.push(ra);
                        newAccountIds.push(ra._id);
                    }
                }
            });
            this._recentlyApplied.response.hits.hits = newApplied;
        }

        else if (this.matches.length == 0 && this.allApplied.response.hits.hits.length > 0) {
            let newApplied = [];
            
            this.allApplied.response.hits.hits.forEach((ra) => {
                if (newApplied.length < 20) {
                    newApplied.push(ra);
                    newAccountIds.push(ra._id);
                }
            });
            console.log('newAccountIds======680-==>>>>>>>>'+newAccountIds);
            this._recentlyApplied.response.hits.hits = newApplied;
        }
        if(this._recentlyApplied.response.hits.hits.length > 0 ) {
            this.showRecentMatches = true;
        }
        //console.log('alex-- _recentlyApplied', this._recentlyApplied);
        this.parseData(this._recentlyApplied, 'recentlyAppliedOutput');
        if(newAccountIds.length > 0){
            this.getResumesBasedOnrecentlyAddedMacthes(newAccountIds);
        }
        
    }
}