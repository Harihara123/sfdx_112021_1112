import {LightningElement, api, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LwcCSCCheckRequestInputFields extends LightningElement {
	@api originId = '';
	proceedSubmit = false;
	subVendorRequired = false;
	showSection = false;
	isLoading = false;
	@api formConfig = {};
	@api backupReqs = {};
	condFields = {};
	fieldsPreLoaded = true;
	@track isAddressChange = false;
	@track isSubReasonCode = true;
	loadedBackups = '';

	// fields for Backup load
	@track brSubReasonCode = '';
	@track brActionReq = '';
	@track brAgreedBonus = '';
	@track brClientAgreed = '';

	// attribute for conditional fields.
	@track showCRSubSec = false;
	@track isBonusBill = false;

	//attribute for case readonly
	@api caseReadonly;

	//Connected Callback
	 connectedCallback() {
		this.isLoading = true;
        this.showSection = false;
    }

	@api 
    submitCRForm(){
		//console.log('Submit from container=====>>>>>');
		this.proceedSubmit = true;
		this.template.querySelectorAll('lightning-input-field').forEach(element => {
		console.log('element title - '+element.fieldName+' - '+element.required+' - '+element.value);
			if(element.required === true && (element.value === null || element.value === '')){
				this.proceedSubmit = false;
				element.reportValidity();
			}
        });
		//console.log('Proceed====>>>>>'+this.proceedSubmit);
		if(this.proceedSubmit){
			this.template.querySelector('lightning-record-edit-form').submit();
		} 

        this.dispatchEvent(new CustomEvent('validation', { detail: this.proceedSubmit }));
    }

	//Handler when lightning record form is loadedFacets
	 handleFormLoad(e) {
		this.isLoading = true;
		const record = e.detail.records;
		let subReasoncode;
		if(record && this.fieldsPreLoaded){
			console.log('onLoad===backupReqs======>>>>>'+this.backupReqs);
			console.log('BONS=====>>>>>>>>>'+JSON.stringify(this.backupReqs['BONS - Bonus Billable-Check/Cheque-Yes-Yes'].item));
			const fields = record[this.originId].fields;
			// on load form based on action required
			const actionReq = (fields.CR_Action_Required__c || {}).value;
			if(this.brSubReasonCode === ''){
				subReasoncode = (fields.Sub_Reason_Code__c || {}).value;
			}else{
				subReasoncode = this.brSubReasonCode;
			}
			console.log('subReasoncode===69===>>>>>>>'+subReasoncode);
			//on Load Based on Sub ReasonCode
			if(actionReq === "Check/Cheque" && (subReasoncode === "BONS - Bonus Billable" || subReasoncode === "NONB - NON-BILLABLE BONUSES")){
				this.showCRSubSec = true;
			}
			if(actionReq === "Check/Cheque" && subReasoncode === "BONS - Bonus Billable"){
				this.isBonusBill = true;
			}

			const agreedBonus = (fields.Does_the_agreed_bonus_amount_need_to_be__c || {}).value;
			const clientAgreed = (fields.Has_the_client_agreed_to_be_billed_for__c || {}).value;
			if (actionReq !== '' && actionReq) {
				const configActionRequired = 'CR-'+actionReq;
                if (this.formConfig[configActionRequired].colOne.fields) {
                    this.isLoading = true;
					this.showSection = true;
					this.condFields = this.formConfig[configActionRequired];
                }
			}
			//on Load Based on Action Required
			if(actionReq !== "Void Only" && actionReq !== "Void and Reissue Check/Cheque"){
				this.isSubReasonCode = true;
			}else{
				this.isSubReasonCode = false;
			}

			// on Load based on Address change
			const addressChange = (fields.Is_the_Check_being_sent_to_an_address__c || {}).value;
			//console.log('addressChange=====>>>>>>>>'+addressChange);
			if(addressChange !== "" && addressChange && addressChange === "Yes"){
				this.isAddressChange = true;
			}

			// load Backup Reqs in onload
			this.loadBackupReqs(subReasoncode, actionReq, agreedBonus, clientAgreed);
        }
		
		if(e.detail){
            this.isLoading = false;
        }
	}

	//Handler for Action required
	handleActionRequired(e){
		const selectionType = e.target.value;
		this.brAgreedBonus = '';
		this.brClientAgreed = '';
		this.brSubReasonCode = '';
		//console.log('selectionType======>>>>>>'+selectionType);
		const configSelectionType = 'CR-'+selectionType;
		this.showSection = false;
		if(selectionType !== "" && this.formConfig[configSelectionType].colOne.fields){
			this.isLoading = true;
			this.showSection = true;
			this.condFields = this.formConfig[configSelectionType];
			this.fieldsPreLoaded = false;
		}
		if(selectionType !== ""){
			this.loadBackupReqs('', selectionType, '', '');
		}else{
			this.loadBackupReqs('', '', '', '');
		}
		if(selectionType !== "Void Only" && selectionType !== "Void and Reissue Check/Cheque"){
			this.isSubReasonCode = true;
		}else{
			this.isSubReasonCode = false;
		}
		// added to stop loading
		//console.log('e.detail=======>>>>>>>>'+e.detail);
		if(e.detail){
            this.isLoading = false;
        }
	}

	//Handler for Check Address other than bank
	handleCheckAddress(e){
		if(e.detail.value === "Yes"){
			this.isAddressChange = true;
		}else{
			this.isAddressChange = false;
		}
	}

	// Handler when lightning record form submitted success
	handleFormSuccess(e){
        this.showToast('Success', 'Form Updated Successfully!', 'success');
    }

	// Handler when lightning record form return error
	handleFormError(e){
		this.showToast('Error', 'An Unexpected Error Occured, please contact your Admin.', 'error');
	}

	//Handler for Sub Reason Code
	handleSubReason(e){
		const selectionType = e.target.value;
		console.log('selectionType====>>>'+selectionType);
		console.log('sub reason====>>>>>'+e.detail.value);
		this.brAgreedBonus = '';
		this.brClientAgreed = '';
		if(e.detail.value === "BONS - Bonus Billable"){
			this.subVendorRequired = true;
		}else{
			this.subVendorRequired = false;
		}
		
		if(selectionType === "BONS - Bonus Billable" || selectionType === "NONB - NON-BILLABLE BONUSES"){
			this.showCRSubSec = true;
		}else{
			this.showCRSubSec = false;
		}
		if(selectionType === "BONS - Bonus Billable"){
			this.isBonusBill = true;
		}else{
			this.isBonusBill = false;
		}
		if(e.detail.value !== ""){
			this.loadBackupReqs(e.detail.value, '', '', '');
		}else{
			this.loadBackupReqs('', '', '', '');
		}
	}

	//Handler for Agreed Bonus
	agreedBonusHandler(e){
		if(e.target.value !== ""){
			this.loadBackupReqs('', '', e.target.value, '');
		}else{
			this.loadBackupReqs('', '', '', '');
		}
	}

	//Handler for Client Agreed
	clientAgreedHandler(e){
		if(e.target.value !== ""){
			this.loadBackupReqs('', '', '', e.target.value);
		}else{
			this.loadBackupReqs('', '', '', '');
		}
	}

	//Mthod to load backup reqs 
	loadBackupReqs(subReasonCode, actionReq, agreedBonus, clientAgreed){
		console.log('subReasonCode======>>>>>>>'+subReasonCode);
		console.log('actionReq======>>>>>>>'+actionReq);
		console.log('agreedBonus======>>>>>>>'+agreedBonus);
		console.log('clientAgreed======>>>>>>>'+clientAgreed);
		if(subReasonCode !== '' && subReasonCode !== null && subReasonCode !== undefined){
			this.brSubReasonCode = subReasonCode;
		}
		if(actionReq !== '' && actionReq !== null && actionReq !== undefined){
			this.brActionReq = actionReq;
		}
		if(agreedBonus != '' && agreedBonus !== null && agreedBonus !== undefined){
			this.brAgreedBonus = agreedBonus;
		}
		if(clientAgreed != '' && clientAgreed !== null && clientAgreed !== undefined){
			this.brClientAgreed = clientAgreed;
		}
		/*if(this.brSubReasonCode !== '' && this.brActionReq !== ''){
			if(this.backupReqs[this.brSubReasonCode+'-'+this.brActionReq] !== undefined){
				this.loadedBackups = this.backupReqs[this.brSubReasonCode+'-'+this.brActionReq].item;
				this.dispatchEvent(new CustomEvent('loadbackup', { detail: this.loadedBackups }));
			}else{
				this.dispatchEvent(new CustomEvent('loadbackup', { detail: '' }));
			}
		}*/
		console.log('backup=====204=====>>>>>>>'+this.brSubReasonCode+'-'+this.brActionReq+'-'+this.brAgreedBonus+'-'+this.brClientAgreed);
		if(this.backupReqs[this.brSubReasonCode+'-'+this.brActionReq+'-'+this.brAgreedBonus+'-'+this.brClientAgreed] !== undefined){
			this.loadedBackups = this.backupReqs[this.brSubReasonCode+'-'+this.brActionReq+'-'+this.brAgreedBonus+'-'+this.brClientAgreed].item;
			this.dispatchEvent(new CustomEvent('loadbackup', { detail: this.loadedBackups }));
		}else{
			this.dispatchEvent(new CustomEvent('loadbackup', { detail: '' }));
		}
	}

	//Toggle section for controlling fields
	get toggleAccordionSection() {
		//console.log('toggle==Accordian=====>>>>>'+this.showSection);
        if (this.showSection) {
            return '';
        }
        return 'slds-hide';
    }

	//Toggle for Conditional
	get getLoadingState() {
		//console.log('toggle==Loading======>>>>>'+this.formConfig+' ====>>>>>'+this.showSection);
		if (this.formConfig && this.showSection) {
            return true;
        }
        return false;
    }

	// This method to show Tost on Form
	showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'sticky'
        });
        this.dispatchEvent(evt);
    }
}