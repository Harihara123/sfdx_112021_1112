import { LightningElement,  api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LwcCSCPayrollAdjModal extends LightningElement {
    @api recordId = "";
    @api modalData;
    @api tableCols;
	@api isPayroll;
	@api isCheckrequest;
	@api tableColsCR;
    
    @track modalTable = [];
    @track draftValues = [];
    @track updatedValues = [];

    isLoading = false;
    hasUTA = false;
    hasPVWage = false;
	isCheck = false;
	isPay = false;

	get weekendingLabel(){
		return this.isPayroll ? 'Weekending' : 'Weekending Day';
	}

    get hasTableData(){
        return (this.modalTable.length > 0);
    }

    @api formatModalData(data, args) {
		//console.log('isPayroll===30===>>>>>>>'+this.isPayroll);
		//console.log('isCheckrequest==32==>>>>'+this.isCheckrequest);
        const { hasPVWage, hasUTA, grossDiff } = args;
        this.hasUTA = hasUTA;
        this.hasPVWage = hasPVWage;
        this.grossDiff = grossDiff;
		
        
        const formattedData = data.map((item, idx) => {
            const tableId = item.Id;
			let updatedCols = this.tableCols;
			//console.log('updatedCols====41===>>>>>>>>'+updatedCols);
			if(this.isPayroll === true){
				updatedCols = this.checkNegative(item, hasUTA);
			}
			if(this.isCheckrequest === true){
				updatedCols = this.checkNegativeCR(item, hasUTA);
			}
			//console.log('updatedCols====48===>>>>>>>>'+updatedCols);
			const inputs = hasPVWage ? ['Weekday__c', 'Weekending__c', 'CSC_Adjustment_Code__c', 'CSC_Earn_Code__c', 'Job_Req_ID__c', 'CSC_Earnings_Code__c'] : ['Weekday__c', 'Weekending__c', 'CSC_Adjustment_Code__c', 'CSC_Earn_Code__c', 'CSC_Earnings_Code__c'];
            let filteredInputs = {};
            Object.keys(item).forEach(key => {
                if (inputs.includes(key)){
                    filteredInputs = { ...filteredInputs, [key]: item[key] };
                }
            });
                
            return {
                headerName: `Adjustment ${(idx + 1)}`,
                hasPVWage: hasPVWage,
                inputGroup: filteredInputs,
                tableData: [item],
                tableCols: updatedCols,
                id: tableId,
                recId: this.recordId
            };

        });
        this.modalTable = formattedData;
        this.isLoading = false;
    }

    @api
    showModal(){
        this.template.querySelector('c-modal').show();
    }

    handleInputChange(e){
		let inputValue = '';
        //const inputValue = e.detail.value;
        const inputLabel = e.currentTarget.dataset.label;
		//console.log('inputLabel=======>>>>>>'+inputLabel);
		if(inputLabel === 'CSC_Adjustment_Code__c' || inputLabel === 'CSC_Earn_Code__c' || inputLabel === 'CSC_Earnings_Code__c'){
			inputValue = e.detail.value[0];
		}else{
			inputValue = e.detail.value;
		}
		const inputId = e.currentTarget.dataset.id;
        const valueObj = Object.assign({[inputLabel]: inputValue});

        this.updateValues(valueObj, inputId);
    }

    handleTableChange(e){
        const displayValue = e.detail.draftValues[0];
        const key = Object.keys(displayValue)[0];
        const valueObj = Object.assign({ [key]: displayValue[key] });

        this.updateValues(valueObj, displayValue.Id);
    }

    handleAddAdj() {
        this.isLoading = true;
        this.dispatchEvent(new CustomEvent('addrow', { detail: "modal" }));
    }

    handleRemoveRow(e) {
        this.isLoading = true;
        const rowId = e.currentTarget.dataset.id;
        const findIndex = this.updatedValues.findIndex(el => el.Id === rowId);
        if(findIndex !== -1){
            this.updatedValues.splice(findIndex);
        }
        this.dispatchEvent(new CustomEvent('removerow', {detail: rowId}));
    }

	// Copy existing Adjustment
	handleCopyRow(e) {
		console.log('copy row=======>>>>>>');
        this.isLoading = true;
        const rowId = e.currentTarget.dataset.id;
        const findIndex = this.updatedValues.findIndex(el => el.Id === rowId);
        if(findIndex !== -1){
            this.updatedValues.splice(findIndex);
        }
        this.dispatchEvent(new CustomEvent('copyrow', {detail: rowId}));
    }

    @api handleClose() {
        this.template.querySelector('c-modal').hide();
    }

    handleSave() { 
		//console.log('isPayroll======>>>>>>>'+this.isPayroll);
		//console.log('isCheckrequest====>>>>'+this.isCheckrequest);
        let isValid = false;
		//const lightingInputs = this.template.querySelectorAll('lightning-input');
        const lightningInputFields = this.template.querySelectorAll('lightning-input-field');
        const inputLength = lightningInputFields.length;
		let inputCount = 0;

		/* lightingInputs.length +
        lightingInputs.forEach(field => {
			if (field.value !== null && field.value !== ''){
                inputCount++;
            } 
            field.reportValidity();
        });*/

        lightningInputFields.forEach(field => {
			if (field.value !== null && field.value !== '') {
                inputCount++;
            }
            field.reportValidity();
        });
		if(inputLength === inputCount){
            isValid = true;
        }
		const tableValues = this.updatedValues;
        // If nothing is updated, copy over pre-existing table state
		if(tableValues.length === 0){
            this.modalTable.forEach(item => tableValues.push(item.tableData[0]));
        }
		//console.log('tableValues===132===>>>>>>'+JSON.stringify(tableValues));
        tableValues.forEach(row =>{
			//console.log('row===132====>>>>>>'+JSON.stringify(row));
			const dataRow = JSON.stringify(row);
			if(this.isPayroll && (!dataRow.includes('Original_Hours__c') || !dataRow.includes('Correct_Hours__c') || !dataRow.includes('Original_Pay_Rate__c') || !dataRow.includes('Original_Bill_Rate__c') || !dataRow.includes('Correct_Pay_rate__c') || !dataRow.includes('Correct_Bill_Rate__c'))){
				//console.log('row===152====>>>>>>'+JSON.stringify(row));
				isValid = false;
			}
			if(this.isCheckrequest && (!dataRow.includes('Original_Hours__c') || !dataRow.includes('Correct_Hours__c') || !dataRow.includes('Original_Pay_Rate__c') || !dataRow.includes('Correct_Pay_rate__c'))){
				//console.log('row===156====>>>>>>'+JSON.stringify(row));
				isValid = false;
			}
            for(const key in row){
                if(key !== 'Weekday__c' && (row[key] === null || row[key] === "")){
                    isValid = false;
                }
            }
        });
        console.log('isValid=======>>>>>>>'+isValid);
		if (isValid){
            const formatData = this.mapData(tableValues);
			//console.log('formatData====167===>>>>>>>>'+JSON.stringify(formatData));
            this.dispatchEvent(new CustomEvent('save', { detail: formatData }));
            this.template.querySelector('c-modal').hide();
        } else {
            this.showToast('Error', 'Please complete required fields before saving.', 'error');
        }
        
    }
    // Helper functions
    checkNegative(obj) {
		//console.log('this.tableCols========>>>>>>>>>'+this.tableCols);
		const keyMatrix = this.hasUTA ? ["Correct_Hours__c", "Original_Hours__c", "Hours_Difference__c", "Travel_Adv_Pd__c", "Original_Gross__c", "Correct_Gross__c", "Gross_Difference__c", "Receipts_Submtd__c", "Receipts_Submtd__c"] :
            ["Correct_Bill_Rate__c", "Correct_Hours__c", "Original_Bill_Rate__c", "Original_Hours__c", "Original_Pay_Rate__c", "Correct_Pay_rate__c", "Hours_Difference__c", "Pay_Rate_Difference__c", "Bill_Rate_Difference__c", "Original_Gross__c", "Correct_Gross__c", "Gross_Difference__c", "Job_Req_ID__c"];

            
        const keys = Object.keys(obj);
        const filteredKeys = keys.filter(item => keyMatrix.indexOf(item) !== -1);
        const checkNum = [];

        filteredKeys.forEach(el => {
            checkNum.push({
                fieldname: el,
                isNeg: (Math.sign(obj[el]) === -1)
            });
        });

        const updatedCols = this.tableCols.map(col => {
            const findIndex = checkNum.findIndex(item => item.fieldname === col.fieldName);
            const numCheck = ((checkNum[findIndex] || {}).isNeg) ? 'slds-text-color_error' : 'slds-text-color_default';
            return {
                ...col,
                cellAttributes: {
                    class: numCheck
                }
            }

        });

        return updatedCols;
    }

	// Helper functions
    checkNegativeCR(obj) {
		//console.log('this.tableColsCR====CR===>>>>>>>>>'+this.tableColsCR);
		const keyMatrix = ["Correct_Hours__c", "Original_Hours__c", "Original_Pay_Rate__c", "Correct_Pay_rate__c", "Original_Gross__c", "Correct_Gross__c", "Gross_Difference__c"];

            
        const keys = Object.keys(obj);
        const filteredKeys = keys.filter(item => keyMatrix.indexOf(item) !== -1);
        const checkNum = [];

        filteredKeys.forEach(el => {
            checkNum.push({
                fieldname: el,
                isNeg: (Math.sign(obj[el]) === -1)
            });
        });

        const updatedCols = this.tableColsCR.map(col => {
            const findIndex = checkNum.findIndex(item => item.fieldname === col.fieldName);
            const numCheck = ((checkNum[findIndex] || {}).isNeg) ? 'slds-text-color_error' : 'slds-text-color_default';
            return {
                ...col,
                cellAttributes: {
                    class: numCheck
                }
            }

        });

        return updatedCols;
    }

    mapData(data){
        let jsonMap = { "Id": "Record_ID", "Original_Hours__c": "ORGNL_HRS", "Original_Pay_Rate__c": "ORGNL_Pay_RT", "Correct_Hours__c": "Correct_HRS", "Original_Bill_Rate__c": "ORGNL_Bill_RT", "Correct_Pay_rate__c": "Correct_Pay_RT", "Correct_Bill_Rate__c": "Correct_Bill_RT", "Weekending__c": "Weekending", "Weekday__c": "Weekday", "CSC_Adjustment_Code__c": "Adjustment_Code", "CSC_Earn_Code__c": "Earn_Code", "CSC_Earnings_Code__c": "Earnings_Code" };

        if(this.hasPVWage){
            jsonMap = { "Id": "Record_ID", "Job_Req_ID__c": "Job_Req_Id", "Original_Hours__c": "ORGNL_HRS", "Original_Pay_Rate__c": "ORGNL_Pay_RT", "Correct_Hours__c": "Correct_HRS", "Original_Bill_Rate__c": "ORGNL_Bill_RT", "Correct_Pay_rate__c": "Correct_Pay_RT", "Correct_Bill_Rate__c": "Correct_Bill_RT", "Weekending__c": "Weekending", "Weekday__c": "Weekday", "CSC_Adjustment_Code__c": "Adjustment_Code", "CSC_Earn_Code__c": "Earn_Code" };
        }
        if (this.hasUTA){
            jsonMap = { "Id": "Record_ID", "Original_Hours__c": "ORGNL_HRS", "Correct_Hours__c": "Correct_HRS", "Weekending__c": "Weekending", "Weekday__c": "Weekday", "CSC_Adjustment_Code__c": "Adjustment_Code", "CSC_Earn_Code__c": "Earn_Code", "Travel_Adv_Pd__c": "Travel_Adv_Pd", "Receipts_Submtd__c": "Receipts_Submtd" };
        } 
        
        const mappedData = data.map(item => {
            let newItem = {};
            for (let key in item) {
                if(item[key] !== null){
                    console.log('Key===272===>>>>>>>>>>'+key+' value======>>>>>>>'+item[key]);
                    if (jsonMap.hasOwnProperty(key)){
                        newItem = { ...newItem, [jsonMap[key]]: item[key].toString() };
                    }
                }else if(item[key] === null){
                    console.log('Key===277===>>>>>>>>>>'+key+' value======>>>>>>>'+item[key]);
                    if (jsonMap.hasOwnProperty(key)){
                        newItem = { ...newItem, [jsonMap[key]]: item[key] };
                    }
                }
                
            }
            return newItem;
        });
        return mappedData;
    }

    updateValues(value, inputId) {
        const updatedIndex = this.updatedValues.findIndex(element => (element.Id === inputId));
        let newValueArr = [...this.updatedValues];
        if (updatedIndex !== -1) {
            newValueArr[updatedIndex] = { ...newValueArr[updatedIndex], ...value };
        } else {
            const tableIndex = this.modalTable.findIndex(element => (element.id === inputId));
            const modalData = this.modalTable[tableIndex].tableData[0];
            newValueArr.push({ ...modalData, ...value });
        }

        this.updatedValues = newValueArr;
		//console.log('this.updatedValues=======>>>>>>>>'+this.updatedValues);
		console.log('this.updatedValues=======>>>>>>>>'+JSON.stringify(this.updatedValues));
        this.dispatchEvent(new CustomEvent('cellupdate', { detail: this.updatedValues }));
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}