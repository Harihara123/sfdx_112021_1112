import { LightningElement, api } from 'lwc';

export default class LwcCSCPayrollBackupTypes extends LightningElement {
    @api config = {};

    renderedCallback(){
		if (this.config.selected) {	//this.config.item.selected     === "true"
			const backupTypeContainer = this.template.querySelector('.backupItems');
            backupTypeContainer.innerHTML = this.config.item;
        }
    }

    handleCheckbox(e){
		const backupTypeContainer = this.template.querySelector('.backupItems');
        backupTypeContainer.innerHTML = e.target.checked ? this.config.item : '';
        this.dispatchEvent(new CustomEvent('select', { detail: { 
                label: e.currentTarget.dataset.label, 
                selected: e.target.checked
            }
        }));
    }
}