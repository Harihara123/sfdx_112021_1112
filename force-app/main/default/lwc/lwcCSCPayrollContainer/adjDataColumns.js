// datatable dataColumns
const dataColumns = [
    /*{
        label: 'Adjustment Number',
        fieldName: 'Name',
        type: 'text',
        hideDefaultActions: true,
    },*/
    {
        label: 'Pay Rate Difference',
        fieldName: 'Pay_Rate_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Bill Rate Difference',
        fieldName: 'Bill_Rate_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Hours Difference',
        fieldName: 'Hours_Difference__c',
        type: 'text',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },{
        label: 'Original Gross',
        fieldName: 'Original_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Correct Gross',
        fieldName: 'Correct_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Gross Difference',
        fieldName: 'Gross_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
];

const dataColumnsModal = [
    {
        label: `ORGNL HRS`,
        fieldName: 'Original_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct HRS`,
        fieldName: 'Correct_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `ORGNL Pay RT`,
        fieldName: 'Original_Pay_Rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `ORGNL Bill RT`,
        fieldName: 'Original_Bill_Rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct Pay RT`,
        fieldName: 'Correct_Pay_rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct Bill RT`,
        fieldName: 'Correct_Bill_Rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `HRS DFFRNC`,
        fieldName: 'Hours_Difference__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Pay RT DFFRNC`,
        fieldName: 'Pay_Rate_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Bill RT DFFRNC`,
        fieldName: 'Bill_Rate_Difference__c',
        type:'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `ORGNL Gross`,
        fieldName: 'Original_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Correct Gross`,
        fieldName: 'Correct_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Gross DFFRNC`,
        fieldName: 'Gross_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
];

const dataColumnsModalUTA = [
    {
        label: `ORGNL HRS`,
        fieldName: 'Original_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct HRS`,
        fieldName: 'Correct_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `HRS DFFRNC`,
        fieldName: 'Hours_Difference__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, 
    {
        label: `TRVL ADV. PD`,
        fieldName: 'Travel_Adv_Pd__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `RECEIPTS SUBMTD`,
        fieldName: 'Receipts_Submtd__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `ORGNL Gross`,
        fieldName: 'Original_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Correct Gross`,
        fieldName: 'Correct_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Gross DFFRNC`,
        fieldName: 'Gross_Difference__c',
        type: 'text',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
];

//Data columns for Check Request modal
const dataColumnsCRModal = [
    {
        label: `Original Hours / Units`,
        fieldName: 'Original_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct Hours / Units`,
        fieldName: 'Correct_Hours__c',
        type: 'number',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Original Rate`,
        fieldName: 'Original_Pay_Rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Correct Rate`,
        fieldName: 'Correct_Pay_rate__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: true,
        required: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
    {
        label: `Original Pay`,
        fieldName: 'Original_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Gross Pay`,
        fieldName: 'Correct_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: `Gross Difference`,
        fieldName: 'Gross_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        wrapText: true,
        editable: false,
        required: false,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
];

const dataColumnsCR = [
    {
        label: 'Original Hours / Units',
        fieldName: 'Original_Hours__c',
        type: 'text',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Correct Hours / Units',
        fieldName: 'Correct_Hours__c',
        type: 'text',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Original Rate',
        fieldName: 'Original_Pay_Rate__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },{
        label: 'Correct Rate',
        fieldName: 'Correct_Pay_rate__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Original Pay',
        fieldName: 'Original_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    }, {
        label: 'Gross Pay',
        fieldName: 'Correct_Gross__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },{
        label: 'Gross Difference',
        fieldName: 'Gross_Difference__c',
        type: 'currency',
        hideDefaultActions: true,
        cellAttributes: {
            class: 'slds-text-color_default'
        }
    },
];
// Backup Reqs for Check Request
const backupReqs = {
	"BONS - Bonus Billable-Check/Cheque-Yes-Yes": {
		item: `<ul>
                    <li>Client signed Billable Bonus Addendum OR Client E-mail Approval</li>
                    <li>FO SMOS E-mail Approval</li>
                    <li>E-mail from Payroll Specialist confirming taxed up amount</li>
                </ul>`
	},
	"BONS - Bonus Billable-Check/Cheque-Yes-No": {
		item: `<ul>
                    <li>Client signed Billable Bonus Addendum OR Client E-mail Approval</li>
                    <li>FO SMOS E-mail Approval</li>
                    <li>E-mail from Payroll Specialist confirming taxed up amount</li>
                    <li>Printscreen of Hyperburden in Position:Burden Panel</li>
                </ul>`
	},
	"BONS - Bonus Billable-Check/Cheque-No-Yes": {
		item: `<ul>
                    <li>Client signed Billable Bonus Addendum OR Client E-mail Approval</li>
                    <li>FO SMOS E-mail Approval</li>
                </ul>`
	},
	"BONS - Bonus Billable-Check/Cheque-No-No": {
		item: `<ul>
                    <li>Client signed Billable Bonus Addendum OR Client E-mail Approval</li>
                    <li>FO SMOS E-mail Approval</li>
                    <li>E-mail from Payroll Specialist confirming taxed up amount</li>
                    <li>Printscreen of Hyperburden in Position:Burden Panel</li>
                </ul>`
	},
	"NONB - NON-BILLABLE BONUSES-Check/Cheque-Yes-": {
		item: `<ul>
                    <li>Consultant Sign Bonus Addendum</li>
                    <li>FO SMOS E-mail Approval</li>
					<li>Printscreen of Hyperburden in Position:Burden Panel</li>
                    <li>E-mail from Payroll Specialist confirming taxed up amount</li>
                </ul>`
	},
	"NONB - NON-BILLABLE BONUSES-Check/Cheque-No-": {
		item: `<ul>
                    <li>Consultant Sign Bonus Addendum</li>
                    <li>FO SMOS E-mail Approval</li>
                    <li>Printscreen of Hyperburden in Position:Burden Panel</li>
                </ul>`
	},
	"CBON - COMPLETION BONUS-Check/Cheque--": {
		item: `<ul>
                    <li>Consultant Signed Completion Bonus Addendum OR FO SMOS E-mail Approval</li>
                    <li>Total Hours Query</li>
					<li>Printscreen of Deferred Comp in Position:Terms OR Hyperburden in Position:Burden</li>
                </ul>`
	},
	"VACA - Vacation Payout-Check/Cheque--": {
		item: `<ul>
                    <li>Vacation Tracker</li>
                    <li>FO SMOS E-mail Approval (if outside of State Law Requirement)</li>
                </ul>`
	},
	"RETR - Retro Pay Request-Check/Cheque--": {
		item: `<ul>
                    <li>Completed Retroactive Pay Request Form</li>
                </ul>`
	},
	"STOP - Checks to be stopped, voided and reissued-Stop Payment Only--": {
		item: `<ul>
                    <li>Consultant Signed Stop Payment Authorization Form</li>
                    <li>Hyperburden Printscreen in Position:Burden (if payment is being stopped due to office error)</li>
                </ul>`
	},
	"STOP - Checks to be stopped, voided and reissued-Stop Payment/Reissue Check/Cheque--": {
		item: `<ul>
                    <li>Consultant Signed Stop Payment Authorization Form</li>
                    <li>Hyperburden Printscreen in Position:Burden (if payment is being stopped due to office error)</li>
                </ul>`
	},
	"-Void Only--": {
		item: `<ul>
                    <li>Copy of Voided Check</li>
                </ul>`
	},
	"-Void and Reissue Check/Cheque--": {
		item: `<ul>
                    <li>Copy of Voided Check</li>
                </ul>`
	},
	"STAT - State Law-Term - Pay w/in 24 hours--": {
		item: `<ul>
                    <li>Cash Pay Refusal document</li>
                </ul>`
	},
	"STAT - State Law-State Law Final Pay - Not Cash Pay--": {
		item: `<ul>
                    <li>Cash Pay Refusal document</li>
                </ul>`
	},
	"-Term - Pay w/in 24 hours--": {
		item: `<ul>
                    <li>Cash Pay Refusal document</li>
                </ul>`
	},
	"-State Law Final Pay - Not Cash Pay--": {
		item: `<ul>
                    <li>Cash Pay Refusal document</li>
                </ul>`
	}
}



export { dataColumns, dataColumnsModal, dataColumnsModalUTA, dataColumnsCR, dataColumnsCRModal, backupReqs };