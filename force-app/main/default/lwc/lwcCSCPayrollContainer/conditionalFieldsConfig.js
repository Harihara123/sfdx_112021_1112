const fieldConfig = {
    "Off Cycle Check Request": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Action Required",
                    fieldApiName: "Action_Required__c",
                    isRequired: true,
                    fieldId: "actionReq",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Is this a Y (Off-Cycle entered by associate) or M (Off-cycle entered by payroll)?",
                    fieldApiName: "Is_this_a_Y_or_M__c",
                    isRequired: true,
                    fieldId: "isYorM",
                    className: "col-50"
                }
            ]
        },
        backupTypes: []
    },
    "Negative Adjustment": {
        colOne: {
            fields: [
                /*{
                    fieldLabel: "Reason For Adjustment",
                    fieldApiName: "Reason_for_Adjustment__c",
                    isRequired: true,
                    fieldId: "reasonForAdj",
                    className: "col-100"
                },*/
                {
                    fieldLabel: "What is the total amount owed by contractor?",
                    fieldApiName: "What_is_the_total_amount_owed_by_contrac__c",
                    isRequired: true,
                    fieldId: "totalAmountOwed",
                    className: "col-50"
                },
                {
                    fieldLabel: "Did you consult with Payroll for this amount?",
                    fieldApiName: "Did_you_consult_with_Payroll_for_this_am__c",
                    isRequired: true,
                    fieldId: "payrollConsult",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Is the Contractor aware of the deduction occurring?",
                    fieldApiName: "Is_the_Contractor_aware_of_the_deduction__c",
                    isRequired: true,
                    fieldId: "contractorDeduction",
                    className: "col-50"
                },
                {
                    fieldLabel: "Attach email showing amount owed from Payroll ",
                    fieldApiName: "Attach_email_showing_amount_owed_from_Pa__c",
                    isRequired: true,
                    fieldId: "attachEmail",
                    className: "col-50"
                }/*,
				{
                    fieldLabel: "Is this a Y (Off-Cycle entered by associate) or M (Off-cycle entered by payroll)?",
                    fieldApiName: "Is_this_a_Y_or_M__c",
                    isRequired: true,
                    fieldId: "isYorM",
                    className: "col-50"
                }*/
            ]
        },
        backupTypes: []
    },
	"Oncycle Adjustment": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: []
	},
    // Fields for Action Required
    "Issue Check": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                }

            ]
        }
    },
    "FLSA Payout (I.e, Bonus/Commision/Stipend)": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Is the payout discretionary or Non Discretionary",
                    fieldApiName: "Is_the_payout_discretionary__c",
                    isRequired: true,
                    fieldId: "payoutDiscretionary",
                    className: "col-50"
                },
                {
                    fieldLabel: "Is the earn code available for the Weekending of the payout?",
                    fieldApiName: "Is_the_earn_code_available_for_the_Weeke__c",
                    isRequired: true,
                    fieldId: "earnCodeWeekending",
                    className: "col-50"
                },
				{
                    fieldLabel: "Is this payout requiring a specific NET amount?",
                    fieldApiName: "Is_this_payout_requiring_a_specific_NET__c",
                    isRequired: true,
                    fieldId: "payoutNETAmount",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                },
				{
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                },
                {
                    fieldLabel: "Are you attaching a list of multiple contractors requiring an off-cycle check?",
                    fieldApiName: "Are_you_attaching_a_spreadsheet_listing__c",
                    isRequired: true,
                    fieldId: "attachSpreadsheet",
                    className: "col-50"
                }

            ]
        }
    },
    "Gift cards (Potential FLSA Requirement)": {
        colOne: {
            fields: [
                
				{
                    fieldLabel: "Is the payout discretionary or Non Discretionary",
                    fieldApiName: "Is_the_payout_discretionary__c",
                    isRequired: true,
                    fieldId: "giftcardsDiscretionary",
                    className: "col-50"
                },
                {
                    fieldLabel: "Is the earn code available for the Weekending of the payout?",
                    fieldApiName: "Is_the_earn_code_available_for_the_Weeke__c",
                    isRequired: true,
                    fieldId: "earnCodeWeekending",
                    className: "col-50"
                },
				{
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                },
                {
                    fieldLabel: "Are you attaching a list of multiple contractors requiring an off-cycle check?",
                    fieldApiName: "Are_you_attaching_a_spreadsheet_listing__c",
                    isRequired: true,
                    fieldId: "attachSpreadsheet",
                    className: "col-50"
                },
                {
                    fieldLabel: "Were gross ups prepared for these payouts by payroll?",
                    fieldApiName: "Were_gross_ups_prepared_for_these_payout__c",
                    isRequired: true,
                    fieldId: "giftGrossUps",
                    className: "col-50"
                }

            ]
        }
    },
    "Stop Payment Only": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                },
				{
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                },
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-50"
                },
                {
                    fieldLabel: "Check Net Pay Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                },
				{
                    fieldLabel: "Stop Payment Reason:",
                    fieldApiName: "Stop_Payment_Reason__c",
                    isRequired: true,
                    fieldId: "stopPayReason",
                    className: "col-100"
                },
                {
                    fieldLabel: "Have ten USPS mailing days past since the check issue date?",
                    fieldApiName: "Have_ten_USPS_mailing_days_past__c",
                    isRequired: true,
                    fieldId: "uspsMailingDays",
                    className: "col-50"
                }
            ]
        }
    },
    "Stop Payment and Reissue Check": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                },
				{
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                },
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-50"
                },
                {
                    fieldLabel: "Check Net Pay Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                },
				{
                    fieldLabel: "Stop Payment Reason:",
                    fieldApiName: "Stop_Payment_Reason__c",
                    isRequired: true,
                    fieldId: "stopPayReason",
                    className: "col-100"
                },
                {
                    fieldLabel: "Have ten USPS mailing days past since the check issue date?",
                    fieldApiName: "Have_ten_USPS_mailing_days_past__c",
                    isRequired: true,
                    fieldId: "uspsMailingDays",
                    className: "col-50"
                }
            ]
        }
    },
    "Void Only": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                },
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Net Pay Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }

            ]
        }
    },
    "Void and Reissue Check": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                },
				{
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Pay Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }

            ]
        }
    },
    "Void and Replace with different hours": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Does this contractor want a live check or DDP/Cash Pay?",
                    fieldApiName: "Does_this_contractor_want_a_live_check_o__c",
                    isRequired: true,
                    fieldId: "contractorLiveCheck",
                    className: "col-50"
                },
				{
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                },
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-50"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Does the contractor have benefits?",
                    fieldApiName: "Does_the_contractor_HAVE_benefits__c",
                    isRequired: true,
                    fieldId: "contractorBenefits",
                    className: "col-50"
                },
				{
                    fieldLabel: "Check Net Pay Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }

            ]
        }
    },
	//Backup Types
	"Time": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: [{
            label: 'Time Corrections:',
            labelId: 'Time_Corrections__c',
            item: `<ul>
                    <li>Corrected timecard or email from the Client confirming the total hours worked.</li>
                    <li>If the time adjustment is due to incorrect earn codes (i.e. overtime hours should be entered as standard), the back-up
                    timecard must include the detailed breakdown of how the hours should be billed (REG, OVT, and DBL).</li>
                    <li>Prevailing Wage/Fringe
                        <ul>
                            <li>Wage Determination Form</li>
                            <li>Print Screen from Position Employee to show the client job title that matches the title on the Wage Determination Form
                            Timecard to show hours worked</li>
                        </ul>
                    </li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved
                    in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Late Timecards:',
            labelId: 'Late_Timecards__c',
            item: `<ul>
                    <li>Copy of the timecard showing hours are approved.</li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved
                    in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Contractor Did Not Work (CDW):',
            labelId: 'Contractor_Did_Not_Work__c',
            item: `<ul>
                        <li>An email from the Client is required if the contractor did not work but hours were submitted and paid.</li>
                        <li>If the hours were paid in error by the CSA, an explanation is required on the form explaining the error. Appropriate timecards should be attached.</li>
                        <li>If the contractor went direct with the client, an email is required from the client confirming the contractor's start date.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Expenses:',
            labelId: 'Expenses__c',
            item: `<ul>
                    <li>Email showing Expense InfoPath form link approved by corporate Expense auditor OR print screen from Aerotek Time & Expense showing expense approved by corporate Expense auditor.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Vacation:',
            labelId: 'CSC_Billable_Vacation__c',
            item: `<ul>
                    <li>Copy of the timecard showing vacation hours; Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Non Billable Vacation:',
            labelId: 'Non_Billable_Vacation__c',
            item: `<ul>
                    <li>Canada Only Needs the Following: Weekly Payroll Query or Print Screens from the Special Accumulator to validate vacation hours</li>
                    <li>Print Screen from Position Burden showing that the burden has been in the req for the required timeframe OR Print Screen of one-time burden to cover the cost.</li>
                    <li>Hours Query sent from OST BOA.</li>
                    <li>Employment Agreement showing the contractor is eligible for vacation as well as the accrual rate. (Not applicable to Puerto Rico)</li>
                    <li>Copy of Vacation Accrual Calculator (For Puerto Rico - attach manual calculations)</li>
                    <li>PTO Report in place of the Vacation Accrual Report for EASi ONLY</li>
                    <li>If not a Required Payout State (Required Payout States are CA, CO, IL, LA, MA, MT, NE, ND, Rl, PR)</li>
                    <li>Email from FOAC approving payout (VOX payout only)</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Per Diem:',
            labelId: 'CSC_Billable_Per_Diem__c',
            item: `<ul>
                        <li>The approved timecard or expense report reflecting the total amount of per diem to be paid.</li>
                        <li>Print screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented and Completed".</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Per Diem:',
            labelId: 'Non_Billable_Per_Diem__c',
            item: `<ul>
                        <li>Print screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented and Completed".</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Billable Holiday:',
            labelId: 'CSC_Billable_Holiday__c',
            item: `<ul>
                        <li>Copy of the timecard showing holiday hours.</li>
                        <li>Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Holiday:',
            labelId: 'Non_Billable_Holiday__c',
            item: `<ul>
                        <li>Print Screen from Position Burden showing that the burden has been in the req for the required timeframe OR Print Screen of one time burden to cover the cost (Not applicable to Canada).</li>
                        <li>If the contractor worked hours in addition to the holiday pay that is being entered, a copy of the timecard is required. If the non-billable holiday pay hours are the only hours being paid out, no timecard is required BUT an email from the AM, Recruiter, or DBO confirming the holiday is required.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Subvendor Adjustments:',
            labelId: 'Time_Subvendor_Adjustments__c',
            item: `<ul>
                        <li>Copy of approved Timecard, Invoice, or email from Client confirming the total hours</li>
                        <li>For contractors paid through a VMS tool - a print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount</li>
                    </ul>`,
            selected: false
        }, {
            label: 'On Premise Adjustments:',
            labelId: 'On_Premise_Adjustments__c',
            item: `<ul>
                        <li>Timecard or email from CSS or RFM approving time worked.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Billable Drug/Background:',
            labelId: 'CSC_Billable_Drug_Background__c',
            item: `<ul>
                        <li>Agreement or email from client confirming hours as billable.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Drug/Background:',
            labelId: 'Non_Billable_Drug_Background__c',
            item: `<ul>
                    <li>Print screen of burden.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Incorrect Requisition:',
            labelId: 'Incorrect_Requisition__c',
            item: `<ul>
                    <li>Timecard.</li>
                    <li>Detailed explanation in comment box stating why we're changing the requisition.</li>
                    <li>If contractor has Expenses - Email showing Expense InfoPath form link approved by corporate Expense auditor OR print screen from Aerotek Time & Expense showing Expense approved by corporate Expense auditor.</li>
                    <li>If contractor has Per Diem - Print Screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented, and Completed".</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Sick Pay:',
            labelId: 'CSC_Billable_Sick_Pay__c',
            item: `<ul>
                        <li>Copy of timecard showing sick hours.</li>
                        <li>Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Sick Pay:',
            labelId: 'Non_Billable_Sick_Pay__c',
            item: `<ul>
                    <li>Print screen from Position Burden showing that it has been in the req for the required timeframe OR Print screen of one-time burden or cover the cost.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Non Billable Meal Break Penalty:',
            labelId: 'Non_Billable_Meal_Break_Penalty__c',
            item: `<ul>
                    <li>Time card with time punches.</li>
					<li>Meal break waiver.</li>
					<li>Print screen of burden or ONET to cover the cost.</li>
                </ul>`,
            selected: false
        }]
	},
	"Rate": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: [{
            label: 'Bill Rate Adjustments:',
            labelId: 'Bill_Rate_Adjustments__c',
            item: `<ul>
                        <li>Exhibit A or rate letter from the client confirming the bill rate. (If client email front office approval required) Exhibit A’s can be located in the Exhibit A sharepoint site or on the SS&O homepage under account summaries for SS&O clients.
                            <ul>
                                <li>Copy of the signed PO confirming the bill rate. (If PO is missing signatures Front Office approval required)</li>
                            </ul>
                        </li>
                        <li>If a VMS client, the following documentation should be included: copy of the PO, work order, or print screen from the VMS tool verifying the contractor name and bill rate Or an Excel sheet downloaded from the VMS tool for multiple contractors.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Pay Rate Adjustments:',
            labelId: 'Pay_Rate_Adjustments__c',
            item: `<ul>
                            <li>Signed/Executed Employment Agreement and/or documentation from the Account Manager, Recruiter, DBO or OPM confirming the accurate pay rate.</li>
                            <li>If a prevailing wage client, the following documentation should be included:
                                <ul>
                                    <li>Wage Determination Form</li>
                                    <li>Print Screen from Position Employee to show the client job title that matches the title on the Wage Determination Form</li>
                                </ul> 
                            </li>
                        </ul>`,
            selected: false
        }, {
            label: 'Subvendor Adjustments:',
            labelId: 'Rate_Subvendor_Adjustments__c',
            item: `<ul>
                        <li>Copy of signed document with reference to rates or email from client confirming rates. (If client email. Front Office approval required)</li>
                        <li>For contractors paid through a VMS tool - a print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Compliance Conducted Audits:',
            labelId: 'Compliance_Conducted_Audits__c',
            item: `<ul>
                        <li>Compliance email/audit that is sent out for adjustment to be completed</li>
                    </ul>`,
            selected: false
        }]
	},
	"Both": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: [{
            label: 'Time Corrections:',
            labelId: 'Time_Corrections__c',
            item: `<ul>
                    <li>Corrected timecard or email from the Client confirming the total hours worked.</li>
                    <li>If the time adjustment is due to incorrect earn codes (i.e. overtime hours should be entered as standard), the back-up
                    timecard must include the detailed breakdown of how the hours should be billed (REG, OVT, and DBL).</li>
                    <li>Prevailing Wage/Fringe
                        <ul>
                            <li>Wage Determination Form</li>
                            <li>Print Screen from Position Employee to show the client job title that matches the title on the Wage Determination Form
                            Timecard to show hours worked</li>
                        </ul>
                    </li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved
                    in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Late Timecards:',
            labelId: 'Late_Timecards__c',
            item: `<ul>
                    <li>Copy of the timecard showing hours are approved.</li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved
                    in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Contractor Did Not Work (CDW):',
            labelId: 'Contractor_Did_Not_Work__c',
            item: `<ul>
                        <li>An email from the Client is required if the contractor did not work but hours were submitted and paid.</li>
                        <li>If the hours were paid in error by the CSA, an explanation is required on the form explaining the error. Appropriate timecards should be attached.</li>
                        <li>If the contractor went direct with the client, an email is required from the client confirming the contractor's start date.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Expenses:',
            labelId: 'Expenses__c',
            item: `<ul>
                    <li>Email showing Expense InfoPath form link approved by corporate Expense auditor OR print screen from Aerotek Time & Expense showing expense approved by corporate Expense auditor.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Vacation:',
            labelId: 'CSC_Billable_Vacation__c',
            item: `<ul>
                    <li>Copy of the timecard showing vacation hours; Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                    <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Non Billable Vacation:',
            labelId: 'Non_Billable_Vacation__c',
            item: `<ul>
                    <li>Canada Only Needs the Following: Weekly Payroll Query or Print Screens from the Special Accumulator to validate vacation hours</li>
                    <li>Print Screen from Position Burden showing that the burden has been in the req for the required timeframe OR Print Screen of one-time burden to cover the cost.</li>
                    <li>Hours Query sent from OST BOA.</li>
                    <li>Employment Agreement showing the contractor is eligible for vacation as well as the accrual rate. (Not applicable to Puerto Rico)</li>
                    <li>Copy of Vacation Accrual Calculator (For Puerto Rico - attach manual calculations)</li>
                    <li>PTO Report in place of the Vacation Accrual Report for EASi ONLY</li>
                    <li>If not a Required Payout State (Required Payout States are CA, CO, IL, LA, MA, MT, NE, ND, Rl, PR)</li>
                    <li>Email from FOAC approving payout (VOX payout only)</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Per Diem:',
            labelId: 'CSC_Billable_Per_Diem__c',
            item: `<ul>
                        <li>The approved timecard or expense report reflecting the total amount of per diem to be paid.</li>
                        <li>Print screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented and Completed".</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Per Diem:',
            labelId: 'Non_Billable_Per_Diem__c',
            item: `<ul>
                        <li>Print screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented and Completed".</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Billable Holiday:',
            labelId: 'CSC_Billable_Holiday__c',
            item: `<ul>
                        <li>Copy of the timecard showing holiday hours.</li>
                        <li>Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Holiday:',
            labelId: 'Non_Billable_Holiday__c',
            item: `<ul>
                        <li>Print Screen from Position Burden showing that the burden has been in the req for the required timeframe OR Print Screen of one time burden to cover the cost (Not applicable to Canada).</li>
                        <li>If the contractor worked hours in addition to the holiday pay that is being entered, a copy of the timecard is required. If the non-billable holiday pay hours are the only hours being paid out, no timecard is required BUT an email from the AM, Recruiter, or DBO confirming the holiday is required.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Subvendor Adjustments:',
            labelId: 'Time_Subvendor_Adjustments__c',
            item: `<ul>
                        <li>Copy of approved Timecard, Invoice, or email from Client confirming the total hours</li>
                        <li>For contractors paid through a VMS tool - a print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount</li>
                    </ul>`,
            selected: false
        }, {
            label: 'On Premise Adjustments:',
            labelId: 'On_Premise_Adjustments__c',
            item: `<ul>
                        <li>Timecard or email from CSS or RFM approving time worked.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Billable Drug/Background:',
            labelId: 'CSC_Billable_Drug_Background__c',
            item: `<ul>
                        <li>Agreement or email from client confirming hours as billable.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Drug/Background:',
            labelId: 'Non_Billable_Drug_Background__c',
            item: `<ul>
                    <li>Print screen of burden.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Incorrect Requisition:',
            labelId: 'Incorrect_Requisition__c',
            item: `<ul>
                    <li>Timecard.</li>
                    <li>Detailed explanation in comment box stating why we're changing the requisition.</li>
                    <li>If contractor has Expenses - Email showing Expense InfoPath form link approved by corporate Expense auditor OR print screen from Aerotek Time & Expense showing Expense approved by corporate Expense auditor.</li>
                    <li>If contractor has Per Diem - Print Screen or email reflecting contractor’s Per Diem InfoPath form status as "Approved, Documented, and Completed".</li>
                </ul>`,
            selected: false
        }, {
            label: 'Billable Sick Pay:',
            labelId: 'CSC_Billable_Sick_Pay__c',
            item: `<ul>
                        <li>Copy of timecard showing sick hours.</li>
                        <li>Copy of Service Agreement or Email from the client confirming that the hours are billable.</li>
                        <li>For contractors paid through a VMS tool - A print screen is required for all adjustments showing entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Non Billable Sick Pay:',
            labelId: 'Non_Billable_Sick_Pay__c',
            item: `<ul>
                    <li>Print screen from Position Burden showing that it has been in the req for the required timeframe OR Print screen of one-time burden or cover the cost.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Non Billable Meal Break Penalty:',
            labelId: 'Non_Billable_Meal_Break_Penalty__c',
            item: `<ul>
                    <li>Time card with time punches.</li>
					<li>Meal break waiver.</li>
					<li>Print screen of burden or ONET to cover the cost.</li>
                </ul>`,
            selected: false
        }, {
            label: 'Bill Rate Adjustments:',
            labelId: 'Bill_Rate_Adjustments__c',
            item: `<ul>
                        <li>Exhibit A or rate letter from the client confirming the bill rate. (If client email front office approval required) Exhibit A’s can be located in the Exhibit A sharepoint site or on the SS&O homepage under account summaries for SS&O clients.
                            <ul>
                                <li>Copy of the signed PO confirming the bill rate. (If PO is missing signatures Front Office approval required)</li>
                            </ul>
                        </li>
                        <li>If a VMS client, the following documentation should be included: copy of the PO, work order, or print screen from the VMS tool verifying the contractor name and bill rate Or an Excel sheet downloaded from the VMS tool for multiple contractors.</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Pay Rate Adjustments:',
            labelId: 'Pay_Rate_Adjustments__c',
            item: `<ul>
                            <li>Signed/Executed Employment Agreement and/or documentation from the Account Manager, Recruiter, DBO or OPM confirming the accurate pay rate.</li>
                            <li>If a prevailing wage client, the following documentation should be included:
                                <ul>
                                    <li>Wage Determination Form</li>
                                    <li>Print Screen from Position Employee to show the client job title that matches the title on the Wage Determination Form</li>
                                </ul> 
                            </li>
                        </ul>`,
            selected: false
        }, {
            label: 'Subvendor Adjustments:',
            labelId: 'Rate_Subvendor_Adjustments__c',
            item: `<ul>
                        <li>Copy of signed document with reference to rates or email from client confirming rates. (If client email. Front Office approval required)</li>
                        <li>For contractors paid through a VMS tool - a print screen is required for all adjustments showing the entry is approved in the tool. This print screen acts as the client approval and ensures Aerotek will receive payment for the amount</li>
                    </ul>`,
            selected: false
        }, {
            label: 'Compliance Conducted Audits:',
            labelId: 'Compliance_Conducted_Audits__c',
            item: `<ul>
                        <li>Compliance email/audit that is sent out for adjustment to be completed</li>
                    </ul>`,
            selected: false
        }]
	},
	// Check request conditional Fields
	"CR-Check/Cheque": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: false,
                    fieldId: "checkIssueDate",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: false,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: false,
                    fieldId: "checkNumber",
                    className: "col-100"
                }
            ]
        }
    },
    "CR-Stop Payment Only": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                }
            ]
        }
    },
	"CR-Stop Payment/Reissue Check/Cheque": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                }
            ]
        }
    },
	"CR-Void Only": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                }
            ]
        }
    },
	"CR-Void and Reissue Check/Cheque": {
        colOne: {
            fields: [
                {
                    fieldLabel: "Check Issue Date",
                    fieldApiName: "Check_Issue_Date__c",
                    isRequired: true,
                    fieldId: "checkIssueDate",
                    className: "col-100"
                },
				{
                    fieldLabel: "Check Net Amount",
                    fieldApiName: "Check_Net_Pay_Amount__c",
                    isRequired: true,
                    fieldId: "checkNetPay",
                    className: "col-100"
                }
            ]
        },
        colTwo: {
            fields: [
                {
                    fieldLabel: "Check Number",
                    fieldApiName: "Check_Number__c",
                    isRequired: true,
                    fieldId: "checkNumber",
                    className: "col-100"
                }
            ]
        }
    },
	"CR-State Law Final Pay": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: []
	},
	"CR-State Law Final Pay - Not Cash Pay": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: []
	},
	"CR-Term - Pay w/in 24 hours": {
        colOne: {
            fields: null
        },
        colTwo: {
            fields: null
        },
        backupTypes: []
	}

}
export default fieldConfig;