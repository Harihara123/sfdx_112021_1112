import { LightningElement, api, wire, track } from 'lwc';
import fieldConfig from './conditionalFieldsConfig'; 
import getAdjustments from '@salesforce/apex/CSC_AdjustmentsController.getAdjustments';
import { dataColumns, dataColumnsModal, dataColumnsModalUTA, dataColumnsCR, dataColumnsCRModal, backupReqs } from './adjDataColumns';
// import getConsentPreferencesModelProperties from '@salesforce/apex/ATS_ConsentPreferencesController.getConsentPreferencesModelProperties';
import addAdjustments from '@salesforce/apex/CSC_AdjustmentsController.addAdjustments';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import isAdjustmentCheck from '@salesforce/apex/CSC_AdjustmentsController.isAdjustmentCheck';
import isCaseClosed from '@salesforce/apex/CSC_AdjustmentsController.isCaseClosed';
import deleteAdjustments from '@salesforce/apex/CSC_AdjustmentsController.deleteAdjustments';
import isParentORChildCase from '@salesforce/apex/CSC_AdjustmentsController.isParentORChildCase';
import calculateDifference from '@salesforce/apex/CSC_AdjustmentsController.calculateDifference';
import saveBackupTypes from '@salesforce/apex/CSC_AdjustmentsController.saveBackupTypes';
//for copy adjustment
import copyAdjustments from '@salesforce/apex/CSC_AdjustmentsController.copyAdjustments';
//for Backup Types
import returnBackupTypes from '@salesforce/apex/CSC_AdjustmentsController.returnBackupTypes';
import isCSCTalentOffice from '@salesforce/apex/CSC_AdjustmentsController.isCSCTalentOffice';
//for check request
import isPayrollORFS from '@salesforce/apex/CSC_AdjustmentsController.isPayrollORFS';
import enableCheckRequest from '@salesforce/label/c.CSC_Enable_Check_Request_Form';
import chekRequestTypes from '@salesforce/label/c.CSC_Check_Request_Types';
import crTalentOffices from '@salesforce/label/c.CSC_Check_Request_Talent_offices';

export default class LwcCSCPayrollContainer extends LightningElement {
	@track searchData = [];
	@track errorMsg = '';
	@track isNoAdjustment = false;
    @track columns = dataColumns;
	@track columnsCR = dataColumnsCR;
    tableData = [];

    isLoading = false;
	isAdjustment;
	isCaseClosed;
	isParentORChild;
	isCSCtalentOfc;
	@track isPayrollForm = false;

	// Check request Fields
	@track isCheckRequest = false;
    enableCheck;
	isCheckRequestType = false;
	isCheckRequestTalent = false;
	checkRequestList = [];
	CheckRequestTalentList = [];
	@track noAdjustmentForm;
	

    showSection = false;
    activeSections = ['section0', 'section1', 'section2', 'section3', 'section10', 'section11', 'section12'];
    conditionalFields = fieldConfig;
	// added for Check request backup Req
	crBackupReqs = backupReqs;
    backupTypeConfig = [];
	dupeBackupTypeConfig = [];
	deDupeBackupTypeConfig = [];
    state = {};
    hasPVWage = false;
    hasUTA = false;

    fieldsValid = true;

    get modalDataCols(){
		return this.hasUTA ? dataColumnsModalUTA : dataColumnsModal;
    }

	get modalDataColsCR(){
		return dataColumnsCRModal;
    }

	get acceptedFormats() {
        return ['.pdf', '.png', '.jpg', '.jpeg', '.xls', '.xlsx', '.doc', '.docx', '.csv'];
    }
    
    @api recordId = "";

    get getIsNoAdj(){
        return this.isNoAdjustment;
    }
	/*
	@wire(returnBackupTypes, { caseRecId: '$recordId' })
	getreturnBackupTypes({error, data}){
		if(error){
            console.log(`getSearchData error===>${error}`);
        } else {
			console.log('data======'+data[Adjustment_Type__c]);

        }
	}*/
	connectedCallback() {
        //console.log('recordId----'+this.recordId);
        //console.log('hasUtA---'+this.hasUTA);
		//console.log('enableCheckRequest====>>>>>>'+enableCheckRequest);
		//console.log('chekRequestTypes====>>>>>>'+chekRequestTypes);
		// Check request form enable or disable flag
		if(enableCheckRequest === "Yes"){
			this.enableCheck = true;
		}else{
			this.enableCheck = false;
		}
		// Check rEquest form enable or disable flag
		// Method to get the Case Data :: Start
		isPayrollORFS({ caseRecId: this.recordId })
		.then(res => {
			//console.log('res===96==>>>>>>>'+JSON.stringify(res));
			// Method to set Payroll OR Chek Request :: Start
			if(JSON.stringify(res).includes('Talent_Office__c')){
				if(res.Talent_Office__r.Talent_s_OPCO__c === "TEK"){
					this.isCheckRequest = true;
				}else if(res.Talent_Office__r.Talent_s_OPCO__c === "ONS"){
					this.isPayrollForm = true;
				}
				if(crTalentOffices === "All"){
					this.isCheckRequestTalent = true;
				}else{
					this.CheckRequestTalentList = crTalentOffices.split(',');
					this.CheckRequestTalentList.forEach(key => {
						if(key === res.Talent_Office__r.Talent_s_Office_Code__c){
							this.isCheckRequestTalent = true;
						}
					});
				}

			}else{
				this.noAdjustmentForm = true;
			}
			//console.log('calling adjustments after response=======>>>>>>>');
			this.callFromConnected();
			// Method to set Payroll OR Chek Request :: End
			// Check Sub type belongs to Check request sub types :: Start
			this.checkRequestList = chekRequestTypes.split(',');
			this.checkRequestList.forEach(key => {
				if(key === res.Sub_Type__c){
					this.isCheckRequestType = true;
				}
			});
			// Check Sub type belongs to Check request sub types :: End 
	
		})
		.catch(err => console.log(`isCSCTalentOffice===108===>${err.message}`));
		// Method to get the Case Data :: End
		/*
        getAdjustments({caseRecId: this.recordId, args: this.hasUTA})
        .then(res => {
            console.log('res---'+JSON.stringify(res));
            this.searchData = res;
            this.tableData = JSON.parse(JSON.stringify(this.searchData));
            this.refreshModal(this.searchData);
            console.log('searchdata456---'+this.searchData );
            // this.columns = this.checkNeg(this.searchData);
            if (this.searchData.length === 0) {
                console.log('Adjustment123---'+this.isNoAdjustment);
                this.isNoAdjustment = true;
            }
        })
        .catch(err => console.log(`getAdjustments==>${err.message}`));
		*/
		

		isCSCTalentOffice({ caseRecId: this.recordId }).then(res2 => {
			this.isCSCtalentOfc = res2;
			//console.log('this.isCSCtalentOfc=====>>>>>'+this.isCSCtalentOfc);
		})
		.catch(err => console.log(`isCSCTalentOffice==>${err.message}`));

        isCaseClosed({ caseRecId: this.recordId }).then(res =>{
            this.isCaseClosed = res;
			console.log(this.isCaseClosed);
        })
		.catch(err => console.log(`isCaseClosed==>${err.message}`));
		
	   isAdjustmentCheck({ caseRecId: this.recordId })
		   .then(data =>{
			this.isAdjustment = data;
			console.log(this.isAdjustment);
		   })
	   .catch(err => console.log(`isAdjustmentCheck==>${err.message}`));
		
		isParentORChildCase({ caseRecId: this.recordId }).then(res1 => {
			this.isParentORChild = res1;
			console.log(this.isParentORChild);
		})
		.catch(err => console.log(`isParentORChildCase==>${err.message}`));
		
		returnBackupTypes({caseRecId: this.recordId})
			.then(res => {
				const parseJSON = res.map(item => JSON.parse(item));
				//console.log('JSON.stringify(parseJSON[0])========>>>>>>>>>'+JSON.stringify(parseJSON[0]));
				if(parseJSON[0].Adjustment_Type !== 'null' && parseJSON[0].Adjustment_Type !== ''){
					this.dupeBackupTypeConfig = this.conditionalFields[parseJSON[0].Adjustment_Type].backupTypes;
					this.backupTypeConfig = this.dupeBackupTypeConfig.map(item => {
						if(JSON.stringify(parseJSON[0]).includes(item.labelId)){
							item.selected = true;
						}
						return item;
					});
				}else{
					this.backupTypeConfig = [];
				}
			})
            .catch(err => console.log(`connectedCallback===returnBackupTypes==>${err.message}`));
	}

	callFromConnected(){
		//console.log('call From Connected======>>>>>>>');
		getAdjustments({caseRecId: this.recordId, args: this.hasUTA})
        .then(res => {
            //console.log('res---'+JSON.stringify(res));
            this.searchData = res;
            this.tableData = JSON.parse(JSON.stringify(this.searchData));
            this.refreshModal(this.searchData);
            //console.log('searchdata456---'+this.searchData );
            // this.columns = this.checkNeg(this.searchData);
            if (this.searchData.length === 0) {
                //console.log('Adjustment123---'+this.isNoAdjustment);
                this.isNoAdjustment = true;
            }
        })
        .catch(err => console.log(`getAdjustments==>${err.message}`));

	}

	/*@wire(isAdjustmentCheck, { caseRecId: '$recordId' })
	getisAdjustment({error, data}){
		if(error){
            console.log(`getSearchData error===>${error}`);
        } else {
			this.isAdjustment = data;
			console.log(this.isAdjustment);
        }
	}

	@wire(isParentORChildCase, { caseRecId: '$recordId' })
	getisParentORChildCase({error, data}){
		if(error){
            console.log(`getParent Or Child error===>${error}`);
        } else {
			this.isParentORChild = data;
			console.log(this.isParentORChild);
        }
	}

	@wire(isCaseClosed, { caseRecId: '$recordId' })
	getisCaseClosed({error, data}){
		if(error){
            console.log(`getCaseclosed error===>${error}`);
        } else {
			this.isCaseClosed = data;
			console.log(this.isCaseClosed);
        }
	}

    @wire(getAdjustments, { caseRecId: '$recordId', args: '$hasUTA' })
    getSearchData({ error, data }) {
        if (error) {
            console.log(`getSearchData error===>${error}`);
        } else if (data) {
			console.log('this.searchData====cntrlr====>>>>>>>>>'+this.searchData);
            this.searchData = data;
            this.tableData = JSON.parse(JSON.stringify(this.searchData));
            this.refreshModal(this.searchData);
            // this.columns = this.checkNeg(this.searchData);
            if (this.searchData.length === 0) {
                this.isNoAdjustment = true;
            }
        }
    }*/

    handleBackupTypeSelect(e){
        const { label, selected } = e.detail;
        let backupTypes = this.state.backupTypes.length > 0 ? [ ...this.state.backupTypes ] : [];
        const findIndex = backupTypes.findIndex(item => item.backupType === label);

        if(findIndex !== -1){
            backupTypes[findIndex].backupState = selected;
        } else {
            backupTypes.push({ backupType: label, backupState: selected });
        }

        this.state = { ...this.state, backupTypes: backupTypes };
    }

    handleSubmitValidation(e){
        this.fieldsValid = e.detail;
    }

    handleSubmit(){
		/*let isTableValid = false;
		This code will work once formula fields can be chnaged as Diplay as blank when value is not there
		console.log('this.tableData========>>>>>>>>'+JSON.stringify(this.tableData));
		if(this.tableData !== null && this.tableData != ''){
			this.tableData.forEach(row => {
				console.log('row=====>>>>>>>'+row.Weekday__c);
				if(row.Bill_Rate_Difference__c !== 0 && row.Hours_Difference__c !== 0 && row.Pay_Rate_Difference__c !== 0 &&  row.Gross_Difference__c !== 0 && row.Original_Gross__c !== 0 && row.Correct_Gross__c !== 0){
					isTableValid = true;
				}
			});
		}
		console.log('isTableValid========>>>>>>>>'+isTableValid);*/
		if(this.tableData !== null && this.tableData != ''){
			this.template.querySelector('c-lwc-c-s-c-payroll-input-fields').submitForm();
			if (this.fieldsValid) {
				const params = JSON.stringify({ 'caseRecId': this.recordId, 'backupTypes': this.state.backupTypes });
				saveBackupTypes({ inputBackup: params })
					.then(res => {
						console.log(`handleSubmit > saveBackupTypes ===>${res}`);
						//this.showToast('Success', 'Form saved successfully!', 'success');
					})
					.catch(err => {
						console.log(`handleSubmit > saveBackupTypes ===>${err.body.message}`);
						this.showToast('Error', 'An unexpected error has occured, please contact your Admin', 'error');
					});
            
			} else {
				this.showToast('Error', 'Please fill out all required fields before submitting.', 'error');
			}
        }else{
			this.showToast('Message:', 'Please complete the adjustment calculation table in order to save this form.', 'warning');
		}
	}

	handleCRSubmit(){
		//console.log('check Req save=====>>>>');
        if(this.tableData !== null && this.tableData != ''){
            this.template.querySelector('c-lwc-c-s-c-check-request-input-fields').submitCRForm();
            //console.log('this.fieldsValid=====272====>>>>>>>>>'+this.fieldsValid);
            if (this.fieldsValid) {
            
            }else{
                this.showToast('Error', 'Please fill out all required fields before submitting.', 'error');
            }
        }else{
			this.showToast('Message:', 'Please complete the adjustment calculation table in order to save this form.', 'warning');
		}
	}

	handleAdjustments(){
        const modal = this.template.querySelector('c-lwc-c-s-c-payroll-adj-modal');
        modal.showModal();
    }

	handleAddAdjustments(e){
        const modal = this.template.querySelector('c-lwc-c-s-c-payroll-adj-modal');
        const isModal = e.detail === "modal" ? true : false;
        const dataMatrix = this.hasUTA ? [{ Record_ID: null, ORGNL_HRS: null, Correct_HRS: null, Travel_Adv_Pd: null, Receipts_Submtd: null, Adjustment_Code: null, Earn_Code: null }]:
            [{ Record_ID: null, ORGNL_HRS: null, ORGNL_Pay_RT: null, Correct_HRS: null, ORGNL_Bill_RT: null, Correct_Pay_RT: null, Correct_Bill_RT: null, Adjustment_Code: null, Earn_Code: null, Job_Req_Id: null, Earnings_Code: null }];
        const params = JSON.stringify({ "CaseId": this.recordId, "adjustments": dataMatrix });
        
        addAdjustments({ inputJson: params })
            .then(res => {
                const modalFormData = res.map(obj => this.tableData.find(o => o.Id === obj.Id)|| obj);
                this.searchData = res;
                this.isNoAdjustment = false;
                this.refreshModal(modalFormData);
                if (!isModal) {
                    modal.showModal();
                }
                this.showToast('Success', 'Adjustment has been successfully added!', 'success');
            })
            .catch(error => {
                console.log(`handleAddAdjustments===>${error.body.message}`);
                this.showToast('Error!', 'An unexpected error has occured, please contact your Admin.', 'error');
            });

	}

    handleRemoveAdjustments(e){
        const adjId = e.detail;
      
        deleteAdjustments({ adjRecId: adjId })
            .then(res => {
                const modalFormData = res.map(obj => this.tableData.find(o => o.Id === obj.Id)|| obj);
                this.searchData = res;
                if(this.searchData.length > 0){
                    this.refreshModal(modalFormData);
                } else {
                    this.template.querySelector('c-lwc-c-s-c-payroll-adj-modal').handleClose();
                    this.isNoAdjustment = true;
                }
                
                this.showToast('Success', 'Adjustment has been successfully removed!', 'success');
            })
            .catch(error => {
                console.log(`handleRemoveAdjustments===>${error.body.message}`);
                this.showToast('Error!', 'An unexpected error has occured, please contact your Admin.', 'error');
            });
    }

	//for Copy Adjusments
	handleCopyAdjustments(e){
        const adjId = e.detail;
      
        copyAdjustments({ adjRecId: adjId })
            .then(res => {
                const modalFormData = res.map(obj => this.tableData.find(o => o.Id === obj.Id)|| obj);
                this.searchData = res;
                if(this.searchData.length > 0){
                    this.refreshModal(modalFormData);
                } else {
                    this.template.querySelector('c-lwc-c-s-c-payroll-adj-modal').handleClose();
                    this.isNoAdjustment = true;
                }
                
                this.showToast('Success', 'Adjustment has been copied Successfully!', 'success');
            })
            .catch(error => {
                console.log(`handleCopyAdjustments===>${error.body.message}`);
                this.showToast('Error!', 'An unexpected error has occured, please contact your Admin.', 'error');
            });
    }

    updateFormEvt() {
        this.dispatchEvent(new CustomEvent('change', { detail: this.state }));
    }

    handleModalSave(e){
        const eventData = JSON.parse(JSON.stringify(e.detail));
        this.state = { ...this.state, tableAdj: eventData};
        
        const params = JSON.stringify({ 'CaseId': this.recordId, 'adjustments': this.state.tableAdj });
        addAdjustments({ inputJson: params })
            .then(res => {
                this.searchData = res;
                this.refreshModal(this.searchData);
                this.showToast('Success!', 'Table Adjustments have been successfuly saved!', 'success');
            })
            .catch(error => {
                console.log(`handleModalSave ==> ${error.body.message}`);
                this.showToast('Error!', 'An unexpected error has occured, please contact your Admin.', 'error');
            });
    }

    handleBackupTypeConfig(e) {
		if(e.detail !== ""){
            returnBackupTypes({caseRecId: this.recordId})
			.then(res => {
				const parseJSON = res.map(item => JSON.parse(item));
				this.dupeBackupTypeConfig = this.conditionalFields[e.detail].backupTypes;
				this.backupTypeConfig = this.dupeBackupTypeConfig.map(item => {
					if(JSON.stringify(parseJSON[0]).includes(item.labelId)){
						item.selected = true;
					}
					return item;
				});
			})
            .catch(err => console.log(`handleBackupTypeConfig==returnBackupTypes==>${err.message}`));
        }else{
			this.backupTypeConfig = [];
		}
        this.state.adjType = e.detail;
        this.state.backupTypes = [];
    }

    get hasBackupType(){
        return this.backupTypeConfig.length > 0 ? true : false;
    }

    get toggleBackupSection() {
        return this.backupTypeConfig.length > 0 ? '' : 'slds-hide';
    }

    get getAdjType() {
        return this.state.adjType ? this.state.adjType : "";
    }

    get getBackupTypes() {
        // const savedBackups = this.state.backupTypes;
        // if (savedBackups.length > 0) {
        //     const updatedConfig = this.backupConfig.map(el => {
        //         // get value from backup arr
        //     });
        //     return updatedConfig;
        // }
        return this.backupTypeConfig;
	}

    handlePVChange(e){
		this.hasPVWage = e.detail == 'Yes' ? true : false;
		this.refreshModal(this.searchData);
    }

    handleUTAChange(e){
        this.hasUTA = e.detail;
        this.refreshModal(this.searchData);
    }

    handleTableRefresh(e){
        const updatedData = JSON.parse(JSON.stringify(e.detail));
        const params = JSON.stringify({"fieldUpdate": updatedData});
        console.log('params======>>>>>>>'+params);
        calculateDifference({userInput: params})
            .then(res => {
                const parseJSON = res.map(item => JSON.parse(item));
                const updatedData = this.tableData.map(item => {
                    const findIndex = parseJSON.findIndex(el => el.Id === item.Id);
                    if (findIndex !== -1){
                        return { ...parseJSON[findIndex] };
                    }
                    return {...item};
                });
                this.refreshModal(updatedData);
            })
            .catch(err => console.log(`handleTableRefresh==>${err.message}`));
    }

    refreshModal(data){
        this.tableData=data;
        //console.log('DT TABLE'+data);
        //console.log('GROSS D'+this.grossDifference);
        this.grossDifference = 0.00;
        if(typeof this.tableData !== 'undefined'){
            for(let i=0; i<this.tableData.length; i++){
                this.gdResult = '';
                //console.log('LOOP D'+i);
                this.gdResult = this.tableData[i].Gross_Difference__c.toString();
                //console.log('gd result'+this.gdResult);
                this.grossDifference = parseFloat(this.grossDifference) + parseFloat(this.gdResult);
                //console.log('vals'+this.grossDifference);
            }
         }
        const modal = this.template.querySelector('c-lwc-c-s-c-payroll-adj-modal');
        const args = { hasPVWage: this.hasPVWage, hasUTA: this.hasUTA, grossDiff: this.grossDifference };
        modal.formatModalData(data, args);
    }

    // checkNeg(data){
    //     const keyMatrix = ["Pay_Rate_Difference__c", "Bill_Rate_Difference__c", "Original_Gross__c", "Correct_Gross__c", "Gross_Difference__c"];
        
    //     const filteredCols = data.map(item => {
    //         const filterItems = Object.keys(item)
    //             .filter(key => keyMatrix.includes(key))
    //             .reduce((obj, key) => {
    //                 obj[key] = item[key];
    //                 return obj;
    //             }, {});
    //         return filterItems;
    //     });

    //     const updatedCols = [];
    //     filteredCols.forEach(el => {
    //         const dataCols = dataColumns.map(col => {
    //             return {
    //                 ...col,
    //                 cellAttributes: {
    //                     class: (Math.sign(el[col.fieldName]) === -1) ? 'slds-text-color_error' : 'slds-text-color_default'
    //                 }
    //             }
    //         });
    //         updatedCols.push({ tableCols: dataCols });
    //     });
        
    //     return updatedCols;
    // }

	//Handler to upload Files
	handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
        this.showToast('Success!', uploadedFiles.length+' Files uploaded Successfully', 'success');
    }

	//Handler for laod Backups
	handleLoadBackupReqs(e){
		console.log('backupd====521====>>>>>>'+e.detail);
		const backupTypeContainer = this.template.querySelector('.backupReqs');
		if(backupTypeContainer !== null){
			backupTypeContainer.innerHTML = e.detail;
		}
	}


    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}