import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LwcCSCPayrollInputFields extends LightningElement {
    @api formConfig = {};
	@api originId = '';
	proceedSubmit = false;
	condFields = {};
    subSectionFields = {};

    fieldsPreLoaded = true;
    isLoading = false;
    showSection = false;
    showSubSection = false;
    
    hasPVWage = false;
    hasUTA = false;

	@track adjType;
	@track disableReason = true;
	@track isNegAdj = false;
	@track conActInAct;
	@track ispayoutNonDisc = false;
	@track isgiftcardNonDisc = false;

	// variables for wage reduction process
	@track isWageReduction = false;
	@track showPayArrang = false;
	@track showchkMoneyOrdCol2 = false;
	@track showchkMoneyOrdCol1 = false;
	@track showExplainDetailCol2 = false;
	@track showExplainDetailCol1 = false;
	@track showManualDeductCol1 = false;
	@track showPleaseExplain = false;
    
    connectedCallback() {
        this.isLoading = true;
        this.showSection = false;
        this.showSubSection = false;
    }

    handleFormLoad(e) {
        this.isLoading = true;
		const record = e.detail.records;
        
		if (record && this.fieldsPreLoaded){
            const fields = record[this.originId].fields; // record['0030K000026Y******'].fields;
            const adjName = (fields.Adjustment_Type__c || {}).value;
			this.adjType = adjName;
			/*if(this.adjType === "Negative Adjustment"){
				this.isNegAdj = true;
			}*/
			const rateOrTime = (fields.Is_this_a_Rate_or_Time_adjustment__c || {}).value;
            const actionReq = (fields.Action_Required__c || {}).value;
            const reasonForAdj = (fields.Reason_for_Adjustment__c || {}).value;
			const discOrNonDisc = (fields.Is_the_payout_discretionary__c || {}).value;
			const conActive = (fields.Is_the_contractor_active_or_inactive__c || {}).value;
			// variables for wage reduction
			const minWageRiskVal = (fields.Does_this_adjustment_pose_a_min_wage__c || {}).value;
			const payAggrVal = (fields.Has_payment_arrangement__c || {}).value;
			const moneyOrderVal = (fields.Do_you_have_a_Check_Money_Order__c || {}).value;
			const expDetailVal = (fields.Explain_in_detail_the_action_item__c || {}).value;
			const manDeductVal = (fields.Will_payroll_be_manually_deducting_this__c || {}).value;
			
			this.conActInAct = conActive;
			this.hasPVWage = (fields.Does_this_contractor_require_prevailing__c || {}).value; // === "Yes" ? true : false;
            this.hasUTA = reasonForAdj === "Unrecouped Travel Advance" ? true : false;
			if(this.hasPVWage){
                this.dispatchEvent(new CustomEvent('pvchange', { detail: this.hasPVWage }));
            }
            
            if(this.hasUTA){
                this.dispatchEvent(new CustomEvent('utachange', { detail: this.hasUTA }));
            }
            
			if(rateOrTime !== '' && rateOrTime){
				this.dispatchEvent(new CustomEvent('configchange', { detail: rateOrTime }));
			}
            if (adjName !== '' && adjName) {
                if (this.formConfig[adjName].colOne.fields) {
                    this.isLoading = true;
                    this.showSection = true;
                    this.condFields = this.formConfig[adjName];
                }
                //this.dispatchEvent(new CustomEvent('configchange', { detail: adjName }));
            }

            if (actionReq !== '' && actionReq) {
                if (this.formConfig[actionReq].colOne.fields) {
                    this.isLoading = true;
                    this.showSubSection = true;
                    this.subSectionFields = this.formConfig[actionReq];
                }
			}

			if(discOrNonDisc !== '' && discOrNonDisc && discOrNonDisc === "Non Discretionary"){
				this.ispayoutNonDisc = true;
				if(actionReq === "Gift cards (Potential FLSA Requirement)"){
					this.isgiftcardNonDisc = true;
				}
			}

            if (adjName !== '' && adjName && adjName.includes("Negative Adjustment")) {
				if(reasonForAdj !== "Wage Reduction / Contractor Overpaid"){
					this.reasonForAdjToast(reasonForAdj);
				}else{
					this.isWageReduction = true;
					if(minWageRiskVal === "Yes"){
						this.showPayArrang = true;
					}else if(minWageRiskVal === "No"){
						this.showchkMoneyOrdCol2 = true;
					}
					if(payAggrVal === "Yes"){
						this.showchkMoneyOrdCol1 = true;
					}else if(minWageRiskVal === "Yes" && payAggrVal === "No"){
						this.showManualDeductCol1 = true;
					}
					if(moneyOrderVal === "No"){
						this.showExplainDetailCol2 = true;
					}
					if(manDeductVal === "No"){
						this.showPleaseExplain = true;
					}
				}
                if(conActive === "Active"){
					this.isNegAdj = true;
				}
            }
            
            
            
            
        }
        if(e.detail){
            this.isLoading = false;
        }
    }

	handleFormSuccess(e){
        this.showToast('Success', 'Form Updated Successfully!', 'success');
    }
    
    handleFormError(e){
		console.log('error=====>>>>>'+e.detail.output.errors);
		console.log('field error======>>>>>'+e.detail.output.fieldErrors);
		console.log('field error===Json===>>>>>'+JSON.stringify(e.detail.output.fieldErrors));
		this.isLoading = false;
		if(e.detail.output.errors.length !== 0 && e.detail.output.errors[0].errorCode === 'ENTITY_IS_LOCKED'){
			this.showToast('Error', 'Payroll form is locked while pending Payroll approval.', 'error');
		}else{
			this.showToast('Error', 'An Unexpected Error Occured, please contact your Admin.', 'error');
		}
    }

    @api 
    submitForm(){
		this.proceedSubmit = true;
		this.template.querySelectorAll('lightning-input-field').forEach(element => {
		//console.log('element title - '+element.fieldName+' - '+element.required+' - '+element.value);
			if(element.required === true && (element.value === null || element.value === '')){
				this.proceedSubmit = false;
				element.reportValidity();
			}
        });
		console.log('Proceed '+this.proceedSubmit);
		if(this.proceedSubmit){
			this.template.querySelector('lightning-record-edit-form').submit();
		} 

        this.dispatchEvent(new CustomEvent('validation', { detail: this.proceedSubmit }));
    }
	

    get toggleSection() {
        return this.showSection;
    }

    get toggleAccordionSection() {
        if (this.showSection) {
            return '';
        }
        return 'slds-hide';
    }

    handleAdjType(e) {
        const selectionType = e.target.value;
        this.adjType = selectionType;
		console.log('conActInAct=======>>>>>>'+this.conActInAct);
		if(this.adjType === "Negative Adjustment" && this.conActInAct === "Active"){
			this.isNegAdj = true;
		}else{
			this.isNegAdj = false;
		}

		if(this.adjType !== "Negative Adjustment"){
			this.isWageReduction = false;
		}
		
        this.template.querySelectorAll('lightning-input-field').forEach(input => {
            if (!input.required) {
                input.reset();
            }
        });
        this.showSection = false;
        this.showSubSection = false;
        this.hasPVWage = false;
        this.hasUTA = false;

        if (selectionType !== "" && this.formConfig[selectionType].colOne.fields) {
            this.isLoading = true;
            this.showSection = true;
            this.condFields = this.formConfig[selectionType];
            this.fieldsPreLoaded = false;

            const hasNegAdj = selectionType === "Negative Adjustment";
            const dataId = hasNegAdj ? "reasonForAdj" : "actionReq";

            setTimeout(() => {
                this.template.querySelectorAll('lightning-input-field').forEach(el => { 
                    if ((el.dataset.id || "") === dataId) {
                        if ((el.value || "") !== "") {
                            if (hasNegAdj) {
                                this.reasonForAdjToast(el.value);
                            } else {
                                this.showSubSection = true;
                                this.subSectionFields = this.formConfig[el.value];
                            }
                        } else {
                            this.showSubSection = false;
                        }
                    }
                });
            }, 1000);
        }

        this.dispatchEvent(new CustomEvent('pvchange', { detail: this.hasPVWage }));
        this.dispatchEvent(new CustomEvent('utachange', { detail: this.hasUTA }));
        //this.dispatchEvent(new CustomEvent('configchange', { detail: selectionType }));
    }

	handlerateTimeAdj(e){
		const selectionType = e.target.value;
		this.dispatchEvent(new CustomEvent('configchange', { detail: selectionType }));
	}

    handleSubSection(e) {
        const fieldId = e.target.dataset.id;
        const selectionType = e.target.value;
		let actionReqVal = '';
        this.hasUTA = false;
		if (fieldId.includes("actionReq")){
            this.showSubSection = false;
			if (selectionType !== "") {
				actionReqVal = selectionType;
				this.isLoading = true;
                this.showSubSection = true;
                this.subSectionFields = this.formConfig[selectionType];
            } 
        }
        if (fieldId.includes("reasonForAdj")){
			if(selectionType !== "Wage Reduction / Contractor Overpaid"){
				this.reasonForAdjToast(selectionType);
				this.isWageReduction = false;
            }else if(selectionType === "Wage Reduction / Contractor Overpaid"){
				this.isWageReduction = true;
			}
            if (selectionType === "Unrecouped Travel Advance"){
                this.hasUTA = true;
            }
            this.dispatchEvent(new CustomEvent('utachange', { detail: this.hasUTA }));
        }
		if(fieldId.includes("payoutDiscretionary") || fieldId.includes("giftcardsDiscretionary")){
			if(selectionType === "Non Discretionary"){
				this.ispayoutNonDisc = true;
			}else{
				this.ispayoutNonDisc = false;
			}
		}else{
				if(fieldId.includes("actionReq")){
					this.ispayoutNonDisc = false;
					this.isgiftcardNonDisc = false;
				}
		}
		if(fieldId.includes("minWageRisk") && selectionType === "Yes"){
			this.showPayArrang = true;
			this.showchkMoneyOrdCol2 = false;
			this.showExplainDetailCol1 = false;
			this.showPleaseExplain = false;
		}
		if(fieldId.includes("minWageRisk") && selectionType === "No"){
			this.showPayArrang = false;
			this.showchkMoneyOrdCol2 = true;
			this.showchkMoneyOrdCol1 = false;
			this.showManualDeductCol1 = false;
			this.showExplainDetailCol1 = false;
			this.showExplainDetailCol2 = false;
			this.showPleaseExplain = false;
		}
		if(fieldId.includes("hasPayArr") && selectionType === "Yes"){
			this.showchkMoneyOrdCol1 = true;
			this.showManualDeductCol1 = false;
			this.showExplainDetailCol2 = false;
			this.showPleaseExplain = false;
		}
		if(fieldId.includes("hasPayArr") && selectionType === "No"){
			this.showManualDeductCol1 = true;
			this.showchkMoneyOrdCol1 = false;
			this.showExplainDetailCol2 = false;
			this.showPleaseExplain = false;
		}
		if(fieldId.includes("chckMoneyOrderCol1")){
			if(selectionType === "No"){
				this.showExplainDetailCol2 = true;
			}else{
				this.showExplainDetailCol2 = false;
				this.showExplainDetailCol1 = false;
				this.showPleaseExplain = false;
			}
		}
		if(fieldId.includes("chckMoneyOrderCol2")){
			if(selectionType === "No"){
				this.showExplainDetailCol1 = true;
			}else{
				this.showExplainDetailCol1 = false;
				this.showExplainDetailCol2 = false;
				this.showPleaseExplain = false;
			}
		}
		if(fieldId.includes("willManDedCol1")){
			if(selectionType === "No"){
				this.showPleaseExplain = true;
			}else{
				this.showExplainDetailCol1 = false;
				this.showExplainDetailCol2 = false;
				this.showPleaseExplain = false;
			}
		}

		
		/*if((fieldId.includes("chckMoneyOrderCol1") || fieldId.includes("chckMoneyOrderCol2")) && selectionType === "Yes"){
			this.showToast("Message:", '"M" should be selected under "Is this a Y (Off-Cycle entered by associate) or M (Off-cycle entered by payroll)?"', "warning");
		}*/
		if(fieldId.includes("earnCodeWeekending") && selectionType === "No"){
			this.showToast("Message:", "Submit Correction request to payroll", "warning");
		}
		if(fieldId.includes("payoutNETAmount") && selectionType === "Yes"){
			this.showToast("Message:", "Please attach email or spreadsheet showing gross up calculations", "warning");
		}
		if(fieldId.includes("giftGrossUps") && selectionType === "No"){
			this.showToast("Message:", "Gross up's are required please contact payroll", "warning");
		}
        this.fieldsPreLoaded = false;
		// added to stop loading
		//console.log('e.detail=======>>>>>>>>'+e.detail);
		if(e.detail){
            this.isLoading = false;
        }
    }

	handleWages(e){
		const selectionType = e.target.value;
		console.log('Wages=====>>>>>>'+selectionType);
		console.log('this.adjType======>>>>>>'+this.adjType);
		if(this.adjType !== "" && this.adjType !== "Negative Adjustment" && selectionType === "Reduce Wages"){
			this.showToast("Reduce Wages", "Please navigate to the Negative Adjustment form for all adjustments that reduce wages", "warning");
		}
	}

	handleConActive(e){
		const selectionType = e.target.value;
		console.log('conActive=====>>>>>>'+selectionType);
		if(this.adjType === "Negative Adjustment" && selectionType === "Active"){
			this.isNegAdj = true;
		}else{
			this.isNegAdj = false;
		}
	}

    reasonForAdjToast(selection) {
        let toastMessage = "";
        switch (selection) {
            case "Company/Division Does Not Match":
                toastMessage = "Negative Hours will not deduct automatically because it's a different company or division [Ex. EASi (division 50) will never deduct from Commercial (ONS-division 6)].";
                break;
            case "Earn Codes Do Not Match":
                toastMessage = "Negative Hours will not deduct automatically on its own because the earn codes do not talk. (Ex. Expenses will not deduct from regular wages).";
                break;
            case "Unrecouped Travel Advance":
                toastMessage = "Must attach Travel Advance authorization form/deduction form.";
                break;
            default:
                break;
        }
        this.showToast(selection, toastMessage, "info");
    }

    get getLoadingState() {
        if (this.formConfig && this.showSection) {
            return true;
        }
        return false;
    }

    handlePrevailingWage(e){
        this.hasPVWage = e.target.value
        this.dispatchEvent(new CustomEvent('pvchange', { detail: this.hasPVWage }));
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'sticky'
        });
        this.dispatchEvent(evt);
    }

}