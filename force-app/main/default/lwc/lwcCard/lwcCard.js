import { LightningElement, track, api } from 'lwc';

const toggle = {
    expanded: 'collapsed',
    collapsed: 'expanded'
}

export default class LwcCard extends LightningElement {
    @track state = 'expanded';
    @api collapsible = false;
	 @api cssCard;

    contentToggle() {
        this.state = toggle[this.state];
    }
}