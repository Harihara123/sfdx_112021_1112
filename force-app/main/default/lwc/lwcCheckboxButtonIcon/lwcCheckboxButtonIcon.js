import { LightningElement, api } from 'lwc';

export default class LwcCheckboxButtonIcon extends LightningElement {
	@api buttonId;
	@api label;
	@api buttonName;
	@api selected;
	@api disabled;
	@api disabledText;
	@api isSlot;

    buttonState = false;

    connectedCallback() {
		if(this.selected){
            this.buttonState = true;
        }
    }

    get getButtonState(){
        return this.buttonState ? 'button-icon selected' : 'button-icon';
    }

    get getCheckboxState(){
        if (this.selected){
            return true;
        }
        return this.buttonState;
    }

    get getButtonTitle(){
        if (this.disabled.includes("true")){
            return this.disabledText;
        }
		return this.label+this.disabledText;
    }

    handleButtonClick(e){
        this.buttonState = !this.buttonState;

        const buttonIcon = this.template.querySelector(".button-icon");

        if (this.buttonState) {
            buttonIcon.classList.add("selected");
			this.selected = true;
        } else {
            buttonIcon.classList.remove("selected");
			this.selected = false;
        }
        e.stopPropagation();
        this.buttonSelectEvent();
    }

    buttonSelectEvent() {
        const buttonLabel = this.buttonState ? this.label : '';
        const params = { 'label': buttonLabel, 'id': this.buttonId, 'selected':this.selected };
		const buttonEvt = new CustomEvent('select', { detail: params });
        this.dispatchEvent(buttonEvt);
    }

	boardSlotClick() {
		const params = { 'boardId': this.buttonId, 'boardName' : this.buttonName };
		const activePostingsEvt = new CustomEvent('viewactivepostings', { detail: params });
        this.dispatchEvent(activePostingsEvt);
	}
}