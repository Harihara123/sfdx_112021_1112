import { LightningElement, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

// importing Apex Class
import communityManagerAlignmentByRunningUser from '@salesforce/apex/TC_CommunityManagerAlignmentController.getCommunityManagerAlignmentByRunningUser';
import getAllRegionName from '@salesforce/apex/TC_CommunityManagerAlignmentController.getAllRegionName';
import updateCommunityManagerAlignment from '@salesforce/apex/TC_CommunityManagerAlignmentController.updateCommunityManagerAlignmentRecord';

export default class LwcCommunityManagerAlignment extends LightningElement {
    @track strOfficeCode = '';
    @track strFilterCriteria='';
    @track strRegionCode = '';    
    @track allRegionItems = [];
    @track data = [];
    @track sortBy = 'office_code';
    @track sortDirection = 'asc';
    @track label = '';
    @track value='all';
    showSpinner = false;

    lookupAdditionlFields = [{ label: 'Title', fieldApiName: 'Title', dataType: 'text' }]


    tableCols = [ //Define columns for the table and associated field to be used as an identifier.
        {label:'Region Code', field:'RegionCode', sort: true, style: 'width: 8rem;'},
        {label:'Region Name', field:'RegionName', sort: true},
        {label:'Office Code', field:'OfficeCode', sort: true, style: 'width: 7rem;'},
        {label:'Office Name', field:'OfficeName', sort: true},
        {label:"Talent Community Manager", field:"TalentCommunityManagerName", sort: true},
        {label:'Last Modified By', field:'LastModifiedByName', sort: true},
        {label:'Last Modified Date', field:'LastModifiedDate', sort: true, type:'date', style: 'width: 10rem;'}        
    ];

    selectedRow = '';
    selectedRows = [];
    selectedManager;

    
    officeFilter = [        
        {label: 'All Offices', value: 'all'},
        {label: 'Inactive Community Manager Offices', value: 'inactive'},
        {label: 'Offices without Community Manager', value: 'noManager'}
    ]
    regionFilter = [
        {label: 'All Regions', value: 'all'},
        {label: 'Aerotek Central', value: 'AerotekCentral'},
        {label: 'Aerotek Corporate', value: 'AerotekCorportate'}
    ]
    lookupDisplay = [{ label: 'Title', fieldApiName: 'Title', dataType: 'text' }];

    filters = {};
    showDialog = false;

    handleOfficeCode(event) {  
        this.label = '';      
        this.strOfficeCode = event.target.value;        
    }

    handleRegionNameChange(event){
        this.label = '';
        this.strRegionCode = event.target.value;        
    }

    handleFilterCriteriaChange(event) {
        this.label = '';
        this.strFilterCriteria = event.target.value;
    }

    handleSearchManagerAlignment(event){

        let errorMsg = '';
        
        this.parseResponse();    
    }

    handleRowEdit(event) {        
        let isBulk = false;
        let updateRowIdList = [];
        let commMgrId = event.detail.navItem.id;
        updateRowIdList.push(event.detail.rowId);
        

        this.updateCommunityManagerAligmentRecord(updateRowIdList,commMgrId,isBulk);
    }

    handleSort(event) {
        this.label = '';
        this.sortData(event.detail.sortField,event.detail.sortOrder);        
    }

    handleMassUpdate() {
        const table = this.template.querySelector('c-lwc-data-table');
        const selectedRows = table.getSelected();

        if (selectedRows.length < 2) {           
            this.showToast('error', 'Error', 'Please select at least 2 records to proceed.');
            //this.label = "Please select a record(s) for mass update."
        } else {
            //Success: Initiate mass update
            this.label = '';
            let rows = [];
            selectedRows.forEach(row => {rows.push(row.id)});
            this.selectedRows = rows;
            this.showDialog = true;
        }
    }

    handleSearch(event) {
        this.label = '';       
    }

    handleSelectedForMass(event) {        
        this.label = '';       
        this.selectedManager = event.detail;        
    }
    handleMassSave() {
        if (this.selectedManager && this.selectedRows.length) {
            const payload = {managerId: this.selectedManager.lookupid.substring(1,this.selectedManager.lookupid.length-1), rows: this.selectedRows};

            let isBulk = true;
            let commMgrId = this.selectedManager.lookupid.substring(1,this.selectedManager.lookupid.length-1);
    
            this.updateCommunityManagerAligmentRecord(this.selectedRows,commMgrId,isBulk);

            this.closeDialog();
        } else {
            //Show error
        }
    }

    handleFilterChange(event) {
        this.label = '';
        const filter = event.target.getAttribute('data-filter');
        this.filters[filter] = event.target.value;        
    }

    closeDialog() {
        this.showDialog = false;
    }

    updateCommunityManagerAligmentRecord(updateRowIdList, commMgrId, isBulk){
        this.showSpinner = true;
        updateCommunityManagerAlignment({updateRowIds : updateRowIdList,updateCommunityManagerAlignmentId: commMgrId, isBulkEdit: isBulk })
        .then(responseMessage => {
            this.showSpinner = false;
            this.parseResponse();
            //this.label = "Talent community manager has been updated successfully";
            this.showToast('success', 'Success', 'Talent community manager has been updated successfully');

        }).catch((err) => {
            this.showToast('error', 'Error', 'Updated did not suceed. Try again.');
            this.showSpinner = false;
		});

    }

    parseResponse() {
        let currentData = [];

        if(this.strFilterCriteria == '')
            this.strFilterCriteria = 'all';
        
        if(this.strRegionCode == '')
            this.strRegionCode = 'all';

        this.showSpinner = true;
        communityManagerAlignmentByRunningUser({officeCode : this.strOfficeCode,filterCriteria : this.strFilterCriteria, regionCode: this.strRegionCode})      
        .then(responseMessage => {  
            this.showSpinner = false;         
            responseMessage.forEach((row) => {                
                let cells = []
                let formattedRow = {}

                formattedRow.id = row.Id || '';
                this.tableCols.forEach(col => {                    
                    let value = row[col.field] || ''

                    if (col.field === 'TalentCommunityManagerName') {
                       cells.push({
                           value: row.TalentCommunityManagerTitle?`${value} (${row.TalentCommunityManagerTitle})`:value,
                           edit: {type: 'lookup'},
                           params: {
                               dataField: "Name",
                               recordTypeName: '', 
                               placeholder: "Talent Community Manager", 
                               clasName: 'Lookup',
                               methodName: 'searchSObject',
                               objectName: "User",
                               isSoql: 'true',
                               useCache: false,
                               addlFields: JSON.stringify(["Title"]),
                               displayFields: [{ label: 'Title', fieldApiName: 'Title', dataType: 'text' }],
                               relatedTo: "User",
                               isCustomCSS: true,
                               recordLimit: '6',
                               firstRecordSearchIcon: "true",
                               searchLookupText: "Community Manager",
                            },
                           navItem: {id: row['TalentCommunityManagerId'] || '', object: "User"}
                       })
                    } else if (col.field === 'LastModifiedByName') {
                        cells.push({
                            value,
                            navItem: {id: row['LastModifiedById'] || '', object: "User"}
                        })
                    } else if (col.field === 'LastModifiedDate') {
                        cells.push({
                            value: new Date(value).toISOString().split('T')[0],
                            date: true
                        })
                    } else {
                        cells.push({value});
                    }
                })

                formattedRow.cells = cells;
                currentData.push(formattedRow);
            });
       
        this.data =currentData;

        }).catch((err) => {
            this.showSpinner = false;
            this.showToast('error', 'Error', 'Failed to retrieve records.');
		});
    }

    getFormattedDate(date) {
        var year = date.getFullYear();
      
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
      
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        
        return month + '/' + day + '/' + year;
      }

    @wire(getAllRegionName)
    wiredAllRegionName({error,data}) {
        if(data){
            this.allRegionItems = [...this.allRegionItems,{value: 'all', label: 'All Regions'}];
            if(data.length > 0 ){
                var i;
                for(i=0;i<data.length;i++) {
                    this.allRegionItems = [...this.allRegionItems,{value: data[i].RegionNameKey, label: data[i].RegionNameValue}];
                }
            this.error = undefined;
            }
            
        }else if(error){
            this.error=error;            
        }
    }

    get getAllRegionName(){
            return this.allRegionItems;
    }
    showToast(variant, title, message) {
        const event = new ShowToastEvent({
			title,
			variant,
            message
        });
        this.dispatchEvent(event);
	}
}