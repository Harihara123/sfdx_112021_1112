import { LightningElement, api } from 'lwc';

export default class LwcCustomTextArea extends LightningElement {
    
    @api value;
    @api placeholder;    
    @api errors;
    @api hasError;

	//D-14458 - Monika - Make Keyword field error message JAWS compatible and focus the keywords text area if there is any error with keywords field
	renderedCallback() {
         const textAreaEl = this.template.querySelector('textarea');
		 if(this.hasError)
			textAreaEl.focus();
    }

    // renderedCallback() {
    //     console.log(this.template.querySelector('textarea').value);
    // }
    
    // @api 
    // get hasError() {
    //     return this.this.inputClass
    // }
    // set hasError(val) {
    //     if (val) {
    //         this.this.inputClass = 'slds-textarea slds-has-error'
    //     } else {
    //         this.inputClass = 'slds-textarea';
    //     }
    // }

    onInput(e) {
        const {clientHeight, scrollHeight, value} = e.target;
        if(scrollHeight > clientHeight) {
            e.target.style.height = scrollHeight+'px';  
        }              
        this.value = value;
    }
    onBlur(e) {
        e.target.removeAttribute('style');
        //e.target.setAttribute('rows', 1);
    }
    onFocus(e) {
        const {clientHeight, scrollHeight } = e.target;
        if(scrollHeight > clientHeight) {
            e.target.style.height = scrollHeight+'px';  
        }   
    }
    onKeyDown(e) {
        if (e.keyCode === 13) {
            this.search(e);
        }
    }
    get inputClass() {
        console.log('checking input err class')
        return this.hasError?'slds-textarea slds-has-error':'slds-textarea'
    }
    
    @api search(e = null) {
        const searchEvent = new CustomEvent('search', { detail: {value: this.value} });        
        this.dispatchEvent(searchEvent);
       
        if (e) {
            e.target.blur();
        } else {
            this.template.querySelector('textarea').blur()
        }
    }
    
}