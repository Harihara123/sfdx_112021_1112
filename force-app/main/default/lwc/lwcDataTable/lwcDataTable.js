import { LightningElement, api, track } from 'lwc';

export default class LwcDataTable extends LightningElement {
    //@api tableDataOld
    @track paginatedTableData = [];    
    @api tableColumns
    @api actions
    @api rowActionHighlight;
    @api selection;
    @api rowSelect;
    
    @api rowId;
    @api selectedRowIds;

    @api showSpinner = false;
    @api pagination = false;
    @api rowsPerPage = 5;
    @api stickyHeader;   
    @api isDatabaseSort = false; 
    @track multiSelect;
    @track tData = [];
    @track noResults;
    @api noResultsMsg = '';    
    @api disableReset = false;
    stickyHeaderCSS = ''    
    selectedRows = [];
    resizeHandler = this.resize.bind(this);
    removeHandler = this.removeEL.bind(this);
    resizing;
    sorting;


    @api
    get tableData() {
        return this.paginatedTableData ? this.paginatedTableData : [];
    }
    set tableData(tableData) { 
        if (tableData && tableData.length) {
            this.noResults = false;

            if(!this.disableReset){
                this.resetColumnSelect();
            }
            // Pass in STRING value
            if (!this.pagination) {            
                this.paginatedTableData = tableData;       
            } else {
                this.tData = tableData;
            } 
        } else {
            this.noResults = true;
        }      
       
        
    }

    connectedCallback() {
        //console.log(this.tableData);
        if (this.selection && this.selection === 'multi') {
            this.multiSelect = true;
        }
        // if (this.pagination) {
        //     this.tData = this.paginatedTableData;
        // }
        if (this.stickyHeader) {
            this.stickyHeaderCSS = 'sticky'
        }
    }
    handleColumnSelect(e) {
        let rows = this.template.querySelectorAll('c-lwc-data-table-row');
        //console.log(rows.length)
        let updatedRows = [];

        if (rows.length) {
            rows.forEach(row => {
                row.selectRow(e.target.checked);
                if(!e.target.checked){
                    updatedRows.push(row.rowId);
                }
            });
        }
        const params = {
            selected: e.target.checked,
            updatedRows: updatedRows
        }
        this.dispatchEvent(new CustomEvent('selectedrow', { detail: params }));
    }
    @api
    resetColumnSelect() {
        const selectAll = this.template.querySelector('th input');
        if (selectAll) {
            selectAll.checked = false;
            this.handleColumnSelect({target: {checked: false}})
        }
    }
    @api
    getSelected() {
        let rows = this.template.querySelectorAll('c-lwc-data-table-row');
        let selectedRows = [];
        if (rows.length) {
            rows.forEach(row => {
                let selectedRow = row.returnRow();
                if(selectedRow) {                    
                    selectedRows.push(selectedRow)                 
                }
            })           
        }
        return selectedRows;
    }
    @api
    removeRow() { 
        // Needs a row id of the row being unchecked
        let rows = this.template.querySelectorAll('c-lwc-data-table-row');

        rows.forEach(row => {
            if (row.rowId === this.rowId){
                row.selectRow(false);
            }
        });
    }

    dispatchAction(e) {
        if (this.rowActionHighlight) {
            let rows = this.template.querySelectorAll('c-lwc-data-table-row');
            rows.forEach((row) => {
                if (e.target === row) {                    
                    row.classList.add('last-action');
					row.classList.remove('bg-color-first');
                } else {                    
                    row.classList.remove('last-action');
					row.classList.remove('bg-color-first');               
                }
            })
        }
        //console.log(e.target)
        this.dispatchEvent(new CustomEvent('rowaction', {detail: e.detail}));
    }
    dispatchEdit(e) {       
        let newTableData;
        if(this.pagination) {
            //console.log('edit event dataTable: ', JSON.stringify(e.detail))
            newTableData = JSON.parse(JSON.stringify(this.tData));
            const rowIndex = newTableData.findIndex(row => row.id === e.detail.rowId)
            const cellIndex = this.tableColumns.findIndex(cell => cell.field === e.detail.column)
            
            newTableData[rowIndex].cells[cellIndex].value = e.detail.value;
            this.tData = newTableData;            
        }        
        this.dispatchEvent(new CustomEvent('edit', {detail: e.detail}))  

    }
    resize(e) {             
        let {col} = this.resizing;
        //let dX = e.pageX - col.getBoundingClientRect().left
        //console.log(dX)       
        col.style.width = e.pageX - col.getBoundingClientRect().left +'px'
        // if (col.nextElementSibling) {
        //     col.nextElementSibling.style.width = e.pageX - col.nextElementSibling.getBoundingClientRect().right +'px'
        // }
    }
    removeEL(e) {
        //console.log('tryign to remove listeners..')
        document.removeEventListener('mousemove', this.resizeHandler);
        document.removeEventListener('mouseup', this.removeHandler);
    }
    mDown(e) {
        this.resizing = {col: e.target.parentElement, initPos: e.pageX}
        document.addEventListener('mousemove', this.resizeHandler);
        document.addEventListener('mouseup', this.removeHandler);
    }
    mUp() {
        document.removeEventListener('mousemove', this.resizeHandler);
    }
    resetCols() {
        let cols = this.template.querySelectorAll('thead th');
        cols.forEach(col => {
            col.style.width = null;
        })
    }
    handleRowChange(e) {
        // console.log(e.detail)
        let tableHeaderSelect = this.template.querySelector('thead input[type="checkbox"]');
        let rows = this.template.querySelectorAll('c-lwc-data-table-row');
        let updatedRowIds = [];

        if ((tableHeaderSelect || {}).checked) {
            if (e.detail.checked) {
                const totalRows = rows.length;
                let selectedRows = 0;
               
                if (rows.length) {
                    rows.forEach(row => {
                        let selectedRow = row.returnRow();
                        if(selectedRow) {                    
                            selectedRows++; 
                        } else {
                            updatedRowIds.push(e.detail.id);
                        }
                    })           
                }
                if (totalRows === selectedRows) {
                    tableHeaderSelect.indeterminate = false;
                }

            } else {
                //console.log('should make the icon for table header input be indeterminate')
                tableHeaderSelect.indeterminate = true;
            }
        }

        if (!e.detail.checked){
            updatedRowIds.push(e.detail.id);
        }

        
        // let selected = e.detail;
        const params = {
            selected: e.detail.checked,
            updatedRows: updatedRowIds
        }
        this.dispatchEvent(new CustomEvent('selectedrow', { detail: params }));
    }
    handlePagination(e) {
        //console.log('updates from pagination', e.detail)
        if(this.disableReset){
            this.paginatedTableData = e.detail;
            const selectAll = this.template.querySelector('th input');
            if (selectAll) {
                selectAll.checked = false;
            }
            setTimeout(() => {
                let rows = this.template.querySelectorAll('c-lwc-data-table-row');
                rows.forEach(row => {
                    if (this.selectedRowIds.indexOf(row.rowId) !== -1) {
                        row.selectRow(true);
                    }
                });
            }, 100);

            this.dispatchEvent(new CustomEvent('pagination'));
            
        } else {
            this.resetColumnSelect();
            this.paginatedTableData = e.detail;
        }
        
    }
    compare(key, order = 'asc') {
        return (a, b) => {
            const valA = (a.cells[key].value || '').toUpperCase();                        
            const valB = (b.cells[key].value || '').toUpperCase();
    
            let comp = 0;
            if (valA > valB) {
                comp = 1;
            } else if (valA < valB) {
                comp = -1;
            }
            return ((order === 'desc') ? (comp * -1) : comp);
        }
    }
    handleSort(e) {
        const field = e.target.getAttribute('data-field')
        const colIndex = this.tableColumns.findIndex(col => col.field === field);

        const sortToggle = {
            asc: 'desc',
            desc: 'asc'
        }

        if(this.sorting) {
            if(this.sorting.field === field) {
                this.sorting.sort = sortToggle[this.sorting.sort];
            } else {
                this.sorting = {field, sort: 'asc'}
            }
        } else {
            this.sorting = {field, sort: 'asc'}
        }
        if(this.isDatabaseSort) {
			 this.dispatchEvent(new CustomEvent('databasesort', {detail: {"sortField":field, "sortOrder":this.sorting.sort}}));
		}
		else {
			this.showSpinner= true;
			setTimeout(() => {
				if (this.pagination) {
					let newPaginatedData = JSON.parse(JSON.stringify(this.tData));
					newPaginatedData.sort(this.compare(colIndex, this.sorting.sort));
					this.tData = newPaginatedData;
				} else {
					let newPaginatedData = JSON.parse(JSON.stringify(this.paginatedTableData));
					newPaginatedData.sort(this.compare(colIndex, this.sorting.sort));
					this.paginatedTableData = newPaginatedData;
				}
				this.showSpinner = false;
			}, 0)
		}

    }
    get getFirstRowClass() {
        return this.rowActionHighlight?'bg-color-first':''
    }
}