import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

//templates
import dataCell from './lwcDataTableCell.html';
import lookup from './lookup.html';

export default class LwcDataTableCell extends NavigationMixin(LightningElement) {
    @api cellData;
    @api colIndex;
    @api columns;
    @track editting = false;
    @track options;
    params = null;
    @track url;

    connectedCallback() {
        if (this.cellData.params) {
            let params = {};
            Object.keys(this.cellData.params).forEach(key => {
                params[key] = this.cellData.params[key];                
            })
            this.params = params;
        }
        if (this.cellData.navItem) {
            this.generateURL(this.cellData.navItem.id, this.cellData.navItem.object);
            // this.accountHomePageRef = {
            //     type: "standard__recordPage",
            //     attributes: {
            //         "recordId": this.cellData.navItem.id,
            //         "objectApiName": this.cellData.navItem.object,
            //         "actionName": "view"
            //     }
            // };
            // this[NavigationMixin.GenerateUrl](this.accountHomePageRef)
            // .then(url => {
            //     this.url = url
            //     //console.log(url)
            // });
        }

        if (this.cellData.edit && this.cellData.edit.options) {
            let newOptions = JSON.parse(JSON.stringify(this.cellData.edit.options))
            
            if (!newOptions.includes(this.cellData.value)) {
                newOptions.unshift(this.cellData.value);
            } else {
                newOptions.forEach((option, i) => {
                    if (option === this.cellData.value) {
                        newOptions.splice(i, 1);
                        newOptions.unshift(option)
                    }
                })
            }
            this.options = newOptions;
        }
        if (!this.cellData.value) {
            //this.cellData.value = ''
            let newCellData = JSON.parse(JSON.stringify(this.cellData))
            newCellData.value = '';
            this.cellData = newCellData
        }
		
		if(this.cellData.date && this.cellData.time){ 
			if(!this.cellData.title){
				this.cellData.title = '';
			}
		}        
    }

    renderedCallback() {
        if (this.editting) {
           
            let input = this.template.querySelector('input')

            const inlineEdit = this.template.querySelector('div.inline-edit')

            if (!input) {
                input = this.template.querySelector('select')
            }
            if(this.cellData.date) {
                input = this.template.querySelector('lightning-input')
            }
            if ((this.cellData || {}).edit.type === 'lookup') {
                input = this.template.querySelector('c-search-lookup-component')
            }
            if (input) {
                input.focus()
            }
            

            //getting values of the viewport as well as input containters
            //coordinates in relation to iewport
            const {innerWidth, innerHeight} = window;
            const {bottom, right} = inlineEdit.getBoundingClientRect();

            //getting delta x and delta y
            const dX = innerWidth-right;
            const dY = innerHeight-bottom;

            //if negative number, element is pushing out of the viewport
            // adjusting the positioning of the element.            
            if (dX < 0) {                                
                inlineEdit.style.right = '0'
            }
            if (dY < 0) {
                inlineEdit.style.top = 'unset'
                inlineEdit.style.bottom = '0'
            }

        }
    }
    render() {        
        if ((this.cellData || {}).edit && (this.cellData || {}).edit.type === 'lookup') {
            return lookup
        }
        return dataCell;
    }
    generateURL(id, object,callback = () => {}) {
        this.accountHomePageRef = {
            type: "standard__recordPage",
            attributes: {
                "recordId": id,
                "objectApiName": object,
                "actionName": "view"
            }
        };
        this[NavigationMixin.GenerateUrl](this.accountHomePageRef)
        .then(url => {
            this.url = url
            callback()
            //console.log(url)
        });
    }
    toggleEdit() {
        this.editting = !this.editting
    }
    handleChange(e) {
        e.target.blur()
        // let newCellData = {...this.cellData};
        // newCellData.value = e.target.value;
        // this.cellData = newCellData;
        //TODO: add custom event to notify parent cmps about the changes
    }
    handleEnter(e) {
        if (e.which === 13) {
            e.target.blur();
        } else if (e.which === 27) {
            e.target.value = this.cellData.value;
            e.target.blur();
        }
    }
    handleClose(e) {       
        if (e.target && (e.target.value !== null || e.target.value !== undefined) && e.target.value !== this.cellData.value) {
            let newCellData = {...this.cellData};
            
            newCellData.prevValue = this.cellData.value
            newCellData.value = e.target.value;
            this.cellData = newCellData;
            this.dispatchEdit(newCellData); 

            if (this.options) {
                this.options.sort((x,y) => x===e.target.value ? -1 : y === e.target.value ? 1 : 0)
            }           
        }        
        this.toggleEdit()
    }
    handleLookupClose() {
        this.editting = false;
    }
    handleSelected(e) {
        console.log(JSON.parse(JSON.stringify(e.detail)))        
        e = {target: {value: e.detail.lookupValue}, detail: {...e.detail}};
        const lookupId = e.detail.lookupid;
        const objectId = lookupId.substring(1, lookupId.length-1);

        console.log(lookupId)
        if (e.target && (e.target.value !== null || e.target.value !== undefined) && e.target.value !== this.cellData.value) {
            
            let newCellData = JSON.parse(JSON.stringify(this.cellData));
            
            newCellData.prevValue = this.cellData.value
            newCellData.value = e.target.value;

            if (newCellData.navItem) {
                newCellData.navItem.prevId = newCellData.navItem.id;
                newCellData.navItem.id = objectId;
            }
            
            this.cellData = newCellData;            
            this.dispatchEdit(newCellData);         
        }
        this.generateURL(this.cellData.navItem.id,this.cellData.navItem.object,() => {
            this.editting = false;
        })
        
    }
    dispatchEdit(data) {
        if (this.columns && this.columns.length) {
            data.column = this.columns[this.colIndex].field;
        }        
        this.dispatchEvent(new CustomEvent('edit', {detail: data}))
    }
    navTo(e) {
        // View a custom object record.
        // this[NavigationMixin.Navigate]({
        //     type: 'standard__recordPage',
        //     attributes: {
        //         recordId: this.cellData.navItem.id,
        //         objectApiName: this.cellData.navItem.object, // objectApiName is optional
        //         actionName: 'view'
        //     }
        // });

        
        e.preventDefault();
        e.stopPropagation();
        // Navigate to the Account Home page.
        //this[NavigationMixin.Navigate](this.accountHomePageRef);
        console.log(this.cellData.value);
        // this custom event is handled on lwcRefSearch to log data in NewRelic  // S-197083 Added by Rajib Maity
        this.dispatchEvent(new CustomEvent('clickonlink',{ bubbles: true, composed:true, detail: {fieldApiName :this.columns[this.colIndex].field, recordId:this.cellData.navItem.id } }))
        console.log(this.columns[this.colIndex].field);
        console.log(this.cellData.navItem.id);
        setTimeout(() => {
            window.open(this.url)
        }, 1000);
        
    }
}