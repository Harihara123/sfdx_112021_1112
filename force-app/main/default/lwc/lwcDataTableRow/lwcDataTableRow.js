import { LightningElement, api, track } from 'lwc';

export default class LwcDataTableRow extends LightningElement {
    @api rowData;
    @api rowId;
    @api actions;
    @api selection;
    @api columns;
    @api rowSelect;
	@api disableCheckbox=false;
    @track multiSelect;    
    selectedRow = false;
    rowClick = this.handleRowClick.bind(this);    

    connectedCallback() {
        if (this.selection === 'multi') {
            this.multiSelect = true;
        }
        if (this.rowSelect) {
            this.template.addEventListener('click', this.rowClick);
        }
        if (this.rowData.hideAction) {
            const actionToHide = this.rowData.hideAction
            let newActions = JSON.parse(JSON.stringify(this.actions));

            const actionIndex = newActions.findIndex(item => item.action === actionToHide);
            if(actionIndex !== -1) {
                newActions.splice(actionIndex, 1);
                this.actions = newActions;
            }
        }

        if (this.rowData.disableAction) {
		    const actionToDisable = this.rowData.disableAction
			let newActions = JSON.parse(JSON.stringify(this.actions));

            const actionIndex = newActions.findIndex(item => item.action === actionToDisable);
			if(actionIndex !== -1) {
                newActions[actionIndex].disable = true;
                this.actions = newActions;
            }
        }
        if(this.rowData.id){
            this.rowId = this.rowData.id;
        }
    }
    disconnectedCallback() {
        if (this.rowSelect) {
            this.template.removeEventListener('click', this.rowClick);
        }
    }
    handleRowClick(e) {
        //console.log(e.path[0].tagName);
        let dataEdit = e.path[0].getAttribute('data-action');
        if (dataEdit) {
            //console.log('Not selecting; other actions available');
        } else {
            //console.log('Selecting; no other actions on the target');
            this.dispatchRowAction('selectedRow')
        }
    }
    handleAction(e) {
        //console.log(e.target.getAttribute('data-action'), this.rowData.id)
        this.dispatchRowAction(e.target.getAttribute('data-action'))        
        //this.dispatchEvent(new CustomEvent('rowaction', {detail: {rowId: this.rowData.id,action:e.target.getAttribute('data-action'),rowDetails: this.rowData}}))
        //todo: create a custom event that will notify DataTable OR parent cmp of action + rowId
        //      return row data as detail in custom event.
    }
    handleSelection(e) {
        //console.log(e.target.checked);
        this.selectedRow = e.target.checked;
        let params = {
            checked: e.target.checked,
            id: this.rowId
        }
        this.dispatchEvent(new CustomEvent('rowselectionchange', { detail: params}));
 
    }
    dispatchRowAction(action) {
        this.dispatchEvent(new CustomEvent('rowaction', {detail: {rowId: this.rowData.id,action,rowDetails: this.rowData}}))
    }
    @api 
    selectRow(state) {
        this.template.querySelector('input').checked = state;
        this.selectedRow = state;
    }
    @api
    returnRow() {
        if (this.selection === 'multi') {
            if (this.selectedRow) {
                return this.rowData;
            }
            return null
        } 
        if (this.template.querySelector('input').checked) {
            return this.rowData
        }
        return null
    }
    dispatchEdit(e) {
        this.dispatchEvent(new CustomEvent('edit', {detail: {rowId: this.rowData.id, ...e.detail}}))
    }
}