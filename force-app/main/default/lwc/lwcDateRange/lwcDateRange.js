import { LightningElement, api } from 'lwc';

import LANG from '@salesforce/i18n/lang';

export default class LwcDateRange extends LightningElement {
    @api from = this.getRelativeDate(-90);
    @api to = this.getRelativeDate(30);
    returnRange = {};
    initRange;
    clicks = this.handleClicks.bind(this);

    // For combobox POC
    @api dateRangeOptions = [{
        label: "Custom",
        value: "custom"
    }, {
        label: "Today",
        value: "today"
    },{
        label: "This Week",
        value: "this-week"
    },{
        label: "Last Week",
        value: "last-week"
    },{
        label: "Next Week",
        value: "next-week"
    }, {
        label: "Last 4 Weeks",
        value: "last-4"
    }, {
        label: "Last 12 Weeks",
        value: "last-12"
    }];

    label = '';

    // Variables for rendering dates on UI
	get formattedDate() {
		if(this.initDatePreview) {
			return this.formatDate(this.initDatePreview.label, this.initDatePreview.from, this.initDatePreview.to);
		}
		else {
			this.selectDateRange(this.dateRangeOptions, this.rangeFilterValue);
			return this.formatDate(this.label, this.from, this.to);	
		}
	}
    formattedDatePreview;
	enableDatePicker = true;
    @api enableRangeFilter;

    rangeFilterValue;
	@api
	initDatePreview;
	enableDatePreview;

    @api 
	get initRangeFilterValue() {
        return this.rangeFilterValue;    
    }
   set initRangeFilterValue(value){
        this.rangeFilterValue = value;
		this.init();
    }

	connectedCallback() {
		setTimeout(()=>{
			this.init();	
		}, 1200);
	}

	init() {
		if(this.rangeFilterValue){
			this.selectDateRange(this.dateRangeOptions, this.rangeFilterValue);
			if(this.rangeFilterValue === "custom"){
				this.enableDatePreview = false;
				this.enableDatePicker = true;				
			} else {
				this.enableDatePicker = false;
				this.enableDatePreview = true;
				this.formattedDatePreview = this.formatDate('', this.from, this.to);
			}
			
		}
	}

    get getOptions(){
        return this.dateRangeOptions;
    }
    // Handles Combobox
    handleRangeChange(e){
        this.rangeFilterValue = e.detail.value;
		this.selectDateRange(this.dateRangeOptions, this.rangeFilterValue);
		if(this.rangeFilterValue === 'custom') {
			this.enableDatePicker = true;
			this.enableDatePreview = false;
		}
		else {
			this.enableDatePicker = false;
			this.enableDatePreview = true;
			
			this.formattedDatePreview = this.formatDate('', this.from, this.to);
		}
    }

    get enableDatePicker(){
        return this.enableDatePicker;
    }

    getRelativeDate(days) {
		const today = new Date().getTime();
		const ms = today + (days * 24 * 60 * 60 * 1000);
		const isoString = new Date(ms).toISOString();
		return isoString.split('T')[0];        
    }

	getWeekStartDay() { //this will return date of sunday
		let dt = new Date();
		let diff = dt.getDate() - dt.getDay();
		let d= new Date(dt.setDate(diff));
		return d;
	}

	getWeekStartEndDay(num) {
		let wsd = this.getWeekStartDay();
		let wday = new Date(wsd.getFullYear(), wsd.getMonth(), wsd.getDate() + num);
		let isoString = wday.toISOString();
		return isoString.split('T')[0]; 
		//return wday;
	}
    
    selectDateRange(options, dateRange){

        let from,
            to,
            label;
		const selectedDateRange = options.find(item => item.value === dateRange);
		switch (selectedDateRange.value){
            case 'today':
                from = this.getRelativeDate(0);
                to = this.getRelativeDate(0);
                label = selectedDateRange.label;
                break;
            case 'this-week':
				from = this.getWeekStartEndDay(0);
				to = this.getWeekStartEndDay(6);
               label = selectedDateRange.label;
            break;
            case 'last-week':
				from = this.getWeekStartEndDay(-7);
				to = this.getWeekStartEndDay(-1);
                label = selectedDateRange.label;
            break;
            case 'next-week':
				from = this.getWeekStartEndDay(7);
				to = this.getWeekStartEndDay(13);
                label = selectedDateRange.label;
            break;
            case 'last-4' :
				from = this.getWeekStartEndDay(-28);
				to = this.getWeekStartEndDay(-1);
                label = selectedDateRange.label;
            break;
            case 'last-12' :
				from = this.getWeekStartEndDay(-84);
				to = this.getWeekStartEndDay(-1);
                label = selectedDateRange.label;
            break;
            default:
				from = (this.from) ? this.from : this.getRelativeDate(-90);
                to = (this.to) ? this.to : this.getRelativeDate(0);
                label = 'custom';
            break;
        }
		
		this.label = label;
        this.from = from;
        this.to = to;
		
    }
	
    formatDate(label, from, to) {
        const options = {
            day: 'numeric',
            month: 'short',
            year: 'numeric'
        }; 
        const dateTimeFormat = new Intl.DateTimeFormat(LANG, options);
       
        const fromDate = new Date(from).setUTCHours(23),
              toDate = new Date(to).setUTCHours(23);
              
        let formattedDate = `${dateTimeFormat.format(fromDate)} - ${dateTimeFormat.format(toDate)}`;

        if (label !== "custom" && label !== ''){
            formattedDate = `${label} (${formattedDate})`;
        }
        
        return formattedDate;
    }

    toggleCustomFilter(){
       this.rangeFilterValue = 'custom';
       this.selectDateRange(this.dateRangeOptions, this.rangeFilterValue);
       const datePreview = this.template.querySelector('.date-range-preview');
       this.toggleSection(datePreview);
	   this.enableDatePreview = false;
       this.enableDatePicker = true;
    }
	
    // Handles date picker
    handleDateChange(e) {
        const label = this.enableRangeFilter ? this.label : 'custom';
        const {name, value} = e.target;
        let newFormattedDate = this.formatDate(label, this.from, this.to);
        if (value !== this[name]) {
            if (!this.initRange) {
                this.initRange = {to: this.to, from: this.from};
            }
            this[name] = value;

            if (name === "from") {
                newFormattedDate = this.formatDate(label, value, this.to);
            } else {
                newFormattedDate = this.formatDate(label, this.from, value);
            }
        }
        
        //this.formattedDate = newFormattedDate;
    }
	togglePicker() {
		if(this.enableRangeFilter && !this.initDatePreview) {
			this.initDatePreview = {rangeFilterValue:this.rangeFilterValue, from:this.from, to:this.to, label:this.label}
			if(this.rangeFilterValue === 'custom') {
				this.enableDatePicker = true;
				this.enableDatePreview = false;
			}
			else {
				this.enableDatePicker = false;
				this.enableDatePreview = true;
			}
		}
		const picker = this.template.querySelector('.date-range-picker');
        this.toggleSection(picker);
    }

    toggleSection(el) {
       
        el = (el !== null) ? el : this.template.querySelector('.date-range-picker');
        
        if (el.classList.contains('slds-hide')) {
            el.classList.remove('slds-hide');
            document.addEventListener('click', this.clicks)
       } else {            
            document.removeEventListener('click', this.clicks)             
            el.classList.add('slds-hide');
       }
	   
    }
    handleReset() {//Need to maintain to different filter so using two variable initDatePreview and initRange to maintain intial state
		if(this.enableRangeFilter && this.initDatePreview){
			this.rangeFilterValue = this.initDatePreview.rangeFilterValue;
			this.to = this.initDatePreview.to;
            this.from = this.initDatePreview.from;
			this.selectDateRange(this.dateRangeOptions, this.rangeFilterValue);
			this.formattedDatePreview = this.formatDate( '', this.from, this.to);
			
		}
        if (this.initRange) {
			this.to = this.initRange.to
            this.from = this.initRange.from
            this.initRange = null;
            const range = this.template.querySelectorAll('lightning-input');
            range.forEach(date => {
                date.value = this[date.name]
            })
        }
		this.initDatePreview = null;
    }
    handleClicks(e) {
        if (!this.template.contains(e.path[0])) {
            const getEl = this.template.querySelector(e.path[0].classList[0]);
            this.toggleSection(getEl);
            this.handleReset();            
        }
    } 
    handleApply() {
		this.returnRange.from = this.from;
        this.returnRange.to = this.to;
		this.returnRange.rangeFilter = this.rangeFilterValue;
		//this.formattedDate = this.formatDate(this.label, this.from, this.to);

		
		
		this.initRange = null;
        this.dispatchEvent(new CustomEvent('rangechange', {detail: this.returnRange}));
        this.togglePicker();
		this.initDatePreview = null;
    }
    handleCancel() {
        this.togglePicker();
        this.handleReset(); 
    }
}