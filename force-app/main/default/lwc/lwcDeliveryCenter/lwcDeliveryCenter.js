import { LightningElement, api, wire, track } from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getRecordTypeId from '@salesforce/apex/ReqRoutingController.getRecordTypeId';
import addDeliveryCenter from '@salesforce/apex/ReqRoutingController.addDeliveryCenter';

import Support_Request_OBJECT from '@salesforce/schema/Support_Request__c';
import DELIVERYOFFICE_FIELD  from '@salesforce/schema/Support_Request__c.Alternate_Delivery_Office__c';
import CURRENTUSERID from '@salesforce/user/Id';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import Opco_FIELD  from '@salesforce/schema/Opportunity.OpCo__c';


export default class LwcDeliveryCenter extends NavigationMixin(LightningElement) {
    
    @track recordTypeId;
    @track isRequired=true;
    userId = CURRENTUSERID;
    @api reqDetails;
    @api item;

    @track isModalOpen=false;
    @track comments;
    @track deliveryOffice;
    @track numOfPositions;
    @track completedDate;
    @track deliveryOfficePicklistOptions = [];
    @track opportunity_No;
    @track recordId;
    @track oppid;
    @api deliveryInfo = {};
    @track recTypeId;
    @track disableSave = false;
    @track hasDeliveryTeam = false;
    @track hasPositions = false;
    @track hasDate = false;
    @track hasComments = false;
    @track deliveryOfficeOptions = [];
    @track errorOnDelivery = false;
    @track errorOnPositions = false;    
    @track ownerId;
    @track ownerName;
    @track recordPageUrl;
    @track opco;
    
    @track value;
    @track statusMsg;
    @track error;
    rmulDeliveryOfficeOptions=[];
    selectedDC;
    mulDeliveryOfficeOptions;

    /*@wire(getObjectInfo, { objectApiName: Support_Request_OBJECT })
    wiredObjectInfo({error, data}) {
      if (error) {
        console.log('In getobjectinfo error ');
      } else if (data) {
        const rtis = data.recordTypeInfos;
        this.recordTypeId = Object.keys(rtis).find(rti => rtis[rti].name === 'Delivery Center');
      }
    };

	get recordTypeId() {
        // Returns a map of record type Ids       
        if(this.objectInfo) {
            const rtis = this.objectInfo.data.recordTypeInfos;
            return Object.keys(rtis).find(rti => rtis[rti].name === 'Delivery Center');
        }          
    }*/

    @wire(getRecordTypeId,{oppId:'$oppid'})
    getRid({error,data}){
        if(data){
            console.log('Record Id'+data);
            this.recordTypeId = data;
        }
    }
    
    

    personFields = ['User.FirstName','User.LastName'];

    //rough
   constructor(){
       super();    
   }
    
   @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: DELIVERYOFFICE_FIELD })    
   getOfficePicklistValues(result) {
       if (result.data) {
           console.log(JSON.stringify(result.data));
           console.log(result.data.values);
           console.log(result.data.controllerValues);
           console.log(this.opco);
           console.log(this.recordTypeId);            
           let replacedOpco = this.opco;
           if(this.opco !== null && this.opco !== undefined && this.opco !== '--') {
               replacedOpco = this.opco.replace('_', ' ');
           }  
           this.deliveryOfficeOptions = [ { label: '--None--', value: '', selected: true } ];            
           result.data.values.forEach((item) => {
               console.log(item.validFor[0]);
               console.log(this.getKeyByValue(result.data.controllerValues, item.validFor[0]));
               if(this.getKeyByValue(result.data.controllerValues, item.validFor[0]) === replacedOpco) {
                   this.deliveryOfficeOptions.push(item);
               }                
           })
           //this.deliveryOfficeOptions = [ { label: '--None--', value: '', selected: true }, ...result.data.values ];
           for(let i=0;i<this.deliveryOfficeOptions.length ;i++){
            //if you want not to store for any specific delivery center
            if(this.deliveryOfficeOptions[i].label === '--None--' || this.deliveryOfficeOptions[i].value === '')
                continue;
            var doff = {};
            doff.label = this.deliveryOfficeOptions[i].label
            doff.value = this.deliveryOfficeOptions[i].value
            doff.highlight = false
            this.rmulDeliveryOfficeOptions.push(doff)                
            }
            this.mulDeliveryOfficeOptions = [...new Map(this.rmulDeliveryOfficeOptions.map(item => [item['value'], item])).values()]
            

       } else if (result.error) {
           console.log('ERROR');
       }
   };
    
    /*@wire(getRecord, { recordId: '$oppid', fields : [Opco_FIELD] })
    OppOpco;

	get opco() {
        return getFieldValue(this.OppOpco.data, Opco_FIELD);
    }*/

    connectedCallback() {
        
    }

    getKeyByValue(object, value) {
        return Object.keys(object).find(key => object[key] === value);
    }

    @api openModal(flag, reqDetails) {    
        this.isModalOpen = flag;      
        this.recTypeId = this.recordTypeId; 
        this.recordId = reqDetails.opp_num;
        this.oppid=reqDetails.id;
        console.log(this.recordId);
        var name = this.recordId + " | " + reqDetails.account_name + " | " + reqDetails.job_title + " | " + reqDetails.ownerAlias;
        this.opportunity_No = name;
        this.owner = reqDetails.req_owner;        
        var today = new Date();
        this.todayDate = today.getTime();
        this.oppstatus = reqDetails.req_stage;
        this.jobtitle = reqDetails.job_title;
        this.accname = reqDetails.account_name;   
        this.disableSave = true;       
        this.ownerId = reqDetails.ownerId;   
        this.ownerName = reqDetails.ownerName; 
        this.opco = reqDetails.opco;                            
    }

    closeModal() {
        this.isModalOpen = false;
        this.disableSave = true;
        this.errorOnDelivery = false;
        this.errorOnPositions = false;
        this.numOfPositions = "";
        this.deliveryOffice = "";
        this.completedDate = "";
        this.comments = "";
        this.hasDeliveryTeam = false;
        this.hasPositions = false;
        this.hasDate = false;
        this.hasComments = false;
        if(this.template.querySelector('[data-id="delivery"]') && this.template.querySelector('[data-id="delivery"]').classList.contains('slds-has-error')) {
            this.template.querySelector('[data-id="delivery"]').classList.remove('slds-has-error');
        }
        if(this.template.querySelector('[data-id="positions"]') && this.template.querySelector('[data-id="positions"]').classList.contains('slds-has-error')) {
            this.template.querySelector('[data-id="positions"]').classList.remove('slds-has-error');
        }
    }

    redirectToUser() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.ownerId,
                actionName: 'view',
            }            
        }).then(url => {
            this.recordPageUrl = url;
            window.open(this.recordPageUrl);
        });                
    }
    
    handleChange(event){
        if(event.target && event.target !== undefined){
            if(event.target.name === 'num-of-pos') {
                this.numOfPositions = event.target.value;            
                if(this.numOfPositions !== "" && this.numOfPositions !== undefined) {
                    this.hasPositions = true;
                    this.errorOnPositions = false;
                    if(this.template.querySelector('[data-id="num-positions"]')) {
                        this.template.querySelector('[data-id="num-positions"]').classList.add('background-color')
                    }
                    if(this.template.querySelector('[data-id="positions"]') && this.template.querySelector('[data-id="positions"]').classList.contains('slds-has-error')) {
                        this.template.querySelector('[data-id="positions"]').classList.remove('slds-has-error');
                    }
                }     
                else {
                    this.errorOnPositions = true;
                    if(this.template.querySelector('[data-id="positions"]')) {
                        this.template.querySelector('[data-id="positions"]').classList.add('slds-has-error');
                    }
                }       
            }
            if(event.target.name === 'Comments') {
                this.comments = event.target.value;
                this.hasComments = true;
                if(this.template.querySelector('[data-id="comments"]')) {
                    this.template.querySelector('[data-id="comments"]').classList.add('background-color')
                }
            }
            if(event.target.name === 'CompletedDate') {
                this.completedDate = event.target.value;  
                this.hasDate = true;
                if(this.template.querySelector('[data-id="completed-date"]')) {
                    this.template.querySelector('[data-id="completed-date"]').classList.add('background-color')
                }          
            }
            if(event.target.name === 'delivery-office') {
                this.deliveryOffice = event.target.value;
                if(this.deliveryOffice !== "" && this.deliveryOffice !== undefined) {
                    this.hasDeliveryTeam = true;
                    this.errorOnDelivery = false;
                    if(this.template.querySelector('[data-id="delivery-team"]')) {
                        this.template.querySelector('[data-id="delivery-team"]').classList.add('background-color')
                    }
                    if(this.template.querySelector('[data-id="delivery"]') && this.template.querySelector('[data-id="delivery"]').classList.contains('slds-has-error')) {
                        this.template.querySelector('[data-id="delivery"]').classList.remove('slds-has-error');
                    }
                }
                else {
                    this.errorOnDelivery = true;
                    if(this.template.querySelector('[data-id="delivery"]')) {
                        this.template.querySelector('[data-id="delivery"]').classList.add('slds-has-error');
                    }
                }
            }    
        }    
        /*
        if(this.deliveryOffice !== undefined && this.deliveryOffice !== "" && this.numOfPositions !== undefined && this.numOfPositions !== "") {
            this.disableSave = false;
        }
        */
        if(this.selectedDC !== undefined && this.selectedDC !== "" && this.numOfPositions !== undefined && this.numOfPositions !== "") {
            if(this.selectedDC.length > 0) this.disableSave = false;
            else this.disableSave = true;
        }
        if(this.errorOnPositions === true || this.errorOnDelivery === true) {
            this.disableSave = true;
        }

	}

    handleDeliveryCenter(event){
        if(event.detail){            
            this.selectedDC = event.detail

        }

        if(this.selectedDC !== undefined && this.selectedDC !== ""){
            if(this.selectedDC.length > 0) 
                this.errorOnDelivery = false;
            else
                this.errorOnDelivery = true;
        }
        if(this.selectedDC !== undefined && this.selectedDC !== "" && this.numOfPositions !== undefined && this.numOfPositions !== "") {
            if(this.selectedDC.length > 0){
                this.disableSave = false;
                this.errorOnDelivery = false;
            }else{
                this.disableSave = true;
                this.errorOnDelivery = true;
            } 
        }
        if(this.errorOnPositions === true || this.errorOnDelivery === true) {
            this.disableSave = true;
        }

        if(this.selectedDC.length === 0) this.selectedDC = undefined;
    }

    undoDeliveryChange() {
        /*this.deliveryOffice = "";
        this.errorOnDelivery = true;
        this.disableSave = true;
        if(this.template.querySelector('[data-id="delivery"]')) {
            this.template.querySelector('[data-id="delivery"]').classList.add('slds-has-error');
        }
        console.log(this.deliveryOffice);*/
        this.errorOnDelivery = true;
        this.disableSave = true;
        this.selectedDC = this.template.querySelector("c-lwc-req-routing-multiple-delivery-center").undoDeliveryChange();
        this.selectedDC = undefined;
    }

    undoPositionChange() {
        this.numOfPositions = "";
        this.errorOnPositions = true;
        this.disableSave = true;
        if(this.template.querySelector('[data-id="positions"]')) {
            this.template.querySelector('[data-id="positions"]').classList.add('slds-has-error');
        }
        console.log(this.numOfPositions);
        
    }

    undoCommentsChange() {
        this.comments = "";        
    }

    undoDateChange() {
        this.completedDate = "";        
    }

    save(event) {
        /*const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        const allValidCombo = [...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid && allValidCombo) {  */          
        
        this.selectedDC = this.template.querySelector("c-lwc-req-routing-multiple-delivery-center").getSelectedDC();
        this.isModalOpen = false;
        addDeliveryCenter({
            selectedDC: this.selectedDC,
            numOfPositions: this.numOfPositions.toString(),
            oppid: this.oppid,
            opco: this.opco,
            recordTypeId: this.recordTypeId,
            comments: this.comments,
            completedDate: this.completedDate !== undefined ? this.completedDate.toString() : ''
        })
        .then(res=>{
            const evt = new ShowToastEvent({                    
                message: 'Support Request was successfully created',
                variant: 'success',
            });
            this.dispatchEvent(evt);
        })
        .catch(err=>{
            console.log('error!!!!'+JSON.stringify(err))
        })
        
            if(this.template.querySelector('[data-id="delivery"]') && this.template.querySelector('[data-id="delivery"]').classList.contains('slds-has-error')) {
                this.template.querySelector('[data-id="delivery"]').classList.remove('slds-has-error');
            }
            if(this.template.querySelector('[data-id="positions"]') && this.template.querySelector('[data-id="positions"]').classList.contains('slds-has-error')) {
                this.template.querySelector('[data-id="positions"]').classList.remove('slds-has-error');
            }
            this.errorOnDelivery = false;
        this.errorOnPositions = false;
        this.numOfPositions = "";
        this.deliveryOffice = "";
        this.completedDate = "";
        this.comments = "";
        this.hasDeliveryTeam = false;
        this.hasPositions = false;
        this.hasDate = false;
        this.hasComments = false;
    }
}