import { LightningElement, api } from 'lwc';

export default class LwcDrawer extends LightningElement {
    @api side = "right";
    @api size = ''; // max
    drawerState = 'hide'

    @api 
    get showDrawer() {
        return ''
    }
    set showDrawer(state) {
        if (state) {
            this.openDrawer()
        } else {
            this.closeDrawer()
        }
    }

    get getDrawerState() {
        return `backdrop ${this.drawerState}`
    }
    @api
    openDrawer() {
        document.addEventListener('keydown', this.handleESC)
        this.drawerState = 'show'
    }
    @api
    closeDrawer() {
        this.drawerState = 'hide'
        document.removeEventListener('keydown', this.handleESC)
    }
    handleBackdropClick(e) {
        if (e.target === e.currentTarget) {
            this.closeDrawer()
        }
    }
    handleESC = (e) => {        
        if (e.keyCode === 27) {
            this.closeDrawer()            
        }
    }
}