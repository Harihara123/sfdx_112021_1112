import { LightningElement, api } from 'lwc';

export default class LwcDynamicResumePreview extends LightningElement {
    //resumePortal;
    state;
    @api
    get resume() {
        return this.state;
    }
    set resume(res) {
        //console.log(res, this.template.querySelector('[data-view="resume"]'))
        this.state = res;        
    }
    renderedCallback() {
        const resumePortal = this.template.querySelector('[data-view="resume"]');
        if (resumePortal) {
            resumePortal.innerHTML = this.state.resume;
        }
    }
    handleResumeClick(e) {
      
        if (e.target.classList.contains('hlt1') || e.target.classList.contains('htl2')) {
            this.dispatchEvent(new CustomEvent('show', { detail: { clickedTarget: 'resume-modal' } }));
        } 
        
    }
}