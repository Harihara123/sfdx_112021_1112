import { LightningElement, api } from 'lwc';
import rsrc from "@salesforce/resourceUrl/ATS_Empty_State_Images"

export default class LwcEmptyStateImage extends LightningElement {
    imgUrl = rsrc
    images = {
        'No Access': "/noAccess.svg",
        'No Connection': "/noConnection.svg",
        'No Content': "/noContent.svg",
        'No Data': "/noData.svg",
        'No Events': "/noEvents.svg",
        'No Task': "/noTask.svg",
        'Start': "/startImage.svg"
    }
    @api img = '';
    @api headerText;
    @api bodyText;

    get imageUrl() {
        return `${rsrc}/slds-empty-state-images${this.images[this.img]}`;
    }
    get text() {
        return  this.bodyText?true:false
    }
    get title() {
        return this.headerText?true:false
    }

}