import { LightningElement, api, track, wire } from 'lwc';
import messageChannel from '@salesforce/messageChannel/handleDropDowns__c';
import { subscribe,  unsubscribe,  APPLICATION_SCOPE, MessageContext } from 'lightning/messageService';

import getDefaultFilterData from '@salesforce/apex/ReqRoutingController.getDefaultFilterData';

export default class LwcFacetCheckboxFilter extends LightningElement {
    @api facetData;
    @api pillContainerCss;

    searchText = '';
    isEnableFilter = false;
    options = [];
    totalOptions = [];
    value = [''];
    timer = 0;
    inputValue = '';
    searchOptions = [];

    @track focusTracker = -1;

    subscription = null;

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        this.handleSubscribe();
    }

    handleSubscribe() {
        if (!this.subscription) {
            this.subscription = subscribe( this.messageContext, messageChannel, (message) => this.handleMessage(message),  { scope: APPLICATION_SCOPE } );
        }
    }

    handleMessage(message) {
        this.handleClose();
    }

    openList() {
        this.isEnableFilter = true;
        this.focusInput();
    }

    closeList() {
        this.isEnableFilter = false;
        this.options = [];
        this.inputValue = '';
        this.focusTracker = -1;
    }

    handleSearchTextChange(event) {
        this.searchText = event.target.value;

        // timeout to wait for the user to stop typing before searching
        clearTimeout(this.timer);
        this.timer = setTimeout(function(e) {
            this.getFilterData(this.searchText);
        }.bind(this), 500);
    }

    handleFilterChange(event) {   
        this.focusTracker = -1;
        let pillName = event.target.dataset.value;
         if(pillName == 'mainpill' || pillName =='add'){
            this.getFilterData(''); 
        }  
        /*if (!this.isEnableFilter) {
            this.getFilterData(''); 
            this.closeList();
        }
        else {
            this.handleClose();
            this.openList();
        }*/
    }

    handleBlur(event) {
        //console.log('alex-- handleBlur');
    }

    getFilterData(searchText) {
        let context = this;
        let tempJson = this.facetData;
        //console.log('tempJson'+JSON.stringify(tempJson));
        let lowercaseSearchText = searchText.toLowerCase();

        getDefaultFilterData({"filterString": JSON.stringify(tempJson), "query":lowercaseSearchText})
        .then(result => {
            let itemCount = 0;
            let newOptions = [];

            let opts= [];
            let jsonData = JSON.parse(result);
            let parsedJson = context.facetData.incomingFacetData.items.split(".").reduce((o, n) => o[n], jsonData.response);
            
            parsedJson.forEach(function(item) {
                let itemJson = {"label":item.key, "value":item.key};
                opts.push(itemJson);
            })                          
            
            parsedJson.forEach(function(item) {
                let name = item[context.facetData.incomingFacetData.name];
                let count = item[context.facetData.incomingFacetData.count];
                let itemJson = {"label":name, "value":name};
                context.totalOptions.push(itemJson);
                if (!context.facetData.selectedValues.includes(itemJson.value) && itemCount < 10) {
                    //newOptions.push({"label": name + " (" + count + ")", "value":name});
                    newOptions.push(itemJson);
                    itemCount++;
                }
            });

            context.options = newOptions;
            context.searchOptions = opts;
            // Do this after everything executes
            context.isEnableFilter = true;
            context.sendFilterOpenedEvent();
            context.focusInput();
        })
        .catch(error => {
            //console.log('alex-- getFilterData error: ', error);
        });
    }

    focusInput() {
        // Focus on input
        setTimeout(function(){
            let input = this.template.querySelector("[data-name='enter-search']");
            if (input) input.focus();
        }.bind(this), 100);
    }

    handleChange(event) {
        let itemValue = event.target.dataset.value;
        let itemAction = event.target.dataset.action;

        if (itemAction == 'add')
            this.addPill(itemValue);
        else if (itemAction == 'remove')
            this.removePill(itemValue);
    }

    @api
    handleOtherFacetOpened(fieldSlug) {
        if (this.facetData.fieldSlug != fieldSlug)
            this.handleClose();
    }

    handleClose() {
        this.isEnableFilter = false;
        this.options = [];
        this.inputValue = '';
        this.focusTracker = -1;
    }

    addPill(pill) {
        let count = 0;
        let localFacet = JSON.parse(JSON.stringify(this.facetData));
      
        if (!localFacet.selectedValues.includes(pill)) {
            localFacet.selectedValues.push(pill);
            this.facetData = localFacet;
            this.sendSelectedEvent();
        }

        let newOptions = [];
    
        this.searchOptions.forEach(function(item) {
            if (item.value.toLowerCase() != pill.toLowerCase() && !localFacet.selectedValues.includes(item.value) && count < 10 && !newOptions.includes( pill.toLowerCase())) {
                newOptions.push(item);
                count++;
            }
        });
        this.options = newOptions;
    }
    
    removePill(pill) {
        let count = 0;
        let localFacet = JSON.parse(JSON.stringify(this.facetData));

        let newSelectedList = [];
        localFacet.selectedValues.forEach(function(item) {
            if (item != pill)
                newSelectedList.push(item);
        });

        localFacet.selectedValues = newSelectedList;
        this.facetData = localFacet;
        this.sendSelectedEvent();

        let newOptions = [];
    
        this.searchOptions.forEach(function(item) {
            if (!localFacet.selectedValues.includes(item.value) && count < 10 && !newOptions.includes(pill.toLowerCase())) {
                newOptions.push(item);
                count++;
            }
        });
        this.options = newOptions;
        
    }

    sendSelectedEvent() {
        const selectedEvent = new CustomEvent('facetitemselected', { bubbles: true, composed: true, detail: this.facetData });
        this.dispatchEvent(selectedEvent);
    }

    sendFilterOpenedEvent() {
        const selectedEvent = new CustomEvent('facetitemopened', { bubbles: true, composed: true, detail: this.facetData.fieldSlug });
        this.dispatchEvent(selectedEvent);
    }

    handleInputChange(event) {
        let keyPressed = event.code;
        let inputLength = this.options.length;
        this.searchText = event.target.value;
        this.inputValue = event.target.value;

        //If these keys are pressed, don't do anything
        if (keyPressed == 'ArrowLeft' || keyPressed == 'ArrowRight') {}
        
        // Close results
        else if (keyPressed == 'Escape') {
            this.handleClose();
        }

        // Move through options
        else if (keyPressed == 'ArrowUp' || keyPressed == 'ArrowDown') {
            if (inputLength > 0) {
                if (keyPressed == 'ArrowDown') {
                    // Go to beginning of list if selecting past the last option
                    if (this.focusTracker == -1)
                        this.focusTracker = 0;
                    else if (this.focusTracker < inputLength - 1)
                        this.focusTracker++;
                    else
                        this.focusTracker = 0;
                }
                else if (keyPressed == 'ArrowUp') {
                    // Go to end of list if selecting past the first option
                    if (this.focusTracker == -1)
                        this.focusTracker = inputLength - 1;
                    else if (this.focusTracker > 0)
                        this.focusTracker--;
                    else
                        this.focusTracker = inputLength - 1;
                }

                this.updateFocus();
            }
        }

        // Select an option
        else if (keyPressed == 'Enter' || keyPressed == 'Tab') {
            if (this.focusTracker != -1 || this.facetData.allowFreeText) {
                this.addPill(this.searchText);
                this.handleClose();
            }
        }

        // Do lookup
        else {
            // Timeout so we don't flood the system with API requests
            clearTimeout(this.timer);
            this.timer = setTimeout(function(e) {
                this.getFilterData(this.searchText);
            }.bind(this), 500);
        }
    }

    updateFocus() {
        let option = this.options[this.focusTracker].value;
        let titleOption = this.template.querySelector('[data-option=\'' + option + '\']');
        let allOptions = this.template.querySelectorAll('p.selectableItem');

        // Remove focus from all options
        if (allOptions) {
            allOptions.forEach(function(item){
                item.className = 'slds-p-horizontal_small slds-p-vertical_x-small selectableItem';
            });
        }

        // Add focus back to selected option
        if (titleOption) {
            titleOption.className = 'slds-p-horizontal_small slds-p-vertical_x-small selectableItem itemSelected';
            this.inputValue = option;
        }
    }

}