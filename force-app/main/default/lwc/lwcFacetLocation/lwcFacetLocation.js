import { LightningElement, api, track,wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getGoogleLocation from '@salesforce/apex/ReqRoutingController.getGoogleLocation';
import getPlaceDetails from '@salesforce/apex/ReqRoutingController.getPlaceDetails';
import messageChannel from "@salesforce/messageChannel/ReqRoutingMessageChannel__c";
import messageChannelDropDowns from '@salesforce/messageChannel/handleDropDowns__c';
import { publish, subscribe, APPLICATION_SCOPE, MessageContext } from "lightning/messageService";

export default class LwcFacetLocation extends LightningElement {
    @api facetData;

    isEnableFilter = false;

    options = [];
    searchText;
    timer = 0;
    inputValue = '';
    
    myLocation;
    showMyLocation = false;

    @track focusTracker = -1;

    @wire(MessageContext) messageContext;

    connectedCallback() {
        navigator.geolocation.getCurrentPosition(this.navigatorSuccess.bind(this), this.navigatorError.bind(this), {timeout:1000});
        this.handleSubscribe(); //
    }

    handleSubscribe() {
        if (!this.subscription) {
            this.subscription = subscribe( this.messageContext, messageChannelDropDowns, (message) => this.handleMessage(message),  { scope: APPLICATION_SCOPE } );
        }
    }

    handleMessage(message) {
        this.closeList();
    }
    //

    navigatorSuccess(position) {
        this.myLocation = position.coords.latitude + "," + position.coords.longitude;
    }

    navigatorError(positionError) {

    }

    openList() {
        this.isEnableFilter = true;
        this.focusInput();
    }

    closeList() {
        this.isEnableFilter = false;
        this.options = [];
        this.inputValue = '';
        this.focusTracker = -1;
    }

    handleFilterChange(event) {
        let pillName = event.target!=undefined ? event.target.dataset.value :'';
        
        if(pillName == 'mainpill' || pillName =='add'){
            this.sendFilterOpenedEvent();
            this.openList();
        }

      /*  if (this.isEnableFilter) {
            this.closeList();
        }
        else {
            this.openList();
        }

        if (this.isEnableFilter) {
            this.sendFilterOpenedEvent();
        } */
    }

    getLocationData(searchText) {
        let context = this;
        let myLocation = '';

        getGoogleLocation({"input":searchText.toLowerCase(), "myLocation":myLocation})
            .then(result => {
                let tempOptions = [];
                let jsonData = JSON.parse(result);
                jsonData.predictions.forEach(function(item) {
                    let jsonItem = {"label":item.description, "value":item.place_id};
                    tempOptions.push(jsonItem);
                });

                context.options = tempOptions;
            })
            .catch(error => {
                //console.log('alex-- getGoogleLocation error: ', error);
            });
    }

    getPlaceData(val) {
        let context = this;
        
        getPlaceDetails({"placeId":val})
            .then(result => {
                let resultJson = JSON.parse(result); // Data
                
                let localFacet = JSON.parse(JSON.stringify(this.facetData));
                localFacet.selectedValues.forEach(function(item){
                    if (item.value == val) {
                        item.data = resultJson;
                    }
                });
                context.facetData = localFacet;

                context.sendSelectedEvent();
                context.focusInput();
            })
            .catch(error => {
                console.log('error: ', error);
            });
    }

    focusInput() {
        // Focus on input
        setTimeout(function(){
            let input = this.template.querySelector("[data-name='enter-search']");
            if (input) input.focus();
        }.bind(this), 100);
    }

    handleChange(event) {
        let itemLabel = event.target.dataset.label;
        let itemValue = event.target.dataset.value;
        let itemAction = event.target.dataset.action;

        if (itemAction == 'add') {
            this.addPill(itemLabel, itemValue);
        }
        else if (itemAction == 'remove') {
            this.removePill(itemValue);
        }
        
       // this.handleClose();
    }

    @api
    handleOtherFacetOpened(fieldSlug) {
        if (this.facetData.fieldSlug != fieldSlug)
            this.handleClose();
    }

    handleClose(event) {
        this.closeList();
    }

    addPill(pill, placeId) {
        let localFacet = JSON.parse(JSON.stringify(this.facetData));
        let placeIdList = [];
        localFacet.selectedValues.forEach(function(item){
            placeIdList.push(item.value);
        });

        if (!placeIdList.includes(placeId)) {
            localFacet.selectedValues.push({"label":pill, "value":placeId});
        }
        else {
            this.showDupeNotification();
        }
        
        this.facetData = localFacet;
        
        this.getPlaceData(placeId);
    }

    removePill(pill) {
        let localFacet = JSON.parse(JSON.stringify(this.facetData));
        
        let newSelectedList = [];
        this.facetData.selectedValues.forEach(function(item) {
            if (item.value != pill)
                newSelectedList.push(item);
        });

        localFacet.selectedValues = newSelectedList;
        this.facetData = localFacet;
        this.sendSelectedEvent();
    }
    
    showDupeNotification() {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: 'You have already added this location',
            variant: 'error',
        });
        this.dispatchEvent(evt);
    }

    sendSelectedEvent() {
        const selectedEvent = new CustomEvent('facetitemselected', { bubbles: true, composed: true, detail: this.facetData });
        this.dispatchEvent(selectedEvent);
    }

    sendFilterOpenedEvent() {
        const selectedEvent = new CustomEvent('facetitemopened', { bubbles: true, composed: true, detail: this.facetData.fieldSlug });
        this.dispatchEvent(selectedEvent);
    }

    handleInputChange(event) {
        let keyPressed = event.code;
        this.searchText = event.target.value;
        let inputLength = this.options.length;

        // If these keys are pressed, don't do anything
        // This is empty so the timeout at the bottom resets when these keys are pressed 
        if (keyPressed == 'ArrowLeft' || keyPressed == 'ArrowRight') { }
        
        // Close results
        else if (keyPressed == 'Escape') {
            this.options = [];
            this.closeList();
        }

        // Move through options
        else if (keyPressed == 'ArrowUp' || keyPressed == 'ArrowDown') {
            if (inputLength > 0) {
                if (keyPressed == 'ArrowDown') {
                    // Go to beginning of list if selecting past the last option
                    if (this.focusTracker == -1)
                        this.focusTracker = 0;
                    else if (this.focusTracker < inputLength - 1)
                        this.focusTracker++;
                    else
                        this.focusTracker = 0;
                }
                else if (keyPressed == 'ArrowUp') {
                    // Go to end of list if selecting past the first option
                    if (this.focusTracker == -1)
                        this.focusTracker = inputLength - 1;
                    else if (this.focusTracker > 0)
                        this.focusTracker--;
                    else
                        this.focusTracker = inputLength - 1;
                }

                this.updateFocus();
            }
        }

        // Select an option
        else if (keyPressed == 'Enter' || keyPressed == 'Tab') {
            if (this.focusTracker != -1 || this.focusTracker == undefined || this.facetData.allowFreeText) {
                let value = this.options[this.focusTracker].value;
                let label = this.options[this.focusTracker].label;

                this.addPill(label, value);
                this.getPlaceData(value);
                this.closeList();
            }
        }

        // Do lookup
        else {
            // Timeout so we don't flood the system with API requests
            clearTimeout(this.timer);
            this.timer = setTimeout(function(e) {
                this.getLocationData(this.searchText);
            }.bind(this), 500);
        }
    }

    updateFocus() {
        let option = this.options[this.focusTracker].value;
        let label = this.options[this.focusTracker].label;
        let titleOption = this.template.querySelector('[data-option=\'' + option + '\']');
        let allOptions = this.template.querySelectorAll('p.selectableItem');

        // Remove focus from all options
        if (allOptions) {
            allOptions.forEach(function(item){
                item.className = 'slds-p-horizontal_small slds-p-vertical_x-small selectableItem';
            });
        }

        // Add focus back to selected option
        if (titleOption) {
            titleOption.className = 'slds-p-horizontal_small slds-p-vertical_x-small selectableItem itemSelected';
            this.inputValue = label;
        }
    }
}