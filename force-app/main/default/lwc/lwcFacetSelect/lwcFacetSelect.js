import { LightningElement, api,wire } from 'lwc';
import messageChannel from '@salesforce/messageChannel/handleDropDowns__c';
import {publish, MessageContext} from 'lightning/messageService'

import getDefaultFilterData from '@salesforce/apex/ReqRoutingController.getDefaultFilterData';

export default class LwcFacetSelect extends LightningElement {
    @wire(MessageContext)
    messageContext;

    _facetData;
    @api
    get facetData() {
        return this._facetData;
    }
    set facetData(value) {
        this._facetData = value;
        this.setValues();
    }

    @api pillContainerCss;

    selectValue = '';

    isRendered = false;

    connectedCallback() {
        this.setValues();
    }

    renderedCallback() {
        this.isRendered = true;
    }

    setValues() {
        setTimeout(() => {
            while (!this.isRendered) {}
            
            let select = this.template.querySelector('select');
            let selectedValue = this.facetData.selectedValues[0];

            // Set value of select
            if (select && selectedValue)
                select.value = selectedValue;

            // Else set selected to first item
            else if (select && !selectedValue)
                select.selectedIndex = 0;
            
        }, 0);
    }

    @api
    handleOtherFacetOpened(fieldSlug) {
        if (this.facetData.fieldSlug != fieldSlug)
            this.handleClose();
    }

    updateFacetData(event) {
        let localFacet = {...this.facetData};
        localFacet.selectedValues = [event.target.value];
        this.facetData = localFacet;
        this.sendSelectedEvent();
    }

    sendSelectedEvent() {
        const selectedEvent = new CustomEvent('facetitemselected', { bubbles: true, composed: true, detail: this.facetData });
        this.dispatchEvent(selectedEvent);
    }
    handleClick() {
        const payload = { facetData: 'lwcFacetSelect' };
        publish(this.messageContext, messageChannel, payload);
    }
}