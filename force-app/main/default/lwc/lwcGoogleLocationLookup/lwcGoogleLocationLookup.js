import googleAPI from '@salesforce/apex/GooglePlacesController.autocomplete';

import { LightningElement, api } from 'lwc';

const DEFAULT_TYPE_TIMEOUT = 300; //default wait time before querying API once typing has started.

export default class LwcGoogleLocationLookup extends LightningElement {
    timeoutRef
    typeTimeout = DEFAULT_TYPE_TIMEOUT;
    apiResponse;
    @api list
    @api value = ''
    @api placeholder = ''
    @api address = false;
    @api label = '';
    @api inputVariant = 'standard';
    @api required = false;
	@api tooltipText;
    
    handleChange(e) {
        const value = e.detail.value;
        this.value = value;
        this.dispatchEvent(new CustomEvent('change', {detail: {value: this.value}}));
        if (value.length >= 4) {
            this.apiCall()
        }
    }
    formatLocations() {
        return this.apiResponse.map(item => item.description);
    }
    handleSelect(e) {
        const value = e.detail.value;
        const selectedLocation = this.apiResponse.filter(item => item.description === value);
        //console.log(value, selectedLocation);
        if (selectedLocation.length) {
            this.dispatchEvent(new CustomEvent('select', {detail: {value: {...selectedLocation[0]}}}));
        }
    }
    apiCall() {
        if (this.timeoutRef) {
            clearTimeout(this.timeoutRef);
        }
        const apiTimeout = setTimeout(async () => {
            try {
                const response = await googleAPI({input: this.value, myLocation: '', getAddress: this.address?true:false});
                if (response) {
                    const parsedRes = JSON.parse(response);
                    if (parsedRes.status === 'OK') {
                        this.apiResponse = parsedRes.predictions;
                        this.list = this.formatLocations();
                        this.template.querySelector('c-lwc-lookup').showList()                                                
                    }
                }
            } catch (err) {
                console.log(err);
            }
        }, 300);
        this.timeoutRef = apiTimeout;
    }

    @api handleInvalidInput(params){
        if (!params.isValid){
            this.template.querySelector('c-lwc-lookup').setValidity(params.isValid, params.errorMessage);
        }
        
    }

}