import {LightningElement, wire, api} from 'lwc';
import getBoardDetails from '@salesforce/apex/JobPostingFormController.emeaBoardDetail';
import LOCALE from '@salesforce/i18n/locale';

const columns = [
		{ label: 'Posting Name', fieldName: 'postingUrl', type : 'url',  typeAttributes: {label: { fieldName: 'postingName' }, target: '_blank'}, initialWidth:130  },
		{ label: 'Posted Date', fieldName: 'postedDate', type: 'date-local', initialWidth:130 },
		{ label: 'Expiration Date', fieldName: 'expDate', type: 'date-local', initialWidth:135 },
		{ label: 'Posting Status', fieldName: 'postingStatus', type: 'text', initialWidth:130 },
		{ label: 'Posting Owner', fieldName: 'owner', type: 'text' },
		{ label: 'Owner Email', fieldName: 'ownerEmail', type: 'email' },
		{ label: 'Owner Phone', fieldName: 'ownerPhone', type: 'phone',  initialWidth:125 },
		{ label: 'Applications Count', fieldName: 'applCount', type: 'number', initialWidth:160 },
	];

export default class LwcJobPostingBoardDetail extends LightningElement {
	@api boardId;
	@api division;

	boardData=[];
	columns = columns;

	connectedCallback() {
		console.log(this.boardId + '-----' + this.division);
	}

	@wire(getBoardDetails, {boardId : '$boardId', division : '$division'})
	boardDetails({error, data}){
		if(data){
			let tempBoardData = [];

			//this.boardData = data;
			data.forEach(board => {
				let prepBoard = {};
				prepBoard.postingName = board.postingName;
				prepBoard.postingUrl = board.postingUrl
				prepBoard.postedDate = board.postedDate;				
				prepBoard.expDate = board.expDate;
				prepBoard.postingStatus = board.postingStatus;
				prepBoard.owner = board.owner;
				prepBoard.ownerEmail = board.ownerEmail;
				prepBoard.ownerPhone = board.ownerPhone;
				prepBoard.applCount = board.applCount;
				tempBoardData.push(prepBoard);
			});
			this.boardData = tempBoardData;
		}
		else if(error) {
			 console.log('error out --- ');
		}
	}
	
	 @api handleCancelModal(event) {
        const cancelEvent = new CustomEvent('cancleclickevent');
        this.dispatchEvent(cancelEvent);
    }
}