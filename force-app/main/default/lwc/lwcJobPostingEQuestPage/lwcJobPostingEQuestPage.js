import {LightningElement, api} from 'lwc';

export default class LwcJobPostingEQuestPage extends LightningElement {
	@api equestUrl;
	@api isReloaded = false;
	theIframe;
    
	/*set equestUrl(val){
		
	}
    get equestUrl() {
        return `https://joblauncher.testing.equest.com/en/job/712997/boards/fields.app?sid=fhktm0lkg7hba2qpkelekr4be3&uid=14209084716001eb033cff40.52597688`;
    }*/
   

    renderedCallback() {
        console.log('rendred callback called' + this.theIframe);
        if (this.theIframe == undefined) {
            this.theIframe = this.template.querySelector('iframe');
            // window resized to remove double scrollbars
            this.theIframe.height = window.innerHeight - 170;
            
            this.theIframe.onload = () => {
                console.log('Onload called' + this.isReloaded);

                if (!this.isReloaded) {
                    this.isReloaded = true;
                    this.theIframe.src = this.theIframe.src;
                }
            }
        }

    }
}