import {LightningElement, api, wire} from 'lwc';
import insertRecord from '@salesforce/apex/JobPostingFormController.createPostingRecord';
import processEMEAPosting from '@salesforce/apex/JobPostingFormController.processEMEAPosting';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

// import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id'
import USER_NAME_FIELD from '@salesforce/schema/User.Name';
import USER_EMAIL_FIELD from '@salesforce/schema/User.Email';
import USER_PHONE_FIELD from '@salesforce/schema/User.Phone';

// labels and error messages
import { labels,errorMessage } from 'c/lwcJobPostingLabels';
//export default class LwcJobPostingFormPreview extends LightningElement {

export default class LwcJobPostingFormPreview extends NavigationMixin(LightningElement) {
    prevData;
    jobDec;
    // objectInfo;
    userEmail;
    userName;
	userPhone;
    error;
    buttonDisabled = false;
	isBSD;

    label = labels;
	@api spclBoard;
	@api roadtechLabel;
	@api emeaBoards;
	@api
    get jobPosting() {
        return this.prevData;
    }

    set jobPosting(val) {
        this.prevData = val;
    }

	get isEMEA(){
		return this.prevData.opco__c === 'AG_EMEA' ? true : false;			
	}

    get getLocation() {
		if(this.prevData.State__c) {
			 return `${this.prevData.City__c}, ${this.prevData.State__c}, ${this.prevData.Country__c}`;
		}
        else if(this.prevData.City__c){
            return `${this.prevData.City__c}, ${this.prevData.Country__c}`;   
        }
		else {
			return `${this.prevData.Country__c}`;
		}
        
    }

    get getpayRateMin() {
        return this.checkVal(this.prevData.Salary_min__c);
    }
    get getpayRateMax() {
        return this.checkVal(this.prevData.Salary_max__c);
    }
    get getFreq() {
        return this.checkVal(this.prevData.Salary_Frequency__c);
    }
    get getAddlBenefits() {
        return this.checkVal(this.prevData.Salary_Benefits__c);
    }

    get getContactName() {
        return this.checkVal(this.userName);
    }

    get getContactEmail() { 
        return this.checkVal(this.userEmail);
    }

    get getContactPhone() {
        return this.checkVal(this.userPhone);
    }
	get isJSFirm() {
		return this.spclBoard === 'JSFirm';
	}
    get isRoadtechs(){
		return this.spclBoard === 'Roadtechs';
	}

    get getPayRateToggleText(){
        return this.prevData.Pay_Rate_Toggle__c ? 'Yes' : 'No';
    }
    @wire(getRecord, {
        recordId: USER_ID,
        fields: [USER_NAME_FIELD, USER_EMAIL_FIELD, USER_PHONE_FIELD]
    }) wireuser({ error, data }){
        if(error){
            this.error = error;
            console.log(this.error)
        } else if(data){
            this.userEmail = data.fields.Email.value ;
            this.userName = data.fields.Name.value;
            this.userPhone = data.fields.Phone.value;
        }
    }

    renderedCallback(){
          this.jobDec = this.prevData.External_Job_Description__c;
		//check if any selected board is BSD then need to display 'Next' button. So making isBSD true.
		const opco = this.prevData.opco__c;
		if (opco === 'AG_EMEA')  {
			for(let board of this.emeaBoards) {
				if(board.isBSD && board.selected) {
					this.isBSD = true;
					break;
				}
			}
	   }
        const container = this.template.querySelector('.container');
        container.innerHTML = this.jobDec;
		
    }
    checkVal(val) {
        if (val !== '' && val !== undefined && val !== null) {
            return val;
        }
        return '--';
    }
    navBack() {
        this.dispatchEvent(new CustomEvent('back', { detail: {'prevData' : this.prevData} }));
    }
	
    createPosting(){
        if(this.prevData.opco__c === 'AG_EMEA') {
			this.processEMEAPosting();
		}
		else{
			this.buttonDisabled = true;
			console.log();
			insertRecord({'jp': this.prevData}).then(response => {
					this.showToast(response, labels.ATS_JOB_POSTING_SUCCESS_TOAST, 'Success');
					this.navigateToListView();		
				}).catch((err) => {
					console.log(err)
					this.showToast('Error', 'An unexpected error occured, please contact your Admin', 'error');
				});
		}
    }
	processEMEAPosting() {
		this.buttonDisabled = true;
		processEMEAPosting({'jp':this.prevData, 'emeaBoards': this.emeaBoards}).then(response =>{
			console.log('---------------------');
			console.log(response);
			if(response.toLowerCase() === 'success') {
				this.showToast(response, labels.ATS_JOB_POSTING_SUCCESS_TOAST, 'Success');
				this.navigateToListView();
			}
			else {
				let el = response.replaceAll('\\','').replaceAll('"','');
				this.dispatchEvent(new CustomEvent('equestcall', { detail: {'equestUrl' : el} }));
			}
		}).catch((err)=>{
			 	this.showToast('Error',err.body.message , 'error');
			});
	}
	navigateToListView() {
     // Navigate to the Joposting object's Recent list view.
     this[NavigationMixin.Navigate]({
         type: 'standard__objectPage',
         attributes: {
             objectApiName: 'Job_Posting__c',
             actionName: 'list'
         },
         state: {
             // 'filterName' is a property on the page 'state'
             // and identifies the target list view.
             // It may also be an 18 character list view id.
             filterName:'00B1p000005O1nfEAC' // or by 18 char "00BT0000002TONQMA4"
         }
     });
 }
    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}