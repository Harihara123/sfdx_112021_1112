import { LightningElement, api, wire, track } from 'lwc';

export default class LwcJobPostingFormScreeningQuestions extends LightningElement {
    @api questionIncrement;
    @api index;
   /* @api
    get questionIncrement(){
        return this._questionIncrement;

    }
    set questionIncrement(val){
       
        this._questionIncrement=val+1;
        alert('rtyui'+this._questionIncrement);
    }*/
    get options() {
        return [
            { label: 'Yes', value: 'option1' },
            { label: 'No', value: 'option2' },
        ];
    }

    removeQuestion() {
        this.dispatchEvent(new CustomEvent('removequestion', {detail: {"questioncounter": this.index}}));
    }
}