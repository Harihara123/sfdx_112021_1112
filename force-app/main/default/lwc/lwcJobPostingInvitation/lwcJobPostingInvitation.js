import { LightningElement, api } from 'lwc';
import jobPostingInvitations from '@salesforce/apex/JobPostingInvitationController.jobPostingInvitations';
import sendInvitation from '@salesforce/apex/JobPostingInvitationController.sendInvitation'

import {ShowToastEvent} from 'lightning/platformShowToastEvent';

import ATS_POSTING_ID from '@salesforce/label/c.ATS_POSTING_ID';
import ATS_TALENT_NAME from '@salesforce/label/c.ATS_TALENT_NAME';
import ATS_EMAIL_ADDRESS from '@salesforce/label/c.ATS_EMAIL_ADDRESS';
import Recruiter_Name from '@salesforce/label/c.Recruiter_Name';
import ATS_DATE_SENT from '@salesforce/label/c.ATS_DATE_SENT';
import Email_Status from '@salesforce/label/c.Email_Status';
import Num_Emails_Sent from '@salesforce/label/c.Num_Emails_Sent';
import NO_POSTING_SENT from '@salesforce/label/c.NO_POSTING_SENT';
import NOT_OFCCP_POSTING from '@salesforce/label/c.NOT_OFCCP_POSTING';
import OFCCP_INVITE_SUCCESS from '@salesforce/label/c.OFCCP_INVITE_SUCCESS';
import OFCCP_INVITATION_LIMIT from '@salesforce/label/c.OFCCP_INVITATION_LIMIT';
import SUCCESS from '@salesforce/label/c.ATS_SUCCESS';


export default class LwcJobPostingInvitation extends LightningElement {
    @api recordId;
    @api privatePost;

	tableData = [];
	noData = false;
	noDataMessage;

     cols = [ //Define columns for the table and associated field to be used as an identifier.
        {label:ATS_POSTING_ID, field:'postingId'},
        {label:ATS_TALENT_NAME, field:'talentName'},
        {label:ATS_EMAIL_ADDRESS, field:'emailAddress'},
        {label:Recruiter_Name, field:"recruiterName"},
        {label:ATS_DATE_SENT, field:'dateSent'},
        {label:Email_Status, field:'emailStatus'},
        {label:Num_Emails_Sent, field:'#ofEmailsSent'},
		//{label:'Resend email link', field:'Resend email link'}        
    ];
   
    actions = [{action: 'resend', icon: 'utility:send'}] //array of actions for each row
    showSpinner = false;
    privateFlag = false;

    connectedCallback() {
		this.populateTableData();
    }

	populateTableData() {
		this.tableData = [];
		this.showSpinner = true;
		jobPostingInvitations({recordId : this.recordId}).then(response => {
			this.showSpinner = false;
			if(response) {
				this.configureInvitationResult(response);
			}
		}).catch(err => { console.log(err)});		
	}
	configureInvitationResult(response) {
		this.isOFCCP = response.isOFCCP;
		let sourceSysId = response.sourceSysId;
		let disableAction;
		if(response.isPostingActive) {
				disableAction =  'enable_action';
		}
		else {
			disableAction = 'resend';
		}
		//if(this.isOFCCP) {
			if(response.jpiWraprList && response.jpiWraprList.length !== 0) {
				this.builtTableData(response.jpiWraprList, disableAction); //JOb Posting Invitation Result
			}
			else {
				this.noData = true;
				this.noDataMessage = NO_POSTING_SENT;
			}					
		/*}
		else {
			this.noData = true;
			let notOfccpPostMsg =  NOT_OFCCP_POSTING.replace('{0}', sourceSysId)
			this.noDataMessage = notOfccpPostMsg;
		}
		*/
	}
	builtTableData(results, disableAction) {
		let tmpTableData=[];
		results.forEach(result=>{
			let record ={id : result.id,
						 contactEmail : result.contactEmail,
						 contactId : result.contactId,
						 invtCount : result.invtCount,
						 talentName : result.contactName,
						  postingId : result.postingId,
						 disableAction : disableAction,
						cells : [{value : result.postingId, name : 'postingId'},
								 {value : result.contactName, name:'talentName', navItem :{object: 'Contact', id: result.contactId, name:'talentName'}},
								 {value : result.contactEmail, name:'emailAddress'},
								 {value : result.userName, name : "recruiterName"},
								 {value : result.dateSent,  date: true}, 
								 {value : result.emailStatus, name : 'emailStatus'},
								 {value : result.invtCount, name : '#ofEmailsSent'}
								]
						}
			tmpTableData.push(record);
		});

		this.tableData = tmpTableData;
	}

    handleRowAction(e) {
        //handle action for the row. (resend email)    
		 console.log(JSON.stringify(e.detail.rowDetails)); 
		let record =  e.detail.rowDetails;  
		if(record.invtCount < 3) {
			sendInvitation({jobPostingSFId:this.recordId, contactEmailId:record.contactEmail, contactId:record.contactId}).then(response => {
				if(response) {
					this.configureInvitationResult(response);
					this.showToast(SUCCESS, OFCCP_INVITE_SUCCESS , SUCCESS);
				}
			});
		}
		else {
			let limitLabel = OFCCP_INVITATION_LIMIT;
			limitLabel = limitLabel.replace('{0}', record.talentName);
			limitLabel = limitLabel.replace('{1}', record.postingId);
			this.showToast('',limitLabel, 'warning');
		}
    }

	showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}