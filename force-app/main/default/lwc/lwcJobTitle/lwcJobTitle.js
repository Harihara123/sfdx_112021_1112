import { LightningElement, api, wire } from 'lwc';
import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import usrId from '@salesforce/user/Id';
import {getRecord, getFieldValue} from 'lightning/uiRecordApi';
import COMP_NAME_FIELD from '@salesforce/schema/User.CompanyName';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LwcJobTitle extends LightningElement {
	@api isFlow = false;
	@api
	jobTitle;
	jobTitleWhereCondition;
	inputLabel = ATS_JOB_TITLE;
	freeText = true;

	@wire(getRecord, {recordId: usrId, fields:[COMP_NAME_FIELD]})
	wiredRecord ({error, data}) {
		if(data) {
			this.jobTitleWhereCondition = 'TextValue2_CompanyName__c = \'' + data.fields.CompanyName.value + '\'';
		}
	}

	handleJobTitleFreeTextEvent(e) {
		if(e.detail) {
			this.jobTitle = e.detail.freeTextValue;
			if(this.jobTitleLengthValidation(this.jobTitle)) {
				this.dispatchEvent(new CustomEvent('jobtitleselection', { detail: { 'jobTitile': this.jobTitle } }));
			}
		}
	}
	
	handleJobTitleSelectedEvent(event) {
        if (event.detail) {
            var lookObj = event.detail;
            this.jobTitle = lookObj.lookupValue;
			if (this.jobTitleLengthValidation(this.jobTitle)) {
				this.dispatchEvent(new CustomEvent('jobtitleselection', {detail : {'jobTitile':this.jobTitle}} ));
			}
        }
	}
	handleBlankJobTitle(){
		this.dispatchEvent(new CustomEvent('searchstringblank'));
	}

	jobTitleLengthValidation(jobTitle) {
		let isValid = true;

		if(jobTitle.length > 255){
			isValid = false;
			this.showToast('Error', 'Job Title is too long, please use a shorter one.', 'error');
		}
		return isValid;
	}

	showToast(title, message, variant) {
		const evt = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant,
			mode: 'dismissable'
		});
		this.dispatchEvent(evt);
	}
}