import { LightningElement, wire, api } from 'lwc';


export default class LwcJobTitlelookupltng extends LightningElement {
    @api value = ''
    @api whereCond = ''

    handleSearchStringChange(e) {
        this.value = e.detail.searchString;
        this.dispatchEvent(new CustomEvent('jobtitleselected', {detail: {"jobtitle": this.value}}));
        console.log(this.value)

	}

}