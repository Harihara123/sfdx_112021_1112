import { LightningElement, api } from 'lwc';

import { loadScript } from "lightning/platformResourceLoader";
import rsrc from "@salesforce/resourceUrl/References";

export default class LwcJsonToCsv extends LightningElement {
    
    @api formatedData = [
        ['Name', 'Phone', 'Email'],
        ['Andy', '7894563210', 'email@email.com'],
        ['Bobert', '6547893210', 'email@test.com']
    ]
    @api fileName;

    connectedCallback() {
        loadScript(this, rsrc + "/references/js/FileSaver.js")
            .then()
            .catch(err => console.log(err));
    }
    exportCSV() {
        let csvString = ``;
        this.formatedData.forEach((row) => {  
            let rowItem =``;          
            row.forEach((cell, j) => {
                if (j===row.length-1) {
                    rowItem += `${cell}\n`
                } else {
                    rowItem += `${cell},`
                }
            })
            csvString += rowItem;
        })

        let blob = new Blob([csvString], {type: "text/plain"});
        window.saveAs(blob, (this.fileName?this.fileName:'submittals')+'.csv');
    }    
}