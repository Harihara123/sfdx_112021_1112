import { LightningElement, api } from 'lwc';

const MIN_LENGTH = 3;
const DEFAULT_LIST_STATE = 'hide';

export default class LwcLookup extends LightningElement {
    @api minLength = MIN_LENGTH;
    @api value = '';
    @api list;
    @api displayList = [];
    @api placeholder = '';
    @api inputLabel = '';
    @api inputVariant;
    @api required;
	@api tooltipText;
    listState = DEFAULT_LIST_STATE;
    listContainerEl = null;
    renderedInit = false;
    inputIcon = '';
    labelOffset;
    
    renderedCallback() {
        if (!this.renderedInit) {
            this.renderedInit = true;
            this.listContainerEl = this.template.querySelector('.slds-dropdown');
            this.checkIcon();
            this.labelOffset = this.inputVariant === 'label-hidden' ? 'top:0.5rem' : 'top:1.75rem';
        }
    }

    checkIcon() {
        const icon = this.template.querySelector('svg');
        if (icon) {
            this.inputIcon = 'icon';
        }
    }
    
    handleChange(e) {
        this.value = e.target.value;
        this.dispatchEvent(new CustomEvent('change', {detail: {value: e.target.value}})); 

        if (this.list && this.value.length >= this.minLength) {
            const filtered = this.filterList(this.value);
            if (filtered.length) {
                this.displayList = filtered;
                if (this.listState === 'hide') {
                    this.showList()
                }
            }
        }
    }
    handleKeyDown(e) {
        switch(e.keyCode) {
            case 40:
                const firstResult = this.template.querySelector('li.slds-listbox__item');
                if (firstResult) {
                    firstResult.focus()
                }
                break;
            case 9:
                this.hideList()
                break;
            case 27:
                this.hideList()
                break;
            default:
                null
        }
    }   
    handleListItemKeys(e) {
        switch(e.keyCode) {
            case 40:
                this.focusNext(e.currentTarget);
                break;
            case 38:
                this.focusPrev(e.currentTarget);
                break;
            case 27:
                this.hideList()
                break;
            case 32:
                e.currentTarget.click()
                break;
            case 13:
                e.currentTarget.click()
                break;                
            default:
                null
        }
    } 
    focusNext(el) {
        if (el) {
            const nextItem = el.nextElementSibling;
            if (nextItem) {nextItem.focus()}
        }
    }
    focusPrev(el) {
        if (el) {
            const prevItem = el.previousElementSibling;
            if (prevItem) {prevItem.focus()}
        }
    }
    filterList(val) {
        return this.list.map(item => item.toLowerCase()).filter(item => item.includes(val.toLowerCase()))
    }
    checkNode = (e) => {
        const el = e.path[0];
        if (this.listContainerEl && !this.listContainerEl.contains(el)) {
            this.hideList()
        }
    }
    @api showList() {
        this.listState = 'show';
        document.addEventListener('click', this.checkNode);
    }
    @api hideList() {
        this.listState = 'hide';
        this.displayList = [];
        document.removeEventListener('click', this.checkNode);
    }
    selectListItem(value) {
        const valueIndex = this.list.map(i => i.toLowerCase()).indexOf(value.toLowerCase());

        if (valueIndex > -1) {
            this.value = this.list[valueIndex];
            this.dispatchEvent(new CustomEvent('select', {detail: {value: this.value}})); 
        }        
        this.hideList()
    }
    handleListClick(e) {
        const value = e.currentTarget.getAttribute('data-value');
        if (value) {
            this.selectListItem(value)
        }
    }

    @api setValidity(isValid, errorMessage) {
        const lookupInput = this.template.querySelector('lightning-input');
        const message = !isValid ? errorMessage : '';

        lookupInput.setCustomValidity(message);
        lookupInput.reportValidity();
    }

}