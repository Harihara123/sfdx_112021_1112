import { LightningElement, api, track, wire } from 'lwc';
import getDisplayConfig from '@salesforce/apex/SearchController.getInterestSignalsDisplayConfig';


export default class Lwc_MatchIndicatorPopoverES extends LightningElement {
    // Public 

    @api dimensionType;
    @api popoverYPos;
    @api popoverHeight;
    @api pageSize;
    @api insight;
    isConfigReady = false;
    displayConfig;

    // Private 
    dimensionSignalArr;
    @track emptyStateMessages = {
        skills: 'No Matched Skills to Display',
        titles: 'No Matched Job Title to Display',
        interest: 'No Available Interest Signals'
    };

    @wire(getDisplayConfig)
    getDisplayConfig({error, data}) {
        if (data) {
            this.displayConfig = JSON.parse(data);
            this.isConfigReady = true;
        } else if (error) {
            console.log(error);
        }
    }

    get processInsights() {
        let signalArr = [];

        if (this.displayConfig && this.insight.dimensions) {
            this.insight.dimensions.forEach((items) => {
                let dimensionType = this.dimensionType.toLowerCase();
                if (dimensionType.includes(items.dimension)) {

                    if (items.dimension.includes("interest")) {

                        items.signals.forEach((j, index) => {

                            let checkValue = !(j.value !== null || j.contributor);
                            let newSignal = {};
                            let sgl = this.displayConfig[j.signal];
                            if (sgl) {
                                newSignal.signal = sgl.label;
                                newSignal.value = j.signal.includes('pipeline') ? false : j.value !== null ? this.setTimeZone(j.value) : false;
    
                                newSignal.showCheckIcon = checkValue ? false : true;
                                newSignal.id = `${index}-${Math.floor(Math.random() * 1000)}`;
                                newSignal.interestList = true;
    
                                signalArr[sgl.position] = newSignal;
                            }
                        });

                        // const falseCheck = signalArr.filter(item => item.showCheckIcon === false);

                        /*if (falseCheck.length === signalArr.length) {
                            signalArr.length = 0;
                        } else {*/
                            signalArr = signalArr.filter(item => item.showCheckIcon === true);
                        //}
                    } else {

                        items.signals.forEach((j) => {

                            let checkValue = !j.signal.includes('intersect');
                            let signalValue = j.value;

                            // If array is empty, enable empty state message
                            if (signalValue.length > 0 && (!j.signal.includes("match"))) {

                                signalValue.forEach((item, index) => {
                                    let newSignal = {};

                                    newSignal.id = `${index}-${Math.floor(Math.random() * 1000)}`;
                                    newSignal.iconName = checkValue ? 'utility:close' : 'utility:check';
                                    newSignal.iconText = checkValue ? 'False' : 'True';
                                    newSignal.variant = checkValue ? 'error' : 'success';
                                    newSignal.signal = item.term;
                                    newSignal.weight = item.weight;
                                    newSignal.interestList = false;

                                    signalArr.push(newSignal);
                        
                                });

                            }
                        });

                        // Sort array after pushing signals
                        if (signalArr.length > 1) {
                            signalArr.sort((a, b) => b.weight - a.weight);
                        }

                        // Trim array if more than 8
                        if (signalArr.length > 8) {
                            signalArr.splice(8, Math.abs(signalArr.length - 8));
                        }

                    }

                }
            });

        }

        this.dimensionSignalArr = signalArr;
        return signalArr;

    }

    get showEmptyState() {
        if (!(this.dimensionSignalArr.length > 0)) {
            return true;
        }
        return false;
    }

    get emptyStateMessage() {
        let emptyMessage;
        const dimension = this.dimensionType.toLowerCase();

        if (dimension.includes('skills')) {
            emptyMessage = this.emptyStateMessages.skills;
        } else if (dimension.includes('title')) {
            emptyMessage = this.emptyStateMessages.titles;
        } else if (dimension.includes('interest')) {
            emptyMessage = this.emptyStateMessages.interest;
        }

        return emptyMessage;
    }

    setTimeZone(date){
        date = (date[0] === '[' ? date.slice(1, -1) : date);

        let dateArr = date.split('T');

        dateArr.splice(1);
        dateArr.push('00:00:00.000Z');
        let newDate = dateArr.join('T');

        return newDate;
    }
}