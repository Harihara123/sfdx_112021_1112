import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {NavigationMixin} from 'lightning/navigation';
import MergeVMSReq from '@salesforce/apex/CRMEnterpriseReqFunctions.MergeVMSReq';
export default class LwcMergeVMSFinalConfirm extends NavigationMixin(LightningElement) {    
    @api vmsReq;    
    @api selectedRecord;
    success;
    loading = false;
	
    connectedCallback() {
        console.log("In final Confirm Merge");  
        console.log(this.selectedRecord.Id);  
        console.log(this.vmsReq.fields.Id.value);          
    }

    viewRecord(event) {
				
		/*var mymap=new Map();
		mymap.set(this.vmsReq.fields.Id.value, this.selectedRecord.Id);*/
        this.loading = true;
        let mymap = {};
        mymap[this.vmsReq.fields.Id.value] = this.selectedRecord.Id;

		MergeVMSReq({MergeReqIds:mymap})
		.then(result => {
			console.log('***********result**'+result);  
            this.loading = false;
            this.success = result;  
            this.showSuccessMsg();                   
		})
		.catch(error => {
			console.log('********error*****'+error);  
            this.loading = false;
            this.success = error; 
            this.closeModal();           
		});                             
    }

    redirectToOpportunityPage() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.selectedRecord.Id,
                objectApiName: 'Opportunity',
                actionName: 'view'
            },
        })
        .then(url => {
            window.open(url);
        });         
    }
    
    showOpportunitySummary() {
        console.log(this.selectedRecord);
        let customEvent = new CustomEvent('summary');
        this.dispatchEvent(customEvent);        
    }

    closeModal() {
        const closeQA = new CustomEvent('close');        
        this.dispatchEvent(closeQA);
    } 

    showSuccessMsg() {
        if(this.success === true) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.selectedRecord.Id,
                    objectApiName: 'Opportunity',
                    actionName: 'view'
                },
            });      
            const evt = new ShowToastEvent({            
                message: 'VMS Req successfully merged with ' + this.selectedRecord.Opportunity_Num__c,
                variant: 'success',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
        this.closeModal();
    }
    
}