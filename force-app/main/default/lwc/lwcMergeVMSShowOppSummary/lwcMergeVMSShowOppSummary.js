import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getOppSummary from '@salesforce/apex/CRMEnterpriseReqFunctions.getOppSummary';

export default class LwcMergeVMSShowOppSummary extends NavigationMixin(LightningElement) {
    @api selectedRecordId;
    @api vmsReq;
    selectedRecord;
    error;
    pills = [];
    showRemote = false;
    showDuration = false;
    showMaxBill = false;
    showMaxPay = false;

    @wire(getOppSummary, { recordId: '$selectedRecordId' })
    getSummaryReq({error, data}) {
        if(data) {
            this.selectedRecord = data;
            console.log(this.selectedRecord.EnterpriseReqSkills__c);
            JSON.parse(this.selectedRecord.EnterpriseReqSkills__c).forEach((item) => {
                console.log(item+","+item.name);
                this.pills.push(item.name);
            });
            if(this.selectedRecord.Req_Work_Remote__c == 'Yes') {
                this.showRemote = true;
            }
            if(this.selectedRecord.Req_Duration__c != null && this.selectedRecord.Req_Duration_Unit__c != null) {
                this.showDuration = true;
            }
            if(this.selectedRecord.Req_Bill_Rate_Max__c != null) {
                this.showMaxBill = true;
            }
            if(this.selectedRecord.Req_Pay_Rate_Max__c != null) {
                this.showMaxPay = true;
            }            
            this.error = null;
        } else if(error) {
            this.selectedRecord = null;
            this.error = error;
        }
    }

    connectedCallback() {
        console.log(this.selectedRecordId);
        console.log(this.vmsReq);
        this.vmsReq = JSON.parse(JSON.stringify(this.vmsReq));
        console.log(this.selectedRecord);
    }

    navigateToOpportunityPage() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.selectedRecord.Id,
                objectApiName: 'Opportunity',
                actionName: 'view'
            },
        })
        .then(url => {
            window.open(url);
        });         
    }

    navigateToUserPage() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.selectedRecord.OwnerId,
                objectApiName: 'User',
                actionName: 'view'
            },
        })
        .then(url => {
            window.open(url);
        });        
    }

    showSelectOpportunity() {
        console.log(this.selectedRecord);
        let customEvent = new CustomEvent('select');
        this.dispatchEvent(customEvent);        
    }
    
    mergeWithOpportunity() {
        console.log(this.selectedRecord);
        let customEvent = new CustomEvent('merge', {detail:this.selectedRecord});
        this.dispatchEvent(customEvent);
    }

    closeModal() {
        let customEvent = new CustomEvent('close');
        this.dispatchEvent(customEvent);
    }
}