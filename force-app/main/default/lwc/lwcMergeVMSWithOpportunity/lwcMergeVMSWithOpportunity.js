import { LightningElement, wire, api } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import OPEN_POSITIONS from '@salesforce/schema/Opportunity.Req_Open_Positions__c';
import TOTAL_POSITIONS from '@salesforce/schema/Opportunity.Req_Total_Positions__c';
import ID from '@salesforce/schema/Opportunity.Id';
//import getRecentOpps from '@salesforce/apex/CRMEnterpriseReqFunctions.getRecentOpps';
import getOppsOnSearch from '@salesforce/apex/CRMEnterpriseReqFunctions.getOppsOnSearch';

const columns = [
    { label: 'Opportunity Name', fieldName: 'nameUrl', type: 'url', wrapText: false, typeAttributes: { label: { fieldName: "name" }, tooltip:{ fieldName: "name" }, target: "_blank", sortable: true }, hideDefaultActions: true, sortable: true },
    { label: 'Account Name', fieldName: 'accountId', type: 'url', wrapText: false, typeAttributes: { label: { fieldName: "account" }, tooltip:{ fieldName: "account" }, target: "_blank", sortable: true }, hideDefaultActions: true, sortable: true },
    { label: 'Owner', fieldName: 'ownerId', type: 'url', wrapText: false, typeAttributes: { label: { fieldName: "owner" }, tooltip:{ fieldName: "owner" }, target: "_blank", sortable: true }, hideDefaultActions: true, sortable: true },
    { label: 'Created Date', fieldName: 'createdDate', wrapText: false, hideDefaultActions: true, sortable: true },
    { label: 'Stage', fieldName: 'stageName', wrapText: false, hideDefaultActions: true, sortable: true },    
];

export default class LwcMergeVMSWithOpportunity extends LightningElement {
    @api recordId;    
    selectedRecord; 
    recentData;
    searchData;
    error;
    columns = columns;
    selectedRecordId;
    vmsReq;
    hasRecentRecords = false;
    hasSearchResults = false;
    showRecentOpps = false;
    showOppSummary = false;    
    showMergeOpp = false;
    sortBy;
    sortDirection;
    queryTerm;
    recentRecords = [];
    searchRecords = [];
    //oppName;
    showSummaryButton = false;

    @wire(getRecord, { recordId: '$recordId', fields: [ID, OPEN_POSITIONS, TOTAL_POSITIONS] })
    getVMSReq({error, data}) {
        if(error) {
            this.error = error;
            this.vmsReq = undefined;
        } else if(data) {
            this.error = undefined;
            this.vmsReq = data;
            console.log(this.vmsReq);
        }
    }

    connectedCallback() {
        this.showSelectOpportunity();        
    }

    @wire(getOppsOnSearch, {searchText : ''})
    wiredRecentOpps({ error, data }) {
        if (data) {
            this.recentData = data;
            this.error = undefined;
            this.hasRecentRecords = true;
            let divblock = this.template.querySelector('[data-id="recent-opps"]');
            if(divblock){
                this.template.querySelector('[data-id="recent-opps"]').classList.add('set-height');
            }            
        } else if (error) {
            this.error = error;
            this.recentData = undefined;
            this.hasRecentRecords = false;
            let divblock = this.template.querySelector('[data-id="recent-opps"]');
            if(divblock && this.template.querySelector('[data-id="recent-opps"]').classList.contains('set-height')){
                this.template.querySelector('[data-id="recent-opps"]').classList.remove('set-height');
            } 
        }
    }

    getRecentRecord(event) {
        if(this.selectedRowId != null && this.selectedRowId != undefined) {
            this.searchRecords = [];            
        }        
        let selectedRow=event.detail.selectedRows; 
        this.selectedRowId = selectedRow[0].oppId;
        this.showSummaryButton = true;
        this.oppName = selectedRow[0].oppName;
    }

    getSelectedRecord(event) {
        if(this.selectedRowId != null && this.selectedRowId != undefined) {
            this.recentRecords = [];
        }
        let selectedRow=event.detail.selectedRows; 
        this.selectedRowId = selectedRow[0].oppId;
        this.showSummaryButton = true;
        this.oppName = selectedRow[0].oppName;
    }

    handleRecentSortdata(event) {  
        let sortbyField = event.detail.fieldName;
        if (sortbyField === "nameUrl") {
            this.sortBy = "name";        
        } else if (sortbyField === "accountId") {
            this.sortBy = "account";
        } else if (sortbyField === "ownerId") {
            this.sortBy = "owner";
        } else {
            this.sortBy = sortbyField;
        }              
        this.sortDirection = event.detail.sortDirection;
        this.recentData = this.sortData(this.recentData, this.sortBy, event.detail.sortDirection);
        this.sortBy = sortbyField;
    }

    handleSearchSortdata(event) {  
        let sortbyField = event.detail.fieldName;
        if (sortbyField === "nameUrl") {
            this.sortBy = "name";        
        } else if (sortbyField === "accountId") {
            this.sortBy = "account";
        } else if (sortbyField === "ownerId") {
            this.sortBy = "owner";
        } else {
            this.sortBy = sortbyField;
        }              
        this.sortDirection = event.detail.sortDirection;
        this.searchData = this.sortData(this.searchData, this.sortBy, event.detail.sortDirection);
        this.sortBy = sortbyField;
    }

    sortData(dataToSort, fieldName, sortDirection){
        let data =  [...dataToSort];        
        let key =(a) => a[fieldName];         
        let reverse = sortDirection === 'asc' ? 1: -1;
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';            
            let vale = (valueA > valueB) - (valueB > valueA);            
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });
        return data;
    }

    handleKeyChange(event) {
        console.log(event.keyCode);   
        this.queryTerm = event.target.value;     
        if (event.keyCode === 13 && this.queryTerm.length >= 5) {                                   
            getOppsOnSearch({ searchText: this.queryTerm })
            .then((result) => {
                console.log('result:'+JSON.stringify(result));
                if(result !== null && result.length != 0) {                    
                    this.searchData=result;                    
                    this.hasSearchResults = true;                    
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock){
                        this.template.querySelector('[data-id="search-table"]').classList.add('set-height');
                    }                        
                } else {
                    this.hasSearchResults = false;
                    this.searchData = [];
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                        this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
                    }
                }                
            })
        } else if(this.queryTerm.length < 5) {
            this.hasSearchResults = false;
            this.searchData = [];
            var divblock = this.template.querySelector('[data-id="search-table"]');
            if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
            }
        }        
    }

    closeModal() {
        const closeQA = new CustomEvent('close');        
        this.dispatchEvent(closeQA);
    }

    showSelectOpportunity() {
        this.showRecentOpps = true;
        this.showOppSummary = false;
        this.showMergeOpp = false;        
        if(this.template.querySelector('[data-id="recent-opps"]')){
            this.template.querySelector('[data-id="recent-opps"]').classList.add('set-height');
        }
        this.hasSearchResults = false;
        this.searchData = [];        
        if(this.template.querySelector('[data-id="search-table"]') && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
            this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
        }
        this.showSummaryButton = false;
    }

    showOppSummaryModal() {
        this.showRecentOpps = false;
        this.showOppSummary = true;
        this.showMergeOpp = false;
        console.log(this.showRecentOpps);
        console.log(this.showOppSummary);
    }

    showMergeOpportunityModal(event) {
        this.showRecentOpps = false;
        this.showOppSummary = false;
        this.showMergeOpp = true;
        this.selectedRecord = event.detail;
    }
}