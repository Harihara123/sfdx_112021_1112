import {LightningElement, track, api} from 'lwc';

export default class LwcMultiSelectPopover extends LightningElement {

	@track value = ['option1'];
	@api filterOptions;
	@api selectedValue;
	@track enableFilter;
	@api filterType;
	 
	connectedCallback() {
		//this.enableFilter = true;
		console.log('this.filterOptions:'+JSON.stringify(this.filterOptions));	
	}

    /*get options() {
        return [
            { label: 'Attempted Contact', value: 'Attempted Contact' },
            { label: 'BD Call', value: 'BD Call' },
			{ label: 'Call', value: 'Call' },
			{ label: 'Email', value: 'Email' },
			{ label: 'G2', value: 'G2' },
			{ label: 'To Do', value: 'To Do' },
			{ label: 'Performance Feedback', value: 'Performance Feedback' },
			{ label: 'Reference Check', value: 'Reference Check' },
			{ label: 'Service Touchpoint', value: 'Service Touchpoint' },
			{ label: 'Other', value: 'Other' }
        ];
    }*/

    get selectedValues() {
        return this.value.join(',');
    }

    handleChange(e) {
        this.value = e.detail.value;
    }
	
	handleSelection(event) {
		let elementValue = event.target.value;
		let isElement = event.target.checked;
		if(!isElement) { // Uncheck Parent checkbox if any checkbox is deselected
			this.filterOptions.forEach((options)=> {
				let parentElements = this.template.querySelectorAll("input[name='selectall']");
				options.values.forEach(function(option) {
					if(elementValue === option.value) {
						parentElements.forEach(function(element) {
							if(element.value === options.type) {
								element.checked = false;
							}
						});
					}
				});
			});

		} else {

			let elements = this.template.querySelectorAll("input[name='options']");
			let selectedElements = [];

			elements.forEach(function(element) {
				if(element.checked) {
					selectedElements.push(element.value);		
				} 

			});

			this.filterOptions.forEach((options)=> {
				let parentElements = this.template.querySelectorAll("input[name='selectall']");				
				let counter = 0;				
				
				options.values.forEach(function(option) { 
					if(selectedElements.includes(option.value)) {
						counter++;
					}
				});
				if(counter == options.values.length) {
					parentElements.forEach(function(element) {
						if(element.value === options.type) {
							element.checked = true;
						}
					});					
				}
			

			});
			
			
		}		
	}

	@api
	handleSelectClick(){
		let elements = this.template.querySelectorAll("input[name='options']");
		let selectedFilters = [];
		let selectedElements = [];
		elements.forEach(function(element) {
			if(element.checked) {
				selectedElements.push(element.value);				
			} 

		});
		console.log('selectedElements:'+JSON.stringify(selectedElements));
		/*this.filterOptions.forEach((options)=> {
			let filterData = {}, filterOptions = [];
			filterData.type = options.type;
			
			
			options.values.forEach(function(option) { 
				let filterOption={};
				filterOption.label = option.label;
				filterOption.value = option.value;
				if(selectedElements.includes(option.value)) {
					filterOption.selected = true;
				} else
					filterOption.selected = false;
				filterOptions.push(filterOption);
			});
			filterData.options = filterOptions;
			selectedFilters.push(filterData);
		});*/
		this.dispatchEvent(new CustomEvent('filterselect', {detail: {'selectedValue':selectedElements,filterType:this.filterType}}));		
		this.filterOptions = [];
		selectedElements=[];
	}
	@api
	handleClick(){
		this.dispatchEvent(new CustomEvent('filterselect', {detail: {'selectedValue':'',filterType:''}}));
		this.filterOptions = [];
	}

	handleAllSelection(event) {
		let filterType = event.target.value;
		let isChecked = event.target.checked;
		let elements = this.template.querySelectorAll("input[name='options']");
		let selectedFilters = this.selectedValue;
		this.filterOptions.forEach(function(options){
			if(filterType === options.type) {
				options.values.forEach(function(option) {
					elements.forEach(function(element) {
						if(element.value === option.value ) {
							if(isChecked) {
								element.checked = true;								
							} else {
								element.checked = false;
							}
						}

					});

				});

			}
			
		});
			
		this.selectedValue = selectedFilters;
	}

}