import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getOppRecord from '@salesforce/apex/SearchController.getOpptyRecord';
import URL from '@salesforce/label/c.CONNECTED_Summer18URL';

export default class LwcOppMatchTalent extends NavigationMixin(LightningElement) {
    @api recordId;
    tracker

    connectedCallback() {
        //req-talent_match_button--006e000000LhstAAAR
        this.tracker = `req-talent_match_button--${this.recordId}`;
    }
    

    async getOpp () {
        try {
            const Opp = await getOppRecord({recordId: this.recordId});            
            this.generateUrl(Opp)
        } catch(err) {
            console.log(err)
        }
    }
    handleClick(e) {
        this.getOpp()
    }
    generateUrl(Opp) {
        const data = Opp[0];
        const location = this.normalizedLocation(
            data.Req_Worksite_City__c, 
            data.Req_Worksite_State__c,
            data.Req_Worksite_Country__c
        )
        const url = URL
        + "/n/ATS_CandidateSearchOneColumn?c__matchJobId=" + data.Id 
        + "&c__srcName=" + encodeURIComponent(data.Name)
        + "&c__srcLoc=" + location
        + "&c__noPush=true&c__srcTitle="+data.Req_Job_Title__c
        + "&c__srcLat="+data.Req_GeoLocation__Latitude__s+"&c__srcLong="+data.Req_GeoLocation__Longitude__s

        this.navUrl(url)

    }
    navUrl(url,callback = () => {}) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url,
                actionName: 'view'
            }
        }        
      );
    }
    normalizedLocation(city, state, country) {
        if (city && city.length) {
            city.trim();
        }
        if (state && state.length) {
            state.trim();
        }
        if (country && country.length) {
            country.trim();
        }        
        let arr = [city, state, country];
        let validLocations = []
        for (let i = 0; i < arr.length; i++) {
            if (arr[i]) {
            validLocations.push(arr[i])
            }
        }
        let location = validLocations.join(", ");
  
  	    return location;
    }
}