/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import ATS_SHOW from '@salesforce/label/c.ATS_SHOW';
import ATS_ROWS_PER_PAGE from '@salesforce/label/c.ATS_ROWS_PER_PAGE';
import ATS_TO from '@salesforce/label/c.ATS_TO';
import ATS_OF from '@salesforce/label/c.ATS_OF';
import ATS_SHOWING_ROWS from '@salesforce/label/c.ATS_SHOWING_ROWS';
import ATS_FIRST_PAGE from '@salesforce/label/c.ATS_FIRST_PAGE';
import ATS_LAST_PAGE from '@salesforce/label/c.ATS_LAST_PAGE';
import ATS_NEXT_PAGE from '@salesforce/label/c.ATS_NEXT_PAGE';
import ATS_PREVIOUS_PAGE from '@salesforce/label/c.ATS_PREVIOUS_PAGE';

export default class LwcPaginator extends LightningElement {
    @track paginationData = [];
    ranges = [5, 10, 15, 25, 50]
    @api defaultRange = 5;    
    @track state = {
        totalPages: 0,
        currentPage: 1,
        rowTotal: 0,
        rowRange: 10,
        rowStart: 1,
        rowEnd: 3,
        remainder: 0,
        buttons: [],
        navBtnStates: {
            first: false,
            previous: false,
            next: false,
            last: false
        }
    }
    
    label = {
        ATS_SHOW,
        ATS_ROWS_PER_PAGE,
        ATS_TO,
        ATS_OF,
        ATS_SHOWING_ROWS,
        ATS_FIRST_PAGE,
        ATS_LAST_PAGE,
        ATS_NEXT_PAGE,
        ATS_PREVIOUS_PAGE
    };
    
    @api 
    get pageData() {
        return this.paginationData;
    }
    set pageData(data) {
        //console.log(data)
        if (data || data.length) {
            this.paginationData = data;
            setTimeout(() => {this.initRange(this.defaultRange?this.defaultRange:this.ranges[0])},0)            
            
        }
    }
    renderedCallback() {
            this.init = true;
            this.template.querySelectorAll('option').forEach(o => {                
                if (parseInt(o.value) === this.defaultRange) {
                    o.selected = true;                    
                }
            })
    }
    connectedCallback() {
        // let i = 1;
        // while (i <= 80) {
        //     this.paginationData.push(i)
        //     i++
        // }
        
        // let totalPages = Math.ceil(this.data.length / this.state.rowRange);
        // let newState = {...this.state}
        // newState.totalPages = totalPages; 
        // let j =1;
        // let stopAt;
        // if (totalPages > 3) {stopAt = 3} else {stopAt = totalPages}
        // while (j <= stopAt) {
        //     this.state.buttons.push({value: j, disabled: newState.currentPage === j})
        //     j++
        // }

        // newState.rowTotal = this.data.length;
        // newState.rowEnd = this.data.length < newState.rowRange? this.data.length : newState.rowRange;
        // this.state = newState;
        
        // this.returnData(1)
        // if (!this.defaultRange) {this.defaultRange = this.ranges[0]}
        // this.initRange(this.defaultRange);
    }
    returnData(page) {
        const {rowRange, totalPages, rowTotal} = this.state;
        const displayData = this.paginationData.slice((page*rowRange)-rowRange, page >= totalPages ? rowTotal : page*rowRange);
        //console.log(displayData);
        this.handleNavBtnStates(page);
        this.dispatchEvent(new CustomEvent('pagination', {detail: [...displayData]}));
    }
    initRange(newRange) {
        this.defaultRange = parseInt(newRange);
        let newState = {...this.state}
        newState.rowRange = newRange;
        newState.buttons = [];
         
        let totalPages = Math.ceil(this.paginationData.length / newState.rowRange);
        if (totalPages === this.state.totalPages) {
            //console.log('same pages')
        } else {
            //console.log('diff pages')
            newState.currentPage = 1;
            newState.rowStart = 1;
        }
        newState.totalPages = totalPages; 
        let j =1;
        let stopAt;
        if (totalPages > 3) {stopAt = 3} else {stopAt = totalPages}
        while (j <= stopAt) {
            newState.buttons.push({value: j, disabled: newState.currentPage === j})
            j++
        }        
        newState.rowTotal = this.paginationData.length;
        
        if (newState.currentPage === 1) {
            newState.rowEnd = this.paginationData.length < newState.rowRange? this.paginationData.length : newState.rowRange;
        }
        if (newState.totalPages === newState.currentPage) {
            newState.rowEnd = newState.rowTotal
        }
        if (newState.currentPage !== 1 && newState.totalPages !== newState.currentPage) {
            newState.rowEnd = ((newState.currentPage)*newState.rowRange)
        }
        
        this.state = newState;
        //console.log(this.state);
        this.setPageTo(this.state.currentPage)
        this.returnData(this.state.currentPage)
    }
    changeRange(e) {
        //console.log(e.target.value);
        let newRange = e.target.value;
        this.initRange(newRange);
        
    }
    next() {
        const {currentPage, totalPages} = this.state
        this.setPageTo(currentPage === totalPages?totalPages:currentPage+1)
    }
    prev() {
        const {currentPage} = this.state
        this.setPageTo(currentPage === 1?1:currentPage-1)
    }
    first() {
        this.setPageTo(1);
    }
    last() {
        this.setPageTo(this.state.totalPages)
    }
    setCurrentButton(buttons, current, totalPages) { 
        let newButtons = [];

        if (current === 1 || totalPages === current) {
            if(current === 1) {
                //console.log('first page');
                buttons.forEach((btn, i) => {
                    if (i===0) {
                        newButtons.push({value: current, disabled: true})
                    } else {
                        newButtons.push({value: i+1, disabled: false})
                    }
                })
            } else {
                //console.log('last page')
                buttons.forEach((btn, i) => {
                    if (i===buttons.length-1) {
                        newButtons.push({value: current, disabled: true})
                    } else {
                        if (i===buttons.length-2) {
                            newButtons.push({value: current-1, disabled: false})
                        } else {
                            newButtons.push({value: current-2, disabled: false})
                        }
                        
                    }
                })
            }
            
        } else {
            //console.log('in-between page')
            buttons.forEach((btn, i) => {
                if (i===buttons.length-2) {
                    newButtons.push({value: current, disabled: true})
                }
                if (i===buttons.length-1) {
                    newButtons.push({value: current+1, disabled: false})
                }
                if (i===buttons.length-3) {
                    newButtons.push({value: current-1, disabled: false})
                }
                
            })
        }

        //console.log(newButtons);
        return newButtons;
    }

    handleClick(e) {
        const page = e.target.getAttribute('data-value');
        //console.log(page)
        this.setPageTo(page);
    }

    setPageTo(page) {
        page = parseInt(page);                
        let newState = {...this.state}
        if (!(page === this.state.currentPage)) {        
            if (page <= 1 || page >= this.state.totalPages) {
                if (page <= 1) {
                    //first page
                    this.returnData(1)
                    newState.currentPage = 1;
                    newState.rowStart = 1;
                    newState.rowEnd = this.paginationData.length < newState.rowRange? this.paginationData.length : newState.rowRange;
  
                } else {
                    //last page
                    this.returnData(newState.totalPages)
                    newState.currentPage = newState.totalPages
                    newState.rowStart = ((newState.totalPages*newState.rowRange)-newState.rowRange)+1;
                    newState.rowEnd = newState.rowTotal
                }
            } else {
                //all other pages
                this.returnData(page)
                newState.currentPage = page;
                newState.rowStart = (((page)*newState.rowRange)-newState.rowRange)+1;
                newState.rowEnd = ((page)*newState.rowRange)
            }
    
            newState.buttons = this.setCurrentButton(newState.buttons, page, newState.totalPages);
            this.state = newState;            
        }        
    }
    handleNavBtnStates(page) {
        //console.log('checking page: ', page)
        if (this.state.totalPages === 1) {
            //console.log('only one page; disable all nav btns')
            let newState = {...this.state}
            newState.navBtnStates.first = true;
            newState.navBtnStates.previous = true;
            newState.navBtnStates.next = true;
            newState.navBtnStates.last = true;
            this.state = newState;
        } else {
            if (page !== 1 && page !== this.state.totalPages) {
                //console.log('in between btn; no need to disable')
                let newState = {...this.state}
                newState.navBtnStates.first = false;
                newState.navBtnStates.previous = false;
                newState.navBtnStates.next = false;
                newState.navBtnStates.last = false;
                this.state = newState;
            } else {
                if (page === 1) {
                    //console.log('first page; disable prev and first btns')
                    let newState = {...this.state}
                    newState.navBtnStates.first = true;
                    newState.navBtnStates.previous = true;
                    newState.navBtnStates.next = false;
                    newState.navBtnStates.last = false;
                    this.state = newState;
                }
                if (page === this.state.totalPages) {
                    //console.log('last page; disable next and last btns')
                    let newState = {...this.state}
                    newState.navBtnStates.first = false;
                    newState.navBtnStates.previous = false;
                    newState.navBtnStates.next = true;
                    newState.navBtnStates.last = true;
                    this.state = newState;
                }
            }
            
        }
    }
}