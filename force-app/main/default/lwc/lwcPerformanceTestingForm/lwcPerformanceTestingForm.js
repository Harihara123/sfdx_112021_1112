import { LightningElement, api } from 'lwc';

export default class LwcCustomTextArea extends LightningElement {
    @api fName;
    @api lName;
    @api submitted;

    onClear = () => {
        this.fName = "";
        this.lName = "";
    }

    onSubmit = () => {
        this.submitted = true;
    }

    onReset = () => {
        this.submitted = false;
    }
}