import { LightningElement, api, wire, track } from 'lwc';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

export default class LwcPerson extends LightningElement {
    @api userId; // User Id to display
    @api clickable = false; // If the link can be clicked, it will open in new window
    @api usePopover = false; // Show a popover dialog or not
    @api fields; // Fields to display for link
    @api popoverFields; // Fields to display for popover
    @api seperator; // Character(s) to separate fields
    @api additionalStyle; // Additional stlye to be applied

    @track output = '';
    @track popoverOutput = [];
    
    link;
    showPopover = false;

    userData;
    userInfo;

    objectInfoObject = '';

    @wire(getRecord, { recordId: '$userId', fields: '$fields', optionalFields: '$popoverFields'})
    userDataWired({ error, data }) {
        let context = this;
        this.userData = data;

        if (error) {
            console.log(error);
        } else if (data) {
            context.link = '/' + context.userId;

            context.fields.forEach(function(f, i) {
                context.output += getFieldValue(data, f);
                if (i < context.fields.length - 1) {
                    context.output += context.seperator;
                }
            });

            context.objectInfoObject = 'User'; // Set to trigger wire below
        }
    }

    @wire(getObjectInfo, { objectApiName: '$objectInfoObject' })
    userInfo({ data, error }) {
        if (data) {
            this.userInfo = data;

            this.popoverFields.forEach(function(f, i) {
                let fieldName = f.substring(f.indexOf('.') + 1);
                let fieldLabel = this.userInfo.fields[fieldName].label;
                let fieldValue = this.userData.fields[fieldName].value;
                
                let o = {"label":fieldLabel, "value":fieldValue};

                if (this.userInfo.fields[fieldName].apiName == 'Title') { o.label = 'Job Title'; }
                else if (this.userInfo.fields[fieldName].apiName == 'CompanyName') { o.label = 'Opco'; }
                
                this.popoverOutput.push(o);
            }.bind(this));
        }
    }

    showhidePopover() {
        if (this.usePopover) {
            this.showPopover = !this.showPopover;
        }
    }
}