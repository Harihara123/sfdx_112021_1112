import { LightningElement, api, wire } from 'lwc';
// import { getRecord } from 'lightning/uiRecordApi';


export default class LwcPhenomLeadActivityIndicator extends LightningElement {
    @api talentId;

    hasList = false;
   

    // @wire(getRecord, { recordId: '$talentId', fields: [] })
    // wiredContacttData({ data, error}) {
    //     console.log(`wiredContacttData===> ${this.talentId}`);
    //     if (data){
    //        
    //     }
    //     if (error) {
    //         console.log(`contactData==> ${error.message}`);
    //     }
    // };

    get toggleIndicator() {
        return this.hasLead;
    }

    get toggleList() {
        return this.hasList;
    }

    togglePopover(e){
        e.preventDefault();
    }
}