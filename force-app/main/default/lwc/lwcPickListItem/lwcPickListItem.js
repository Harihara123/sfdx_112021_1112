import { LightningElement, api } from 'lwc';

export default class LwcPickListItem extends LightningElement {
    @api item;
   
    connectedCallback() {
        this._item = JSON.parse(JSON.stringify(this.item));
    }
    get itemClass() {
        return 'slds-listbox__item ms-list-item' + (this.item.selected ? ' slds-is-selected' : '');
    }
    @api
    resetSelectedItem() {
        const listItem = this.template.querySelector('[data-id]');

        if (listItem.classList.contains('slds-is-selected')){
            listItem.classList.remove('slds-is-selected');
        }
        
    }

    onItemSelected(event) {
        this.template.querySelector('[data-id]').classList.toggle('slds-is-selected');

        const evt = new CustomEvent('items', { detail: { 'item': this.item, 'selected': !this.item.selected } });
        this.dispatchEvent(evt);
        event.stopPropagation();
    }
}