import { LightningElement, api } from 'lwc';

export default class LwcPillBar extends LightningElement {
    @api pills = [];
    @api customClass = '';
    @api pillVariant = 'neutral';

    @api
    getPills(){
       return this.pills;
    }

    handleRemove(e) {
        const label = e.currentTarget.getAttribute('data-pill');
        this.removePill(label)
    }
    removePill(label) {
        const trimmedLabel = label.trim()
        const pillIndex = this.findPillIndex(trimmedLabel);

        if (pillIndex > -1) {
            let newPills = [...this.pills];
            newPills.splice(pillIndex, 1);
            this.pills = newPills;
            this.pillChange()
        }
    }
    findPillIndex(label) {
        return this.pills.map(p => p.label.toLowerCase()).indexOf(label.toLowerCase());
    }
    pillChange() {
        this.dispatchEvent(new CustomEvent('pillchange', { detail: { pills: [...this.pills] } }));
    }
}