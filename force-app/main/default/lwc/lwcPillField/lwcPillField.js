import { LightningElement, api } from 'lwc';

export default class LwcPillField extends LightningElement {
    @api pills = [];
    @api value = '';
    @api placeholder = '';
    @api customBlur = false;
    @api className = '';
    @api isIgnoreEnterKeyPress = false;

    state = '';
    container = null;

    handleSubmit(e) {
        e.preventDefault(); 
        if (this.value.trim()) {
            this.addPill(this.value)        
        }
    }
    handleInput(e) {
        const value = e.target.value;
        if (value && value.substring(0, [value.length-1]).trim() && value[value.length-1] === ',') {            
            const label = value.substring(0, value.length-1).trim()
            this.addPill(label);            
        } else {
            this.value = value;
            this.valueChange()
        }
        //custom event needed
    }
    handleBackspace(e) {
        if(e.keyCode === 13 && this.isIgnoreEnterKeyPress) { 
            e.preventDefault(); 
            return;
        }

        this.dispatchEvent(new CustomEvent('keydown', {detail: {e}}))
        if (e.keyCode === 8 && !e.currentTarget.value) {
            const pills = this.template.querySelectorAll('.slds-pill a');
            if (pills.length) {
                pills[pills.length-1].focus()
            }
        }
    }
    addPill(label) {
        const trimmedLabel = label.trim()
        if (this.findPillIndex(trimmedLabel) === -1) {
            const newPills = [...this.pills]
            newPills.push(trimmedLabel)
            this.pills = newPills;
            this.pillChange()

            this.value = ''
        } else {
            this.template.querySelector('input').value = '';
            this.value = '';
            this.dispatchEvent(new CustomEvent('dupe', {detail: label}))
        }
    }
    removePill(label) {
        const trimmedLabel = label.trim()
        const pillIndex = this.findPillIndex(trimmedLabel);
    
        if (pillIndex > -1) {
            let newPills = [...this.pills];
            newPills.splice(pillIndex, 1);
            this.pills = newPills;
            this.pillChange()
        }
        this.template.querySelector('input').focus()
    }
    handleRemove(e) {
        const label = e.currentTarget.getAttribute('data-pill');
        this.removePill(label)
    }
    findPillIndex(label) {
        return this.pills.map(p => p.toLowerCase().trim()).indexOf(label.toLowerCase());
    }
    handleFocus() {
        this.state = 'focused';
        this.dispatchEvent(new CustomEvent('focus', {detail: {value: this.value}}))
    }
    handleKeyDown(e) {   
            //LEFT KYBOARD ARROW
            if (e.keyCode === 37) {
                const prevElem = e.currentTarget.parentNode.previousElementSibling;
                    
                if (prevElem && prevElem.tagName === 'SPAN') {                    
                    prevElem.querySelector('a').focus();
                } else {
                    const pillEls = this.template.querySelectorAll('.slds-pill a')
                    pillEls[pillEls.length-1].focus()                    
                }
            //RIGHT KYBOARD ARROW
            } else if (e.keyCode === 39) {
                const nextElem = e.currentTarget.parentNode.nextElementSibling;
                
                if (nextElem && nextElem.tagName !== 'INPUT') {
                    nextElem.querySelector('a').focus()
                } else {
                    const pillEls = this.template.querySelectorAll('.slds-pill a')
                    pillEls[0].focus()                    
                }
            //BACKSPACE OR DEL
            } else if (e.keyCode === 8 || e.keyCode === 46) {
                e.currentTarget.nextElementSibling.click();
                this.focusInput()                                       
            }
    }
    handleBlur() {
        if (this.customBlur) {
            this.dispatchEvent(new CustomEvent('blur', {detail: {value: this.value}}))
            this.state = '';
        } else {
            this.state = '';
            if (this.value.trim()) {
                this.addPill(this.value)        
            }
        }
        //custom event needed
    }
    @api
    focusInput() {
        const input = this.template.querySelector('input');
        input.focus()
    }
    pillChange() {
        this.dispatchEvent(new CustomEvent('pillchange', {detail: {pills: [...this.pills]}}))
    }
    valueChange() {
        this.dispatchEvent(new CustomEvent('valuechange', {detail: {value: this.value}}))
    }
}