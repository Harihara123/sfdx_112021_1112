import { LightningElement, api } from 'lwc';
import {BD_HELPER_PARAMS, TYPE_PAUSE, getSoqlParams, getExtraParams} from './params';
import performServerCall from '@salesforce/apex/BaseController.performServerCall';

export default class LwcPillsLookup extends LightningElement {
    @api skills = [];
    @api type = 'skills';
    @api opco = ''
    @api className = 'req-skill-search primary'
    value= '';

    allSuggestedSkills = [];
    _suggestedSkills = [];
    @api 
    get suggestedSkills() {
        return this._suggestedSkills;
    }
    set suggestedSkills(value) {
        this.allSuggestedSkills = value;
        this._suggestedSkills = value.slice(0, this.suggestedSkillsMax);
        this.setAttribute('_suggestedSkills', this._suggestedSkills);
        this.updateSuggestedFromSelected();
    }
    
    _allSelectedSkills = [];
    @api
    get allSelectedSkills(){
        return this._allSelectedSkills;
    }
    set allSelectedSkills(value) {
        this._allSelectedSkills = value;
        this.updateSuggestedFromSelected();
    }

    @api inUseSuggestedSkills = [];
    suggestedSkillsMax = 10;
    resultsState = 'hide';
    results = []
    timeoutRef = null;
    SOQL_PARAMS = null;  
    valueAttributeMap = {};
    isCaseSensitive = false;
    valueAttribute = "";
    isIgnoreEnterKeyPress = false;

    connectedCallback() {
        this.SOQL_PARAMS = getSoqlParams(this.type);
        this.updateSuggestedFromSelected();
        const extraParams = getExtraParams(this.type);
        this.isCaseSensitive = extraParams["isCaseSensitive"];
        this.valueAttribute = extraParams["valueAttribute"];
        this.isIgnoreEnterKeyPress = extraParams["isIgnoreEnterKeyPress"];
    }   

    handlePillChange(e) {
        const {pills} = e.detail;

        this.skills = pills;
        let skillDataValues = [];
        if(this.valueAttribute){
            pills.map(pill => skillDataValues.push(this.valueAttributeMap[pill]));
        }

        this.resultsState = 'hide'
        this.results = [];
        this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills, skillDataValues}}));
    }
    handleSuggestedSkillAdd(e) {
        const value = e.currentTarget.getAttribute('data-val');        
        
        const newPills = [...this.skills]; //create shallow copy.
        newPills.push(value); //push new skill to copy.

        this.skills = newPills; //set new skills; re-render.
        this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills}}))
        this.template.querySelector('c-lwc-pill-field').focusInput()//public method from lwcPillField to focus the textfield
    }

    handleSearchedSkillAdd(e) {
        const searchedSkill = e.currentTarget.getAttribute('data-val'); //get data-val attribute set during iteration
        //console.log(searchedSkill);

        const searchedSkillIndex = this.isCaseSensitive
            ? this.skills.indexOf(searchedSkill)
            : this.skills.map(s=> s.toLowerCase()).indexOf(searchedSkill.toLowerCase()); //Check if searched skill clicked is within the skills already.

        if (searchedSkillIndex === -1) {
            //searched skill NOT within skills; add it.
            const newSkills = [...this.skills]; //shalow copy
            newSkills.push(searchedSkill); //addign new skill to copy
            this.skills = newSkills; //setting skills to updated skilss
            // to store and send pill values if the display values and actual values are different
            let skillsDataValues = [];
            if(this.valueAttribute){
                newSkills.map(skill => {
                    skillsDataValues.push(this.valueAttributeMap[skill])
                })
            }

            this.value = ''; //clear out value in textfield
            this.results = []; //clear out results so they dont show during next skill lookup
            this.hideResults() //close results container
            this.template.querySelector('c-lwc-pill-field').focusInput()
            this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills, skillsDataValues}}))
        } else {
            this.value = ''; //clear out value in textfield
            this.results = []; //clear out results so they dont show during next skill lookup
            this.hideResults() //close results container
            this.template.querySelector('c-lwc-pill-field').focusInput()
            this.handleDupeMsg(searchedSkill)
        }
    }
    handleKeyDown(e) {
        const event = e.detail.e; //Event passed up from lwcPillField cmp.
        if (event) {        
            if (event.keyCode === 40 && this.resultsState === 'show') { //If Arrow down pressed and results are shown.
                const liElement = this.template.querySelector('.slds-listbox__item'); //find first available search result.
                if (liElement) {liElement.focus()} //focus the element if axists.
            } else if (event.keyCode === 9) { //tab key
                this.hideResults() //tabbing away; hide searched skill container
            }
        }
    }
    handleSearchedSkillNav(e) { //Handle onkeydown of the <li> element in results.
        e.preventDefault();
        if (e.keyCode === 40) { //Arrow down.
            const nextElem = e.currentTarget.nextElementSibling; //get next element.
            if (nextElem) {nextElem.focus()} //focus next element.
        } else if (e.keyCode === 38) {//arrow up.
            const prevElem = e.currentTarget.previousElementSibling; //get previous element.
            if (prevElem) {prevElem.focus()} //focus previous element.
        } else if (e.keyCode === 13) {//enter
            e.currentTarget.click(); //click the searched skill, which has a handler to add it to skills and close the results container.
            //this.template.querySelector('c-lwc-pill-field').focusInput() //public method from lwcPillField to focus the textfield
        } else if (e.keyCode === 27) {//escape
            this.hideResults()
            this.template.querySelector('c-lwc-pill-field').focusInput() //public method from lwcPillField to focus the textfield
        }
    }
    handleBlur(e) {
        //console.log(this.value)
    }
    handleChange(e) { //custom event from lwcPillField when value changes in the textfield.
        const value = e.detail.value; 
        this.value = e.detail.value //setting value from lwcPillField to be same as our value

        if (value.length > 2) { //if the value is longer than 2 characters initialize server call
            this.callServer(value)
        }
    }
    checkClickedNode(e) {
        const clickedEl = e.path[0]; //get the element clicked; e.path is safer way to get an element when need to "reach" into other component elements.
        const resultsContainer = this.template.querySelector('.slds-dropdown') //get container element.

        if (!resultsContainer.contains(clickedEl)) { //if the clicked element is NOT within the container close the results; clicked away.
            this.hideResults()
        }
    }
    hideResults() { //Hide results and remove event listener.
        this.resultsState = 'hide';
        document.removeEventListener('click', this.checkClickedNode.bind(this))
    }
    showResults() { //show results and initialize the event listener that will check what was clicked.
        this.resultsState = 'show';
        document.addEventListener('click', this.checkClickedNode.bind(this))
    }
    callServer(searchString) { 
        this.showResults();  // starting to show results before getting them from server  
        if (this.timeoutRef) { //checking if timeout reference was set; if it was, clear out the timeout and no server call     
            clearTimeout(this.timeoutRef)
        }

        this.timeoutRef = setTimeout(async () => { //timeout to call server
            const updatedSoqlParams = {...this.SOQL_PARAMS, whereCondition: `${this.SOQL_PARAMS.whereCondition} ${this.opco?`'${this.opco}'`:''}`} // no opco keep it conditional
            const searchParams = {searchString, ...updatedSoqlParams} //combining searchString value with SOQL_PARAMS object
            const search = {parameters: searchParams, ...BD_HELPER_PARAMS} //combining searchParams with BD_HELPER_PARAMS
             try {
                const results = await performServerCall(search);  //wait for the response from server            
                const parsedResults = JSON.parse(results);
                let resultsArray = [];
                //iterate over the skills and reduce the array into simple string array; set it to display results
                parsedResults.map(skill => {
                    resultsArray.push(skill.value);
                    if(this.valueAttribute) {
                        this.valueAttributeMap[skill.value] = skill[this.valueAttribute];
                    }
                })
                this.results = resultsArray;
            } catch (err) {
                console.log(err)
            }
        },TYPE_PAUSE)
    }
    handleDupeMsg(e) {
        const val = e.detail || e
        this.dispatchEvent(new CustomEvent('dupe', {detail: {val}}))
    }

    updateSuggestedFromSelected() {
        let newSuggestedSkills = [];

        //let allSelectedSkillsList = this.allSelectedSkills.map(s => s.name.toLowerCase());
        let allSelectedSkillsList = [];
        if(this.isCaseSensitive){
            this.allSelectedSkills.forEach(function(item) {
                if (item.name != undefined && item.name != "") {
                    allSelectedSkillsList.push(item.name);
                }
            });
    
            this.allSuggestedSkills.forEach(function(skill, index) { // Loop through all suggested skills
                if (!allSelectedSkillsList.includes(skill)) { // If the suggested skill isn't currently selected... 
                    newSuggestedSkills.push(skill); // ...keep it in the suggested list
                }
            }); 
        } else {
            this.allSelectedSkills.forEach(function(item) {
                if (item.name != undefined && item.name != "") {
                    allSelectedSkillsList.push(item.name.toLowerCase());
                }
            });
    
            this.allSuggestedSkills.forEach(function(skill, index) { // Loop through all suggested skills
                if (!allSelectedSkillsList.includes(skill.toLowerCase())) { // If the suggested skill isn't currently selected... 
                    newSuggestedSkills.push(skill); // ...keep it in the suggested list
                }
            });
        }

        this._suggestedSkills = newSuggestedSkills.slice(0, this.suggestedSkillsMax);
        this.setAttribute('_suggestedSkills', this._suggestedSkills);
    }
}