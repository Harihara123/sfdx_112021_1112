export const SOQL_PARAMS_MAP = { //params for searching using BaseController method
    skills: {
        addlReturnFields: '[]',
        type: 'Global_LOV__c',
        idFieldOverride: 'id',
        nameFieldOverride: 'Text_Value__c',
        recTypeName: 'MatchSkillList',
        sortCondition: 'Number_Value__c DESC',
        useCache: false,
        whereCondition: "Lov_Name__c = 'MatchSkillList' and TextValue2_CompanyName__c ="
    },
    jobTitle: {
        addlReturnFields: '[]',
        type: 'Global_LOV__c',
        idFieldOverride: 'id',
        nameFieldOverride: 'Text_Value__c',
        recTypeName: 'JobTitleList',
        sortCondition: 'Number_Value__c DESC',
        useCache: false, // 
        whereCondition: "Lov_Name__c = 'JobTitleList' and TextValue2_CompanyName__c =" // to get data
    },
    tfoDrzBoards: {
        addlReturnFields: '[]',
        type: 'DRZ_Board_Data__c',
        idFieldOverride: 'id',
        nameFieldOverride: 'Board_Name__c',
        recTypeName: '',
        useCache: false,
        whereCondition: "DRZ_Parent_Board__c != null"
    }
}

export const getSoqlParams = (type) => {
    switch(type) {
        case 'skills':
            return SOQL_PARAMS_MAP['skills'];
        case 'jobTitle':
            return SOQL_PARAMS_MAP['jobTitle'];
        case 'tfoDrzBoards':
            return SOQL_PARAMS_MAP['tfoDrzBoards'];
        default:
            return SOQL_PARAMS_MAP['skills'];
    }
}

export const BD_HELPER_PARAMS = { //params for BaseController method.
    className: 'Lookup',
    methodName: 'querySObject',
    subClassName: ""
}

export const getExtraParams = (type) => {
    switch(type) {
        case 'tfoDrzBoards':
            return ({
                isCaseSensitive: true,
                valueAttribute: "id",
                isIgnoreEnterKeyPress: true,
            });
        default:
            return ({
                isCaseSensitive: false,
                valueAttribute: "",
                isIgnoreEnterKeyPress: false
            });
    }
}

export const TYPE_PAUSE = 300; //Query server if nothing typed for this length of time after starting typing;