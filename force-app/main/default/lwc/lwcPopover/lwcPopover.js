import {LightningElement, api} from 'lwc';

export default class LwcPopover extends LightningElement {
    @api title = '';
    @api cssContactLayout = '';

    closePopover(event) {
        this.dispatchEvent(new CustomEvent('filterselect', {detail: {'selectedValue':'',filterType:''}}));
    }
}