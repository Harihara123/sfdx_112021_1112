import { LightningElement, api, wire, track } from 'lwc';
/* Wire adapter to fetch record data */
import { getRecord } from 'lightning/uiRecordApi';
import additionalProfileDetails from '@salesforce/apex/MyActivitiesController.getProfileDetails';
import ATS_PROFILE from '@salesforce/label/c.ATS_PROFILE';
import ATS_EMPLOYER from '@salesforce/label/c.ATS_EMPLOYER';
import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import ATS_EMAIL from '@salesforce/label/c.ATS_EMAIL';
import ATS_PHONE from '@salesforce/label/c.ATS_PHONE';
import ATS_LOCATION from '@salesforce/label/c.ATS_LOCATION';
import ATS_TALENT_ID from '@salesforce/label/c.ATS_TALENT_ID';
import ATS_RESUME_LAST_UPDATED from '@salesforce/label/c.ATS_RESUME_LAST_UPDATED';
import ATS_WEBSITE from '@salesforce/label/c.ATS_WEBSITE';
import ATS_RECENT_SUBMITTAL from '@salesforce/label/c.ATS_RECENT_SUBMITTAL';
import ATS_RECET_SUBIMITTAL_DATE from '@salesforce/label/c.ATS_RECET_SUBIMITTAL_DATE';
import ATS_COMMENT from '@salesforce/label/c.ATS_COMMENT';

const FIELDS = [
		'Contact.Name',
		'Contact.Title',
		'Contact.Phone',
		'Contact.Email',
		'Contact.Current_Employer__c',
		'Contact.Preferred_Email__c',
		'Contact.Other_Email__c',
		'Contact.Work_Email__c',
		'Contact.MobilePhone',
		'Contact.HomePhone',
		'Contact.Phone',
		'Contact.OtherPhone',
		'Contact.Preferred_Phone_Value__c',
		'Contact.Mailing_Address_Formatted__c',
		'Contact.Talent_Id__c',
		'Contact.AccountId',
		'Contact.Website_URL__c',
		'Contact.Do_Not_Use__c',
		'Contact.RWS_DNC__c',
		'Contact.Do_Not_Contact__c',
];
export default class LwcProfileTab extends LightningElement {
	label = {
        ATS_PROFILE,
        ATS_EMPLOYER,
        ATS_JOB_TITLE,
        ATS_EMAIL,
        ATS_PHONE,
        ATS_LOCATION,
        ATS_TALENT_ID,
        ATS_RESUME_LAST_UPDATED,
        ATS_WEBSITE,
        ATS_RECENT_SUBMITTAL,
        ATS_RECET_SUBIMITTAL_DATE,
        ATS_COMMENT
    };	

	@api recordId;
	@api accountId;
	@track submittalStatus;
	@track submittalDate;
	@track resumeLastModified;
	@track recentSubmittal;
	@track recentSubmittalDate;
	@track comments;
	@track currentEmployerText;
	@track currentEmployerName;
	@track currentEmployerId;
	@track isCurrentEmployerText = false;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    contact;

    get name() {	
        return this.contact.data.fields.Name.value;
    }

    get title() {
        return this.contact.data.fields.Title.value;
    }

    get phone() {
		let phone;
		if(this.contact.data.fields.Phone.value != null)
			phone = 'W:'+this.contact.data.fields.Phone.value;
		else
			phone = this.contact.data.fields.Phone.value;
			
        return phone;
    }

    get email() {
        return this.contact.data.fields.Email.value;
    }
	get emailLink() {
		let link='';
		if(this.contact.data.fields.Email.value != null) {
			link = "mailto:"+this.contact.data.fields.Email.value;
		}
		return link;

	}

	get currentEmployer() {
		//let test = "<a href='/0031700000wSLyKAAW' target='_blank'><a href='/0031700000wSLyKAAW' target='_blank'>test</a></a>";		
		return this.contact.data.fields.Current_Employer__c.value;
		//return test;
	}
	get preferredEmail() {
		return this.contact.data.fields.Preferred_Email__c.value;
	}
	get otherEmail() {
		return this.contact.data.fields.Other_Email__c.value;
	}
	get otherEmailLink() {
		let link='';
		if(this.contact.data.fields.Other_Email__c.value != null) {
			link = "mailto:"+this.contact.data.fields.Other_Email__c.value;
		}
		return link;

	}
	get workEmail() {
		return this.contact.data.fields.Work_Email__c.value;
	}
	get workEmailLink() {
		let link='';
		if(this.contact.data.fields.Work_Email__c.value != null) {
			link = "mailto:"+this.contact.data.fields.Work_Email__c.value;
		}
		return link;

	}
	get mobilePhone() {
		return this.contact.data.fields.MobilePhone.value;
	}
	get mobilePhoneNotNull() {
		if(this.contact.data.fields.MobilePhone.value != null)
			return true;
		else
			return false;

		return false;
	}
	get mobilePhoneLink() {
		let link='';
		if(this.contact.data.fields.MobilePhone.value != null) {
			link = "tel:"+this.contact.data.fields.MobilePhone.value;
		}
		return link;

	}
	get homePhone() {
		return this.contact.data.fields.HomePhone.value;
	}
	get homePhoneNotNull() {
		if(this.contact.data.fields.HomePhone.value != null)
			return true;
		else
			return false;

		return false;
	}
	get homePhoneLink() {
		let link='';
		if(this.contact.data.fields.HomePhone.value != null) {
			link = "tel:"+this.contact.data.fields.HomePhone.value;
		}
		return link;

	}
	get phone() {
		return this.contact.data.fields.Phone.value;
	}
	get phoneNotNull() {
		if(this.contact.data.fields.Phone.value != null)
			return true;
		else
			return false;

		return false;
	}
	get phoneLink() {
		let link='';
		if(this.contact.data.fields.Phone.value != null) {
			link = "tel:"+this.contact.data.fields.Phone.value;
		}
		return link;

	}
	get otherPhone() {
		return this.contact.data.fields.OtherPhone.value;
	}
	get otherPhoneNotNull() {
		if(this.contact.data.fields.OtherPhone.value != null)
			return true;
		else
			return false;

		return false;
	}
	get otherPhoneLink() {
		let link='';
		if(this.contact.data.fields.OtherPhone.value != null) {
			link = "tel:"+this.contact.data.fields.OtherPhone.value;
		}
		return link;

	}
	get preferredPhoneValue() {
		return this.contact.data.fields.Preferred_Phone_Value__c.value;
	}
	get mailingAddress() {
		return this.contact.data.fields.Mailing_Address_Formatted__c.value;
	}
	get talentId() {
		return this.contact.data.fields.Talent_Id__c.value;
	}
	get AccountId() {		
		return this.contact.data.fields.AccountId.value;
	}
	get webSiteURL() {
		let websiteUrl;
		if(this.contact.data.fields.Website_URL__c.value != null) {
			websiteUrl = 'https://'+this.contact.data.fields.Website_URL__c.value;
		} else {
			websiteUrl = this.contact.data.fields.Website_URL__c.value;
		}
		return websiteUrl;
	}
	get dncDnu() {
		//if(this.doNotUse || this.rwsdnc || this.doNotContact)dncAccount
		if(this.doNotUse || this.rwsdnc || this.dncAccount)
			return true;
		else	
			return false;

		return false;

	}
	get dnc() {
		//if(this.rwsdnc || this.doNotContact)
		if(this.rwsdnc || this.dncAccount)
			return true;
		else	
			return false;

		return false;
	}

	get dnu() {
		if(this.doNotUse)
			return true;
		else
			return false;
		
		return false;
	}

	get doNotContact() {
		return this.contact.data.fields.Do_Not_Contact__c.value;
	}

	get rwsdnc() {
		return this.contact.data.fields.RWS_DNC__c.value;
	}

	get doNotUse() {
		return this.contact.data.fields.Do_Not_Use__c.value;
	}

	connectedCallback(){
		///this.accountId = this.contact.data.fields.AccountId.value;
		
		this.getAddlProfileDetails();
	}

	@track dncAccount = false;
	getAddlProfileDetails() {
		
		additionalProfileDetails({contactId: this.recordId}).then((result) =>{
			if(result != undefined){           
				this.handleSearchResponse(result);
			}                
			}).catch((error) => {
			console.log('performServerCall 2 '+JSON.stringify(error));
		});

	}

	handleSearchResponse(result) {
		if(result.resumeLastUpdatedDate)
			this.resumeLastModified = result.resumeLastUpdatedDate;
		if(result.recentSubmittal)
			this.recentSubmittal = result.recentSubmittal;
		if(result.recentSubmittalDate)
			this.recentSubmittalDate = result.recentSubmittalDate.toString();
		this.comments = result.comments;
		if(result.currentEmployerId === '') {			
			this.currentEmployerText = result.currentEmployerText;
			this.isCurrentEmployerText = true;
		}
		else {
			this.currentEmployerName = result.currentEmployerName;
			this.currentEmployerId = result.currentEmployerId;
			this.isCurrentEmployerText = false;

		}
		//Do not contact field from account
		this.dncAccount = result.doNotContact;

	}

}