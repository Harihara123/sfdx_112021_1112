import { api, LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import LANG from '@salesforce/i18n/lang';
import searchResult from '@salesforce/apex/CSC_RTCAReportController.searchResult';

// datatable columns
const columns = [
	{
		label: 'Case Number',
		fieldName: 'caseNumber',
		//type: 'text',
	},
	{
		label: 'Owner',
		fieldName: 'caseOwnerName',
	},
	{
		label: 'Contact',
		fieldName: 'contactName',
	},
	{
		label: 'Created Date',
		fieldName: 'CreatedDate',
	},
	{
		label: 'Closed Date',
		fieldName: 'ClosedDate',
	},
	{
		label: 'Case Creator-Office ID',
		fieldName: 'officeId',
	},
	{
		label: 'Region',
		fieldName: 'caseRegion',
	},
	{
		label: 'Status',
		fieldName: 'Status',
	},
	{
		label: 'Type',
		fieldName: 'caseType',
	},
	{
		label: 'Sub Type',
		fieldName: 'caseSubType',
	},{
		label: 'Business Unit',
		fieldName: 'businessUnit',
	},{
		label: 'Business Region',
		fieldName: 'businessRegion',
	},
	{
		label: 'Center Name',
		fieldName: 'centerName',
	},
	{
		label: 'Talent Office',
		fieldName: 'officeTalent',
	},
	{
		label: 'New',
		fieldName: 'ageNew',
	},
	{
		label: 'Assigned',
		fieldName: 'ageAssigned',
	},
	{
		label: 'In Progress',
		fieldName: 'ageInProgress',
	},
	{
		label: 'On Hold',
		fieldName: 'ageOnHold',
	},
	{
		label: 'Escalated',
		fieldName: 'ageEscalated',
	},
	{
		label: 'Under Review',
		fieldName: 'ageUnderReview',
	},
	{
		label: 'Case Age Excluding On Hold',
		fieldName: 'caseAge',
	},
	{
		label: 'Case Re-opened Count',
		fieldName: 'reOpenCount',
	},
];

export default class LwcRealTimeCaseAgeReport extends LightningElement {
	isLoading = false;
    @api incomingData = {};
    state = {};
	@track columns = columns;
	@track searchData;
    opcoValue = 'TEK';
    centerValue = '';
    statusValues = [];
    statusOptions = [
        { key: 1, value: 'New', selected: false },
        { key: 2, value: 'Assigned', selected: false },
        { key: 3, value: 'In Progress', selected: false },
        { key: 4, value: 'On Hold', selected: false },
        { key: 5, value: 'Escalated', selected: false },
        { key: 6, value: 'Under Review', selected: false },
        { key: 7, value: 'Closed', selected: false }
    ];

    talentOffice = '';
    talentOfficeList = [];

    // Default value for date range
    today = new Date();
    createdDateFrom = this.today;
    createdDateTo = this.today;
    createdDateRequired = true;

    closedDateFrom = this.today;
    closedDateTo = this.today;
    closedDateRequired = false;

    connectedCallback() {
        this.isLoading = true;
    }

    handleLoad(e) {
        if(e.detail){
            this.isLoading = false;
        }
    }

    get opcoOpts() {
        return [
            { label: 'TEK', value: 'TEK' },
            { label: 'ONS', value: 'ONS' }
        ];
    } 

    get centerOpts() {
        return [
            { label: 'Jacksonville', value: 'JACKSONVILLE' },
            { label: 'Tempe', value: 'TEMPE' }
        ];
    } 

    get statusOpts() {
        return this.statusOptions;
    } 

    handleOpcoSelect(e){
        this.opcoValue = e.target.value;
        const submitData = { ...this.state, opcoValue: this.opcoValue };
        this.state = submitData;
    }

    handleCenterSelect(e){
        this.centerValue = e.target.value;
        const submitData = { ...this.state, centerValue: this.centerValue };
        this.state = submitData;
    }

    handleCreatedDateRange(e){
        const { name, value } = e.target;

        if( name === 'from'){
            this.createdDateFrom = value;
        }

        if( name === 'to'){
            this.createdDateTo = value;
        }

        const dateInput = '.createdDate';
        const formattedDate = this.getDateRange( this.createdDateFrom, this.createdDateTo);

        if(this.validateDate(formattedDate[0], formattedDate[1], dateInput)) {
            this.state = {
                ...this.state,
                createdDate: formattedDate.join(',')
            }
        }
        
    }

    handleCloseDateRange(e){
        const { name, value } = e.target;

        if (name === 'from') {
            this.closedDateFrom = value;
        }

        if (name === 'to') {
            this.closedDateTo = value;
        }

        const dateInput = '.closedDate';
        const formattedDate = this.getDateRange( this.closedDateFrom, this.closedDateTo);

        if (this.validateDate(formattedDate[0], formattedDate[1], dateInput)) {
            this.state = {
                ...this.state, 
                closedDate: formattedDate.join(',')
            }
        } 
    }

    formatDate(date) {
        const options = {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric'
        };
        const dateTimeFormat = new Intl.DateTimeFormat(LANG, options);
        
        const formattedDate = dateTimeFormat.format(new Date(date).setUTCHours(23))
        
        return formattedDate;
    }

    validateDate(from, to, input) {
        const fromDate = new Date(from).getTime();
        const toDate = new Date(to).getTime();
        const dateInput = this.template.querySelectorAll(input);

        if(fromDate > toDate){
            dateInput.forEach(input => {
                input.setCustomValidity("Please Enter a valid date range");
                input.reportValidity();
            });
            return false;
        } else {
            dateInput.forEach(input => {
                input.setCustomValidity("");
                input.reportValidity();
            });
            return true;
        }
    }

    getDateRange(from, to) {
        const formattedDate = [];
     
        formattedDate.push(this.formatDate(from));
        formattedDate.push(this.formatDate(to));
        
        return formattedDate;
    }

    handleStatusSelect(e){
        if(e.detail){
            const selectedValues = [];

            e.detail.forEach((item) => {
                selectedValues.push(item.value);
            });

            this.statusValues = selectedValues;

            const findClosed = this.statusValues.findIndex(value => value === 'Closed');

            if (findClosed !== -1) {
                this.createdDateRequired = false;
                this.closedDateRequired = true;
            } else {
                this.createdDateRequired = true;
                this.closedDateRequired = false;
            }

            this.state = { ...this.state, statusValues: this.statusValues };
        }
    }

    get setRequiredCreatedDate() {
        return this.createdDateRequired;
    }

    get setRequiredClosedDate() {
        return this.closedDateRequired;
    }

    handleTargetOffice(e){
		this.talentOffice = e.detail.value;
        this.state = { ...this.state, talentOfficeId: this.talentOffice };
		
    }

    validateInputs(input, message) {
        const dateInput = this.template.querySelectorAll(input);
        dateInput.forEach(input => {
            input.setCustomValidity(message);
            input.reportValidity();
        });
    }

    exportExcel(){
        let isValid = true;
        const findClosed = (this.state.statusValues || []).findIndex(value => value === 'Closed');

        if (findClosed !== -1 && (this.state.closedDate || '') === '') {
            isValid = false;
            this.validateInputs('.closedDate', "Please Enter a valid date range");
        } else if (findClosed === -1 && (this.state.createdDate || '') === '') {
            isValid = false;
            this.validateInputs('.createdDate', "Please Enter a valid date range");
        } else {
            // Clear any present errors
            this.validateInputs('lightning-input', "");
        }

        if(isValid){
            this.isLoading = true;
            searchResult({ inputData: JSON.stringify(this.state) }).then(result => {
                //this.state = { ...this.state, result };
                this.searchData = result;
                this.isLoading = false;
                // Method to generate CSV :: START ::
                if(this.searchData != '' && this.searchData != null){
					 let columnHeader = ["Case Number", "Owner", "Contact", "Created Date", "Closed Date", "Case Creator-Office ID", "Region", "Status", "Type", "Sub Type", "Business Unit", "Business Region", "Center Name", "Talent Office", "New", "Assigned", "In Progress", "OnHold", "Escalated", "Under Review", "Case Age Excluding On Hold", "Case Re-opened Count"];  // This array holds the Column headers to be displayd
					 let jsonKeys = ["caseNumber", "caseOwnerName", "contactName", "CreatedDate", "ClosedDate", "officeId", "caseRegion", "Status", "caseType", "caseSubType", "businessUnit", "businessRegion", "centerName", "officeTalent", "ageNew", "ageAssigned", "ageInProgress", "ageOnHold", "ageEscalated", "ageUnderReview", "caseAge", "reOpenCount"]; // This array holds the keys in the json data  
					 var jsonRecordsData = this.searchData;  
					 let csvIterativeData;  
					 let csvSeperator  
					 let newLineCharacter;  
					 csvSeperator = ",";  
					 newLineCharacter = "\n";  
					 csvIterativeData = "";  
					 csvIterativeData += columnHeader.join(csvSeperator);  
					 csvIterativeData += newLineCharacter;  
					 for (let i = 0; i < jsonRecordsData.length; i++) {  
					   let counter = 0;  
					   for (let iteratorObj in jsonKeys) {  
						 let dataKey = jsonKeys[iteratorObj];  
						 if (counter > 0) {  csvIterativeData += csvSeperator;  }  
						 if (  jsonRecordsData[i][dataKey] !== null &&  
						   jsonRecordsData[i][dataKey] !== undefined  
						 ) {  csvIterativeData += '"' + jsonRecordsData[i][dataKey] + '"';  
						 } else {  csvIterativeData += '""';  
						 }  
						 counter++;  
					   }  
					   csvIterativeData += newLineCharacter;  
					 }  
					 console.log("csvIterativeData", csvIterativeData);  
				 
					// Creating anchor element to download
					let downloadElement = document.createElement('a');
					downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvIterativeData);
					downloadElement.target = '_self';
					downloadElement.download = 'Real Time Case Age Report.csv';
					document.body.appendChild(downloadElement);
					downloadElement.click();

					// Method to generate CSV :: END ::

					this.isLoading = false;
				}else{
					this.isLoading = false;
					this.showToast('Error', 'Data Not Found', 'error');
				}
                // Method to generate CSV :: END ::

            }).catch(error => {
                console.log(error);
                this.showToast('Error', 'An unexpected error has occured, please contact your Admin', 'error');
                this.isLoading = false;
            });
        } else {
            this.showToast('Error', 'Please enter valid data', 'error');
        }

    }

    clearFilters(){
        this.state = {};
        this.opcoValue = 'TEK';
        this.centerValue = '';
        this.talentOffice = '';

        // Clear Status Multi Select
        const statusOptions = this.template.querySelector('c-lwc-multi-select-picklist');
        statusOptions.resetSelectedItems();

        // Set Date Range to default values
        this.createdDateFrom = this.today;
        this.createdDateTo = this.today;

        this.closedDateFrom = this.today;
        this.closedDateTo = this.today;

        // Clear Date Range fields
        this.template.querySelectorAll("lightning-input").forEach(input => {
            input.value = "";
            input.required = false;
            input.setCustomValidity("");
            input.reportValidity();
        });

        this.createdDateRequired = true;
        this.closedDateRequired = false;

        this.template.querySelectorAll('.createdDate').forEach(input => input.required = this.createdDateRequired);
        this.template.querySelectorAll('.closedDate').forEach(input => input.required = this.closedDateRequired);
       
        
        // Clear Lookup field
        this.template.querySelector('lightning-input-field').reset();
        
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

}