import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import TC_within_24_hours from '@salesforce/label/c.TC_within_24_hours';

export default class LwcRecordUrl extends NavigationMixin(LightningElement) {
    @api recordId
    @api object
    @api fieldLabel
    linkText = ''
    url = ''

    @api
    get text() {
        return this.linkText
    }
    set text(text) {
        this.linkText = text;
        setTimeout(() => {
            this.generateUrl()
        }, 0)
    }


    generateUrl() {
        const params = {
            type: "standard__recordPage",
            attributes: {
                "recordId": this.recordId,
                "objectApiName": this.object,
                "actionName": "view"
            }
        };
        this[NavigationMixin.GenerateUrl](params)
        .then(url => {
            this.url = url
        });
    }
    // S-197083 Added by Rajib Maity
    navTo(e) {
        e.preventDefault();
        e.stopPropagation();
        // this custom event is handled on lwcRefSearch to log data in NewRelic  // S-197083 Added by Rajib Maity
        this.dispatchEvent(new CustomEvent('clickonlink',{ bubbles: true, composed:true, detail: {recordId : this.recordId, fieldLabel : this.fieldLabel} }))
        setTimeout(() => {
            window.open(this.url)
        }, 1000);
        
    }
}