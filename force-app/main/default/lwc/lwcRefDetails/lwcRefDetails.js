import { LightningElement, api } from 'lwc';
import getRefDetails from '@salesforce/apex/RunSearchNDisplayClass.getRefDetails';
import {objPath} from 'c/lwcUtilities'
import {ConnectedLogApi} from 'c/connectedLogUtils'; // S-185932 Added by Rajib Maity

const connectedLogAPI = new ConnectedLogApi(); // S-185932 Added by Rajib Maity
const DETAILS_SCHEMA = [
    {label: 'name', field: 'Recommendation_ID__r.Talent_Contact__r.Name'},
    {label: 'talentId', field: 'Recommendation_ID__r.Talent_Contact__c'},
    {label: 'createdDate', field: 'CreatedDate'},
    {label: 'title', field: 'Recommendation_ID__r.Talent_Contact__r.Title'},
    {label: 'email', field: ['Recommendation_ID__r.Talent_Contact__r.Email']},
    {
        label: 'phone', 
        field: [
            'Recommendation_ID__r.Talent_Contact__r.Phone',
            'Recommendation_ID__r.Talent_Contact__r.OtherPhone',
            'Recommendation_ID__r.Talent_Contact__r.HomePhone'
        ]
    },
    {label: 'startDate', field: 'Recommendation_ID__r.Start_Date__c'},
    {label: 'endDate', field: 'Recommendation_ID__r.End_Date__c'},
    {label: 'referenceCheckedBy', field: 'Recommendation_ID__r.Completed_By__r.Name'},
    {label: 'userId', field: 'Recommendation_ID__r.Completed_By__r.Id'},
    {label: 'jobDuties', field: 'Recommendation_ID__r.Talent_Job_Duties__c'},
    {label: 'projectDescription', field: 'Recommendation_ID__r.Project_Description__c'}
]

export default class LwcRefDetails extends LightningElement {
    showSpinner = false;
    record = {};
    references = []
    @api async getDetails(id) {        
        this.showSpinner = true;
        try {
            const refDetails = await getRefDetails({refSearchId: id});            
            this.references = [];
            this.references = this.formatDetails(refDetails)
            this.record = this.formatRecord(refDetails)
            this.showSpinner = false;
        } catch(err) {
            console.log(err)
        }
    }
    formatDetails(details) {
        const formattedDetails = details.map((item, index) => {
            let returnObj = {}
            DETAILS_SCHEMA.forEach(schema => {
                if (schema.field.constructor === Array) {      
                    if (schema.label === 'email') {
                        returnObj[schema.label] = this.formatHTML(schema.field, item, 'email')
                    } else if (schema.label === 'phone') {
                        returnObj[schema.label] = this.formatHTML(schema.field, item, 'phone')
                    } else {
                        returnObj[schema.label] = schema.field.reduce((acc, curVal) => {
                            const val = objPath(item, curVal);
                            if (val) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                    }
                } else {
                    returnObj[schema.label] = objPath(item, schema.field);
                }
            })

            returnObj.collapsed = (index !== 0)
            return returnObj
        })        
        return formattedDetails;
    }
    formatRecord(details) {
        const rec = details[0] || {};        
        return {
            id: rec.Recommendation_From__c,
            object: 'Contact',
            name: rec.Full_Name__c,
            jobTitle: rec.Reference_Job_Title__c,
            company: rec.Recommendation_ID__r.Organization_Name__c,
            companyId: rec.Recommendation_ID__r.Client_Account__c
        }
    }
    formatHTML(emails, details, type) {
        const typeFx = type === 'email' ? this.formatEmailHTML : this.formatPhoneHTML;
        return emails.reduce((acc, curVal) => {
            const val = objPath(details, curVal);
            if (val) {
                acc += typeFx(val)
            }
            return acc
        }, '')        
    }
    formatEmailHTML(email) {
        return `<div class="slds-truncate"><a title="${email}" href="mailto:${email}">${email}</a></div>`
    }
    formatPhoneHTML(phone) {
        return `<div class="slds-truncate" title="${phone}"><a href="tel:${phone}">${phone}</a></div>`
    }

	// S-197083 Added by Rajib Maity
    handleLinkClickAction(e){
        console.log('----ref detail------');
        console.log(e.detail.fieldLabel);
        let fieldLabel = e.detail.fieldLabel;
        if(fieldLabel == 'ReferenceFor'){
            connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch/talentcard',  Method: 'handleLinkClickAction', Module: 'Ref Search', RecordId:e.detail.recordId, AdditionalData : {action: 'click talent'} });
        }
        if(fieldLabel == 'ReferenceCheckedBy'){
            connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch/talentcard',  Method: 'handleLinkClickAction', Module: 'Ref Search', RecordId:e.detail.recordId, AdditionalData : {action: 'click reference'} });
        }
    }
}