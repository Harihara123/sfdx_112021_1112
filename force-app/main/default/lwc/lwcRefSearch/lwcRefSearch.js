import locale from '@salesforce/i18n/locale';
import { LightningElement } from 'lwc';
import fetchAcc from '@salesforce/apex/RunSearchNDisplayClass.fetchAccount';
import getPlaceDetails from '@salesforce/apex/GooglePlacesController.getPlaceDetails';
import {ConnectedLogApi} from 'c/connectedLogUtils'; // S-185932 Added by Rajib Maity

const connectedLogAPI = new ConnectedLogApi(); // S-185932 Added by Rajib Maity
const TABLE_COLS = [
    {label:'Name', field:'Full_Name__c', sort: true},
    {label:'Company', field:'Organization_Name__c', sort: true},
    {label:'Reference Job Title', field:'Reference_Job_Title__c', sort: true},
    {label:'Talent Location', field:'MailingAddress__c', sort: true},
    {label:'Last Reference', field:'CreatedDate', sort: true},
    {label:'Phone', field:'Recommendation_ID__r.Home_Phone__c'},
    {label:'Email', field:'Recommendation_ID__r.Email__c'},
]
// {label:'Job Duties', field:'Talent_Job_Duties__c'},
// {label:'Project Description', field:'Project_Description__c'}
const TABLE_ACTIONS = [
    {action: 'view', icon: 'utility:preview'}
]
const SEARCH_INPUTS = [
    {field: 'refName', label: "Reference's Name", icon: 'utility:user'},
    {field: 'refCompany', label: "Reference's Company", icon: 'utility:company'},
    {field: 'refTitle', label: "Reference's Title", icon: 'utility:case'},
    // {field: 'lcoation', label: "Location", icon: 'utility:location'}
]
//    {field: 'candidateTitle', label: "Candidate’s Skills/Job Titles", icon: 'utility:copy_to_clipboard'}
const PHONE_REF_FIELDS = [
    ['H','Recommendation_From__r.HomePhone'],
    ['M', 'Recommendation_From__r.MobilePhone'],
    ['W', 'Recommendation_From__r.Phone'],
    ['O', 'Recommendation_From__r.OtherPhone'],  
]

const PHONE_TALENT_FIELDS = [
    ['H','Recommendation_ID__r.Home_Phone__c'],
    ['M', 'Recommendation_ID__r.Mobile_Phone__c'],
    ['W', 'Recommendation_ID__r.Work_Phone__c'],
    ['O', 'Recommendation_ID__r.Other_Phone__c'],
]

const EMAIL_REF_FIELDS = [
    'Recommendation_From__r.Email'
]

const EMAIL_TALENT_FIELDS = [
    'Recommendation_ID__r.Email__c',
]

const LOCALE_UNIT = (locale) => {
    switch(locale) {
        case 'en-US':
            return 'mi'
        default:
            return 'km'
    }
}

const RADII_VALUES =  [
        {value: 5, selected: ''},
        {value: 10,  selected: ''},
        {value: 20, selected: ''},
        {value: 30, selected: 'selected'},
        {value: 40, selected: ''},
        {value: 50, selected: ''},
        {value: 75, selected: ''},
        {value: 100, selected: ''}
    ]

export default class LwcRefSearch extends LightningElement {
    tableColumns = TABLE_COLS;
    tableActions = TABLE_ACTIONS;
    inputs = SEARCH_INPUTS;
    detailsFor = null;
    searchObject = {};
    tableData = [];
    showSpinner = false;
    noResults = false;
    pills = [];
    disableRadius = true;
    radi = RADII_VALUES;
    radiusUnit = LOCALE_UNIT(locale);

    handleInputChange(e) {
        const field = e.target.getAttribute('data-field');
        this.searchObject[field] = e.target.value;
    }
    handlePillChange(e) {
        const pills = e.detail.pills;
        this.pills = pills;        
        this.searchObject.candidateTitle = JSON.stringify(pills);
    }
    handleSubmit(e) {
        e.preventDefault();
        this.search();
    }
    handleLocationChange(e) {
        const value = e.detail.value;
        this.searchObject.lcoation = value;
        this.disableRadius = true;        
        if (this.searchObject.radius) {
            const newSearchObject = {...this.searchObject};
            delete newSearchObject.radius;
            this.searchObject = newSearchObject;
        }
    }
    async handleLocationSelect(e) {
        const value = e.detail.value;
       //this.searchObject.lcoation = value;
        if (value) {
            const locationDetails = await this.getLocationDetails(value.place_id);
            if (locationDetails) {
                //console.log(locationDetails)
                const locationType = locationDetails.result.types[0];
                
                if (!['administrative_area_level_1', 'country'].includes(locationType)) {                
                    this.disableRadius = false;
                    const {address_components, geometry} = locationDetails.result
                    this.searchObject.lcoation = {address_components, geometry}
                    this.searchObject.radius = this.template.querySelector('.slds-select.radius').value;
                } else {
                    this.searchObject.lcoation = locationDetails.result.formatted_address
                }
            }
        }
    }
    async getLocationDetails(placeId) {
        try {
            const locationDetails = await getPlaceDetails({placeId});
            const parsedLocationDetails = JSON.parse(locationDetails);
            if (parsedLocationDetails && parsedLocationDetails.status === 'OK') {
                return parsedLocationDetails;
            }
            return null            
        } catch(err) {
            console.log(err)
            return null
        }
    }
    reset() {
        const inputs = this.template.querySelectorAll('.search input');
        inputs.forEach(input => {
            input.value = ''
        });
        this.template.querySelector('c-lwc-google-location-lookup').value = '';
        this.template.querySelector('.slds-select.radius').value = 30;
        this.disableRadius = true;
        this.searchObject = {};
        this.tableData = [];
        this.pills = [];
    }
    search() {
        //console.log('this.searchObject--------------------'+ JSON.parse(JSON.stringify(this.searchObject)));
		var locality ;
		var administrative_area_level_3;
		var administrative_area_level_2 ;
		var administrative_area_level_1 ;
		var country ;
		var postal_code ;
		var radd;
		var radUnit;
		var geo;
		var adrs;
		var lati;
		var longi;
        // Log info to New Relic // S-185932 Added by Rajib Maity
        connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch', Method: 'search', Module: 'Ref Search', AdditionalData : {action: 'Search'} });
		if(!(this.searchObject.lcoation)){
			//console.log('this.address--------------------Blank');	
			locality = '';
			administrative_area_level_3 = '';
			administrative_area_level_2 = '';
			administrative_area_level_1 = '';
			country = '';
			postal_code = '';
			radd = '';
			radUnit = '';
			geo = '';
			adrs = '';
			lati ='';
			longi ='';
		}else{

			radd = this.searchObject.radius //JSON.parse(JSON.stringify(this.searchObject.radius));

			radUnit = this.radiusUnit // JSON.parse(JSON.stringify(this.radiusUnit));
			geo = this.searchObject.lcoation.geometry // JSON.parse(JSON.stringify(this.searchObject.lcoation.geometry));
			adrs = this.searchObject.lcoation.address_components // JSON.parse(JSON.stringify(this.searchObject.lcoation.address_components));

			lati = geo ? JSON.stringify(geo.location.lat) : null;
			longi = geo ? JSON.stringify(geo.location.lng) : null;
		
		if(adrs && adrs[0].types[0].includes('locality')){
			locality = adrs[0].long_name;			
        } else {
            locality = this.searchObject.lcoation;
        }
        if (adrs) {
            for (var i = 0; i < adrs.length; i++){

            if(adrs[i].types[0].includes('locality')){
                locality = adrs[i].long_name;

            }
            if(adrs[i].types[0].includes('administrative_area_level_3')){
                administrative_area_level_3 = adrs[i].long_name;

            }
            if(adrs[i].types[0].includes('administrative_area_level_2')){
                administrative_area_level_2 = adrs[i].long_name;

            }
            if(adrs[i].types[0].includes('administrative_area_level_1')){
                administrative_area_level_1 = adrs[i].long_name;

            }
            if(adrs[i].types[0].includes('country')){
                country = adrs[i].long_name;

            }
            if(adrs[i].types[0].includes('postal_code')){
                postal_code = adrs[i].long_name;

            }
            }
        }
        
		}
		
		
		/*console.log('this.locality-------------------'+JSON.stringify(locality));
		console.log('this.administrative_area_level_3-------------------'+JSON.stringify(administrative_area_level_3));
		console.log('this.administrative_area_level_2--------------------'+JSON.stringify(administrative_area_level_2));
		console.log('this.administrative_area_level_1------------------'+JSON.stringify(administrative_area_level_1));
		console.log('this.country-------------------'+JSON.stringify(country));
		console.log('this.postalCode-------------------'+JSON.stringify(postal_code));*/
		
		const localityKey = locality ;
		const administrative_area_level_3Key = administrative_area_level_3;
		const administrative_area_level_2Key = administrative_area_level_2;
		const administrative_area_level_1Key = administrative_area_level_1;
		const countryKey = country;
		const postal_codeKey = postal_code ;
		const radiusKey = radd;
        const refNameKey = this.searchObject.refName;
		const refCompanyKey = this.searchObject.refCompany;
		const refTitleKey = this.searchObject.refTitle;
		const radUnitKey = radUnit;
		const candidateTitleKey = this.searchObject.candidateTitle;
		const latKey = lati;
		const lngKey = longi;
        if (refNameKey || refCompanyKey || refTitleKey ||  candidateTitleKey || localityKey) { //refNameKey || refCompanyKey || refTitleKey ||  candidateTitleKey || latKey || lngKey || localityKey || postal_codeKey
            this.showSpinner = true;
            fetchAcc({refName: refNameKey,refCompany: refCompanyKey,refTitle: refTitleKey,pocode: postal_codeKey,cntry: countryKey,level_1: administrative_area_level_1Key,level_2: administrative_area_level_2Key,local: localityKey,level_3 : administrative_area_level_3Key,lat: latKey,lng: lngKey,candidateTitle: candidateTitleKey,radius: radiusKey,radiUnit: radUnitKey}).then(res => {
                this.showSpinner = false;
                
                let rows = [];
                if (res.length) {
                    this.noResults = false;
                    res.forEach(row => {
                        let tableRow = {id: row.Id, cells: []};
    
                        TABLE_COLS.forEach(col => {
                            if (col.label === 'Phone') {
                                tableRow.cells.push({value: '', innerHTML: this.getPhoneHTML(row)})
                            } else if (col.label === 'Email') {
                                tableRow.cells.push({value: '', innerHTML: this.getEmailHTML(row)})
                            } else if (col.label === 'Last Reference') {
                                let value = this.objPath(row, col.field) || '';
                                tableRow.cells.push({value, date: true})
                            } else if (col.label === 'Name') {
                                let value = this.objPath(row, col.field) || '';
                                let id = this.objPath(row, 'Recommendation_From__c') || '';
                                if (value && id) {
                                    tableRow.cells.push({
                                        value,
                                        navItem: {object: 'Contact', id}
                                    })
                                } else {
                                    tableRow.cells.push({value})
                                }
                            } else if (col.label === 'Company') {
                                let value = this.objPath(row, col.field) || '';
                                let id = this.objPath(row, 'Recommendation_ID__r.Client_Account__c') || '';
                                if (value && id) {
                                    tableRow.cells.push({
                                        value,
                                        navItem: {object: 'Contact', id}
                                    })
                                } else {
                                    tableRow.cells.push({value})
                                }
                            }
                            else {
                                let value = this.objPath(row, col.field) || ''; 
                                tableRow.cells.push({value})
                            }
                        })
                        rows.push(tableRow);
                    })
                    this.tableData = rows;
                } else {
                    this.noResults = true;
                }
            }).catch(err => {
                this.showSpinner = false;
                console.log(err)
            })
        }
    }
    showDrawer() {
        this.template.querySelector('c-lwc-drawer').openDrawer();
    }
    getPrioratizedPhoneFields(row) {
        return PHONE_REF_FIELDS.map((refField, index) => {
            return [refField[0], this.objPath(row, refField[1]) || this.objPath(row, PHONE_TALENT_FIELDS[index][1])]
        })
    }
    getPrioratizedEmailFields(row) {
        return EMAIL_TALENT_FIELDS.map((talentField, index) => {
            if (EMAIL_REF_FIELDS[index]) {
                return [this.objPath(row, EMAIL_REF_FIELDS[index])]
            }
            return [this.objPath(row, talentField)]
        })
    }
    getPhoneHTML(row) {
        const prioratizedFields = this.getPrioratizedPhoneFields(row)

        let innerHTML = '';
        prioratizedFields.forEach(item => {
           innerHTML = innerHTML + this.phoneInnerHTML(item[0], item[1]);
        })
        innerHTML = innerHTML + this.phoneInnerHTML('', (row.Recommendation_ID__r || {}).Phone);
        return innerHTML;
    }
    getEmailHTML(row) {
        // let innerHTML = this.emailInnerHTML((row.Recommendation_ID__r || {}).Email__c);
        // innerHTML = innerHTML + this.emailInnerHTML((row.Recommendation_ID__r || {}).Email);
        // return innerHTML;
        const prioritizedFields = this.getPrioratizedEmailFields(row)
        return prioritizedFields.reduce((acc, curVal) => {
            const email = curVal
            if (email) {
                acc += this.emailInnerHTML(email);
            }
            return acc
        },'')
    }
    phoneInnerHTML(type, phone) {
        return phone?`<div class="slds-truncate" title="${phone}">(${type}) <a href="tel:${phone}">${phone}</a></div>`:''
    }
    emailInnerHTML(email) {
        return email?`<div class="slds-truncate"><a title="${email}" href="mailto:${email}">${email}</a></div>`:''
    }
    objPath(obj, path) {
        const arrPath = path.split('.');
        return arrPath.reduce((o,i, index) => {            
            if (o[i]) {
                return o[i]
            }
            if (arrPath.length-1 === index) {                
                return null
            }
            return {}
        }, obj) 
        //return path.split('.').reduce((o,i)=>o[i] || {}, obj)
    }
    handleTableAction(e) {
        // Log info to New Relic // S-185932 Added by Rajib Maity
        connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch/results', RecordId: e.detail.rowId, Method: 'handleTableAction', Module: 'Ref Search', AdditionalData : {action: 'view talent'} });
        this.template.querySelector('c-lwc-ref-details').getDetails(e.detail.rowId);
        this.template.querySelector('c-lwc-drawer').openDrawer();
    }
    // S-197083 Added by Rajib Maity
    handleLinkClickAction(e){
        console.log('------ref search-------');
        console.log(e.detail);
        let columnApiName = e.detail.fieldApiName;
        if(columnApiName == 'Full_Name__c'){
            connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch/results',  Method: 'handleLinkClickAction', Module: 'Ref Search',  RecordId:e.detail.recordId, AdditionalData : {action: 'click reference'} });
        }
        if(columnApiName == 'Organization_Name__c'){
            connectedLogAPI.logInfo(this.template,{LogTypeId: 'refsearch/results',  Method: 'handleLinkClickAction', Module: 'Ref Search', RecordId:e.detail.recordId, AdditionalData : {action: 'click company'} });
        }
    }
}