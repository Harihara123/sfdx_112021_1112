import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import initOppDetails from '@salesforce/apex/ReqHeader.initOppDetails';
import toggleFollow from '@salesforce/apex/ReqHeader.followRecord';

import canViewWinProbability from '@salesforce/customPermission/Win_Probability';

const BUTTONS = [
    {label: 'Print', value: 'print'},
	{label: 'Clone', value: 'clone'},
    {label: 'Link or Submit Specific Talent', value: 'linkOrSubmit'},
    
]
//Jason
const BUTTONS_TEK = [
    {label: 'Copy/Print', value: 'print'},
	{label: 'Clone', value: 'clone'},
    {label: 'Link or Submit Specific Talent', value: 'linkOrSubmit'},
    
]

const BUTTONS_AEROTEK = [
    {label: 'Copy/Print Req', value: 'print'},
    {label: 'Clone', value: 'clone'},
    {label: 'Add Talent', title: true, value: 'Link or Submit Specific Talent'},
    
]

const BUTTONS_ALT = [
    //{label: 'Sync to DRZ', value: 'syncToDrz'},
    {label: 'Add Self to Team', value: 'addToTeam'},
    // {label: 'Create Posting', value: 'createPost'}
]

const BUTTONS_ALT_TEK = [
    {label: 'Add Self to Team', value: 'addToTeam'},
    {label: 'Create Posting', value: 'createPost'},
	//{label: 'Sync to DRZ', value: 'syncToDrz'}
]

const QUICK_ACTION_MAP = {
    'Print': 'Opportunity.PrintOption',
	'Copy/Print': 'Opportunity.PrintOption',//Jason
    'Copy/Print Req': 'Opportunity.PrintOption',
    'Clone': 'Opportunity.Clone_Opportunity',// 'Sync to DRZ': 'Opportunity.Sync_to_DRZ',
    'Link or Submit Specific Talent': 'Opportunity.Link_to_Specific_Talent',
    'Add Talent': 'Opportunity.Link_to_Specific_Talent',
    'Add Self to Team': 'Opportunity.Add_Self_to_Team',
    'Create Posting': 'Opportunity.Create_Posting'    
}

const FIELDS_SCHEMA = [
    {label: 'Account Name', field: 'Account.Name', object: 'Account',Id: 'Account.Id', nav: true},
    {label: 'Hiring Manager', field: 'Req_Hiring_Manager__r.Name', object: 'Contact', Id: 'Req_Hiring_Manager__r.Id', nav: true},
    {label: 'Opportunity Owner', field: 'Owner.Name', object: 'User', Id: 'Owner.Id', nav: true},
    {label: 'Office', field: 'Organization_Office__r.Name', object: 'User_Organization__c', Id:'Organization_Office__r.Id', nav: true},
    {label: 'Open Positions', field: 'Req_Open_Positions__c'},
    {label: 'Stage', field: 'StageName'},
    {label: 'Req Grade', field: 'Req_Grade__c'},
    {label: 'Status', field: 'Req_Days_Open__c'},
    {label: 'Submittals', field: 'submittals'}
]

const FIELDS_SCHEMA_TEK = [
    {label: 'Account Name', field: 'Account.Name', object: 'Account',Id: 'Account.Id', nav: true},
    {label: 'Placement Type', field: 'Req_Product__c'},
    {label: 'Duration', field: 'Req_Duration__c'},
    {label: 'Bill Rate Max', field: 'Req_Bill_Rate_Max__c'},
    {label: 'Pay Rate Max', field: 'Req_Pay_Rate_Max__c'},
    {label: 'Skill Speciality', field: 'Req_Skill_Specialty__c'},
    {label: 'Main Skill', field: 'Legacy_Product__r.Name', object: 'Product', Id: 'Legacy_Product__r.Id'},
    {label: 'Open Positions', field: 'Req_Open_Positions__c'},
    {label: 'Qualifying Stage', field: 'Req_Qualifying_Stage__c'},
    {label: 'Status', field: 'Req_Days_Open__c'},   
    {label: 'Submittals', field: 'submittals'},
    {label: 'Win Probability', field: 'ED_Outcome__c', colorCode: true}
]

const FIELDS_SCHEMA_AEROTEK = [
    {label: 'Status', field: 'Req_Days_Open__c'},
    {label: 'Qualifying Stage', field: 'Req_Qualifying_Stage__c'},
    {label: 'Days To Start', field: 'Start_Date__c'},
    {label: 'Placement Type', field: 'Req_Product__c'},
    {label: 'Exclusive', field: 'Req_Exclusive__c'},
    {label: 'Go To Work', field: 'Immediate_Start__c'},
    {label: 'Subs', field: 'submittals'},
    {label: 'Interviewing', field: 'Req_Sub_Interviewing__c'},
    {label: 'Open Positions', field: 'Req_Open_Positions__c'},
    {label: 'Total Positions', field: 'Req_Total_Positions__c'},
  	{label: 'Win Probability', field: 'ED_Outcome__c', colorCode: true}
]

export default class LwcReqHeader extends NavigationMixin(LightningElement) {
    @api Id;
	isAerotek = false;
	@api opco;
	buttons = BUTTONS
    buttonsAlt = (this.opco == 'AG_EMEA' ? BUTTONS_ALT : BUTTONS_ALT_TEK);
    Opp
    fields
    OppName = '';
    infoFields = [];    
    following = null;
    details = {};
    headerClass = "sticky";    
    colorRed = false;
    colorYellow = false;
    colorGreen = false;    

   @api connectedCallback() {
		this.getSplitView();
        this.initOpp(this.Id);
        if(this.opco == 'Aerotek, Inc' ) {
			this.isAerotek = true;
            this.buttons = BUTTONS_AEROTEK;
        }
		//Jason
		else if(this.opco == 'TEKsystems, Inc.') {
		this.buttons = BUTTONS_TEK;		
		}
    }
    async initOpp(recordId) {
        try {
			this.getSplitView();
            const opp = await initOppDetails({recordId});            
            const parsedData = JSON.parse(opp);
            if (parsedData && parsedData.details) {
                parsedData.details.submittals = parsedData.submittals;

                this.infoFields = await this.formatFields(parsedData);
                this.OppName = parsedData.details.Name;
                this.setfollowState(parsedData.following);                
            }
            this.Opp = parsedData;            
        } catch(err) {
            console.log(err)
        }
    }
    async follow(state) {
        try {
            const res = await toggleFollow({recordId: this.Id, isFollowed: state, OnLoad: false});
            this.setfollowState(res);
        } catch (err) {
            console.log(err)
        }
    }

    handleHeaderToggle() {
        const toggle = {
            sticky: 'not',
            not: 'sticky'
        }
        this.headerClass = toggle[this.headerClass];
    }
    async formatFields(data) {
        let  fieldsSchema;
        if(this.opco == 'AG_EMEA') {
            fieldsSchema = FIELDS_SCHEMA;
        }
        else if(this.opco == 'TEKsystems, Inc.') {
            fieldsSchema = FIELDS_SCHEMA_TEK;          
       
        }
        else if(this.opco == 'Aerotek, Inc') {
            fieldsSchema = FIELDS_SCHEMA_AEROTEK;            
        }
        return Promise.all(fieldsSchema.map(async item => {           
            let formattedObj = {}

            if (item.field === 'Req_Days_Open__c') {
                if(this.isAerotek) {
                    if (!data.details.IsClosed) {
                        if(data.details[item.field] > 0) {
                            formattedObj.value = 'Open, ' + data.details[item.field] + ' Days';    
                        }
                        else if(data.details[item.field] == 0) {
                            formattedObj.value = 'TODAY';
                        }                        
                    } else {
                        formattedObj.value = 'Closed';
                    }
                }
                else {
                    if (!data.details.IsClosed) {
                        formattedObj.value = `Open for ${data.details[item.field] > -1 ? data.details[item.field] : '-'} days`;
                    } else {
                        formattedObj.value = 'Closed';
                    }
                }                
            } else if (item.field === 'Req_Open_Positions__c' ) {
                formattedObj.value = (data.details[item.field] > 0 ? data.details[item.field] : 0);
            } else if (item.field === 'Req_Duration__c' ) {
                formattedObj.value = this.replaceUndefined(data.details[item.field]) + ' '+this.replaceUndefined(data.details['Req_Duration_Unit__c']);
            } else if (item.field === 'Start_Date__c') {                                   
                let numOfDays = (new Date(data.details[item.field]) - new Date(Date.now())) / (1000*60*60*24);
                if(!data.details[item.field]) {
                    formattedObj.value = '-';
                }
                else if(Math.round(numOfDays) == 0) {                    
                    formattedObj.value = 'TODAY';
                }
                else if(Math.round(numOfDays) > 0) {
                    formattedObj.value = parseInt(numOfDays) + 1;
                }
                else {
                    formattedObj.value =  -parseInt(numOfDays) + '-PAST';
                }
            } else if (item.field === 'Req_Exclusive__c') {                
                if( data.details[item.field]) {
                    formattedObj.value = 'Yes';
                }  
                else {
                    formattedObj.value = '-';
                }
            } else if(item.field === 'Immediate_Start__c') {                
                if( data.details[item.field]) {
                    formattedObj.value = 'Yes';
                }  
                else {
                    formattedObj.value = '-';
                }
			} else if(item.field === 'ED_Outcome__c') {                
                if( data.details[item.field] && canViewWinProbability) {
                    formattedObj.value = (Math.round(data.details[item.field] * 10))/10;   
                                  
                    if(formattedObj.value <= 25) {
                        this.colorRed = true;
                    }
                    else if(formattedObj.value > 25 && formattedObj.value <= 75) {
                         this.colorYellow = true;
                    }
                    else {
                        this.colorGreen = true;
                    }
                }  
                 else {
                    formattedObj.value = "-";
                    item.colorCode = false;
                }
            }
            else {
                formattedObj.value = data.details[item.field] || this.traverseDetails(item.field, data.details) || '-';            
            }
            if (item.nav) {
                const id = data.details[item.Id] || this.traverseDetails(item.Id, data.details);
                formattedObj.url = await this.navUrl(id, item.object);
            }

            return {...formattedObj, ...item}
        }))       

    }
    traverseDetails(path, details) {
        return path.split('.').reduce((o,i) => o[i], details);
    }
    setfollowState(state) {
        if (state) {
            this.following = 'slds-button slds-button_neutral slds-button_dual-stateful slds-is-pressed';
        } else {
            this.following = 'slds-button slds-button_neutral slds-button_dual-stateful'
        }
		if(this.Opp){
			this.Opp.following = state;
		}
    }
    handleQuickAction(e) {        
        const action = e.target.label;
        if (action && QUICK_ACTION_MAP[action]) {
            this.dispatchEvent(new CustomEvent('quickaction', {detail: {action: QUICK_ACTION_MAP[action]}}));
        }
    }
    handleOppStatusChange(e) {
        this.dispatchEvent(new CustomEvent('oppstatuschange'));
    }
    handleFollow(e) {        
        const btn = e.currentTarget;
        btn.classList.toggle('slds-is-pressed')        
        this.follow(this.Opp.following);
    }
    // handleNav(e) {
    //     e.preventDefault();
    //     const navObj = e.currentTarget.getAttribute('data-nav');
    //     const Id = e.currentTarget.getAttribute('data-id');
    //     const recordId = this.Opp.details[Id] || this.traverseDetails(Id, this.Opp.details)

    //     this.navUrl(recordId, navObj);
    // }
    async navUrl(id, object,callback = () => {}) {
        const navData = {
            type: "standard__recordPage",
            attributes: {
                "recordId": id,
                "objectApiName": object,
                "actionName": "view"
            }
        };
        try {
            const url = await this[NavigationMixin.GenerateUrl](navData);
            return url           
            
        } catch(err) {
            console.log(err)
        }
    }
	getSplitView() {
        if (window.location.href.indexOf("view?sv=") > -1) {
            this.headerClass = "nonSticky";			
        } else {
            this.headerClass = "sticky";
        }
    }

	replaceUndefined(vl){
	 return  (typeof(vl) === "undefined")?  "": vl;
	}
    
}