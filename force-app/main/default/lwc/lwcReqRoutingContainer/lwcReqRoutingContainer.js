import { LightningElement,wire,track } from "lwc";
import messageChannel from '@salesforce/messageChannel/handleDropDowns__c';
import {publish, MessageContext} from 'lightning/messageService'

export default class LwcReqRoutingContainer extends LightningElement {
    selectedTab;
    reqSearchResults = [];

    @wire(MessageContext)
    messageContext;


    @track dashboardSelected = false;
    @track deliveryCenterReqsSelected = false;
    @track allocatedReqsSelected = false;
    @track savedReqsSelected = false;
    @track sharedReqsSelected = false;
    @track hiddenReqsSelected = false;

    totalrecordscount;
    @track recordsPerPage;
    @track startIndex;

    isRendered = false;

    renderedCallback() {
        this.isRendered = true;
    }

    tabselected(event) {
        this.dashboardSelected = false;
        this.deliveryCenterReqsSelected = false;
        this.allocatedReqsSelected = false;
        this.savedReqsSelected = false;
        this.sharedReqsSelected = false;
        this.hiddenReqsSelected = false;

        // Change tab
        this[event.detail.selected_tab + "Selected"] = true;
        this.selectedTab = event.detail.selected_tab;

        // Reset results for new tab search
        this.reqSearchResults = [];

        // Don't do this stuff the first time it's loaded
        if (!event.detail.is_first) {
            // Reset filters on tab change
            window.history.replaceState(null, "Req Routing", "/lightning/n/Req_Routing");

            // Start async code to wait for render
            setTimeout(function(){
                while (!this.isRendered) {}

                this.template.querySelector("c-lwc-req-routing-filters").updateOnTabChange();
            }.bind(this), 0);
        }
    }

    updateResults(event) {
        this.reqSearchResults = [];
        this.reqSearchResults = event.detail;
    }

    fetchrecordcount(event) {
        //console.log("eventrecordcount" + event.detail.totalResults);
        this.totalrecordscount = event.detail.totalResults;
        this.recordsPerPage = event.detail.recordsPerPage;
        this.startIndex = event.detail.startIndex;
    }
    handledropdown() {
        const payload = { facetData: 'lwcReqRoutingContainer' };
        publish(this.messageContext, messageChannel, payload);

    }
}