import { LightningElement, api } from 'lwc';

export default class LwcReqRoutingFilter extends LightningElement {
    @api facetData;
    
    lwcFacetCheckboxTypeahead = false;
    lwcFacetLocation = false;
    lwcFacetSelect = false;

    connectedCallback() {
        this[this.facetData.component] = true;
    }

    @api
    closeAllTypeaheads(openedFacet) {
        let facetCheckboxFilter = this.template.querySelector('c-lwc-facet-checkbox-filter');
        if (facetCheckboxFilter) facetCheckboxFilter.handleOtherFacetOpened(openedFacet);

        let facetLocation = this.template.querySelector('c-lwc-facet-location');
        if (facetLocation) facetLocation.handleOtherFacetOpened(openedFacet);

        let facetSelect = this.template.querySelector('c-lwc-facet-select');
        if (facetSelect) facetSelect.handleOtherFacetOpened(openedFacet);
    }
}