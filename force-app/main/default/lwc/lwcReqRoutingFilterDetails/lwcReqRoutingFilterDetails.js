import { LightningElement, api, wire, track } from "lwc";
import currentUserId from "@salesforce/user/Id";
import messageChannel from "@salesforce/messageChannel/ReqRoutingMessageChannel__c";
import FiltersMessageChannel from "@salesforce/messageChannel/ReqRoutingFiltersMessageChannel__c";
import { publish, subscribe, MessageContext } from "lightning/messageService";
import messageChannelDropDowns from '@salesforce/messageChannel/handleDropDowns__c';

import getReqsFromFacets from "@salesforce/apex/ReqRoutingController.getReqsFromFacets";
import getOpportunityDetails from "@salesforce/apex/ReqRoutingController.getOpportunityDetails";
import getDefaultFilters from "@salesforce/apex/ReqRoutingController.getDefaultFilters";
import getUserFilterCriteria from "@salesforce/apex/ReqRoutingController.getUserFilterCriteria";
import getOppRecruiters from "@salesforce/apex/ReqRoutingController.getOppRecruiters";
import getSupportRequests from "@salesforce/apex/ReqRoutingController.getSupportRequests";
import upsertCachedSearch from "@salesforce/apex/ReqRoutingController.upsertCachedSearch";
import getCachedSearch from "@salesforce/apex/ReqRoutingController.getCachedSearch";
import getPastSharedReqUsers from "@salesforce/apex/ReqRoutingController.getPastSharedReqUsers";
import getSavedReqsStatus from "@salesforce/apex/ReqRoutingController.getSavedReqsStatus";

export default class LwcReqRoutingFilterDetails extends LightningElement {
    reqs = [];

    _filters = [];

    get filters() {
        return this._filters;
    };
    set filters(value) {
        this._filters = value;
        this.saveCache();
    };

    @track initialFilters = [];
    @track additionalFilters = [];
    @track clearFilters = [];
    @track allocatedRecruiters;
    @track deliveryCenters;
    @track sharedReqs;

    @api view = "dashboard";

    @api filterCount;

    @api filterValue;
    faultString = "";
    isSearching = false;
    showFiltersModal = false;

    sortFilter = "job_create_date:desc";

    isAllocation = false;
    isExclusive = false;
    isRemote = false;
    isVMS = false;
    isSubVendor = false;

    showing = "20";
    totalResults = "";
    showSummary = false;
    @api filtercountchanged;
    userSelectedOption;

    trackingGuid = "";

    @api searchInputText = '';

    cacheKey = '';
    userHasSearched = false;

    isRendered = false;

    startindex = 0;
    recordsPerPage = 30;
    subscription = null;
  
    @wire(MessageContext) messageContext;

    connectedCallback() {
        this.setTrackingGuid();
        this.addEventListener("reqRoutingFilterChange", this.updateFilters);
        this.handleSubscribe();
    }

    renderedCallback() {
        this.isRendered = true;
    }

    handleSubscribe() {
        if (this.subscription) {
          return;
        }
    
        this.subscription = subscribe(
            this.messageContext,
            messageChannel,
            (message) => {
                //console.log("message@@" + JSON.stringify(message));
                window.scrollTo(0,0);
                this.startindex = message.startindex

                this.recordsPerPage = message.recordsPerPage ? message.recordsPerPage : this.recordsPerPage;

                this.updateResults(this.clearFilters); // This is being called from pagination, so use the existing filters
            }
        );
    }

    /**
     * Sets this.trackingGuid to a new GUID for API requests.
     */
    setTrackingGuid() {
        let guid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
            v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });

        this.trackingGuid = guid;
    }

    /**
     * Sets filters to values saved in either:
     *  1. clearAllFilters()
     *  2. updateResults()
     */
    resetFilters() {
        this.updateFiltersFromCancel();
        this.handleOnChange();
    }

    handleOnChange(event){
        let itemLabel = '';
        itemLabel = event?.currentTarget?.dataset?.value;
        const payload = { facetData: itemLabel};
        publish(this.messageContext, messageChannelDropDowns, payload);
    }

    /**
     * Sets filters to blank
     */
    clearAllFilters() {
        this.saveFilterForCancelAction();
        let tempFilters = JSON.parse(JSON.stringify(this.filters));

        tempFilters.forEach(function (item) {
            if (item.selected) {
                item.selectedValues = [];
            }
        });

        this.sortFilter = 'job_create_date:desc';

        const selectedEvent = new CustomEvent("clearallfilters");
        this.dispatchEvent(selectedEvent);

        this.filters = tempFilters;
        this.setDefaultParametersFromTab();
        this.handleOnChange();
    }

    filterOpened() {}

    /**
     * Click handler when a filter is clicked.
     * Shows the 'Save View' button.
     * 
     * @param {html event} event 
     */
    updateFilters(event) {
        this.filterValue = event.detail.value;
        if (this.filtervalue === "None") this.filtercountchanged = false;
        else this.filtercountchanged = true;
        //this.filtercountchanged = this.filters.length != this.initialFilters.length ? false : true;
        const selectedEvent = new CustomEvent("filtervaluechange", {
            detail: this.filtercountchanged
        });
        this.dispatchEvent(selectedEvent);
    }

    /**
     * Called from lwcReqRoutingFilters when user saves a view
     * 
     * @param {string} selectedOption 
     */
    @api
    updateSelectedFilter(selectedOption) {
        this.userSelectedOption = selectedOption;
        this.saveCache();
    }

    /**
     * Gets user filters parameters FROM getUserFilterCriteria 
     * */
    @api
    filtersByUser(selectedOption) {
        let context = this;
        this.userSelectedOption = selectedOption;

        this.startindex = 0;

        getUserFilterCriteria({ selectedOption: this.userSelectedOption }).then((result) => {
            let userFacetData = JSON.parse(result);
            if (userFacetData) {
                this.filters = userFacetData.FilterCriteria;
                this.initialFilters = userFacetData.FilterCriteria;

                if (this.userSelectedOption == 'Default View') {
                    this.setDefaultParametersFromTab();
                    this.dispatchEvent(new CustomEvent("savedsearchloaded", {detail: {}}));
                }
                else
                    this.dispatchEvent(new CustomEvent("savedsearchloaded", {detail: {search_text:userFacetData.search_text, sort:userFacetData.sort}}));

                let message = { filtersApplied: true };
                publish(this.messageContext, FiltersMessageChannel, message);
            }
        }).catch((error) => {
            context.faultString = error;
            context.isSearching = false;
        });
    }

    /**
     * Gets the default parameters
     * FROM Req_Routing_Search_Criteria__c
     * WHERE Search_Name__c = 'DefaultSearchCriteria'
     */
    @wire(getDefaultFilters)
    getDefaultFiltersWired({ error, data }) {
        if (error) {
            console.log("error: ", error);
        }
        if (data) {
            let facetData = JSON.parse(data);

            const urlParams = new URLSearchParams(window.location.search);
            const myParam = urlParams.get('c__cacheKey');

            if (myParam != null) {
                this.cacheKey = myParam;
                this.getCache();
            }
            else {
                this.filters = facetData.FilterCriteria;
                this.initialFilters = facetData.FilterCriteria;

                // Fire event to save default
                const selectedEvent = new CustomEvent('facetitemselected');
                this.dispatchEvent(selectedEvent);

                if (this.view == 'dashboard') {
                    this.filters.forEach(filter => {
                        if (filter.fieldSlug == 'isAllocation') {
                            filter.selectedValues = ['[0,0]'];
                        }
                    });
                }
            }
            
            this.saveFilterForCancelAction();
        }
    }

    /**
     * Called from lwcReqRoutingFilters when sort is changed.
     * 
     * @param {string} sortFilter sort field with direction. (ex: job_create_date:desc)
     */
    @api
    updateSorting(sortFilter) {
        this.startindex = 0;

        this.sortFilter = sortFilter;
        this.updateResults();
    }

    @api
    updateFromSearchText() {
        this.startindex = 0;

        this.updateResults();
    }

    sendUpdatedPageData() {
        this.dispatchEvent(
            new CustomEvent("totalrecordscounts", {
                bubbles: true, 
                composed: true, 
                detail: {
                    totalResults:this.totalResults,
                    recordsPerPage:this.recordsPerPage,
                    startIndex:this.startindex
                }
            })
        );
    }

    handleApply() {
        this.startindex = 0;
        this.sendUpdatedPageData();

        this.updateResults();

        // Send message to pagination
        let message = { filtersApplied: true };
        publish(this.messageContext, FiltersMessageChannel, message);
    }

    @api
    updateResultsFromTabChange(reExecuteSearch) {
        let context = this;
        getDefaultFilters().then((result) => {
            let jsonData = JSON.parse(result);
            context.filters = jsonData.FilterCriteria;
            context.initialFilters = jsonData.FilterCriteria;

            this.setDefaultParametersFromTab();

            context.updateResults();
        }).catch((error) => {

        });
    }
    
    /**
     * Set default search criteria based on which tab you're on
     */
    setDefaultParametersFromTab() {
        this.filters.forEach(filter => {
            // Dashboard: set Allocation to 'Reqs without Allocation'
            if (this.view == 'dashboard') {
                if (filter.fieldSlug == 'isAllocation') {
                    filter.selectedValues = ['[0,0]'];
                }
            }

            // Allocated Reqs: set Allocation to 'Reqs with Allocation'
            if (this.view == 'allocatedReqs') {
                if (filter.fieldSlug == 'isAllocation') {
                    filter.selectedValues = ['[1,100]'];
                }
            }
        });
    }

    /**
     * Performs faceted req search
    */
    @api
    updateResults(filterOverride) {
        this.isSearching = true;
        this.userHasSearched = true;
        
        let query = "-ljsdflkjlkjasdf";
        if (this.searchInputText !== "" && this.searchInputText !== undefined) {
            query = this.searchInputText;
        }

        let localFilters = {};
        if (filterOverride) {
            localFilters = JSON.parse(JSON.stringify(filterOverride)); // Create deep copy
            query = (this.savedInput != '' && this.savedInput != undefined) ? this.savedInput : '-ljsdflkjlkjasdf';
        }
        else {
            this.saveFilterForCancelAction();
            localFilters = JSON.parse(JSON.stringify(this.filters)); // Create deep copy
        }
        
        this.closeAllTypeaheads({ event: { detail: "" } });
        this.saveCache();

        if (this.view == "allocatedReqs") {
            localFilters.push({reqSearchParams: ["flt.allocated_recruiter_creator_ids"], selectedValues: [currentUserId], selected: true});
        }
        if (this.view == "deliveryCenterReqs") {
            localFilters.push({reqSearchParams: ["flt.has_delivery_center_support_request"], selectedValues: [true], selected: true});
        }
        if (this.view == "sharedReqs") {
            localFilters.push({reqSearchParams: ["flt.req_router_shared_user_ids"], selectedValues: [currentUserId], selected: true});
        }
        if (this.view == "savedReqs") {
            localFilters.push({reqSearchParams: ["flt.req_router_saved_user_ids"], selectedValues: [currentUserId], selected: true});
        }

        // Convert certain fields to API format
        localFilters.forEach(function (item) {
            // Builds location into correct array
            if (item.fieldSlug == "req_location") {
                let localSelectedValues = [];
                item.selectedValues.forEach(function (location) {
                    localSelectedValues.push(location.data);
                });

                item.selectedValues = [JSON.stringify(localSelectedValues)];
            }
        });

        let context = this;
        let jsonString = '{"FilterCriteria":' + JSON.stringify(localFilters) + "}";
        let sortFilter = encodeURI(this.sortFilter);

        let now = new Date();
        let secondsSinceEpoch = Math.round(now.getTime() / 1000);

        getReqsFromFacets({
            query: query,
            size: this.recordsPerPage,
            fromIndex: this.startindex,
            filterJson: jsonString,
            sortFilter: sortFilter,
            trackingGuid: this.trackingGuid,
            cacheBreaker: secondsSinceEpoch
        }).then((result) => {
            //console.log("@@result" + JSON.stringify(JSON.parse(result)));
            let opportunityIds = [];
            let jsonData = JSON.parse(result);

            if (jsonData.fault != undefined) {
                context.faultString = jsonData.fault.faultstring;
                this.isSearching = false;
            } else {
                context.reqs = [];
                context.showing = jsonData.response.hits.hits.length;
                context.totalResults = jsonData.response.hits.total;

                jsonData.response.hits.hits.forEach(function (req) {
                    let jsonReq = context.fillReq(req);

                    opportunityIds.push(req._source.job_id);

                    // Add to array
                    context.reqs.push(jsonReq);
                });

                context.getOpporunityDetailsSoql(opportunityIds);
            }
        }).catch((error) => {
            context.faultString = error;
            context.isSearching = false;
        });
    }

    /**
     * Gets remaining Opportunity details from Salesforce
     * opp_ids based on IDs from updateResults()
     */
    getOpporunityDetailsSoql(opp_ids) {
        let context = this;

        getOpportunityDetails({ listOfOppIds: opp_ids }).then((result) => {
            getOppRecruiters({ OppId: opp_ids }).then((users) => {
                //console.log("users@@@", users);
                if (users !== null) {
                    context.allocatedRecruiters = users;
                }
                getPastSharedReqUsers({ OppId: opp_ids }).then((sharedReqs) => {
                    //console.log("----------->sharedReqs" + JSON.parse(JSON.stringify(sharedReqs)));              
                    if (JSON.parse(JSON.stringify(sharedReqs)) !== null) {
                        context.sharedReqs = JSON.parse(JSON.stringify(sharedReqs));
                    }
                
                    getSupportRequests({ OppId: opp_ids }).then((supportRequests) => {
                        if (supportRequests !== null) {
                            context.deliveryCenters = supportRequests;
                        }
                        
                        getSavedReqsStatus({ listOfOppIds: opp_ids }).then((savedResult) => {
                            let savedReqsResponse = JSON.parse(JSON.stringify(savedResult));
                            let savedReqsArray = [];
                            savedReqsResponse.forEach(savedReq => {
                                savedReqsArray.push(savedReq.Opportunity__c);
                            });
                            context.savedReqs = savedReqsArray;

                            // Standardize output
                            context.fillReqFromOppys(result);

                            // Reset error
                            context.faultString = "";

                            // Set false to hide spinner
                            context.isSearching = false;

                            // Show summary
                            context.showSummary = true;

                            // Dispatch event for result list
                            context.dispatchEvent(
                                new CustomEvent("filtersearchexecuted", {
                                    bubbles: true,
                                    composed: true,
                                    detail: context.reqs
                                })
                            );

                            // Dispatch event for page change
                            context.sendUpdatedPageData();

                            // Scroll to top (D-18112)
                            window.scrollTo(0, 0);

                        }).catch((error) => {console.error(error)});
                    }).catch((error) => {console.error(error)});
                }).catch((error) => {console.error(error)});
            }).catch((error) => {console.error(error)});
        })
        .catch((error) => {
            console.error(error);

            // Set false to hide spinner
            context.isSearching = false;
        });
    }

    /**
     * 
     * @param {JSON} req data from API response
     * @returns {JSON} standardized object
     */
    fillReq(req) {
        let jsonReq = {};

        jsonReq.id = this.buildValue(req._source.job_id); // Opportunity ID
        jsonReq.opp_url = "/" + req._source.job_id; // Opportunity Link
        jsonReq.account_id = this.buildValue(req._source.organization_id); // Account ID
        jsonReq.account_id_url = "/" + this.buildValue(req._source.organization_id); // Account Link
        jsonReq.job_title = this.buildValue(req._source.position_title); // Job Title
        jsonReq.opp_num = this.buildValue(req._source.position_id); //Opportunity name (O-1234567)
        jsonReq.canuse_subvendor = this.buildValue(req._source.can_use_approved_sub_vendor); 
        jsonReq.canuse_subvendor = jsonReq.canuse_subvendor ===true ? 'Yes' : (jsonReq.canuse_subvendor === false ? 'No' : '--');
        jsonReq.location = this.buildLocation(
            req._source.position_street_name,
            "",
            req._source.city_name,
            req._source.country_sub_division_code,
            req._source.country_code,
            req._source.postal_code
        );

        jsonReq.req_skill_specialty = this.buildValue(req._source.req_skill_specialty); // Skill specialty
        jsonReq.created_date = this.buildValue(req._source.job_create_date); // Opportunity create date
        jsonReq.req_owner = this.buildValue(req._source.owner_id); // Req owner
        jsonReq.placement_type = this.buildValue(req._source.position_offering_type_code); // Placement type

        var winProbeScore = req._source.req_win_score;

        if (winProbeScore >= 70 && winProbeScore <= 100) {
            jsonReq.win_prob_score = "High";
        } else if (winProbeScore >= 30 && winProbeScore <= 70) {
            jsonReq.win_prob_score = "Medium";
        } else if (winProbeScore === 0 || (winProbeScore > 0 && winProbeScore <= 30)) {
            jsonReq.win_prob_score = "Low";
        } else {
            jsonReq.win_prob_score = "N/A";
        }

        jsonReq.req_stage = this.buildValue(req._source.req_stage); // Stage
        jsonReq.max_bill_rate = this.buildValue(req._source.remuneration_bill_rate_max); // Max bill rate
        jsonReq.max_pay_rate = this.buildValue(req._source.remuneration_pay_rate_max); // Max bill rate

        jsonReq.industry = "N/A"; // Industry
        jsonReq.req_skills = this.buildSkills(req._source.skills); // Skills
        jsonReq.req_skills_length = jsonReq.req_skills.length > 0 ? true : false;
        jsonReq.islocationAvailable = jsonReq.location ? true : false;
        jsonReq.isRemoteWork = req._source.work_remote;

        if (req._source.posting_requester?.role_name == 'Hiring Manager') {
            jsonReq.hiring_manager_url = "/" + req._source.posting_requester.person_id;
        };

        return jsonReq;
    }

    /**
     * Fills the reqData with more data from Salesforce
     * 
     * @param {Opportunity} opps Opportuniy data retrieved from Salesforce
     */
    fillReqFromOppys(opps) {
        let context = this;
        let finalReqList = [];
        let finalReqListIds = [];
        let reqData = this.reqs;

        reqData.forEach(function (reqItem) { // From API
            let foundOpp = false;
            opps.forEach(function (oppItem) { // From SFDC
                if (oppItem.Id == reqItem.id && !finalReqListIds.includes(oppItem.Id)) {
                    foundOpp = true;

                    reqItem.is_saved = context.savedReqs.includes(reqItem.id);
                    
                    if (oppItem.Account) {
                        reqItem.account_name = context.buildValue(oppItem.Account.Name);
                    }

                    reqItem.open_positions = context.buildValue(oppItem.Req_Open_Positions__c);
                    reqItem.total_positions = context.buildValue(oppItem.Req_Total_Positions__c);
                    reqItem.days_open = context.buildValue(oppItem.Req_Days_Open__c);
                    reqItem.linked = context.buildValue(oppItem.Req_Sub_Linked__c);
                    reqItem.submitted = context.buildValue(oppItem.Req_Sub_Submitted__c);
                    reqItem.interviewing = context.buildValue(oppItem.Req_Sub_Interviewing__c);
                    reqItem.offer_accepted = context.buildValue(oppItem.Req_Sub_Offer_Accepted__c);
                    reqItem.started = context.buildValue(oppItem.Req_Sub_Started__c);
                    reqItem.not_proceeding = context.buildValue(oppItem.Req_Sub_Not_Proceeding__c);
                    reqItem.is_vms = context.buildValue(oppItem.Req_VMS__c);
                    reqItem.is_exclusive = context.buildValue(oppItem.Req_Exclusive__c);
                    reqItem.ownerId = context.buildValue(oppItem.Owner.Id);
                    reqItem.ownerName = context.buildValue(oppItem.Owner.Name);
                    reqItem.ownerAlias = context.buildValue(oppItem.Owner.Alias);
                    reqItem.opco = context.buildValue(oppItem.OpCo__c);
                    reqItem.allocatedRecruiters = context.buildValue(context.allocatedRecruiters);
                    reqItem.deliveryCenters = context.buildValue(context.deliveryCenters);
              reqItem.sharedReqs = context.buildValue(context.sharedReqs);

                    console.log(reqItem);

                    finalReqListIds.push(reqItem.id);
                    finalReqList.push(reqItem);
                }
            });

            if (!foundOpp) {
                reqItem.account_name = "---";
                reqItem.open_positions = "---";
                reqItem.total_positions = "---";
                reqItem.days_open = 0;
                reqItem.linked = "---";
                reqItem.submitted = "---";
                reqItem.interviewing = "---";
                reqItem.offer_accepted = "---";
                reqItem.started = "---";
                reqItem.not_proceeding = "---";
                reqItem.is_vms = "---";
                reqItem.is_exclusive = "---";
                reqItem.ownerId = "---";
                reqItem.ownerName = "---";
                reqItem.ownerAlias = "---";
                finalReqListIds.push(reqItem.id);
                finalReqList.push(reqItem);
            }
        });

        this.reqs = finalReqList;
    }

    /**
     * Checks for all empty states
     */
    buildValue(val) {
        if (val == undefined || val === "" || val == null) {
            return "--";
        } else {
            return val;
        }
    }

    /**
     * Builds out location to readable format
     */
    buildLocation(street1, street2, city, state, country, postal) {
        let location = "";

        if (street1 != undefined && street1 != "") location += street1;
        if (street2 != undefined && street2 != "") location += ", " + street2;
        if (city != undefined && city != "") location += ", " + city;
        if (state != undefined && state != "") location += ", " + state;
        if (country != undefined && country != "") location += ", " + country;
        if (postal != undefined && postal != "") location += ", " + postal;

        return location;
    }

    buildSkills(skills) {
        let skillsArray = [];
        if (skills) {
            skills.forEach(function (skill) {
                skillsArray.push(skill.skill_name);
            });
        }
        return skillsArray;
    }

    /**
     * Triggered from facetitemselected event
     */
    updateFacets(event) {
        let facetItem = event.detail;
        this.filters.forEach(function (item) {
            if (item.fieldSlug == facetItem.fieldSlug) {
                item.selectedValues = facetItem.selectedValues;
            }
        });

        this.saveCache();
    }

    @api
    userSelectedFilters() {
        let userFilters = JSON.parse(JSON.stringify(this.filters));
        let jsonString = '{"FilterCriteria":' + JSON.stringify(userFilters) + "}";
        return jsonString;
    }

    /**
     * Add facets from filter json
     */
    toggleFilters(event) {
        this.showFiltersModal = !this.showFiltersModal;
        this.handleOnChange();
    }

    addFilters(event) {
        this.filters.forEach(function (item) {
            let fieldCheckbox = this.template.querySelectorAll("[data-fieldname='" + item.fieldSlug + "']")[0]; 
            if (fieldCheckbox != undefined) {
                item.selected = fieldCheckbox.checked;

                if (!item.selected) item.selectedValues = [];
            }
        }.bind(this));

        this.saveCache();
        this.toggleFilters();
    }

    @api
    closeAllTypeaheads(event) {
        let openedFacet = event.detail;
        let facetFilters = this.template.querySelectorAll("c-lwc-req-routing-filter");

        facetFilters.forEach(function (item) {
            item.closeAllTypeaheads(openedFacet);
        });
    }

    saveFilterForCancelAction() {
        this.savedView = this.userSelectedOption;
        this.savedInput = this.searchInputText;
        this.savedSort = this.sortFilter;
        
        this.clearFilters = JSON.parse(JSON.stringify(this.filters));
    }

    updateFiltersFromCancel() {
        const selectedEvent = new CustomEvent("cacheupdate", {
            detail: {
                sort: this.savedSort,
                search_text: this.savedInput
            }
        });
        this.dispatchEvent(selectedEvent);

        this.filters = JSON.parse(JSON.stringify(this.clearFilters));
    }

    @api
    updateFromCache(sort, search, view) {
        this.sortFilter = sort;
        this.searchInputText = search;
        this.userSelectedOption = view;
        this.saveCache();
    }

    @api
    saveCache() {
        if (this.cacheKey == '')
            this.cacheKey = new Date().getTime();

        let filterString = '{"FilterCriteria":' + JSON.stringify(this.filters) + "}";
        let tempJson = JSON.parse(filterString);

        // Add sorting for lookup later
        tempJson.sort = this.sortFilter;

        // Add search text for lookup later
        if (this.searchInputText) {
            tempJson.search_text = this.searchInputText;
        }

        // Add selected filter for lookup later
        if (this.userSelectedOption) {
            tempJson.selected_view = this.userSelectedOption;
        }

        // Add page information
        tempJson.pages = {from:this.startindex, size:this.recordsPerPage};

        // Add boolean on whether the user has searched on the dashboard or not
        tempJson.show_results = this.userHasSearched;

        // Save current tab in local storage
        sessionStorage.setItem('rr_selectedtab_' + this.cacheKey, this.view);

        // Send to server
        upsertCachedSearch({"cacheKey":this.cacheKey, "filters":JSON.stringify(tempJson)})
            .then((result) => {
                var title = 'Req Routing';
                var url = '/lightning/n/Req_Routing?c__cacheKey=' + this.cacheKey;

                window.history.replaceState(null, title, url);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getCache() {
        // Send to server
        getCachedSearch({"cacheKey":this.cacheKey})
            .then((result) => {
                let facetData = JSON.parse(result.Search_Criteria__c);
                this.filters = facetData.FilterCriteria;
                this.initialFilters = facetData.FilterCriteria;

                this.userHasSearched = facetData.show_results;

                /*
                 * Using setTimeout with timeout value 0 to start asynchronous thread.
                 * During this thread, we wait for the component to render, and for the toggles to exist.
                 */
                setTimeout(function() {
                    while (!this.isRendered) {}

                    // Send event to lwcReqRoutingFilters so it can update the sort and search field
                    const selectedEvent = new CustomEvent('cacheupdate', { detail: {sort:facetData.sort, search_text:facetData.search_text, selected_view:facetData.selected_view} });
                    this.dispatchEvent(selectedEvent);

                    this.startindex = facetData.pages.from;
                    this.recordsPerPage = facetData.pages.size;

                    this.saveFilterForCancelAction();

                    if (facetData.show_results) {
                        this.updateResults();
                    }
                }.bind(this), 0);
            })
            .catch((error) => {
                console.log(error);
            });
    }

}