import {LightningElement,track,api,wire} from 'lwc';

import UserId from '@salesforce/user/Id';
import Req_Routing_Search_Criteria_OBJECT from '@salesforce/schema/Req_Routing_Search_Criteria__c';
import insertReqFilters from '@salesforce/apex/ReqRoutingController.insertReqFilters';
import getUserFilters from '@salesforce/apex/ReqRoutingController.getUserFilters';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import FiltersMessageChannel from "@salesforce/messageChannel/ReqRoutingFiltersMessageChannel__c";
import { publish, MessageContext } from "lightning/messageService";
import messageChannel from '@salesforce/messageChannel/handleDropDowns__c';


export default class LwcReqRoutingFilters extends LightningElement {
    isFilter = true;
    variant = 'neutral';
    @api view;
	saveviewpopup = false;
    @track newviewinputname;
    @track selectedOption;
    @track selectedCombo ;
    @track selectedOptionID ='';
    newViewName ='';
	@track disableButton=false;
    @track newviewname;
	@api filtercountchanged;
    @track comboOptions=[];
    @track returnOps=[];
    @track showError;
   // @track userComboValues;
	//isEnableNewView = false;
    @track reqRecord = Req_Routing_Search_Criteria_OBJECT;
    @api userSelectedValues;
    @track userSelectedFilters=[];
    @track userSelectedFilter=[];
    @track showErrorMsg =  false;
    throwErr;
    @track errorMessage = '';
    
    @track filterContainerClasses = 'filterContainerParent open';
    @track filterButtonClasses = 'slds-button slds-button_neutral btn-filter btn-filter-open';
    
    searchInpt;
    searchInptSaved;
    sortValue = 'job_create_date:desc';

    @api initialFilterState = "open"

    @track sortByValues = [
        {label: 'Most Recent', value:'job_create_date:desc'},
        {label: 'Win Prob Score', value:'req_win_score:desc'},
        {label: 'Placement Type', value:'position_offering_type_code:asc'},
        {label: 'Stage', value:'req_stage:desc'},
        {label: '# of Allocations', value:'allocated_recruiter_count:asc'},
        {label: '# of Submittals', value:'submittal_count:asc'},
        {label: '# of Open Positions', value:'position_open_quantity:desc'}
    ];

    connectedCallback() {
        this.getUserFilterData();
        
        if (this.initialFilterState == 'closed') {
            this.isFilter = false;
            this.closeFilters();
        }
    }

    @wire(MessageContext) messageContext;
    
    filterChange(event) {
        this.filtercountchanged = event.bubbles;
    }
    
    selectedFilters(event){
        let uFilters = this.template.querySelector("c-lwc-req-routing-filter-details").userSelectedFilters();
        this.userSelectedFilters = uFilters;
    }
    
    getUserFilterData() {
        getUserFilters().then(result => {
            let options = JSON.parse(JSON.stringify(result));
            var returnOptions = [];
            returnOptions.push({label: 'Default View', value: 'Default View' });

            options.forEach(function (item) {
                returnOptions.push({label:item.Search_Name__c, value:item.Search_Name__c});
            });

            this.selectedCombo = returnOptions[0].value;
            this.selectedOption = returnOptions[0].label;
            this.returnOps = returnOptions;

            this.updateSelect();
        })
        .catch(error => {
            this.error = error.message;
        });
    }

    handleClick(event) {
        if(this.isFilter) {
            this.closeFilters();
		}else {
            this.openFilters();
        }

        this.isFilter = !this.isFilter;
        
    }

    openFilters() {
        this.filterContainerClasses = 'filterContainerParent open';
        this.filterButtonClasses = 'slds-button slds-button_neutral btn-filter btn-filter-open';
    }

    closeFilters() {
        var filters = this.template.querySelector('c-lwc-req-routing-filter-details');
        if (filters)
            filters.closeAllTypeaheads({"event":{"detail":""}});

        this.filterContainerClasses = 'filterContainerParent';
        this.filterButtonClasses = 'slds-button slds-button_brand btn-filter btn-filter-open';
    }

    newview(event){
        this.saveviewpopup = true;  
        this.handleDropDown(); //D-17318
    }

    cancelbtn(){
        this.saveviewpopup = false;
        this.showErrorMsg = false;
        this.newviewinputname = '';
        
    }

    closemodal(){
        this.saveviewpopup = false;
        this.showErrorMsg = false;
        this.newviewinputname = '';
        this.disableButton = false;
    }

    handleSaveView() {
        if (!this.disableButton) {
            this.disableButton = true;

            // Get most recent filters
            this.selectedFilters();

            // Adding search text and sort values to json to save
            let tempJson = JSON.parse(this.userSelectedFilters);
            if (this.searchInpt) tempJson.search_text = this.searchInpt;
            if (this.sortValue) tempJson.sort = this.sortValue;
            this.userSelectedFilters = JSON.stringify(tempJson);

            let searchViewName = '';
            let viewInput = this.template.querySelector(".nameInput");
            let inputText = viewInput.value;
            if (this.validateInput(inputText, false)) {
                if(this.selectedOption == 'Default View'){
                    const event = new ShowToastEvent({
                        title: 'Error',
                        message: 'Default view name cannot be renamed.',
                        variant: 'error',
                        mode: 'dismissable'
                    });
                    this.dispatchEvent(event);
                    this.disableButton = false;
                }
                else {
                    let isRename = false;

                    // Set current Id
                    this.reqRecord.Id = this.selectedOptionID;

                    // Set Search Name
                    if (inputText === "" || inputText === null || inputText == undefined) {
                        searchViewName = this.selectedOption;
                        this.reqRecord.Search_Name__c = this.selectedOption;
                    }
                    else {
                        isRename = true;
                        searchViewName = inputText;
                        this.reqRecord.Search_Name__c = inputText;
                    }
                    
                    // Set Search Criteria
                    if (this.userSelectedFilters.length <= 0) this.reqRecord.Search_Criteria__c = '';
                    else this.reqRecord.Search_Criteria__c = this.userSelectedFilters;
                    
                    let context = this;
                    insertReqFilters({ reqSearchSave : context.reqRecord, viewName : context.selectedOption }).then(result => {
                        context.dispatchEvent(new ShowToastEvent({
                            title: 'Success!',
                            message: 'Your current view updated successfully!',
                            variant: 'success'
                        }));

                        if (isRename) {
                            let oldView = context.selectedOption;
                            let newView = inputText;
                            
                            let newReturnOps = [];
                            context.returnOps.forEach(function (item) {
                                if (item.label == oldView){
                                    item.label = newView;
                                    item.value = newView;
                                }

                                newReturnOps.push(item);
                            });
                            context.returnOps = newReturnOps;

                            context.returnOps=[...context.returnOps];
                            context.selectedCombo = inputText;
                            context.selectedOption = inputText;

                            context.updateSelect();
                        }

                        context.closemodal();
                        context.updateSelectedFilter(searchViewName);
                    }).catch(error => {
                        context.error = error.message;
                    });
                }
            }
            else {
                this.disableButton = false;
            }
        }
    }

    handleSaveNewView() {
        if (!this.disableButton) {
            this.disableButton = true;

            // Get most recent filters
            this.selectedFilters();
            
            // Adding search text and sort values to json to save
            let tempJson = JSON.parse(this.userSelectedFilters);
            if (this.searchInpt) tempJson.search_text = this.searchInpt;
            if (this.sortValue) tempJson.sort = this.sortValue;
            this.userSelectedFilters = JSON.stringify(tempJson);

            let viewInput = this.template.querySelector(".nameInput");
            let inputText = viewInput.value;
            if (this.validateInput(inputText, true)) {
                this.reqRecord.Id = null;
                this.reqRecord.OwnerId = UserId;
                this.reqRecord.Search_Name__c = inputText;
                this.reqRecord.Search_Type__c = 'UserSearchCriteria';
                this.reqRecord.Search_Criteria__c = this.userSelectedFilters;

                let context = this;
                insertReqFilters({ reqSearchSave : context.reqRecord, viewName : '' }).then(result => {
                    context.dispatchEvent(new ShowToastEvent({
                        title: 'Success!',
                        message: 'New View saved Successfully!',
                        variant: 'success'
                    }));
                    const option = {
                        label: inputText,
                        value: inputText
                    };
                    
                    context.returnOps.splice(1, 0, option);
                    context.returnOps=[...context.returnOps];
                    context.selectedCombo = option.value;
                    context.selectedOption = option.value;

                    context.updateSelect();
                    context.closemodal();
                    context.updateSelectedFilter(option.value);
                }).catch(error => {
                    context.error = error.message;
                });
            }
        }
    }

    validateInput(inputValue, isNewView) {
        let inputIsEmpty = false;
        let inputIsDuplicate = false;
        
        let isUpdate = false;
        let isRename = false;

        if (isNewView) {
            // Check for empty value
            if (inputValue == '' || inputValue == undefined || inputValue == null) {
                inputIsEmpty = true;
            }

            // Check for duplicate value
            this.returnOps.forEach(function (item) {
                var currentOps = item.label.toLowerCase();
                if(inputValue.toLowerCase() === currentOps){
                    inputIsDuplicate = true;
                }
            });
        }
        else {
            isUpdate = true;
            if (inputValue == '' || inputValue == undefined || inputValue == null) {
                isRename = true;
            }
            else {
                // Check for duplicate value
                this.returnOps.forEach(function (item) {
                    var currentOps = item.label.toLowerCase();
                    if(inputValue.toLowerCase() === currentOps){
                        inputIsDuplicate = true;
                    }
                });
            }
        }

        let isValid = true;
        let errorMessage = '';

        if (inputIsEmpty) {
            isValid = false;
            errorMessage = 'Please enter a name for your new view.';
        }
        else if (inputIsDuplicate) {
            isValid = false;
            errorMessage = 'This List Name already exists. Please update the list name and save again.';
        }

        if (isValid) {
            // Hide error message
            let viewInput = this.template.querySelector(".nameInput");
            viewInput.setCustomValidity('');
            viewInput.reportValidity();
        }
        else {
            // Show error message
            let viewInput = this.template.querySelector(".nameInput");
            viewInput.setCustomValidity(errorMessage);
            viewInput.reportValidity();
        }

        return isValid;
    }

    updateSelect() {
        setTimeout(function(){this.template.querySelector('.viewSelect').value = this.selectedCombo;}.bind(this), 10);
    }

    updateSortSelect() {
        setTimeout(function(){this.template.querySelector('.sortSelect').value = this.sortValue;}.bind(this), 10);
    }

    handleDropDown(event){ //D-17318
        const payload = { facetData: 'lwcReqRoutingFilters' };
        console.log(payload);
        publish(this.messageContext, messageChannel, payload);
    }

    handleChange(event){
        // Clear input text to get a fresh search
        this.searchInpt = '';
        let comboSelected = event.target.value;
        this.selectedOption = comboSelected;
        this.selectedCombo = comboSelected;
        this.updateSelect();
        this.template.querySelector("c-lwc-req-routing-filter-details").filtersByUser(comboSelected);
        
    }

	updateSortFilters(event) {
		let sortableField = event.target.value;
        this.sortValue = sortableField;
        this.template.querySelector("c-lwc-req-routing-filter-details").updateSorting(sortableField);
        
        this.updateSortSelect();

        let message = { filtersApplied: true };
        publish(this.messageContext, FiltersMessageChannel, message);
	}

    /**
     * KeyUp event from the search input
     * 
     * @param {HTML event} event 
     */
    handleKeyUp(event){
        let keyPressed = event.code;
        this.searchInpt = event.target.value;
        let filterDetails = this.template.querySelector('c-lwc-req-routing-filter-details');

        if (keyPressed == 'Enter') {
            event.target.blur();
            filterDetails.updateFromSearchText();

            let message = { filtersApplied: true };
            publish(this.messageContext, FiltersMessageChannel, message);
        }
        /*else
            filterDetails.saveCache();*/
    }

    handleSearchChange(event) {
        this.searchInpt = event.target.value;
    }

    handleClearAll() {
        // Clear text box
        this.searchInptSaved = this.searchInpt;
        this.searchInpt = '';

        // Clear sorting
        this.sortValue = 'job_create_date:desc';
        this.updateSortSelect();
    }

    handleCancel() {
        this.searchInpt = this.searchInptSaved;
    }

    updateFromSavedSearch(event) {
        this.updateFromCache(event);
        this.template.querySelector('c-lwc-req-routing-filter-details').updateFromSearchText();
    }

    updateFromCache(event) {
        if (event.detail.sort)
            this.sortValue = event.detail.sort;
        else 
            this.sortValue = 'job_create_date:desc';

        if (event.detail.search_text)
            this.searchInpt = event.detail.search_text;
        else 
            this.searchInpt = '';

        this.updateSortSelect();
        
        if (event.detail.selected_view) {
            this.selectedCombo = event.detail.selected_view;
            this.selectedOption = event.detail.selected_view;
            this.updateSelect();
        }
        
        this.template.querySelector('c-lwc-req-routing-filter-details').updateFromCache(this.sortValue, this.searchInpt, this.selectedCombo);
    }

    @api
    updateOnTabChange() {
        this.getUserFilterData();	
		this.template.querySelector("c-lwc-req-routing-filter-details").updateResultsFromTabChange(true);
	}

    updateSelectedFilter(selectedOption) {
        let filterDetails = this.template.querySelector("c-lwc-req-routing-filter-details");
        if (filterDetails)
            filterDetails.updateSelectedFilter(selectedOption);
    }
    
}