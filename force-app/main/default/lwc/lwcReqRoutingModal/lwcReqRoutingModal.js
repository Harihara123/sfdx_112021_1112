import { LightningElement } from "lwc";

export default class LwcReqRoutingModal extends LightningElement {
  closeModal() {
    this.dispatchEvent(new CustomEvent("close"));
  }
}