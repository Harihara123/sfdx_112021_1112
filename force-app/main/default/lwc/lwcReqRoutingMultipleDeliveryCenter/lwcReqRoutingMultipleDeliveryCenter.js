import { LightningElement, track, api } from 'lwc';
 
export default class LwcReqRoutingMultipleDeliveryCenter extends LightningElement {
    
    @api options;
    @api selectedValue;
    @api selectedValues = [];
    @api label;
    @api minChar = 2;
    @api disabled = false;
    @api multiSelect = false;
    @track value;
    @track values = [];
    @track optionData;
    @track searchString = '--None--';
    @track message;
    @track showDropdown = false;
    selectedDC = [];
    @track _start;
    @track _end;
    @track _pointer;
    @track _downCounter = 4;
    @track _upCounter = 0;

    connectedCallback() {
        console.log('final->'+JSON.stringify(this.options));
        this.showDropdown = false;
        var optionData = this.options ? (JSON.parse(JSON.stringify(this.options))) : null;
        var value = this.selectedValue ? (JSON.parse(JSON.stringify(this.selectedValue))) : null;
        var values = this.selectedValues ? (JSON.parse(JSON.stringify(this.selectedValues))) : null;
        if(value || values) {
            var searchString;
         var count = 0;
         if(optionData){
                for(var i = 0; i < optionData.length; i++) {
                    if(this.multiSelect) {
                        if(values.includes(optionData[i].value)) {
                            optionData[i].selected = true;
                            count++;
                        }  
                    } else {
                        if(optionData[i].value == value) {
                            searchString = optionData[i].label;
                        }
                    }
                }
            }
            if(this.multiSelect)
                this.searchString = '--None--';
            else
                this.searchString = searchString;
        }
        this.value = value;
        this.values = values;
        this.optionData = optionData;

          //keyboard listner for up and down arrow and enter  
          this._start = 0;
          this._end = this.optionData ? this.optionData.length - 1 : 0;
          this._pointer = this._start - 1;
          console.log('ans-->'+JSON.stringify(this.optionData))     
          document.addEventListener('keydown', (event) => {
            var code = event.code;
            if(code === 'ArrowDown' && this.showDropdown){   
                if(this._pointer !== -1)
                    this.optionData[this._pointer].highlight = false;

                this._pointer = this._pointer === -1 ? this._start : this._pointer !== this._end ? this._pointer + 1 : this._start;
                while(!this.optionData[this._pointer].isVisible) 
                    this._pointer = this._pointer === -1 ? this._start : this._pointer !== this._end ? this._pointer + 1 : this._start;
                this.optionData[this._pointer].highlight = true;
                console.log('down arrow!!!!!!!----'+'  st: '+this._start+'  end: '+this._end+'  pt: '+this._pointer);
                console.log('selected value-->'+JSON.stringify(this.optionData[this._pointer]))

                if(this._upCounter<=4 && this._downCounter>=0){
                    this._upCounter++;
                    this._downCounter--;
                }else if(this._pointer > 5){
                    var eleItem = this.optionData[this._pointer].value
                    var divEle = '[data-id="'+eleItem+'"]'
                    const topDiv = this.template.querySelector(divEle);
                    topDiv.scrollIntoView(false);
                }

                if(this._pointer === 0){
                    var eleItem = this.optionData[this._pointer].value
                    var divEle = '[data-id="'+eleItem+'"]'
                    const topDiv = this.template.querySelector(divEle);
                    topDiv.scrollIntoView(true);
                }
                event.preventDefault();
            }
            else if(code === 'ArrowUp' && this.showDropdown){
                if(this._pointer !== -1)
                    this.optionData[this._pointer].highlight = false;

                this._pointer = this._pointer === -1 ? this._end : this._pointer !== this._start ? this._pointer - 1 : this._end;
                while(!this.optionData[this._pointer].isVisible)
                    this._pointer = this._pointer === -1 ? this._end : this._pointer !== this._start ? this._pointer - 1 : this._end;    
                this.optionData[this._pointer].highlight = true;
                console.log('up arrow!!!!!!!!---'+'  st: '+this._start+'  end: '+this._end+'  pt: '+this._pointer);
                console.log('selected value-->'+JSON.stringify(this.optionData[this._pointer]))

                if(this._downCounter<=4 && this._upCounter>=0){
                    this._upCounter--;
                    this._downCounter++;
                }else if(this._pointer < this.optionData.length - 5){
                    var eleItem = this.optionData[this._pointer].value
                    var divEle = '[data-id="'+eleItem+'"]'
                    const topDiv = this.template.querySelector(divEle);
                    topDiv.scrollIntoView(true);
                }
                if(this._pointer === this.optionData.length - 1){
                    var eleItem = this.optionData[this._pointer].value
                    var divEle = '[data-id="'+eleItem+'"]'
                    const topDiv = this.template.querySelector(divEle);
                    topDiv.scrollIntoView(false);
                }
                event.preventDefault();
            }
            else if(code === 'Enter' && this.showDropdown){               
                if(this._pointer >= this._start && this._pointer <= this._end){                  
                    var selectedVal = this.optionData[this._pointer].value;
                    console.log('selected DC in cons@@@@'+selectedVal);
                    this.selectItemGen(selectedVal);
                }
            }
          }, false);
          
    }

    disconnectedCallback(){
        //document.body.style.overflow = '';
    }
 
    filterOptions(event) {
        this.searchString = event.target.value;
        console.log('@@@-------------------'+this.searchString)
        if (event.key === 'Enter') {
            event.preventDefault();
        }
        if( this.searchString && this.searchString.length >= 0 ) {
            this.message = '';
            //if(this.searchString.length >= this.minChar) {
                var flag = true;
                if(this.optionData){
                    for(var i = 0; i < this.optionData.length; i++) {
                        if(this.optionData[i].label.toLowerCase().trim().includes(this.searchString.toLowerCase().trim())) {
                            //if(!this.optionData[i].selected) this.optionData[i].isVisible = true;
                            this.optionData[i].isVisible = true;
                            flag = false;
                        } else {
                            this.optionData[i].isVisible = false;
                        }
                    }
                }
                if(flag) {
                    this.message = "No results found for '" + this.searchString + "'";
                }
            //}
            this.showDropdown = true;
        } else {
            //this.showDropdown = false;
        }
 }
 
    selectItem(event) {
        var selectedVal = event.currentTarget.dataset.id;
        console.log('selected DC@@'+selectedVal);
        if(selectedVal) {
            this.selectItemGen(selectedVal);
            if(this.multiSelect)
                event.preventDefault();
            else
                this.showDropdown = false;
        }       
    }
 
    showOptions() {
        if(this.disabled == false && this.options) {
            this.message = '';
            this.searchString = '';
            var options = JSON.parse(JSON.stringify(this.optionData));
            for(var i = 0; i < options.length; i++) {
                if(!options[i].selected) options[i].isVisible = true;
            }
            if(options.length > 0) {
                this.showDropdown = true;
            }
            this.optionData = options;
        }
    }
 
    removePill(event) {
        var value = event.currentTarget.name;
        var count = 0;
        var options = JSON.parse(JSON.stringify(this.optionData));
        for(var i = 0; i < options.length; i++) {
            if(options[i].value === value) {
                options[i].selected = false;
                this.values.splice(this.values.indexOf(options[i].value), 1);
            }
            if(options[i].selected) {
                count++;
            }
        }
        this.optionData = options;
        if(this.multiSelect){
            if(count > 0) this.searchString = '';
            else this.searchString = '--None--';
        }
          
        for(var i = 0; i < this.selectedDC.length; i++){
            if(this.selectedDC[i] === value) this.selectedDC.splice(this.selectedDC.indexOf(value), 1);
        }

        this.customEventFired('officechange', this.selectedDC);
    }
 
    blurEvent() {
        var previousLabel;
        var count = 0;
        for(var i = 0; i < this.optionData.length; i++) {
            if(this.optionData[i].value === this.value) {
                previousLabel = this.optionData[i].label;
            }
            if(this.optionData[i].selected) {
                count++;
            }
        }
        if(this.multiSelect){
         if(count > 0) this.searchString = '';
         else this.searchString = '--None--';
        }
        else
         this.searchString = previousLabel;
        
        this.showDropdown = false;
 
        this.dispatchEvent(new CustomEvent('select', {
            detail: {
                'payloadType' : 'multi-select',
                'payload' : {
                    'value' : this.value,
                    'values' : this.values
                }
            }
        }));
    }

    @api 
    getSelectedDC() {
        return this.selectedDC;
      }
    @api
    undoDeliveryChange(){
        this.selectedDC = [];
        this.optionData.forEach(ele=>{
            ele.selected = false;
        });
        return this.selectedDC;
    }

    selectItemGen(selectedVal){
        if(this.selectedDC.indexOf(selectedVal) !== -1) this.selectedDC.splice(this.selectedDC.indexOf(selectedVal), 1)
        else this.selectedDC.push(selectedVal)
        var count = 0;
        var options = JSON.parse(JSON.stringify(this.optionData));
        for(var i = 0; i < options.length; i++) {
            if(options[i].value === selectedVal) {
                if(this.multiSelect) {
                    if(this.values.includes(options[i].value)) {
                        this.values.splice(this.values.indexOf(options[i].value), 1);
                    } else {
                        this.values.push(options[i].value);
                    }
                    options[i].selected = options[i].selected ? false : true; 
                    //options[i].isVisible = false;
                } else {
                    this.value = options[i].value;
                    this.searchString = options[i].label;
                }
            }
            if(options[i].selected) {
                count++;
            }
        }
        this.optionData = options;
        if(this.multiSelect){
            // if(count > 0) this.searchString = '';
            // else this.searchString = '--None--';
            this.searchString = '';
        }

        this.customEventFired('officechange', this.selectedDC);
    }

    customEventFired(name, details){
        var cEvent = new CustomEvent(name, { detail:  details});
        this.dispatchEvent(cEvent);
    }
}