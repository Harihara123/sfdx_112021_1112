import { LightningElement, track, wire, api } from "lwc";
import messageChannel from "@salesforce/messageChannel/ReqRoutingMessageChannel__c";
import FiltersMessageChannel from "@salesforce/messageChannel/ReqRoutingFiltersMessageChannel__c";
import { publish, MessageContext, subscribe } from "lightning/messageService";

export default class App extends LightningElement {
  @track pages = [];
  @api totalrecords;
  @api currentpage = 1;
  @api recordsPerPage = 30;
  @api startindex = 0;
  
  totalpages;
  lastpage;
  startpage = 1;
  lastindex = this.recordsPerPage;
  startingindex = 1;
  subscription = null;

  @wire(MessageContext) messageContext;

  connectedCallback() {
    // Current page calculation
    this.currentpage = (this.startindex / this.recordsPerPage) + 1;

    this.handleSubscribe();
  }

  renderedCallback() {
    this.template.querySelector("[data-name='perPageSelect']").value = this.recordsPerPage;
    
    let recordscount = this.calculatePageSize();
    console.log("recordscount@@" + recordscount);

    if (this.totalpages != recordscount) {
        this.totalpages = recordscount;
        this.fetchpagenumbers();
        this.getindexes(this.currentpage, false);
    }
    
    this.getindexes(this.currentpage, false);
    this.selectedbtndisable();
  }

  handleSubscribe() {
    if (this.subscription) {
      return;
    }
    this.subscription = subscribe(
      this.messageContext,
      FiltersMessageChannel,
      (message) => {
        console.log("message##" + JSON.stringify(message));
        if (message.filtersApplied) {
          this.currentpage = 1;
          this.fetchpagenumbers();
          this.getindexes(this.currentpage, false);
        }
      }
    );
  }

  calculateCurrentPage() {
    return (this.startindex / this.recordsPerPage) + 1;
  }

  handleOptionChange(event) {
    this.recordsPerPage = event.target.value;
    this.totalpages = this.calculatePageSize();
    this.currentpage = 1;
    this.fetchpagenumbers();
    this.getindexes(this.currentpage, true);
  }

  handlefirst() {
    this.currentpage = 1;
    this.getindexes(this.currentpage, true);
    this.fetchpagenumbers();
  }

  handlelast() {
    this.currentpage = this.totalpages;
    this.getindexes(this.currentpage, true);
    this.fetchpagenumbers();
  }

  handleclick(event) {
    this.currentpage = event.target.label;
    this.getindexes(this.currentpage, true);
    this.fetchpagenumbers();
    this.selectedbtndisable();
  }

  get previousbtndisabled() {
    return this.currentpage == 1 ? true : false;
  }

  get nextbtndisabled() {
    return this.currentpage == this.totalpages ? true : false;
  }

  getindexes(currentpage, updateResults) {
    this.startindex = (currentpage - 1) * this.recordsPerPage;
    this.startingindex = this.startindex + 1;
    this.lastindex =
      currentpage * this.recordsPerPage >= this.totalrecords
        ? this.totalrecords
        : currentpage * this.recordsPerPage;
    console.log("totalrecords!!" + this.totalrecords);
    console.log("lastindex!!" + this.lastindex);
    let message = {
      startindex: this.startindex,
      recordsPerPage: this.recordsPerPage
    };

    if (updateResults)
        publish(this.messageContext, messageChannel, message);
  }

  fetchpagenumbers() {
    this.totalpages = this.calculatePageSize();
    console.log("totalpages" + this.totalpages);
    this.startpage = this.currentpage - 3 > 0 ? this.currentpage - 3 : 1;
    // if (this.startpage >= this.totalpages) {
    //   this.currentpage = 1;
    //   this.startpage = 1;
    // }
    this.lastpage =
      this.currentpage + 3 < this.totalpages
        ? this.currentpage + 3
        : this.totalpages;
    this.pages = [];
    for (let i = this.startpage; i <= this.lastpage; i++) {
      this.pages.push(i);
    }
  }

  handlenext(event) {
    this.currentpage = this.currentpage + 1;
    this.getindexes(this.currentpage, true);
    this.fetchpagenumbers();
    this.selectedbtndisable();
  }

  handleprevious(event) {
    this.currentpage = this.currentpage - 1;
    this.getindexes(this.currentpage, true);
    this.fetchpagenumbers();
    this.selectedbtndisable();
  }

  selectedbtndisable() {
    const element = Array.from(
      this.template.querySelectorAll("lightning-button")
    );

    element.forEach((item) => {
      if (item.label == this.currentpage) {
        item.disabled = true;
      }
      if (item.label != this.currentpage) {
        item.disabled = false;
      }
    });
  }

  calculatePageSize() {
    console.log("recordsPerPage" + this.recordsPerPage);

    var totalpages = 0;
    if (this.recordsPerPage == 20) {
      totalpages =
        this.totalrecords > 10000
          ? 500
          : Math.ceil(this.totalrecords / this.recordsPerPage);
      return totalpages;
    }
    if (this.recordsPerPage == 30) {
      totalpages =
        this.totalrecords > 10000
          ? 333
          : Math.ceil(this.totalrecords / this.recordsPerPage);
      return totalpages;
    }
    if (this.recordsPerPage == 50) {
      totalpages =
        this.totalrecords > 10000
          ? 200
          : Math.ceil(this.totalrecords / this.recordsPerPage);
      return totalpages;
    }
  }
}