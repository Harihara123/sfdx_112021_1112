import {LightningElement} from 'lwc';

export default class LwcReqRoutingPopOver extends LightningElement {

	showSearchResults = false;
	searchResults = [];
	opco = 'TEK';
	whereCond = 'Text_Value_2__c = \'' + this.opco + '\'';
	jobTitle = '';

	handleResultItemClick(event){

	}
	closeButton(event){
		this.dispatchEvent(new CustomEvent("close",{detail:{"isClose":true}}));
	}
}