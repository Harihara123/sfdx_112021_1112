import {LightningElement, api, wire, track} from 'lwc';
import currentUserId from '@salesforce/user/Id';
import saveUserActions from '@salesforce/apex/ReqRoutingController.saveUserActions';
import deleteSavedReqs from "@salesforce/apex/ReqRoutingController.deleteSavedReqs";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";
import getOppRecruiters from "@salesforce/apex/ReqRoutingController.getOppRecruiters";
import getSupportRequests from "@salesforce/apex/ReqRoutingController.getSupportRequests";
import getPastSharedReqUsers from "@salesforce/apex/ReqRoutingController.getPastSharedReqUsers";
import unAllocatedRecruiter from "@salesforce/apex/ReqRoutingController.unAllocatedRecruiter";

export default class LwcReqRoutingSearchResultItem extends NavigationMixin(LightningElement)  {

	@api reqDetails;
    @api view;
	isLoaded = false;
	isRendered = false;
	@track openAllocatedRecModal = false;
	@track openDeliveryCenterModal = false;
    @track openShareReqsModal = false;
	@track deliveryInfo = [];
	@track usrs = [];
	@track hasAllocated = false;
  @track allocatedRecruiters = {};
  @track SupportRequests = {};
  @track sharedReqs = [];
  is_saved = false;
  showUAModal = false;

  //for unallocate Req
   @api unallocateUserName;
   @api unallocateUserId;
   @api unallocateReqId;

    personFields = ['User.FirstName','User.LastName'];
    popoverFields = ['User.Title','User.Office__c','User.CompanyName','User.Email','User.Phone','User.Division'];

  parameterObject;

    showDeliveryCenter = false;
    showAllocated = false;
    showSharedReqs = false;

    connectedCallback() {    
        this.showAllocated = true;
        this.usrs = [];
        this.hasAllocated = false;
        let allocatedRecs = JSON.stringify(this.reqDetails.allocatedRecruiters);
        if (allocatedRecs != null && allocatedRecs != undefined && allocatedRecs != "--") {
            let userMap = JSON.parse(allocatedRecs);
            let vals = [];
            this.hasAllocated = true;
            for (const prop in userMap) {
                if (prop == this.reqDetails.id) {
                    vals = userMap[prop];
                }
            }
            let usr = {};
            vals.forEach((item) => {
                this.usrs.push({
                    Id: "/" + item.split(":")[0],
                    Name: item.split(":")[1]
                });
            });
			console.log('JSON.stringify(this.usrs)--'+JSON.stringify(this.usrs));
        }
        if (this.view == "deliveryCenterReqs") {
            let Requests = JSON.stringify(this.reqDetails.deliveryCenters);
            if (Requests != null && Requests != undefined && Requests != "--") {
                let supportMap = JSON.parse(Requests);
                let vals = [];
                this.showDeliveryCenter = false;
                for (const prop in supportMap) {
                    if (prop == this.reqDetails.id) {
                        vals = supportMap[prop];
                        this.SupportRequests = supportMap[prop];
                        this.showDeliveryCenter = true;
                    }
                }
            }      
        }
        if (this.view == "sharedReqs") {
            let tmpSharedReqs = this.reqDetails.sharedReqs;
            if (tmpSharedReqs != null && tmpSharedReqs != undefined && tmpSharedReqs != "--") {
                this.sharedReqs = [];
                tmpSharedReqs.forEach((item) => {
                    if (item.reqId == this.reqDetails.id) {
                        this.showSharedReqs = true;
                        item.sharedReqUsers.forEach((req) => {
                            req = Object.assign({}, req);
                            req.url = "/"+req.Id;
                            this.sharedReqs.push(req);
                        });                        
                    }
                    console.log('this.sharedReqs------'+JSON.stringify(this.sharedReqs));
                });
      }
	  console.log('this.SupportRequests------'+JSON.stringify(this.SupportRequests));
      console.log('this.sharedReqs------'+JSON.stringify(this.showSharedReqs));
    }
    this.is_saved = this.reqDetails.is_saved;
  }

  renderedCallback() {
    if (this.reqDetails.is_saved) {
      let element = this.template.querySelector(".saveButton");
      element.variant = "brand";
      element.label = "Saved";
      
    }
  }

	renderedCallback() {
    if (!this.isRendered) {
      console.log("isrendered@@@" + this.isRendered);
      if (this.reqDetails.is_saved) {
        let element = this.template.querySelector(".saveButton");
        element.variant = "brand";
        element.label = "Saved";
      }
      this.isRendered = true;
    }
  }
    
  get selectedTab() {
    if (this.view == "dashboard") {
      return true;
    }
    if (this.view == "savedReqs") {
      return false;
    }
  }
  get winScoreStyle() {
    var color = 'fieldValue ';
    var winscore = this.reqDetails.win_prob_score;
    if (winscore == "High") {
      color += "winScoreGreen";
    } else if (winscore == "Medium") {
      color += "winScoreOrange";
    } else if (winscore == "Low") {
      color += "winScoreRed";
    }
    return color;
  }

    showAllocateRecModal(event) {
        this.template.querySelector('c-lwc_allocated-recruiters').openModal('true', this.reqDetails.id);
    }

	showDeliveryModal(event) {
		this.template.querySelector('c-lwc-delivery-center').openModal('true',this.reqDetails);		
	}
    showShareReqsModal(event) {
        this.template.querySelector("c-lwc-share-reqs").openModal("true", this.reqDetails.id);
    }

  showSavedReqsModal(event) {
        //this.isModalOpen = true;
    this.oppId = event.target.getAttribute("data-value");
    console.log("oppID@@" + this.oppId);
	console.log("reqDetails%%" + JSON.stringify(this.reqDetails));
	this.saveActions();
  }
  handleModal(event) {
    if (event.target.label == "No") {
      this.isModalOpen = false;
    } else if (event.target.label == "Yes") {
      console.log("called@@");
      this.saveActions();
    }
  }

  closeModal(event) {
    this.isModalOpen = false;
  }
    handleDeliveryCenterSave(event) {
        this.deliveryInfo = event.detail.deliveryInfo;
        console.log("event received");
        console.log(event.detail.deliveryInfo);
        console.log(this.deliveryInfo);
        if (this.view == "deliveryCenterReqs") {
            getSupportRequests({ OppId: [this.reqDetails.id] }).then((requests) => {
                if (requests !== null) {
                    let Requests = JSON.stringify(requests);
                    if (Requests != null && Requests != undefined && Requests != "--") {
                        let supportMap = JSON.parse(Requests);
                        let vals = [];
                        this.showDeliveryCenter = false;
                        for (const prop in supportMap) {
                            if (prop == this.reqDetails.id) {
                                vals = supportMap[prop];
                                this.SupportRequests = supportMap[prop];
                                this.SupportRequests.forEach((item) => {
                                    item.url = "/" + item.Id;
                                });
                                this.showDeliveryCenter = true;
                            }
                        }
                    }                  
                } else {
                    this.SupportRequests = [];
                    this.showDeliveryCenter = false;
                }
                console.log(this.SupportRequests);
            });
        }
    }

	refreshAllocatedRecruiters(event) {
		console.log("event received");
		console.log(event.detail);		
		getOppRecruiters({ OppId: [this.reqDetails.id] })
        .then(users => {
            if(users !== null) {								
				let allocatedRecs = JSON.stringify(users);
				this.allocatedRecruiters = users;
			  	this.hasAllocated = true;
				if(allocatedRecs != null && allocatedRecs != undefined && allocatedRecs != "--" ) {
					let userMap = JSON.parse(allocatedRecs);			
					let vals = [];
					this.hasAllocated = true;
					for (const prop in userMap) {
						if(prop == this.reqDetails.id) {
							vals = userMap[prop];
						}				
					}	
					if(vals !== null && vals !== undefined && vals !== []) {
						this.usrs = [];				
						vals.forEach((item) => {
							this.usrs.push({Id: "/"+item.split(":")[0], Name: item.split(":")[1]});	
							console.log('JSON.stringify(this.usrs)----------------'+JSON.stringify(this.usrs));
						})
					}					
				}                
            }
			else {
				this.hasAllocated = false;
				this.usrs = [];
			}
		});
    }

    handleShareReqsRefresh(event) {      
        console.log("event received");
        if (this.view == "sharedReqs") {
            getPastSharedReqUsers({ OppId: [this.reqDetails.id] }).then((resShared) => {
                console.log(resShared);
                if (resShared !== null) {
                    let tmpSharedReqs = resShared;
                    if (tmpSharedReqs != null && tmpSharedReqs != undefined && tmpSharedReqs != "--") {
                        this.sharedReqs = [];
                        tmpSharedReqs.forEach((item) => {
                            if (item.reqId == this.reqDetails.id) {
                                item.sharedReqUsers.forEach((req) => {  
                                    req = Object.assign({}, req);
                                    req.url = "/"+req.Id;
                                    this.sharedReqs.push(req);                                                                     
                                });                  
                                this.showSharedReqs = true;
                            }
                        });              
                    }
                } else {
                  this.sharedReqs = [];
                  this.showSharedReqs = false;
                }
                console.log(this.sharedReqs);
            });
        }
    }
	// Unallocate Req       
    
	showRemovePopup(event) {      
        console.log("Inside showRemovePopup");
        // to open modal set showUAModal tarck value as true
        this.unallocateUserName=event.target.label;
		this.unallocateUserId=event.target.dataset.unaluid;
		this.unallocateUserId= this.unallocateUserId.substring(1);
		this.unallocateReqId=event.target.dataset.reqid;
		console.log('unallocateUserId'+this.unallocateUserId);
		console.log('unallocateReqId'+this.unallocateReqId);
        this.showUAModal = true;
    }
    closeUAModal() {
        // to close modal set showUAModal tarck value as false
        this.showUAModal = false;
    }
   UARecruiter(event) {
        // to close modal set showUAModal tarck value as false
        //Add your code to call apex method or do some processing
        this.showUAModal = false;
		console.log("unallocateUserId:" +this.unallocateUserId);
		console.log("unallocateReqId:" +this.unallocateReqId);
		var uidtemp= '/'+this.unallocateUserId;
		console.log('uidtemp---'+uidtemp);
		console.log('JSON.stringify(this.usrs)--UARecruiter---'+JSON.stringify(this.usrs));
		var currentusrstemp = this.usrs;
		
		console.log('JSON.stringify(this.usrs)--currentusrstemp---'+JSON.stringify(currentusrstemp));
		/*let parameterObject = {
		  OppId : this.unallocateReqId,
		  RecruiterId : this.unallocateUserId      
		};       
      unAllocatedRecruiter({ actions: parameterObject })
        .then((result) => {
          console.log("result:" + JSON.stringify(result));
				//this.isLoaded = false;
          let msg, variant, title;
          if (result !== "SUCCESS") {
					msg = result;
            variant = "error";
            title = "Error";
          } else {
            //let element = this.template.querySelector(".saveButton");
           // element.variant = "Neutral";
            //element.label = "Save";
           // console.log("element@@" + JSON.stringify(element));
            msg = "Req is Unallocated Successfully";
            variant = "success";
            title = "Success";
			
          }
          const evt = new ShowToastEvent({
            title: title,
            message: msg,
            variant: variant
          });
          this.dispatchEvent(evt);
          this.is_saved = false;
        })
        .catch((error) => {
          this.isLoaded = false;
          const evt = new ShowToastEvent({
            title: "Error",
            message: "Error",
            variant: "error"
          });
          this.dispatchEvent(evt);
          console.log("error:" + JSON.stringify(error));
        });*/
		unAllocatedRecruiter({ 
            OppId : this.unallocateReqId,
			RecruiterId : this.unallocateUserId 
         })
         .then(result => {
             const event = new ShowToastEvent({
                 title: 'Success',
                 message: '1 Req has been Unallocated Successfully ',
                 variant: 'success'
             });
             this.dispatchEvent(event);
				var pos = currentusrstemp.map(function(e) {
				return e.Id;
				}).indexOf(uidtemp);

				if(pos !== -1){
					currentusrstemp.splice(pos,1)
				}
         })
         .catch(error => {
             const event = new ShowToastEvent({
                 title : 'Error',
                 message : 'Error removing Req. Please Contact System Admin',
                 variant : 'error'
             });
             this.dispatchEvent(event);
         });
		  //this.refreshAllocatedRecruiters(event);
    }
  saveActions() {
    this.isModalOpen = false;
			this.isLoaded = true;
    let parameterObject = {
      opportunityId: this.oppId,
      currentUserId: currentUserId,
      actionType: "Save",
      isActive: true
    };

    if (this.is_saved) {
      console.log("view@@" + this.view);
      deleteSavedReqs({ actions: parameterObject })
        .then((result) => {
          console.log("result:" + JSON.stringify(result));
				this.isLoaded = false;
          let msg, variant, title;
          if (result !== "SUCCESS") {
					msg = result;
            variant = "error";
            title = "Error";
          } else {
            let element = this.template.querySelector(".saveButton");
            element.variant = "Neutral";
            element.label = "Save";
            console.log("element@@" + JSON.stringify(element));
            msg = "Req is no longer saved and will not appear on your Saved Reqs tab";
            variant = "success";
            title = "Success";
          }

          const evt = new ShowToastEvent({
            title: title,
            message: msg,
            variant: variant
          });
          this.dispatchEvent(evt);
          this.is_saved = false;
        })
        .catch((error) => {
          this.isLoaded = false;
          const evt = new ShowToastEvent({
            title: "Error",
            message: "Error",
            variant: "error"
          });
          this.dispatchEvent(evt);
          console.log("error:" + JSON.stringify(error));
        });
        
    }

    if (!this.is_saved) {
      saveUserActions({ actions: parameterObject })
        .then((result) => {
          console.log("result:" + JSON.stringify(result));
          this.isLoaded = false;
          let msg, variant, title;
          if (result !== "SUCCESS") {
            msg = result;
            variant = "error";
            title = "Error";
          } else {
            let element = this.template.querySelector(".saveButton");
            element.variant = "brand";
            element.label = "Saved";
            console.log("element@@" + JSON.stringify(element));
            msg = "Req has been Saved Successfully";
            variant = "success";
            title = "Success";
          }

          const evt = new ShowToastEvent({
					title: title,
					message: msg,
            variant: variant
          });
          this.dispatchEvent(evt);
          this.is_saved = true;
          //this.contacts = result;
          //this.error = undefined;
        })
        .catch((error) => {
          this.isLoaded = false;
          const evt = new ShowToastEvent({
            title: "Error",
            message: "Error",
            variant: "error"
          });
          this.dispatchEvent(evt);
          console.log("error:" + JSON.stringify(error));
        });
    }
  }
}