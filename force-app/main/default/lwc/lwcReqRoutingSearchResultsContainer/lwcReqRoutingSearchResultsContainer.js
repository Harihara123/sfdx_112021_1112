import { LightningElement, api,wire } from "lwc";
import { publish, MessageContext } from "lightning/messageService";
import messageChannel from '@salesforce/messageChannel/handleDropDowns__c';

export default class LwcReqRoutingSearchResultsContainer extends LightningElement {
    _reqSearchResults;
    _totalRecords;

    showEmptyState = true;
    showTotalCount = false;

    @api recordsPerPage;
    @api view = 'dashboard';
    @api startIndex;

    @api 
    get totalrecords() {
        return this._totalRecords;
    }
    set totalrecords(value) {
        this._totalRecords = value;
        this.showTotalCount = !(value == undefined);
    }

    @api
    get reqSearchResults() {
        return this._reqSearchResults;
    }
    set reqSearchResults(value) {
        this._reqSearchResults = value;

        this.showEmptyState = this._reqSearchResults.length > 0 ? false : true;
    }
    @wire(MessageContext) messageContext;

    hanldeDropDown(){
        const payload = { facetData: 'lwcReqRoutingSearchResultsContainer' };
        publish(this.messageContext, messageChannel, payload);
    }
}