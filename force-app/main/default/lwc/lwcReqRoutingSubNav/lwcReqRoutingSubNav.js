import {LightningElement} from 'lwc';

export default class LwcReqRoutingSubNav extends LightningElement {
    
    isFirst = true;
    selectedItem = 'dummyitem';
    
    connectedCallback() {
        const cacheParam = new URLSearchParams(window.location.search).get('c__cacheKey');

        let localSelectedTab = sessionStorage.getItem('rr_selectedtab_' + cacheParam);
        if (localSelectedTab) {
            this.selectedItem = localSelectedTab;
        }
        else {
            this.selectedItem = 'dashboard';
        }
    }

    handleSelect(event) {
        this.selectedItem = event.detail.name;

        const selectedEvent = new CustomEvent('tabselected', { detail: {selected_tab:this.selectedItem, is_first:this.isFirst }});
        this.dispatchEvent(selectedEvent);
        this.isFirst = false;
    }
}