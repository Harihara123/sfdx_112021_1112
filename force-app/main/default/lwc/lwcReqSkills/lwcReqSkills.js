import { LightningElement, api } from 'lwc';
import performServerCall from '@salesforce/apex/BaseController.performServerCall';

const SOQL_PARAMS = { //params for searching using BaseController method
    addlReturnFields: '[]',
    type: 'Global_LOV__c',
    idFieldOverride: 'id',
    nameFieldOverride: 'Text_Value__c',
    recTypeName: 'MatchSkillList',
    sortCondition: 'Number_Value__c DESC',
    useCache: false,
    whereCondition: "Lov_Name__c = 'MatchSkillList' and TextValue2_CompanyName__c =" //!!NEED TO CONCAT OpCo__c!! ('AG_EMEA')
}
const BD_HELPER_PARAMS = { //params for BaseController method.
    className: 'Lookup',
    methodName: 'querySObject',
    subClassName: ""
}

const TYPE_PAUSE = 300; //Query server if nothing typed for this length of time after starting typing;

export default class LwcReqSkills extends LightningElement {
    @api skills = [];
    @api opco = ''
    @api className = 'req-skill-search primary'
    value= '';

    allSuggestedSkills = [];

    @api suggestedSkills = []
    @api inUseSuggestedSkills = []
    resultsState = 'hide';
    results = []
    timeoutRef = null;    

    handlePillChange(e) {        
        const {pills} = e.detail;

        if (pills.length < this.skills.length) { //If update pill count is lower than current pills, check if delta is within inUseSuggestedSkills. 

            const removedSkill = this.skills.filter(skill => !pills.includes(skill))[0]; //Return skills that aren't included in updated pills (pills removed).
           
            const removedSkillIndex = this.inUseSuggestedSkills.indexOf(removedSkill); //get an index of removed skill; if -1, skill doesn't exist within inUseSuggestedSkills.

            if (removedSkillIndex !== -1) {
                //removed skill was found within inUsedSuggestedSkills; were added from suggested skills.
                //add it back to suggested skills;
                const newInsUseSuggestedSkills = [...this.inUseSuggestedSkills];
                newInsUseSuggestedSkills.splice(removedSkillIndex, 1) //Remove the skill from inUse;
                this.inUseSuggestedSkills = newInsUseSuggestedSkills;
                
                const newSuggestedSkills = [...this.suggestedSkills]
                newSuggestedSkills.unshift(removedSkill); //Add the skill back to the front of the list
                this.suggestedSkills = newSuggestedSkills;

                this.dispatchEvent(new CustomEvent('suggestedskillschange', {detail: {pills: newSuggestedSkills}}))
            }
        }        

        this.skills = pills;
        this.resultsState = 'hide'
        this.results = []

        this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills}}))
    }
    handleSuggestedSkillAdd(e) {
        const value = e.currentTarget.getAttribute('data-val');        
        
        const existingPill = this.skills.map(s => s.toLowerCase()).indexOf(value.toLowerCase()); //check if skill clicked already exists in skills. 

        if (existingPill === -1) {            
            //Pill is not within the included skills; add it.
            
            const suggestedSkillIndex = this.suggestedSkills.indexOf(value); //Get skill index in suggested list.
            const newSuggestedSkills = [...this.suggestedSkills];
            const inUseSuggestedSkill = newSuggestedSkills.splice(suggestedSkillIndex,1)[0]; //Remove the skill from suggestedSkills and set it to a variable.
            this.suggestedSkills = newSuggestedSkills;
            this.dispatchEvent(new CustomEvent('suggestedskillschange', {detail: {pills: newSuggestedSkills}}))

            const newInUseSuggestedSkills = [...this.inUseSuggestedSkills]
            newInUseSuggestedSkills.push(inUseSuggestedSkill) //Add the skill to the list to be tracked for re-adding purpose.
            this.inUseSuggestedSkills = newInUseSuggestedSkills;

            const newPills = [...this.skills]; //create shallow copy.
            newPills.push(inUseSuggestedSkill); //push new skill to copy.

            this.skills = newPills; //set new skills; re-render.
            this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills}}))
            this.template.querySelector('c-lwc-pill-field').focusInput()//public method from lwcPillField to focus the textfield
        } else {
            //Pill is within existing list; don't add it.
            this.handleDupeMsg(value)            
        }
    }

    handleSearchedSkillAdd(e) {
        const searchedSkill = e.currentTarget.getAttribute('data-val'); //get data-val attribute set during iteration
        //console.log(searchedSkill);

        const searchedSkillIndex = this.skills.map(s=> s.toLowerCase()).indexOf(searchedSkill.toLowerCase()); //Check if searched skill clicked is within the skills already.

        if (searchedSkillIndex === -1) {
            //searched skill NOT within skills; add it.
            const newSkills = [...this.skills]; //shalow copy
            newSkills.push(searchedSkill); //addign new skill to copy
            this.skills = newSkills; //setting skills to updated skilss
            this.value = ''; //clear out value in textfield
            this.results = []; //clear out results so they dont show during next skill lookup
            this.hideResults() //close results container
            this.template.querySelector('c-lwc-pill-field').focusInput()
            this.dispatchEvent(new CustomEvent('skillschange', {detail: {skills: this.skills}}))
        } else {
            this.value = ''; //clear out value in textfield
            this.results = []; //clear out results so they dont show during next skill lookup
            this.hideResults() //close results container
            this.template.querySelector('c-lwc-pill-field').focusInput()
            this.handleDupeMsg(searchedSkill)
        }
    }
    handleKeyDown(e) {
        const event = e.detail.e; //Event passed up from lwcPillField cmp.
        if (event) {            
            if (event.keyCode === 40 && this.resultsState === 'show') { //If Arrow down pressed and results are shown.
                const liElement = this.template.querySelector('.slds-listbox__item'); //find first available search result.
                if (liElement) {liElement.focus()} //focus the element if axists.
            } else if (event.keyCode === 9) { //tab key
                this.hideResults() //tabbing away; hide searched skill container
            }
        }
    }
    handleSearchedSkillNav(e) { //Handle onkeydown of the <li> element in results.
        e.preventDefault();
        if (e.keyCode === 40) { //Arrow down.
            const nextElem = e.currentTarget.nextElementSibling; //get next element.
            if (nextElem) {nextElem.focus()} //focus next element.
        } else if (e.keyCode === 38) {//arrow up.
            const prevElem = e.currentTarget.previousElementSibling; //get previous element.
            if (prevElem) {prevElem.focus()} //focus previous element.
        } else if (e.keyCode === 13) {//enter
            e.currentTarget.click(); //click the searched skill, which has a handler to add it to skills and close the results container.
            //this.template.querySelector('c-lwc-pill-field').focusInput() //public method from lwcPillField to focus the textfield
        } else if (e.keyCode === 27) {//escape
            this.hideResults()
            this.template.querySelector('c-lwc-pill-field').focusInput() //public method from lwcPillField to focus the textfield
        }
    }
    handleBlur(e) {
        //console.log(this.value)
    }
    handleChange(e) { //custom event from lwcPillField when value changes in the textfield.
        const value = e.detail.value; 
        this.value = e.detail.value //setting value from lwcPillField to be same as our value

        if (value.length > 2) { //if the value is longer than 2 characters initialize server call
            this.callServer(value)
        }
    }
    checkClickedNode(e) {
        const clickedEl = e.path[0]; //get the element clicked; e.path is safer way to get an element when need to "reach" into other component elements.
        const resultsContainer = this.template.querySelector('.slds-dropdown') //get container element.

        if (!resultsContainer.contains(clickedEl)) { //if the clicked element is NOT within the container close the results; clicked away.
            this.hideResults()
        }
    }
    hideResults() { //Hide results and remove event listener.
        this.resultsState = 'hide';
        document.removeEventListener('click', this.checkClickedNode.bind(this))
    }
    showResults() { //show results and initialize the event listener that will check what was clicked.
        this.resultsState = 'show';
        document.addEventListener('click', this.checkClickedNode.bind(this))
    }
    callServer(searchString) {  
        this.showResults();  // starting to show results before getting them from server  
        if (this.timeoutRef) { //checking if timeout reference was set; if it was, clear out the timeout and no server call     
            clearTimeout(this.timeoutRef)
        }

        this.timeoutRef = setTimeout(async () => { //timeout to call server
            const updatedSoqlParams = {...SOQL_PARAMS, whereCondition: `${SOQL_PARAMS.whereCondition} '${this.opco}'`}
            const searchParams = {searchString, ...updatedSoqlParams} //combining searchString value with SOQL_PARAMS object
            const search = {parameters: searchParams, ...BD_HELPER_PARAMS} //combining searchParams with BD_HELPER_PARAMS
             try {                
                const results = await performServerCall(search);  //wait for the response from server            
                const parsedResults = JSON.parse(results);
        
                this.results = parsedResults.map(skill => skill.value) //iterate over the skills and reduce the array into simple string array; set it to display results
            } catch (err) {
                console.log(err)
            }
        },300)
    }
    handleDupeMsg(e) {
        const val = e.detail || e
        this.dispatchEvent(new CustomEvent('dupe', {detail: {val}}))
    }
}