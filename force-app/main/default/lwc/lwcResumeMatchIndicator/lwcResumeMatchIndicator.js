import { LightningElement, api, track } from 'lwc';

export default class LwcResumeMatchIndicator extends LightningElement {
    @api match = 0;
    @track indicator = '';
    @api matchability = 0; 
	@api message ='';
	@api duplicateMatchFlags='';

    connectedCallback() {
		
        this.matchability = Number.parseFloat(this.match * 100).toPrecision(2);        
        if (this.match < 0.5) {
            this.indicator = 'single'
        } else if (this.match >= 0.5 && this.match <= 0.9) {
            this.indicator = 'double'
        } else {
            this.indicator = 'tripple';
        }
    }
}