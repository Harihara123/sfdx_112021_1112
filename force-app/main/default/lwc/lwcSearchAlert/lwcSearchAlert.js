import { LightningElement, api } from 'lwc';

export default class LwcSearchAlert extends LightningElement {
    @api alert
    @api currentAlert

    @api emailStatus


    onClick(e) {
        this.dispatchEvent(new CustomEvent('action', 
            {detail: {
                action: e.target.getAttribute('data-action'), 
                alert: e.currentTarget.getAttribute('data-id')
            }}
        ));
    }

    get showAlerts() {
        return (this.alert.Result_Count_V2__c && this.alert.Result_Count_V2__c > 0)
    }
    get status() {
        return (this.emailStatus?'email-on':'email-off')
    }
    get checkCurrent() {
        return (this.alert.Id === this.currentAlert?'current':'');
    }
}