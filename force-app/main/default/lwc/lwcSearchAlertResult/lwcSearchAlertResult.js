import { LightningElement, api } from 'lwc';

import ConnectedUrl from '@salesforce/label/c.ConnectedOneSlashURL';

export default class LwcSearchAlertResult extends LightningElement {
    @api result

    label = {
        ConnectedUrl
    }

    renderedCallback() {
        this.getTeaser()
    }

    get getFullName() {
        const {givenName, familyName} = this.result;
        return (givenName+' '+familyName)
    }
    get getFormattedUrl() { 
        // return ('/lightning/r/Contact/' + this.result.talentContactId + '/view')
        return `${this.label.ConnectedUrl}/sObject/${this.result.candidateId}/view`;
    }
    get getLocation() {
        let returnArr = [];
        ['city', 'state', 'country'].forEach(item => {
            if(this.result[item] && this.result[item] !== 'null') {
                returnArr.push(this.result[item]) 
            }            
        })
        return returnArr;
    }
    get getTriggerDate() {
        if (this.result.alertTrigger.length > 0 && this.result.alertTrigger[0] === 'Resume Updated' && (this.result.resumeLastModifiedDate)) {
            let d = this.formatDate(this.result.resumeLastModifiedDate);
            return `Resume Updated Date: ${d}`;
        } else if (this.result.createdDate !== null && (this.result.createdDate)) {
            let d = this.formatDate(this.result.createdDate);
            return `Created Date: ${d}`;
        } else {
            return `Created Date: N/A`;
        }
    }

    formatDate(d) {
        d = new Date(d);
        const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' });
        const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat.formatToParts(d);

        return `${month}-${day}-${year }`;
    }

    getTeaser() {
        let teaserContainer = this.template.querySelector('[data-id=teaser]');
        //console.log(teaserContainer);
        teaserContainer.innerHTML = this.result.teaser;
    }
}