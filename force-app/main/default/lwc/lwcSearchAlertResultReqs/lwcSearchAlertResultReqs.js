import { LightningElement, api } from 'lwc';

// import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import ConnectedUrl from '@salesforce/label/c.ConnectedOneSlashURL';
import ATS_OWNER from '@salesforce/label/c.ATS_OWNER';
import ATS_STAGE from '@salesforce/label/c.ATS_STAGE';
import ATS_CREATED_DATE from '@salesforce/label/c.ATS_CREATED_DATE';
import ATS_DURATION from '@salesforce/label/c.ATS_DURATION';
import ATS_BILL_RATE from '@salesforce/label/c.ATS_BILL_RATE';
import ATS_REMOTE from '@salesforce/label/c.ATS_REMOTE';
import ATS_SUBVENDOR from '@salesforce/label/c.ATS_SUBVENDOR';
import ATS_SKILLS from '@salesforce/label/c.ATS_SKILLS';

export default class LwcSearchAlertResultReqs extends LightningElement {

    @api results;


    label = {
        ConnectedUrl,
        ATS_OWNER,
        ATS_STAGE,
        ATS_CREATED_DATE,
        ATS_DURATION,
        ATS_BILL_RATE,
        ATS_REMOTE,
        ATS_SUBVENDOR,
        ATS_SKILLS
    }


    get getUrl(){
        return `${this.label.ConnectedUrl}/sObject/${this.results.jobId}/view`;
    }

    get getJobTitle() {
        return `${this.results.positionTitle}@ ${this.results.accountName}`;
    }

    get getLocation() {
        let returnArr = [];
        ['city', 'state', 'country'].forEach(item => {
            if (this.results[item] && this.results[item] !== 'null') {
                returnArr.push(this.results[item])
            }
        })
        return returnArr;
    }

    get getDuration() {
        if (this.getResults(this.results.jobDuration) === 'N/A'){
            return 'N/A';
        }
        let durationUnit = this.results.jobDurationUnit.replace(/[()]/g, '');
        if(this.results.jobDuration.length === 1){
            durationUnit = durationUnit.substring(0, durationUnit.length - 1);
        }
        return `${this.results.jobDuration} ${durationUnit}`;
    }

    get getBillRate() {
        let jobMinRate = this.getResults(this.results.jobMinRate),
            jobMaxRate = this.getResults(this.results.jobMaxRate);
        let billRate;

        if (jobMinRate === 'N/A' && jobMaxRate === 'N/A'){
            billRate = 'N/A';
        } else if (jobMinRate === 'N/A'){
            billRate = `$${parseInt(jobMaxRate, 10)}/hr`;
        } else if (jobMaxRate === 'N/A') {
            billRate = `$${parseInt(jobMinRate, 10)}/hr`;
        } else {
            billRate = `$${parseInt(jobMinRate, 10)} - ${parseInt(jobMaxRate, 10)}/hr`;
        }

        return billRate;
    }

    get getOwner() {
        return this.getResults(this.results.jobOwner);
    }

    get getSkillsTeaser(){
        return this.getResults(this.results.skills);
    }

    get getTriggerDate() {
        if (this.results.alertMatchTimestamp.length > 0 && (this.results.jobCreatedDate)) {
            let d = this.formatDate(this.results.jobCreatedDate);
            return d;
        } else {
            return `N/A`;
        }
    }

    formatDate(d) {
        d = new Date(d);
        const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' });
        const [{ value: month }, , { value: day }, , { value: year }] = dateTimeFormat.formatToParts(d);

        return `${month} ${day}, ${year}`;
    }

    getResults(res) {
        if ((res || []).length > 0) {
            return res;
        }
        return 'N/A';
    }

}