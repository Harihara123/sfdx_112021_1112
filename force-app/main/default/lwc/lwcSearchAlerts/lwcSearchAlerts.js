import { LightningElement, track, api } from 'lwc';

//APEX Methods:
import getSearchAlertCriteria from '@salesforce/apex/SearchController.getSearchAlertCriteria'
import getUserAlertResultMap from '@salesforce/apex/SearchController.getUserAlertResultMap'
import setAlertCriteriaEmailPreference from '@salesforce/apex/SearchController.setAlertCriteriaEmailPreference'
import deleteAlertByID from '@salesforce/apex/SearchController.deleteAlertByID'
import resetSeenResultCount from '@salesforce/apex/SearchController.resetSeenResultCount'
import resetNewResultFlag from '@salesforce/apex/SearchController.resetNewResultFlag'
import checkGroupPermission from '@salesforce/apex/SearchController.checkGroupPermission'
//checkGroupPermission

export default class LwcSearchAlerts extends LightningElement {

    @track emptyText = 'To create an alert, use an existing or new saved search to set one up.';
    @track showSpinner;
    @track showAlertsSpinner;
    @track activeTab = 'T';
    @track state = {
        userGroup: '',
        val: '',
        alerts: {},
        results: [],
        currentResults: 0,
        currentAlert: '',
        resultRenderLimit: 15,
        shownResults: 0,
        totalCurrentResults: 0,
        unseenAlerts: false,
    }
    alertResults = []
    // haveTalentAlerts = false;
    // haveReqAlerts = false;

    connectedCallback() {
        //this.init();
        this.checkFlag();
    }
    getAlerts(fx = (response) => { }) {
        getSearchAlertCriteria()
            .then((res) => {
                fx(res);
            })
            .catch((err) => {
                this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
            })
    }
    checkFlag() {
        getSearchAlertCriteria()
            .then((res) => {
                let unseenAlerts = res.findIndex(alert => alert.NewResultFlag__c);
                if (unseenAlerts !== -1) { this.state.unseenAlerts = true }
                this.highlightUtilityBar(this.state.unseenAlerts)
            })
            .catch((err) => {
                this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
            })

        // this.getAlerts((res) => {
        //     let unseenAlerts = res.findIndex(alert => alert.NewResultFlag__c);
        //     if (unseenAlerts !== -1) {this.state.unseenAlerts = true};
        //     this.highlightUtilityBar(this.state.unseenAlerts)
        // });
    }
    @api
    init() {
        checkGroupPermission()
            .then(response => {
                this.state.userGroup = response;
                //Commented w.r.t S-146370 Re-enable the Alerts Notification on Utility Bar
                //if (!response.isInGroupB) {
                this.showAlertsSpinner = true;
                getSearchAlertCriteria().then(res => {

                    if (res && res.length > 0) {

                        let talentAlert = [],
                            reqAlert = [];
                        // let talentAlert = [];

                        res.forEach(item => {
                            if (item.Alert_Type__c && item.Alert_Type__c === 'R') {
                                reqAlert.push(item);
                            } else {
                                talentAlert.push(item);
                            }
                        });

                        this.state.alerts.talent = talentAlert;

                        this.state.alerts.req = reqAlert;
                        this.showAlertsSpinner = false;

                        if (this.state.unseenAlerts) { this.resetUtilityBar() }

                        if (this.activeTab.includes('T')) {
                            if (talentAlert.length > 0) {
                                this.state.currentAlert = talentAlert[0].Id;
                            }
                        } else {
                            if (reqAlert.length > 0) {
                                this.state.currentAlert = reqAlert[0].Id;
                            }
                        }
                       

                        this.getResults(this.state.currentAlert);
                    } else {
                        this.showAlertsSpinner = false;
                    }
                }).catch(err => {
                    //console.log(err)
                    this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
                })
                //Commented w.r.t S-146370 Re-enable the Alerts Notification on Utility Bar   
                /*} else {
                    this.emptyText = 'You are participating in a Search experiment. This feature is not currently supported.';
                }*/
            })
            .catch(err => {
                //ERR
            })
    }

    handleTabSelect(e) {
        const selectedTab = e.target.value;
        this.activeTab = selectedTab;

        if (selectedTab.includes('T')) {
            if ((this.state.alerts.talent || []).length > 0) {
                this.state.currentAlert = this.state.alerts.talent[0].Id;
            }
        } else {
            if ((this.state.alerts.req || []).length > 0) {
                this.state.currentAlert = this.state.alerts.req[0].Id;
            }
        }
        this.state.results = [];
        this.getResults(this.state.currentAlert);
    }

    resetUtilityBar() {
        // console.log('resetting UB apex')
        resetNewResultFlag()
            .then(res => {
                //success
            })
            .catch(err => {
                this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
            })
    }

    getResults(alertId) {
        this.showSpinner = true;
        this.state.currentResults = 0;
        this.resetCounter(alertId);
        getUserAlertResultMap({ alertId })
            .then((response) => {
                this.parseResults(response)
                this.showSpinner = false;
            })
            .catch(err => {
                this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
            })
    }

    resetCounter(alertId) {
        let newState = JSON.parse(JSON.stringify(this.state))

        let currentTab = this.activeTab.includes('T') ? newState.alerts.talent : newState.alerts.req;

        const index = currentTab.findIndex(alert => alert.Id === alertId);

        if (index !== -1) {
            currentTab[index].Result_Count_V2__c = 0;
            this.state = newState;

            resetSeenResultCount({ alertId })
                .then((response) => {
                    //SUCCESS MSG
                })
                .catch((err) => {
                    //ERROR MSG
                    this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");
                })
        }

    }
    parseResults(response) {
       
        let results = [];
        
        Object.keys(response).forEach((date) => {
            let ress = [];

            response[date].forEach((item) => {
                ress.push(JSON.parse(item));
            });
            results.push({ date, results: ress, items: response[date].length });
        });

        this.state.results = results;
        
    }
    handleAction(e) {
        //console.log('action fired!', e.detail.action, e.detail.alert)

        const actions = {
            emailOnOff: (alertId) => {
                this.toggleEmailStatus(alertId);
            },
            delete: (alertId) => {
                this.deleteAlert(alertId);
            }

        }
        if (!e.detail.action) {
            this.state.currentAlert = e.detail.alert;
            this.getResults(e.detail.alert)
            //getUserAlertResultMap({alertId: e.detail.alert}).then(res => {this.parseResults(res)}).catch(err => console.log(err))
        } else {
            actions[e.detail.action](e.detail.alert);
        }
    }
    toggleEmailStatus(alertId) {
        //console.log(alertId);
        let updatedAlerts = JSON.parse(JSON.stringify(this.state))

        let currentTab = this.activeTab.includes('T') ? updatedAlerts.alerts.talent : updatedAlerts.alerts.req;
        const index = currentTab.findIndex(alert => alert.Id === alertId);

        if (index !== -1) {
            currentTab[index].Send_Email__c = !currentTab[index].Send_Email__c
            this.state = updatedAlerts;

            setAlertCriteriaEmailPreference({ alertId })
                .then((response) => {
                    // SUCCESS TOAST MSG HERE
                    this.showToast("Email Notification Preference Updated Successfully", "Success", "success");
                })
                .catch((err) => {
                    //ERROR TOAST MSG HERE
                    this.showToast("Some error has occured, please contact your System Administrator ", "Error", "error");

                })
        }
    }
    deleteAlert(alertId) {
        let newState = JSON.parse(JSON.stringify(this.state));
        let currentTab = this.activeTab.includes('T') ? newState.alerts.talent : newState.alerts.req;
        const index = currentTab.findIndex(alert => alert.Id === alertId);

        if (index !== -1) {
            currentTab.splice(index, 1)
            this.state = newState;

            deleteAlertByID({ alertId })
                .then((res) => {
                    //SUCCESS DELETION TOAST MSG
                    this.showToast("Alert Deleted Successfully", "Success", "success");
                    let evt = new CustomEvent('updatehistorytab');
                    this.dispatchEvent(evt);
                })
                .catch((err) => {
                    //ERROR DELETION TOAST MSG
                    this.showToast("Failed to delete Alert", "Error", "error");
                })
        }
    }
    get getCurrentResults() {
        if (this.state.results[this.state.currentResults]) {
            let returnObj = Object.assign({}, this.state.results[this.state.currentResults]);
            this.state.totalCurrentResults = this.state.results[this.state.currentResults].results.length;
            let results = returnObj.results.filter((item, index) => index < this.state.resultRenderLimit)
            returnObj.results = results;
            this.state.shownResults = results.length
            return returnObj
        }
        return { results: [] };
    }

    setDefaultSection(e) {
        let index = e.currentTarget.getAttribute('data-index');
        //console.log(index, this.state.currentResults)
        if (parseInt(index, 10) !== parseInt(this.state.currentResults, 10)) {
            const container = this.template.querySelector("[data-id='results-container']");
            container.scrollTop = 0;
            this.state.resultRenderLimit = 15;
            this.state.currentResults = parseInt(index, 10);
        } else {
            this.state.currentResults = -1
            this.state.resultRenderLimit = 15;
        }
    }
    loadMore() {
        this.state.resultRenderLimit = this.state.resultRenderLimit + 15;
    }
    loadAlert(e) {
        let index = e.target.getAttribute('data-index');
        //console.log(index)
        this.state.currentResults = parseInt(index, 10);
    }
    get showMore() {
        //console.log('checking showMore', this.state.resultRenderLimit, this.state.totalCurrentResults)
        return this.state.resultRenderLimit < this.state.totalCurrentResults
    }
    get haveAlerts() {
        return Object.entries(this.state.alerts).length > 0;
    }
    get haveResults() {
        return this.state.results.length > 0
    }
    get haveTalentAlerts() {
        return this.state.alerts.talent.length > 0;
    }
    get haveReqAlerts() {
        return this.state.alerts.req.length > 0;
    }
    showToast(message, title, toastType) {
        let evt = new CustomEvent('showtoast', { detail: { message, title, toastType } });
        this.dispatchEvent(evt);
    }
    highlightUtilityBar(highlight) {
        let evt = new CustomEvent('glagub', { detail: { highlight } });
        this.dispatchEvent(evt);
    }
}