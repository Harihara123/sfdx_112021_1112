import { LightningElement, api } from 'lwc';

const SECTION_MAP = {
    'slds-section slds-is-open': 'slds-section',
    'slds-section': 'slds-section slds-is-open'
}

export default class LwcSection extends LightningElement {
    @api title = '';
    @api collapsed = false;
    sectionClass = 'slds-section slds-is-open'

    connectedCallback() {
        if (this.collapsed) {
            this.sectionClass = 'slds-section'
        }
    }

    open() {
        this.sectionClass = 'slds-section slds-is-open'
    }
    close() {
        this.sectionClass = 'slds-section'        
    }
    toggleSection(e) {
        console.log(e.target.getAttribute('data-action'))
        this.sectionClass = SECTION_MAP[this.sectionClass]
    }
}