import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {ConnectedLogApi} from 'c/connectedLogUtils';

import LANG from '@salesforce/i18n/lang';
import TIME_ZONE from '@salesforce/i18n/timeZone';

import checkContactSelfReportedInfo from '@salesforce/apex/SelfReportedInfo.checkContactSelfReportedInfo';
// LABELS
import ATS_SELF_REPORTED_INFO from '@salesforce/label/c.ATS_SELF_REPORTED_INFO';
import ATS_SKILLS from '@salesforce/label/c.ATS_SKILLS';
import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import ATS_TALENT_COMM_PROFILE from '@salesforce/label/c.ATS_TALENT_COMM_PROFILE';
import COMM_INFO_TEXT from '@salesforce/label/c.COMM_INFO_TEXT';

const connectedLogAPI = new ConnectedLogApi();

export default class LwcSelfReportedInfo extends LightningElement {
    label = {
        ATS_SELF_REPORTED_INFO,
        ATS_JOB_TITLE,
        ATS_SKILLS,
        ATS_TALENT_COMM_PROFILE,
        COMM_INFO_TEXT
    };
    data = {};
    jobTitle;
    skillsList;
        
    @api recordId;
    @api enableButton;
    popoverEv = this.handleEvent.bind(this);
    
    togglePopover = false;
    showCaret;
    iconFill;
    popoverClass;
    buttonClass;
    hover = false;
    showingPopover = false;
    
    showDate;
    showJobTitle = false;
    showSkills = false;
    showPopover = false;

    initRendered = false;

	//@api runOnce= false;

    connectedCallback(){
        console.log('inside callback---lwcSelfReportedInfo-js-------');
        connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/pagerefresh', Method: 'connectedCallback', Module: 'talentLanding'});
        checkContactSelfReportedInfo({recordId: this.recordId})
            .then(response => {
                if (response) {
					//connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/pagerefresh', Method: 'connectedCallback', Module: 'talentLanding'});
                    connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/loaded', Method: 'connectedCallback', Module: 'talentLanding'});
                    const result = JSON.parse(response);
					//console.log('result.Profile.LastUpdate.UpdateDate-------'+result.Profile.LastUpdate.UpdateDate);
                    if (result.Profile.LastUpdate.UpdateDate) {
                        this.data.updatedDate = `${this.label.ATS_TALENT_COMM_PROFILE} ${this.formatDate(result.Profile.LastUpdate.UpdateDate)}`;
                    }
					//console.log('result.Profile.JobTitle.Value-------'+result.Profile.JobTitle.Value);
                    if (result.Profile.JobTitle.Value) {					
                        this.showJobTitle = true;
                        this.data.jobTitle = result.Profile.JobTitle.Value;
                    }
					//console.log('result.Profile.Skill.length-------'+result.Profile.Skill.length);
                    if (result.Profile.Skill.length > 0) {
                        this.showSkills = true;
                        this.data.skills = this.formatSkillData(result.Profile.Skill);
                    }
                    this.showPopover = true;
                    this.iconFill = this.getIconFill();
                    this.popoverClass = this.getPopoverClass();
                    this.buttonClass = this.getButtonClass();
                    this.showCaret = (this.enableButton === 'true' ? true : false);

                    let contentLoaded = this.showPopover;
                    const dataLoadedEvt = new CustomEvent('dataloaded',{
                        detail: { contentLoaded }
                    });
                    this.dispatchEvent(dataLoadedEvt);
                } 
				else{
					
					//connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/pagerefresh', Method: 'connectedCallback', Module: 'talentLanding'});
					//console.log('lwcSelfReportedInfo-js-----Else Block--');
                    
				}
            })
            .catch(error => {
            });
            
    }

    renderedCallback(){
        if(!this.initRendered && this.showPopover){     
            const togglePopover = this.template.querySelector(".toggle-popover");
            if (this.enableButton === 'true')  {
                togglePopover.addEventListener('click', this.popoverEv);
            } else {
                this.hover = true;
                togglePopover.addEventListener('mouseover', this.handleHover);
            }     
            // const eventType = this.enableButton === 'true' ? 'click' : 'mouseover';
            // const togglePopover = this.template.querySelector(".toggle-popover");
            // togglePopover.addEventListener(eventType, this.popoverEv);
           
        }
    }

    formatSkillData(data){
        // data.forEach((item) => {
        //     const skills = {};
        //     skills.value = item.Skills;
        //     skills.id = `${item.ID}-${this.guid()}`;
        //     skillArr.push(skills);
        // });
        const parsedSkills = data[0].Skills.split(',');
        return parsedSkills.map(skill => ({value: skill, id: this.guid()}));

    }

    getPopoverClass(){
        return `${(this.enableButton === 'true') ? 'sri-popover' : 'sri-popover-hover slds-nubbin_top-left'} slds-popover slds-hide`;
    }

    getButtonClass(){
        return this.enableButton === 'true' ? 'toggle-popover' : 'toggle-popover hover';
    }

    getIconFill(){
        let iconColor = {
            grey: '#949DA9',
            white: '#FFFFFF'
        }
        if(this.enableButton === 'true'){
            return iconColor.grey;
        }
        return iconColor.white;
    }

    guid(){
        return `${this.s4()}${ this.s4()}-${ this.s4()}${this.s4()}`;  
    }

    s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    formatDate(date) {
        const options = {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
            timeZone: TIME_ZONE
        };
        const dateTimeFormat = new Intl.DateTimeFormat(LANG, options);
        const pastDate = new Date(date);
        
        return dateTimeFormat.format(pastDate);
    }

    toggleSection(e) {        
        const el = this.template.querySelector('section');

        if (el.classList.contains('slds-hide')) {
            connectedLogAPI.logInfo(this.template,{LogTypeId: 'ExternalData/Communities/hover', Method: 'toggleSection', Module: 'talentLanding'});
            el.classList.remove('slds-hide');
            document.addEventListener('click', this.popoverEv);
            this.showingPopover = true;

            if (this.hover) {
                el.addEventListener('mouseleave', this.popoverEv)
            }

        } else if (!el.contains(e.path[0])) {
            document.removeEventListener('click', this.popoverEv);
            el.classList.add('slds-hide');
            this.showingPopover = false;

            if (this.hover) {
                el.removeEventListener('mouseleave', this.popoverEv)
            }

        }       
    }

    handleEvent(e) {
        this.toggleSection(e);
        e.stopPropagation();
    }

    handleHover = (e) => {
        if (!this.showingPopover) {
            this.toggleSection(e);
        }
    }
    
    showToast(message, title, type) {
        let evt = new ShowToastEvent({ 
            title: title,
            message: message,
            variant: type
        });

        this.dispatchEvent(evt);
    }
    handleCopy(e) {
        connectedLogAPI.logInfo(this.template,{
            LogTypeId: 'ExternalData/Communities/copy',
            Method: 'handleCopy',
            Module: 'talentLanding',
            AdditionalData: {
                DataElement: e.target.getAttribute('data-type'),
                Value: window.getSelection().toString()
            } 
        });
    }

};