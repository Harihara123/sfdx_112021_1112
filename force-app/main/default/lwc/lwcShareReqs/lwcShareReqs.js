import { LightningElement, api, track } from 'lwc';
//import getContacts from '@salesforce/apex/ReqRoutingController.getRecruiters';
import getContactsToShare from '@salesforce/apex/ReqRoutingController.getContactsToShare';
import getPastSharedUsers from '@salesforce/apex/ReqRoutingController.getPastSharedUsers';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import currentUserId from "@salesforce/user/Id";
import shareUserActions from "@salesforce/apex/ReqRoutingController.shareUserActions";



const columns = [
    { label: 'Name', fieldName: 'recordLink', type: 'url', wrapText: false, hideDefaultActions: true, typeAttributes: { label: { fieldName: "Name" }, tooltip:{ fieldName: "Name" }, target: "_blank" } },
    { label: 'Email', fieldName: 'Email', type: 'email', wrapText: false, hideDefaultActions: true },
    { label: 'Phone', fieldName: 'Phone', type: 'phone', wrapText: false, hideDefaultActions: true },
    { label: 'Office', fieldName: 'Office__c', wrapText: false, hideDefaultActions: true },
    { label: 'Title', fieldName: 'Title', wrapText: false, hideDefaultActions: true },    
];


export default class LwcShareReqs extends LightningElement {
    @track isModalOpen = false;
    @track hasPastContacts = false;
    @track foundNoContactsOnSearch = false;    
    pastContactsData = [];
    findContactsData = [];
    allSearchResult = [];
	selectedRow;
	selectedRecords;
	finalSelectedRecords=[];
    columns = columns;
    queryTerm;
	OppId;
    sortBy;
    sortDirection;
    isLoaded = false;
	toastMessage;

	parameterObject;
	messagetouser;
    connectedCallback() {
                        
    }

    @api openModal(flag,oppid) {		
        this.isModalOpen = flag;
		this.OppId=oppid;
		this.pastContactData();
    }

	@api pastContactData(){
		getPastSharedUsers({OppId: null})
            .then((result) => {
                console.log('result:'+JSON.stringify(result));
                if(result !== null) {
                    let tempContactsData=[];
                    for (let i = 0; i < result.length; i++) {  
                        let tempRecord = Object.assign({}, result[i]); //cloning object  
                        if(tempRecord.UserRoleId != undefined && tempRecord.UserRoleId != null) {
                            tempRecord.userRoleName = tempRecord.UserRole.Name;
                        }    
                        tempRecord.recordLink = "/" + tempRecord.Id;
                        tempContactsData.push(tempRecord);  
                    }
                    this.pastContactsData=tempContactsData;                    
                    this.hasPastContacts = true;                       
                    console.log(this.pastContactsData);
                    let divblock = this.template.querySelector('[data-id="past-contacts"]');
                    if(divblock){
                        this.template.querySelector('[data-id="past-contacts"]').classList.add('set-height');
                    }
                } else {
				    this.hasPastContacts = false;    
                    let divblock = this.template.querySelector('[data-id="past-contacts"]');
                    if(divblock && this.template.querySelector('[data-id="past-contacts"]').classList.contains('set-height')){
                        this.template.querySelector('[data-id="past-contacts"]').classList.remove('set-height');
                    }          
                }
                //this.dispatchEvent(evt);
            })
	}

    addMoreData() {
        const offset = this.findContactsData.length;
        let newData = [];
        if(this.allSearchResult.length > 0) {
            newData = this.allSearchResult.splice(0,5);
            this.findContactsData = [...this.findContactsData, ...newData];
        }   
        //this.addMoreDataCounter++;
    }

    onLoadMoreHandler() {
        console.log('load more');
        this.addMoreData();

        /*if(this.addMoreDataCounter === 4) {
            this.isLoaded = false;
        }*/
        if(this.allSearchResult.length === 0) {
            this.isLoaded = false;
        }
    }
    
    closeModal() {        
        this.isModalOpen = false;
        console.log(this.queryTerm);
        this.foundNoContactsOnSearch = false;
        this.findContactsData = [];
        this.allSearchResult = [];
		this.clearData();
        var divblock = this.template.querySelector('[data-id="search-table"]');
        if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
            this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
        }        
    }

	handleRowSelection (event){
        this.selectedRow=event.detail.selectedRows;        
    }

	getSelectedRecords (event){
		this.selectedRecords=event.detail.selectedRows;
	}

    /*submitDetails(evt) {
        this.isModalOpen = false;
        console.log(this.queryTerm);
		if(this.selectedRow !=undefined && this.selectedRow != null){
			this.selectedRow.forEach((item) => this.finalSelectedRecords.push(item));
		}
		if(this.selectedRecords !=undefined && this.selectedRecords != null){
            this.selectedRecords.forEach((item) => this.finalSelectedRecords.push(item));
		}
        if(this.finalSelectedRecords.length == 0) {
            this.toastMessage = 'No user selected. Please, select at least one.'
            this.showInfoToast('error');
        }
        else {
            saveSharedContact({ RecruiterData: this.finalSelectedRecords,OppId: this.OppId })
                .then((result) => {
                    console.log('result:'+JSON.stringify(result));
                    if(result !== null) {
                        if(result.createdRecords !== undefined){
                            this.toastMessage= result.createdRecords+ ' User(s) have been allocated' ;
                            this.showInfoToast('success');
                        }                     
                        if(result.ExistingRecords !== undefined){
                            this.toastMessage= result.ExistingRecords+ ' User(s) you have selected is already added to the req';
                            this.showInfoToast('info'); 
                        } 	
                        const selectedEvent = new CustomEvent("allocate", {
                            detail: this.finalSelectedRecords
                        });
                        this.dispatchEvent(selectedEvent); 				                   
                    } else {
                        
                    }
                    this.clearData();
                    this.pastContactData();
                    
                    //this.dispatchEvent(evt);
                })
            }       
       
    }*/

    handleKeyChange(evt) {        
        this.queryTerm = evt.target.value;
        if (this.queryTerm != '') {
            //this.queryTerm = evt.target.value;            
            getContactsToShare({ searchText: this.queryTerm, OppId: this.OppId })
            .then((result) => {
                console.log('result:'+JSON.stringify(result));
                if(result !== null) {
                    let tempContactsData=[];
                    for (let i = 0; i < result.length; i++) {  
                        let tempRecord = Object.assign({}, result[i]); //cloning object  
                        if(tempRecord.UserRoleId != undefined && tempRecord.UserRoleId != null) {
                            tempRecord.userRoleName = tempRecord.UserRole.Name;
                        }    
                        tempRecord.recordLink = "/" + tempRecord.Id;  
                        tempContactsData.push(tempRecord);  
                    }
                    this.allSearchResult=tempContactsData;
                    this.findContactsData=this.allSearchResult.splice(0,5);
                    this.foundNoContactsOnSearch = true;
                    this.isLoaded = true;
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock){
                        this.template.querySelector('[data-id="search-table"]').classList.add('set-height');
                    }    
                    console.log(this.findContactsData);
                } else {
                    this.foundNoContactsOnSearch = false;
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                        this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
                    }
                }
                //this.dispatchEvent(evt);
            })
        }
        else {
            console.log(this.queryTerm);
            this.foundNoContactsOnSearch = false;
            this.findContactsData = [];
            this.allSearchResult = [];
            var divblock = this.template.querySelector('[data-id="search-table"]');
            if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
            }
        }
			//this.dispatchEvent(evt);		
    }

    handleSortdata(event) {        
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {        
        let parseData = JSON.parse(JSON.stringify(this.allSearchResult));
        let keyValue = (a) => {
            return a[fieldname];
        };
        let isReverse = direction === 'asc' ? 1: -1;
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : '';
            y = keyValue(y) ? keyValue(y) : '';
            return isReverse * ((x > y) - (y > x));
        });
        this.findContactsData = parseData;
    }

	showInfoToast(variant) {
        const evt = new ShowToastEvent({            
            message: this.toastMessage,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

	 clearData() {
		this.selectedRow=null;
		this.selectedRecords=null;
		this.finalSelectedRecords=[];
		this.foundNoContactsOnSearch = false;
        this.findContactsData = [];
        this.allSearchResult = [];
		this.toastMessage='';
		this.messagetouser='';
        var divblock = this.template.querySelector('[data-id="search-table"]');
        if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
            this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
        }        
	 }
	 messagetouserchange(event) {
        this.messagetouser= event.target.value;
    }
	 shareReqs(event) {
		if (event.target.title === "Share") {
		this.isModalOpen = false;
		console.log("messagetouser-----:" +this.messagetouser);
		if(this.selectedRow !=undefined && this.selectedRow != null){
		
			this.selectedRow.forEach((item) => this.finalSelectedRecords.push(item));
		}
		if(this.selectedRecords !=undefined && this.selectedRecords != null){
            this.selectedRecords.forEach((item) => this.finalSelectedRecords.push(item));
		}
        if(this.finalSelectedRecords.length == 0) {
            this.toastMessage = 'No user selected. Please, select at least one.'
            this.showInfoToast('error');
        }
        else {
			this.parameterObject = {
				opportunityId: this.OppId,
				currentUserId: currentUserId,
				actionType: event.target.title,
				messagetouser: this.messagetouser,
				targetUser: this.finalSelectedRecords				
			};
			console.log("this.finalSelectedRecords-----:" +JSON.stringify(this.finalSelectedRecords));
			console.log("this.parameterObject-----:" +JSON.stringify(this.parameterObject));
			shareUserActions({ actions: this.parameterObject })
				.then((result) => {
					console.log('result:'+JSON.stringify(result));
                    if(result !== null) {
                        if(result.createdRecords !== undefined){
                            this.toastMessage= 'Req has been shared with '+result.createdRecords+ ' user(s).' ;
							this.showInfoToast('success');
                        }                     
                        if(result.ExistingRecords !== undefined){
                            this.toastMessage= result.ExistingRecords+ ' User(s) you have selected is already shared to the req';
                            this.showInfoToast('info'); 
                        } 	
                        const selectedEvent = new CustomEvent("share", {
                            detail: this.finalSelectedRecords
                        });
                        this.dispatchEvent(selectedEvent); 				                   
                    } else {
                        
                    }
                    this.clearData();
                    this.pastContactData();
				})			
			}//end of else
		}
	}
}