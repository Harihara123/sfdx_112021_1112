import { LightningElement, api } from 'lwc';

import getAgnosticFilters from '@salesforce/apex/SmartSearchController.getAgnosticFilters';

export default class LwcSmartSearch extends LightningElement {
    // Suggested Pills
    _suggestions;
    get suggestions() {
        return this._suggestions;
    }
    set suggestions(value) {
        this._suggestions = value;

        // Don't show the response if cancel is true
        // This happens when the 'Enter' button is pressed before the response comes back
        if (this.cancelRequest) {
            this.cancelRequest = false;
        }
        else {
            this.showSuggestions = (this._suggestions.suggestions && this._suggestions.suggestions.length > 0);
            this.showResults = this.showSuggestions;
        }
        setTimeout(()=>{ this.resizeSearchWindows(); }, 5);
    }

    // Selected Pills
    selectedCountLabel = '';
    selectedOutput;
    selectedOutputInline;
    _selected = [];
    get selected() {
        return this._selected;
    }
    set selected(value) {
        this._selected = value;

        let iSelected = this._selected.length;
        this.selectedCountLabel = iSelected + ' selected'; // Set label for front end

        if (iSelected > 0) {
            this.hasError = false; // Set error to false once pills are selected
            this._isSmartSearch = true;
        }
        else {
            this._isSmartSearch = false;
        }

        if (this.showResults)
            this.convertForOutput(this.responseData);

        this.updateSelectedForOutput();

        this.showSelected = (this._selected.length > 0);
    }

    _searchValueInput = '';
    @api 
    get searchValueInput() {
        return this._searchValueInput;
    }
    set searchValueInput(value) {
        this._searchValueInput = value;
        this.searchValue = this._searchValueInput;
        this.template.querySelector('.search-input').value = this._searchValueInput;
    }

    searchValue = '';
    responseData;
    cancelRequest = false;

    // Used in C_SearchCriteria.
    // If user has pills selected, perform Smart Search.
    // Else, perform boolean search.
    _isSmartSearch = false;
    @api isSmartSearch() { return this._isSmartSearch; }
    
    showSummary = false;
    showResults = false; // Show results window
    showSuggestions = false; // Show Suggestions from API
    showSelected = false; // Show selected options
    showCount = false;

    typeArray = ['skill', 'title', 'skillset', 'company', 'industry', 'job_function']; // Must match the json response field

    cancelBlur = false;
    timer = 0;
    @api hasError;
    @api errors;

    /**
     * Lifecycle hook
     */
    connectedCallback() {
        
    }

    @api
    getQueryStringValue() {
        return this.sendSearchFilters();
    }

    @api
    getSelectedTerms() {
        this.showResults = false;
        return this.selected;
    }

    /**
     * Search input focus
     */
    handleFocus(event) {
        // this.showResults = (this.showSuggestions || this.showSelected); // Used when the selected items are in the popup
        this.showResults = this.showSuggestions;
        this.showSummary = false;

        if (this.searchValue != '')
            this.getResults(this.searchValue);
        else
            this.showResults = false;

        this.setTextareaHeight();
        setTimeout(()=>{ this.resizeSearchWindows(); }, 5);
    }

    /**
     * Search input blur
     */
    handleBlur(event) {
        if (!this.cancelBlur) { 
            this.showResults = false;
            this.resetTextareaHeight();

            if (this.selected.length > 0)
                this.showSummary = true;
            
            if (this._isSmartSearch) {
                this.clearHideResults();
            }
        }
        else if (this.showResults) { // if 'no results' message should show
            this.template.querySelector('.search-input').focus();
            this.showResults = true;
            this.showSummary = false;
        }
        else { // actually blur
            this.template.querySelector('.search-input').blur();
            this.resetTextareaHeight();
        }
        
        this.cancelBlur = false;
    }

    /**
     * Cancel blur from input if anything in div is clicked
     */
    handleCancelBlur(event) {
        if (event.target.dataset.removebutton && this.showResults) {
            this.showSuggestions = true;
            this.cancelBlur = true;
        }
        else if (event.target.dataset.overrideblur) {
            this.cancelBlur = true;
        }
    }

    /**
     * Closes suggestion window
     */
    closeWindow(event) {
        event.stopPropagation();
        this.clearHideResults();
    }

    // Clears results and hides the popup
    clearHideResults() {
        this.showResults = false;

        if (this._selected.length > 0)
            this.showSummary = true;

        this.suggestions = {suggestions:[]};
        
        if (this._isSmartSearch) {
            this.searchValue = '';
            this.template.querySelector('.search-input').value = '';
        }
    }

    // Clears selected
    clearSelected() {
        this.selected = [];
        this.showSummary = false;

        this.template.querySelector('.search-input').focus();
    }

    focusInput() {
        this.showSummary = false;

        this.template.querySelector('.search-input').focus();
    }

    /**
     * Handles keyup event from search box
     * Waits 500 milliseconds for user to stop typing before making request
     * 
     * @param {HTML event} event 
     */
    handleKeyUp(event) {
        if (event.code == 'Escape') { // On escape, just close results
            this.showResults = false;
        }
        else if (event.code == 'Enter' && !this._isSmartSearch) { // On enter, if NOT smart search, perform Boolean search
            this.cancelRequest = true;
            event.preventDefault();
            event.stopPropagation();
            this.template.querySelector('.search-input').value = this.searchValue;

            this.hasError = false;
            
            this.search();
        }
        else if (event.code == 'Enter' && this._isSmartSearch) { // On enter, if smart search, perform match
            this.template.querySelector('.search-input').value = this.searchValue;

            this.hasError = false;
            this.smartSearch();
        }
        else { // Else, just allow typing and set height appropriately
            this.setTextareaHeight();

            let searchValue = event.target.value;
            this.searchValue = searchValue;

            if (searchValue != '') {
                this.hasError = false;
                this.getResults(searchValue);
            }
            else
                this.suggestions = {suggestions:[]};
        }
    }
    
    setTextareaHeight() {
        // Set height to show all of boolean
        if (!this._isSmartSearch) {
            let taWrapper = this.template.querySelector('.search-input-wrapper');
            let ta = this.template.querySelector('textarea.search-input');

            this.resetTextareaHeight();

            const {clientHeight, scrollHeight } = ta;

            if(scrollHeight > clientHeight + 2) {
                ta.style.paddingTop = '5px';
                ta.style.lineHeight = '1.3rem';
                ta.style.height = scrollHeight + 'px';
                taWrapper.style.height = (scrollHeight + 2) + 'px';
            } 
        }
        else {
            this.resetTextareaHeight();
        }
    }

    resetTextareaHeight() {
        let taWrapper = this.template.querySelector('.search-input-wrapper');
        taWrapper.removeAttribute('style');

        let ta = this.template.querySelector('textarea.search-input');
        ta.removeAttribute('style');
    }

    /**
     * Makes request to API to get suggestions for pills
     * takes one param "searchText" which is the input from the search field
     * 
     * @param {String} searchText 
     */
    getResults(searchText) {
        let qs = 'flt.group_filter=all&appId=ATS_SmartSearch&q=' + encodeURIComponent(searchText);

        getAgnosticFilters({queryString:qs}).then((result) => {
            let jsonData = JSON.parse(result);

            this.responseData = JSON.parse(JSON.stringify(jsonData.response));
            this.convertForOutput(jsonData.response);
        }).catch((error) => {
            console.log('getAgnosticFilters error', error);
        });
    }

    /**
     * Put json into format that the front end can work with
     */
    convertForOutput(jsonResponse) {
        let outputJSON = {'suggestions':[]};

        let typeDisplayValue = '';
        
        this.typeArray.forEach((type) => {
            switch (type) {
                case 'skill':
                    typeDisplayValue = 'Skills';
                    break;
                case 'title':
                    typeDisplayValue = 'Job Titles';
                    break;
                case 'skillset':
                    typeDisplayValue = 'Skill Sets';
                    break;
                case 'company':
                    typeDisplayValue = 'Companies';
                    break;
                case 'industry':
                    typeDisplayValue = 'Industries';
                    break;
                case 'job_function':
                    typeDisplayValue = 'Job Code';
                    break;
            }

            if (jsonResponse[type]) {
                let suggs = this.getSuggestions(jsonResponse[type], type, typeDisplayValue);
                if (suggs.options.length > 0) outputJSON.suggestions.push(suggs);
            }
        });
        
        if (outputJSON.suggestions.length > 0) {
            // Combine cluster and granular entities
            // Comment to not combine
            outputJSON = this.combineTypes(outputJSON, 'suggestions');

            this.suggestions = outputJSON;
        }
        else {
            this.suggestions = {suggestions:[]};
            this.showResults = this._isSmartSearch; // Show the popup to display 'No results' message, only if smart search
        }
    }

    /**
     * Format individual entity objects
     * 
     * @param {JSON object} jsonResponse 
     * @param {String} type 
     * @param {String} displayValue 
     * @returns 
     */
    getSuggestions(suggestionList, type, displayValue) {
        let id = 0;
        let formattedObject = [];

        if (suggestionList.length > 0) {
            suggestionList.forEach((item) => {
                let o = {'type':type, 'name':item.term, 'weight':item.weight, 'id':type + '_' + id + '_' + item.weight};
                id++;

                let add = true;
                this.selected.forEach((s) => {
                    if (s.type == o.type && s.name == o.name) {
                        add = false;
                    }
                });
                
                if (add) {
                    formattedObject.push(o);
                }
            });

            return {'name':displayValue, 'options':formattedObject};
        }

        return undefined;
    }

    /**
     * Combines granular and clustered entity types.
     * Job Title -> Job Code
     * Skills -> Skill Set
     * Company -> Industry
     * 
     * @param {JSON object} suggestionJson 
     */
    combineTypes(suggestionJson, type) {
        let newSuggestions = {[type]:[]};
        let addToSkills = [];
        let addToCompanies = [];
        let addToTitles = [];
        let currentSkills = [];
        let currentCompanies = [];
        let currentTitles = [];

        // Combine clusters and granulars
        suggestionJson[type].forEach((sugg) => {
            if (sugg.name.toLowerCase() == 'skills') {
                currentSkills = sugg.options;
            }
            else if (sugg.name.toLowerCase() == 'companies') {
                currentCompanies = sugg.options;
            }
            else if (sugg.name.toLowerCase() == 'job titles') {
                currentTitles = sugg.options;
            }
            else if (sugg.name.toLowerCase() == 'skill sets') {
                addToSkills = sugg.options;
            }
            else if (sugg.name.toLowerCase() == 'industries') {
                addToCompanies = sugg.options;
            }
            else if (sugg.name.toLowerCase() == 'job code') {
                addToTitles = sugg.options;
            }
        });

        // Put into new array without those clusters
        /*suggestionJson[type].forEach((sugg) => {
            if (sugg.name.toLowerCase() == 'skills') {
                let allOptions = addToSkills.concat(sugg.options);
                newSuggestions[type].push({name:'Skills', 'options':allOptions});
            }
            else if (sugg.name.toLowerCase() == 'companies') {
                let allOptions = addToCompanies.concat(sugg.options);
                newSuggestions[type].push({name:'Companies', 'options':allOptions});
            }
            else if (sugg.name.toLowerCase() == 'job titles') {
                let allOptions = addToTitles.concat(sugg.options);
                newSuggestions[type].push({name:'Job Titles', 'options':allOptions});
            }
        });*/

        // Put all into a json object
        let allSkills = addToSkills.concat(currentSkills);
        let allCompanies = addToCompanies.concat(currentCompanies);
        let allTitles = addToTitles.concat(currentTitles);
        if (allTitles.length > 0) newSuggestions[type].push({name:'Job Title/Code', 'options':allTitles});
        if (allCompanies.length > 0) newSuggestions[type].push({name:'Company/Industry', 'options':allCompanies});
        if (allSkills.length > 0) newSuggestions[type].push({name:'Skill/Set', 'options':allSkills});

        // Show icon if clustered, and sort by weight
        newSuggestions[type].forEach((sugg) => {
            /*sugg.options.sort(function(a, b) {
                return a.weight > b.weight;
            });*/

            sugg.options.forEach((item) => {
                item.showIcon = (item.type == 'skillset' || item.type == 'industry' || item.type == 'job_function');
            });
        });

        return newSuggestions;
    }

    /**
     * Add pill to this.selected
     * Remove pill from this.suggestions
     * 
     * @param {HTML event} event 
     */
    selectPill(event) {
        let pillId = event.target.dataset.pillid;
        
        let newSelected = this.selected;
        let sugg = this.suggestions;

        sugg.suggestions.forEach((s) => {
            s.options.forEach((o) => {
                if (o.id == pillId) {
                    o.searchtype = 'include';
                    newSelected.push(o);
                }
            });
        });

        this.selected = newSelected;
        
        if (this.suggestions?.suggestions?.length == 0) {
            this.showResults = true;
            this.showSuggestions = false;
        }

        setTimeout(()=>{ this.resizeSearchWindows(); }, 5);
    }

    /**
     * Remove pill from this.selected
     * Add pill to this.suggestions
     * 
     * @param {HTML event} event 
     */
    removePill(event) {
        let pillId = event.target.dataset.pillid;
        
        let newSelected = [];

        this.selected.forEach((s) => {
            if (s.id != pillId) {
                // If it doesn't match, just push it back into selected
                newSelected.push(s);
            }
        });

        this.selected = newSelected;
        this.template.querySelector('.search-input').focus();
    }

    /**
     * Toggles pill between include/exclude
     * 
     * @param {HTML event} event 
     */
    toggleIncludeExclude(event) {
        let pillId = event.target.dataset.pillid;

        let changeSelected = this.selected;

        changeSelected.forEach((i) => {
            if (i.id == pillId) {
                i.searchtype = (i.searchtype == 'include') ? 'exclude' : 'include';
            }
        });

        this.selected = changeSelected;
    }

    /**
     * Changes this.selected into format for front end
     */
    updateSelectedForOutput() {
        let selectedSkill = [];
        let selectedTitle = [];
        let selectedSkillSet = [];
        let selectedCompany = [];
        let selectedIndustry = [];
        let selectedJobCode = [];
        
        let jobInlineCount = 0;
        let companyInlineCount = 0;
        let skillInlineCount = 0;

        this._selected.forEach(element => {
            if (element.type == 'skill') {
                selectedSkill.push(element);
                skillInlineCount++;
            }
            else if (element.type == 'title') {
                selectedTitle.push(element);
                jobInlineCount++;
            }
            else if (element.type == 'skillset') {
                selectedSkillSet.push(element);
                skillInlineCount++;
            }
            else if (element.type == 'company') {
                selectedCompany.push(element);
                companyInlineCount++;
            }
            else if (element.type == 'industry') {
                selectedCompany.push(element);
                companyInlineCount++;
            }
            else if (element.type == 'job_function') {
                selectedJobCode.push(element);
                jobInlineCount++;
            }
        });

        let newOutput = { selected:[] };

        if (selectedSkill.length > 0) newOutput.selected.push({name:'Skills', options:selectedSkill});
        if (selectedTitle.length > 0) newOutput.selected.push({name:'Job Titles', options:selectedTitle});
        if (selectedSkillSet.length > 0) newOutput.selected.push({name:'Skill Sets', options:selectedSkillSet});
        if (selectedCompany.length > 0) newOutput.selected.push({name:'Companies', options:selectedCompany});
        if (selectedIndustry.length > 0) newOutput.selected.push({name:'Industries', options:selectedIndustry});
        if (selectedJobCode.length > 0) newOutput.selected.push({name:'Job Code', options:selectedJobCode});

        let tempInline = [];
        if (jobInlineCount > 0) {
            tempInline.push({name:'Job Title/Code (' + jobInlineCount + ')', type:'title'});
        }
        if (companyInlineCount > 0) {
            tempInline.push({name:'Company/Industry (' + companyInlineCount + ')', type:'company'});
        }
        if (skillInlineCount > 0) {
            tempInline.push({name:'Skill/Set (' + skillInlineCount + ')', type:'skill'});
        }

        this.selectedOutputInline = tempInline;
        this.selectedOutput = this.combineTypes(newOutput, 'selected');

        this.setTextareaHeight();
    }

    /**
     * creates a query string to pass along to C_SearchCriteria
     */
    sendSearchFilters() {
        let qs = '';

        // Skills
        let skillsArray = this.selected.filter(s => s.type == 'skill' || s.type == 'skillset').map(s => s.name);
        if (skillsArray.length > 0) qs += 'facetFlt.skills=' + encodeURI(skillsArray.join('|')) + '&';

        // Skill Set
        let skillSetArray = this.selected.filter(s => s.type == 'skillset').map(s => s.name);
        //if (skillSetArray.length > 0) qs += 'facetFlt.req_skill_specialty=' + encodeURI(skillSetArray.join('|')) + '&';

        // Titles
        let titlesArray = this.selected.filter(s => s.type == 'title').map(s => s.name);
        if (titlesArray.length > 0) qs += 'facetFlt.employment_position_title=' + encodeURI(titlesArray.join('|')) + '&';

        // Companies
        let companiesArray = this.selected.filter(s => s.type == 'company' || s.type == 'industry').map(s => s.name);
        if (companiesArray.length > 0) qs += 'facetFlt.companyName=' + encodeURI(companiesArray.join('|')) + '&';

        // Job Code
        let industryArray = this.selected.filter(s => s.type == 'industry').map(s => s.name);
        //if (industryArray.length > 0) qs += 'facetFlt.companyName=java%20ee%7Cjava%20se=' + encodeURI(industryArray.join('|')) + '&';

        qs += 'q=-asdasdasd';

        return qs;
    }

    resizeSearchWindows() {
        let isMagnified = true;
        let bottomPadding = 50;
        let screenheight = isMagnified ? screen.height / 1.5 : screen.height;
        let selectedPills = this.template.querySelector('.pills-selected');
        let options = this.template.querySelector('.pills-suggested');

        let isSelectedBelow = false;
        let isSuggestionsBelow = false;

        if (isMagnified) {
            isSelectedBelow = selectedPills && (selectedPills.getBoundingClientRect().bottom > ((screenheight - bottomPadding) / 1.5));
            isSuggestionsBelow = options && (options.getBoundingClientRect().bottom > ((screenheight - bottomPadding) / 1.5));
        }
        else {
            isSelectedBelow = selectedPills && (selectedPills.getBoundingClientRect().bottom > (screenheight - bottomPadding));
            isSuggestionsBelow = options && (options.getBoundingClientRect().bottom > (screenheight - bottomPadding));
        }

        // Selected and Suggestions are below page fold
        if (isSelectedBelow && isSuggestionsBelow) {
            options.style.maxHeight = isMagnified ? '160px' : '250px';
            options.style.overflowY = 'scroll';

            let newSelectedHeight = screenheight - bottomPadding - options.getBoundingClientRect().top;
            selectedPills.style.maxHeight = isMagnified ? (newSelectedHeight / 1.5) + 'px' : newSelectedHeight + 'px';
            selectedPills.style.overflowY = 'scroll';
        }

        // Selected are below page fold
        else if (isSelectedBelow && !isSuggestionsBelow) {
            let newSelectedHeight = screenheight - bottomPadding - selectedPills.getBoundingClientRect().top;
            selectedPills.style.maxHeight = isMagnified ? (newSelectedHeight / 1.5) + 'px' : newSelectedHeight + 'px';
            selectedPills.style.overflowY = 'scroll';
        }

        // Suggestions are below page fold
        else if (!isSelectedBelow && isSuggestionsBelow) {
            let newOptionsHeight = screenheight - bottomPadding - options.getBoundingClientRect().top;
            
            if (newOptionsHeight > 250) {
                options.style.maxHeight = isMagnified ? (newOptionsHeight / 1.5) + 'px' : newOptionsHeight + 'px';
                options.style.overflowY = 'scroll';
            }
            else {
                // Reset to 250
                options.style.height = isMagnified ? '160px' : '250px';
                options.style.maxHeight = isMagnified ? '160px' : '250px';

                if (selectedPills) {
                    let newSelectedHeight = screenheight - bottomPadding - options.clientHeight - selectedPills.getBoundingClientRect().top;
                    selectedPills.style.maxHeight = isMagnified ? (newSelectedHeight / 1.5) + 'px' : newSelectedHeight + 'px';
                    selectedPills.style.overflowY = 'scroll';
                }
            }
        }
    }

    // Standard Boolean search
    @api search(e = null) {
        this.showResults = false;
        this.hasError = false;
        
        const searchEvent = new CustomEvent('search', { detail: {value: this.searchValue, isSmartSearch: this._isSmartSearch}});        
        this.dispatchEvent(searchEvent);
        
        if (e) {
            e.target.blur();
        } else {
            this.template.querySelector('.search-input').blur();
        }
    }

    smartSearch() {
        this.showResults = false;
        this.hasError = false;
        
        const searchEvent = new CustomEvent('smartsearch', { detail: {value: this.searchValue, isSmartSearch: true}});        
        this.dispatchEvent(searchEvent);
        
        this.template.querySelector('.search-input').blur();
    }
}