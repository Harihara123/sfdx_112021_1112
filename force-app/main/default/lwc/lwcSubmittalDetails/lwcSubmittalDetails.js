import { LightningElement, api, track, wire} from 'lwc';
import getOrderHistoryRecords from '@salesforce/apex/LWCSubmittalController.getOrderHistoryRecords'; //importing getOrderHistoryRecords apex method from SubmittalController.cls
import updateOrderHistoryRecord from '@salesforce/apex/LWCSubmittalController.updateOrderHistoryRecord';
import ATS_StatusNCompensation from '@salesforce/label/c.ATS_StatusNCompensation';
import ATS_STATUS from '@salesforce/label/c.ATS_STATUS';
import ATS_Currency from '@salesforce/label/c.ATS_Currency';
import ATS_COMMENTS from '@salesforce/label/c.ATS_COMMENTS';
import ATS_RATE_TYPE from '@salesforce/label/c.ATS_RATE_TYPE';
import ATS_CANCEL from '@salesforce/label/c.ATS_CANCEL';
import ATS_SAVE from '@salesforce/label/c.ATS_SAVE';
import ATS_BILL_RATE from '@salesforce/label/c.ATS_BILL_RATE';
import ATS_PAY_RATE from '@salesforce/label/c.ATS_PAY_RATE';
import ATS_BURDEN from '@salesforce/label/c.ATS_BURDEN';
import ATS_Submittal_Details from '@salesforce/label/c.ATS_Submittal_Details';
import ATS_History from '@salesforce/label/c.ATS_History';
import ATS_CREATED_DATE from '@salesforce/label/c.ATS_CREATED_DATE';
import CreatedBy from '@salesforce/label/c.CreatedBy';
import ATS_NOT_PROCEEDING_REASON from '@salesforce/label/c.ATS_NOT_PROCEEDING_REASON';
import ATS_INTERVIEW_TYPE from '@salesforce/label/c.ATS_INTERVIEW_TYPE';
import ATS_Interview_Status from '@salesforce/label/c.ATS_Interview_Status';
import ATS_Interview_Date_Time from '@salesforce/label/c.ATS_Interview_Date_Time';
import ATS_YEARLY_SALARY from '@salesforce/label/c.ATS_YEARLY_SALARY';
import ATS_BONUS from '@salesforce/label/c.ATS_BONUS';
import ATS_ADDL_COMPENSATION from '@salesforce/label/c.ATS_ADDL_COMPENSATION';
import ATS_SOURCE from '@salesforce/label/c.ATS_SOURCE';
import ATS_Add_Interview from '@salesforce/label/c.ATS_Add_Interview';
import CRM_Date_Required from '@salesforce/label/c.CRM_Date_Required';
import CRM_Select_Valid_Date from '@salesforce/label/c.CRM_Select_Valid_Date';
import ATS_START_DATE from '@salesforce/label/c.ATS_START_DATE';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import DELIVERY_FIELD from '@salesforce/schema/Order.Delivery_Center__c';

//import {label} from './labels.js';
//Map for the fields. First value is field, second is a label.
const placementMap = {
    Permanent: [
        ['Salary__c', ATS_YEARLY_SALARY],
        ['Bonus__c', ATS_BONUS],
        ['Additional_Compensation__c', ATS_ADDL_COMPENSATION]
    ],
    Contract: [
        ['Rate_Frequency__c',ATS_RATE_TYPE],
        ['Bill_Rate__c', ATS_BILL_RATE],
        ['Pay_Rate__c', ATS_PAY_RATE],
        ['Burden__c', ATS_BURDEN]
    ],
    'Contract to Hire': [
        ['Rate_Frequency__c', ATS_RATE_TYPE],
        ['Salary__c', ATS_YEARLY_SALARY],
        ['Bonus__c', ATS_BONUS],
        ['Additional_Compensation__c', ATS_ADDL_COMPENSATION],
        ['Bill_Rate__c', ATS_BILL_RATE],
        ['Pay_Rate__c', ATS_PAY_RATE],
        ['Burden__c', ATS_BURDEN]
    ]
}

const COLUMNS = (tab) => {
    switch(tab) {
        case 'Application':
            return [
                {label: ATS_STATUS, field: 'Type'},
                {label: ATS_CREATED_DATE, field: 'CreatedDate'},
                {label: CreatedBy, field: 'CreatedBy'},
                {label: ATS_SOURCE, field: 'Source__c'},
                {label: ATS_COMMENTS, field: 'Description'},
                {label: ATS_NOT_PROCEEDING_REASON, field: 'Not_Proceeding_Reason__c'},
                {label: ATS_Interview_Date_Time, field: 'StartDateTime'},
                {label: ATS_INTERVIEW_TYPE, field: 'Interview_Type__c'},
                {label: ATS_Interview_Status, field: 'Interview_Status__c'}
            ]
        default:
            return [
                {label: ATS_STATUS, field: 'Type'},
                {label: ATS_CREATED_DATE, field: 'CreatedDate'},
                {label: CreatedBy, field: 'CreatedBy'},
                {label: ATS_COMMENTS, field: 'Description'},
                {label: ATS_NOT_PROCEEDING_REASON, field: 'Not_Proceeding_Reason__c'},
                {label: ATS_Interview_Date_Time, field: 'StartDateTime'},
                {label: ATS_INTERVIEW_TYPE, field: 'Interview_Type__c'},
                {label: ATS_Interview_Status, field: 'Interview_Status__c'}
            ]
    }
}
export default class lwcSubmittalDetails extends LightningElement {
label = {
          ATS_StatusNCompensation,
		  ATS_STATUS,
		  ATS_Currency,
		  ATS_COMMENTS,
		  ATS_RATE_TYPE,
		  ATS_CANCEL,
		  ATS_RATE_TYPE,
		  ATS_SAVE,
		  ATS_BILL_RATE,
		  ATS_PAY_RATE,
          ATS_BURDEN,
          ATS_History,
          ATS_Submittal_Details,
		  ATS_YEARLY_SALARY,
		  ATS_BONUS,
		  ATS_ADDL_COMPENSATION,
		  ATS_SOURCE,
		  ATS_Add_Interview,
		  CRM_Date_Required,
		  CRM_Select_Valid_Date,
          ATS_START_DATE
    };
@api tab;
@api status = {
        value: '',
        options: []
    }

@api comments = '';
@api startdate;
@api rowsPerPage = 5;
@api runningUser;
@track cols = [];
orderFields = {};
@track actions = [{action: 'edit', icon: 'utility:edit'}];
@api submittalDetails;
@track title = '';
@track tableData=[];
@track compFields;
@track showSpinner = true;
@track disableComp = false;
@track disableStartDate = false;
events;
lookupDisplay = [{ label: 'Title', fieldApiName: 'Title', dataType: 'text' }];
@track currentOffice;
@track deliveryCenterValue;
@track hideDeliveryCenter = false;
@track DeliveryPicklistValues = []

@api
get submittal() {
    return this.submittalDetails
}
@wire(getPicklistValues, { recordTypeId: '01224000000kOSE', fieldApiName: DELIVERY_FIELD })
loadDeliveryCenterPicklist({error, data}) {
    if (data) {
        console.log({error, data}, 'LDS DATA')
        this.DeliveryPicklistValues = [...data.values]
    }
}


set submittal(submittal) {
    setTimeout(() => {
        this.cols = COLUMNS(this.tab);
    },0)
        
    if (submittal) {        
        this.submittalDetails = submittal;
        this.showSpinner = true;
        this.setCompensationState();
        console.log(JSON.parse(JSON.stringify(submittal)), 'this.submittalDetails');
        this.title = submittal.Opportunity ? submittal.Opportunity.Req_Job_Title__c : '';
        //this.compFields = placementMap[submittal.Opportunity.Req_Product__c];
        this.compFields = this.formatCompensationFields(submittal.Opportunity.Req_Product__c);
        this.getOrderHistory(submittal.Id);
		this.deliveryCenterValue = this.submittalDetails.Delivery_Center__c;
		setTimeout(() => {
            this.currentOffice = (this.submittalDetails.DeliveryOffice__r || {}).Name
        }, 100)
    }
}
@api
refreshHistory() {
    this.showSpinner = true;
    this.getOrderHistory(this.submittalDetails.Id);
}
get showAddInterview() {
    return this.submittal.Status === 'Interviewing';
}
getOrderHistory(id) {
    getOrderHistoryRecords({recordId: id}).then(response => {					
        if(response){
            //console.log(JSON.parse(JSON.stringify(response)));
            this.events = response;
            this.parseResponse(response);
            this.showSpinner = false;
        }else{
            //console.log('No Order History Record to display!!!!');
        }   
    }).catch(err => {console.log(err)});
}
setCompensationState() {
    const {Status} = this.submittalDetails
    const {StageName} = this.submittalDetails.Opportunity
	const statusCheck = Status.includes('Not Proceeding') || Status.includes('Started')
    const StageNameCheck = StageName.includes('Closed Wash') || StageName.includes('Closed Lost')
	
    if (statusCheck || StageNameCheck) {
        this.disableComp = true;
    }
	if (Status !== 'Started') {
		this.disableStartDate = true;
	}
	if(Status !== 'Linked' && Status !== 'Applicant'){
		this.hideDeliveryCenter = true;
	}
}

parseResponse(response){
        console.log('Inside Parse Response');
        const opcoName = (this.runningUser.OPCO__c?this.runningUser.OPCO__c: '');
		let formattedData = [];
				// for (let i = 0; i < response.length; i++) {	
                response.forEach(row => {
                    let tempRow = {id: row.Id, cells: []}

                    this.cols.forEach(col => {
                        let value = row[col.field] || '';

                        const constructCell = {
                            CreatedDate: () => {tempRow.cells.push({value, date: true})},
                            CreatedBy: () => {
                                tempRow.cells.push({
                                    value: row.CreatedBy.Name || '', 
                                    navItem: {
                                        object: 'User', id: row.CreatedById
                                    }
                            })},
                            Description: () => {
                                tempRow.cells.push({
                                    value: (row.Type === 'Interviewing'?'':(row[col.field]?row[col.field]:'')),
                                    edit: (row.Type === 'Interviewing'? false : {type:'inline'})
                                })
                            },
                            StartDateTime: () => {
                                tempRow.cells.push({
                                    value: (row.Type === 'Interviewing'?row[col.field]:''), 
                                    date: true, 
                                    time: true,
									title: (row.Type === 'Interviewing'? this.formatDate(new Date(row[col.field]), opcoName):'')
                                })
                            },
                        }
                        if (constructCell[col.field]) {
                            constructCell[col.field]();
                        } else {
                            tempRow.cells.push({value});
                        }


                        // switch(col.field) {
                        //     case 'CreatedDate':
                        //         tempRow.cells.push({value, date: true})
                        //     case 'CreatedBy':
                        //         tempRow.cells.push({
                        //             value: row.CreatedBy.Name || '', 
                        //             navItem: {
                        //                 object: 'User', id: row.CreatedById
                        //             }
                        //         })
                        //     case 'Description':
                        //         tempRow.cells.push({
                        //             value: (row.Type === 'Interviewing'?'':(row[col.field]?row[col.field]:'')),
                        //             edit: (row.Type === 'Interviewing'? false : {type:'inline'})
                        //         })
                        //     case 'StartDateTime':
                        //         tempRow.cells.push({
                        //             value: (row.Type === 'Interviewing'?row[col.field]:''), 
                        //             date: true, 
                        //             time: true
                        //         })
                        //     default:
                        //         tempRow.cells.push({value});
                        // }
                        if (row.Type !== 'Interviewing') {
                            tempRow.hideAction = 'edit';
                        }                        
                    })
                    formattedData.push(tempRow); 
                })
                                      

				//   let tempVar = {id:response[i].Id,
				// 			    cells:[
				// 				{value:response[i].Type},
				// 				{value:response[i].CreatedDate, date: true },
				// 				{value: response[i].CreatedBy.Name, navItem: {object: 'User', id: response[i].CreatedById}},
                //                 {
                //                     value: (response[i].Type === 'Interviewing'? '' : (response[i].Description?response[i].Description:'')), 
                //                     edit: (response[i].Type === 'Interviewing'? false : {type:'inline'}) 
                //                 },
                //                 {value:response[i].Not_Proceeding_Reason__c},
                //                 {value:(response[i].Type === 'Interviewing'?response[i].StartDateTime:''), date: true, time: true},
				// 				{value:response[i].Interview_Type__c},
				// 				{value:response[i].Interview_Status__c}
																
                //                ]}
                //                if (response[i].Type !== 'Interviewing') {
                //                    tempVar.hideAction = 'edit';
                //                }
                //                 formattedData.push(tempVar);
                                

				// }
				
                console.log(formattedData);
                this.tableData = formattedData;
				//console.log(this.tableData);
}
formatCompensationFields(placement) {
    const compFieldSet = placementMap[placement];

    if (compFieldSet) {
        let compFields = [];
        compFieldSet.forEach(field => {
            let fieldObj = {
                value: this.submittalDetails[field[0]],
                label: field[1],
                field: field[0]
            }

            if (field[0] === 'Rate_Frequency__c') {
                fieldObj.options = [
                    {label: '--None--', value: '--None--'},
                    {label: 'Hourly', value: 'Hourly'},
                    {label: 'Daily', value: 'Daily'},
                    {label: 'Weekly', value: 'Weekly'},
                    {label: 'Monthly', value: 'Monthly'},
                ];
            }

            compFields.push(fieldObj)
        })

        return compFields;
    }
    return null;

}
handleCancel() {
    this.dispatchEvent(new CustomEvent('cancel'));
}
handleSave() {

	if (Object.keys(this.orderFields).length !== 0) {
        //console.log('changes were made to field(s); save.',this.orderFields)
        this.orderFields.Id = this.submittalDetails.Id;
		this.dispatchEvent(new CustomEvent('edit', {detail: {submittalFields:this.orderFields,column:'DetailFields'}}));
    } else {
        console.log('no changes were made to field(s); do NOT save.',this.orderFields)
    }
	this.dispatchEvent(new CustomEvent('cancel'));
}
handleAddInterview() {
    this.dispatchEvent(new CustomEvent('interviewupdate', {detail: {rowId: this.submittalDetails.Id, value: 'Add New Interview', prevValue: this.submittalDetails.Status,column:'Status', sub: this.submittalDetails}}));
}
handleFieldEdit(e) {
	this.template.querySelector('[data-id="saveBtn"]').disabled = false;
    const field = e.target.getAttribute('data-field');
	if(field == "Start_Date__c"){
	let  dateCmp =  this.template.querySelector(".startDateCls"); 
		if(!dateCmp.value){
			dateCmp.setCustomValidity(this.label.CRM_Date_Required);
			this.template.querySelector('[data-id="saveBtn"]').disabled = true;
		}else{
			let todayDate = new Date();
			let startDate = new Date(dateCmp.value);
			if(startDate <= todayDate){
				this.template.querySelector('[data-id="saveBtn"]').disabled = false;
				dateCmp.setCustomValidity("");
			}else {
				dateCmp.setCustomValidity(this.label.CRM_Select_Valid_Date);
				this.template.querySelector('[data-id="saveBtn"]').disabled = true;
			}
		}
	}
    if (field === 'DeliveryOffice__c') {
        if (e.detail.lookupid) {
            //console.log(JSON.parse(JSON.stringify(e.detail)))
            const {lookupid, lookupValue} = e.detail;
            this.orderFields[field] = lookupid.substring(1,lookupid.length-1);
            this.orderFields.DeliveryOffice__r = {Name: lookupValue, Id: lookupid.substring(1,lookupid.length-1)}
        }
        //console.log(JSON.parse(JSON.stringify(e.detail)))
    } else {		
        const {value} = e.target;        
		this.orderFields[field] = value;
    }
       
    
	//console.log(JSON.parse(JSON.stringify(this.orderFields)))
}
handleEventEdit(e) {
    this.showSpinner = true;
    //console.log(JSON.parse(JSON.stringify(e.detail)));
    const {rowId, value} = e.detail;

    const event = {
        Id: rowId,
        Description: value
    }
    //console.log(event);
    updateOrderHistoryRecord({ev:event})
        .then(response => {
            if (response) {
                //SUCCESS toast
                this.showSpinner = false;                
            } else {
                //ERROR revert value and toast
                this.showSpinner = false;                
            }
        }).catch(err => {
            this.showSpinner = false;
            //console.log(err)
        })		

}
handleStatus(e) {
    //console.log(e.target.value)
    if(['Add New Interview', 'Edit Interview', 'Edit Not Proceeding'].includes(e.target.value)) {
        const value = e.target.value;
        let newStatus = JSON.parse(JSON.stringify(this.status))
        newStatus.value = '';
        newStatus.options = [];
        this.status = newStatus;
        // console.log(JSON.stringify(this.status), 'in details; handleStatus')

        setTimeout(() => {
            this.dispatchEvent(new CustomEvent('edit', {detail: {rowId: this.submittalDetails.Id, value, prevValue: this.submittalDetails.Status,column:'Status'}}));

        }, 0)

    } else {

        this.dispatchEvent(new CustomEvent('edit', {detail: {rowId: this.submittalDetails.Id, value: e.target.value, prevValue: this.submittalDetails.Status,column:'Status'}}));
    }

}
get getOfficeState() {
    return ((this.status.value === 'Offer Accepted' || this.status.value === 'Started') && 
    this.submittalDetails.Opportunity.OpCo__c.includes('Aerotek'));
}
handleHistoryInterviewEdit(e) {
    //console.log(JSON.parse(JSON.stringify(e.detail)));
    this.dispatchEvent(new CustomEvent('interviewupdate', {detail: {rowId: this.submittalDetails.Id, value: 'Edit Interview', prevValue: this.submittalDetails.Status,column:'Status', sub: this.submittalDetails, eventId: e.detail.rowId}}));
}
get checkApplicationTab() {
    return this.tab === 'Application'
}

formatDate(date, userOpcoFlag) {
	 var hours = date.getHours();
	 var minutes = date.getMinutes();
	 var strTime ='';
	 var dateString = '';
	 var dMonth = date.getMonth()+1 ;
	 if(userOpcoFlag == 'AG_EMEA' || userOpcoFlag == 'GB1'){
	 strTime = hours + ':' + minutes ;
	 dateString = date.getDate() + "/" + dMonth + "/" + date.getFullYear() + ", " + strTime;
	 }else{
	 var ampm = hours >= 12 ? 'PM' : 'AM';
	 hours = hours % 12;
	 hours = hours ? hours : 12; // the hour '0' should be '12'
	 minutes = minutes < 10 ? '0'+minutes : minutes;
	 strTime = hours + ':' + minutes + ' ' + ampm;
	 dateString = dMonth + "/" + date.getDate() + "/" + date.getFullYear() + ", " + strTime;
	 }
	 return dateString;
}


}