import AG_Portal_URL from '@salesforce/label/c.AG_Portal_URL';
import Instance_URL_For_RWS from '@salesforce/label/c.Instance_URL_For_RWS';
import ATS_SUBMITTALS from '@salesforce/label/c.ATS_SUBMITTALS'
import ATS_Applications from '@salesforce/label/c.ATS_Applications'
import ATS_MASS_STATUS_UPDATE from '@salesforce/label/c.ATS_MASS_STATUS_UPDATE';
import ATS_NO_SUBMITTALS_FOUND from '@salesforce/label/c.ATS_NO_SUBMITTALS_FOUND';
import ATS_NO_APPLICATIONS_FOUND from '@salesforce/label/c.ATS_NO_APPLICATIONS_FOUND';
import ATS_NAME from '@salesforce/label/c.ATS_NAME';
import ATS_ACCOUNT_NAME from '@salesforce/label/c.ATS_ACCOUNT_NAME';
import ATS_LASTMOD_BY from '@salesforce/label/c.ATS_LASTMOD_BY';
import ATS_LAST_MODIFIED_DATE from '@salesforce/label/c.ATS_LAST_MODIFIED_DATE';
import ATS_STATUS from '@salesforce/label/c.ATS_STATUS';
import ESF from '@salesforce/label/c.ESF';
import SIF_Label from '@salesforce/label/c.SIF_Label';
import ATS_NOT_PROCEEDING_REASON from '@salesforce/label/c.ATS_NOT_PROCEEDING_REASON';
import ATS_COMMENT from '@salesforce/label/c.ATS_COMMENT';
import TC_Title from '@salesforce/label/c.TC_Title';
import ATS_FULL_NAME from '@salesforce/label/c.ATS_FULL_NAME';
import ATS_NEW_SIF from '@salesforce/label/c.ATS_NEW_SIF';
import View_Edit_ESF from '@salesforce/label/c.View_Edit_ESF';
import New_ESF from '@salesforce/label/c.New_ESF';
import ATS_ICON_TITLE_SUCCESS from '@salesforce/label/c.ATS_ICON_TITLE_SUCCESS';
import Ats_MassUpdate_validation_Error from '@salesforce/label/c.Ats_MassUpdate_validation_Error';
import ATS_LOCATION from '@salesforce/label/c.ATS_LOCATION';
import ATS_APPLICATION_DATE from '@salesforce/label/c.ATS_APPLICATION_DATE';
import ATS_SOURCE from '@salesforce/label/c.ATS_SOURCE';
import Valid_Opcos_For_Platform_Event from '@salesforce/label/c.Valid_Opcos_For_Platform_Event';
const labels = {
    AG_Portal_URL,
    Instance_URL_For_RWS,
    ATS_SUBMITTALS,
	ATS_Applications,
    ATS_MASS_STATUS_UPDATE,
    ATS_NO_SUBMITTALS_FOUND,
	ATS_NO_APPLICATIONS_FOUND,
    ATS_NAME,
    ATS_ACCOUNT_NAME,
    ATS_LASTMOD_BY,
    ATS_LAST_MODIFIED_DATE,
    ATS_STATUS,
    ESF,
    SIF_Label,
    ATS_NOT_PROCEEDING_REASON,
    ATS_COMMENT,
    TC_Title,
    ATS_FULL_NAME,
    ATS_NEW_SIF,
    View_Edit_ESF,
    New_ESF,
    ATS_ICON_TITLE_SUCCESS,
	Ats_MassUpdate_validation_Error,
	ATS_LOCATION,
	ATS_APPLICATION_DATE,
	ATS_SOURCE,
    Valid_Opcos_For_Platform_Event
}
export {labels}