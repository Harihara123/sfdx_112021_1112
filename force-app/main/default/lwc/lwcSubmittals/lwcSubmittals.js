import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getOrderRecords from '@salesforce/apex/LWCSubmittalController.getOrderRecords'; //importing getOrderRecords apex method from SubmittalController.cls
import updateSubmittalRecord from '@salesforce/apex/LWCSubmittalController.updateSubmittalRecord'; //importing updateSubmittalRecord apex method from SubmittalController.cls

import {labels} from './labels.js';

export default class lwcSubmittals extends LightningElement {

	//Public Variables
   @api runningUser;
   @api currentTab;
   @api userOwnership;
   @api recordId;
   @api pageRef;
   @api rowsPerPage = 5;

   //Private Reactive Variables
   @track submittals = [];
   @track submittalDetailsView = 'hide';
   @track submittalsView = 'show';
   @track proactiveSubmittal = {};
   @track showingProactiveDetails;
   @track submittalDetailStatus = {
	   		value: '',
	   		options: []
   };
   @track submittalDetailStartDate;   
   @track submittalDetail;
   @track submittalDetailComments;   
   @track tableData=[];
   @track paginatedTableData;
   @track exportData=[];
   @track cols = [];
	@track actions = [
        {action: 'view', icon: 'utility:summarydetail'}
    ];
   @track showSpinner;
   @track showEmptyState = false;
   @track isApplicationTab = false;
   @track historyRowsPerPage = 5;

   //Private Variables
   spinnerToggle = this.toggleSpinner;
   statusMap;
   submittalsData;
   Labels = labels;

   connectedCallback() { // this is similar to doinit
	
	this.showSpinner = true;
		if(this.pageRef === 'Opportunity'){	//SK - Opp Column Headers
				this.historyRowsPerPage = 10;
				this.cols = [
					{label:labels.ATS_FULL_NAME, field:'ShipToContactName'},
					{label:labels.ATS_LASTMOD_BY, field:'LastModifiedBy', sort: true},
					{label:labels.ATS_LAST_MODIFIED_DATE, field:'LastModifiedDate', sort: true},
					{label:labels.ATS_STATUS, field:'Status'},
					{label:labels.ESF+'/'+labels.SIF_Label, field:'ESF', style: 'width: 5rem;'},
					{label:labels.ATS_COMMENT, field:'Comments', sort: true}
				];					
		}
		else{
				if(this.currentTab ==='Application'){
							this.cols = [
						{label:labels.TC_Title, field:'Title'}, //Label: Need clearification if able to use "TC_" labels or create new for "ATS_"
						{label:labels.ATS_NAME, field:'Name'},
						{label:labels.ATS_ACCOUNT_NAME, field:'AccountName'},
						{label:labels.ATS_LASTMOD_BY, field:'LastModifiedBy', sort: true},
						{label:labels.ATS_LAST_MODIFIED_DATE, field:'LastModifiedDate', sort: true},
						{label:labels.ATS_LOCATION, field:'Location'},
						{label:labels.ATS_STATUS, field:'Status'},
						{label:labels.ATS_APPLICATION_DATE, field:'EffectiveDate'},
						{label:labels.ATS_SOURCE, field:'Source'}																						
					];

					this.isApplicationTab = true;

				}else{
							this.cols = [
						{label:labels.TC_Title, field:'Title'}, //Label: Need clearification if able to use "TC_" labels or create new for "ATS_"
						{label:labels.ATS_NAME, field:'Name'},
						{label:labels.ATS_ACCOUNT_NAME, field:'AccountName'},
						{label:labels.ATS_LASTMOD_BY, field:'LastModifiedBy', sort: true},
						{label:labels.ATS_LAST_MODIFIED_DATE, field:'LastModifiedDate', sort: true},
						{label:labels.ATS_STATUS, field:'Status'},
						{label:labels.ESF+'/'+labels.SIF_Label, field:'ESF', style: 'width: 5rem;'},
						{label:labels.ATS_NOT_PROCEEDING_REASON, field: 'Submittal_Not_Proceeding_Reason__c'},
						{label:labels.ATS_COMMENT, field:'Comments', sort: true}
					];
				}
			
		}
        getOrderRecords({recordId: this.recordId, pageRef: this.pageRef, currentTab: this.currentTab}).then(response => {
			
			this.showSpinner = false;
			if(response){
				this.submittalsData = response;
				this.parseResponse(response);
			}else{
				
				this.showEmptyState = true;
				
			}	
        }).catch((err) => {
			this.showSpinner = false;
			this.showEmptyState = true;
			console.log(err)
		});
		
	}

   //Pubic Functions
   @api 
   updateSubmittalStatus(status, submittalId, prevStatus = null) {
		
		const rowIndex = this.tableData.findIndex(row => row.id === submittalId);
		const cellIndex = this.cols.findIndex(cell => cell.field === 'Status')

		let newTableData = JSON.parse(JSON.stringify(this.tableData));
		const reqOpco = newTableData[rowIndex].opco

		if (newTableData[rowIndex].cells[cellIndex].edit && newTableData[rowIndex].cells[cellIndex].edit.options) {
			if (prevStatus && this.statusMap[`${reqOpco}-${status} - ${prevStatus}`]) {
				newTableData[rowIndex].cells[cellIndex].edit.options = this.statusMap[`${reqOpco}-${status} - ${prevStatus}`]
			} else {
				newTableData[rowIndex].cells[cellIndex].edit.options = this.statusMap[reqOpco+'-'+status]
			}		
		}
				
		newTableData[rowIndex].cells[cellIndex].value = status;

		this.tableData = newTableData;
		
		if (this.showingDetails) {
			this.prepareStatusForDetails(submittalId)
		}		
    }
	@api
	removeSubmittalRecord(submittalId){
		this.showSpinner = true;

	
		const rowIndex = this.tableData.findIndex(row => row.id === submittalId);
		let newTableData = [...this.tableData];
		newTableData.splice(rowIndex,1);
		this.tableData = newTableData;

		this.showSpinner = false;
	}
	@api
	refreshSubmittalHistory() {
		if (this.submittalDetailsView === 'show') {
			this.template.querySelector('c-lwc-submittal-details').refreshHistory();
		}
	}
	@api
	clearSelections() {
		this.template.querySelector('c-lwc-data-table').resetColumnSelect();		
	}
	@api refreshSubmittals() {
		if(this.submittalDetailsView === 'show'){
			this.toggleDetailsView();
		}
		if (this.recordId) {
			
			getOrderRecords({recordId: this.recordId, pageRef: this.pageRef, currentTab: this.currentTab}).then(response => {
				
				this.showSpinner = false;
				if(response){
					this.submittalsData = response;
					this.parseResponse(response);
					eval("$A.get('e.force:refreshView').fire();");
				}else{
					
					this.showEmptyState = true;
					
				}	
			}).catch((err) => {
				this.showSpinner = false;
				this.showEmptyState = true;
				console.log(err)
			});
		}
	}
	@api
	toggleSpinner(state) {
		this.showSpinner = state;
	}
	@api
	showToast(variant, title, message) {
        const event = new ShowToastEvent({
			title,
			variant,
            message
        });
        this.dispatchEvent(event);
	}

	updateSubmittalComments(comments, submittalId) {
		
		const rowIndex = this.tableData.findIndex(row => row.id === submittalId);
		const cellIndex = this.cols.findIndex(cell => cell.field === 'Comments')

		let newTableData = [...this.tableData]

		newTableData[rowIndex].cells[cellIndex].value = comments;

		this.tableData = newTableData;
	}
	updateSubmittalCompFields(compFields, submittalId) {
		const submittals = JSON.parse(JSON.stringify(this.submittalsData));
		const submittalIndex = submittals.submittalList.findIndex(submittal => submittal.Id === submittalId);

		Object.keys(compFields).forEach(key => {
			submittals.submittalList[submittalIndex][key] = compFields[key];
		})
		
		this.submittalsData = submittals;
	}	
	formatTableRow(submittal) {
		
		
		let Key;
		let cellSchema;
		let cells = [];
		if ((submittal || {}).Opportunity && this.pageRef=='Opportunity') {
			
			Key = submittal.Opportunity.OpCo__c+'-'+submittal.Status;
			cellSchema = {
				opco: submittal.Opportunity.OpCo__c,
				ShipToContactName: {
					value: submittal.ShipToContact.Name,
					navItem: {object: 'Contact', id:submittal.ShipToContactId}
				},
				LastModifiedBy: {value: submittal.LastModifiedBy.Name},
				LastModifiedDate: {value: submittal.Display_LastModifiedDate__c,
					date: true
				},
				Status: {
					value: submittal.Status,
					edit: {type:'inline',options: this.statusMap[Key]}
				},
				ESF: this.esfLogic(submittal) === '#'
				?
				{
					value: 'In Progress. ESF is not yet available. Please try again soon.',
					icon: 'utility:warning'
				}
				:
				{
					value: '',
					innerHTML: this.esfLogic(submittal)
				},
				Comments: (submittal || {}).Comments__c
				?
				{
					value: submittal.Comments__c,
					edit:{type:'inline'}
				}
				:
				{
					value: '',
					edit:{type:'inline'}
				}
			}
		}
		else if((submittal || {}).Opportunity && this.pageRef!=='Opportunity') {
			Key = submittal.Opportunity.OpCo__c+'-'+submittal.Status;
			cellSchema = {
				opco: submittal.Opportunity.OpCo__c,
				Title: {
					value: submittal.Opportunity.Req_Job_Title__c,
					navItem: {object: 'Opportunity', id:submittal.OpportunityId}
				},
				Name: {value: submittal.Opportunity.Name},
				AccountName: {value: submittal.Opportunity.Account_name_Fourmula__c},
				LastModifiedBy: {value: submittal.LastModifiedBy.Name},
				LastModifiedDate: {
					value: submittal.Display_LastModifiedDate__c,
					date: true
				},
				Status: {
					value: submittal.Status,
					edit: {type:'inline',options: this.statusMap[Key]}
				},
				Location :{value: submittal.Opportunity.Account_City__c},
				EffectiveDate: {
					value: submittal.EffectiveDate,
					date: true
				},
				Source: (submittal || {}).Events
				?
				 {
					value: submittal.Events[0].JB_Source_Name__c
				 }
				 :
				{
					value: ''
					
				},
				ESF: this.esfLogic(submittal) === '#'
				?
				{
					value: 'In Progress. ESF is not yet available. Please try again soon.',
					icon: 'utility:warning'
				}
				:
				{
					value: '',
					innerHTML: this.esfLogic(submittal)
				},
				Submittal_Not_Proceeding_Reason__c: {value: submittal.Submittal_Not_Proceeding_Reason__c},
				Comments: (submittal || {}).Comments__c
				?
				{
					value: submittal.Comments__c,
					edit:{type:'inline'}
				}
				:
				{
					value: '',
					edit:{type:'inline'}
				}
			}
		} else {
			Key = 'Proactive-'+submittal.Status;
			cellSchema = {
				opco: 'Proactive',
				Title: {
					value: submittal.Job_Title__c,
					innerHTML: this.getPIconHTML(submittal.Job_Title__c)
				},
				Name: {value: 'Proactive'},
				AccountName: {value: submittal.Account.Name},
				LastModifiedBy: {value: submittal.LastModifiedBy.Name},
				LastModifiedDate: {
					value: submittal.Display_LastModifiedDate__c,
					date: true
				},
				Status: {
					value: submittal.Status,
					edit: {type:'inline',options: this.statusMap[Key]}
				},
				ESF: this.esfLogic(submittal) === '#'
				?
				{
					value: 'In Progress. ESF is not yet available. Please try again soon.',
					icon: 'utility:warning'
				}
				:
				{
					value: '',
					innerHTML: this.esfLogic(submittal)
				},
				Submittal_Not_Proceeding_Reason__c: {value: submittal.Submittal_Not_Proceeding_Reason__c},
				Comments: (submittal || {}).Comments__c
				?
				{
					value: submittal.Comments__c,
					edit:{type:'inline'}
				}
				:
				{
					value: '',
					edit:{type:'inline'}
				}
			}
		}
		if (Key && cellSchema) {
			this.cols.forEach(col => {
				cells.push(cellSchema[col.field])
			})			
			return {id: submittal.Id, opco: cellSchema.opco, cells};
		}
		return null;
	}
	parseResponse(response){
		this.statusMap = response.statusMap				
				let formattedData = [];				

				for (let i = 0; i < response.submittalList.length; i++) {
					formattedData.push(this.formatTableRow(response.submittalList[i]));
				}

				this.tableData = formattedData;
				this.prepareExportData();
				
	}		
	esfLogic(submittal) {
		if ((submittal || {}).Opportunity && (submittal || {}).Opportunity.Req_OpCo_Code__c) {
			const srcSysId = submittal.Source_system_id__c;
			const esfId = submittal.Start_Form_ID__c;
			const opcoCode = submittal.Opportunity.Req_OpCo_Code__c;			
			const status = submittal.Status;
			const talentAccountId = submittal.ShipToContact.AccountId;
			let opco = labels.Valid_Opcos_For_Platform_Event;
			let rwsURL = labels.Instance_URL_For_RWS;
			rwsURL = rwsURL.substring(0,rwsURL.indexOf('RWS/')+4);
			const partnerReqId = submittal.Opportunity.Req_origination_System_Id__c;
			const partnerReqSystemID = submittal.Opportunity.Req_Origination_Partner_System_Id__c;
			let contactSrcSysId = submittal.ShipToContact.Source_System_Id__c;

			if(opco.includes(opcoCode)){
				if (srcSysId) {
					if (esfId) {
							return this.getInnerHTML(rwsURL + 'esf/viewESF.action?esfId=' + esfId, labels.View_Edit_ESF)
					}
					if (['Offer Accepted', 'Started'].includes(status)) {
						if (contactSrcSysId) {
							const contSysId = contactSrcSysId.replace('R.', '');
								return this.getInnerHTML(
									rwsURL + 
									'esf/createESF.action?partnerREQID=' + partnerReqId + 
									'&talentID=' + talentAccountId + 
									'&partnerReqSystemID=' + partnerReqSystemID + 
									'&candidateID=' + contSysId
									, 'New ESF')
						}
						return this.getInnerHTML(
							rwsURL+
							"esf/createESF.action?partnerREQID="+partnerReqId+
							"&talentID="+talentAccountId+
							"&partnerReqSystemID="+partnerReqSystemID
							, labels.New_ESF);
					}					
				} else {
					if (['Offer Accepted', 'Started'].includes(status)) {
						//TODO: triangle indicator In Progress
						return '#'
					}
				}
			}

			if(!opco.includes(opcoCode)){
				if(srcSysId && srcSysId.indexOf("S.") >= 0) {
					return this.getInnerHTML(labels.AG_Portal_URL+"?SubmittalId="+submittal.Id, labels.ATS_ICON_TITLE_SUCCESS)
				}
			}

			if (opcoCode.includes('EMEA')) {
				if (!srcSysId && (status === 'Offer Accepted' || status === 'Started')) {
					return this.getInnerHTML(labels.AG_Portal_URL+"?SubmittalId="+submittal.Id, labels.ATS_NEW_SIF)
				}
			}			
			return ''
		}
		return ''
	}
	getPIconHTML(title) {
		return `<div title="${title}" class="slds-truncate"><span style='background: #045797;
			border-radius: .6em;
			color: #ffffff;
			display: inline-block;
			font-weight: 600;
			line-height: 1.2em;
			margin-right: 5px;
			text-align: center;
			width: 1.2em;
			font-size: 12px;
			text-align: center;
			font-weight: 400;
			padding-top: 1px;'>P</span>${title}</div>`
	}
	getInnerHTML(url, text) {
		return `<a href="${url}" target="_blank">${text}</a>`
	}
	getSelected() {		
        let selected = this.template.querySelector('c-lwc-data-table').getSelected();
		

		let submittalIds = [];

		for (let i = 0; i < selected.length; i++) { 
		submittalIds.push(selected[i].id);
		}
		if (submittalIds.length < 2) {
			this.showToast('error', 'Error',labels.Ats_MassUpdate_validation_Error);
		} else {
			this.dispatchEvent(new CustomEvent('massupdate', {detail: {subIds: submittalIds}}));
		}		
    }

   
    handleEdit(e) {
			
		
		//Checking if edit is for the status
		if(e.detail.column === 'Status') {		
			//getting the full submittal data for the C_SubmittalHelper method
			//looking at the submittals array and getting the object which has matching Id's.	
			const sub = this.submittalsData.submittalList.find((submittal) => submittal.Id === e.detail.rowId);
			const reqOpco = (sub.Opportunity? sub.Opportunity.OpCo__c : "");

			//Updating the table data to match with the new status change. Assuming happy path.
			//Errors or Cancel actions are handled by setting the value back to previous.			
			if (!['Add New Interview', 'Edit Interview', 'Edit Not Proceeding'].includes(e.detail.value)) {
				if (status.includes('Not Proceeding')) {
					this.updateSubmittalStatus(e.detail.value, e.detail.rowId, e.detail.prevValue);
				} else if (e.detail.value.includes('Started') && !reqOpco.includes('EMEA')) {
					this.updateSubmittalStatus(e.detail.value, e.detail.rowId, e.detail.prevValue);
				} else if (e.detail.value.includes('Offer Accepted')) {
					
					if (!(sub.ShipToContact.MailingCountry != null && (sub.ShipToContact.MailingState != null || sub.ShipToContact.MailingPostalCode != null))) {
						this.updateSubmittalStatus(e.detail.prevValue, e.detail.rowId);
						this.showToast('error', 'Error', 'The talent record is missing location information. Please populate the location information before proceeding.');
						return
					}
					this.updateSubmittalStatus(e.detail.value, e.detail.rowId);
				}else {					
					this.updateSubmittalStatus(e.detail.value, e.detail.rowId);
				}
			} else {
				let tempValue = 'Interviewing';
				if (e.detail.value === 'Edit Not Proceeding') {
					tempValue = 'Not Proceeding'
				}
				this.updateSubmittalStatus(tempValue, e.detail.rowId);
			}
			
			//(JSON.stringify -> JSON.parse) is one way of creating a new copy of the object.
			//Javascript creates variables by references if they are not a primitive type.
			//need to create a new copy to modify nested values. Otherwise will throw Proxy trap component error.
			let editedSub = JSON.parse(JSON.stringify(sub));

			//adding Account key to match data structure for 'Interviewing' modal.
			if (editedSub.Opportunity) {
				editedSub.Account = {Name: editedSub.Opportunity.Account_name_Fourmula__c};
			}

			//Custom event with needed data for Aura to handle status change.
			this.dispatchEvent(new CustomEvent('statuschange', {detail: {submittal: editedSub, status: e.detail.value, prevStatus: e.detail.prevValue, fx: this.showSpinner }}));
		} else {
			this.showSpinner = true;
			let submittalObject={};
			if(e.detail.column === 'Comments'){
				this.updateSubmittalComments(e.detail.value, e.detail.rowId);
				submittalObject={Id:e.detail.rowId, Comments__c:e.detail.value};
			}else{ // this will be executed for detail section fields from lwcSubmittalDetails
				const {submittalFields} = e.detail;
				this.updateSubmittalCompFields(submittalFields, submittalFields.Id)
				if (submittalFields.Comments__c || submittalFields.Comments__c == "") {
					this.updateSubmittalComments(submittalFields.Comments__c, submittalFields.Id);
				}
				
				submittalObject= e.detail.submittalFields
			}
			updateSubmittalRecord({orderObj:submittalObject, recordId:this.recordId, pageRef:this.pageRef, recordCount: this.tableData.length, currentTab: this.currentTab }).then(response => {
				this.showSpinner = false;
				if(response.recordUpdated){
					this.parseResponse(response);
					this.showToast('success', 'Success', 'Submittal Updated Successfully.')
					
				}else if(!response.exceptionFlag){
				this.showToast('success', 'Success', 'Submittal Updated Successfully.')
				
			}else{
				
				
				this.refreshSubmittals()
				this.showToast('error', 'Error', 'Submittal Update Failed.')
			}
		}).catch(err => {
			console.log(err)
			this.showToast('error', 'Error', 'Submittal Update Failed.')
			this.updateSubmittalComments(e.detail.prevValue, e.detail.rowId);
		});
		}


	}
	handleAction(e) {
		
		const subOpco = e.detail.rowDetails.opco;

		if (subOpco === 'Proactive') {

			
			this.proactiveSubmittal.conId = e.detail.rowId;
			this.proactiveSubmittal.talentId = this.recordId;
			this.proactiveSubmittal.runningUser = this.runningUser;

			this.showingProactiveDetails = true;
			this.toggleDetailsView()

			//TODO: handle proactive submittal here.
		} else {
			const submittals = JSON.parse(JSON.stringify(this.submittalsData));
			const submittalIndex = submittals.submittalList.findIndex(submittal => submittal.Id === e.detail.rowId);
	
			this.prepareStatusForDetails(e.detail.rowId)
	
			//comments prep
			if (this.currentTab === 'Submittal') {
				const commentsIndex = this.cols.findIndex(col => col.field === 'Comments');
				this.submittalDetailComments = e.detail.rowDetails.cells[commentsIndex].value;
				this.submittalDetailStartDate = this.submittalsData.submittalList[submittalIndex].Start_Date__c
			}
	
			this.showSubmittalDetails(this.submittalsData.submittalList[submittalIndex]);
		}
	}
	prepareExportData(){
		let tmpHeader = [];
	
		for (let i = 0; i < this.cols.length; i++) { 
			tmpHeader.push(this.cols[i].label);
		}
		this.exportData.push(tmpHeader);

		for (let i = 0; i < this.tableData.length; i++) {  
			let tmpdata = [];
			for (let j = 0; j < this.tableData[i].cells.length; j++) {  
			tmpdata.push(this.tableData[i].cells[j].value);
			}
			this.exportData.push(tmpdata);	
		}
	}

	toggleDetailsView() {
		const cssMap = {
			show: 'hide',
			hide: 'show'
		}
		if (cssMap[this.submittalDetailsView] !== 'show' && this.showingProactiveDetails) {
			this.showingProactiveDetails = false;
		}
		this.submittalDetailsView = cssMap[this.submittalDetailsView]
		this.submittalsView = cssMap[this.submittalsView]
				
	}
	showSubmittalDetails(data) {
		this.toggleDetailsView()
		this.submittalDetail = data
	}
	hideSubmittalDetails() {
		this.toggleDetailsView()
		this.submittalDetail = null;
	}
	get showingDetails() {
		return this.submittalDetailsView === 'show' && !this.showingProactiveDetails
	}
	prepareStatusForDetails(submittalId) {
		const colIndex = this.cols.findIndex(col => col.field === 'Status');
		const submittalIndex = this.tableData.findIndex(submittal => submittal.id === submittalId);

		const submittal = JSON.parse(JSON.stringify(this.tableData[submittalIndex]));
		const status = submittal.cells[colIndex].value;
		if(!submittal.cells[colIndex].edit.options){return;}
		let options = []
		submittal.cells[colIndex].edit.options.forEach(option => {
			options.push({label: option, value: option})
		})
		if (options.findIndex(option => option.value === status) === -1) {
			options.unshift({
				label: status,
				value: status
			})
		}

		this.submittalDetailStatus = {
			value: status,
			options
		}


	}
    handleDetailInterviewUpdates(e) {
		
		let editedSub = JSON.parse(JSON.stringify(e.detail.sub));
		editedSub.Account = {Name: editedSub.Opportunity.Account_name_Fourmula__c};

		this.dispatchEvent(new CustomEvent('statuschange', {detail: {submittal: editedSub, status: e.detail.value, prevStatus: e.detail.prevValue, preselect: e.detail.eventId}}));
	}
	handleProactiveSubmittalChange() {
		
		this.proactiveSubmittal = {};

		this.showingProactiveDetails = false;
		this.submittalsView = 'show';
		this.submittalDetailsView = 'hide';

		this.refreshSubmittals();
	}
}

		