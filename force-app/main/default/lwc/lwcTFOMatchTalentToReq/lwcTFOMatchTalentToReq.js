import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from "lightning/navigation";
import matchReq from '@salesforce/apex/TalentFirstOpportunity.matchReq';
import { CloseActionScreenEvent } from 'lightning/actions';
import CONNECTED_Summer18URL from '@salesforce/label/c.CONNECTED_Summer18URL'

export default class LwcTFOMatchTalentToReq extends NavigationMixin(LightningElement) {
    @api recordId;

    @wire(matchReq, {recordId: '$recordId'})
    matchTalentToReq({data, error}) {
        if(data) {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__webPage',
                attributes: {
                    url: CONNECTED_Summer18URL
                    + "/n/Search_Reqs?c__matchTalentId=" + data.AccountId
                    + "&c__contactId=" + data.Id
                    + "&c__srcName=" + data.Name
                    +"&c__noPush=true"
                }
            }).then(url => {
                window.open(url);
                this.dispatchEvent(new CloseActionScreenEvent());
            });
        } else if(error) {
            this.dispatchEvent(new CloseActionScreenEvent());
        }
    }
}