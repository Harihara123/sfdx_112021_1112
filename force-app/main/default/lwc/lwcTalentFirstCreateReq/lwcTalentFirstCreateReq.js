import { LightningElement, wire, api } from 'lwc';
import userId from '@salesforce/user/Id';
import { getRecord } from 'lightning/uiRecordApi';
import {getObjectInfo} from 'lightning/uiObjectInfoApi';
import Opportunity from '@salesforce/schema/Opportunity';
import OPCO_FIELD from '@salesforce/schema/User.OPCO__c';
import CompanyName from '@salesforce/schema/User.CompanyName';
import { CloseActionScreenEvent } from 'lightning/actions';
import getContactRoles from '@salesforce/apex/TalentFirstOpportunity.getContactRoles';
import { NavigationMixin } from "lightning/navigation";

const fields = [OPCO_FIELD, CompanyName];

export default class LwcTalentFirstCreateReq extends NavigationMixin(LightningElement) {
    isModalOpen = false;
    selectedValue = '';
    options = [{value: '', label: 'Select a contact or Other'}];
    contacts = [];
    @api _recordId; 
    recordTypeId;
    loggedInUserId = userId;  
    redirectComponent;    
    acc;
    con;
    opco;
    companyName;
    error;
    showErrorMessage = false;
    address;

    @api
    get recordId() {
        return this._recordId;
    }
   
    set recordId(value) {
        this._recordId = value;        
        getContactRoles({'recordId': this._recordId})
        .then(data => {
            this.contacts = data;
            this.options = [{value: '', label: 'Select a contact or Other'}];
            for(let d in data) {
                this.options = [...this.options, {label: data[d].Contact.Name, value: data[d].ContactId}];
            }
            this.options = [ ...this.options, {label: 'Other', value: 'Other'}];                      
            this.error = undefined;
        })
        .catch(error => {
            this.error = error;
            this.options = undefined;    
        });
    }      

    @wire(getRecord, { recordId: '$loggedInUserId', fields })
    userRecord({error, data}) {
        if(data) {
            this.opco = data.fields.OPCO__c.value;   
            this.companyName = data.fields.CompanyName.value;         
        }
    }    

    @wire(getObjectInfo, {objectApiName: Opportunity})
    getObjectData({data, error}) {
        if(data) {            
            for(let recType in data.recordTypeInfos) {                
                if(data.recordTypeInfos[recType].name == 'Req') {
                    this.recordTypeId = data.recordTypeInfos[recType].recordTypeId;
                }
            }
        }
    }

    closeModal() {        
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    handleChange(event) {
        this.selectedValue = event.target.value;            
        if(this.selectedValue != '' && this.selectedValue != 'Other') {            
            for(let d in this.contacts) {
                if(this.contacts[d].ContactId == this.selectedValue) {
                    this.acc = {'Id' : this.contacts[d].Contact.AccountId,
                                    'Name' : this.contacts[d].Contact.Account.Name};
                    this.con = {'Id' : this.contacts[d].Contact.Id,
                                    'Name' : this.contacts[d].Contact.Name};
                    this.address = this.contacts[d].Contact.Account.Account_Street__c + ';' +
                                    this.contacts[d].Contact.Account.Account_City__c + ';' +
                                    this.contacts[d].Contact.Account.Account_State__c + ';' +
                                    this.contacts[d].Contact.Account.Account_Country__c + ';' +
                                    this.contacts[d].Contact.Account.Account_Zip__c;
                }
            } 
            this.showErrorMessage = false;           
        } else if(this.selectedValue == 'Other') {
            this.acc = null;
            this.con = null;
            this.address = ';;;;';
            this.showErrorMessage = false;
        }                
    }

    submitDetails(event) {  
        if(this.selectedValue == '') {
            this.showErrorMessage = true;            
        } else {            
            if(this.opco == 'TEK') {
                this.opco = 'TEKSystems, Inc.';
                this.redirectComponent = 'c__CreateEnterpriseTEKReq';                               
            } else if(this.opco == 'ONS') {
                this.opco = 'Aerotek, Inc.';
                this.redirectComponent = 'c__CreateEnterpriseAerotekReq_New'; 
            } else if(this.companyName == 'AG_EMEA') {
                this.opco = 'AG_EMEA';
                this.redirectComponent = 'c__CreateEnterpriseEmeaReq'; 
            } 
            
            const navConfig = {
                type: "standard__component",
                attributes: {
                componentName: this.redirectComponent
                },
                state: {
                    "c__recId": this._recordId,
                    "c__tgsOppId" : '',
                    "c__soppId" : '',
                    "c__oppRecId" : this.recordTypeId,
                    "c__TFOFlag" : true,
                    "c__address" : this.address,
                    "c__accId" : this.acc,
                    "c__conId" : this.con                    
                }
            };
            this[NavigationMixin.Navigate](navConfig);
        }        
    }

}