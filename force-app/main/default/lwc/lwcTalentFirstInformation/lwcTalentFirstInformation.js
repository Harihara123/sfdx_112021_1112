import { LightningElement, wire, api, track } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import CURRENT_COMPANY from '@salesforce/schema/Account.Talent_Current_Employer__c';
import { loadStyle } from 'lightning/platformResourceLoader';
import csshelptext from '@salesforce/resourceUrl/csshelptext';
import { NavigationMixin } from 'lightning/navigation';

const fields = [CURRENT_COMPANY];

export default class LwcTalentFirstInformation extends NavigationMixin(LightningElement) {
    activeSections = ['A', 'B'];
    activeSectionsMessage = '';
    editFlag = false;
    topSkills = [];
    location;
    billMin='N/A';
    billMax='N/A';
    salMin='N/A';
    salMax='N/A';
    @api recordId;
    showSkills = false;
    showLocation = false;
    showCompany = false;
    currentCompany;
	formattedTopSkill;	
	accntId;
	opportunityOwner;
    oppRecord = {};

    @wire(getRecord, { recordId: '$accntId', fields: fields })
    accRecord({error, data}) {
        if(data && data.fields.Talent_Current_Employer__c.value && (data.fields.Talent_Current_Employer__c.value != 'null' || data.fields.Talent_Current_Employer__c.value != 'undefined')) {
            this.currentCompany = data.fields.Talent_Current_Employer__c.value;
            this.showCompany = true;            
        }
    }

    recordLoad(event) {              
            let recordUI = event.detail.records;
            let skills = recordUI[this.recordId]?.fields?.EnterpriseReqSkills__c?.value;            
            if(skills) {
                let parseSkills = JSON.parse(skills);
                this.topSkills = parseSkills?.skills;
                this.showSkills = true;
            }
            this.location = this.checkNull(recordUI[this.recordId]?.fields?.Req_Worksite_Street__c?.value) + ',' + 
                            this.checkNull(recordUI[this.recordId]?.fields?.Req_Worksite_City__c?.value) + ',' +
                            this.checkNull(recordUI[this.recordId]?.fields?.Req_Worksite_State__c?.value) + ',' +
                            this.checkNull(recordUI[this.recordId]?.fields?.Req_Worksite_Country__c?.value) + ',' +
                            this.checkNull(recordUI[this.recordId]?.fields?.Req_Worksite_Postal_Code__c?.value);
            this.showLocation = true;
            this.billMin = recordUI[this.recordId]?.fields?.Req_Pay_Rate_Min__c?.value;        
            this.billMax = recordUI[this.recordId]?.fields?.Req_Pay_Rate_Max__c?.value;        
            this.salMin = recordUI[this.recordId]?.fields?.Req_Salary_Min__c?.value;        
            this.salMax = recordUI[this.recordId]?.fields?.Req_Salary_Max__c?.value;
            this.accntId = recordUI[this.recordId]?.fields?.AccountId?.value;      
            this.Req_Opportunity_Owner__c= recordUI[this.recordId]?.fields?.OwnerId?.value;
			
			var yPos=document.getElementById("oppLogistics")?.offsetTop;
			window.scrollTo(0,yPos-50);        
    }

	handleCancel(event){
		this.editFlag = false;
	}

	onSubmit(event){
        event.preventDefault();
        const inputFields = event.detail.fields;
        if (inputFields) {            
			inputFields.OwnerId=inputFields.Req_Opportunity_Owner__c;
			inputFields.EnterpriseReqSkills__c=JSON.stringify(this.formattedTopSkill);			
        }
		event.detail.fields=inputFields;						
        this.template.querySelector('lightning-record-edit-form').submit(inputFields);
		this.editFlag = false;
	}

    handleTopSkillsChange(event) {
        this.topSkills = event.detail.skills;
		this.formattedTopSkill={"skills":this.topSkills};
    }

	handleOwnerChange(event){
		this.opportunityOwner = event.target.value;
		this.OwnerId = event.target.value;
	}
	
    checkNull(param) {
        if(param == null || param == undefined) {
            return '';
        }
        else {
            return param;
        }
    }

    handleSectionToggle(event) {
        const openSections = event.detail.openSections;

        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        }
    }

    handleEditClick(event) {
        this.editFlag = true;
    }   

    redirectToAccount(event) {        
        let currValue = event.target.value; 
        if(currValue.includes("href=")) {
            currValue = currValue.split(" ");
            let companyId = currValue[1].slice(6, currValue[1].lastIndexOf('"'));
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: companyId,
                    actionName: 'view',
                },
            }).then(url => {
                window.open(url);
            });
        }
    }
    
    renderedCallback() {        
        Promise.all([
            loadStyle( this, csshelptext )
            ]).then(() => {
                console.log( 'Files loaded' );
            })
            .catch(error => {
                console.log( error.body.message );
        });
    }
}