import { LightningElement, track, api } from 'lwc';
import getListViewData from '@salesforce/apex/TalentFirstListController.getListViewData';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Opportunity Name', fieldName: 'oppLink', type: 'url', wrapText: false, hideDefaultActions: true, typeAttributes: { label: { fieldName: "oppName", target: '_blank' } } },
    { label: 'Opportunity Owner', fieldName: 'ownerLink', type: 'url', wrapText: false, hideDefaultActions: true, typeAttributes: { label: { fieldName: "ownerName", target: '_blank' } } },
    { label: 'Job Title', fieldName: 'jobTitle', type: 'text', wrapText: false, hideDefaultActions: true },
    { label: 'Office', fieldName: 'officeLink', type: 'url', wrapText: false, hideDefaultActions: true, typeAttributes: { label: { fieldName: "officeName", target: '_blank' } } },
    { label: 'Division', fieldName: 'division', wrapText: false, hideDefaultActions: true },   
    { label: 'Stage', fieldName: 'stage', wrapText: false, hideDefaultActions: true },    
    { label: 'Created Date', fieldName: 'createdDate', type: 'date-local', wrapText: false, hideDefaultActions: true },
    { label: 'Close Date', fieldName: 'closeDate', type: 'date-local', wrapText: false, hideDefaultActions: true },    
    { label: '100% Remote Work', fieldName: 'remoteWork', type: 'text', wrapText: false, hideDefaultActions: true }
];

export default class LwcTalentFirstList extends LightningElement {
    @api recordId;

    @track tableData = [];
    @track showTable = false;
    @track loading = false;

    columns = columns;
    

    connectedCallback() {
        this.getData();
    }

    getData() {
        this.loading = true;
        getListViewData({contactId: this.recordId})
        .then(response => {
            if (response.length > 0) {
                var tableData = response;
                
                // create links
                tableData.forEach(item => {
                    item['oppLink'] = item['oppLink'] != null ? "/" + item['oppLink'] : item['oppLink'];
                    item['ownerLink'] = item['ownerLink'] != null ? "/" + item['ownerLink'] : item['ownerLink'];
                    item['officeLink'] = item['officeLink'] != null ? "/" + item['officeLink'] : item['officeLink'];
                });

                this.tableData = tableData;
                this.showTable = true;
            }
            this.loading = false;
        }) 
        .catch(error => {
            this.loading = false;
			this.showEmptyState = true;
            this.showToast('', 'An error occurred while loading the TFO tab, please contact to System Administrator.', 'error');
			console.log(error);
        });
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }
}