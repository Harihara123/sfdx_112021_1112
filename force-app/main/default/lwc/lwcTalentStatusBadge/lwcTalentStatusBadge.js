import { LightningElement, api } from 'lwc';

export default class LwcTalentStatusBadge extends LightningElement {
    @api status = '';

    get getClass() {
        return (this.status) ? this.status.toLowerCase() : ''; 
    }
}