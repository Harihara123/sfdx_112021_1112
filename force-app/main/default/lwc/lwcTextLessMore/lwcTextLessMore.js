import { LightningElement, api } from 'lwc';

const toggle = {
    'Show More': 'Show Less',
    'Show Less': 'Show More'
}

const DEFAULT_LIMIT = 255;

export default class LwcTextLessMore extends LightningElement {
    text = '';
    fullText = '';
    toggleLabel = ''
    @api expanded = false;
    @api limit = DEFAULT_LIMIT;
    @api isTGSReq = false;
    @api
    get value() {
        return this.text;
    }
    set value(value) {
        setTimeout(() => {
            this.fullText = value;
            if(this.isTGSReq && this.template.querySelector('.toggle')) {
                this.template.querySelector('.toggle').classList.add('toggle-left');
                this.template.querySelector('.toggle').classList.remove('toggle');
            }
            if (value && value.length > this.limit) {
                if (this.expanded) {
                    this.text = value;
                    this.toggleLabel = 'Show Less'
                } else {
                    this.text = value.substring(0,this.limit)+'...';
                    this.toggleLabel = 'Show More'
                }
            } else {
                this.toggleLabel = ''
                this.text = value;
            }
        },0)
    }
    handleToggle() {
        this.expanded = !this.expanded;
        this.toggleLabel = toggle[this.toggleLabel];

        if (this.toggleLabel === 'Show Less') {
            this.text = this.fullText;
        } else {
            this.text = this.fullText.substring(0, this.limit)+'...'
        }
    }
}