import { LightningElement, api } from 'lwc';
import {trackedChannel} from 'c/lwcTrackingSubscriber';

export default class LwcTrackingProvider extends LightningElement {     
    channel = trackedChannel(); 

    connectedCallback() {
        this.connectChannel()
    }
    disconnectedCallback() {
        this.channel.unsubscribe(this.channelTrigger.bind(this));
    }
    @api getChannel() {
       return 'hello'
    }

    @api connectChannel() {
        this.channel.subscribe(this.channelTrigger.bind(this));
        //this.dispatchEvent(new CustomEvent('connectedChannel'))
        console.log('subscribing')
    }

    channelTrigger(data) {
        this.dispatchEvent(new CustomEvent('LWCEvent', {detail: {data}}))
        console.log(data)
    }

}