import { LightningElement } from 'lwc';

export const trackedChannel = (() => {
    let instance;

    const createInstance = () => {
        let subscribers = [];

        const send = (data) => {
            subscribers.forEach(fx => {fx(data)});
        }
        const subscribe = (fx) => {
            if (subscribers.indexOf(fx) < 0) {
                subscribers.push(fx) //subscribed fx was not found to exist in subscribers array; subscribe
            } else {                
                //fx found in subscribers array; do not subscribe again
            }
        }
        const unsubscribe = (fx) => {
            const index = subscribers.indexOf(fx);
            subscribers.splice(index, 1);
        }
        return {subscribe, unsubscribe, send}
    }

    return () => {
        if (!instance) {
            instance = createInstance();
        }
        return instance;
    }


})();

export default class LwcTrackingSubscriber extends LightningElement {
    trackedElements = [];
    sendData = trackedChannel().send


    disconnectedCallback() {
        if (this.trackedElements.length > 0) {
            this.clearListeners()
        }
    } 
    
    handleSlotChange(e) {
        const trackedElements = this.querySelectorAll('[data-tracker]');
        if (trackedElements) {
            trackedElements.forEach(tracked => {
                if (tracked.tagName === 'TEXTAREA') {
                    tracked.addEventListener('blur', this.handleInput.bind(this))
                    this.trackedElements.push({element: tracked, handler: this.handleInput.bind(this), listener: 'input'})
                } else {
                    tracked.addEventListener('click', this.handleClick.bind(this))
                    this.trackedElements.push({element: tracked, handler: this.handleClick.bind(this), listener: 'click'})
                }                
            })
        }
        else if (!trackedElements && this.trackedElements.length > 0) {
            //remove all the event listeners from the array
            this.clearListeners()
        }
    }

    clearListeners() {
        this.trackedElements.forEach(item => {
            item.element.removeEventListener(item.listener, item.handler);
        })
    }
    handleClick(e) {
        this.sendData({id: e.target.getAttribute('data-tracker')})
    }
    handleInput(e) {
        //console.log(e.target.value)
        this.sendData({value: e.target.value, id: e.target.getAttribute('data-tracker')});
    }
}