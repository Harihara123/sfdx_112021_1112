import { LightningElement, api } from 'lwc';

const USER_INFO_SCHEMA = [
    {field: 'Username', label: 'Username', type: 'input'},
    {field: 'Email', label: 'Email', type: 'text'},
    {field: 'Active', label: 'Active', type: 'checkbox', disabled: true},
    {field: 'StatusOverride', label: 'Talent Active Status Override', type: 'checkbox'},
    {field: 'ReasonforOverride', label: 'Reason for Override', type: 'input'},
	{field: 'FirstName', label: 'FirstName', type: 'input'},
	{field: 'LastName', label: 'LastName', type: 'input'},
    {field: 'Lastlogin', label: 'Last Login', type: 'text'},
    {field: 'Office', label: 'Office', type: 'text'},
    {field: 'Createdate', label: 'Created Date', type: 'text'},
    {field: 'Region', label: 'Region', type: 'text'},
    {field: 'Enddate', label: 'End Date', type: 'text'},
    {field: 'Startdate', label: 'Start Date', type: 'text'},
    {field: 'Finishcode', label: 'Finish Code', type: 'text'}
]

const CONTACT_INFO_SCHEMA = [
    {field: 'Email', label: 'Email', type: 'input'},
    {field: 'HomePhone', label: 'Home Phone', type: 'input'},
    {field: 'Phone', label: 'Phone', type: 'input'},
    {field: 'rwsId', label: 'RWSID', type: 'rwsid'},
    {field: 'Address', label: 'Address', type: 'text'}
]

const TEAM_INFO = [
    {field: 'Recruiter', label: 'Recruiter', object: 'User', dataField: 'Name'},
    {field: 'AccountManager', label: 'Account Manager', object: 'User', dataField: 'Name'},
    {field: 'CommunityManager', label: 'Community Manager', object: 'User', dataField: 'Name'},
    {field: 'TalentCSA', label: 'Talent CSA', object: 'User', dataField: 'Name'},
	{field: 'Talentbbiller', label: 'Biller', object: 'User', dataField: 'Name'},
    {field: 'CommunityRep', label: 'Community Rep', object: 'User', dataField: 'Name'},
    {field: 'TalentProgramManager', label: 'Program Manager', object: 'User', dataField: 'Name'},
    {field: 'TalentDeliveryManager', label: 'Delivery Manager', object: 'User', dataField: 'Name'},
	{field: 'TalentEmployeeServiceSpecialist', label: 'Employee Service Specialist', object: 'User', dataField: 'Name'}
]

export default class LwcUserDetails extends LightningElement {
    userInfo = [];
    contactInfo = [];
    teamInfo = [];
    buttonValue = '';
    CM = false;
    contactChanges = {};
    showSpinner = false;
    reasonErr = '';
    displayFields = [{label: "Title", fieldApiName: "Title", dataType: "text"}];
    @api modalPromise;
    @api userMethod;
    @api save;

    contactData
    @api 
    get values() {
        return this.contactData
    };
    set values(vals) {
        if (vals) {
            this.CM = !vals.isCommunityManager;
            console.log(vals.isCommunityManager);
            let newUserInfo = [];
            USER_INFO_SCHEMA.forEach(item => {
                if (vals.hasOwnProperty(item.field)) {
                    if (item.field === 'Active') {
                        newUserInfo.push({
                            field: item.field,
                            value: vals[item.field],
                            label: item.label,                        
                            [item.type]: true,
                            disabled:  this.CM
                        });
                    } else {

                        newUserInfo.push({
                            field: item.field,
                            value: vals[item.field],
                            label: item.label,                        
                            [item.type]: true,
                            disabled: true
                        });
                    }
                }                
            })
            this.userInfo = newUserInfo;

            let newContactInfo = [];
            CONTACT_INFO_SCHEMA.forEach(item => {
                if (vals.hasOwnProperty(item.field)) {
                    let newContactInfoField = {
                        field: item.field,
                        value: vals[item.field],
                        label: item.label,
                        [item.type]: true
                    }
                    if (item.type === 'rwsid') {
                        newContactInfoField.url = `http://rws.allegisgroup.com/RWS/candidate/displayProfile.action?candidateID=${item.value}`
                    }
                    newContactInfo.push(newContactInfoField);                    
                } 
            })

           
            this.contactInfo = newContactInfo;

            let newTeamInfo = [];
            TEAM_INFO.forEach(item => {
                if (vals.hasOwnProperty(item.field)) {
                    let newTeamInfoField = { //vals[item.field]
                        value: vals[item.field], 
                        objectName: item.object,
                        label: item.label,
                        dataField: item.dataField,
                        placeholder: item.label,
                        searchLookupText: item.label,
                        field: item.field                        
                    }
                    newTeamInfo.push(newTeamInfoField)                    
                } 
            })
            this.teamInfo = newTeamInfo;
        }
        this.buttonValue = vals.buttonValue;
        this.contactData = vals;
    }
    handleInput(e) {
        const field = e.target.getAttribute('data-field')
        const value = e.target.value;

        this.contactChanges[field] = value;

        // if (field === 'StatusOverride') {
        //     //VALIDATE REASON
        //     console
        //     this.template.querySelector('[data-field="ReasonforOverride"]').style = 'border: 1px solid #ac0000'
        // }
    }
    handleChange(e) {
        const field = e.target.getAttribute('data-field')
        const value = e.target.checked;
        this.contactChanges[field] = value;
    }
    handleSelected(e) {
        const id = e.detail.lookupid        
        this.contactChanges[e.target.getAttribute('data-id')+'Id'] = id.substring(1, id.length-1);
    }    
    handleCancel() {
        this.closeModal()
    }
    handleSave() {
        if(this.save && Object.keys(this.contactChanges).length) {
            const reasonInput = this.template.querySelector('[data-field="ReasonforOverride"]');
            if (!reasonInput.value) {
                const overideInput = this.template.querySelector('[data-field="StatusOverride"]')
                if (overideInput.checked) {
                    const reasonDiv = this.template.querySelector('[data-class="ReasonforOverride"]');                    

                    reasonDiv.classList.add('slds-has-error');
                    reasonInput.focus()
                } else {
                    const reasonDiv = this.template.querySelector('[data-class="ReasonforOverride"]'); 
                    reasonDiv.classList.remove('slds-has-error');
                    this.save({...this.contactData, ...this.contactChanges}, this.closeModal.bind(this), this.toggleSpinner.bind(this));
                }

            } else {
                this.save({...this.contactData, ...this.contactChanges}, this.closeModal.bind(this), this.toggleSpinner.bind(this));            
            }
        } else {
            this.closeModal();
        }
    }
    handleUserAction() {
        if (this.userMethod) {
            this.userMethod(this.contactData.userId, this.contactData.buttonValue, this.toggleSpinner.bind(this));            
        }        
    }
    closeModal() {
        if (this.modalPromise) {
			this.modalPromise.then(modal => {modal.close()});
        }
    }
    toggleSpinner() {
        this.showSpinner = !this.showSpinner;
    }
}