export const compareValues = (key, order = 'asc') => {
    return (a, b) => {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // property doesn't exist on either object
            return 0;
        }

        const varA = (typeof a[key] === 'string') ?
            a[key].toUpperCase() : a[key];
        const varB = (typeof b[key] === 'string') ?
            b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }

        return (
            (order === 'desc') ? (comparison * -1) : comparison
        );
    };
}

//expects an object as first argument and path as a string in second
//example:
// const obj = {nestedObj: {key: 'awesome value here'}}
//objPath(obj, 'nestedObj.key') ==> 'awesome value here'
export const objPath = (obj, path) => {
    return path.split('.').reduce((o,i)=>o[i], obj)
}

//returns environment (DEV, TEST, LOAD or PROD) by checking the address bar
export const getEnv = () => {
    const hostname = window.location.hostname;
    const regexEnv = /(dev|test|load)/;
    
    const env = regexEnv.exec(hostname);
    if (env) {
        return env[1].toUpperCase(); 
    }
    return 'PROD'
}