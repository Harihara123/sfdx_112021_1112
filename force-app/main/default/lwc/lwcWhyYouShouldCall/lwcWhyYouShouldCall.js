import { LightningElement, api} from 'lwc';

export default class LwcWhyYouShouldCall extends LightningElement {
    @api matchData;

    // Style for tooltip. Changes if tooltip falls below container bottom
    tooltipStyle = 'content-tooltip';

    get validationTooltip() {
        if(
            this.matchData.recent_submittal || 
            this.matchData.recent_applications || 
            this.matchData.close_to_end_date || 
            this.matchData.available_soon || 
            this.matchData.recent_resume_update || 
            this.matchData.in_pipeline) return true 
        if(
            !this.matchData.recent_submittal && 
            !this.matchData.recent_applications && 
            !this.matchData.close_to_end_date && 
            !this.matchData.available_soon && 
            !this.matchData.recent_resume_update && 
            !this.matchData.in_pipeline) return false 
    }

    handleHover() {
        let popoverContainer = this.template.querySelector('.content-tooltip'); // DIV popover element

        if (popoverContainer) {
            let resultsContainer = popoverContainer.closest('.resultsContainer'); // Container for ALL results
            let itemContainer = popoverContainer.closest('.resultItem'); // Container for the candidate row
            
            let containerBottom = resultsContainer.scrollTop + resultsContainer.offsetHeight; // Calulates scrolled bottom of result container
            let itemBottom = itemContainer.offsetTop + itemContainer.offsetHeight; // Calculates bottom of result item

            if ((itemBottom + popoverContainer.offsetHeight + 20) > containerBottom) { // If bottom of popover element is past the bottom of the visibile div (plus 20 for buffer)
                this.tooltipStyle = 'content-tooltip tooltipUp'; // Add style to make popover display above, instead of below
            }
            else {
                this.tooltipStyle = 'content-tooltip'; // Else, keep normal style
            }
        }
    }
}