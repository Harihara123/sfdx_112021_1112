import { LightningElement, api } from "lwc";

import { loadScript } from "lightning/platformResourceLoader";
import rsrc from "@salesforce/resourceUrl/References";

import { structure, content, doc, parts } from 'c/lwcRefDocxTemp';
//import { structure, content, doc, docxParts } from "c/lwc_RefDocxMultiTemplate";

export default class Lwc_RefDocxConstructor extends LightningElement {
    @api state = {};
    @api logo = null;
    @api docxDl = {};
    logoSize = {};

    @api
    get items() {
        //console.log("getter", this.state);
        return this.state;
    }
    set items(item) {
        item = JSON.parse(JSON.stringify(item));
        console.log(item);
        if (item) {
            let newData = [];
            item.forEach(ref => {
                let data = {
                    talent: this.constructTalentInfo(ref),
                    paras: this.constructParagraphs(ref.reference.recommendation),
                    ref: this.constructReference(ref.reference)
                };
                newData.push(data);
            });
            this.state = { downloadData: [...newData] };
            console.log(JSON.parse(JSON.stringify(this.state)), "--STATE--");
            // this.triggerEvent();
            //this.downloadDocx();
        }
        if (item) {
            let completeRef = [];
            item.forEach(ref => {
                completeRef.push({
                    completeName: this.parseSpecialChars(this.constructRefFor(ref)),
                    titleCompany: this.parseSpecialChars(this.constructTitleCompany(ref)),
                    refBy: this.parseSpecialChars(this.constructRefBy(ref)),
                    dateEmployed: this.parseSpecialChars(this.constructDateEmployed(ref)),
                    infoTable: this.constructInfoTable(ref),
                    paras: this.constructParagraphs(ref.reference.recommendation)
                })
            })
            // let completeRef = {
            //     completeName: this.parseSpecialChars(this.constructRefFor(item[0])),
            //     titleCompany: this.parseSpecialChars(this.constructTitleCompany(item[0])),
            //     refBy: this.parseSpecialChars(this.constructRefBy(item[0])),
            //     dateEmployed: this.parseSpecialChars(this.constructDateEmployed(item[0])),
            //     infoTable: this.constructInfoTable(item[0]),
            //     paras: this.constructParagraphs(item[0].reference.recommendation)
            // }
            this.docxDl = completeRef;
            console.log(completeRef);
            this.downloadDocxOld()
        }
    }
    constructInfoTable(ref) {
        let infoTable = []

        let rec = [
            ['Title', 'Position'],
            ['Relationship__c', 'Relationship'],
            ['Rehire_Status__c', 'Rehire'],
            ['Relationship_Duration__c', 'Duration']        
        ];
        rec.forEach(item => {
            if (item[1] === 'Position') {
                if (ref.reference.contact[item[0]]) {
                    infoTable.push({a: item[1],b: this.parseSpecialChars(ref.reference.contact[item[0]])})
                }
            } else {
                if (ref.reference.recommendation[item[0]]) {
                    infoTable.push({a: item[1],b: this.parseSpecialChars(ref.reference.recommendation[item[0]])})                
            }
            }

        })

        let contact = [
            ['Email', 'Email'],
            ['MobilePhone', 'Mobile'],
            ['Phone', 'Work'],
            ['HomePhone', 'Home']
        ]
        contact.forEach(item => {
            if (ref.reference.contact[item[0]]) {
                if (item[0] === 'Phone') {
                    if (ref.reference.contact.WorkExtension__c) {
                        infoTable.push({a: item[1],b: this.parseSpecialChars(ref.reference.contact[item[0]]+' ('+ref.reference.contact.WorkExtension__c+')')})
                    } else {infoTable.push({a: item[1],b: this.parseSpecialChars(ref.reference.contact[item[0]])})}
                } else {
                    infoTable.push({a: item[1],b: this.parseSpecialChars(ref.reference.contact[item[0]])})
                }
            }
        })  
        
        return infoTable
    }
    constructDateEmployed(ref) {
        let dateRange = [];
        ['Start_Date__c', 'End_Date__c'].forEach(item => {
            if (ref.reference.recommendation[item]) {
                dateRange.push(ref.reference.recommendation[item])
            }            
        })
        return dateRange.join(' - ').trim()
    }
    constructRefBy(ref) {
        const names = [
            "Salutation",
            "FirstName",
            "MiddleName",
            "LastName",
            "Suffix",
            "Title"
        ];
        let referee = [];
        names.forEach(item => {
            if (ref.reference.contact[item]) {                
                referee.push(item === "Title"?"● "+ref.reference.contact[item]:ref.reference.contact[item]);
            }
        });
        if (ref.reference.contact)
        return referee.join(' ').trim()

    }

    constructTitleCompany(ref) {
        const talentInfo = [
            "Talent_Job_Title__c",
            "Organization_Name__c"
        ];

        let infoArr = [];
        infoArr[0] = ref.reference.recommendation[talentInfo[0]]
            ? ref.reference.recommendation[talentInfo[0]] + " at"
            : "";
        infoArr[1] = ref.reference.recommendation[talentInfo[1]]
            ? ref.reference.recommendation[talentInfo[1]]
            : "";
        
        return infoArr.join(" ").trim()


    }

    constructRefFor(ref) {
        const names = ["Salutation","FirstName", "MiddleName", "LastName", "Suffix"];
        let completeName = ["Reference for"]

        names.forEach(item => {
            if (ref.talent[item]) {
                if(item === 'Suffix') {
                    completeName.push(`, ${ref.talent[item]}`);
                } else {
                    completeName.push(ref.talent[item]);
                }
                
            }
        });                
        return completeName.join(" ").trim();
    }

    triggerEvent() {
        this.dispatchEvent(new CustomEvent("done"));
    }

    constructParagraphs(obj) {
        let refPeraData = [];
        const refParaItems = [
            ["Talent_Job_Duties__c", "Job Duties &amp; Technologies"],
            ["Project_Description__c", "Project Description"],
            ["Workload_Description__c", "Work Load", "Workload__c"],
            ["Quality_Description__c", "Quality of Work", "Quality__c"],
            [
                "Competence_Description__c",
                "Performance &amp; Ability",
                "Competence__c"
            ],
            ["Leadership_Description__c", "Initiative", "Leadership__c"],
            [
                "Collaboration_Description__c",
                "Cooperation/Communication",
                "Collaboration__c"
            ],
            [
                "Trustworthy_Description__c",
                "Attendance/Reliability",
                "Trustworthy__c"
            ],
            ["Cultural_Environment__c", "Cultural Environment"],
            ["Non_technical_skills__c", "Non-Technical Skills"],
            ["Recommendation__c", "Strengths"],
            ["Growth_Opportunities__c", "Growth Opportunities"],
            ["Additional_Reference_Notes__c", "Additional Information &amp; Comments"]
        ];
        refParaItems.forEach(item => {
            if (obj[item[0]] || obj[item[2]]) {
                refPeraData.push({
                    title: item[1],
                    value: obj[item[0]]?this.parseSpecialChars(obj[item[0]]):'',
                    rating: obj[item[2]] ? obj[item[2]] : ""
                });
            }
        });
        //console.log(refPeraData);
        return refPeraData;
    }
    constructTalentInfo(data) {
        const { talent, reference } = data;
        let talentData = ["Reference for"];

        const talentItems = ["FirstName", "MiddleName", "LastName"];
        const talentInfo = [
            "Talent_Job_Title__c",
            "Organization_Name__c",
            "Start_Date__c",
            "End_Date__c"
        ];

        talentItems.forEach(item => {
            if (talent[item]) {
                talentData.push(data.talent[item]);
            }
        });

        let infoArr = [];
        infoArr[0] = reference.recommendation[talentInfo[0]]
            ? reference.recommendation[talentInfo[0]] + " at"
            : "";
        infoArr[1] = reference.recommendation[talentInfo[1]]
            ? reference.recommendation[talentInfo[1]]
            : "";
        infoArr[2] =
            reference.recommendation[talentInfo[2]] &&
                reference.recommendation[talentInfo[3]]
                ? "● " +
                reference.recommendation[talentInfo[2]] +
                " - " +
                reference.recommendation[talentInfo[3]]
                : "";


        return [
            {
                template: "REF_FOR",
                value: this.parseSpecialChars(talentData.join(" ").trim())
            },
            {
                template: "TALENT_INFO",
                value: this.parseSpecialChars(infoArr.join(" ").trim())
            }
        ];

    }
    constructReference(ref) {
        let refData = [];
        let referee = ["Reference by"];
        const refContactItems = [
            ["FirstName", "FNAME"],
            ["MiddleName", "MNAME"],
            ["LastName", "LNAME"],
            ["Title", "POSITION"],
            ["Email", "EMAIL"],
            ["MobilePhone", "MOBILE"],
            ["Phone", "WORK"],
            ["HomePhone", "HOME"],
            ["WorkExtension__c", "EXTENSION"]
        ];
        const refNames = [
            "Salutation",
            "FirstName",
            "MiddleName",
            "LastName",
            "Suffix"
        ];

        const refItems = [
            ["Organization_Name__c", "COMPANY"],
            ["Relationship__c", "RELATIONSHIP"],
            ["Rehire_Status__c", "REHIRE"],
            ["Talent_Job_Title__c", "TALENT_TITLE"],
            ["Start_Date__c", "START_DATE"],
            ["Start_Date__c", "END_DATE"],
            ["Relationship_Duration__c", "DURATION"]
        ];

        refContactItems.forEach((item, index) => {
            if (ref.contact[item[0]]) {
                refData.push({
                    template: item[1],
                    value: (index === refContactItems.length -1)?this.parseSpecialChars('('+ref.contact[item[0]]+')'):this.parseSpecialChars(ref.contact[item[0]])
                });
            } else {
                refData.push({ template: item[1], value: "" });
            }
        });

        refNames.forEach(item => {
            if (ref.contact[item]) {
                referee.push(ref.contact[item]);
            }
        });
        let refereeString = referee.join(" ").trim();

        refData.push({
            template: "REF_BY",
            value: this.parseSpecialChars(refereeString)
        });

        refItems.forEach(item => {
            if (ref.recommendation[item[0]]) {
                refData.push({
                    template: item[1],
                    value: this.parseSpecialChars(ref.recommendation[item[0]])
                });
            } else {
                refData.push({ template: item[1], value: "" });
            }
        });

        //console.log(refData);
        return refData;
    }
    constructTemplateString(rec, at = 1, recString = '{HEADER0}{INFO0}{PARA0}{PAGE_BR0}') {
        if (rec === at) {
            recString = recString.replace(`{PAGE_BR${at - 1}}`, '');
            return recString;
        }
        recString = `${recString}{HEADER${at}}{INFO${at}}{PARA${at}}{PAGE_BR${at}}`
        return this.constructTemplateString(rec, at + 1, recString)
    }
    parseSpecialChars(string) {
        return string
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&apos;");
    }
    getImageSize() {
        return new Promise((resolve, reject) => {
            const i = new Image(); 
            i.onload = () => {
                const emuInPx = 9525;
                const aspectRatio = i.width/i.height;
                const logoHeight = (161 / aspectRatio)*emuInPx;
                const logoWidth = 161*emuInPx;
                
                resolve({cx: logoWidth | 0, cy: logoHeight | 0});                

                // const checkFit = (x,y, ar) => {
                //   if (x<=39 && y<=161) {
                //     //GTG case
                //     resolve({cx: x*9525 | 0, cy:y*9525 | 0});
                //   } else {
                //     if (x>=39) {
                //       //set height to 39 and adjust width
                //       y=39
                //       x = y*ar
                //       resolve({cx: x*9525 | 0, cy:y*9525 | 0});
                                            
                //     }
                //     if (y>=161) {
                //       //set width to 161 and adjust height
                //       x=161
                //       y=x/ar
                //       resolve({cx: x*9525 | 0, cy:y*9525 | 0});
                //     }
                //   }
                  
                // }
                // checkFit(logoWidth, logoHeight, aspectRatio); 
                //resolve({cx: 1533525, cy:logoHeight*9525 | 0}); 
            };
            i.src = 'data:image/png;base64,'+this.logo; 
            setTimeout(() => {resolve({cx:0, cy:0})}, 3000);
        })
        

    }
    connectedCallback() {
        loadScript(this, rsrc + "/references/js/jszip.min.js")
            .then(() => console.log("loaded!", window))
            .catch(err => console.log(err));
        loadScript(this, rsrc + "/references/js/FileSaver.js")
            .then(() => console.log("loaded!", window))
            .catch(err => console.log(err));
    }
    constructDocx(item, last) {
        let main = parts.refFor(item.completeName);
        let second = parts.titleCompany(item.titleCompany)
            +parts.refByDate('Reference by', item.refBy)
            +(item.dateEmployed?parts.refByDate('Date employed', item.dateEmployed):'');
        let logo = parts.logo(this.logoSize.cx, this.logoSize.cy);

        let tableHeader = parts.tableHeader(main, second, logo)+parts.spacer


        let infoTableParas = '';
        item.infoTable.forEach(item => {infoTableParas += parts.infoTablePara(item.a, item.b)});

        let infoTable = parts.infoTable(parts.spacer+infoTableParas+parts.spacer)+parts.spacer;


        let paragraphs = '';
        item.paras.forEach(item => {
            if (item.value) {
                paragraphs += parts.title(item.title, item.rating)+parts.paragraph(item.value)+parts.spacer
            } else {
                paragraphs += parts.title(item.title, item.rating)+parts.spacer
            }
            
        })

        return last? tableHeader+infoTable+paragraphs:tableHeader+infoTable+paragraphs+parts.pageBr;
    }
    downloadDocxOld() {
        this.getImageSize().then(result => {
        //example code
        //let strings = { ...content };
        this.logoSize = result;
        const zip = new window.JSZip();
        structure.files.forEach(file => {
            if (file === "word/document.xml") {
                let fullDoc = '';
                this.docxDl.forEach((ref, index) => {
                    fullDoc += this.constructDocx(ref, this.docxDl.length-1 === index)
                })
                zip.file(file, doc(fullDoc));                
            } else {
                if (file === "word/media/image1.png") {
                    if (this.logo) {
                        zip.file(file, this.logo, { base64: true });
                    } else {
                        zip.file(file, content[file], { base64: true });
                    }
                } else {
                    zip.file(file, content[file]);
                }
            }
        });

        zip.generateAsync({ type: "blob" }).then(collection => {
            window.saveAs(
                collection,
                `${this.docxDl[0].completeName}.docx`
            );
            this.triggerEvent();

        });
        }).catch(err => console.log(err))

    }
    constructDoc(array, last = true) {
        // let newTemp = doc;
        let newTemp = last ? '{HEADER}{INFO}{PARA}' : '{HEADER}{INFO}{PARA}{PAGE_BR}';

        //Construct the Header
        let header = docxParts.headerTbl;
        array.talent.forEach(item => {
            header = header.replace(`{${item.template}}`, item.value);
        });
        newTemp = newTemp.replace("{HEADER}", header);
        //Set image size
        const cxString = "{cx}";
        const cxRegx = new RegExp(cxString, "g");
        newTemp = newTemp.replace(cxRegx, this.logoSize.cx);

        const cyString = "{cy}";
        const cyRegx = new RegExp(cyString, "g");
        newTemp = newTemp.replace(cyRegx, this.logoSize.cy);        

        // newTemp = newTemp.replace("{cx}", this.logoSize.cx);
        // newTemp = newTemp.replace("{cy}", this.logoSize.cy);

        //Construct Info Table
        let infoTable = docxParts.infoTbl;
        array.ref.forEach(item => {
            infoTable = infoTable.replace(`{${item.template}}`, item.value);
        });
        newTemp = newTemp.replace("{INFO}", infoTable);

        //Construct Paragraphs
        let paraTable = docxParts.paraTable;
        array.paras.forEach((item, index) => {
            if (index === array.paras.length - 1) {
                //Last item in the array; do not append {PARAGRAPH} at the end.
                let paragraph = docxParts.paragraph;
                ["TITLE", "RATING", "VALUE"].forEach(template => {
                    paragraph = paragraph.replace(
                        `{${template}}`,
                        item[template.toLocaleLowerCase()]
                    );
                });
                paraTable = paraTable.replace("{PARAGRAPH}", paragraph);
            } else {
                //not the last item in paragraph array; append {PARAGRAPH} for next item.
                let paragraph = docxParts.paragraph;
                ["TITLE", "RATING", "VALUE"].forEach(template => {
                    paragraph = paragraph.replace(
                        `{${template}}`,
                        item[template.toLocaleLowerCase()]
                    );
                });
                paraTable = paraTable.replace("{PARAGRAPH}", paragraph + "{PARAGRAPH}");
            }
        });
        newTemp = newTemp.replace("{PARA}", array.paras.length > 0? paraTable:'');

        //Add page break if exists
        if (!last) {
            newTemp = newTemp.replace("{PAGE_BR}", docxParts.pageBr);
        }
        return newTemp;
    }

    downloadDocx() {
        this.getImageSize().then((result) => {
            this.logoSize = result;
            const zip = new window.JSZip();
            structure.files.forEach(file => {
                if (file === "word/document.xml") {
                    let newTemp = doc;
                    let tempDoc = '';
                    this.state.downloadData.forEach((fullDoc, index) => {
                            tempDoc = tempDoc + this.constructDoc(fullDoc, index === this.state.downloadData.length - 1);
                    })
                    newTemp = newTemp.replace('{FULL_REF}', tempDoc);
                    zip.file(file, newTemp);
                } else {
                    if (file === "word/media/image1.png") {
                        zip.file(file, this.logo, { base64: true });
                    } else {
                        zip.file(file, content[file]);
                    }
                }
            });
            zip.generateAsync({ type: "blob" }).then((archive) => {
                // see FileSaver.js
                window.saveAs(archive, `${this.state.downloadData[0].talent[0].value}.docx`);
                this.triggerEvent();
            });
        })        

    }
}