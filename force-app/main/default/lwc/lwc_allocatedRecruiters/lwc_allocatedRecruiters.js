import { LightningElement, api, track } from 'lwc';
import getRecruiters from '@salesforce/apex/ReqRoutingController.getRecruiters';
import saveAllocatedRecruiter from '@salesforce/apex/ReqRoutingController.saveAllocatedRecruiter';
import getPastAllocated from '@salesforce/apex/ReqRoutingController.getPastAllocated';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


const columns = [
    { label: 'Name', fieldName: 'recordLink', type: 'url', wrapText: false, hideDefaultActions: true, typeAttributes: { label: { fieldName: "Name" }, tooltip:{ fieldName: "Name" }, target: "_blank" } },
    { label: 'Email', fieldName: 'Email', type: 'email', wrapText: false, hideDefaultActions: true },
    { label: 'Phone', fieldName: 'Phone', type: 'phone', wrapText: false, hideDefaultActions: true },
    { label: 'Office', fieldName: 'Office__c', wrapText: false, hideDefaultActions: true },
    { label: 'Title', fieldName: 'Title', wrapText: false, hideDefaultActions: true },    
];


export default class Lwc_allocatedRecruiters extends LightningElement {
    @track isModalOpen = false;
    @track hasPastAllocations = false;
    @track foundNoRecruitersOnSearch = false;    
    pastAllocationsData = [];
    findRecruitersData = [];
    allSearchResult = [];
	selectedRow;
	selectedRecords;
	finalSelectedRecords=[];
    columns = columns;
    queryTerm;
	OppId;
    sortBy;
    sortDirection;
    isLoaded = false;
	toastMessage;

    connectedCallback() {
                        
    }

    @api openModal(flag,oppid) {		
        this.isModalOpen = flag;
		this.OppId=oppid;
		this.pastAllocationData();
    }

	@api pastAllocationData(){
		getPastAllocated({OppId: null})
            .then((result) => {
                console.log('result:'+JSON.stringify(result));
                if(result !== null) {
                    let tempRecruitersData=[];
                    for (let i = 0; i < result.length; i++) {  
                        let tempRecord = Object.assign({}, result[i]); //cloning object  
                        if(tempRecord.UserRoleId != undefined && tempRecord.UserRoleId != null) {
                            tempRecord.userRoleName = tempRecord.UserRole.Name;
                        }    
                        tempRecord.recordLink = "/" + tempRecord.Id;  
                        tempRecruitersData.push(tempRecord);  
                    }
                    this.pastAllocationsData=tempRecruitersData;                    
                    this.hasPastAllocations = true;                       
                    console.log(this.pastAllocationsData);
                    let divblock = this.template.querySelector('[data-id="past-recruiters"]');
                    if(divblock){
                        this.template.querySelector('[data-id="past-recruiters"]').classList.add('set-height');
                    }
                } else {
				    this.hasPastAllocations = false;    
                    let divblock = this.template.querySelector('[data-id="past-recruiters"]');
                    if(divblock && this.template.querySelector('[data-id="past-recruiters"]').classList.contains('set-height')){
                        this.template.querySelector('[data-id="past-recruiters"]').classList.remove('set-height');
                    }          
                }
                //this.dispatchEvent(evt);
            })
	}

    addMoreData() {
        const offset = this.findRecruitersData.length;
        let newData = [];
        if(this.allSearchResult.length > 0) {
            newData = this.allSearchResult.splice(0,5);
            this.findRecruitersData = [...this.findRecruitersData, ...newData];
        }   
        //this.addMoreDataCounter++;
    }

    onLoadMoreHandler() {
        console.log('load more');
        this.addMoreData();

        /*if(this.addMoreDataCounter === 4) {
            this.isLoaded = false;
        }*/
        if(this.allSearchResult.length === 0) {
            this.isLoaded = false;
        }
    }
    
    closeModal() {        
        this.isModalOpen = false;
        console.log(this.queryTerm);
        this.foundNoRecruitersOnSearch = false;
        this.findRecruitersData = [];
        this.allSearchResult = [];
		this.clearData();
        var divblock = this.template.querySelector('[data-id="search-table"]');
        if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
            this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
        }
    }

	handleRowSelection (event){
        this.selectedRow=event.detail.selectedRows;        
    }

	getSelectedRecords (event){
		this.selectedRecords=event.detail.selectedRows;
	}

    submitDetails(evt) {
        this.isModalOpen = false;
        console.log(this.queryTerm);
		if(this.selectedRow !=undefined && this.selectedRow != null){
			this.selectedRow.forEach((item) => this.finalSelectedRecords.push(item));
		}
		if(this.selectedRecords !=undefined && this.selectedRecords != null){
            this.selectedRecords.forEach((item) => this.finalSelectedRecords.push(item));
		}
        if(this.finalSelectedRecords.length == 0) {
            this.toastMessage = 'No recruiter selected. Please, select at least one.'
            this.showInfoToast('error');
        }
        else {
            saveAllocatedRecruiter({ RecruiterData: this.finalSelectedRecords,OppId: this.OppId })
                .then((result) => {
                    console.log('result:'+JSON.stringify(result));
                    if(result !== null) {
                        if(result.createdRecords !== undefined){
                            this.toastMessage= result.createdRecords+ ' Recruiter(s) have been allocated' ;
                            this.showInfoToast('success');
                        }               
						//console.log('result.updatedRecords-----------'+result.updatedRecords);
						//console.log('result.createdRecords-----------'+result.createdRecords);       
                        /*if(result.ExistingRecords !== undefined && result.updatedRecords < 1){
                            this.toastMessage= result.ExistingRecords+ ' Recruiter(s) you have selected is already added to the req';
                            this.showInfoToast('info'); 
                        } 
						if(result.ExistingRecords !== undefined && result.updatedRecords > 0){
                            this.toastMessage= result.updatedRecords+ ' Recruiter(s) have been allocated' ;
                            this.showInfoToast('success');
                        }*/
                        
                        if(result.ExistingRecords !== undefined && result.updatedRecords == undefined){
                            this.toastMessage= result.ExistingRecords+ ' Recruiter(s) you have selected is already added to the req';
                            this.showInfoToast('info'); 
                        }
                        if(result.ExistingRecords !== undefined && result.updatedRecords !== undefined){
                            this.toastMessage= result.updatedRecords+ ' Recruiter(s) have been allocated' ;
                            this.showInfoToast('success'); 
                        }  
                        if(result.ExistingRecords !== undefined && result.updatedRecords > 0){
                            this.toastMessage= result.updatedRecords+ ' Recruiter(s) have been allocated' ;
                            this.showInfoToast('success');
                        }

                        const selectedEvent = new CustomEvent("allocate", {
                            detail: this.finalSelectedRecords
                        });
                        this.dispatchEvent(selectedEvent); 				                   
                    } else {
                        
                    }
                    this.clearData();
                    this.pastAllocationData();
                    
                    //this.dispatchEvent(evt);
                })
            }       
       
    }

    handleKeyChange(evt) {
        const isEnterKey = evt.keyCode === 13;
        this.queryTerm = evt.target.value;
        if (this.queryTerm != '') {
            //this.queryTerm = evt.target.value;            
            getRecruiters({ searchText: this.queryTerm })
            .then((result) => {
                console.log('result:'+JSON.stringify(result));
                if(result !== null) {
                    let tempRecruitersData=[];
                    for (let i = 0; i < result.length; i++) {  
                        let tempRecord = Object.assign({}, result[i]); //cloning object  
                        if(tempRecord.UserRoleId != undefined && tempRecord.UserRoleId != null) {
                            tempRecord.userRoleName = tempRecord.UserRole.Name;
                        }    
                        tempRecord.recordLink = "/" + tempRecord.Id;  
                        tempRecruitersData.push(tempRecord);  
                    }
                    this.allSearchResult=tempRecruitersData;
                    this.findRecruitersData=this.allSearchResult.splice(0,5);
                    this.foundNoRecruitersOnSearch = true;
                    this.isLoaded = true;
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock){
                        this.template.querySelector('[data-id="search-table"]').classList.add('set-height');
                    }    
                    console.log(this.findRecruitersData);
                } else {
                    this.foundNoRecruitersOnSearch = false;
                    var divblock = this.template.querySelector('[data-id="search-table"]');
                    if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                        this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
                    }
                }
                //this.dispatchEvent(evt);
            })
        }
        else {
            console.log(this.queryTerm);
            this.foundNoRecruitersOnSearch = false;
            this.findRecruitersData = [];
            this.allSearchResult = [];
            var divblock = this.template.querySelector('[data-id="search-table"]');
            if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
                this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
            }
        }
			//this.dispatchEvent(evt);		
    }

    handleSortdata(event) {        
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {        
        let parseData = JSON.parse(JSON.stringify(this.allSearchResult));
        let keyValue = (a) => {
            return a[fieldname];
        };
        let isReverse = direction === 'asc' ? 1: -1;
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : '';
            y = keyValue(y) ? keyValue(y) : '';
            return isReverse * ((x > y) - (y > x));
        });
        this.findRecruitersData = parseData;
    }

	showInfoToast(variant) {
        const evt = new ShowToastEvent({            
            message: this.toastMessage,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

	 clearData() {
		this.selectedRow=null;
		this.selectedRecords=null;
		this.finalSelectedRecords=[];
		this.foundNoRecruitersOnSearch = false;
        this.findRecruitersData = [];
        this.allSearchResult = [];
		this.toastMessage='';
        var divblock = this.template.querySelector('[data-id="search-table"]');
        if(divblock && this.template.querySelector('[data-id="search-table"]').classList.contains('set-height')){
            this.template.querySelector('[data-id="search-table"]').classList.remove('set-height');
        }
	 }
}