import { LightningElement, api, track} from 'lwc';

import ATS_DELETE_CONFIRMATION_HEADER from '@salesforce/label/c.ATS_DELETE_CONFIRMATION_HEADER';
import ATS_DELETE_CONFIRMATION from '@salesforce/label/c.ATS_DELETE_CONFIRMATION'

import ATS_YES from '@salesforce/label/c.ATS_YES'
import ATS_NO from '@salesforce/label/c.ATS_NO'

export default class Lwcconfirmationmodal extends LightningElement {

    @api recordId;
	@track hasRendered = true;

    label = {        
        ATS_DELETE_CONFIRMATION_HEADER,
        ATS_DELETE_CONFIRMATION,
        ATS_YES,
        ATS_NO,
    };

	//S-190757 - Make delete button in the actions dropdown of the callsheet JAWS compatible
	renderedCallback() {
	   if (this.hasRendered) {
		   const closeModalIcon = this.template.querySelector('[data-id="closeModalBtn"]');
		   closeModalIcon.focus();
		   this.hasRendered = false;
       }
    }

    closeModal(){
        const closeModalEvent = new CustomEvent('confirmmodalcloseevent', {                        
            detail : {"recordId":this.recordId}
        });
        this.dispatchEvent(closeModalEvent);
    }
    saveModal(){
        const saveModalEvent = new CustomEvent('confirmmodalsaveevent', {                        
            detail : {"recordId":this.recordId}
        });
        this.dispatchEvent(saveModalEvent);
    }

	///S-190757 - Make delete button in the actions dropdown of the callsheet JAWS compatible
	setManualFocusforYesBtn(event){
		event.preventDefault();
		//focus close icon on Tab
		if ( event.keyCode == 9 ) {	
			const closeModalIcon = this.template.querySelector('[data-id="closeModalBtn"]');
			closeModalIcon.focus();
		}

		//focus No button on shift tab
		if (event.shiftKey && event.keyCode == 9) {
			const noModalIcon = this.template.querySelector('[data-id=noModalBtn]');
			noModalIcon.focus();
		}

	}

	setManualFocusforCloseBtn(event){
		//focus No button on Yes tab
		if (event.shiftKey && event.keyCode == 9) {
			event.preventDefault();
			const yesModalIcon = this.template.querySelector('[data-id=yesModalBtn]');
			yesModalIcon.focus();
		}
	}
	///end
}