import { LightningElement, api, track, wire } from 'lwc';
import globalLOVSearch from '@salesforce/apex/globalLOVSearch.globalLOVSearch'

const MIN_LENGTH = 3;
const DEFAULT_LIST_STATE = 'hide';
//Delay to reduce amount of Apex callouts
const DELAY = 500;


export default class Lwclookupltng extends LightningElement {
    @api minLength = MIN_LENGTH
    @api placeholder = ''
    @api textValue = ''
    @api searchString = ''
    @api whereCond = ''
    @api lovname = ''
    @api sortCond = ''
    @track listItems = []
    @track focusTracker = -1;
    listState = DEFAULT_LIST_STATE    


    renderedCallback() {
        console.log('callback add event listener')
        document.addEventListener('keyup', this.handleKeyUp);

    }

    @wire(globalLOVSearch, {whereCondition: '$whereCond',
                         LOVName: '$lovname',
                         sortCondition: '$sortCond',
                         searchString: '$searchString'
                         })
    getLOV({error, data}) {
        if(data) {
            this.listItems = data
            this.focusTracker = -1;
            if(this.listItems.length < 1)
                this.hideList();
        }
        else {
            this.hideList();
        }
    }

    @api showList() {
        if(this.searchString.length >= 3)
            this.listState = 'show';
    }

    @api hideList() {
        this.focusTracker = -1;
        this.listState = 'hide';
    }

    @api
    focus() {
        const input = this.template.querySelector('lightning-input');
        if (input) {
            input.focus()
        }
    }

    blurHandler() {
        setTimeout(() => {
            this.hideList();
         }, 250);
    }

    defocusPreviousItem(){
        if(this.focusTracker >= 0){
            let proxy = JSON.parse(JSON.stringify(this.listItems))          
            var focusedItem = proxy[this.focusTracker];
            const textInput = this.template.querySelector('[data-id='+focusedItem.Id+']');
            textInput.className = 'deFocusList';            
        }
    }

    focusPreviousItem(){
        if(this.focusTracker >= 0){
            let proxy = JSON.parse(JSON.stringify(this.listItems))          
            var focusedItem = proxy[this.focusTracker];
            const textInput = this.template.querySelector('[data-id='+focusedItem.Id+']');
            textInput.className = 'listItemFocus';            
        }
    }

    handleKeyUp = (event) => {
        var key = event.keyCode 

        if (key == 40 || key == 38) {
            // On UP and DOWN keys, defocus old selection, update focusTracker, and focus new selection.
            this.defocusPreviousItem();
            var optionsLength = this.listItems.length;
            if (key == 40) {            
                if (this.focusTracker < optionsLength - 1) {
                    this.focusTracker++;
                }
            }
            else {
            	// focusTracker does not decrement below zero. -1 (default) implies arrow keys not used yet.
            	if (this.focusTracker > 0) {
            		this.focusTracker--;
            	}
            }
            // Apply styles to hightlight the LI
            this.focusPreviousItem();
        }
        else if (key == 13) {
           this.updateValueFromKeyboard();
        }
        else if (!this.textValue && ( key == 46 || key == 8)) {
			this.hideList();
        }   
        else if(key == 27){
            this.hideList();
        }
    }
    capitalizeTerm(string) {
        return string.replace(/\w\S*/g, (txt) => { 
        return txt.charAt(0).toUpperCase() + txt.substr(1); 
        });
    }
    updateValueFromKeyboard() {
        if(this.focusTracker >= 0){
            let proxy = JSON.parse(JSON.stringify(this.listItems))          
            var focusedItem = proxy[this.focusTracker];
            this.searchString = focusedItem.Text_Value__c;
            this.textValue = this.capitalizeTerm(focusedItem.Text_Value__c);
            this.recordId = focusedItem.Id;
            this.hideList();
            this.dispatchEvent(new CustomEvent('searchstringchange', {detail: {"searchString": this.capitalizeTerm(this.textValue)}})); 
        } else {
            if(this.textValue.length >= 3){
                this.searchString = this.textValue
            }
            this.showList();
        }
    }

    handleListSelect(e) {
        var selectedItem = e.currentTarget.dataset.value
        this.listItems = [];        
        this.textValue = this.capitalizeTerm(selectedItem);
        this.searchString = selectedItem
        this.dispatchEvent(new CustomEvent('searchstringchange', {detail: {"searchString": this.textValue}}));
        this.hideList();       
    }

    handleChange(e) {
        window.clearTimeout(this.delayTimeout);
        const searchKey = e.target.value;
        this.delayTimeout = setTimeout(() => {
            this.textValue = this.capitalizeTerm(searchKey);
            if(searchKey !== undefined && searchKey.length >= 3) {
                this.searchString = searchKey;
                this.showList();
            }
            else{
                this.searchString = ''
                this.hideList();
            }
            this.dispatchEvent(new CustomEvent('searchstringchange', {detail: {"searchString": this.textValue}}));
        }, DELAY);
    }
    
}