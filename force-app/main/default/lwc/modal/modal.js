import { LightningElement, api, track } from 'lwc';

const CSS_CLASS = 'modal-hidden';

export default class Modal extends LightningElement {
    @api cssModal;
	@api modalResult;
    @api
    set header(value) {
        this.hasHeaderString = value !== '';
        this._headerPrivate = value;
    }
    get header() {
        return this._headerPrivate;
    }

    @track hasHeaderString = false;
    _headerPrivate;

    @api show() {
		let content = this.querySelectorAll('*');
        content.forEach(element => {
            if (element.initModal) {
                element.initModal();
            }
        })
        const outerDivEl = this.template.querySelector('div');
        outerDivEl.classList.remove(CSS_CLASS);
    }

    @api hide() {
        let content = this.querySelectorAll('*');
        content.forEach(element => {
            if (element.clearData) {
                element.clearData();
            }
        })

        const outerDivEl = this.template.querySelector('div');
        outerDivEl.classList.add(CSS_CLASS);        
    }

    handleDialogClose() {
		let content = this.querySelectorAll('*');
        content.forEach(element => {
            if (element.handleCancelModal) {
                element.handleCancelModal();
            }
        })
        this.hide();
    }
    handleSlotFooterChange() {
        const footerEl = this.template.querySelector('footer');
        footerEl.classList.remove(CSS_CLASS);
    }

	@api handleCancelButton() {
		let cancelMethod =  this.querySelectorAll('*');
		cancelMethod.forEach(element => {
			if (element.handleCancelModal) {
				 console.log(element);
                element.handleCancelModal();
            }
            
        });
	}

	@api handleSaveButton() {
		let cancelMethod =  this.querySelectorAll('*');
		cancelMethod.forEach(element => {
			if (element.handleSave) {
				 console.log(element);
                element.handleSave();
            }
           
        });
	}
}