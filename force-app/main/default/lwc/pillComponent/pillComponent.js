import { LightningElement, api } from 'lwc';

export default class PillComponent extends LightningElement {
    
    @api pillLabel;
    @api pillId;
    @api pillCss;
    @api showClose;
    @api pillIndex;
    
    handlePillClose(event){
        console.log('handlePillClose');
        const pillitemcloseevent = new CustomEvent('pillitemcloseevent', {                    
            detail : {"pillId":this.pillId,"pillLabel":this.pillLabel,"pillIndex":this.pillIndex}
        });
        this.dispatchEvent(pillitemcloseevent);
    }

    get showCloseButton(){
        return (this.showClose != undefined && this.showClose == 'true');
    }

}