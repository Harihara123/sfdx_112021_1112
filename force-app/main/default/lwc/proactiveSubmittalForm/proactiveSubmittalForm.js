import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import submittalInfo from '@salesforce/apex/ATSProactiveSubmittal.getSubmittalFormInfo';

import getPASubmittalRecordDetails from '@salesforce/apex/ATSProactiveSubmittal.getPASubmittalRecordDetails';
import saveProactiveSubmittal from '@salesforce/apex/ATSProactiveSubmittal.saveProactiveSubmittal';
/* eslint-disable no-console */
/* eslint-disable no-alert */
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ORDER_OBJECT from '@salesforce/schema/Order';
import SUBMITTALTYPE_FIELD from '@salesforce/schema/Order.Type';
import RATE_TYPE_FIELD from '@salesforce/schema/Order.Rate_Frequency__c';
import SUBM_CURRENCY_FIELD from '@salesforce/schema/Order.Proactive_Submittal_Currency__c';
import { featureItem, anTargetObject, anTableColumns, anSearchInFields, anReturningFields, anCondition, 
		 hmTargetObject, hmTableColumns, hmSearchInFields, hmReturningFields, hmCondition} from 'c/proactiveSubmittalFormHelper';

import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import ATS_SKILLS from '@salesforce/label/c.ATS_SKILLS';
import ATS_ACCOUNT_NAME from '@salesforce/label/c.ATS_ACCOUNT_NAME';
import ATS_HIRING from '@salesforce/label/c.ATS_HIRING';
import ATS_SUBMISSION_TYPE from '@salesforce/label/c.ATS_SUBMISSION_TYPE';
import ATS_TALENT_QUALIFICATIONS from '@salesforce/label/c.ATS_TALENT_QUALIFICATIONS';
import ATS_DESC_ROLE from '@salesforce/label/c.ATS_DESC_ROLE';
import ATS_PAYRATE from '@salesforce/label/c.ATS_PAYRATE';
import ATS_DESIRED_PAY from '@salesforce/label/c.ATS_DESIRED_PAY';
import ATS_RATE_TYPE from '@salesforce/label/c.ATS_RATE_TYPE';
import ATS_Currency from '@salesforce/label/c.ATS_Currency';
import ATS_SEARCH_JOB_TITLE from '@salesforce/label/c.ATS_SEARCH_JOB_TITLE';
import ATS_SEARCH_USERS from '@salesforce/label/c.ATS_SEARCH_USERS';
import ATS_SEARCH_SKILLS from '@salesforce/label/c.ATS_SEARCH_SKILLS';
import ATS_SEARCH_ACCOUNTS from '@salesforce/label/c.ATS_SEARCH_ACCOUNTS';
import ATS_SEARCH_HIRING_MANAGER from '@salesforce/label/c.ATS_SEARCH_HIRING_MANAGER';
import ATS_ENTER_BRF_DESC from '@salesforce/label/c.ATS_ENTER_BRF_DESC';
import ATS_ENTER_PAY from '@salesforce/label/c.ATS_ENTER_PAY';
import ATS_DESIRED_SAL_ENTER from '@salesforce/label/c.ATS_DESIRED_SAL_ENTER';
import ATS_SELECT_RATE_TYPE from '@salesforce/label/c.ATS_SELECT_RATE_TYPE';
import ATS_SELECT_CURRENCY from '@salesforce/label/c.ATS_SELECT_CURRENCY';
import ATS_CREATE_PRO_SUBMITTAL from '@salesforce/label/c.ATS_CREATE_PRO_SUBMITTAL';
import ATS_EDIT_PROACTIVE_SUBMITTAL from '@salesforce/label/c.ATS_EDIT_PROACTIVE_SUBMITTAL';
import ATS_CANCEL from '@salesforce/label/c.ATS_CANCEL';
import ATS_SAVE from '@salesforce/label/c.ATS_SAVE';
import ATS_DETAILS from '@salesforce/label/c.ATS_DETAILS';
import ATS_POC_AM from '@salesforce/label/c.ATS_POC_AM';

//import isGroupMember from '@salesforce/apex/NewOppList_Controller.isGroupMember';     //S-223083


const DELAY = 300;

export default class ProactiveSubmittalForm extends NavigationMixin(LightningElement) {
    label = {
        ATS_JOB_TITLE,
        ATS_SKILLS,
        ATS_ACCOUNT_NAME,
        ATS_HIRING,
        ATS_SUBMISSION_TYPE,
        ATS_TALENT_QUALIFICATIONS,
        ATS_DESC_ROLE,
        ATS_PAYRATE,
        ATS_DESIRED_PAY,
        ATS_RATE_TYPE,
        ATS_Currency,
        ATS_SEARCH_JOB_TITLE,
        ATS_SEARCH_USERS,
        ATS_SEARCH_SKILLS,
        ATS_SEARCH_ACCOUNTS,
        ATS_SEARCH_HIRING_MANAGER,
        ATS_ENTER_BRF_DESC,
        ATS_ENTER_PAY,
        ATS_DESIRED_SAL_ENTER,
        ATS_SELECT_RATE_TYPE,
        ATS_SELECT_CURRENCY,
        ATS_CREATE_PRO_SUBMITTAL,
        ATS_EDIT_PROACTIVE_SUBMITTAL,
        ATS_CANCEL,
        ATS_SAVE,
        ATS_DETAILS,
        ATS_POC_AM
    };
    @track recordTypeId;
    @track pcacMgrKey = '';
    @api conId;
    @api talentId;
    @track jobTitle;
    @api jobTitles;
    @track skills;
    @api accName;
    @api accountId;
    @api accId;
    @api hiringMgr;;
    @api hiringMgrId;
    @api caretClick;
    @api iconClass;
    @api iconClassComp;
    @api cssDisplay;
    @api cssDisplayComp;
    @api submittalType;
    @api caretClickComp;
    @track talentQualification;
    @track descRole;
    @track submittalType;
    @api runningUser;
    @track submitData = {};

    @track payRate;
    @track desireSalary;
    @track rateType;
    @track submittalCurrency;

    //Lookup Modal
	@track modalFeature
    @track modalSearchKey;
    @track modalTrgObject;
    @track modalTblColumns;
    @track modalCondition;
    @track moadalHeader;
	@track modalSchInFlds;
	@track modalRtnFlds;
	@track modalSortCond;

    //Account Name lookup modal
    @track targetAccObject = 'Account';
    @track accountNameSearchKey;
    @track accTableColumns;
    @track accAddlFields;
    @track accCondition;
    @track accModalHeader;
    @track modelOpen = true;

    //Hiring Manager Lookup Modal
    @track targetHMObject = 'Contact';
    @track hmNameSearchKey;
    @track hmTableColumns;
    @track hmAddlFields;
    @track hmModalHeader;
	hmModalCondition;
    @track loading = false;

    @track options = [];

    // skills list for the child components.
    @track skillsObjects = [];
    @track searchText = '';

    @track amAddlFields;

    @api recordType = 'Contact'; // Which indicates the record id type. Job Title, skills etc are retrieved based on this value. by defualt it is Contact
    orderId;
    redirectRecordId; // It could be a contact or an opp record id to which user is redirected after the operations on the form
	isOpportunityChecked = false;
    constructor() {
        super();
        this.loadHMLookupModalDetail();
        this.loadAMAdditionalFields();
    }

    connectedCallback() {
        if (this.recordType === 'Order') {
            getPASubmittalRecordDetails({ orderId: this.conId }).then(res => {
                this.orderId = this.conId;
                this.redirectRecordId = res.shipToContactId;
                this.jobTitle = res.jobTitle;
                this.accountNameSearchKey = res.accountName;
                this.pcacMgrKey = res.accountManagerName;
                this.hmNameSearchKey = res.hiringManagerName;
                this.talentQualification = res.talentQualification;
                this.descRole = res.description;
                this.submittalType = res.submissionType;

                this.payRate = res.payRate;
                this.desireSalary = res.salary;
                this.rateType = res.rateFreq;
                this.submittalCurrency = res.submittalCurrency;

                this.submitData.jobTitle = res.jobTitle;
                this.submitData.accName = res.AccountName;
                this.submitData.accountId = res.accountId;
                this.submitData.orderId = res.orderId;

                this.submitData.pcacMgrKey = res.accountManagerName;
                this.submitData.hiringMgr = res.hiringManagerName;
                this.submitData.hiringMgrId = res.hiringManagerId;
                this.submitData.accountMgrID = res.accountManagerId;
                
                this.submitData.talenQualification = res.talentQualification;
                this.submitData.descRole = res.description;
                this.submitData.submittalType = res.submissionType;

                this.submitData.payRate = res.payRate;
                this.submitData.desireSalary = res.salary;
                this.submitData.rateType = res.rateFreq;
                this.submitData.submittalCurrency = res.submittalCurrency;

                this.submitData.skills = res.skills;
				this.submitData.psStatus = res.psStatus;				

                this.skills = res.skills;
                this.loadSkillsObjects();
            }).catch(err => console.log(err));
        } else {
            submittalInfo({ contactId: this.conId }).then(res => {
                this.redirectRecordId = this.conId;
                this.jobTitle = res.jobTitle;
                this.submitData.jobTitle = res.jobTitle;
                this.submitData.skills = res.skills;

                this.skills = res.skills;
                this.loadSkillsObjects();
            }).catch(err => console.log(err));
        }
    }

    loadSkillsObjects() {
        if (this.skills != undefined) {
            var skillJSON = JSON.parse(this.skills);
            var arrSkills = skillJSON.skills;
            if (arrSkills != undefined) {
                var newSkillsObj = [];
                arrSkills.forEach(function(skill, index) {
                    var itemObj = {};
                    itemObj.pillId = '' + index;
                    itemObj.pillIndex = index;
                    itemObj.pillLabel = skill;
                    itemObj.showClose = 'true';
                    newSkillsObj.push(itemObj);
                });
                this.skillsObjects = newSkillsObj;
            }
        }
    }
    loadAccountLookupModalDetail() {
        this.accModalHeader = 'Account Results';
        this.accTableColumns = [
            { label: 'Name', fieldName: 'Name' },
            { label: 'Street', fieldName: 'BillingStreet' },
            { label: 'City', fieldName: 'BillingCity' },
            { label: 'State', fieldName: 'BillingState' },
            { label: 'Status', fieldName: 'Customer_Status__c' },
            { label: 'REQS', fieldName: 'No_of_Reqs__c', type: 'number' },
            { label: 'Last Activity Date', fieldName: 'Last_Activity_Date__c' },
            { label: 'Account ID', fieldName: 'Siebel_ID__c' },
            { label: 'Global Account Id', fieldName: 'Master_Global_Account_Id__c' }
        ];

        this.accCondition = [{ "fieldName": "recordType.name", "fieldValue": "Client" }];

        let addlFields;
        this.accTableColumns.forEach(function(queryField) {
            if (addlFields === '')
                addlFields += "\"" + queryField['fieldName'] + "\"";
            else
                addlFields += ", \"" + queryField['fieldName'] + "\"";
        });

        this.accAddlFields = "[" + addlFields + "]";
    }

    //Account Manager Additional Fields
    loadAMAdditionalFields() {
        this.amAddlFields = JSON.stringify([
            "Title", "Phone"
        ]);
        this.amDisplayFields = [
            { label: 'Title', fieldApiName: 'Title', dataType: 'text' },
            { label: 'Phone', fieldApiName: 'Phone', dataType: 'phone' }
        ];
    }

    //Hiring Manager Modal Details
    loadHMLookupModalDetail() {      
        let addlFields = '';
        let hmReturningFieldsArr = hmReturningFields.split(",");
        hmReturningFieldsArr.forEach(function(field) {
            if (field !== 'Name') {
                if (addlFields === '')
                    addlFields += "\"" + field.trim() + "\"";
                else
                    addlFields += ",\"" + field.trim() + "\"";
            } 
            
        });
        this.hmAddlFields = "[" + addlFields + "]";
        
    }

    handleCaretClickComp() {
        this.caretClickComp = !this.caretClickComp;

    }
    handleCaretClick() {
        this.caretClick = !this.caretClick;
    }
    get iconchevDownComp() {
        if (!this.caretClickComp) {
            this.iconClassComp = this.caretClickComp;
        }
        if (this.caretClickComp) {
            this.iconClassComp = this.caretClickComp;
        }
        return this.iconClassComp;
    }
    get iconchevDown() {
        if (!this.caretClick) {
            this.iconClass = this.caretClick;
        }
        if (this.caretClick) {
            this.iconClass = this.caretClick;
        }
        return this.iconClass;
    }
    get sectionDisplayComp() {
        if (this.caretClickComp) {
            this.cssDisplayComp = "d-block-none"
        }
        if (!this.caretClickComp) {
            this.cssDisplayComp = "d-block"
        }
        return this.cssDisplayComp;
    }


    get sectionDisplay() {
        if (this.caretClick) {
            this.cssDisplay = "d-block-none"
        }
        if (!this.caretClick) {
            this.cssDisplay = "d-block"
        }

        return this.cssDisplay;
    }

    @wire(getObjectInfo, { objectApiName: ORDER_OBJECT })
    getobjectInfo(result) {
        if (result.data) {
            const rtis = result.data.recordTypeInfos;
            this.recordTypeId = Object.keys(rtis).find((rti) => rtis[rti].name === 'Proactive');
        }
    }

    @wire(getPicklistValues, {
        recordTypeId: '$recordTypeId',
        fieldApiName: SUBMITTALTYPE_FIELD
    })
    submittalTypeOption({error, data}){
        if(data){
            this.submittalType = data.defaultValue.value;
            this.submitData.submittalType = this.submittalType;
            this.options = data.values;
        } else if (error) {
            console.log(error);
        }
    }

    get options() {
        return this.options;
    }

    @wire(getPicklistValues, {
        recordTypeId: '$recordTypeId',
        fieldApiName: RATE_TYPE_FIELD
    })
    rateTypeOption;

    @wire(getPicklistValues, {
        recordTypeId: '$recordTypeId',
        fieldApiName: SUBM_CURRENCY_FIELD
    })
    submCurrencyOption;

    get checkEdit() {
        return (this.recordType != undefined && this.recordType === 'Order');
    }


    handleJobTitle(event) {
        window.clearTimeout(this.delayTimeout);
        this.submitData.jobTitle = event.target.value;
        const jbTitle = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.jobTitle = jbTitle;
        }, DELAY);
    }

    handleAccountMgrChange(event) {
        window.clearTimeout(this.delayTimeout);
        const pcacMgrKey = event.target.value;
        this.submitData.accountMgrID = event.target.value;

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.pcacMgrKey = pcacMgrKey;
        }, DELAY);
    }


    handleSkill(event) {
        window.clearTimeout(this.delayTimeout);
        this.submitData.skills = event.target.value;
    }

    handleAccountName(event) {
        this.submitData.accountId = event.target.value;
    }

    handleHiringManager(event) {
        this.submitData.hiringMgrId = event.target.value;
    }
    handleSubmittalTypeChange(event) {
        this.submitData.submittalType = event.target.value;
    }

    handleQualificationChange(event) {
        this.submitData.talentQualification = event.target.value;
    }

    handleDescRoleChange(event) {
        this.submitData.descRole = event.target.value;
    }

    handlePayRateChange(event) {
        this.submitData.payRate = event.target.value;
    }

    handleRateTypeChange(event) {
        this.submitData.rateType = event.target.value;
    }

    handleDesireSalaryChange(event) {
        this.submitData.desireSalary = event.target.value;
    }

    handleSubmCurrencyChange(event) {
        this.submitData.submittalCurrency = event.target.value;
    }

    handleCancel(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.redirectRecordId,
                objectApiName: 'Contact',
                actionName: 'view'
            }
        });
    }
    get jobTitleWhereCondition() {
        var whereCond = '';
        if (this.runningUser != undefined && this.runningUser.OPCO__c != undefined) {
            whereCond = 'Text_Value_2__c = \'' + this.runningUser.OPCO__c + '\'';
        }
        return whereCond;
    }

    saveSubmittal(event) {
        
        if ( (this.submitData.accountId != undefined && this.submitData.accountId != '' && this.submitData.accountId.length != 0) &&
			(this.submitData.accountMgrID != undefined && this.submitData.accountMgrID != '' && this.submitData.accountMgrID.length != 0) &&
			(this.submitData.hiringMgrId != undefined && this.submitData.hiringMgrId != '' && this.submitData.hiringMgrId.length != 0)
			) {
            this.submitData.contactId = this.conId;
            if (this.isFormValid()) {
                this.loading = true;
                saveProactiveSubmittal({ psModal: this.submitData, orderId: this.orderId })
                    .then(res => {
						this.loading = false;
                        this.showToast('', (this.orderId != undefined ? 'Proactive submittal updated successfully.' : 'Proactive submittal created successfully.'), 'success');
						if(this.isOpportunityChecked) {
							this.redirectToCreateOpportunity();
						}
						else {
							this.redirectToTLP();
						}
                        
                    })
                    .catch(error => {
                        this.loading = false;
                        this.showToast('', 'Some error occurred, please contact to System Administrator.', 'error');
                    });
            } else {
                this.showToast('', 'Please update the invalid form entries and try again.', 'error');
            }
        } else {
            if (this.submitData.accountId === '' || this.submitData.accountId === undefined || this.submitData.accountId.length == 0) {
                this.showToast('', 'Please select Account Name.', 'error');
            }
            if (this.submitData.hiringMgrId === '' || this.submitData.hiringMgrId === undefined || this.submitData.hiringMgrId.length == 0) {
                this.showToast('', 'Please select Hiring Manager.', 'error');
            }
            if (this.submitData.accountMgrID === '' || this.submitData.accountMgrID === undefined || this.submitData.accountMgrID.length == 0) {
                this.showToast('', 'Please select Account Manager.', 'error');
            }		
        
        }
    
    }

    isFormValid() {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        return allValid;
    }

    handleAccountLookupModal() {
        this.modelOpen = true;
		this.modalFeature = featureItem;
        this.modalSearchKey = this.accountNameSearchKey;
        this.modalTrgObject = anTargetObject;
        this.modalTblColumns = anTableColumns;
        this.modalCondition = anCondition;
        this.moadalHeader = 'Account Results';
		this.modalSchInFlds = anSearchInFields; 
		this.modalRtnFlds = anReturningFields;
        const modal = this.template.querySelector('c-modal');
        modal.show();
    }

    handleHMLookupModal() {
		this.modelOpen = true;
		this.modalFeature = featureItem;
		this.modalSearchKey = this.hmNameSearchKey;
        this.modalTrgObject = hmTargetObject;
        this.modalTblColumns = hmTableColumns;
        this.modalCondition = hmCondition;
        this.moadalHeader = 'Hiring Manager Results';
		this.modalSchInFlds = hmSearchInFields; 
		this.modalRtnFlds = hmReturningFields;

        const modal = this.template.querySelector('c-modal');
        modal.show();
    }

	handleCancelButton() {
		const modal = this.template.querySelector('c-modal');
        modal.handleCancelButton();
	}
	handleSaveButton() {
		const modal = this.template.querySelector('c-modal');
        modal.handleSaveButton();
	}

    handleCancelModal() {
		this.accountNameSearchKey = '';
		this.submitData.accountId = '';

		this.hmNameSearchKey = '';
		this.submitData.hiringMgrId = '';
        const modal = this.template.querySelector('c-modal');
        modal.hide();
    }

    handleCloseModal() {
        const modal = this.template.querySelector('c-modal');
        modal.hide();
    }

    showToast(title, message, variant) {
            const evt = new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
        // JobTitle look up component event handler.
    handleJobTitleSelectedEvent(event) {
        if (event.detail != undefined) {
            var lookObj = event.detail;
            this.jobTitle = lookObj.lookupValue;
            this.submitData.jobTitle = lookObj.lookupValue;
        }
    }

    handleAMSelectedEvent(event) {
        if (event.detail !== undefined) {
            let recId;
            let lookObj = event.detail;
            recId = '' + lookObj.lookupid;
            this.pcacMgrKey = lookObj.lookupValue;
            this.submitData.accountMgrID = recId.slice(1, -1);
        }
    }

    handleAccountSelectedEvent(event) {
		
        if (event.detail !== undefined) {
            let recId;
            let lookObj = event.detail;
            if (lookObj.source !== undefined && lookObj.source !== null && lookObj.source === 'searchicon') {
				this.accountNameSearchKey = lookObj.lookupValue;
                this.handleAccountLookupModal();
            } else {
				
					if(lookObj.lookupid != '' && lookObj.lookupValue != '') {
							recId = '' + lookObj.lookupid;
							this.accountNameSearchKey = lookObj.lookupValue;
							this.submitData.accountId = recId.slice(1, -1);
					} else {
						if(this.hmNameSearchKey != '' && this.submitData.hiringMgrId != '') {
							this.hmNameSearchKey = '';
						this.submitData.hiringMgrId = '';

					} 
					recId = '' + lookObj.lookupid;
					this.accountNameSearchKey = lookObj.lookupValue;
					this.submitData.accountId = recId.slice(1, -1);
					
				}
            }			
        }
    }

    handleHMSelectedEvent(event) {
        if (event.detail !== undefined) {
            let recId;
            let lookObj = event.detail;
            this.hmNameSearchKey = lookObj.lookupValue;
            if (lookObj.source !== undefined && lookObj.source !== null && lookObj.source === 'searchicon') {
                this.handleHMLookupModal();
            } else {
                recId = '' + lookObj.lookupid;
                this.submitData.hiringMgrId = recId.slice(1, -1);
                // This is for Account Name auto populate after selection of Hiring Manager
                if (lookObj.lookupRecord !== undefined) {
					if(lookObj.lookupRecord.AddlFields.length > 0) {
						this.submitData.accountId = lookObj.lookupRecord.AddlFields[0];
						this.accountNameSearchKey = lookObj.lookupRecord.AddlFields[9];
					}
                }
            }

        }
    }

    renderSearchField(event) {
        if (event.detail !== undefined) {
            let eventParams = event.detail;
            if (eventParams.objectName === 'Account') {
                this.accountNameSearchKey = eventParams.selectedrow.Name;
                this.submitData.accountId = eventParams.selectedrow.Id;
				this.hmNameSearchKey = '';
				this.submitData.hiringMgrId = '';
            } else if (eventParams.objectName === 'Contact') {
                this.hmNameSearchKey = eventParams.selectedrow.Name;
                this.submitData.hiringMgrId = eventParams.selectedrow.Id;
                this.accountNameSearchKey = eventParams.selectedrow.Related_Account_Name__c;
                this.submitData.accountId = eventParams.selectedrow.AccountId.replace('/', '');
            }

        }
        this.handleCloseModal(); 
    }

    handlelookupitemselectedevent(event) {
        let lookObj = event.detail;
        // Handle new skills and update both in the skills string and this.submitData.skills
        if (lookObj != undefined) {
            var skillsArr = [];
            var pillsItems = lookObj.pillitems;
            pillsItems.forEach(function(pillItm, index) {
                skillsArr.push(pillItm.pillLabel);
            });
            var jsObj = { "skills": skillsArr };
            this.skills = JSON.stringify(jsObj);
            this.submitData.skills = JSON.stringify(jsObj);
        }
        this.searchText = '';

    }

    get isAccountReadOnly() {
        return (this.recordType != undefined && this.recordType === 'Order');
    }
    get isHiringManagerReadOnly() {
        return ((this.recordType != undefined && this.recordType === 'Order') &&
            (this.hmNameSearchKey != undefined && this.hmNameSearchKey.length > 0));
    }

	redirectToTLP() {
		this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.redirectRecordId,
                objectApiName: 'Contact',
                actionName: 'view'
            }
        });
	}

	redirectToCreateOpportunity () {
		//Adding w.r.t Story S-177138
        //S-223083 starts
		// var groupMember=false;
		// isGroupMember().then(res => {
        //         this.groupMember = res;
		// 		var ReqComponent;		
		// 		if (this.runningUser.CompanyName == "AG_EMEA") {
		// 			this.ReqComponent = 'c__CreateEnterpriseEmeaReq';
		// 		}else if (this.runningUser.CompanyName == "TEKsystems, Inc." && this.groupMember==true) {
		// 			this.ReqComponent = 'c__CreateEnterpriseTekReq';
		// 		}else if (this.runningUser.CompanyName == "Aerotek, Inc" && this.groupMember==true) {
		// 			this.ReqComponent = 'c__CreateEnterpriseAerotekReq_New';
		// 		}else{
		// 			this.ReqComponent = 'c__CreateEnterpriseReq';
		// 		}	

		// 		this[NavigationMixin.Navigate]({
		// 			type: 'standard__component',
		// 			attributes: {
		// 				"componentName": this.ReqComponent
		// 		},
		// 		state: {
		// 			"c__recId" : this.orderId,
		// 			"c__proactiveSubmittalStatus" : this.submitData.psStatus,
		// 			"c__source" : "skuid"
		// 		}
		// 		});
		// 	}
		// );
        //S-223083 ends
        var ReqComponent;		
        if (this.runningUser.CompanyName == "AG_EMEA") {
            this.ReqComponent = 'c__CreateEnterpriseEmeaReq';
        }else if (this.runningUser.CompanyName == "TEKsystems, Inc.") {
            this.ReqComponent = 'c__CreateEnterpriseTekReq';
        }else if (this.runningUser.CompanyName == "Aerotek, Inc") {
            this.ReqComponent = 'c__CreateEnterpriseAerotekReq_New';
        }else{
            this.ReqComponent = 'c__CreateEnterpriseReq';
        }	

        this[NavigationMixin.Navigate]({
            type: 'standard__component',
            attributes: {
                "componentName": this.ReqComponent
        },
        state: {
            "c__recId" : this.orderId,
            "c__proactiveSubmittalStatus" : this.submitData.psStatus,
            "c__source" : "skuid"
        }
        });	
	}

	createOpportunityChecked() {
		if(this.recordType != undefined && this.recordType === 'Order' && this.submitData.psStatus != undefined) {
			this.isOpportunityChecked = this.template.querySelector('[data-id=createopp]').checked
		}
	}

	get displayCreateOppoSection() {
		
		let isStatusNotProceeding = false;
		if(this.submitData.psStatus != undefined) {
			let status = this.submitData.psStatus.toUpperCase();
			if(status.match('NOT PROCEEDING')) {
				isStatusNotProceeding = true;
			}
		}
		
		return (this.recordType != undefined && this.recordType === 'Order' && !isStatusNotProceeding) ;
	}
}