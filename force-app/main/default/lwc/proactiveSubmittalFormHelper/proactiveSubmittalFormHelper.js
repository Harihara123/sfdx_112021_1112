//import {LightningElement} from 'lwc';

//export default class ProactiveSubmittalFormHelper extends LightningElement {}
//Account Name modal declaration
const featureItem = 'ProactiveSubmittal'
const anTargetObject = 'Account';
const anSearchInFields = 'Name';
const anReturningFields = 'Name, BillingStreet, BillingCity, BillingState, Customer_Status__c, No_of_Reqs__c, Last_Activity_Date__c, Siebel_ID__c ';
const anCondition = [{ "fieldName": "recordType.name", "fieldValue": "Client" }];
//const anModalHeader = 'Account Results';
const anTableColumns = [
            { label: 'Name', fieldName: 'Name' , initialWidth : 210},
            { label: 'Street', fieldName: 'BillingStreet', initialWidth : 200},
            { label: 'City', fieldName: 'BillingCity',initialWidth : 110  },
            { label: 'State', fieldName: 'BillingState',initialWidth : 80 },
            { label: 'Status', fieldName: 'Customer_Status__c',initialWidth : 120  },
            { label: 'REQS', fieldName: 'No_of_Reqs__c', type: 'number',initialWidth : 70 },
            { label: 'Last Activity Date', fieldName: 'Last_Activity_Date__c' ,initialWidth : 100},
            { label: 'Account ID', fieldName: 'Siebel_ID__c',initialWidth :100 }
        ];

//Hiring Manager Modal declaration
const hmTargetObject = 'Contact';
const hmSearchInFields = 'Name';
const hmReturningFields = 'Name, AccountId, MailingStreet, MailingCity, MailingState, Title, Email, Phone, Customer_Status__c, Last_Activity_Date__c, Related_Account_Name__c';//, Account.Name';
const hmCondition = [{ "fieldName": "recordType.name", "fieldValue": "Client" }];
const hmTableColumns = [
             { label: 'Name', fieldName: 'Name', initialWidth : 180},
            { label: 'Account', fieldName: 'AccountId', initialWidth : 170, type: 'url', typeAttributes: { label: { fieldName: 'Related_Account_Name__c' }, target: '_blank' } },
            { label: 'Street', fieldName: 'MailingStreet' , initialWidth : 100},
            { label: 'City', fieldName: 'MailingCity',  initialWidth : 80},
            { label: 'State', fieldName: 'MailingState', initialWidth : 50 },
            { label: 'Title', fieldName: 'Title', initialWidth : 100 },
            { label: 'Email', fieldName: 'Email',initialWidth : 160 },
            { label: 'Phone Number', fieldName: 'Phone' , initialWidth : 100},
            { label: 'Status', fieldName: 'Customer_Status__c', initialWidth :110},
            { label: 'Last Activity', fieldName: 'Last_Activity_Date__c', initialWidth :90}
        ];

export {anTargetObject, anTableColumns, anSearchInFields, anReturningFields, anCondition, 
		hmTargetObject, hmTableColumns, hmSearchInFields, hmReturningFields, hmCondition};