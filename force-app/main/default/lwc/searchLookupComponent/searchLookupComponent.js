import { LightningElement, api, track } from 'lwc';
import performServerCall from '@salesforce/apex/BaseController.performServerCall';
import ATS_SEARCH from '@salesforce/label/c.ATS_SEARCH';

export default class SearchLookupComponent extends LightningElement {
    
    label = {
        ATS_SEARCH
    };
    @api objectName;
    @api recordTypeName;
    @api targetFields; // Fields to search 
    @api placeHolder;
    @api isSOSL;
    @api isMultiSearch; // Allows user to search for more than one item
    @api isSynchronousSearch;
    @api sortCondition;
    @api useCache;
    @api whereCondition; 
    @api clasName;
    @api methodName;
    @api minCharacters;
    @api runningUser;
    @api addlFields;
    @api relatedTo;
    @api classRelatedSkill;
    @api firstRecordSearchIcon=false;
    @api labelText = '';
    @api required;
    requsetParameters;

    @api searchString;
    @track searchResults;
    @track focusTracker = -1;    

    @track tmpSearchResults;
    @api displayFields;
    @api isCustomCss;
    @api isReadOnly = false;
	@api searchLookupText;
    @api recordLimit;
    
    @api variant = 'standard';

    @api freeText = false;
    freeTextString = '';

    connectedCallback(){
                                
        this.loadRequestParameters();
              
    }
    loadRequestParameters(){
        //this.searchString = '';
        this.minCharacters = 3;
        this.useCache = false;
        
        
        this.targetFields = '';
        var emtValue = null;
        this.requsetParameters = {};
        this.requsetParameters.addlReturnFields = ((this.addlFields != undefined ||this.addlFields != '')? this.addlFields:'[]');
        this.requsetParameters.recTypeName = this.recordTypeName;
        this.requsetParameters.nameFieldOverride = emtValue;
        this.requsetParameters.idFieldOverride = emtValue;
        
        this.requsetParameters.useCache = this.useCache;
        this.requsetParameters.whereCondition = this.whereCondition;
        this.requsetParameters.sortCondition = this.sortCondition;  
        this.requsetParameters.type = this.objectName;
		this.requsetParameters.recordLimit = ((this.recordLimit != undefined ||this.recordLimit != '')? this.recordLimit:'20');
        
        this.displayFields = this.addlFields != undefined ? this.displayFields:'[]';
        this.isCustomCss = this.isCustomCss !== undefined ? this.isCustomCss:false;
    }

    handleSearch(){		
        const textInput = this.template.querySelector('[data-id=userinput]');
        this.focusTracker = -1;
        if(textInput!=undefined){            
            this.requsetParameters['searchString'] = textInput.value;               
            performServerCall({className : this.clasName, subClassName : '', methodName : this.methodName, parameters : this.requsetParameters}).then((result) =>{
                if(result!=undefined){                 
                    this.handleSearchResponse(JSON.parse(result),this.displayFields,this.isCustomCss);
                }                
            }).catch((error) => {
                console.log('performServerCall 2 '+JSON.stringify(error));
            });
        }
    }
    @api
    focus() {
        const input = this.template.querySelector('lightning-input');
        if (input) {
            input.focus()
        }
    }     
    handleSearchResponse(result,displayFields,isCustomCss){
        var serchResults = [];
        if(this.firstRecordSearchIcon) {
            let dpFieldItems = [];
			let searchItem;
			searchItem = 'Search \"' +this.template.querySelector('[data-id=userinput]').value+'\" in '+this.searchLookupText+'';	
			
            dpFieldItems.push({"name":"_searchIcon_", "value":searchItem,"dataType":"Text","isPhone":false,"isText":true});
            serchResults.push({"recordId" : '_searchIcon_',"DisplayValu":'search',"DisplayFields":dpFieldItems});
        }
        
        result.forEach(function(item,index){   
            var displayFieldItems = [];
            if(isCustomCss) 
                displayFieldItems.push({"name":"recordName", "value":item.value,"dataType":"Text","isPhone":false,"isText":true,"isTitleField":false, "isPhoneField":false,"isNameField":true});
            else
                displayFieldItems.push({"name":"recordName", "value":item.value,"dataType":"Text","isPhone":false,"isText":true});
            if(displayFields != undefined) {

                if(displayFields.length > 0 && item.addlFields.length > 0) {
                    
                    displayFields.forEach(function(displayItem,index) {
                        if(isCustomCss) {
                            if(displayItem.dataType === "phone")
                                displayFieldItems.push({"name":displayItem.name,"value":(item.addlFields[index] != null ?item.addlFields[index]:''),"dataType":displayItem.dataType,"isPhone":true,"isText":false, "isTitleField":false, "isPhoneField":true,"isNameField":false});
                            else
                                displayFieldItems.push({"name":displayItem.name,"value":(item.addlFields[index] != null ?item.addlFields[index]:''),"dataType":displayItem.dataType,"isPhone":false,"isText":true,"isTitleField":true, "isPhoneField":false,"isNameField":false});
                        } else {
                            if(displayItem.dataType === "phone")
                                displayFieldItems.push({"name":displayItem.name,"value":(item.addlFields[index] != null ?item.addlFields[index]:''),"dataType":displayItem.dataType,"isPhone":true,"isText":false});
                            else
                                displayFieldItems.push({"name":displayItem.name,"value":(item.addlFields[index] != null ?item.addlFields[index]:''),"dataType":displayItem.dataType,"isPhone":false,"isText":true});
                        }
                    });
                    

                }

            }
            serchResults.push({"recordId" : (item.id != undefined ? '_'+item.id+'_' : 'slkp_'+index),"DisplayValu":item.value,"DisplayFields":displayFieldItems, "AddlFields":item.addlFields, "isCustomCss":isCustomCss});
        });
        this.searchResults = serchResults;
    }
    handleResultItemClick(event){    
        var selectedItem = this.searchResults[event.currentTarget.dataset.item];
        this.searchResults = [];        
        if(selectedItem.recordId === '_searchIcon_') {
            this.openLookupModal();
        }
        else {
            this.searchString = selectedItem.DisplayValu;
            this.recordId = selectedItem.recordId;
            this.selectedRecord = selectedItem;            
            this.fireValueUpdateEvent();
            //resetting skills textbox
			 if(this.relatedToSkill) {
			    this.focusTracker = -1;
				this.searchString = '';
				this.template.querySelector('[data-id=userinput]').value = '';
			 }            
        }
    }

    handleonfocusout(event){
        var key = 'which' in event ? event.which : event.keyCode;
       // this.searchResults = [];
    }
    handleKeyUp(event){
        var key = 'which' in event ? event.which : event.keyCode;
        
        if (key == 40 || key == 38) {
            // On UP and DOWN keys, defocus old selection, update focusTracker, and focus new selection.
            this.defocusPreviousItem();
            var optionsLength = this.searchResults.length;
            if (key == 40) {            
                if (this.focusTracker < optionsLength - 1) {
                    this.focusTracker++;
                }
            }
            else {
            	// focusTracker does not decrement below zero. -1 (default) implies arrow keys not used yet.
            	if (this.focusTracker > 0) {
            		this.focusTracker--;
            	}
            }
            // Apply styles to hightlight the LI
            this.focusPreviousItem();
        }
        else if (key == 13) {
           this.updateValueHideList();
        }
        else if (!this.freeText && (key == 27 ||  key == 46 || key == 8)) {
			this.clearSearchResult();
        }
        else{
            var inputText = event.target.value;
            if(inputText.length >= this.minCharacters){
                this.handleSearch();
            }
        }       
    }

    defocusPreviousItem(){
        if(this.focusTracker >= 0){            
            var searchResItem = this.searchResults[this.focusTracker];
            const textInput = this.template.querySelector('[data-id='+searchResItem.recordId+']');
            textInput.className = 'deFocusList';            
        }
    }

    focusPreviousItem(){
        if(this.focusTracker >= 0){
            let searchResItem = this.searchResults[this.focusTracker];
            const textInput = this.template.querySelector('[data-id='+searchResItem.recordId+']');
            textInput.className = 'listItemFocus';            
        }
    }
    updateValueHideList(){
        if(this.focusTracker === -1){
            this.openLookupModal();
        }
        else if(this.focusTracker >= 0){
            let searchResItem = this.searchResults[this.focusTracker];
            this.searchResults = [];
            if(searchResItem.recordId === '_searchIcon_') {
                this.openLookupModal();
            }
            else {
                this.searchString = searchResItem.DisplayValu;
                this.recordId = searchResItem.recordId;
				this.selectedRecord = searchResItem;

                this.fireValueUpdateEvent(); 
            }  
        } else {

            const textInput1 = this.template.querySelector('[data-id=userinput]');
            this.searchString = textInput1.value;
            this.recordId = '';
            this.searchResults = [];
            this.fireValueUpdateEvent();             
  
        }
        //resetting skills textbox
        if(this.relatedToSkill) {
			this.searchString = '';
			this.template.querySelector('[data-id=userinput]').value = '';
			this.focusTracker = -1;
		}
    }
    clearSearchResult(){		
		this.searchString = '';
		this.searchResults = [];
		this.selectedRecord = {"recordId":"","DisplayValu":"","AddlFields":[]};
		this.recordId = "";		
		this.focusTracker = -1;
		this.fireValueUpdateEvent(true);
    }

    get showSearchResults(){
        return (this.searchResults != undefined && this.searchResults.length>0);
    }

    get relatedToSkill() {
        return (this.relatedTo === 'skill');
    }
    get relatedToUser() {
        return (this.relatedTo === 'User');
    }
    get classRelatedSkill() {
        return (this.relatedTo === 'skill' ? 'transform: translate(-91px, 37px)' : '');
    }
    get placeHolderText(){
        return (this.placeHolder != undefined ? this.placeHolder : ATS_SEARCH);
    }
    // Fire the event with the selected value
    fireValueUpdateEvent(changeEvent = false, freeText = false){  
        const lookUpItemSelectedEvent = new CustomEvent('lookupitemselectedevent', {                      
            detail : {"lookupid":this.recordId,"lookupValue":this.searchString,"lookupRecord":this.selectedRecord}
        });
        const freeTextSelectedEvent = new CustomEvent('freetextselectedevent', {
            detail: {"freeTextValue": this.freeTextString}
        });
        if (!changeEvent) {
            this.dispatchEvent(new CustomEvent('selected', {detail: {"lookupid":this.recordId,"lookupValue":this.searchString,"lookupRecord":this.selectedRecord}}));
        } 
       
        if (freeText) {
            this.dispatchEvent(freeTextSelectedEvent);
        } else {
            this.dispatchEvent(lookUpItemSelectedEvent);
        }     
        
		this.focusTracker = -1; 
    }
    

    handleResultSetFocus() {
        if(this.tmpSearchResults.length > 0)
            this.searchResults = this.tmpSearchResults;
    }
	
    blurHandler() {
		setTimeout(() => {
           this.searchResults = []
           this.dispatchEvent(new CustomEvent('blur'));           
        }, 250);   

        if (this.freeTextString.length) {
            this.fireValueUpdateEvent(true, true);
        } 

        this.blankSearchString();    
    }

	blankSearchString(){
		const srchText = this.template.querySelector('[data-id=userinput]').value;
		if(srchText.length === 0) {
			this.dispatchEvent(new CustomEvent('searchstringblank'));
		}
	}

    openLookupModal() {
		const textInput = this.template.querySelector('[data-id=userinput]');
		const lookUpItemSelectedEvent = new CustomEvent('lookupitemselectedevent', {                      
            detail : {"lookupid":this.recordId,"lookupValue":textInput.value,"lookupRecord":this.selectedRecord, "source":"searchicon"}
        });
        this.dispatchEvent(lookUpItemSelectedEvent);
	}

	handleOnChange() {
        const inputText = this.template.querySelector('[data-id=userinput]').value;
        if(this.freeText){
            this.freeTextString = inputText;
            this.focusTracker = -1;
            this.fireValueUpdateEvent(true);
        }

		if(inputText.length < 3) {
			this.searchString = '';
			this.searchResults = [];
			this.selectedRecord = {"recordId":"","DisplayValu":"","AddlFields":[]};
			this.recordId = "";		
			this.focusTracker = -1;
			this.fireValueUpdateEvent(true);
		}
    }
    
}