import { LightningElement, api, track } from 'lwc';
import ATS_Duplicate_Value from '@salesforce/label/c.ATS_Duplicate_Value';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
export default class SearchLookupPillContainer extends LightningElement {

    label = {
        ATS_Duplicate_Value,
    }

    @track pillItems = []; // Contains list of pill details.
    //Here is the sample format of the each pill - {"pillId": "","pillLabel": "","pillCSS": "","showClose":""}
    @api allowSearch; // This will decide to show/hide search
    @api maxPillsAllowed;
    @api pillCss; // This is the common style for all the pills.
    @api showClose; // This is the common property for all the pills.
    @track searchText; // Defualt text in the search field
    // In case if each pill has its own style and option for close, then construct the pills outside and pass them to the component.
    // It is an array of strings. Each string will represent a pill.
    @api allowDuplicateSelection = false;
    initialPillsSize;

    @track placeholder = 'Search for Skill';

    // The component should be passed either pillsList or the pillsString.
    @api 
    get pillsList(){
        
    }
    set pillsList(value){
        this.pillItems = value;
        this.initialPills = this.pillItems.length;
    }

    @api 
    get pillString(){
        
    }
    set pillString(value){
        console.log('pillString '+value);
        if(value != undefined){
            var arrStrings = value;
            var allPills = [];
            arrStrings.forEach(function(item,index){
                var itemObj = {};
                itemObj.pillId = 'pill'+index;
                itemObj.pillIndex = index;
                itemObj.pillLabel = item;
                /*
                if(this.pillCss != undefined){
                    itemObj.pillCSS = this.pillCss;
                }*/
                itemObj.showClose = 'true';             
                allPills.push(itemObj);                
            });
            this.pillItems = allPills;
        }        
    }
    get skillsWhereCondition(){
        var whereCond = '';
        /*
        if(this.runningUser != undefined && this.runningUser.OPCO__c != undefined){
            whereCond = 'Text_Value_2__c = \'' +this.runningUser.OPCO__c+'\'';
        } */
        whereCond = 'Text_Value_2__c = \'' +'AP'+'\'';
        return whereCond;
    }
    handleSkillSelectedEvent(event){
        if(event.detail != undefined){
            var lookObj = event.detail;
            var newLookUpItem = lookObj.lookupValue;            
            this.searchText = '';
            if(!this.checkDuplicateSelection(newLookUpItem)){
                this.initialPillsSize = this.initialPillsSize+1;
                if(newLookUpItem != undefined && newLookUpItem.length >0){
                    var itemObj = {};
                    itemObj.pillId = ''+this.pillItems.length;
                    if(this.pillItems.length > 0)
                        itemObj.pillIndex = this.pillItems[this.pillItems.length-1].pillIndex+1;
                    else
                        itemObj.pillIndex = this.pillItems.length;
                    itemObj.pillLabel = newLookUpItem;                
                    itemObj.showClose = 'true';  
                    console.log('this.pillItems:'+JSON.stringify(this.pillItems));
                    this.pillItems = [...this.pillItems,itemObj];
                    
                    console.log('newPillItemsList updated:'+JSON.stringify(this.pillItems));
                    this.raiseSkillsChangeEvent();
                }                
            }else{
                this.searchText = '';
                const toastEvent = new ShowToastEvent({
                    title: ATS_Duplicate_Value,
                    message: newLookUpItem + ' already exists !!',
                });
                this.dispatchEvent(toastEvent);
            }           
        }   
    }
    checkDuplicateSelection(newSelection){
        var isDuplicate = false;
        for(var co=0;co<this.pillItems.length; co++){
            var item=this.pillItems[co];
            var objValue = item['pillLabel'];
            console.log(objValue + ' === '+ newSelection);
            if(objValue === newSelection){
                isDuplicate = true;
                break;
            }
        }        
        return isDuplicate;
    }
    handlepillitemcloseevent(event){
        let lookObj = event.detail;
        let pillIndex = lookObj.pillIndex;
        if(pillIndex<=this.pillItems.length){
            console.log('pillIndex Before splice:'+pillIndex);
            console.log('pillIndex Before splice:'+this.pillItems);
            //var currList = this.pillItems;
            //currList.splice(pillIndex,1);
            this.pillItems = [...this.pillItems.slice(0,pillIndex),...this.pillItems.slice(pillIndex+1,this.pillItems.length)];

            console.log('pillIndex After splice:'+pillIndex);
            console.log('pillIndex After splice:'+this.pillItems);
            this.resetPillIndex();
        }
        this.raiseSkillsChangeEvent();        
    }
    // Raise an event so that the component parents can be notified with the new value lists.
    raiseSkillsChangeEvent(){
        const lookuppillitemselectedevent = new CustomEvent('lookuppillitemselectedevent', {                      
            detail : {"pillitems":this.pillItems}
        });
        this.dispatchEvent(lookuppillitemselectedevent);
    }

    //reset pill Index once skill gets deleted
    resetPillIndex(){
        var currList = this.pillItems;
        this.pillItems = [];
        var allPillItems = [];
        currList.forEach(function(item,index){
            var itemRec = {};
            itemRec.pillId = ''+index;
            itemRec.pillIndex = index;
            itemRec.pillLabel = item.pillLabel;
            itemRec.showClose = item.showClose;            
            allPillItems.push(itemRec);                
        });
        this.pillItems = allPillItems;
    }
    
}