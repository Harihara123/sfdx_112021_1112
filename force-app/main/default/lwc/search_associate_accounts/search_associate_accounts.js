import { LightningElement, wire, api, track } from 'lwc';
import searchClientContacts from '@salesforce/apex/SearchAndAssociateAccountHelper.filterRequiredRecords';
import createStrategicPlanRecord from '@salesforce/apex/SearchAndAssociateAccountHelper.createStrategicRecords';
import NoData from '@salesforce/label/c.No_Data_Search_And_Associates';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Account Name', fieldName: 'linkName', type: 'url', 
        typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
    { label: 'Status Filter', fieldName: 'Status_Filter__c', type: 'text' },
    { label: 'Master Account ID', fieldName: 'Siebel_ID__c', type: 'text' },
    { label: 'Master Global Account ID', fieldName: 'Master_Global_Account_Id__c', type: 'text' },
    { label: 'Account Phone', fieldName: 'Phone', type: 'text' },
    { label: 'Address', fieldName: 'FullAddress', type: 'text' ,wrapText: true}

];

export default class DetailSearchLookup extends LightningElement
{
 label = {
        NoData,
    };
    @api tableColumns;
    @api qcondition;
    @api sortCondition;
    @api searchString;
    @api searchInFields;
    @api returningFields;
    @api featureItem;
    @api objectName;
    @api recordId;

    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track clearIconFlag = false;
    @track iconFlag = true;
    @track statusOptions;
    @track statusValue;
    @track tableData;
    @track objName;
    @track loading = false;
   
    accQueryFields = [];
    //selectedRows;

    //Filter
    @track keywordFilterValue;
    @track locatonFilterValue;
    orignalResult = [];
    globalfilterResults = [];
    isKeywordFilter = false;
    isLocationFilter = false;
    isStatusFilter = false;
    keywordFilterKey;
    locationFilterKey;
    statusFilterKey;
   
    @track accountData = [];
    @track columns = columns;
    @track recordCount;
    @track selectedRows = [];
    @track onLoadParam;

    renderedCallback()
    {
        this.statusValue = "All Statuses";
    }
   
    //initialRender;
    constructor()
    {
       super();
       this.objectName = 'Account';
       this.searchInFields = 'Name,Siebel_ID__c,Master_Global_Account_Id__c,Website';
       this.returningFields = 'Id,Name,Status_Filter__c,Siebel_ID__c,Master_Global_Account_Id__c,Phone,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode,Website';
       this.qcondition = [{ "fieldName": "recordType.name", "fieldValue": "Client" }];
       this.recordCount = false;
       this.onLoadParam = false;
    }

   
    handleChange(event)
    {
        this.searchString = event.detail.value;
    }

    handleSearch()
    {
        console.log('in handle search');
        this.keywordFilterValue = '';
        this.template.querySelector('[data-id=keywordinput]').value = '';
        this.locatonFilterValue = '';
        this.template.querySelector('[data-id=locationinput]').value = '';
        this.recordCount = false;
        if (this.searchString !== undefined && this.searchString.length > 4)
        {                    
            var tmpSearchString = this.searchString.replace(/[^\w\s]/gi, '');
			this.loading = true;
            searchClientContacts({'featureItem':this.featureItem, 'searchString': tmpSearchString, 'searchInFields': this.searchInFields, 'returningFields' : this.returningFields, 'targetObject': this.objectName, 'condition': this.qcondition, 'sortString': this.sortCondition,'strategicObjectId':this.recordId,'limitValue':'1000'}).then(result => {          
                if(result !== undefined && result != null)
                {
                    console.log('Account Result:'+JSON.stringify(result));
                    console.log('Account Result:'+JSON.stringify(result));
                    let resultTable = result;
                    resultTable.forEach(row => row.FullAddress = (row.BillingStreet != undefined ? row.BillingStreet : '') + ' ' + (row.BillingCity != undefined ? row.BillingCity : '') + ' ' + (row.BillingState != undefined ? row.BillingState : '') + ' ' + (row.BillingCountry != undefined ? row.BillingCountry : '') + ' ' + (row.BillingPostalCode != undefined ? row.BillingPostalCode : ''));
                    resultTable.forEach(function(record){
                        record.linkName = '/' + record.Id;
                    });
                    console.log('resultTable:'+JSON.stringify(resultTable));
                    this.orignalResult = resultTable;
                    this.tableData = resultTable;
                    this.globalfilterResults = resultTable;
                    this.accountData = resultTable;
                    if(this.accountData == undefined || this.accountData == null || this.accountData.length == 0)
                    {
                        console.log('this.accountData:-in if'+this.accountData);
                        this.recordCount = true;
                    }
                    /*let results = this.updateResults(result[0]);
                    this.orignalResult = result[0];
                    this.tableData = result[0];
                    this.globalfilterResults = result[0];
                    this.accountData = result[0];*/

                }
                this.loading = false;

            }).catch(error => {
                console.log('error****'+JSON.stringify(error));
                this.loading = false;
            });
        }
		else
	{
		this.dispatchEvent(
                new ShowToastEvent({
                        title: 'Error!!',
                        message: 'Please enter at least 5 characters.',
                        variant: 'error',
                        mode: 'pester'
                    })
            );
	}
    }
    handleRowSelection(event)
    {
        //this.selectedRows = event.detail.selectedRows;
        var selectedKeys = [];
        var selection = event.detail.selectedRows;
        for(var i = 0;i < selection.length; i++) 
        {           
            selectedKeys.push(selection[i].Id);
            this.selectedRows.push(selection[i].Id);
        }
        var itemsToRemove = [];
        let myArray = this.tableData;
        for(let i = 0; i < myArray.length; i++) 
        {
            var existFlag = false;
            for(var j = 0;j < selectedKeys.length;j++) 
            {
                if(myArray[i].Id == selectedKeys[j])
                {
                    existFlag = true;
                }
            }
            if(existFlag == false)
            {
                itemsToRemove.push(myArray[i].Id);
            }
        }
        console.log('selectedKeys'+selectedKeys);
        console.log('itemsToRemove'+itemsToRemove);
        console.log('selectedRows'+this.selectedRows);
        this.selectedRows = [...new Set(this.selectedRows)];
        var tempSelectedRowsAll = this.selectedRows;
        var itemsSelected = [];
        for(let i = 0; i < tempSelectedRowsAll.length; i++) 
        {
            var existFlag = true;
            for(var j = 0;j < itemsToRemove.length;j++) 
            {
                if(itemsToRemove[j] == tempSelectedRowsAll[i])
                {
                    existFlag = false;                   
                }
            }
            if(existFlag == true)
            {
                itemsSelected.push(tempSelectedRowsAll[i]);
            }
        }
        console.log('itemsSelected'+itemsSelected);
        this.selectedRows = [...new Set(itemsSelected)];
        console.log('this.selectedRows-End:'+this.selectedRows);
    }

    @api handleCancelModal(event)
    {
        const cancelEvent = new CustomEvent('cancleclickevent');
        this.dispatchEvent(cancelEvent);
    }

    handleKeywordFilter(event)
    {
        this.keywordFilterKey = event.target.value;
        this.keywordFilterKey = this.keywordFilterKey.replace(/[\/\\|\+\[\*()?]/gi,'');
        this.isKeywordFilter = true;
        this.buildFilteredResult();    
    }

    handleLocationFilter(event)
    {
        this.locationFilterKey = event.target.value;
        this.locationFilterKey = this.locationFilterKey.replace(/[\/\\|\+\[\*()?]/g,'');
        console.log('this.locationFilterKey:'+this.locationFilterKey);
        this.isLocationFilter = true;
        this.buildFilteredResult();
    }

    keywordFilterr()
    {
        let filterKey = this.keywordFilterKey.toUpperCase();

        let myArray = this.globalfilterResults;
        if (filterKey === '')
        {
            this.isKeywordFilter = false;
            this.buildFilteredResult();
        }
        else
        {
            let filteredResults = [];
            for (let i = 0; i < myArray.length; i++)
            {
                let nameStr = myArray[i].Name.toUpperCase();
                if (nameStr.match(filterKey))
                {
                    filteredResults.push(myArray[i]);
                    continue;
                }
                if(myArray[i].Status_Filter__c !== undefined)
                {
                    let streetVal = myArray[i].Status_Filter__c.toUpperCase();
                    if (streetVal.match(filterKey))
                    {
                        filteredResults.push(myArray[i]);
                        continue;
                    }
                }
				if(myArray[i].Siebel_ID__c !== undefined)
                {
                    let streetVal = myArray[i].Siebel_ID__c.toUpperCase();
                    if (streetVal.match(filterKey))
                    {
                        filteredResults.push(myArray[i]);
                        continue;
                    }
                }
				if(myArray[i].Master_Global_Account_Id__c !== undefined)
                {
                    let streetVal = myArray[i].Master_Global_Account_Id__c.toUpperCase();
                    if (streetVal.match(filterKey))
                    {
                        filteredResults.push(myArray[i]);
                        continue;
                    }
                }
				if(myArray[i].Phone !== undefined)
                {
                    let streetVal = myArray[i].Phone.toUpperCase();
                    if (streetVal.match(filterKey))
                    {
                        filteredResults.push(myArray[i]);
                        continue;
                    }
                }
            }
            this.tableData = filteredResults;
            this.globalfilterResults = filteredResults;
        }
    }

    locationFilter()
    {
        let filterKey = this.locationFilterKey.toUpperCase();
        let myArray = this.globalfilterResults;

        if (filterKey === '')
        {
            this.isLocationFilter = false;
            this.buildFilteredResult();
        }
        else
        {
            let filteredResults = [];
            for (let i = 0; i < myArray.length; i++)
            {
                if (this.objectName === 'Account')
                {
                    //Address
                    if (myArray[i].FullAddress !== undefined)
                    {
                        let streetVal = myArray[i].FullAddress.toUpperCase();
                        if (streetVal.match(filterKey))
                        {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    /*City
                    if (myArray[i].BillingCity !== undefined)
                    {
                        let cityVal = myArray[i].BillingCity.toUpperCase();
                        if (cityVal.match(filterKey))
                        {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }
                    //State
                    if (myArray[i].BillingState !== undefined)
                    {
                        let stateVal = myArray[i].BillingState.toUpperCase();
                        if (stateVal.match(filterKey))
                        {
                            filteredResults.push(myArray[i]);
                            continue;
                        }
                    }*/
                }
            }
            this.tableData = filteredResults;
            this.globalfilterResults = filteredResults;
        }
    }


    buildFilteredResult()
    {
        if (!this.isKeywordFilter && !this.isLocationFilter)
        {
            this.tableData = this.orignalResult;
        }

        this.globalfilterResults = this.orignalResult

        if (this.isKeywordFilter)
        {
            this.keywordFilterr();
        }

        if (this.isLocationFilter)
        {
            this.locationFilter();
        }

        if(this.tableData.length == 0)
        {
            this.onLoadParam = true;
        }

        let selectedKeys = [];
        for(var i=0;i < this.selectedRows.length;i++)
        {
            selectedKeys.push(this.selectedRows[i]);
        }
        console.log('IN selectedKeys'+selectedKeys);
        this.selectedRows = [...new Set(selectedKeys)];
        console.log("IN ==selectedRows---" + JSON.stringify(this.selectedRows));
    }

    handleClick(event)
    {
        this.selectedRows =this.tableData.find(({Id}) => Id === event.currentTarget.value);
    }

    handleKeyUp(event)
    {
        var key = 'which' in event ? event.which : event.keyCode;
        if (key == 13) {
            this.handleSearch();
        }
    }

    saveStrategicRecords()
    {
        if(this.selectedRows.length == 0)
        {
            this.dispatchEvent(
                new ShowToastEvent({
                        title: 'Error!!',
                        message: 'Please select atleast one Account to Save.',
                        variant: 'error',
                        mode: 'pester'
                    })
            );
        }
        else
        {
            createStrategicPlanRecord({'strategicObjectId':this.recordId, 'selectedAccIdList': this.selectedRows}).then(result => {          
                if(result !== undefined && result != null) 
                {
                    console.log('Account Result:'+JSON.stringify(result));
                }
                this.loading = false;
                location.reload();
            }).catch(error => {
                console.log('error****'+JSON.stringify(error));
                this.loading = false;
            });
        }
    }

    closeQuickAction() 
    {
        const closeQA = new CustomEvent('close');
        // Dispatches the event.
        this.dispatchEvent(closeQA);
    }
}