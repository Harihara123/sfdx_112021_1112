import { LightningElement, api, track} from 'lwc';

import ATS_NAME from '@salesforce/label/c.ATS_NAME';
import ATS_JOB_TITLE from '@salesforce/label/c.ATS_JOB_TITLE';
import ATS_FACET_OFFICE from '@salesforce/label/c.ATS_FACET_OFFICE';
import ATS_FACET_OPCO from '@salesforce/label/c.ATS_FACET_OPCO';
import ATS_EMAIL from '@salesforce/label/c.ATS_EMAIL';
import ATS_PHONE from '@salesforce/label/c.ATS_PHONE';
import ATS_DIVISION from '@salesforce/label/c.ATS_DIVISION';

import ATS_LAST_EDITED from '@salesforce/label/c.ATS_LAST_EDITED';
import ATS_START_DATE_TIME from '@salesforce/label/c.ATS_START_DATE_TIME';
import ATS_MORE from '@salesforce/label/c.ATS_MORE';
import ATS_LESS from '@salesforce/label/c.ATS_LESS';
import CRM_EmailLabel from '@salesforce/label/c.view_emaildt';
import { NavigationMixin } from 'lightning/navigation';
import performServerCall from '@salesforce/apex/BaseController.performServerCall'; 


import getUserDetails from '@salesforce/apex/ATSTalentActivityFunctions.getUserDetails';
export default class Timelineitemdetails extends NavigationMixin(LightningElement) {

    label = {
        ATS_NAME, 
        ATS_JOB_TITLE,
        ATS_FACET_OFFICE,
        ATS_FACET_OPCO,
        ATS_EMAIL,
        ATS_PHONE,
        ATS_DIVISION,
        ATS_LAST_EDITED,
        ATS_START_DATE_TIME,
		ATS_MORE,
        ATS_LESS,
        CRM_EmailLabel
    };

   maxCharactersToShowMore = 75;
   
   @api lastModifiedTime;
   @api userId;

   @api fieldValue;


   @api fieldLabel;
   @api popOver;
   @api popoverAlignment = '';
   @api defualtLineNumbersVisible = 1;
   @api userDetails;
   @api isTextMore = false;
   @track cssPopOverDisplay = '';
   @track cssDatePopOverDisplay = '';
   @track displaySpinner = '';
   @track isTaskRecordType;
   @track isEventRecordType;

   @api popOverDisplayed = false;
   @api datePopOverDisplayed = false;

   @api showMore = false;
   @api valuLength = 0;
   
   @api ignoreSymb;
   loadedDisplayValue = false;
   @api moreButton = false;
   @track hasRendered = false;
   @track moreButtonLabel = ATS_MORE+' >';
   @api isComments;
   @api recordId;
   emailMessageId;

   connectedCallback() {
    if(this.recordId!==undefined){
    var params = { recordId: this.recordId };
    performServerCall({
      className: "CRM",
      subClassName: "CRMEnterpriseReqFunctions",
      methodName: "getEmailMessage",
      parameters: params,
    })
      .then((result) => {
        this.emailMessageId = result.Id;
      })
      .catch((error) => {
        this.error = error;
        this.contacts = undefined;
      });
    }else{
    }
  }

   //
   renderedCallback() {        
       if(!this.hasRendered) {
        this.loadDisplayValue;     
        let commentBox = this.template.querySelector('[data-id=htmlDataId]');
        if(commentBox) {
            if (this.showMore && commentBox.scrollHeight > commentBox.offsetHeight) {                
                this.moreButton = true;
            } else {                
                commentBox.style.height = 'auto';
            } 
        }
       }
       this.hasRendered = true;       
    }
    toggleContent() {
        let commentBox = this.template.querySelector('[data-id=htmlDataId]');
        if (commentBox) {
            if(commentBox.style.height === 'auto') {
                commentBox.style.height = this.isDefualtTwoLinesVisiblie ?'2rem':'1rem';
                this.moreButtonLabel = ATS_MORE+' >'
            } else {
                commentBox.style.height = 'auto';
                this.moreButtonLabel = ATS_LESS+' >'
            }
        }
       }
    //

   @api
   get fieldName(){
       return this.fieldName;
   }
   set fieldName(value){
       this.showName = (value != undefined && value != '');
       this.fieldLabel= value;
   }

   @api 
   get fieldType(){
       return this.fieldType;
   }
   
   set fieldType(value){
       this.isDateType = (value === 'date');      
   }

   // Check the task record type
   @api
   get recordType(){
       return this.recordType;
   }
   set recordType(value){
     this.isTaskRecordType = (value != undefined && (value==='Task'));
     this.isEventRecordType = (value != undefined && (value==='Event'));
   }
   
   
   get formattedDate(){
       var dateString = '';

       if(this.fieldValue != undefined){

            var date = new Date(this.fieldValue);
            dateString = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();            
       }
       return dateString;
   }
   //Mouseover for user details
   onMouseOverDetails() {
    if (this.userDetails){        
        this.cssPopOverDisplay = 'show-popover';
    } else {
        this.cssPopOverDisplay = 'show-popover';
        this.displaySpinner = 'slds-show';
        this.loadUserDetails();
    }
   }

   onMouseOutDetails() {
       this.cssPopOverDisplay = 'hide-popover';
   }
   //
    
   // Handle Mouse Over action only for the Date pop over for the event recor.
   handleMouseOver(event){    
        if(this.isEventRecordType){
            this.datePopOverDisplayed = true;
            this.cssDatePopOverDisplay = 'show-popover';
        }               
    }
    
   // Handle Text More or Less
   handleTextMoreLess(event){
        this.isTextMore = !this.isTextMore;        
   }
   get showMoreButton(){
        return (this.showMore && this.valuLength >= this.maxCharactersToShowMore);
    }
    get showMoreButtonTitle(){
        return this.isTextMore ?"LESS >":"MORE >";
    }
    get displayValue(){
        return (this.isTextMore ? (this.fieldValue) : (this.fieldValue.substring((this.fieldLabel.length != undefined ? (this.fieldLabel.length + 5) : 0),this.maxCharactersToShowMore)));
    }
    get isDefualtTwoLinesVisiblie(){
        return (this.defualtLineNumbersVisible > 1);
    }
   // Handle Mouse Over Out for the date field
   handleMouseOut(event){
        this.datePopOverDisplayed = false;
        this.cssDatePopOverDisplay = 'hide-popover';  
   }
   
   @api
   closeAllPopups(){
    this.datePopOverDisplayed = false;
       this.cssDatePopOverDisplay = 'hide-popover';

    this.popOverDisplayed = false;
       this.cssPopOverDisplay = 'hide-popover';
   }
   // Handle click on the user link
//    clickLink(event){
              
//    }
   loadUserDetails(){
    if(this.userId != undefined && this.userId.length>0){
        
        getUserDetails({'userId': this.userId}).then(result => {
            if(result != undefined && result.user != null){
                this.userDetails = result;  
                this.displaySpinner = 'slds-hide';
                // this.cssPopOverDisplay = '';        
            }
        }).catch(error => {
            //console.log(JSON.stringify(error));
        });        
    }    
   }
   
   get loadDisplayValue(){
               
       if(this.fieldValue != undefined){       
            if(true){
                const htmlEle = this.template.querySelector('[data-id=htmlDataId]');
                if(htmlEle != undefined){
                    if(this.isTextMore){
                        htmlEle.innerHTML = this.fieldValue;
                    }else{
                        htmlEle.innerHTML = this.fieldValue;
                    }            
                }else{
                    //console.log(" ORPHAN " + this.fieldLabel + "   "+this.fieldValue);
                }                
            }else{                
                if(this.isTextMore){
                    return this.fieldValue;
                }else{
                    return this.fieldValue;
                } 
            }
       }            
   }

  navigateToemail() {
  window.open("/" + this.emailMessageId,'_blank');
     //this[NavigationMixin.Navigate]({
       // type: "standard__recordRelationshipPage",
        //attributes: {
         // url: window.open("/" + this.emailMessageId),
       // },
     // });
}

//    get popoverStyles() {
//        let popoverStyles = 'act-detail-assign slds-popover slds-nubbin_top-right slds-popover_medium slds-p-around_small';
    
//        if (this.popoverStyles) {
//            popoverStyles = 'act-detail-assign-top slds-popover slds-nubbin_bottom-right slds-popover_medium slds-p-around_small';
//        }

//        return popoverStyles;
    
//    }
   
}