trigger AccountChangeAsyncTrigger on AccountChangeEvent(after insert) {
	List<AccountChangeEvent> changes = Trigger.new;
	AccountTriggerHandler handler = new AccountTriggerHandler();
	Set<String> recordIds = new Set<String> ();
	Set<String> recordIdSet = new Set<String>();
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');

	try {
		//Get all record Ids for this change and add it to a set for further processing
		for (AccountChangeEvent ev : changes) {
			System.debug('ChangeEvent::::'+ev);
			
			List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
			recordIds.addAll(tempIds);
			
			
			if (ev.ChangeEventHeader.getChangeType() != 'Delete') {
				recordIdSet.addAll(tempIds);
			}

		}
		//Handler for after insert
		
		if (recordIds.size() > 0) {
			
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Account');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Account',ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'AccountChangeAsyncTrigger', 'triggerBody', 
					'CDC Sync triggered ' + recordIds.size() + ' Account records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
			
		// CDCDataExportUtility.addRecordToCatcher(recordIds);
		}
		//to update ownerId
		if(recordIdSet.size()>0){
			handler.OnAfterInsertAsync(recordIdSet);	
		}
	} catch (Exception ex) {
        ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'AccountChangeAsyncTrigger', 'triggerBody', ex);
    }
}