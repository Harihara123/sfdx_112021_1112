// Trigger for listening to Cloud_News events.
trigger AccountPlatformEventTrigger on AccountChangesMDMMessage__e (after insert) {

List<Account_Platform_Event__c> msg = new List<Account_Platform_Event__c>();

    // Iterate through each notification.
    for (AccountChangesMDMMessage__e event : Trigger.New) 
    {
              // Create Record to hold the event data.
           System.debug('#event#'+event);
        Account_Platform_Event__c platformEvent = new Account_Platform_Event__c();

            platformEvent.Id__c = event.Id__c;
            platformEvent.Name__c  = event.Name__c; 
            platformEvent.Siebel_ID__c  = event.Siebel_ID__c; 
			platformEvent.Outbound_Message__c = event.Outbound_Message__c;
            msg.add(platformEvent );        

    }
	
          // Insert all records in the list.
            if (msg.size() > 0) 
            {
                insert msg;
            }
}