/*******************************************************************
Name        : AccountStrategicInitTrigger
Created By  : Vivek Ojha (Appirio)
Date        : 04 June 2015
Story/Task  : S-321244 / T-407647
Purpose     : This trigger is invoked for all contexts
              and delegates control to AccountStrategicInitTriggerHandler
********************************************************************/
trigger AccountStrategicInitTrigger on Account_Strategic_Initiative__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
  if(TriggerState.isActive('AccountStrategicInitTrigger')) {
        AccountStrategicInitTriggerHandler handler = new AccountStrategicInitTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }
}