/*******************************************************************
Name        : Acct_Cont_Assoc_Trigger
Created By  : JFreese (Appirio)
Date        : 19 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : This trigger is invoked for all contexts
              and delegates control to Acct_Cont_Assoc_TriggerHandler
********************************************************************/
trigger Acct_Cont_Assoc_Trigger on Account_Contact_Association__c (before insert, after insert, before update, after update, before delete, after delete) {

    if(TriggerState.isActive('Acct_Cont_Assoc_Trigger')) {
        Acct_Cont_Assoc_TriggerHandler handler = new Acct_Cont_Assoc_TriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for after Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }
}