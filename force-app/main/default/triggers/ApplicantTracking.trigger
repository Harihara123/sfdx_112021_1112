trigger ApplicantTracking on ApplicantPriorityTracking__ChangeEvent (after insert)  { 

	List<ApplicantPriorityTracking__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 
    CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');

    try {
        //Get all record Ids for this change and add it to a set for further processing
        for (ApplicantPriorityTracking__ChangeEvent ev : changes) {
            System.debug('ChangeEvent::::'+ev);
            recordIds = new Set<String>();
            List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
            recordIds.addAll(tempIds);
            if (recordIds.size() > 0) {
        
                // for CDC flow
                if(config.Data_Export_All_Fields__c){
                    List<String> ignoreFields  = DataExportUtility.ObjectMap.get('ApplicantPriorityTracking__c');
                    System.debug('ignoreFields::'+ignoreFields);
                    CDCDataExportUtility.getRecords(recordIds,'ApplicantPriorityTracking__c',ignoreFields);
                    if (config.Enable_Trace_Logging__c) {
                        ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'ApplicantTracking', 'triggerBody', 
						    'CDC Sync triggered ' + recordIds.size() + ' ApplicantPriorityTracking__c records: ' + CDCDataExportUtility.joinIds(recordIds));
                    }
                }
            }
            
        }
    }  catch (Exception ex) {
        ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'ApplicantTracking', 'triggerBody', ex);
    }

}