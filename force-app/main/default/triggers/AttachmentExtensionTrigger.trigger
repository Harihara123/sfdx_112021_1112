trigger AttachmentExtensionTrigger on Attachment_Extension__c (after insert, after update)  { 
	AttachmentExtensionTriggerHandler handler = new AttachmentExtensionTriggerHandler();

	if(Trigger.isAfter && Trigger.isInsert) {
		handler.onAfterInsert(Trigger.newMap);
	}
	if(Trigger.isUpdate && Trigger.isAfter){
		handler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
	}

}