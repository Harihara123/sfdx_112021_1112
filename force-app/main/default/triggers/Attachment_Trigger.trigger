trigger Attachment_Trigger on Attachment (after delete, after insert, after update) {
    List<Attachment_Extension__c> attExtnList = new List<Attachment_Extension__c>();
    Attachment_Extension__c attExtn;
    List<Database.UpsertResult > results;

    private static boolean hasBeenProccessed = false;
    //
    Set<ID>parentIds = new Set<ID>();    
            
    try {
        if (Trigger.isDelete) {
            for(Attachment att : Trigger.Old) {
                parentIds.add(att.ParentId);
            }

            Map<Id, Talent_Document__c> tdMap = new Map<Id, Talent_Document__c>([Select Id, Committed_Document__c, Document_Conversion__c, Talent__c from Talent_Document__c where ID in: parentIds]);
                
            for (Attachment att : Trigger.Old) {
                attExtn = new Attachment_Extension__c();
                attExtn.Record_Id__c = att.Id;
                attExtn.Object_Name__c = att.ParentId.getSObjectType().getDescribe().getName();
                attExtn.Parent_Id__c = att.ParentId;
                attExtn.Event__c = 'Delete';
                if (UserInfo.getName() == 'Talent IntegrationBatch') {
                    // Set values for RWS resume pushes, to set it as a lower priority
                    // This condition needs to be first since the profile will match (System Integration) the second condition as well.
                    attExtn.Processing_Priority__c = 3;
                    attExtn.Processing_Type__c = 'BATCH';
                } else if (UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration) {
                    // Set values for batch or data migration data, to set it as a lower priority
                    attExtn.Processing_Priority__c = 4;
                    attExtn.Processing_Type__c = 'BATCH';
                }
            
                Talent_Document__c talentDoc = tdMap.get(att.ParentId);
                if (talentDoc != Null) {
                    attExtn.Gparent_Id__c = talentDoc.Talent__c;
                    attExtn.Talent_Document_Committed__c = talentDoc.Committed_Document__c;
                    attExtn.Document_Conversion__c = talentDoc.Document_Conversion__c;
                }
                attExtnList.add(attExtn);
            }

            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'Attachment_Trigger', 'triggerBody', 
                        'Triggered ' + tdMap.values().size() + ' Talent_Document__c records: ' + CDCDataExportUtility.joinObjIds(tdMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(tdMap.values(), 'Talent_Document__c');
                hasBeenProccessed = true; 
            } 
        }
        
        if (Trigger.isInsert || Trigger.isUpdate) {
            for (Attachment att : Trigger.New) {
                parentIds.add(att.ParentId);
            }
            
            Map<Id, Talent_Document__c> tdMap = new Map<Id, Talent_Document__c>([Select Id, Committed_Document__c, Document_Conversion__c, Talent__c from Talent_Document__c where ID in: parentIds]);
            
            for (Attachment att : Trigger.New){
                attExtn = new Attachment_Extension__c();
                attExtn.Record_Id__c = att.Id;
                attExtn.Object_Name__c = att.ParentId.getSObjectType().getDescribe().getName();
                attExtn.Parent_Id__c = att.ParentId;
                attExtn.Event__c = (Trigger.isInsert) ? 'Create' : 'Update';   
                if (UserInfo.getName() == 'Talent IntegrationBatch') {
                    // Set values for RWS resume pushes, to set it as a lower priority
                    // This condition needs to be first since the profile will match (System Integration) the second condition as well.
                    attExtn.Processing_Priority__c = 3;
                    attExtn.Processing_Type__c = 'BATCH';
                } else if (UserInfo.getProfileId().substring(0,15) == system.label.Profile_System_Integration) {
                    // Set values for batch or data migration data, to set it as a lower priority
                    attExtn.Processing_Priority__c = 4;
                    attExtn.Processing_Type__c = 'BATCH';
                }

                Talent_Document__c talentDoc = tdMap.get(att.ParentId);
                if (talentDoc != Null) {
                    attExtn.Gparent_Id__c = talentDoc.Talent__c;
                    attExtn.Talent_Document_Committed__c = talentDoc.Committed_Document__c;
                    attExtn.Document_Conversion__c = talentDoc.Document_Conversion__c;
                }
                attExtnList.add(attExtn);
            }

            Connected_Data_Export_Switch__c config = Connected_Data_Export_Switch__c.getInstance('DataExport');
            if (!Test.isRunningTest() && config.Data_Extraction_Insert_Flag__c && !hasBeenProccessed ) {
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/Trigger/InsertDataExtraction', 'Attachment_Trigger', 'triggerBody', 
                        'Triggered ' + tdMap.values().size() + ' Talent_Document__c records: ' + CDCDataExportUtility.joinObjIds(tdMap.values()));
                }
                PubSubBatchHandler.insertDataExteractionRecord(tdMap.values(), 'Talent_Document__c');
                hasBeenProccessed = true; 
            } 


        }
    } catch (Exception e){               
        System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
        System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
        System.debug(logginglevel.WARN,'Cause: ' + e.getCause());    
        System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
        System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
    }
    
    try{
       System.debug(logginglevel.WARN,'Attachment Ids to upsert - ' + attExtnList.size());
       if (!attExtnList.isEmpty()) {
           System.debug(logginglevel.WARN,'Attachment Ids to upsert - ' + attExtnList.size());
           results = Database.upsert(attExtnList, Attachment_Extension__c.Fields.Record_Id__c, true); 
           System.debug(logginglevel.WARN,'results :' + results); 
            for (Database.UpsertResult result : results){
                if (!result.getErrors().isEmpty()){
                    System.debug(logginglevel.WARN,'Errors :' + result.getErrors());  
                }
           }      
       }
    } catch (DmlException de) {
        Integer numErrors = de.getNumDml();
        System.debug(logginglevel.WARN,'getNumDml=' + numErrors);
        for(Integer i=0;i<numErrors;i++) {
            System.debug(logginglevel.WARN,'getDmlFieldNames=' + de.getDmlFieldNames(i));
            System.debug(logginglevel.WARN,'getDmlMessage=' + de.getDmlMessage(i)); 
        }
    } catch (Exception e) {
        System.debug(logginglevel.WARN,'Exception type caught: ' + e.getTypeName());    
        System.debug(logginglevel.WARN,'Message: ' + e.getMessage());    
        System.debug(logginglevel.WARN,'Cause: ' + e.getCause());
        System.debug(logginglevel.WARN,'Line number: ' + e.getLineNumber());    
        System.debug(logginglevel.WARN,'Stack trace: ' + e.getStackTraceString()); 
    }
}