trigger Attachment_Validation_Trigger on Attachment bulk (before insert) {
    static List<String> genericTypes = new List<String>();
    static List<String> resumeTypes = new List<String>();
    Global_LOV__c[] fileTypes = [SELECT Id, LOV_Name__c, Text_Value__c from Global_LOV__c where LOV_Name__c in ('ATSGenericFiletypesAllowed','ATSResumeFiletypesAllowed')];
    
    // Load allowed file types list from Global LOV.
    for (Global_LOV__c lov : fileTypes) {
        if (lov.LOV_Name__c == 'ATSGenericFiletypesAllowed') {
            genericTypes.add(lov.Text_Value__c);
        } else if (lov.LOV_Name__c == 'ATSResumeFiletypesAllowed') {
            resumeTypes.add(lov.Text_Value__c);
        }
    }

    Integer MaxAttachmentSize = 5242880;
    
    List<Id> idList = new List<Id>();
    for (Attachment a : Trigger.new) {
        idList.add(a.ParentId);
    }
    
    Map<Id, Talent_Document__c> tdMap = new Map<Id, Talent_Document__c>([SELECT Id, Document_Type__c from Talent_Document__c where Id = :idList]);
    
    // Disallow any attachment exceeding max size limit or other than the allowed extension types.
    for (Attachment a : Trigger.new) {
        // Execute actions only if the parent object is Talent_Document__c
        if (a.ParentId.getSObjectType().getDescribe().getName().equalsIgnoreCase('Talent_Document__c')) {
            if (a.BodyLength > MaxAttachmentSize) {
                a.addError('Attachment too large. Maximum allowed size is 5MB.');
            } 
            if (UserInfo.getName() != 'Cloudstorage Integration') {
                //String docType = [SELECT Id, Document_Type__c from Talent_Document__c where Id = :a.ParentId][0].Document_Type__c;
                String docType = tdMap.get(a.ParentId).Document_Type__c;
            	String extn = a.Name.right(a.Name.length() - a.Name.lastIndexOf('.') - 1);
                Boolean match = false;
                for (String ftype : (docType == 'Resume')? resumeTypes : genericTypes) {
                    if (extn == ftype) {
                        match = true;
                        break;
                    }
                }
                if (!match) {
                    a.addError('Invalid file type');
                }
            } 
        }
    }
}