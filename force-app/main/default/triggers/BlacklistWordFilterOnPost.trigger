/**
 * Filter Feed Items for blacklisted words
 * Author: Quinton Wall - qwall@salesforce.com
 */
trigger BlacklistWordFilterOnPost on FeedItem (before insert,before update) 
{
     new BlacklistFilterDelegate().filterFeedItems(trigger.new);
}