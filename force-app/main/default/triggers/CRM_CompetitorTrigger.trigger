trigger CRM_CompetitorTrigger on Competitors_Strongest_to_Weakest__c (after insert,after update,after delete) 
{
    List<Id> oppList = new List<Id>();
    
    if(trigger.isInsert && trigger.isAfter)
    {
        for(Competitors_Strongest_to_Weakest__c obj : trigger.new)
        {
            oppList.add(obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,NULL,trigger.newMap);
        }
    }
    
    if(trigger.isUpdate && trigger.isAfter)
    {
        for(Competitors_Strongest_to_Weakest__c obj : trigger.new)
        {
            if(obj.Strongest_to_Weakest__c <> trigger.oldMap.get(obj.id).Strongest_to_Weakest__c || 
               obj.Competitor__c <> trigger.oldMap.get(obj.id).Competitor__c)
            {
                oppList.add(Obj.Opportunity__c);
            }
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,NULL,trigger.newMap);
        }
    }
    if(trigger.isDelete && trigger.isAfter)
    {
        for(Competitors_Strongest_to_Weakest__c obj : trigger.old)
        {
            oppList.add(Obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,NULL,NULL);
        }
    }
    
}