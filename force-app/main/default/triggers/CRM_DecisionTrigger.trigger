trigger CRM_DecisionTrigger on Decision_Criteria_Strongest_to_Weakest__c (after insert,after update,after delete) 
{
    List<Id> oppList = new List<Id>();
    
    if(trigger.isInsert && trigger.isAfter)
    {
        for(Decision_Criteria_Strongest_to_Weakest__c obj : trigger.new)
        {
            oppList.add(obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,trigger.newMap,NULL);
        }
    }
    
    if(trigger.isUpdate && trigger.isAfter)
    {
        for(Decision_Criteria_Strongest_to_Weakest__c obj : trigger.new)
        {
            if(obj.Strongest_to_Weakest__c <> trigger.oldMap.get(obj.id).Strongest_to_Weakest__c || 
               obj.Decision_Criteria__c <> trigger.oldMap.get(obj.id).Decision_Criteria__c)
            {
                oppList.add(Obj.Opportunity__c);
            }
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,trigger.newMap,NULL);
        }
    }
    if(trigger.isDelete && trigger.isAfter)
    {
        for(Decision_Criteria_Strongest_to_Weakest__c obj : trigger.old)
        {
            oppList.add(Obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,NULL,NULL);
        }
    }
    
    
}