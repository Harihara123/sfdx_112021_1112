trigger CRM_NumOfSeatsByLocationTrigger on Num_of_Seats_by_Location__c (after insert,after update,after delete) {
    
    CRM_NumOfSeatsByLocationTriggerHelper handler = new CRM_NumOfSeatsByLocationTriggerHelper();
    if(Trigger.isInsert && Trigger.isAfter){
        handler.updateOppNumOfResources(Trigger.newMap);
    }else if(Trigger.isUpdate && Trigger.isAfter){
        handler.updateOppNumOfResources(Trigger.newMap);    
    }/*else if(Trigger.isDelete && Trigger.isAfter){
        handler.updateOppNumOfResources(Trigger.oldMap);
    }*/
}