trigger CRM_TGSDifferentiationTrigger on TGS_Differentiation_Strongest_Weakest__c (after insert,after update,after delete) 
{
    
    List<Id> oppList = new List<Id>();
    
    if(trigger.isInsert && trigger.isAfter)
    {
        for(TGS_Differentiation_Strongest_Weakest__c obj : trigger.new)
        {
            oppList.add(obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,trigger.newMap,NULL,NULL);
        }
    }
    
    if(trigger.isUpdate && trigger.isAfter)
    {
        for(TGS_Differentiation_Strongest_Weakest__c obj : trigger.new)
        {
            if(obj.Strongest_to_Weakest__c <> trigger.oldMap.get(obj.id).Strongest_to_Weakest__c || 
               obj.TGS_Differentiation__c <> trigger.oldMap.get(obj.id).TGS_Differentiation__c)
            {
                oppList.add(Obj.Opportunity__c);
            }
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,trigger.newMap,NULL,NULL);
        }
    }
    if(trigger.isDelete && trigger.isAfter)
    {
        for(TGS_Differentiation_Strongest_Weakest__c obj : trigger.old)
        {
            oppList.add(Obj.Opportunity__c);
        }
        if(!oppList.isEmpty())
        {
            CRM_TGSRelatedTriggerHelper.updateOpportunity(oppList,NULL,NULL,NULL);
        }
    }
    
    
}