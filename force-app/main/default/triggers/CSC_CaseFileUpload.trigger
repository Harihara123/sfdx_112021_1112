trigger CSC_CaseFileUpload on ContentDocumentLink (before insert,before delete) {
    if(trigger.isBefore){
        if(trigger.isInsert)
            CSC_CaseFileUploadHelper.updateCaseFlag(trigger.new);
        if(trigger.isDelete)
            CSC_CaseFileUploadHelper.beforeDelete(trigger.old);
    }
}