trigger CSC_CaseStatusHistoryTrigger on Case_Status_History__c (before insert) {
	if(Trigger.isInsert && Trigger.isBefore)
        CSC_CaseStatusHistoryTriggerHelper.updateOnHoldReason(Trigger.New);
}