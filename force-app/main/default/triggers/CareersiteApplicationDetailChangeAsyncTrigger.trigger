trigger CareersiteApplicationDetailChangeAsyncTrigger on Careersite_Application_Details__ChangeEvent (after insert) {
List<Careersite_Application_Details__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');

	try {
		//Get all record Ids for this change and add it to a set for further processing
		for (Careersite_Application_Details__ChangeEvent ev : changes) {
			System.debug('ChangeEvent::::'+ev);
		// recordIds = new Set<String> ();
			List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
			recordIds.addAll(tempIds);
		}
		//Handler for after insert

		if (recordIds.size() > 0) {
		
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Careersite_Application_Details__c');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Careersite_Application_Details__c',ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'CareersiteApplicationDetailChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Careersite_Application_Details__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		}
	} catch (Exception ex) {
        ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'CareersiteApplicationDetailChangeAsyncTrigger', 'triggerBody', ex);
    }

}