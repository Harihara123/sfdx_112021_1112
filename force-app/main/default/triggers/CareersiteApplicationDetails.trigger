trigger CareersiteApplicationDetails on Careersite_Application_Details__c (after insert, after update)  {


    if(TriggerState.isActive('CareersiteApplicationDetails')) {
             

                CareersitApplicationDetailTriggerHandler handler = new CareersitApplicationDetailTriggerHandler();
                  
                    if(Trigger.isInsert && Trigger.isAfter) {
                        //Handler for after insert
                        handler.OnAfterInsert(Trigger.new);
                    } else if(Trigger.isUpdate && Trigger.isAfter){
                        //Handler for after update trigger
                        handler.OnAfterUpdate(Trigger.new);
                    } 
           }


 }