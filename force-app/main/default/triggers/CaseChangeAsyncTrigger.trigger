trigger CaseChangeAsyncTrigger on CaseChangeEvent (after insert) {
    List<CaseChangeEvent> changes = Trigger.new;

    Set<String> recordIds = new Set<String> ();
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');

    try {
        for (CaseChangeEvent ev : changes) {
			// System.debug('ChangeEvent::::'+ev);
			EventBus.ChangeEventHeader ceHeader = ev.ChangeEventHeader;
			// System.debug('Change event type ' + ceHeader.changetype);
			recordIds.addAll(ev.ChangeEventHeader.getRecordIds());
		}

        if (recordIds.size() > 0) {
			// for CDC flow
			if(config.Data_Export_All_Fields__c) {
				List<String> ignoreFields = DataExportUtility.ObjectMap.get('Case');
				CDCDataExportUtility.getRecords(recordIds, 'Case', ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'CaseChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Case records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		} 

    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'CaseChangeAsyncTrigger', 'triggerBody', ex);
    }
}