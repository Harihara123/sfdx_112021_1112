trigger CaseTrigger on Case (before insert, after insert, before update,after update, after delete) {
private static boolean activityCreated = false; 
    if(TriggerState.isActive('caseTrigger')) {
        CaseTriggerHandler handler = new CaseTriggerHandler();
           
        //Before Insert
        if (Trigger.isInsert && Trigger.isBefore) {
            handler.handleCaseBeforeInsert(Trigger.new);
            // S-161597 Region Re-Alignment. 
            Map<Id,Schema.RecordTypeInfo> rtMap = Case.sobjectType.getDescribe().getRecordTypeInfosById();
            CSC_CaseTriggerHelper.populateCenterName(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.populateFSGRecordType(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.autoPriorityOnCases(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.processE2CCases(Trigger.new, rtMap);
        }
        
        //After Insert
        else if (Trigger.isInsert && Trigger.isAfter) {
             // we are skipping future method when triggers get call  from batch else we execute event creation activity, but we are creating Event activity in batch class instead of trigger
          
            if(!System.isBatch()){
                handler.handleCaseAfterInsert(Trigger.new);
            }
            //Activity creation call skipped if trigger get call from batch, for Retention case no need to create Task  
            //Task getting created for Close case only, in retention case status is not close.  
            if(!activityCreated && !System.isBatch()){
                activityCreated = true;
                handler.handleActivityCreation(Trigger.new,Trigger.oldMap, Trigger.newMap, true);
            }
            handler.syncToGCP(Trigger.new);
            
            // Process CSC Email2Case 
        }
        //After Update
        else if(Trigger.isUpdate && Trigger.isAfter){
            //for story S-157870, S-158776 // Evolve team feature
            Map<Id,Schema.RecordTypeInfo> rtMap = Case.sobjectType.getDescribe().getRecordTypeInfosById();
            // S-158776 Lock the case status during holidays and weekends
            CSC_CaseTriggerHelper.lockStatus(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            // S-176349
            CSC_CaseTriggerHelper.updateCaseStatusHistory(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            // S-178309 Auto close Parent cases
            CSC_CaseTriggerHelper.autoCloseParentCases(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.updateParentStatus(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.processMilestone(Trigger.new,Trigger.oldMap);
            if(!System.isBatch() && !activityCreated){
                activityCreated = true;
                handler.handleActivityCreation(Trigger.new,Trigger.oldMap, Trigger.newMap, false);
            }
            //S-199480 
            CSC_CaseTriggerHelper.changeStartDate(Trigger.new,Trigger.oldMap, Trigger.newMap,rtMap);
            handler.syncToGCP(Trigger.new);
        }
        //before Update
        else if(Trigger.isUpdate && Trigger.isBefore){
            // S-161597 Region Re-Alignment. 
            Map<Id,Schema.RecordTypeInfo> rtMap = Case.sobjectType.getDescribe().getRecordTypeInfosById();
            if(CSC_CaseTriggerHelper.isFirstTime){
                CSC_CaseTriggerHelper.isFirstTime = false;
            	CSC_CaseTriggerHelper.populateCenterName(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            }
            CSC_CaseTriggerHelper.reopenCaseStatus(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.populateFSGRecordType(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.autoPriorityOnCases(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            // S-157870 Future date must be a working day. 
            CSC_CaseTriggerHelper.futureDateValidation(Trigger.new,Trigger.oldMap,Trigger.newMap,rtMap);
            CSC_CaseTriggerHelper.processClosedMilestone(Trigger.new,Trigger.oldMap);            
        } 
        // After delete
        else if(Trigger.isDelete && Trigger.isAfter) {
            handler.syncToGCP(Trigger.old);
        }
        
    }
}