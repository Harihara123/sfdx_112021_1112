trigger ContactChangeAsyncTrigger on ContactChangeEvent(after insert) {
	List<ContactChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> ();
	Map<Id, Contact> oldRecords = null;
	Map<Id, Contact> createdRecords = new Map<Id, Contact> ();
	Map<Id, Contact> updatedRecords = new Map<Id, Contact> ();
	Map<Id, Contact> deletedRecords = new Map<Id, Contact> ();
	Map<Id, String> optoutBrandMap = new Map<Id, String> ();

	try {
		//Get all record Ids for this change and add it to a set for further processing
		for (ContactChangeEvent ev : changes) {
			System.debug('ChangeEvent::::' + ev);
			EventBus.ChangeEventHeader ceHeader = ev.ChangeEventHeader;
			System.debug('Change event type ' + ceHeader.changetype);
			//recordIds = new Set<String> ();
			List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
			recordIds.addAll(tempIds);
			
			if ((ev.ChangeEventHeader.getChangeType() == 'CREATE') ||
			(ev.ChangeEventHeader.getChangeType() == 'UPDATE') ||
			(ev.ChangeEventHeader.getChangeType() == 'Delete')) {
				for (String recId : tempIds) {
					Id conId = Id.valueOf(recId);
					if (ev.ChangeEventHeader.getChangeType() == 'CREATE') {
						createdRecords.put(conId, null);
						//S-233076
						optoutBrandMap = ContactChangeAsyncTriggerHandler.optoutBrand(conId, ev, optoutBrandMap);
					}
					if (ev.ChangeEventHeader.getChangeType() == 'UPDATE') {
						updatedRecords.put(conId, null);
						//S-233076
						optoutBrandMap = ContactChangeAsyncTriggerHandler.optoutBrand(conId, ev, optoutBrandMap);
					}
					else {
						deletedRecords.put(conId, null);
					}
				}
			}

		}
		//Handler for after insert
		CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');

		if (recordIds.size() > 0) {

			// for CDC flow
			if (config.Data_Export_All_Fields__c) {
				List<String> ignoreFields = DataExportUtility.ObjectMap.get('Contact');
				System.debug('ignoreFields::' + ignoreFields);
				CDCDataExportUtility.getRecords(recordIds, 'Contact', ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'ContactChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Contact records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
			//CDCDataExportUtility.addRecordToCatcher(recordIds);
		}

		if (createdRecords.size() > 0) {
			for (Contact cont :[Select Id, Email, Other_Email__c, Work_Email__c, FirstName, LastName, CreatedDate, LastModifiedDate, externalSourceId__c,
								PrefCentre_Aerotek_OptOut_Email__c, PrefCentre_TEKsystems_OptOut_Email__c, PrefCentre_AstonCarter_OptOut_Email__c,
								PrefCentre_EASi_OptOut_Email__c, PrefCentre_AGS_OptOut_Email__c, PrefCentre_Brand__c								 
								from Contact where Id in :updatedRecords.keySet()]) {
				createdRecords.put(cont.Id, cont);
			}
			//S-233076
		/*	if(optoutBrandMap.size() > 0) {
				ContactChangeAsyncTriggerHandler.consentPrefEmailOptoutPE(createdRecords, optoutBrandMap);
			}*/
		}
		// UTM changes..S-183029
		if (updatedRecords.size() > 0) {
			// Get required fields from the contact record
			for (Contact cont :[Select Id, Email, Other_Email__c, Work_Email__c, FirstName, LastName, CreatedDate, LastModifiedDate, externalSourceId__c,
								PrefCentre_Aerotek_OptOut_Email__c, PrefCentre_TEKsystems_OptOut_Email__c, PrefCentre_AstonCarter_OptOut_Email__c,
								PrefCentre_EASi_OptOut_Email__c, PrefCentre_AGS_OptOut_Email__c, PrefCentre_Brand__c								 
								from Contact where Id in :updatedRecords.keySet()]) {
				updatedRecords.put(cont.Id, cont);
			}
			ContactTriggerHandler.CandidateSyncWithLinkedIn('afterUpdate', oldRecords, updatedRecords, false);
			//S-233076
			if(optoutBrandMap.size() > 0) {
				ContactChangeAsyncTriggerHandler.consentPrefEmailOptoutPE(updatedRecords, optoutBrandMap);
			}
		}
		if (deletedRecords.size() > 0) {
			for (Contact cont :[Select Id, Email, Other_Email__c, Work_Email__c, FirstName, LastName, CreatedDate, LastModifiedDate from Contact where Id in :deletedRecords.keySet()]) {
				deletedRecords.put(cont.Id, cont);
			}
			ContactTriggerHandler handler = new ContactTriggerHandler();
			handler.OnAfterDelete(deletedRecords, false);
		}
	} catch(Exception ex) {
		// ConnectedLog.LogException('ContactChangeAsyncTrigger', 'main', ex);
	}

	
}