/***************************************************************************
Name        : ContactTrigger
Created By  : SRajoria (Appirio)
Date        : 20 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : Keep single trigger for all contact trigger events
*****************************************************************************/
trigger ContactTrigger on Contact (before insert, after insert, before update, after update,
                                   before delete, after delete, after undelete) {

  if(TriggerState.isActive('ContactTrigger')) {
        ContactTriggerHandler handler = new ContactTriggerHandler();
      if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
           // if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
          //  System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
          //  et4ae5.triggerUtility.automate('Contact');
          //  }
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
             handler.OnAfterInsert(Trigger.new, Trigger.oldMap, Trigger.newMap);
         //   if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
         //   System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
          //  et4ae5.triggerUtility.automate('Contact');
         //  } 
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
          //  if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
         //   System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
         //   et4ae5.triggerUtility.automate('Contact');
         //   }
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            // if(!System.isBatch()){ // Bypass for UTM Batches - S-183029
                // Undoing this change to make Contact List sync to GCP work - S-223025
                handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
            // }
          //  if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
          //  System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
          //  et4ae5.triggerUtility.automate('Contact');
         //  }           
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);            
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for after Delete trigger
            if(!System.isBatch()){ // Bypass for UTM Batches - S-183029
                handler.OnAfterDelete(Trigger.oldMap,true);
                }
        } else if (Trigger.isUndelete && Trigger.isAfter) {
            //Handler for after Undelete trigger
            handler.OnAfterUndelete(Trigger.newMap);
        }
  }

}