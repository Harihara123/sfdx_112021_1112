/*******************************************************************
Name        : EngagementRuleTrigger
Created By  : ISingh
Date        : 30 Oct 2017
Story/Task  : S-41856
Purpose     : This trigger executes when there is a change on 
			  Engaggment rule is created or updated.
********************************************************************/

trigger EngagementRuleTrigger on Engagement_Rule__c (before insert, after insert, before update, after update, before delete, after delete) {

	  if(TriggerState.isActive('Engagement_Rule__c')) {
        EngagementRuleTriggerHandler handler = new EngagementRuleTriggerHandler();
  		List<Engagement_Rule__c> engagementRuleList = Trigger.New;

		  if(Trigger.isInsert && Trigger.isAfter){
		  	for(Engagement_Rule__c er : engagementRuleList){
				handler.OnAfterInsert(er.Account__c);
				}
	        } else if(Trigger.isUpdate && Trigger.isAfter){
				for(Engagement_Rule__c er : engagementRuleList){
					handler.OnAfterUpdate(er.Account__c);
				}  
	        } else if (Trigger.isDelete && Trigger.isAfter) {
	        	for(Engagement_Rule__c er : engagementRuleList){
	            	handler.OnAfterDelete(er.Account__c);
	           }
        }
    }

}