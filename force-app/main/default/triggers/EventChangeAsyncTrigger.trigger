trigger EventChangeAsyncTrigger on EventChangeEvent (after insert) {
	List<EventChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 
	Set<String> appIds = new Set<String>();
    //List to send Events to MC journey
	List<String> eventList = new List<String>(); 
	EventTriggerHandler trgHandler = new EventTriggerHandler();

    //Get all record Ids for this change and add it to a set for further processing
	for (EventChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
     //   recordIds = new Set<String> ();
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
       /*
        if (recordIds.size() > 0) {
       
            // for CDC flow
            if(CDC_Data_Export_Flow__c.getInstance('DataExport').Data_Export_All_Fields__c){
                List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Event');
                System.debug('ignoreFields::'+ignoreFields);
                CDCDataExportUtility.getRecords(recordIds,'Event',ignoreFields);
                CDCDataExportUtility.addRecordToCatcher(recordIds);
            }
		}
	*/
		//Applicant Prioritization, only when applicant gets created
		 system.debug('eventList---from ICID-'+ev.ICID__c);
		if(ev.ChangeEventHeader.getChangeType() == 'CREATE') {
			appIds.addAll(tempIds);
            /*	S-216773 Create record in MC journey via API - by Debasis*/
			//--------Start---------------------------
			if(ev.ICID__c=='csr_exp_group-b_04262021'){
				eventList.addAll(tempIds);
            	system.debug('eventList---from EventAsync-'+eventList);
			}
			if(eventList.size() > 0) {
				EventTriggerHandler eth = new EventTriggerHandler();
				eth.callMCJourneyController(eventList);
			}
			//----------End------------------------------
		}
	}
	
	//Handler for after insert( App Prioritization)
	if(appIds.size() > 0) {
		trgHandler.OnAfterInsertAsync(appIds);		
	}
	
	if (recordIds.size() > 0) {
       
		// for CDC flow
		if(CDC_Data_Export_Flow__c.getInstance('DataExport').Data_Export_All_Fields__c){
			List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Event');
			System.debug('ignoreFields::'+ignoreFields);
			CDCDataExportUtility.getRecords(recordIds,'Event',ignoreFields);
		}
	}
	
}