trigger FyreSyncTrigger on FyreSync__Requisition__c (after insert) {
    FyreSyncTriggerHandler handler= new FyreSyncTriggerHandler();
    if(Trigger.isInsert && Trigger.isAfter){
        handler.onAfterInsert(Trigger.new);        
    }
}