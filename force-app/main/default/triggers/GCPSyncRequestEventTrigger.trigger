trigger GCPSyncRequestEventTrigger on GCP_Sync_Request_Event__e (after insert) {
    List<GCP_Sync_Request_Event__e> changes = Trigger.new;

    List<String> deleteRecordJsons = new List<String>();
    List<String> delIds = new List<Id>();
    Set<String> idSet = new Set<String>();
    String sObjName;
    ConnectedLog.LogSack logSack = new ConnectedLog.LogSack();

    try {
        for (GCP_Sync_Request_Event__e gsre : changes) {
            System.debug(gsre.ObjectId__c);
            System.debug(gsre.IsDelete__c);
            System.debug(gsre.SObjectName__c);
            System.debug(gsre.ObjectJson__c);
            System.debug('==================================');
    
            if (gsre.IsDelete__c == true) {
                String delRegex = ',\\s*\\"IsDeleted\\"\\s*:\\s*false\\s*,';
                String deletedJson = gsre.ObjectJson__c.replaceFirst(delRegex, ',"isDeleted": true,');
                System.debug(deletedJson);
                deleteRecordJsons.add(deletedJson);
                delIds.add(gsre.ObjectId__c);
            } else {
                idSet.add(gsre.ObjectId__c);
            }
    
            // Assumption that all the bulkified records will be same SObject. The last one in array will be retained.
            sObjName = gsre.SObjectName__c;
        }
    
        if (CDC_Data_Export_Flow__c.getInstance('DataExport').Process_GCP_Sync_Request_Events__c) {
            if (deleteRecordJsons.size() > 0) {
                logSack.AddInformation('GCPSync/GCPSyncRequest/Deletes', 'GCPSyncRequestEventTrigger', 'trigger', 
                                    'Delete ' + sObjName + ' records ' + String.join(delIds, ','));
                CDCDataExportUtility.sendJsonsToPubSub(deleteRecordJsons);
            }
    
            if (idSet.size() > 0) {
                logSack.AddInformation('GCPSync/GCPSyncRequest/Updates', 'GCPSyncRequestEventTrigger', 'trigger', 
                                    'Update ' + sObjName + ' records ' + CDCDataExportUtility.joinIds(idSet));
                List<String> ignoreFields = DataExportUtility.ObjectMap.get(sObjName);
                CDCDataExportUtility.getRecords(idSet, sObjName, ignoreFields);
            }
        }
    } catch (Exception e) {
        logSack.AddException('GCPSync/GCPSyncRequest/Exception', 'GCPSyncRequestEventTrigger', 'trigger', e);
    } finally {
        logSack.Write();
    }
}