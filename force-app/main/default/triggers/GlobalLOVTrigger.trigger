trigger GlobalLOVTrigger on Global_LOV__c (after insert, after update,after delete) {
	
    GlobalLOVTriggerHandler handler = new GlobalLOVTriggerHandler();
    				if(Trigger.isInsert && Trigger.isAfter) {
                        //Handler for after insert
                        handler.OnAfterInsert(Trigger.new);
                    } else if(Trigger.isUpdate && Trigger.isAfter){
                        //Handler for after update trigger
                        handler.OnAfterUpdate(Trigger.new);
                    } else if (Trigger.isDelete && Trigger.isAfter) {
            			//Handler for after Delete trigger
            			handler.OnAfterDelete(Trigger.oldMap);
        			}
    
}