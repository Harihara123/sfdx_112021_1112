trigger JobTitle on Job_Title__c (after insert, after update,after delete)  {

  if(TriggerState.isActive('JobTitleTrigger')) {
         
            JobTitleTriggerHandler handler = new JobTitleTriggerHandler();
                if(Trigger.isInsert && Trigger.isAfter) {
                    //Handler for after insert
                    handler.OnAfterInsert(Trigger.new);
                } else if(Trigger.isUpdate && Trigger.isAfter){
                    //Handler for after update trigger
                    handler.OnAfterUpdate(Trigger.new);
                } else if (Trigger.isDelete && Trigger.isAfter) {           
					handler.OnAfterDelete(Trigger.oldMap);
				}
       } 

}