trigger JobTitleChangeAsyncTrigger on Job_Title__ChangeEvent (after insert) {
List<Job_Title__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 

    //Get all record Ids for this change and add it to a set for further processing
	for (Job_Title__ChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
       // recordIds = new Set<String> ();
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
	}
	//Handler for after insert
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
			if (recordIds.size() > 0) {
		
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Job_Title__c');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Job_Title__c',ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'JobTitleChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Job_Title__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		}
	} catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'JobTitleChangeAsyncTrigger', 'triggerBody', ex);
	}

}