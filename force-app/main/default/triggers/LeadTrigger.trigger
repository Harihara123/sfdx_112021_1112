trigger LeadTrigger on Lead (after insert, after update) {

  if(TriggerState.isActive('LeadTrigger')) {
        LeadTriggerHandler handler = new LeadTriggerHandler();
      
      if(Trigger.isInsert && Trigger.isAfter){
        //handler for after insert
        handler.onAfterInsert(Trigger.new);
        if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
            System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
            et4ae5.triggerUtility.automate('Lead');
            }
        } else if(Trigger.isUpdate && Trigger.isAfter){
        //handler for after update
        handler.OnAfterUpdate(Trigger.oldMap,Trigger.newMap);
        if ((label.SkipTriggeredSend != 'True') && (UserInfo.getProfileId().substring(0,15) != Label.Profile_System_Integration)){
            System.debug('MARKETING CLOUD TRIGGER UTILITY CALLED ----------------------------------------------------------------------');
            et4ae5.triggerUtility.automate('Lead');
            }
        }
      
      
      
  }

}