trigger LogItemTrigger on Log_Item__e (after insert) {
system.debug('in trigger');
    LogItemTriggerHandler lith = new LogItemTriggerHandler();
    lith.OnAfterInsert(trigger.new);
/*    
    Integer seqCtr = 1;
    List<Log_Item_bo__b> bo_items = new List<Log_Item_bo__b>();
        
    for (Log_Item__e li : Trigger.new) {                
        Boolean isArray = (li.Payload__c.left(1) == '[') ? true : false;          
        Map<String, Object> limap;
        List<Object> liList;
        
        if (isArray) {
            liList = (List<Object>)JSON.deserializeUntyped(li.Payload__c);
            
            for (Integer ctr = 0; ctr < liList.size(); ctr++) {
                Log_Item_bo__b lib = new Log_Item_bo__b();
                System.debug('ctr = ' + ctr);
                Map<String, Object> mapItem = (Map<String, Object>)liList[ctr];
                
                lib.LogItemType__c = (String)mapItem.get('LogItemType');
                lib.LogTypeId__c = (String)mapItem.get('LogITypeId');
                lib.ExceptionOccured__c = String.valueOf((Boolean)mapItem.get('ExceptionOccured'));
                lib.ClassName__c = (String)mapItem.get('ClassName');
                lib.Method__c = (String)mapItem.get('Method');
                lib.RecordId__c = (String)mapItem.get('RecordId');
                lib.Message__c = (String)mapItem.get('Message');
                lib.TimeStamp__c = (DateTime)JSON.deserialize('"' + (String)mapItem.get('TimeStamp') + '"', DateTime.class);
                lib.UserId__c = (String)mapItem.get('UserId');
                lib.Duration__c = ((Long)mapItem.get('Duration'));
                lib.TransactionId__c = (String)mapItem.get('TransactionId');
                lib.CorrelationId__c = (String)mapItem.get('CorrelationId');
                lib.AdditionalData__c = JSON.Serialize(mapItem.get('AdditionalData'));
                lib.Sequence__c = seqCtr;
                
                bo_items.add(lib);
                seqCtr++;
            } 
            
        } else {
            Log_Item_bo__b lib = new Log_Item_bo__b();
            Map<String, Object> mapItem = (Map<String, Object>)JSON.deserializeUntyped(li.payload__c);
            
            lib.LogItemType__c = (String)mapItem.get('LogItemType');
            lib.LogTypeId__c = (String)mapItem.get('LogITypeId');
            lib.ExceptionOccured__c = String.valueOf((Boolean)mapItem.get('ExceptionOccured'));
            lib.ClassName__c = (String)mapItem.get('ClassName');
            lib.Method__c = (String)mapItem.get('Method');
            lib.RecordId__c = (String)mapItem.get('RecordId');
            lib.Message__c = (String)mapItem.get('Message');
            lib.TimeStamp__c = (DateTime)JSON.deserialize('"' + (String)mapItem.get('TimeStamp') + '"', DateTime.class);
            lib.UserId__c = (String)mapItem.get('UserId');
            lib.Duration__c = ((Long)mapItem.get('Duration'));
            lib.TransactionId__c = (String)mapItem.get('TransactionId');
            lib.CorrelationId__c = (String)mapItem.get('CorrelationId');
            lib.AdditionalData__c = JSON.Serialize(mapItem.get('AdditionalData'));
            lib.Sequence__c = seqCtr;
            
            bo_items.add(lib);
            seqCtr++;
        }
    }
    System.debug('bo_items count = ' + bo_items.size());
    Database.insertImmediate(bo_items);
*/
}