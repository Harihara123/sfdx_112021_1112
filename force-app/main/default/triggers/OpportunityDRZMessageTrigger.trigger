trigger OpportunityDRZMessageTrigger on OpportunityDRZMessage__e (after insert) {

if(TriggerState.isActive('DRZSwitch')) {
    List<MyOpptyEvent__c> msg = new List<MyOpptyEvent__c>();

// Get user Id for record owner. Replace username value with a valid value.
  //  User user = [SELECT User_Id__c FROM User WHERE Username='tmcwhorter@allegisgroup.com.digiredzon'];

    // Iterate through each notification.
    for (OpportunityDRZMessage__e event : Trigger.New) 
    {
        System.debug('OpportunityID: ' + event.opp_id__c);
        if (event.opp_id__c <> '') 
        {
            // Create Record to hold the event data.
           
        MyOpptyEvent__c rec = new MyOpptyEvent__c();

        // populate the record

           
            rec.Account_ID__c = event.AccountId__c;
            rec.AccountName__c = event.AccountName__c;
            rec.Can_Use_Approved_Sub_Vendor__c = event.Can_Use_Approved_Sub_Vendor__c;
            rec.Event_Created_Date__c = event.CreatedDate;
            rec.LastModifiedById__c = event.LastModifiedById__c;
            rec.LastModifiedDate__c = event.LastModifiedDate__c;
            rec.Event_Sub_Type__c = event.Event_Sub_Type__c;
            rec.Exclusive__c = event.Is_Exclusive__c;
            rec.Is_Red_Zone__c = event.Is_Red_Zone__c;
            rec.Job_Description__c = event.Job_Description__c;
            rec.JobTitle__c= event.JobTitle__c;
            rec.Organization_Office__c = event.Organization_Office__c;
            rec.Open_Positions__c = event.Open_Positions__c;
            rec.Opportunity_Num__c = event.Opportunity_Num__c;
            rec.opp_id__c = event.opp_id__c;
            rec.Owner_Id__c = event.OwnerId__c;
            rec.Owner_Name__c = event.Owner_Name__c;
            rec.Replay_ID__c = event.ReplayId;
            rec.Req_Division__c = event.Req_Division__c;
            rec.Req_Job_Code__c = event.Req_Job_Code__c;
            rec.Req_Segment__c = event.Req_Segment__c;
            rec.Stage_Name__c = event.Stage_Name__c;
            rec.Status__c = event.Status__c;
            rec.Total_Positions__c = event.Total_Positions__c;
            rec.Total_Filled__c = event.Total_Filled__c;
            rec.CreatedDate__c = event.CreatedDate__c;
            rec.CloseDate__c = event.CloseDate__c;
            rec.Total_Lost__c = event.Total_Lost__c;
            rec.Total_Washed__c = event.Total_Washed__c;
            rec.Total_Filled__c = event.Total_Filled__c;
            rec.Hiring_Manager__c = event.Hiring_Manager__c;
            rec.BoardId__c = event.BoardId__c;
            rec.Req_Interview_Date__c = event.Expected_Interview_Date__c;
            rec.Req_Qualifying_Stage__c = event.Qualifying_Stage__c;
            rec.Start_Date__c =  event.Start_Date__c;
            rec.Immediate_Start__c= event.Immediate_Start__c;
            rec.Skill_Specialty__c = event.Skill_Specialty__c; 
            rec.Is_Sync_DRZ__c = event.Is_Sync_DRZ__c;
            rec.Sync_DRZ_User__c = event.Sync_DRZ_User__c; 
            rec.OpportunityTeamJson__c = event.OpportunityTeamJson__c;
            rec.Delivery_Center_Detailed__c = event.Delivery_Center_Detailed__c;
            rec.Team__c = event.Team__c;
			rec.Req_Score_Card__c = event.Req_Score_Card__c;
            // Set rec owner ID so it is not set to the Automated Process entity.
            //rec.OwnerId = user.User_Id__c;
            msg.add(rec);
        }
    }

  // Insert all records in the list.
    if (msg.size() > 0) 
    {
        insert msg;
    }
    }
}