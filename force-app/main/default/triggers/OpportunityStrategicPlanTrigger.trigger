trigger OpportunityStrategicPlanTrigger on Opportunity_Strategic_Initiative__c (before insert,after insert, after delete)
{
    if(Trigger.isInsert && Trigger.isBefore)
    {
        OpportunityStrategicPlanTriggerHandler.OnBeforeInsert(Trigger.new);
    }
    
    if(Trigger.isInsert && Trigger.isAfter)
    {
        OpportunityStrategicPlanTriggerHandler.OnAfterInsert(Trigger.new);
    }
    
    if(Trigger.isDelete && Trigger.isAfter)
    {
        OpportunityStrategicPlanTriggerHandler.OnAfterDelete(Trigger.old);
    }
}