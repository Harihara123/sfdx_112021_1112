trigger OpportunityTrigger on Opportunity (before insert, before update,after insert,after update,before delete,after delete,after undelete) {

 
 if(TriggerState.isActive('OpportunityTrigger')) {
        OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
 
       if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
      }  
   }