trigger OrderChangeAsyncTrigger on OrderChangeEvent (after insert) {
List<OrderChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 
    OrderTriggerHandler handler = new OrderTriggerHandler();
    Map<Id,Order> deletedRecords = new Map<Id,Order>();

    //Get all record Ids for this change and add it to a set for further processing
	for (OrderChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
		
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
        if (ev.ChangeEventHeader.getChangeType() == 'DELETE') {
            for(String recId : tempIds){
                deletedRecords.put(Id.valueOf(recId), null);				
            }			
        }
	}
	//Handler for after insert
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
		if (recordIds.size() > 0) {
       
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Order');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Order',ignoreFields);
				// CDCDataExportUtility.addRecordToCatcher(recordIds);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'OrderChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Order records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		}
    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'OrderChangeAsyncTrigger', 'triggerBody', ex);
    }
    
   /* // Handle after delete
    if(deletedRecords.size()>0){
        deletedRecords = new Map<Id,Order>([Select Id,ShipToContactId from Order where Id in:deletedRecords.keyset() ALL ROWS]);
		System.debug('deletedRecords Orders ' + deletedRecords);
        handler.OnAfterDelete(deletedRecords);
    }*/
	
}