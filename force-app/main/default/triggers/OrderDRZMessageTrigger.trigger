trigger OrderDRZMessageTrigger on OrderDRZMessage__e (after insert) {

if(TriggerState.isActive('DRZSwitch')) {
 
List<MyORderEvent__c> msg = new List<MyOrderEvent__c>();

// Get user Id for record owner. Replace username value with a valid value.
    //User user = [SELECT User_Id__c FROM User WHERE Username='tmcwhorter@allegisgroup.com.digiredzon'];

    // Iterate through each notification.
    for (OrderDRZMessage__e event : Trigger.New) 
    {
        System.debug('OpportunityID: ' + event.ReplayId);
        if (event.ReplayId <> '') 
        {
            // Create Record to hold the event data.
           
        MyOrderEvent__c rec = new MyOrderEvent__c();

        // populate the record

            //rec.CreatedBy__c = event.CreatedBy;
            rec.CreatedDate__c = event.CreatedDate;
            rec.ReplayId__c = event.ReplayId;
            rec.Event_Sub_Type__c = event.Event_Sub_Type__c;
            rec.LastModifiedDate__c = event.LastModifiedDate__c;
            rec.OpportunityId__c = event.OpportunityId__c;
            rec.OrderId__c = event.OrderId__c;
            rec.OwnerId__c = event.OwnerId__c;
            rec.Status__c = event.Status__c;
            rec.TalentName__c = event.TalentName__c;
            rec.CreatedDate__c = event.CreatedDate__c;
            rec.ReqCreatedDate__c = event.CreatedDate__c;
            rec.ShipToContactId__c = event.ShipToContactId__c;
            rec.BoardId__c = event.BoardId__c;
            rec.Is_Sync_DRZ__c = event.Is_Sync_DRZ__c;
            rec.Sync_DRZ_User__c = event.Sync_DRZ_User__c; 
            rec.Last_Moved_By__c = event.LastModifiedById__c;
            rec.Interview_Details_JSON__c = event.Interview_Details_JSON__c;
            rec.Delivery_Center__c = event.Delivery_Center__c;
            // Set rec owner ID so it is not set to the Automated Process entity.
           // rec.OwnerId = user.User_Id__c;
            msg.add(rec);
        }
    }

  // Insert all records in the list.
    if (msg.size() > 0) 
    {
        insert msg;
    }
    }
}