trigger Pipeline on Pipeline__c (after insert, after update,after delete)  {

  if(TriggerState.isActive('PipelineTrigger')) {
         
            PipelineTriggerHandler handler = new PipelineTriggerHandler();
                if(Trigger.isInsert && Trigger.isAfter) {
                    //Handler for after insert
                    handler.OnAfterInsert(Trigger.new);
                } else if(Trigger.isUpdate && Trigger.isAfter){
                    //Handler for after update trigger
                    handler.OnAfterUpdate(Trigger.new);
                } else if (Trigger.isDelete && Trigger.isAfter) {           
					handler.OnAfterDelete(Trigger.oldMap);
				}
       } 

}