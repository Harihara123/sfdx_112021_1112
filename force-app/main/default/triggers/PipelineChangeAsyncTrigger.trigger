trigger PipelineChangeAsyncTrigger on Pipeline__ChangeEvent (after insert) {
List<Pipeline__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 

    //Get all record Ids for this change and add it to a set for further processing
	for (Pipeline__ChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
      //  recordIds = new Set<String> ();
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
	}
	//Handler for after insert
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
		if (recordIds.size() > 0) {
       
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Pipeline__c');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Pipeline__c',ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'PipelineChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Pipeline__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		}
    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'PipelineChangeAsyncTrigger', 'triggerBody', ex);
    }

}