trigger Poller on Poller__e (after insert)  { 
	PollerTriggerHandler handler = new PollerTriggerHandler();
	handler.polling(Trigger.new);
}