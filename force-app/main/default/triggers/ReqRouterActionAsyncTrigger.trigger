trigger ReqRouterActionAsyncTrigger on Req_Router_Action__ChangeEvent (after insert) {
    List<Req_Router_Action__ChangeEvent> changes = Trigger.new;

    Set<String> recordIds = new Set<String> ();

    try {
        for (Req_Router_Action__ChangeEvent ev : changes) {
			// System.debug('ChangeEvent::::'+ev);
			EventBus.ChangeEventHeader ceHeader = ev.ChangeEventHeader;
			// System.debug('Change event type ' + ceHeader.changetype);
			recordIds.addAll(ev.ChangeEventHeader.getRecordIds());
		}

		CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
        if (recordIds.size() > 0) {
			// for CDC flow
			if(config.Data_Export_All_Fields__c) {
				List<String> ignoreFields = DataExportUtility.ObjectMap.get('Req_Router_Action__c');
				CDCDataExportUtility.getRecords(recordIds, 'Req_Router_Action__c', ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'ReqRouterActionAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Req_Router_Action__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		} 

    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'ReqRouterActionAsyncTrigger', 'triggerBody', ex);
    }
}