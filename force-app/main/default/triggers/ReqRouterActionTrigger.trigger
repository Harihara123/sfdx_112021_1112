trigger ReqRouterActionTrigger on Req_Router_Action__c (before insert, after insert, before update, after update,
                                           before delete, after delete, after undelete) {
	if(TriggerState.isActive('ReqRouterActionTrigger')) {
        ReqRouterActionTriggerHandler handler = new ReqRouterActionTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.syncToGCP(Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.syncToGCP(Trigger.new);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for after Delete trigger
            handler.syncToGCP(Trigger.old);
        } else if (Trigger.isUndelete && Trigger.isAfter) {
            //Handler for after Undelete trigger
        }
    } 
}