/*******************************************************************
Name        : ReqSkillsTrigger
Created By  : Preetham Uppu
Date        : 26 Jan 2017
Story/Task  : CRMX 2104
Purpose     : This trigger is invoked for all contexts
              and delegates control to ReqSkillsTriggerHandler
********************************************************************/
trigger ReqSkillsTrigger on Req_Skill__c (before insert, after insert, before update, after update,
                                before delete, after delete, after undelete) {
                                
      if(TriggerState.isActive('ReqSkillsTrigger')) {
        ReqSkillsTriggerHandler handler = new ReqSkillsTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        }
    }

}