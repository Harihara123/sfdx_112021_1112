/***************************************************************************
Name        : ReqTeamMemberTrigger
Created By  : SRajoria (Appirio)
Date        : 21 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : This trigger is invoked for all contexts
              and delegates control to ReqTeamMemberTriggerHandler
*****************************************************************************/

trigger ReqTeamMemberTrigger on Req_Team_Member__c (before insert, after insert, before update, after update,
                                                    before delete, after delete, after undelete) {

	if(TriggerState.isActive('ReqTeamMemberTrigger')) {
        ReqTeamMemberTriggerHandler handler = new ReqTeamMemberTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap,Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for after Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        } else if (Trigger.isUndelete && Trigger.isAfter) {
            //Handler for after Undelete trigger
            handler.OnAfterUndelete(Trigger.newMap);
        }
    }
}