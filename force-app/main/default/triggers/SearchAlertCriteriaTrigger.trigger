trigger SearchAlertCriteriaTrigger on Search_Alert_Criteria__c (after insert, after update, after delete)  {

	if(TriggerState.isActive('SearchAlertCriteriaTrigger')) {
            SearchAlertCriteriaTriggerHandler handler = new SearchAlertCriteriaTriggerHandler();
                if(Trigger.isInsert && Trigger.isAfter) {
                    //Handler for after insert
                    handler.OnAfterInsert(Trigger.new);
                } else if(Trigger.isUpdate && Trigger.isAfter){
                    //Handler for after update trigger
                    handler.OnAfterUpdate(Trigger.new);
                } else if (Trigger.isDelete && Trigger.isAfter) {           
					handler.OnAfterDelete(Trigger.oldMap);
				}
       } 

}