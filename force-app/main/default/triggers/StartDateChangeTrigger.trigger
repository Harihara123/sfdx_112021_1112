trigger StartDateChangeTrigger on StartDateEvent__e (after insert) {
    System.debug('******************inside platform event trigger********');
    List<Case> caseListtoUpdate   = new List<Case>();
    List<Event> eventListToUpdate = new List<Event>();
    List<Order> OrderListToUpdate = new List<Order>();
    
	Map<Id,StartDateEvent__e> getCaseIdFromOrder=new Map<Id,StartDateEvent__e>();
    Map<Id,StartDateEvent__e> getCastIdFromEvent=new Map<Id,StartDateEvent__e>();
    Map<Id,StartDateEvent__e> getEventIdFromOrder=new Map<Id,StartDateEvent__e>();
    Map<Id,StartDateEvent__e> getEventIdFromCase=new Map<Id,StartDateEvent__e>();
    Map<Id,StartDateEvent__e> getOrderIdFromCase=new Map<Id,StartDateEvent__e>();
    Map<Id,StartDateEvent__e> getOrderIdFromEvent=new Map<Id,StartDateEvent__e>();
    
    for(StartDateEvent__e PlatformEvent : Trigger.new){
        System.debug('event in loop:'+PlatformEvent);
        System.debug('event Sobjecttype:'+PlatformEvent.SObjectType__c);
        Id recordId = Id.valueOf(PlatformEvent.OriginationID__c);
        if(PlatformEvent.SObjectType__c == 'Order'){           
            System.debug('***************recordId****'+recordId);
            //Update related Case
            getCaseIdFromOrder.put(recordId,PlatformEvent);
            //Update related Events
            getEventIdFromOrder.put(recordId,PlatformEvent);            
        }
        if(PlatformEvent.SObjectType__c == 'Case'){            
            system.debug('Record ID : '+recordId);
            //Update related Submittal
            getOrderIdFromCase.put(recordId,PlatformEvent);            
            //Update related Events
            getEventIdFromCase.put(recordId,PlatformEvent);            
        }
        if(PlatformEvent.SObjectType__c == 'Event'){            
            system.debug('Record ID : '+recordId);
            //Update related Case
            getCastIdFromEvent.put(recordId,PlatformEvent);
            //Update related Submittal
            getOrderIdFromEvent.put(recordId,PlatformEvent);
        }
    }
    Map<Id,Case> CaseIdsWithOrder=new Map<Id,Case>();
    Map<Id,Case> CaseIdsWithEvents=new Map<Id,Case>();
    Map<Id,Event> EventIdsWithOrder=new Map<Id,Event>();
    Map<Id,Event> EventIdsWithCase=new Map<Id,Event>();
    Map<Id,Order> OrderIdsWithCase=new Map<Id,Order>();
    Map<Id,Order> OrderIdsWithEvent=new Map<Id,Order>();
    
    if(getCaseIdFromOrder.size()>0){
       CaseIdsWithOrder=StartDateChangeTriggerHandler.getCaseIdListNew(getCaseIdFromOrder.keySet(),'Order');
    }
    if(getCastIdFromEvent.size()>0){
       CaseIdsWithEvents=StartDateChangeTriggerHandler.getCaseIdListNew(getCastIdFromEvent.keySet(),'Event');
    }
    if(getEventIdFromOrder.size()>0){
       EventIdsWithOrder=StartDateChangeTriggerHandler.getEventIdListNew(getEventIdFromOrder.keySet(),'Order');
    }
    if(getEventIdFromCase.size()>0){
       EventIdsWithCase=StartDateChangeTriggerHandler.getEventIdListNew(getEventIdFromCase.keySet(),'Case');
    }
    if(getOrderIdFromCase.size()>0){
       OrderIdsWithCase=StartDateChangeTriggerHandler.getOrderIdListNew(getOrderIdFromCase.keySet(),'Case');
    }
    if(getOrderIdFromEvent.size()>0){
       OrderIdsWithEvent=StartDateChangeTriggerHandler.getOrderIdListNew(getOrderIdFromEvent.keySet(),'Event');
    }
    
    for(StartDateEvent__e PlatformEvent : Trigger.new){        
        Id recordId = Id.valueOf(PlatformEvent.OriginationID__c);
        if(PlatformEvent.SObjectType__c == 'Order'){
            System.debug('***************recordId****'+recordId);
            //Update related Case
            caseListtoUpdate.addAll(StartDateChangeTriggerHandler.updateCase(PlatformEvent,CaseIdsWithOrder.get(recordId)));
            //Update related Events
            eventListToUpdate.addAll(StartDateChangeTriggerHandler.updateEvent(PlatformEvent,EventIdsWithOrder.get(recordId)));
        }
        if(PlatformEvent.SObjectType__c == 'Case'){
            system.debug('Record ID : '+recordId);
            //Update related Submittal
            OrderListToUpdate.addAll(StartDateChangeTriggerHandler.updateSubmittal(PlatformEvent,OrderIdsWithCase.get(recordId)));    
            //Update related Events
            eventListToUpdate.addAll(StartDateChangeTriggerHandler.updateEvent(PlatformEvent,EventIdsWithCase.get(recordId)));
        }
        if(PlatformEvent.SObjectType__c == 'Event'){
            system.debug('Record ID : '+recordId);
            //Update related Case
            caseListtoUpdate.addAll(StartDateChangeTriggerHandler.updateCase(PlatformEvent,CaseIdsWithEvents.get(recordId)));
            //Update related Submittal
            OrderListToUpdate.addAll(StartDateChangeTriggerHandler.updateSubmittal(PlatformEvent,OrderIdsWithEvent.get(recordId)));
        }
    }    
    if(caseListtoUpdate.size()>0){
        try{
            //update caseListtoUpdate ;
            List<Database.SaveResult> updateCaseResults = database.update(caseListtoUpdate);
        }catch(exception ex){
            System.Debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }    	
    }
    if(eventListToUpdate.size()>0){
        try{
            List<Event> uniqueEventList = new List<Event>(new Set<Event>(eventListToUpdate));
    		//update uniqueEventList;
            List<Database.SaveResult> updateEventResults = database.update(uniqueEventList);
        }catch(exception ex){
            System.Debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }
        
    }
    if(OrderListToUpdate.size()>0){
        try{
            //update OrderListToUpdate;
            List<Database.SaveResult> updateOrderResults = database.update(OrderListToUpdate);
        }catch(exception ex){
            System.Debug('---error--' + ex.getLineNumber() + '--' + ex.getMessage());
        }    	
    }
}