/*******************************************************************
Name        : Strat_Init_Memb_Trigger
Created By  : JFreese (Appirio)
Date        : 23 Feb 2015
Story/Task  : S-291651 / T360150
Purpose     : This trigger is invoked for all contexts
              and delegates control to Strat_Init_Memb_TriggerHandler
********************************************************************/
trigger Strat_Init_Memb_Trigger on Strategic_Initiative_Member__c (before insert, after insert, before update, after update, before delete, after delete) {

    if(TriggerState.isActive('Strat_Init_Memb_Trigger')) {
        Strat_Init_Memb_TriggerHandler handler = new Strat_Init_Memb_TriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }
}