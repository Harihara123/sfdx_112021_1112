/*******************************************************************
Name        : StrategicPlanTrigger
Created By  : Nidish Rekulapalli
Date        : 10 July 2015
Purpose     : This trigger is invoked for all contexts
              and delegates control to StrategicPlanTriggerHandler
********************************************************************/
trigger StrategicPlanTrigger on Strategic_Initiative__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
  if(TriggerState.isActive('StrategicPlanTrigger')) {
        StrategicPlanTriggerHandler  handler = new StrategicPlanTriggerHandler ();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }
}