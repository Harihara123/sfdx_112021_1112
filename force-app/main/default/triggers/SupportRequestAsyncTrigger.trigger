trigger SupportRequestAsyncTrigger on Support_Request__ChangeEvent (after insert) {
    List<Support_Request__ChangeEvent> changes = Trigger.new;

    Set<String> recordIds = new Set<String> ();

    try {
        for (Support_Request__ChangeEvent ev : changes) {
			// System.debug('ChangeEvent::::'+ev);
			EventBus.ChangeEventHeader ceHeader = ev.ChangeEventHeader;
			// System.debug('Change event type ' + ceHeader.changetype);
			recordIds.addAll(ev.ChangeEventHeader.getRecordIds());
		}

		CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
        if (recordIds.size() > 0) {
			// for CDC flow
			if(config.Data_Export_All_Fields__c) {
				List<String> ignoreFields = DataExportUtility.ObjectMap.get('Support_Request__c');
				CDCDataExportUtility.getRecords(recordIds, 'Support_Request__c', ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'SupportRequestAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Support_Request__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		} 

    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'SupportRequestAsyncTrigger', 'triggerBody', ex);
    }
}