/*******************************************************************
Name        : SupportRequestTrigger
Created By  : Nidish Rekulapalli
Date        : 21 March 2016
Story/Task  : CRMX-642
Purpose     : Support Request Creation
********************************************************************/
trigger SupportRequestTrigger on Support_Request__c (before insert, after insert, before update, after update,
                                           before delete, after delete, after undelete) {
      if(TriggerState.isActive('SupportRequestTrigger')) {
        SupportRequestTriggerHandler handler = new SupportRequestTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.new,Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for after Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        } else if (Trigger.isUndelete && Trigger.isAfter) {
            //Handler for after Undelete trigger
            handler.OnAfterUndelete(Trigger.newMap);
        }
    }                                       

}