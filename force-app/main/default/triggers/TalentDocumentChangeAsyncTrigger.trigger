trigger TalentDocumentChangeAsyncTrigger on Talent_Document__ChangeEvent (after insert) {
List<Talent_Document__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 

    //Get all record Ids for this change and add it to a set for further processing
	for (Talent_Document__ChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
     //   recordIds = new Set<String> ();
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
    }

    List<Task> newTasks = new List<Task>();
    List<User> HSUser = [SELECT Id FROM User WHERE Name = 'Hiringsolved Integration' LIMIT 1];
    static String HiringSolvedId = String.ValueOf(HSuser.get(0).Id);
    
    List<Id> accIds = new List<Id>();
    for(Talent_Document__ChangeEvent tdc : changes){
        accIds.add(tdc.Talent__c);
    }
    List<Contact> cons = [SELECT Id, accountId FROM Contact WHERE accountId =: accIds ORDER BY LastModifiedDate desc];
    List<Id> conId = new List<Id>();
    
    for(Talent_Document__ChangeEvent change : changes){
        EventBus.ChangeEventHeader header = change.ChangeEventHeader;
        if(header.changetype == 'CREATE'){
            if(change.Document_Type__c == 'Social Profile'){
                Task t = new Task();
                for(Contact con : cons){
                    if(con.accountId == change.Talent__c)
                        conId.add(con.Id);
                }
                t.Status = 'Completed';
                Date d = date.newinstance(change.CreatedDate.year(), change.CreatedDate.month(), change.CreatedDate.day());
                t.ActivityDate = d;
                t.Type = 'Other';
                t.CreatedById = HiringSolvedId;
                t.OwnerId = HiringSolvedId;
                t.Activity_Type__c = 'Other';
                t.Subject = 'Added new Hiring Solved Social Profile Document';
                t.Priority = 'Normal';
                t.WhatId = change.Talent__c;
                t.WhoId = conId[0];
                newTasks.add(t);
            }
        }
    }
    if(newTasks != null && !newTasks.isEmpty()) {
            insert newTasks;
    }
    CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    //Handler for after insert
    try {
        if (recordIds.size() > 0) {
        
            // for CDC flow
            if(config.Data_Export_All_Fields__c){
                List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Talent_Document__c');
                System.debug('ignoreFields::'+ignoreFields);
                CDCDataExportUtility.getRecords(recordIds,'Talent_Document__c',ignoreFields);
                if (config.Enable_Trace_Logging__c) {
                    ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'TalentDocumentChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Talent_Document__c records: ' + CDCDataExportUtility.joinIds(recordIds));
                }
            }
        }
    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'TalentDocumentChangeAsyncTrigger', 'triggerBody', ex);
    }

}