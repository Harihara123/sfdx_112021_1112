trigger TalentExperienceAsyncChangeEvent on Talent_Experience__ChangeEvent (after insert) {
List<Talent_Experience__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 

    //Get all record Ids for this change and add it to a set for further processing
	for (Talent_Experience__ChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
       // recordIds = new Set<String> (); 
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
	}
	//Handler for after insert
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
		if (recordIds.size() > 0) {
       
			// for CDC flow
			if(config.Data_Export_All_Fields__c){
				List<String> ignoreFields  = DataExportUtility.ObjectMap.get('Talent_Experience__c');
				System.debug('ignoreFields::'+ignoreFields);
				CDCDataExportUtility.getRecords(recordIds,'Talent_Experience__c',ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'TalentExperienceAsyncChangeEvent', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Talent_Experience__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		}
    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'TalentExperienceAsyncChangeEvent', 'triggerBody', ex);
    }

}