trigger TalentFitmentAsyncTrigger on TalentFitment__ChangeEvent (after insert)  { 

	List<TalentFitment__ChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 

    //Get all record Ids for this change and add it to a set for further processing
	for (TalentFitment__ChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);

        List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
    }
    CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
        if (recordIds.size() > 0) {
       
            // for CDC flow
            if(config.Data_Export_All_Fields__c){
                List<String> ignoreFields  = DataExportUtility.ObjectMap.get('TalentFitment__c');
                System.debug('ignoreFields::'+ignoreFields);
                CDCDataExportUtility.getRecords(recordIds,'TalentFitment__c',ignoreFields);
                if (config.Enable_Trace_Logging__c) {
				    ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'TalentFitmentAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' TalentFitment__c records: ' + CDCDataExportUtility.joinIds(recordIds));
                }
            }
		}
	} catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'TalentFitmentAsyncTrigger', 'triggerBody', ex);
    }

}