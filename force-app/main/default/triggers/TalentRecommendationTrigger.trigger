/*******************************************************************
Name        : TalentRecommendationTrigger
Created By  : Ajay Panachickal
Date        : 6/8/2016
Story/Task  : ATS-1288
Purpose     : This trigger is invoked for all contexts
              and delegates control to TalentRecommendationTriggerHandler
********************************************************************/
trigger TalentRecommendationTrigger on Talent_Recommendation__c (before insert, after insert, before update, after update,
	                         							before delete, after delete) {
    if(TriggerState.isActive('TalentRecommendationTrigger')) {
        TalentRecommendationTriggerHandler handler = new TalentRecommendationTriggerHandler();

        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete && Trigger.isBefore) {
            //Handler for before Delete trigger
            handler.OnBeforeDelete(Trigger.oldMap);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }

}