trigger TalentWorkHistoryTrigger on Talent_Work_History__c (after insert, after update)  { 


        if(TriggerState.isActive('TalentWorkHistoryTrigger')) {
         

            TalentWorkHistoryTriggerHandler handler = new TalentWorkHistoryTriggerHandler();
              
                if(Trigger.isInsert && Trigger.isAfter) {
                    //Handler for after insert
                    handler.OnAfterInsert(Trigger.new);
                } else if(Trigger.isUpdate && Trigger.isAfter){
                    //Handler for after update trigger
                    handler.OnAfterUpdate(Trigger.new);
                } else if (Trigger.isDelete && Trigger.isAfter) {
                    handler.OnAfterDelete(Trigger.oldMap); 
                }
       }

}