/*********************************************************************************************
Name        : Talent_Document_Trigger
Created By  : Bhushan Adhikari
Date        : 17 Aug 2016
Story/Task  : COM-339/COM-1543
Purpose     : This trigger is invoked on after update and delegates control 
                to TC_TalentDocumentTriggerHandler.
******************************************************************************************/
trigger Talent_Document_Trigger on Talent_Document__c (after update, before insert, after delete) {
    if(TriggerState.isActive('Talent_Document_Trigger')) {
        TC_TalentDocumentTriggerHandler handler = new TC_TalentDocumentTriggerHandler();
        if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        }
        if(Trigger.isInsert && Trigger.isBefore){
            //Handler for after update trigger
            handler.OnBeforeInsert(Trigger.new);
        }
        else if (Trigger.isDelete && Trigger.isAfter) {
            //Handler for before Delete trigger
            handler.OnAfterDelete(Trigger.oldMap);
        }
    }
}