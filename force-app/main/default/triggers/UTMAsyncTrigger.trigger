trigger UTMAsyncTrigger on Unified_Merge__ChangeEvent (after insert) {
    List<Unified_Merge__ChangeEvent> changes = Trigger.new;

    Set<String> recordIds = new Set<String> ();

    try {
        for (Unified_Merge__ChangeEvent ev : changes) {
			// System.debug('ChangeEvent::::'+ev);
			EventBus.ChangeEventHeader ceHeader = ev.ChangeEventHeader;
			// System.debug('Change event type ' + ceHeader.changetype);
			recordIds.addAll(ev.ChangeEventHeader.getRecordIds());
		}
		CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
        if (recordIds.size() > 0) {
			// for CDC flow
			if(config.Data_Export_All_Fields__c) {
				List<String> ignoreFields = DataExportUtility.ObjectMap.get('Unified_Merge__c');
				CDCDataExportUtility.getRecords(recordIds, 'Unified_Merge__c', ignoreFields);
				if (config.Enable_Trace_Logging__c) {
					ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'UTMAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' Unified_Merge__c records: ' + CDCDataExportUtility.joinIds(recordIds));
				}
			}
		} 

    } catch (Exception ex) {
        ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'UTMAsyncTrigger', 'triggerBody', ex);
    }
}