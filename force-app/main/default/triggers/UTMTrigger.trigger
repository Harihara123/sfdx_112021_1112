trigger UTMTrigger on Unified_Merge__c (before insert, after insert, before update, after update,
                               before delete, after delete, after undelete) {
	
	
	if(TriggerState.isActive('UTMTrigger')) {
        UTMTriggerHandler handler = new UTMTriggerHandler();

        if(Trigger.isInsert && Trigger.isAfter){
            handler.publishUpdates(Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            handler.publishUpdates( Trigger.new);
        } else if (Trigger.isDelete && Trigger.isAfter) {
            handler.publishUpdates(Trigger.old);		
        } else if (Trigger.isUndelete && Trigger.isAfter) {
            handler.publishUpdates(Trigger.new);
        }
    }
}