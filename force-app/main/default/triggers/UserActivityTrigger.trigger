trigger UserActivityTrigger on User_Activity__c (after insert, after update)  {


       if(TriggerState.isActive('UserActivityTrigger')) {
         

            UserActivityTriggerHandler handler = new UserActivityTriggerHandler();
                if(Trigger.isInsert && Trigger.isAfter) {
                    //Handler for after insert
                    handler.OnAfterInsert(Trigger.new);
                } else if(Trigger.isUpdate && Trigger.isAfter){
                    //Handler for after update trigger
                    handler.OnAfterUpdate(Trigger.new);
                } 
       } 

  }