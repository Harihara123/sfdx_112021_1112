trigger UserChangeAsyncTrigger on UserChangeEvent (after insert) {
	List<UserChangeEvent> changes = Trigger.new;
	Set<String> recordIds = new Set<String> (); 
    List<Id> idList = new List<Id>();

    //Get all record Ids for this change and add it to a set for further processing
	for (UserChangeEvent ev : changes) {
		System.debug('ChangeEvent::::'+ev);
      //  recordIds = new Set<String> ();
		List<String> tempIds = ev.ChangeEventHeader.getRecordIds();
		recordIds.addAll(tempIds);
        idList.addAll(tempIds);
	}
	CDC_Data_Export_Flow__c config = CDC_Data_Export_Flow__c.getInstance('DataExport');
    try {
        if (recordIds.size() > 0) {
       
            // for CDC flow
            if(config.Data_Export_All_Fields__c){
                List<String> ignoreFields  = DataExportUtility.ObjectMap.get('User');
                System.debug('ignoreFields::'+ignoreFields);
                CDCDataExportUtility.getRecords(recordIds,'User',ignoreFields);
                if (config.Enable_Trace_Logging__c) {
				    ConnectedLog.LogInformation('GCPSync/AsyncTrigger', 'UserChangeAsyncTrigger', 'triggerBody', 
						'CDC Sync triggered ' + recordIds.size() + ' User records: ' + CDCDataExportUtility.joinIds(recordIds));
                }
            }
		}

        if (idList.size() > 0 
                && !Test.isRunningTest() 
                && Connected_Data_Export_Switch__c.getInstance('DataExport').Data_Extraction_Insert_Flag__c ) {
            PubSubBatchHandler.insertDataExtractionRecordByIds(idList, 'User');
            if (config.Enable_Trace_Logging__c) {
                ConnectedLog.LogInformation('GCPSync/AsyncTrigger/InsertDataExtraction', 'UserChangeAsyncTrigger', 'triggerBody', 
                    'Inserted ' + idList.size() + ' User records: ' + CDCDataExportUtility.joinIds(idList));
            }
        }
    } catch (Exception ex) {
		ConnectedLog.LogException('GCPSync/AsyncTrigger/Exception', 'UserChangeAsyncTrigger', 'triggerBody', ex);
    }
}