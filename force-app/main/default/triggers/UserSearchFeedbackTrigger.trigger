/*******************************************************************
Name        : UserSearchFeedbackTrigger
Created By  : apanachi
Date        : 29 May 2019
Story/Task  : S-134060 (version 1)
Purpose     : This trigger is invoked for after insert and update
              and delegates control to UserSearchFeedbackTriggerHandler
********************************************************************/
trigger UserSearchFeedbackTrigger on User_Search_Feedback__c (after insert, after update) {
	if(TriggerState.isActive('UserSearchFeedbackTrigger')) {
        UserSearchFeedbackTriggerHandler handler = new UserSearchFeedbackTriggerHandler();

		if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        } 
    }
}