/***************************************************************************
Name        : UserTrigger
Created By  : Vivek Ojha (Appirio)
Date        : 24 Feb 2015
Story/Task  : S-291651 / T-363984
Purpose     : This trigger is invoked for all contexts
              and delegates control to UserTriggerHandler
*****************************************************************************/
trigger UserTrigger on User (before insert, after insert, before update, after update,
                             before delete, after delete, after undelete) {
BypassLogic bl=new BypassLogic();
  if(TriggerState.isActive('UserTrigger')) {
        UserTriggerHandler handler = new UserTriggerHandler();
      if(Trigger.isInsert && Trigger.isBefore){

            //Handler for before insert
            handler.OnBeforeInsert(Trigger.new);
        } else if(Trigger.isInsert && Trigger.isAfter){
            //Handler for after insert
            handler.OnAfterInsert(Trigger.newMap,Trigger.new);
        } else if(Trigger.isUpdate && Trigger.isBefore){
            //Handler for before update trigger
            handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        } else if(Trigger.isUpdate && Trigger.isAfter){
            //Handler for after update trigger
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        }
   }

}