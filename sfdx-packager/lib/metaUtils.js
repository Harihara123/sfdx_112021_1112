const xmlBuilder = require('xmlbuilder');
const fs = require('fs-extra');
const mkdirp = require('mkdirp');

/**
 * Mapping of file name to Metadata Definition
 */
// @todo -- finish out all the different metadata types
const metaMap = {
	'actionLinkGroupTemplates': 'ActionLinkGroupTemplate',
	'analyticSnapshots': 'AnalyticSnapshot',
	'classes': 'ApexClass',
	'components': 'ApexComponent',
	'pages': 'ApexPage',
	'triggers': 'ApexTrigger',
	'appMenus': 'AppMenu',
	'approvalProcesses': 'ApprovalProcess',
	'assignmentRules': 'AssignmentRules',
	'aura': 'AuraDefinitionBundle',
	'authproviders': 'AuthProvider',
	'autoResponseRules': 'AutoResponseRules',
	'callCenters': 'CallCenter',
	'certs': 'Certificate',
	'channelLayouts': 'ChannelLayout',
	'communities': 'Community',
	'connectedApps': 'ConnectedApp',
	'corsWhitelistOrigins': 'CorsWhitelistOrigin',
	'applications': 'CustomApplication',
	'customApplicationComponents': 'CustomApplicationComponent',
	'feedFilters': 'CustomFeedFilter',
	'labels': 'CustomLabels',
	'customMetadata': 'CustomMetadata',
	'objects': 'CustomObject',
	'objectTranslations': 'CustomObjectTranslation',
	'weblinks': 'CustomPageWebLink',
	'customPermissions': 'CustomPermission',
	'sites': 'CustomSite',
	'tabs': 'CustomTab',
	'dashboards': 'Dashboard',
	'datacategorygroups': 'DataCategoryGroup',
	'delegateGroups': 'DelegateGroup',
	'documents': 'Document',
	'email': 'EmailTemplate',
	'entitlementProcesses': 'EntitlementProcess',
	'entitlementTemplates': 'EntitlementTemplate',
	'escalationRules': 'EscalationRules',
	'dataSources': 'ExternalDataSource',
	'flexipages': 'FlexiPage',
	'flows': 'Flow',
	'flowDefinitions': 'FlowDefinition',
	'globalPicklists': 'GlobalPicklist',
	'groups': 'Group',
	'homePageComponents': 'HomePageComponent',
	'homePageLayouts': 'HomePageLayout',
	'installedPackages': 'InstalledPackage',
	'moderation': 'KeywordList',
	'layouts': 'Layout',
	'letterhead': 'Letterhead',
	'lwc': 'LightningComponentBundle',
	'managedTopics': 'ManagedTopics',
	'matchingRules': 'MatchingRules',
	'namedCredentials': 'NamedCredential',
	'networks': 'Network',
	'pathAssistants': 'PathAssistant',
	'permissionsets': 'PermissionSet',
	'cachePartitions': 'PlatformCachePartition',
	'portals': 'Portal',
	'postTemplates': 'PostTemplate',
	'profiles': 'Profile',
	'queues': 'Queue',
	'quickActions': 'QuickAction',
	'remoteSiteSettings': 'RemoteSiteSetting',
	'reports': 'Report',
	'reportTypes': 'ReportType',
	'roles': 'Role',
	'samlssoconfigs': 'SamlSsoConfig',
	'scontrols': 'Scontrol',
	'settings': 'Settings',
	'sharingRules': 'SharingRules',
	'sharingSets': 'SharingSet',
	'siteDotComSites': 'SiteDotCom',
	'staticresources': 'StaticResource',
	'synonymDictionaries': 'SynonymDictionary',
	'transactionSecurityPolicies': 'TransactionSecurityPolicy',
	'translations': 'Translations',
	'wave': 'WaveApplication',
	'wave': 'WaveDashboard',
	'wave': 'WaveDataflow',
	'wave': 'WaveDataset',
	'wave': 'WaveLens',
	'workflows': 'Workflow',
	'fields': 'CustomField',
	'listViews': 'ListView',
	'matchingRules': 'MatchingRule',
	'recordTypes': 'RecordType',
	'validationRules': 'ValidationRule',
	'objectWebLinks': 'WebLink'

};

exports.packageWriter = function(metadata, apiVersion) {
	apiVersion = apiVersion || '52.0';
	const xml = xmlBuilder.create('Package', { version: '1.0'});
	xml.att('xmlns', 'http://soap.sforce.com/2006/04/metadata');

	for (const type in metadata) {

		if (metadata.hasOwnProperty(type)) {
			const typeXml = xml.ele('types');
			metadata[type].forEach(function(item) {
				typeXml.ele('members', item);
			});

			typeXml.ele('name', metaMap[type]);
		}
	}
	xml.ele('version', apiVersion);

	return xml.end({pretty: true});
};

exports.buildPackageDir = function (basePath, dirName, name, metadata, packgeXML, destructive, cb) {

	let packageDir;
	let packageFileName;
	if (destructive) {
		packageDir = dirName + (dirName.substr(-1)==='/'?'':'/') + name + '/destructive';
		packageFileName = '/destructiveChanges.xml';
	} else {
		packageDir = dirName + (dirName.substr(-1)==='/'?'':'/') + name + '/unpackaged';
		packageFileName = '/package.xml';
	}

	// @todo -- should probably validate this a bit
	mkdirp(packageDir, (err) => {
		if(err) {
			return cb('Failed to write package directory ' + packageDir);
		}

		fs.writeFile(packageDir + packageFileName, packgeXML, 'utf8', (err) => {
			if(err) {
				return cb('Failed to write xml file');
			}

			return cb(null, packageDir);
		});

	});
};

exports.copyFiles = function(sourceDir, buildDir, files) {

	if(sourceDir.substr(-1) !== '/'){
		sourceDir = sourceDir + '/';
	}
	if(buildDir.substr(-1) !== '/'){
		buildDir = buildDir + '/';
	}
	console.log("Source Directory :"+sourceDir);
	console.log("Destination Directory : "+buildDir);
    files.forEach(function(file) {
        if(file) {
			if(fs.existsSync(sourceDir + file)){
				console.log("file :"+file);
				fs.copySync(sourceDir + file, buildDir + file);
				var auraLocator = file.indexOf("aura/");
				var lwcLocator = file.indexOf("lwc/");
				
			if (auraLocator >= 0) {
				// console.log("Aura bundle --- copy folder");
				var bundleFolder = file.substr(0, file.indexOf("/", auraLocator + 5));
				var destFolder = bundleFolder.substr(bundleFolder.indexOf("aura/"), bundleFolder.length);
				// console.log("Source folder to copy --- " + sourceDir + bundleFolder);
				// console.log("Dest folder --- " + destFolder);
				fs.copySync(sourceDir + bundleFolder, buildDir + destFolder);
			} else if (lwcLocator >= 0) {
				// console.log("Lightning Web Component bundle --- copy folder");
				var bundleFolder = file.substr(0, file.indexOf("/", lwcLocator + 5));
				var destFolder = bundleFolder.substr(bundleFolder.indexOf("lwc/"), bundleFolder.length);
				// console.log("Source folder to copy --- " + sourceDir + bundleFolder);
				// console.log("Dest folder --- " + destFolder);
				fs.copySync(sourceDir + bundleFolder, buildDir + destFolder);
			} else if(file.endsWith('-meta.xml')) {
				const nonMeta = file.replace('-meta.xml', '');
				if(fs.existsSync(sourceDir + nonMeta)){
					fs.copySync(sourceDir + nonMeta, buildDir + nonMeta);
				}
			} else {
				if(fs.existsSync(sourceDir + file + '-meta.xml')){
					const meta = file + '-meta.xml';
					fs.copySync(sourceDir + meta, buildDir + meta);
				}
			}
		}
			
    }
});

};
